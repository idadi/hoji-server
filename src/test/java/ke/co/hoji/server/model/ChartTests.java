package ke.co.hoji.server.model;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.LiveField;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static com.jayway.jsonassert.impl.matcher.IsCollectionWithSize.hasSize;
import static ke.co.hoji.server.model.ProjectSetup.createLiveFields;
import static ke.co.hoji.server.model.ProjectSetup.getBinarySingleChoiceField;
import static ke.co.hoji.server.model.ProjectSetup.getCategoricalSingleChoiceField;
import static ke.co.hoji.server.model.ProjectSetup.getDecimalNumberField;
import static ke.co.hoji.server.model.ProjectSetup.getMultiChoiceField;
import static ke.co.hoji.server.model.ProjectSetup.getTextField;
import static ke.co.hoji.server.model.ProjectSetup.getWholeNumberField;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;

public class ChartTests {

    @Test
    public void shouldInitializeChartWithKey() {
        Field singleChoiceField = getBinarySingleChoiceField();
        List<LiveField> liveFields = createLiveFields(singleChoiceField, 1L, 1L, 2L);

        Chart chart = new Chart(singleChoiceField, liveFields);

        Assert.assertThat(chart.getKey(), is(singleChoiceField.getId()));
    }

    @Test
    public void shouldInitializeChartWithLabel() {
        Field singleChoiceField = getBinarySingleChoiceField();
        List<LiveField> liveFields = createLiveFields(singleChoiceField, 1L, 1L, 2L);

        Chart chart = new Chart(singleChoiceField, liveFields);

        String expectedChartLabel = singleChoiceField.getName() + ". " + singleChoiceField.getResolvedHeader();
        Assert.assertThat(chart.getLabel(), is(expectedChartLabel));
    }

    @Test
    public void shouldInitializeChartWithOrdinal() {
        Field singleChoiceField = getBinarySingleChoiceField();
        List<LiveField> liveFields = createLiveFields(singleChoiceField, 1L, 1L, 2L);

        Chart chart = new Chart(singleChoiceField, liveFields);

        Assert.assertThat(chart.getOrdinal(), is(singleChoiceField.getOrdinal()));
    }

    @Test
    public void shouldInitializeChartWithCorrectSampleSize() {
        Field singleChoiceField = getBinarySingleChoiceField();
        List<LiveField> liveFields = createLiveFields(singleChoiceField, 1L, 1L, null);

        Chart chart = new Chart(singleChoiceField, liveFields);

        Assert.assertThat(chart.getSampleSize(), is(3));
        Assert.assertThat(chart.getData(), contains(
            new DataPoint("Yes", 2L),
            new DataPoint("No", 0L),
            new DataPoint("Missing", 1L)
        ));
    }

    @Test
    public void givenLiveFieldsForSingleChoiceFieldShouldInitializeBinaryChartWithCorrectDataPoints() {
        Field singleChoiceField = getBinarySingleChoiceField();
        List<LiveField> liveFields = createLiveFields(singleChoiceField, 1L, 1L, 2L);

        Chart chart = new Chart(singleChoiceField, liveFields);

        Assert.assertThat(chart.getType(), is(Chart.Type.BINARY));
        Assert.assertThat(chart.getData(), hasSize(2));
        Assert.assertThat(chart.getData(), contains(
            new DataPoint("Yes", 2L),
            new DataPoint("No", 1L)
        ));
    }

    @Test
    public void givenLiveFieldsForSingleChoiceFieldWithThreeChoicesShouldInitializeCategoricalChartWithCorrectDataPoints() {
        Field singleChoiceField = getCategoricalSingleChoiceField();
        List<LiveField> liveFields = createLiveFields(singleChoiceField, 1L, 1L, 2L);

        Chart chart = new Chart(singleChoiceField, liveFields);

        Assert.assertThat(chart.getType(), is(Chart.Type.CATEGORICAL));
        Assert.assertThat(chart.getData(), hasSize(3));
        Assert.assertThat(chart.getData(), contains(
            new DataPoint("Yes", 2L),
            new DataPoint("No", 1L),
            new DataPoint("Don't know", 0L)
        ));
    }

    @Test
    public void givenLiveFieldsForMultipleChoiceFieldShouldInitializeCategoricalChartWithCorrectDataPoints() {
        Field multipleChoiceField = getMultiChoiceField();
        String value1 = "1#0|2#1|3#1";
        String value2 = "1#1|2#1|3#0";
        List<LiveField> liveFields = createLiveFields(multipleChoiceField, value1, value2);

        Chart chart = new Chart(multipleChoiceField, liveFields);

        Assert.assertThat(chart.getType(), is(Chart.Type.CATEGORICAL));
        Assert.assertThat(chart.getData(), hasSize(3));
        Assert.assertThat(chart.getData(), contains(
            new DataPoint("Rugby", 1L),
            new DataPoint("Football", 2L),
            new DataPoint("Basketball", 1L)
        ));
    }

    @Test
    public void givenLiveFieldsForWholeNumberFieldShouldInitializeNumericIntegerChartWithCorrectDataPoints() {
        Field wholeNumberField = getWholeNumberField();
        List<LiveField> liveFields = createLiveFields(wholeNumberField, 30, 24);

        Chart chart = new Chart(wholeNumberField, liveFields);

        Assert.assertThat(chart.getType(), is(Chart.Type.NUMERIC_INTEGER));
        Assert.assertThat(chart.getData(), hasSize(2));
        Assert.assertThat(chart.getData(), contains(
            new DataPoint("30", 30),
            new DataPoint("24", 24)
        ));
    }

    @Test
    public void givenLiveFieldsForDecimalNumberFieldShouldInitializeNumericDecimalChartWithCorrectDataPoints() {
        Field decimalNumberField = getDecimalNumberField();
        List<LiveField> liveFields = createLiveFields(decimalNumberField, 30D, 24D);

        Chart chart = new Chart(decimalNumberField, liveFields);

        Assert.assertThat(chart.getType(), is(Chart.Type.NUMERIC_DECIMAL));
        Assert.assertThat(chart.getData(), hasSize(2));
        Assert.assertThat(chart.getData(), contains(
            new DataPoint("30.0", 30.0),
            new DataPoint("24.0", 24.0)
        ));
    }

    @Test
    public void givenLiveFieldsForTextFieldShouldInitializeTextualChartWithCorrectDataPoints() {
        Field textField = getTextField();
        String value1 = " Where's the fire?";
        String value2 = " Vice-president ";
        List<LiveField> liveFields = createLiveFields(textField, value1, value2);

        Chart chart = new Chart(textField, liveFields);

        Assert.assertThat(chart.getType(), is(Chart.Type.TEXTUAL));
        Assert.assertThat(chart.getData(), hasSize(2));
        String expectedValue1 = "wheres the fire";
        String expectedValue2 = "vice-president";
        Assert.assertThat(chart.getData(), contains(
            new DataPoint(textField.getId().toString(), expectedValue1),
            new DataPoint(textField.getId().toString(), expectedValue2)
        ));
    }

    @Test
    public void givenLiveFieldsWithNullValueForSingleChoiceFieldShouldInitializeBinaryChartWithMissingDataPoint() {
        Field singleChoiceField = getBinarySingleChoiceField();
        List<LiveField> liveFields = createLiveFields(singleChoiceField, null, null);

        Chart chart = new Chart(singleChoiceField, liveFields);

        Assert.assertThat(chart.getType(), is(Chart.Type.BINARY));
        Assert.assertThat(chart.getData(), hasSize(3));
        Assert.assertThat(chart.getData(), contains(
            new DataPoint("Yes", 0L),
            new DataPoint("No", 0L),
            new DataPoint("Missing", 2L)
        ));
    }

    @Test
    public void givenLiveFieldsWithNullValueForMultipleChoiceFieldShouldInitializeCategoricalChartWithMissingDataPoint() {
        Field multiChoiceField = getMultiChoiceField();
        List<LiveField> liveFields = createLiveFields(multiChoiceField, null, null);

        Chart chart = new Chart(multiChoiceField, liveFields);

        Assert.assertThat(chart.getType(), is(Chart.Type.CATEGORICAL));
        Assert.assertThat(chart.getData(), hasSize(4));
        Assert.assertThat(chart.getData(), contains(
            new DataPoint("Rugby", 0L),
            new DataPoint("Football", 0L),
            new DataPoint("Basketball", 0L),
            new DataPoint("Missing", 2L)
        ));
    }

    @Test
    public void givenLiveFieldsWithNullValueForWholeNumberFieldShouldInitializeNumericIntegerChartWithNullDataPoint() {
        Field wholeNumberField = getWholeNumberField();
        List<LiveField> liveFields = createLiveFields(wholeNumberField, null, null);

        Chart chart = new Chart(wholeNumberField, liveFields);

        Assert.assertThat(chart.getType(), is(Chart.Type.NUMERIC_INTEGER));
        Assert.assertThat(chart.getData(), hasSize(2));
        Assert.assertThat(chart.getData(), contains(
            new DataPoint("Missing", null),
            new DataPoint("Missing", null)
        ));
    }

    @Test
    public void givenLiveFieldsWithMissingValueForWholeNumberFieldShouldInitializeNumericIntegerChartWithNullDataPoint() {
        Field wholeNumberField = getWholeNumberField();
        wholeNumberField.setMissingValue("999, 000");
        List<LiveField> liveFields = createLiveFields(wholeNumberField, 999L, 0L);

        Chart chart = new Chart(wholeNumberField, liveFields);

        Assert.assertThat(chart.getType(), is(Chart.Type.NUMERIC_INTEGER));
        Assert.assertThat(chart.getData(), hasSize(2));
        Assert.assertThat(chart.getData(), contains(
            new DataPoint("Missing", null),
            new DataPoint("Missing", null)
        ));
    }

    @Test
    public void givenLiveFieldsWithNullValueForDecimalFieldShouldInitializeNumericDecimalChartWithNullDataPoint() {
        Field decimalNumberField = getDecimalNumberField();
        List<LiveField> liveFields = createLiveFields(decimalNumberField, null, null);

        Chart chart = new Chart(decimalNumberField, liveFields);

        Assert.assertThat(chart.getType(), is(Chart.Type.NUMERIC_DECIMAL));
        Assert.assertThat(chart.getData(), hasSize(2));
        Assert.assertThat(chart.getData(), contains(
            new DataPoint("Missing", null),
            new DataPoint("Missing", null)
        ));
    }

    @Test
    public void givenLiveFieldsWithMissingValueForDecimalNumberFieldShouldInitializeNumericDecimalChartWithNullDataPoint() {
        Field decimalNumberField = getDecimalNumberField();
        decimalNumberField.setMissingValue("999, 000, 999.0");
        List<LiveField> liveFields = createLiveFields(decimalNumberField, 999D, 0D);

        Chart chart = new Chart(decimalNumberField, liveFields);

        Assert.assertThat(chart.getType(), is(Chart.Type.NUMERIC_DECIMAL));
        Assert.assertThat(chart.getData(), hasSize(2));
        Assert.assertThat(chart.getData(), contains(
            new DataPoint("Missing", null),
            new DataPoint("Missing", null)
        ));
    }

    @Test
    public void givenLiveFieldsWithNullValueForTextFieldShouldInitializeTextualChartWithNoDataPoints() {
        Field textField = getTextField();
        List<LiveField> liveFields = createLiveFields(textField, null, null);

        Chart chart = new Chart(textField, liveFields);

        Assert.assertThat(chart.getType(), is(Chart.Type.TEXTUAL));
        Assert.assertThat(chart.getData(), hasSize(0));
    }

    @Test
    public void givenLiveFieldsWithMissingValueForTextFieldShouldInitializeTextualChartWithNoDataPoints() {
        Field textField = getTextField();
        textField.setMissingValue("John Doe , Jane Doe");
        List<LiveField> liveFields = createLiveFields(textField, "John Doe", "Jane Doe");

        Chart chart = new Chart(textField, liveFields);

        Assert.assertThat(chart.getType(), is(Chart.Type.TEXTUAL));
        Assert.assertThat(chart.getData(), hasSize(0));
    }
}