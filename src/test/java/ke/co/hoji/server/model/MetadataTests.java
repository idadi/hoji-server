package ke.co.hoji.server.model;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.server.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static ke.co.hoji.core.data.dto.DataElement.Type.DATE;
import static ke.co.hoji.core.data.dto.DataElement.Type.NUMBER;
import static ke.co.hoji.core.data.dto.DataElement.Type.STRING;
import static ke.co.hoji.server.model.Metadata.ISO_DATE_TIME_FORMAT;
import static ke.co.hoji.server.model.Metadata.Label.CREATED_BY;
import static ke.co.hoji.server.model.Metadata.Label.DATE_COMPLETED;
import static ke.co.hoji.server.model.Metadata.Label.DATE_CREATED;
import static ke.co.hoji.server.model.Metadata.Label.DATE_UPDATED;
import static ke.co.hoji.server.model.Metadata.Label.DATE_UPLOADED;
import static ke.co.hoji.server.model.Metadata.Label.SPEED;
import static ke.co.hoji.server.model.Metadata.Label.TIME_TO_COMPLETE;
import static ke.co.hoji.server.model.Metadata.Label.USER_ID;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MetadataTests {

    @Test
    public void getMetadata_shouldReturnListOfDataElementsWithDateDetails() {
        MainRecord mainRecord = Mockito.mock(MainRecord.class);
        UserService userService = Mockito.mock(UserService.class);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -3);
        Date aMinuteAgo = calendar.getTime();
        Date now = Calendar.getInstance().getTime();
        when(mainRecord.getDateCreated()).thenReturn(aMinuteAgo.getTime());
        when(mainRecord.getDateUpdated()).thenReturn(aMinuteAgo.getTime());
        when(mainRecord.getDateCompleted()).thenReturn(now.getTime());
        when(mainRecord.getDateUploaded()).thenReturn(now.getTime());
        when(mainRecord.getLiveFields()).thenReturn(Arrays.asList(new LiveField(), new LiveField()));
        when(userService.findById(anyInt())).thenReturn(new User());

        List<DataElement> metadata = new Metadata(mainRecord, userService).getMetadata();

        Assert.assertThat(metadata.size(), is(18));
        Assert.assertThat(metadata, hasItems(
                new DataElement(DATE_CREATED, Utils.formatIsoDateTime(aMinuteAgo), DATE, ISO_DATE_TIME_FORMAT),
                new DataElement(DATE_UPDATED, Utils.formatIsoDateTime(aMinuteAgo), DATE, ISO_DATE_TIME_FORMAT),
                new DataElement(DATE_COMPLETED, Utils.formatIsoDateTime(now), DATE, ISO_DATE_TIME_FORMAT),
                new DataElement(DATE_UPLOADED, Utils.formatIsoDateTime(now), DATE, ISO_DATE_TIME_FORMAT),
                new DataElement(SPEED, String.valueOf(Utils.roundDouble(2D/3D)), NUMBER, ""),
                new DataElement(TIME_TO_COMPLETE, String.valueOf(3D), NUMBER, "")
        ));
    }

    @Test
    public void getMetadata_shouldReturnListOfDataElementsWithUserDetails() {
        MainRecord mainRecord = Mockito.mock(MainRecord.class);
        UserService userService = Mockito.mock(UserService.class);
        Integer userId = 1;
        when(mainRecord.getUserId()).thenReturn(userId);
        User user = new User();
        user.setFullName("Test User");
        when(userService.findById(eq(userId))).thenReturn(user);

        List<DataElement> metadata = new Metadata(mainRecord, userService).getMetadata();

        Assert.assertThat(metadata.size(), is(18));
        Assert.assertThat(metadata, hasItems(
                new DataElement(USER_ID, userId.toString(), NUMBER, userId.toString()),
                new DataElement(CREATED_BY, user.getFullName(), STRING, "")
        ));
    }
}