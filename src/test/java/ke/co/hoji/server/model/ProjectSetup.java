package ke.co.hoji.server.model;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.response.DecimalResponse;
import ke.co.hoji.core.response.MultipleChoiceResponse;
import ke.co.hoji.core.response.NumberResponse;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.response.SingleChoiceResponse;
import ke.co.hoji.core.response.TextResponse;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public class ProjectSetup {

    public static Field getTextField() {
        FieldType textType = new FieldType(2, "TEXT", FieldType.Code.SINGLE_LINE_TEXT);
        textType.setDataType(FieldType.DataType.STRING);
        textType.setResponseClass(TextResponse.class.getName());
        Field textField = new Field(1);
        textField.setType(textType);
        textField.setEnabled(true);
        Form form = new Form(1);
        textField.setForm(form);
        form.setFields(Collections.singletonList(textField));
        return textField;
    }

    public static Field getWholeNumberField() {
        FieldType wholeNumberType = new FieldType(2, "WHOLE NUMBER", FieldType.Code.WHOLE_NUMBER);
        wholeNumberType.setDataType(FieldType.DataType.NUMBER);
        wholeNumberType.setResponseClass(NumberResponse.class.getName());
        Field wholeNumberField = new Field(2);
        wholeNumberField.setType(wholeNumberType);
        wholeNumberField.setEnabled(true);
        Form form = new Form(1);
        wholeNumberField.setForm(form);
        form.setFields(Collections.singletonList(wholeNumberField));
        return wholeNumberField;
    }

    public static Field getDecimalNumberField() {
        FieldType decimalNumberType = new FieldType(2, "DECIMAL NUMBER", FieldType.Code.DECIMAL_NUMBER);
        decimalNumberType.setDataType(FieldType.DataType.DECIMAL);
        decimalNumberType.setResponseClass(DecimalResponse.class.getName());
        Field decimalNumberField = new Field(3);
        decimalNumberField.setType(decimalNumberType);
        decimalNumberField.setEnabled(true);
        Form form = new Form(1);
        decimalNumberField.setForm(form);
        form.setFields(Collections.singletonList(decimalNumberField));
        return decimalNumberField;
    }

    public static Field getBinarySingleChoiceField() {
        Choice yes = new Choice(1, "Yes", "Y", 0, false);
        Choice no = new Choice(2, "No", "N", 0, false);
        ChoiceGroup yesNo = new ChoiceGroup(asList(yes, no));
        FieldType singleChoiceType = new FieldType(1, "Single Choice", FieldType.Code.SINGLE_CHOICE);
        singleChoiceType.setDataType(FieldType.DataType.NUMBER);
        singleChoiceType.setResponseClass(SingleChoiceResponse.class.getName());
        Field singleChoiceField = new Field(4);
        singleChoiceField.setType(singleChoiceType);
        singleChoiceField.setEnabled(true);
        singleChoiceField.setChoiceGroup(yesNo);
        Form form = new Form(1);
        singleChoiceField.setForm(form);
        form.setFields(Collections.singletonList(singleChoiceField));
        return singleChoiceField;
    }

    public static Field getCategoricalSingleChoiceField() {
        Choice yes = new Choice(1, "Yes", "Y", 3, false);
        Choice no = new Choice(2, "No", "N", 2, false);
        Choice dontKnow = new Choice(3, "Don't know", "Dk", 1, false);
        ChoiceGroup yesNo = new ChoiceGroup(asList(yes, no, dontKnow));
        FieldType singleChoiceType = new FieldType(1, "Single Choice", FieldType.Code.SINGLE_CHOICE);
        singleChoiceType.setDataType(FieldType.DataType.NUMBER);
        singleChoiceType.setResponseClass(SingleChoiceResponse.class.getName());
        Field singleChoiceField = new Field(5);
        singleChoiceField.setType(singleChoiceType);
        singleChoiceField.setEnabled(true);
        singleChoiceField.setChoiceGroup(yesNo);
        Form form = new Form(1);
        singleChoiceField.setForm(form);
        form.setFields(Collections.singletonList(singleChoiceField));
        return singleChoiceField;
    }

    public static Field getMultiChoiceField() {
        Choice rugby = new Choice(1, "Rugby", "R", 10, false);
        Choice football = new Choice(2, "Football", "F", 8, false);
        Choice basketball = new Choice(3, "Basketball", "B", 6, false);
        ChoiceGroup yesNo = new ChoiceGroup(asList(rugby, football, basketball));
        FieldType multiChoiceType = new FieldType(1, "Multi Choice", FieldType.Code.MULTIPLE_CHOICE);
        multiChoiceType.setDataType(FieldType.DataType.STRING);
        multiChoiceType.setResponseClass(MultipleChoiceResponse.class.getName());
        Field multiChoiceField = new Field(6);
        multiChoiceField.setType(multiChoiceType);
        multiChoiceField.setEnabled(true);
        multiChoiceField.setChoiceGroup(yesNo);
        Form form = new Form(1);
        multiChoiceField.setForm(form);
        form.setFields(Collections.singletonList(multiChoiceField));
        return multiChoiceField;
    }

    static List<LiveField> createLiveFields(Field field, Object... values) {
        return Stream
                .of(values)
                .map(value -> {
                    LiveField liveField = new LiveField(field);
                    liveField.setResponse(Response.create(null, liveField, value, false));
                    return liveField;
                })
                .collect(Collectors.toList());
    }
}
