package ke.co.hoji.server.model;

import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static ke.co.hoji.core.data.dto.DataElement.Type.NUMBER;
import static ke.co.hoji.core.data.dto.DataElement.Type.STRING;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;

public class DataTableTests {

    @Test
    public void shouldReturnTransposedDataRow() {
        Form form = getForm("Indicator,Sex,Age Group", "Students");
        String maleTestedColumn = "Number Tested|Male|15 - 19";
        String femaleTestedColumn = "Number Tested|Female|15 - 19";
        Field maleTested = getField(1, maleTestedColumn);
        Field femaleTested = getField(2, femaleTestedColumn);
        form.setFields(Arrays.asList(maleTested, femaleTested));
        DataElement testedMaleDataElement = new DataElement(maleTestedColumn, "12", NUMBER, "");
        DataElement testedFemaleDataElement = new DataElement(femaleTestedColumn, "18", NUMBER, "");
        DataRow dataRow = new DataRow(Arrays.asList(testedMaleDataElement, testedFemaleDataElement));
        DataTable dataTable = new DataTable(Collections.singletonList(dataRow), "");

        DataTable transposed = dataTable.requestTranspose(form);

        Assert.assertThat(transposed.getDataRows(), hasSize(2));
        //males
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements(), hasSize(4));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(0).getLabel(), is("Indicator"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(0).getValue(), is("Number Tested"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(1).getLabel(), is("Sex"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(1).getValue(), is("Male"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(2).getLabel(), is("Age Group"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(2).getValue(), is("15 - 19"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(3).getLabel(), is("Students"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(3).getValue(), is("12"));
        //female
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements(), hasSize(4));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(0).getLabel(), is("Indicator"));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(0).getValue(), is("Number Tested"));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(1).getLabel(), is("Sex"));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(1).getValue(), is("Female"));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(2).getLabel(), is("Age Group"));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(2).getValue(), is("15 - 19"));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(3).getLabel(), is("Students"));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(3).getValue(), is("18"));
    }

    @Test
    public void shouldReturnTransposedDataRowWithExtraDataElements() {
        Form form = getForm("Indicator,Sex,Age Group", "Students");
        String maleTestedColumn = "Number Tested|Male|15 - 19";
        Field maleTested = getField(1, maleTestedColumn);
        form.setFields(Collections.singletonList(maleTested));
        DataElement recordId = new DataElement("ID", "1", NUMBER, "");
        DataElement testedMaleDataElement = new DataElement(maleTestedColumn, "12", NUMBER, "");
        DataElement locationDataElement = new DataElement("Location", "Location A", STRING, "");
        DataRow dataRow = new DataRow(Arrays.asList(recordId, testedMaleDataElement, locationDataElement));
        DataTable dataTable = new DataTable(Collections.singletonList(dataRow), "");

        DataTable transposed = dataTable.requestTranspose(form);

        Assert.assertThat(transposed.getDataRows(), hasSize(1));
        //males
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements(), hasSize(6));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(0).getLabel(), is("ID"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(0).getValue(), is("1"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(1).getLabel(), is("Indicator"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(1).getValue(), is("Number Tested"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(2).getLabel(), is("Sex"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(2).getValue(), is("Male"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(3).getLabel(), is("Age Group"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(3).getValue(), is("15 - 19"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(4).getLabel(), is("Students"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(4).getValue(), is("12"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(5).getLabel(), is("Location"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(5).getValue(), is("Location A"));
    }

    @Test
    public void shouldReturnTransposedDataRowWithDefaultVariableAndValueLabels() {
        Form form = getForm(null, null);
        String maleTestedColumn = "Number Tested|Male|15 - 19";
        String femaleTestedColumn = "Number Tested|Female|15 - 19";
        Field maleTested = getField(1, maleTestedColumn);
        Field femaleTested = getField(2, femaleTestedColumn);
        form.setFields(Arrays.asList(maleTested, femaleTested));
        DataElement testedMaleDataElement = new DataElement(maleTestedColumn, "12", NUMBER, "");
        DataElement testedFemaleDataElement = new DataElement(femaleTestedColumn, "18", NUMBER, "");
        DataRow dataRow = new DataRow(Arrays.asList(testedMaleDataElement, testedFemaleDataElement));
        DataTable dataTable = new DataTable(Collections.singletonList(dataRow), "");

        DataTable transposed = dataTable.requestTranspose(form);

        Assert.assertThat(transposed.getDataRows(), hasSize(2));
        //males
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements(), hasSize(4));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(0).getLabel(), is("Category 1"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(0).getValue(), is("Number Tested"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(1).getLabel(), is("Category 2"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(1).getValue(), is("Male"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(2).getLabel(), is("Category 3"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(2).getValue(), is("15 - 19"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(3).getLabel(), is("Value"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(3).getValue(), is("12"));
        //female
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements(), hasSize(4));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(0).getLabel(), is("Category 1"));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(0).getValue(), is("Number Tested"));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(1).getLabel(), is("Category 2"));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(1).getValue(), is("Female"));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(2).getLabel(), is("Category 3"));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(2).getValue(), is("15 - 19"));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(3).getLabel(), is("Value"));
        Assert.assertThat(transposed.getDataRows().get(1).getDataElements().get(3).getValue(), is("18"));
    }

    @Test
    public void shouldReturnUnchangedDataTableWhenFieldVariableNamesDoNotMatch() {
        Form form = getForm("Indicator,Sex", "Value");
        String maleTestedColumn = "Number Tested|";
        Field maleTested = getField(1, maleTestedColumn);
        String firstTestedColumn = "First Tested|Male";
        Field firstTested = getField(2, firstTestedColumn);
        form.setFields(Arrays.asList(maleTested, firstTested));
        DataElement totalDataElement = new DataElement(maleTestedColumn, "12", NUMBER, "");
        DataElement firstTestedDataElement = new DataElement(firstTestedColumn, "5", NUMBER, "");
        DataRow dataRow = new DataRow(Arrays.asList(totalDataElement, firstTestedDataElement));
        DataTable dataTable = new DataTable(Collections.singletonList(dataRow), "");

        DataTable transposed = dataTable.requestTranspose(form);

        Assert.assertThat(transposed.getDataRows(), hasSize(1));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements(), hasSize(2));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(0).getLabel(), is("Number Tested|"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(0).getValue(), is("12"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(1).getLabel(), is("First Tested|Male"));
        Assert.assertThat(transposed.getDataRows().get(0).getDataElements().get(1).getValue(), is("5"));
    }

    private Form getForm(String categories, String valueLabel) {
        Form form = new Form(1);
        form.setTranspositionCategories(categories);
        form.setTranspositionValueLabel(valueLabel);
        return form;
    }

    private Field getField(int id, String variableName) {
        FieldType numericType = new FieldType(2, "Numeric", FieldType.Code.WHOLE_NUMBER);
        Field field = new Field(id);
        field.setColumn(variableName);
        field.setType(numericType);
        return field;
    }

}