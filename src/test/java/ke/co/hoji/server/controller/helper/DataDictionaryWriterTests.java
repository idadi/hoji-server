package ke.co.hoji.server.controller.helper;

import static org.hamcrest.Matchers.is;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;
import org.supercsv.prefs.CsvPreference;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;

/**
 * Created by geoffreywasilwa on 10/07/2017.
 */
public class DataDictionaryWriterTests {

    private static String HEADERS = "No.,Field,Choice Type,Choice,Code,Scale,Description";

    @Test
    public void shouldGenerateCsvFileGivenFormWithSingleChoiceField() throws Exception {
        ChoiceGroup yesNo = getYesNoGroup();
        FieldType type = new FieldType(1, "Select one", FieldType.Code.SINGLE_CHOICE);
        Field field = getField(type);
        field.setChoiceGroup(yesNo);
        Form form = new Form();
        form.setFields(Collections.singletonList(field));
        CsvPreference preference = CsvPreference.STANDARD_PREFERENCE;
        String expectedOutput =
                HEADERS + preference.getEndOfLineSymbols() +
                        "1,Question #1," + type.getName() + ",Yes,Y,0," + preference.getEndOfLineSymbols()
                        + ",,,No,N,0," + preference.getEndOfLineSymbols();

        File codeBook = File.createTempFile("test", ".csv");
        DataDictionaryWriter.write(form, preference, codeBook);
        Assert.assertThat(new String(Files.readAllBytes(codeBook.toPath())), is(expectedOutput));
        codeBook.delete();
    }

    @Test
    public void shouldGenerateCsvFileWithFieldColumn() throws Exception {
        ChoiceGroup yesNo = getYesNoGroup();
        FieldType type = new FieldType(1, "Select one", FieldType.Code.SINGLE_CHOICE);
        Field field = getField(type);
        field.setChoiceGroup(yesNo);
        field.setColumn("Column");
        Form form = new Form();
        form.setFields(Collections.singletonList(field));
        CsvPreference preference = CsvPreference.STANDARD_PREFERENCE;
        String expectedOutput =
                HEADERS + preference.getEndOfLineSymbols() +
                        "1,Column," + type.getName() + ",Yes,Y,0," + preference.getEndOfLineSymbols() + ",,,No,N,0,"
                        + preference.getEndOfLineSymbols();

        File codeBook = File.createTempFile("test", ".csv");
        DataDictionaryWriter.write(form, preference, codeBook);
        Assert.assertThat(new String(Files.readAllBytes(codeBook.toPath())), is(expectedOutput));
        codeBook.delete();
    }

    @Test
    public void shouldGenerateCsvFileGivenFormWithMultiChoiceField() throws Exception {
        Choice yes = new Choice(1, "Yes", null, "Y", 0, false);
        Choice no = new Choice(2, "No", null, "N", 0, false);
        Choice dk = new Choice(3, "Don't Know", null, "DK", 0, false);
        ChoiceGroup yesNoDk = new ChoiceGroup(Arrays.asList(yes, no, dk));
        FieldType type = new FieldType(1, "Select multiple", FieldType.Code.MULTIPLE_CHOICE);
        Field field = getField(type);
        field.setChoiceGroup(yesNoDk);
        Form form = new Form();
        form.setFields(Collections.singletonList(field));
        CsvPreference preference = CsvPreference.STANDARD_PREFERENCE;
        String expectedOutput =
                HEADERS + preference.getEndOfLineSymbols() +
                        "1,Question #1," + type.getName() + ",Yes,Y,0," + preference.getEndOfLineSymbols()
                        + ",,,No,N,0," + preference.getEndOfLineSymbols() + ",,,Don't Know,DK,0,"
                        + preference.getEndOfLineSymbols();

        File codeBook = File.createTempFile("test", ".csv");
        DataDictionaryWriter.write(form, preference, codeBook);
        Assert.assertThat(new String(Files.readAllBytes(codeBook.toPath())), is(expectedOutput));
        codeBook.delete();
    }

    @Test
    public void shouldGenerateEmptyCsvFileGivenFormWithNoChoiceField() throws Exception {
        FieldType type = new FieldType(1, "Number", FieldType.Code.WHOLE_NUMBER);
        Field field = getField(type);
        Form form = new Form();
        form.setFields(Collections.singletonList(field));
        CsvPreference preference = CsvPreference.STANDARD_PREFERENCE;
        String expectedOutput = HEADERS + preference.getEndOfLineSymbols();

        File codeBook = File.createTempFile("test", ".csv");
        DataDictionaryWriter.write(form, preference, codeBook);
        Assert.assertThat(new String(Files.readAllBytes(codeBook.toPath())), is(expectedOutput));
        codeBook.delete();
    }

    private ChoiceGroup getYesNoGroup() {
        Choice yes = new Choice(1, "Yes", null, "Y", 0, false);
        Choice no = new Choice(2, "No", null, "N", 0, false);
        return new ChoiceGroup(Arrays.asList(yes, no));
    }

    private Field getField(FieldType type) {
        Field field = new Field();
        field.setId(1);
        field.setName("1");
        field.setDescription("Question #1");
        field.setType(type);
        return field;
    }

}