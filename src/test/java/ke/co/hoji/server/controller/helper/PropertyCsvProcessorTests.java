package ke.co.hoji.server.controller.helper;

import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.server.exception.CsvFormatException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;

public class PropertyCsvProcessorTests {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void process_shouldThrowErrorWhenCsvIsNull() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("inputStream cannot be null");

        new PropertyCsvProcessor().process(null);
    }

    @Test
    public void process_shouldThrowErrorWhenCsvIsBlank() {
        thrown.expect(CsvFormatException.class);
        thrown.expectMessage("The uploaded csv is blank");

        new PropertyCsvProcessor().process(new ByteArrayInputStream("".getBytes(UTF_8)));
    }

    @Test
    public void process_shouldFailWhenProvidedCsvHeadersAreIncorrect() {
        thrown.expect(CsvFormatException.class);
        thrown.expectMessage("The expected headers are: key, value");

        new PropertyCsvProcessor().process(new ByteArrayInputStream("k,v".getBytes(UTF_8)));
    }

    @Test
    public void process_shouldFailWhenProvidedCsvHeadersAreMoreThanExpected() {
        thrown.expect(CsvFormatException.class);
        thrown.expectMessage("The expected headers are: key, value");
        ByteArrayInputStream inputStream = new ByteArrayInputStream("key,value,survey_id".getBytes(UTF_8));

        new PropertyCsvProcessor().process(inputStream);
    }

    @Test
    public void process_shouldThrowWhenCsvContainsOnlyHeaders() {
        thrown.expect(CsvFormatException.class);
        thrown.expectMessage("The uploaded csv only contains headers, please provide values and re-upload");
        InputStream inputStream = new ByteArrayInputStream("key,value".getBytes(UTF_8));

        new PropertyCsvProcessor().process(inputStream);
    }

    @Test
    public void process_shouldReturnOneProperty() {
        InputStream inputStream = new ByteArrayInputStream("key,value\n1,200".getBytes(UTF_8));

        List<Property> properties = new PropertyCsvProcessor().process(inputStream);

        Assert.assertThat(properties, hasItems(
                allOf(
                        hasProperty("key", is("1")),
                        hasProperty("value", is("200"))
                )
        ));
    }

    @Test
    public void process_shouldProcessPropertiesSpecifiedInAnyOrder() {
        InputStream inputStream = new ByteArrayInputStream("value,key\n200,1".getBytes(UTF_8));

        List<Property> properties = new PropertyCsvProcessor().process(inputStream);

        Assert.assertThat(properties, hasItems(
                allOf(
                        hasProperty("key", is("1")),
                        hasProperty("value", is("200"))
                )
        ));
    }

    @Test
    public void process_shouldUseLastPropertyInCaseOfDuplicates() {
        InputStream inputStream = new ByteArrayInputStream("key,value\n1,200\n1,50".getBytes(UTF_8));

        List<Property> properties = new PropertyCsvProcessor().process(inputStream);

        Assert.assertThat(properties, hasItems(
                allOf(
                        hasProperty("key", is("1")),
                        hasProperty("value", is("50"))
                )
        ));
    }

}
