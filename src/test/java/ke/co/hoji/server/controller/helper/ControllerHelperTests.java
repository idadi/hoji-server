package ke.co.hoji.server.controller.helper;

import ke.co.hoji.server.model.PostError;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.validation.FieldError;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 13/07/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class ControllerHelperTests {

    @Mock
    private MessageSource messageSource;

    @Test
    public void shouldReturnPostErrorsGivenFieldErrors() throws Exception {
        when(messageSource.getMessage(any(MessageSourceResolvable.class), any(Locale.class)))
                .thenReturn("message");
        FieldError fieldError = new FieldError("field", "id", "message");
        List<PostError> postErrors = new ControllerHelper(messageSource)
                .getPostErrors(Collections.singletonList(fieldError));

        Assert.assertThat(postErrors, hasSize(1));
        Assert.assertThat(postErrors.get(0).getParameter(), is("id"));
        Assert.assertThat(postErrors.get(0).getMessage(), is("message"));
    }

    @Test
    public void shouldReturnPostErrorsGivenParameterAndMessage() throws Exception {
        List<PostError> postErrors = new ControllerHelper(messageSource)
                .getPostErrors("parameter", "message");

        Assert.assertThat(postErrors, hasSize(1));
        Assert.assertThat(postErrors.get(0).getParameter(), is("parameter"));
        Assert.assertThat(postErrors.get(0).getMessage(), is("message"));
    }

    @Test
    public void shouldReturnCorrectRedirectString() throws Exception {
        String expected = "redirect:/url";

        Assert.assertThat(new ControllerHelper(messageSource).redirect("url"), is(expected));
    }

}