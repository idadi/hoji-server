package ke.co.hoji.server.controller.helper;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.server.exception.CsvFormatException;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;

/**
 * Created by geoffreywasilwa on 21/04/2017.
 */
public class ChoiceCsvProcessorTests {

    String input = 
            "Code,Name,Description,Parent,Scale,Loner,Choice_Group\n" + 
            "1,Yes,,,,,Yes/No\n" + 
            "2,No,,,,,Yes/No\n" + 
            "1,Yes,,,,,Yes/No/DK\n" + 
            "2,No,,,,,Yes/No/DK\n" + 
            "96,DK,,,,,Yes/No/DK\n" + 
            "10,Africa,,,,,Continents\n" + 
            "20,Asia,,,,,Continents\n" + 
            "100,Kenya,,Africa,,,Countries\n" + 
            "200,Nigeria,,Africa,,,Countries\n" + 
            "300,India,,Asia,,,Countries\n" + 
            "400,Japan,,Asia,,,Countries";

    String inputWithDuplicates = 
            "Code,Name,Description,Parent,Scale,Loner,Choice_Group\n" + 
            "1,yes ,,,,,Yes/No \n" + 
            "1,Yes,yes option,,10,0,Yes/No\n" + 
            "2, no,,,,,Yes/No\n" + 
            "2,No,no option,,20,0,Yes/No\n";

    String inputWithoutCodes =
            "Code,Name,Description,Parent,Scale,Loner,Choice_Group\n" + 
            ", Yes,,,,, Yes/No \n"
            + ",No ,,,,,Yes/No\n";

    String africa = 
            "Code,Name,Description,Parent,Scale,Loner,Choice_Group\n" + 
            "10,Africa,,,,,Continents\n" + 
            "100,Kenya,,Africa,,,Countries\n" + 
            "200,Uganda,,Africa,,,Countries\n" + 
            "300,Tanzania,,Africa,,,Countries\n" + 
            "400,Nigeria,,Africa,,,Countries\n";

    String noGroup =
            "Code,Name,Description,Parent,Scale,Loner,Choice_Group\n" + 
            "1,Yes,,,,,Yes/No\n" + 
            "2,No,,,,,Yes/No\n" + 
            "1,Yes,,,,,Yes/No/DK\n" + 
            "2,No,,,,,Yes/No/DK\n" + 
            "96,DK,,,,,Yes/No/DK\n" + 
            "20,Asia,,,,,Continents\n" + 
            "100,Kenya,,Africa,,,Countries\n" + 
            "200,Nigeria,,Africa,,,Countries\n" + 
            "300,India,,Asia,,,Countries\n" + 
            "400,Japan,,Asia,,,Countries";

    String withParent = 
            "Code,Name,Description,Parent,Scale,Loner,Choice_Group\n" + 
            "1,Audi,,,,,Car Make\n" + 
            "1,A4,,Audi,,,Car Model\n" + 
            "2,A6,,Audi,,,Car Model\n";

    String withParentOutOfOrder = 
            "Code,Name,Description,Parent,Scale,Loner,Choice_Group\n" + 
            "1,A4,,Audi,,,Car Model\n" + 
            "2,A6,,Audi,,,Car Model\n" + 
            "1,Audi,,,,,Car Make\n";

    String withSimilarParentChildName = 
            "Code,Name,Description,Parent,Scale,Loner,Choice_Group\n" +
            "1,Garissa,,,,,County\n" + 
            "1,Garissa,,Garissa,,,Sub county\n" + 
            "2,Ijara,,Garissa,,,Sub county\n";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowWhenCsvBlank() {
        thrown.expect(CsvFormatException.class);
        InputStream inputStream = new ByteArrayInputStream("".getBytes(StandardCharsets.UTF_8));
        new ChoiceCsvProcessor().process(inputStream);
    }

    @Test
    public void shouldThrowWhenCsvContainsOnlyHeaders() {
        thrown.expect(CsvFormatException.class);
        InputStream inputStream = new ByteArrayInputStream(
                "Code,Name,Description,Parent,Scale,Loner,Choice_Group".getBytes(StandardCharsets.UTF_8));
        new ChoiceCsvProcessor().process(inputStream);
    }

    @Test
    public void shouldThrowWhenCsvHasMissingHeaders() {
        thrown.expect(CsvFormatException.class);
        thrown.expectMessage(containsString("Description, Parent"));
        InputStream inputStream = new ByteArrayInputStream(
                "Code,Name,Scale,Loner,Choice_Group".getBytes(StandardCharsets.UTF_8));
        new ChoiceCsvProcessor().process(inputStream);
    }

    @Test
    public void shouldThrowWhenCsvHasInvalidHeaders() {
        thrown.expect(CsvFormatException.class);
        thrown.expectMessage(containsString("Code, Name, Description, Parent, Scale, Loner, Choice_Group"));
        InputStream inputStream = new ByteArrayInputStream("Wrong,Wrong".getBytes(StandardCharsets.UTF_8));
        new ChoiceCsvProcessor().process(inputStream);
    }

    @Test
    public void shouldGenerateChoiceGroupGraph() {
        InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        List<ChoiceGroup> choiceGroups = new ChoiceCsvProcessor().process(inputStream);

        Assert.assertThat(choiceGroups, hasSize(4));
        Assert.assertThat(choiceGroups.get(0).getChoices(), hasSize(2));
        Assert.assertThat(choiceGroups.get(1).getChoices(), hasSize(3));
        Assert.assertThat(choiceGroups.get(2).getChoices(), hasSize(2));
        Assert.assertThat(choiceGroups.get(3).getChoices(), hasSize(4));
    }

    @Test
    public void shouldMaintainListOfChoicesAsSpecifiedInCsv() {
        Choice kenya = new Choice(null, "Kenya", "100", 0, false);
        Choice uganda = new Choice(null, "Uganda", "200", 0, false);
        Choice tanzania = new Choice(null, "Tanzania", "300", 0, false);
        Choice nigeria = new Choice(null, "Nigeria", "400", 0, false);
        InputStream inputStream = new ByteArrayInputStream(africa.getBytes(StandardCharsets.UTF_8));
        List<ChoiceGroup> choiceGroups = new ChoiceCsvProcessor().process(inputStream);

        Assert.assertThat(choiceGroups, hasSize(2));
        Assert.assertThat(new ArrayList<ChoiceGroup>(choiceGroups).get(1).getChoices(),
                contains(kenya, uganda, tanzania, nigeria));
    }

    @Test
    public void shouldReturnFirstChoiceOnDuplicateChoices() {
        InputStream inputStream = new ByteArrayInputStream(inputWithDuplicates.getBytes(StandardCharsets.UTF_8));
        List<ChoiceGroup> choiceGroups = new ChoiceCsvProcessor().process(inputStream);

        Assert.assertThat(choiceGroups, hasSize(1));
        Assert.assertThat(new ArrayList<>(choiceGroups).get(0).getChoices(), hasSize(2));
        Choice yes1 = new ArrayList<>(choiceGroups).get(0).getChoices().get(0);
        Assert.assertThat(yes1.getName(), is("yes"));
        Assert.assertThat(yes1.getCode(), is("1"));
        Assert.assertNull(yes1.getDescription());
        Assert.assertThat(yes1.getScale(), is(0));
        Assert.assertThat(yes1.isLoner(), is(false));
        Choice no1 = new ArrayList<>(choiceGroups).get(0).getChoices().get(1);
        Assert.assertThat(no1.getName(), is("no"));
        Assert.assertThat(no1.getCode(), is("2"));
        Assert.assertNull(no1.getDescription());
        Assert.assertThat(no1.getScale(), is(0));
        Assert.assertThat(no1.isLoner(), is(false));
    }

    @Test
    public void shouldGenerateChoiceGroupGraphWithoutChoiceCodes() {
        InputStream inputStream = new ByteArrayInputStream(inputWithoutCodes.getBytes(StandardCharsets.UTF_8));
        List<ChoiceGroup> choiceGroups = new ChoiceCsvProcessor().process(inputStream);

        Assert.assertThat(choiceGroups, hasSize(1));
        Assert.assertThat(new ArrayList<>(choiceGroups).get(0).getChoices(), hasSize(2));
        Choice yes = new ArrayList<>(choiceGroups).get(0).getChoices().get(0);
        Assert.assertThat(yes.getName(), is("Yes"));
        Assert.assertThat(yes.getCode(), nullValue());
        Choice no = new ArrayList<>(choiceGroups).get(0).getChoices().get(1);
        Assert.assertThat(no.getName(), is("No"));
        Assert.assertThat(no.getCode(), nullValue());
    }

    @Test
    public void shouldGenerateChoicesWithParents() {
        InputStream inputStream = new ByteArrayInputStream(withParent.getBytes(StandardCharsets.UTF_8));
        List<ChoiceGroup> choiceGroups = new ChoiceCsvProcessor().process(inputStream);

        Assert.assertThat(choiceGroups, hasSize(2));
        Choice a4 = new ArrayList<>(choiceGroups).get(1).getChoices().get(0);
        Choice a6 = new ArrayList<>(choiceGroups).get(1).getChoices().get(1);
        Assert.assertThat(a4.getParent().getName(), is("Audi"));
        Assert.assertThat(a6.getParent().getName(), is("Audi"));
    }

    @Test
    public void shouldGenerateChoicesWithParentsOutOfOrder() {
        InputStream inputStream = new ByteArrayInputStream(withParentOutOfOrder.getBytes(StandardCharsets.UTF_8));

        List<ChoiceGroup> choiceGroups = new ChoiceCsvProcessor().process(inputStream);

        Assert.assertThat(choiceGroups, hasSize(2));
        Choice a4 = new ArrayList<>(choiceGroups).get(0).getChoices().get(0);
        Choice a6 = new ArrayList<>(choiceGroups).get(0).getChoices().get(1);
        Assert.assertThat(a4.getParent().getName(), is("Audi"));
        Assert.assertThat(a6.getParent().getName(), is("Audi"));
    }

    @Test
    public void shouldThrowWhenCsvHeadersAreNotInCorrectOrder() {
        thrown.expect(CsvFormatException.class);
        thrown.expectMessage(containsString("Code, Name, Description, Parent, Scale, Loner, Choice_Group"));
        InputStream inputStream = new ByteArrayInputStream(
                "Description,Parent,Code,Name,Scale,Loner,Choice_Group".getBytes(StandardCharsets.UTF_8));
        new ChoiceCsvProcessor().process(inputStream);
    }

    @Test
    public void shouldDifferentiateParentChildSharingSimilarName() {
        InputStream inputStream = new ByteArrayInputStream(withSimilarParentChildName.getBytes(StandardCharsets.UTF_8));

        List<ChoiceGroup> choiceGroups = new ChoiceCsvProcessor().process(inputStream);

        Assert.assertThat(choiceGroups, hasSize(2));
        Choice garissaCounty = new ArrayList<>(choiceGroups).get(0).getChoices().get(0);
        Choice garissaSubCounty = new ArrayList<>(choiceGroups).get(1).getChoices().get(0);
        Assert.assertNull(garissaCounty.getParent());
        Assert.assertThat(garissaSubCounty.getParent().getName(), is("Garissa"));
    }

}