package ke.co.hoji.server.controller.helper;

import org.junit.Assert;
import org.junit.Test;
import org.supercsv.prefs.CsvPreference;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;

/**
 * Created by geoffreywasilwa on 10/07/2017.
 */
public class DataWriterTests {

    @Test
    public void shouldWriteToFileGivenData() throws Exception {
        List<String> headers = Arrays.asList("field1", "field2");
        List<String> data = Arrays.asList("val1", "val2");
        List<List<String>> document = Arrays.asList(headers, data);
        CsvPreference preference = CsvPreference.STANDARD_PREFERENCE;
        String expectedOutput = "field1,field2" + preference.getEndOfLineSymbols() +
                "val1,val2" + preference.getEndOfLineSymbols();
        File temp = File.createTempFile("test", ".csv");
        DataWriter.write(document, preference, temp);
        Assert.assertThat(new String(Files.readAllBytes(temp.toPath())), is(expectedOutput));
        temp.delete();
    }

    @Test
    public void shouldWriteToFileGivenDataWithDuplicateHeaders() throws Exception {
        List<String> headers = Arrays.asList("field1", "field1");
        List<String> data = Arrays.asList("val1", "val2");
        List<List<String>> document = Arrays.asList(headers, data);
        CsvPreference preference = CsvPreference.STANDARD_PREFERENCE;
        String expectedOutput = "field1,field1" + preference.getEndOfLineSymbols() +
                "val1,val2" + preference.getEndOfLineSymbols();
        File temp = File.createTempFile("test", ".csv");
        DataWriter.write(document, preference, temp);
        Assert.assertThat(new String(Files.readAllBytes(temp.toPath())), is(expectedOutput));
        temp.delete();
    }

}