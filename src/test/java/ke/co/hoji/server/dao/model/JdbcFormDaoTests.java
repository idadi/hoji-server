package ke.co.hoji.server.dao.model;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.bean.DatabaseConfigBean;
import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import net.jcip.annotations.NotThreadSafe;
import org.dbunit.database.DatabaseDataSourceConnection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = JdbcFormDaoTests.TestConfiguration.class)
@TestExecutionListeners(listeners = {
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@DbUnitConfiguration(databaseConnection = "dbUnitDatabaseConnection")
@DatabaseSetup("linked_form_dataset.xml")
@NotThreadSafe
public class JdbcFormDaoTests {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void select_shouldGetAllFormsIncludingDependentForms() {
        JdbcFormDao formDao = new JdbcFormDao(jdbcTemplate, null);

        List<Form> forms = formDao.getFormsBySurvey(new Survey(1, "", true, ""));

        assertThat(forms, hasSize(4));
        assertThat(forms.get(0).getDependentForms(), hasSize(3));
        assertThat(forms.get(1).getDependentForms(), hasSize(0));
        assertThat(forms.get(2).getDependentForms(), hasSize(0));
        assertThat(forms.get(3).getDependentForms(), hasSize(0));
    }

    @Test
    public void select_shouldGetOneFormIncludingDependentForms() {
        JdbcFormDao formDao = new JdbcFormDao(jdbcTemplate, null);

        Form form = formDao.getFormById( 1);

        assertThat(form.getDependentForms(), hasSize(3));
    }

    @Configuration
    @PropertySource("classpath:hoji.properties")
    static class TestConfiguration {

        @Bean
        public DataSource dataSource() {
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            return builder
                    .setType(EmbeddedDatabaseType.H2)
                    .setName("test_form")
                    .addScripts("create-db.sql")
                    .build();
        }

        @Bean
        public DatabaseConfigBean databaseConfig() {
            return new DatabaseConfigBean();
        }

        @Bean
        public DatabaseDataSourceConnection dbUnitDatabaseConnection() throws Exception {
            DatabaseDataSourceConnectionFactoryBean dataSourceConnectionFactoryBean =
                    new DatabaseDataSourceConnectionFactoryBean();
            dataSourceConnectionFactoryBean.setDataSource(dataSource());
            dataSourceConnectionFactoryBean.setDatabaseConfig(databaseConfig());
            return dataSourceConnectionFactoryBean.getObject();
        }

        @Bean
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        public JdbcTemplate jdbcTemplate() {
            return Mockito.spy(new JdbcTemplate(dataSource()));
        }

        @Bean
        public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
            return new PropertySourcesPlaceholderConfigurer();
        }
    }

}