package ke.co.hoji.server.dao.model;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.bean.DatabaseConfigBean;
import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import net.jcip.annotations.NotThreadSafe;
import org.dbunit.database.DatabaseDataSourceConnection;
import org.h2.tools.Server;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.sql.DataSource;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = JdbcChoiceGroupDaoTests.TestConfiguration.class)
@TestExecutionListeners(listeners = {
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@DbUnitConfiguration(databaseConnection = "dbUnitDatabaseConnection")
@DatabaseSetup("choice_dataset.xml")
@NotThreadSafe
public class JdbcChoiceGroupDaoTests {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    JdbcChoiceGroupDao choiceGroupDao;

    @Before
    public void setup() {
        JdbcChoiceDao choiceDao = new JdbcChoiceDao(jdbcTemplate);
        choiceGroupDao = new JdbcChoiceGroupDao(jdbcTemplate, choiceDao);
    }

    @Test
    public void select_shouldEagerFetchChoices() {
        ChoiceGroup choiceGroup = choiceGroupDao.getChoiceGroupById(1);

        assertThat(choiceGroup.getChoices(), hasSize(2));
    }

    @Test
    public void select_shouldEagerFetchChoicesAndParents() {
        ChoiceGroup choiceGroup = choiceGroupDao.getChoiceGroupById(3);

        assertThat(choiceGroup.getChoices(), hasSize(1));
        assertThat(choiceGroup.getChoices(), hasItem(hasProperty("name", equalTo("Kenya"))));
        assertThat(choiceGroup.getChoices(), hasItem(hasProperty("parent", hasProperty("name", equalTo("E. Africa")))));
    }

    @Test
    public void saveChoiceGroup_shouldMaintainItsListOfChoices() {
        ChoiceGroup toUpdate = new ChoiceGroup(1);
        toUpdate.setName("Yes/No");
        toUpdate.setUserId(2);
        Choice yes = new Choice(2, "Yes", "Y", 0, false);
        yes.setUserId(2);
        toUpdate.setChoices(Collections.singletonList(yes));

        ChoiceGroup beforeUpdate = choiceGroupDao.getChoiceGroupById(1);

        assertThat(beforeUpdate.getChoices(), hasSize(2));

        ChoiceGroup afterUpdate = choiceGroupDao.saveChoiceGroup(toUpdate);

        assertThat(afterUpdate.getChoices(), hasSize(2));
    }

    @Test
    public void getChoiceGroupById_shouldPreferChoiceCodeWhenSpecified() {
        ChoiceGroup models = choiceGroupDao.getChoiceGroupById(4);

        assertThat(models.getChoices(), hasSize(1));
        assertThat(models.getChoices(), hasItem(hasProperty("code", equalTo("B4s"))));
    }

    @Test
    public void getChoiceGroupById_shouldDefaultChoiceOrdinalWhenCodeNotSpecified() {
        ChoiceGroup models = choiceGroupDao.getChoiceGroupById(3);

        assertThat(models.getChoices(), hasSize(1));
        assertThat(models.getChoices(), hasItem(hasProperty("code", equalTo("1"))));
    }

    @Test
    public void saveChoiceGroup_shouldAddChoicesToGroup() {
        Choice bmw3s = new Choice(null, "BMW 3 Series", "B3s", 0, false);
        bmw3s.setUserId(2);
        ChoiceGroup models = new ChoiceGroup(4);
        models.setName("Models");
        models.setUserId(2);
        models.setChoices(Collections.singletonList(bmw3s));

        choiceGroupDao.saveChoiceGroup(models);

        ChoiceGroup fromDb = choiceGroupDao.getChoiceGroupById(4);

        assertThat(fromDb.getChoices(), hasSize(2));
        assertThat(fromDb.getChoices().get(0).getCode(), equalTo("B4s"));
        assertThat(fromDb.getChoices().get(0).getOrdinal().intValue(), equalTo(1));
        assertThat(fromDb.getChoices().get(1).getCode(), equalTo("B3s"));
        assertThat(fromDb.getChoices().get(1).getOrdinal().intValue(), equalTo(2));
    }

    @Test
    public void saveChoiceGroup_shouldSaveChoiceGroupWithAChoiceHavingAParent() {
        Choice busia = new Choice(null, "Busia county", "1", 0, false);
        busia.setUserId(2);
        Choice tesoFacility = new Choice(null, "Teso facility", "1", 0, false);
        tesoFacility.setUserId(2);
        tesoFacility.setParent(busia);
        ChoiceGroup county = choiceGroupDao.getChoiceGroupById(6);
        county.setChoices(Collections.singletonList(busia));
        ChoiceGroup subCounty = choiceGroupDao.getChoiceGroupById(7);
        subCounty.setChoices(Collections.singletonList(tesoFacility));

        choiceGroupDao.saveChoiceGroups(Arrays.asList(county, subCounty));

        ChoiceGroup subCountyFromDb = choiceGroupDao.getChoiceGroupById(7);

        assertThat(subCountyFromDb.getChoices(), hasSize(1));
        assertThat(subCountyFromDb.getChoices(),
                hasItem(
                        hasProperty("parent",
                                hasProperty("name", equalTo("Busia county")))));
    }

    @Test
    public void getChoiceGroupById_shouldFetchChoiceGroupThatDoesNotHaveChoices() {
        ChoiceGroup continents = choiceGroupDao.getChoiceGroupById(5);

        assertThat(continents, hasProperty("name", equalTo("Continents")));
        assertThat(continents.getChoices(), hasSize(0));
    }

    @Configuration
    @PropertySource("classpath:hoji.properties")
    static class TestConfiguration {

        @Bean
        public DataSource dataSource() {
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            return builder
                    .setType(EmbeddedDatabaseType.H2)
                    .setName("test_cg")
                    .addScripts("create-db.sql")
                    .build();
        }

        @Bean
        public DatabaseConfigBean databaseConfig() {
            return new DatabaseConfigBean();
        }

        @Bean
        public DatabaseDataSourceConnection dbUnitDatabaseConnection() throws Exception {
            DatabaseDataSourceConnectionFactoryBean dataSourceConnectionFactoryBean =
                    new DatabaseDataSourceConnectionFactoryBean();
            dataSourceConnectionFactoryBean.setDataSource(dataSource());
            dataSourceConnectionFactoryBean.setDatabaseConfig(databaseConfig());
            return dataSourceConnectionFactoryBean.getObject();
        }

        @Bean
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        public JdbcTemplate jdbcTemplate() {
            return Mockito.spy(new JdbcTemplate(dataSource()));
        }

        @Bean
        public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
            return new PropertySourcesPlaceholderConfigurer();
        }

        Server webServer;

        /* @PostConstruct
        public void startDBManager() throws SQLException {
            webServer = Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8082").start();
        }

        @PreDestroy
        public void stopServers() {
            if (webServer != null) {
                webServer.stop();
            }
        } */
    }

}