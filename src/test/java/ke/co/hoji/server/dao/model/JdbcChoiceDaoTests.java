package ke.co.hoji.server.dao.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.sql.DataSource;

import org.dbunit.database.DatabaseDataSourceConnection;
import org.h2.tools.Server;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.bean.DatabaseConfigBean;
import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.server.exception.DuplicateChoiceException;
import ke.co.hoji.server.exception.MissingChoiceParentException;
import net.jcip.annotations.NotThreadSafe;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = JdbcChoiceDaoTests.TestConfiguration.class)
@TestExecutionListeners(listeners = { DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DbUnitConfiguration(databaseConnection = "dbUnitDatabaseConnection")
@DatabaseSetup("choice_dataset.xml")
@NotThreadSafe
public class JdbcChoiceDaoTests {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private JdbcChoiceDao choiceDao;

    @Before
    public void setup() {
        choiceDao = new JdbcChoiceDao(jdbcTemplate);
    }

    @Test
    public void getById_shouldReturnChoice() {
        Choice choice = choiceDao.getById(2);

        assertThat(choice, hasProperty("name", equalTo("Yes")));
        assertThat(choice, hasProperty("scale", equalTo(0)));
        assertThat(choice, hasProperty("loner", equalTo(false)));
    }

    @Test
    public void getByGroup_shouldEagerFetchParentChoices() {
        Page<Choice> choices = choiceDao.getByGroup(3, PageRequest.of(0, 5));

        assertThat(choices.getContent(), hasSize(1));
        assertThat(choices.getContent().get(0), hasProperty("name", equalTo("Kenya")));
        assertThat(choices.getContent().get(0), hasProperty("parent", hasProperty("name", equalTo("E. Africa"))));
    }

    @Test
    public void getByGroup_shouldSortChoicesByOrdinal() {
        Page<Choice> choices = choiceDao.getByGroup(6, PageRequest.of(0, 5, Sort.by(Direction.ASC, "ordinal")));

        assertThat(choices.getContent(), hasSize(3));
        assertThat(choices.getContent().get(0).getName(), is("Nairobi"));
        assertThat(choices.getContent().get(1).getName(), is("Kisumu"));
        assertThat(choices.getContent().get(2).getName(), is("Nakuru"));
    }

    @Test
    public void getByGroup_shouldSortChoicesByName() {
        Page<Choice> choices = choiceDao.getByGroup(6, PageRequest.of(0, 5, Sort.by(Direction.ASC, "name")));

        assertThat(choices.getContent(), hasSize(3));
        assertThat(choices.getContent().get(0).getName(), is("Kisumu"));
        assertThat(choices.getContent().get(1).getName(), is("Nairobi"));
        assertThat(choices.getContent().get(2).getName(), is("Nakuru"));
    }

    @Test
    public void searchInGroupByName_shouldOnlyMatchBeginingCharacters() {
        Page<Choice> choices = choiceDao.searchInGroupByName(6, "k", PageRequest.of(0, 5, Sort.by(Direction.ASC, "name")));

        assertThat(choices.getContent(), hasSize(1));
        assertThat(choices.getContent().get(0).getName(), is("Kisumu"));
        //Nakuru should not be returned since it does not begin with a 'k' but contains the letter 'k'.
    }

    @Test
    public void searchByName_shouldOnlyMatchBeginingCharacters() {
        Page<Choice> choices = choiceDao.searchByName(2, "k", PageRequest.of(0, 5, Sort.by(Direction.ASC, "name")));

        assertThat(choices.getContent(), hasSize(2));
        assertThat(choices.getContent().get(0).getName(), is("Kenya"));
        assertThat(choices.getContent().get(1).getName(), is("Kisumu"));
        //Nakuru should not be returned since it does not begin with a 'k' but contains the letter 'k'.
    }

    @Test
    public void saveOrUpdate_shouldSaveAndGroupNewChoice() {
        Choice yes = new Choice(null, "Nein", "N", 99, true);
        yes.setUserId(2);

        choiceDao.saveOrUpdate(8, yes);

        Page<Choice> choices = choiceDao.getByGroup(8, PageRequest.of(0, 5));

        assertThat(choices.getContent(), hasSize(1));
        assertThat(choices.getContent().get(0), hasProperty("name", equalTo("Nein")));
        assertThat(choices.getContent().get(0), hasProperty("code", equalTo("N")));
        assertThat(choices.getContent().get(0), hasProperty("scale", equalTo(99)));
        assertThat(choices.getContent().get(0), hasProperty("loner", equalTo(true)));
    }

    @Test
    public void saveOrUpdate_shouldUseOrdinalAsCodeIfNotSpecified() {
        Choice yes = new Choice(null, "Nein", null, 99, true);
        yes.setUserId(2);
;
        choiceDao.saveOrUpdate(8, yes);

        Page<Choice> choices = choiceDao.getByGroup(8, PageRequest.of(0, 5));

        assertThat(choices.getContent(), hasSize(1));
        assertThat(choices.getContent().get(0), hasProperty("name", equalTo("Nein")));
        assertThat(choices.getContent().get(0), hasProperty("code", equalTo("1")));
        assertThat(choices.getContent().get(0), hasProperty("scale", equalTo(99)));
        assertThat(choices.getContent().get(0), hasProperty("loner", equalTo(true)));
    }

    @Test
    public void saveOrUpdate_shouldSaveNewChoiceWithSelfAsParent() {
        Choice garissa = new Choice(null, "Garissa", null, 0, false);
        garissa.setParent(garissa);
        garissa.setUserId(2);

        choiceDao.saveOrUpdate(7, garissa);
        Page<Choice> choices = choiceDao.getByGroup(7, PageRequest.of(0, 5));

        assertThat(choices.getContent(), hasSize(1));
        assertEquals(choices.getContent().get(0), choices.getContent().get(0).getParent());
    }

    @Test
    public void saveOrUpdate_shouldUpdateOrdinalWhenSavingNewChoice() {
        Choice bungoma = new Choice(null, "Bungoma", null, 0, false);
        bungoma.setOrdinal(new BigDecimal(2));
        bungoma.setUserId(2);

        choiceDao.saveOrUpdate(6, bungoma);
        Page<Choice> choices = choiceDao.getByGroup(6, PageRequest.of(0, 5));

        assertThat(choices.getContent(), hasSize(4));
        assertThat(choices.getContent().get(0).getName(), is("Nairobi"));
        assertThat(choices.getContent().get(0).getOrdinal().intValue(), is(1));
        assertThat(choices.getContent().get(1).getName(), is("Bungoma"));
        assertThat(choices.getContent().get(1).getOrdinal().intValue(), is(2));
        assertThat(choices.getContent().get(2).getName(), is("Kisumu"));
        assertThat(choices.getContent().get(2).getOrdinal().intValue(), is(3));
        assertThat(choices.getContent().get(3).getName(), is("Nakuru"));
        assertThat(choices.getContent().get(3).getOrdinal().intValue(), is(4));
    }

    @Test
    public void saveOrUpdate_shouldSaveAndGroupChoices() {
        Choice ja = new Choice(null, "Ja", null, 0, false);
        ja.setUserId(2);
        Choice nein = new Choice(null, "Nein", null, 0, false);
        nein.setUserId(2);

        choiceDao.saveOrUpdate(8, Arrays.asList(ja, nein));
        Page<Choice> choices = choiceDao.getByGroup(8, PageRequest.of(0, 5));

        assertThat(choices.getContent(), hasSize(2));
        assertThat(choices.getContent().get(0).getName(), is("Ja"));
        assertThat(choices.getContent().get(0).getOrdinal().intValue(), is(1));
        assertThat(choices.getContent().get(1).getName(), is("Nein"));
        assertThat(choices.getContent().get(1).getOrdinal().intValue(), is(2));
    }

    @Test
    public void saveOrUpdate_shouldNotCreateNewChoicesWhenOneAlreadyExists() {
        Choice yes = new Choice(null, "yes", "Y", 0, false);
        yes.setOrdinal(new BigDecimal(1));
        yes.setUserId(2);

        choiceDao.saveOrUpdate(1, yes);
        List<Choice> choices = choiceDao.searchByName(2, "yes", null).getContent();

        assertThat(choices, hasSize(1));
    }

    @Test(expected = DuplicateChoiceException.class)
    public void saveOrUpdate_shouldFailWhenUpdateCreatesDuplicateChoices() {
        Choice yes = new Choice(11, "Si", "Y", 0, false);
        yes.setOrdinal(new BigDecimal(1));
        yes.setUserId(2);

        choiceDao.saveOrUpdate(9, yes);
    }

    @Test
    public void saveOrUpdate_shouldUpdateChoiceName() {
        Choice africa = choiceDao.getById(12);

        assertThat(africa.getName(), is("Africa"));

        africa.setName("Afrika");
        choiceDao.saveOrUpdate(13, africa);

        Choice afrika = choiceDao.getById(12);

        assertThat(afrika.getName(), is("Afrika"));
    }

    @Test(expected = MissingChoiceParentException.class)
    public void saveOrUpdate_shouldFailIfParentDoesNotExist() {
        Choice europe = new Choice();
        europe.setName("Europe");
        Choice italy = new Choice();
        italy.setName("Italy");
        italy.setParent(europe);

        choiceDao.saveOrUpdate(3, italy);
    }

    @Test
    @Ignore("h2 does not support stored procedures which is used by the remove method")
    public void removeFromGroup_shouldDeleteGroupedChoice() {
        choiceDao.removeFromGroup(99, 999);

        Page<Choice> choices = choiceDao.getByGroup(99, PageRequest.of(0, 5));

        assertThat(choices.getContent(), hasSize(0));
    }

    @Test
    @Ignore("h2 does not support stored procedures which is used by the remove method")
    public void removeFromGroup_shouldUpdateOrdinalsOfRemainingChoices() {
        Page<Choice> choices = choiceDao.getByGroup(14, PageRequest.of(0, 5));
        assertThat(choices.getContent(), hasSize(5));
        
        choiceDao.removeFromGroup(14, 18);

        choices = choiceDao.getByGroup(14, PageRequest.of(0, 5));
        assertThat(choices.getContent(), hasSize(4));
        assertThat(choices.getContent().get(0).getOrdinal().intValue(), is(1));
        assertThat(choices.getContent().get(1).getOrdinal().intValue(), is(2));
        assertThat(choices.getContent().get(2).getOrdinal().intValue(), is(3));
        assertThat(choices.getContent().get(3).getOrdinal().intValue(), is(4));
        
        choiceDao.removeFromGroup(14, 16);

        choices = choiceDao.getByGroup(14, PageRequest.of(0, 5));
        assertThat(choices.getContent(), hasSize(3));
        assertThat(choices.getContent().get(0).getOrdinal().intValue(), is(1));
        assertThat(choices.getContent().get(1).getOrdinal().intValue(), is(2));
        assertThat(choices.getContent().get(2).getOrdinal().intValue(), is(3));
        
        choiceDao.removeFromGroup(14, 21);
        
        choices = choiceDao.getByGroup(14, PageRequest.of(0, 5));
        assertThat(choices.getContent(), hasSize(2));
        assertThat(choices.getContent().get(0).getOrdinal().intValue(), is(1));
        assertThat(choices.getContent().get(1).getOrdinal().intValue(), is(2));
    }

    @Configuration
    @PropertySource("classpath:hoji.properties")
    static class TestConfiguration {

        @Bean
        public DataSource dataSource() {
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            return builder.setType(EmbeddedDatabaseType.H2).setName("test_choice")
                .addScripts("create-db.sql").build();
        }

        @Bean
        public DatabaseConfigBean databaseConfig() {
            return new DatabaseConfigBean();
        }

        @Bean
        public DatabaseDataSourceConnection dbUnitDatabaseConnection() throws Exception {
            DatabaseDataSourceConnectionFactoryBean dataSourceConnectionFactoryBean = new DatabaseDataSourceConnectionFactoryBean();
            dataSourceConnectionFactoryBean.setDataSource(dataSource());
            dataSourceConnectionFactoryBean.setDatabaseConfig(databaseConfig());
            return dataSourceConnectionFactoryBean.getObject();
        }

        @Bean
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        public JdbcTemplate jdbcTemplate() {
            return Mockito.spy(new JdbcTemplate(dataSource()));
        }

        @Bean
        public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
            return new PropertySourcesPlaceholderConfigurer();
        }

        /*Server webServer;

        @PostConstruct
        public void startDBManager() throws SQLException {
            webServer = Server.createWebServer("-web", "-webAllowOthers", "-webPort", "8082").start();
        }

        @PreDestroy
        public void stopServers() {
            if (webServer != null) {
                webServer.stop();
            }
        }*/
    }

}