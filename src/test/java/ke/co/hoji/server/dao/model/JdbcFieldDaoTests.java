package ke.co.hoji.server.dao.model;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.bean.DatabaseConfigBean;
import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;
import ke.co.hoji.core.data.model.Field;
import org.dbunit.database.DatabaseDataSourceConnection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = JdbcFieldDaoTests.TestConfiguration.class)
@TestExecutionListeners(listeners = {
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@DbUnitConfiguration(databaseConnection = "dbUnitDatabaseConnection")
@DatabaseSetup("linked_form_dataset.xml")
public class JdbcFieldDaoTests {

    @Autowired
    private JdbcFieldDao fieldDao;

    @Test
    public void select_shouldGetAllFields() {

        List<Field> fields = fieldDao.getFields(2);

        assertThat(fields, hasSize(2));
    }


    @Configuration
    @ComponentScan(basePackages = "ke.co.hoji.server.dao")
    @PropertySource("classpath:hoji.properties")
    static class TestConfiguration {

        @Bean
        public DataSource dataSource() {
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            return builder
                    .setType(EmbeddedDatabaseType.H2)
                    .setName("test_field")
                    .addScripts("create-db.sql")
                    .build();
        }

        @Bean
        public DatabaseConfigBean databaseConfig() {
            return new DatabaseConfigBean();
        }

        @Bean
        public DatabaseDataSourceConnection dbUnitDatabaseConnection() throws Exception {
            DatabaseDataSourceConnectionFactoryBean dataSourceConnectionFactoryBean =
                    new DatabaseDataSourceConnectionFactoryBean();
            dataSourceConnectionFactoryBean.setDataSource(dataSource());
            dataSourceConnectionFactoryBean.setDatabaseConfig(databaseConfig());
            return dataSourceConnectionFactoryBean.getObject();
        }

        @Bean
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        public JdbcTemplate jdbcTemplate() {
            return Mockito.spy(new JdbcTemplate(dataSource()));
        }

        @Bean
        public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
            return new PropertySourcesPlaceholderConfigurer();
        }
    }
}
