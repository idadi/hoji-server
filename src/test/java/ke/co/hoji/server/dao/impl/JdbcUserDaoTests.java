package ke.co.hoji.server.dao.impl;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.bean.DatabaseConfigBean;
import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;
import ke.co.hoji.server.model.User;
import net.jcip.annotations.NotThreadSafe;
import org.dbunit.database.DatabaseDataSourceConnection;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = JdbcUserDaoTests.TestConfiguration.class)
@TestExecutionListeners(listeners = {
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@DbUnitConfiguration(databaseConnection = "dbUnitDatabaseConnection")
@DatabaseSetup("user_dataset.xml")
@NotThreadSafe
public class JdbcUserDaoTests {

    @Autowired
    private JdbcTemplate template;

    private JdbcUserDao userDao;

    @Before
    public void setup() {
        userDao = new JdbcUserDao(template);
    }

    @Test
    public void select_shouldFetchUsersOrderedAlphabeticallyByFirstName() {
        List<User> users = userDao.findAllUsers(null);

        assertThat(users.get(0), hasProperty("firstName", equalTo("Bee")));
        assertThat(users.get(1), hasProperty("firstName", equalTo("Deedee")));
        assertThat(users.get(2), hasProperty("firstName", equalTo("Mimi")));
        assertThat(users.get(3), hasProperty("firstName", equalTo("Xoxo")));
    }

    @Test
    public void select_shouldReturnEmptyCollectionNoUserMatchesCriteria() {
        List<User> users = userDao.findByTenantId(99);

        assertThat(users, hasSize(0));
    }

    @Test
    public void findById_shouldFindUserById() {
        User found = userDao.findById(1);

        assertThat(found.getFirstName(), is("Xoxo"));
    }

    @Test
    public void findById_shouldReturnNullIfUserNotFound() {
        User found = userDao.findById(Integer.MAX_VALUE);

        assertNull(found);
    }

    @Test
    public void findByIds_shouldFindUserByIds() {
        List<User> users = userDao.findByIds(Arrays.asList(1, 2));

        assertThat(users, hasSize(2));
        assertThat(users.get(0), hasProperty("firstName", equalTo("Xoxo")));
        assertThat(users.get(1), hasProperty("firstName", equalTo("Deedee")));
    }

    @Test
    public void findByIds_shouldReturnEmptyCollectionIfUsersNotFound() {
        List<User> users = userDao.findByIds(Collections.emptyList());

        assertThat(users, hasSize(0));
    }

    @Test
    public void findByUsername_shouldFindUserByUserName() {
        User found = userDao.findByUsername("xoxo@me.co.ke");

        assertThat(found.getFirstName(), is("Xoxo"));
    }

    @Test
    public void findByUsername_shouldReturnNullIfUserNotFound() {
        User found = userDao.findByUsername("");

        assertNull(found);
    }

    @Test
    public void findByTenantId_shouldFindUserByTenantId() {
        List<User> users = userDao.findByTenantId(1);

        assertThat(users, hasSize(1));
        assertThat(users.get(0), hasProperty("firstName", equalTo("Xoxo")));
    }

    @Test
    public void findByTenantId_shouldReturnEmptyListIfNoUserExistUnderTenantId() {
        List<User> users = userDao.findByTenantId(Integer.MAX_VALUE);

        assertThat(users, hasSize(0));
    }

    @Test
    public void findAllUsers_shouldReturnAllUsers() {
        List<User> users = userDao.findAllUsers(null);

        assertThat(users, hasSize(4));
        assertThat(users.get(0), hasProperty("firstName", equalTo("Bee")));
        assertThat(users.get(1), hasProperty("firstName", equalTo("Deedee")));
        assertThat(users.get(2), hasProperty("firstName", equalTo("Mimi")));
        assertThat(users.get(3), hasProperty("firstName", equalTo("Xoxo")));
    }

    @Test
    public void findAllUsers_shouldReturnEnabledUsers() {
        List<User> enabled = userDao.findAllUsers(true);

        assertThat(enabled, hasSize(3));
        assertThat(enabled.get(0), hasProperty("firstName", equalTo("Bee")));
        assertThat(enabled.get(1), hasProperty("firstName", equalTo("Mimi")));
        assertThat(enabled.get(2), hasProperty("firstName", equalTo("Xoxo")));
    }

    @Test
    public void findAllUsers_shouldReturnDisabledUsers() {
        List<User> disabled = userDao.findAllUsers(false);

        assertThat(disabled, hasSize(1));
        assertThat(disabled.get(0), hasProperty("firstName", equalTo("Deedee")));
    }

    @Test
    public void modifyUser_shouldUpdateUserDetails() {
        User xoxo = userDao.findById(1);
        xoxo.setLastName("Chu");

        userDao.modifyUser(xoxo);

        xoxo = userDao.findById(1);

        Assert.assertThat(xoxo.getLastName(), is("Chu"));
    }

    @Test
    public void changeType_shouldChangeUserType() {
        User xoxo = userDao.findById(1);

        assertThat(xoxo.getType(), is(User.Type.PREPAID));

        xoxo.setType(User.Type.POSTPAID);

        userDao.changeType(xoxo);

        xoxo = userDao.findById(1);

        Assert.assertThat(xoxo.getType(), is(User.Type.POSTPAID));
    }

    @Test
    public void verifyUser_shouldUpdateUserStatusAsVerified() {
        User bee = userDao.findById(4);

        assertFalse(bee.isVerified());

        bee.setVerified(true);

        userDao.verifyUser(bee);

        bee = userDao.findById(4);

        Assert.assertTrue(bee.isVerified());
    }

    @Test
    public void changeEmail_shouldUpdateUserEmail() {
        User bee = userDao.findById(4);

        assertThat(bee.getEmail(), is("bee@me.co.ke"));

        bee.setUsername("iam@bee.co.ke");

        userDao.changeEmail(bee);

        bee = userDao.findById(4);

        Assert.assertThat(bee.getEmail(), is("iam@bee.co.ke"));
    }

    @Configuration
    public static class TestConfiguration {

        @Bean
        public DataSource dataSource() {
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            return builder
                    .setType(EmbeddedDatabaseType.H2)
                    .setName("testdb")
                    .addScript("create-user.sql")
                    .build();
        }

        @Bean
        public DatabaseConfigBean databaseConfig() {
            DatabaseConfigBean configBean = new DatabaseConfigBean();
            return configBean;
        }

        @Bean
        public DatabaseDataSourceConnection dbUnitDatabaseConnection() throws Exception {
            DatabaseDataSourceConnectionFactoryBean dataSourceConnectionFactoryBean =
                    new DatabaseDataSourceConnectionFactoryBean();
            dataSourceConnectionFactoryBean.setDataSource(dataSource());
            dataSourceConnectionFactoryBean.setDatabaseConfig(databaseConfig());
            return dataSourceConnectionFactoryBean.getObject();
        }

        @Bean
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        public JdbcTemplate template() {
            return new JdbcTemplate(dataSource());
        }

    }

}