package ke.co.hoji.server.dao.model;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.bean.DatabaseConfigBean;
import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Survey;
import org.dbunit.database.DatabaseDataSourceConnection;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JdbcPropertyDaoTests.TestConfiguration.class})
@TestExecutionListeners(listeners = {
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
@DbUnitConfiguration(databaseConnection = "dbUnitDatabaseConnection")
public class JdbcPropertyDaoTests {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private JdbcPropertyDao propertyDao;

    @Before
    public void setup() {
        propertyDao = new JdbcPropertyDao(jdbcTemplate);
    }

    @Test
    public void getProperty_shouldRetrievePropertyFromDb() {
        Integer surveyId = 3;
        Property property = propertyDao.getProperty("field.256", surveyId);

        assertThat(property, allOf(
                hasProperty("key", equalTo("field.256")),
                hasProperty("value", equalTo("250")),
                hasProperty("survey", hasProperty("id", equalTo(surveyId)))
        ));
    }

    @Test
    public void getProperties_shouldRetrievePropertiesBySurveyId() {
        Integer surveyId = 4;
        List<Property> properties = propertyDao.getProperties(surveyId);

        assertThat(properties, hasSize(2));
        assertThat(properties, hasItems(
                allOf(
                        hasProperty("key", equalTo("field.256")),
                        hasProperty("value", equalTo("250")),
                        hasProperty("survey", hasProperty("id", equalTo(surveyId)))
                ),
                allOf(
                        hasProperty("key", equalTo("field.257")),
                        hasProperty("value", equalTo("300")),
                        hasProperty("survey", hasProperty("id", equalTo(surveyId)))
                )
        ));
    }

    @Test
    public void saveProperty_shouldCreateNewProperty() {
        Integer surveyId = 1;
        Property property = new Property("field.128", "Answer", new Survey(1, "Sample", true, "SMPL"));

        propertyDao.saveProperty(property);

        Property fromDb = propertyDao.getProperty("field.128", surveyId);
        assertThat(fromDb, allOf(
                hasProperty("key", equalTo("field.128")),
                hasProperty("value", equalTo("Answer")),
                hasProperty("survey", hasProperty("id", equalTo(surveyId)))
        ));
    }

    @Test
    public void saveProperty_shouldUpdateProperty() {
        Integer surveyId = 5;
        Survey survey = new Survey();
        survey.setId(surveyId);
        Property property = new Property("field.256", "1000", survey);

        propertyDao.saveProperty(property);

        Property fromDb = propertyDao.getProperty("field.256", surveyId);
        assertThat(fromDb, allOf(
                hasProperty("key", equalTo("field.256")),
                hasProperty("value", equalTo("1000")),
                hasProperty("survey", hasProperty("id", equalTo(surveyId)))
        ));
    }

    @Test
    public void saveProperties_shouldCreateNewProperties() {
        Integer surveyId = 2;
        Survey survey = new Survey();
        survey.setId(surveyId);
        Property prop1 = new Property("field.128", "512", survey);
        Property prop2 = new Property("field.129", "256", survey);

        propertyDao.saveProperties(Arrays.asList(prop1, prop2));

        List<Property> sampleProps = propertyDao.getProperties(surveyId);
        Assert.assertThat(sampleProps, hasSize(2));
        assertThat(sampleProps, hasItems(
                allOf(
                        hasProperty("key", equalTo("field.128")),
                        hasProperty("value", equalTo("512")),
                        hasProperty("survey", hasProperty("id", equalTo(surveyId)))
                ),
                allOf(
                        hasProperty("key", equalTo("field.129")),
                        hasProperty("value", equalTo("256")),
                        hasProperty("survey", hasProperty("id", equalTo(surveyId)))
                )
        ));
    }

    @Test
    public void saveProperties_shouldUpdateProperties() {
        Integer surveyId = 6;
        Survey survey = new Survey();
        survey.setId(surveyId);
        Property prop = new Property("field.256", "512", survey);

        propertyDao.saveProperties(Collections.singletonList(prop));

        List<Property> sampleProps = propertyDao.getProperties(surveyId);
        Assert.assertThat(sampleProps, hasSize(1));
        assertThat(sampleProps, hasItems(allOf(
                hasProperty("key", equalTo("field.256")),
                hasProperty("value", equalTo("512")),
                hasProperty("survey", hasProperty("id", equalTo(surveyId)))
        )));
    }

    @Test
    public void deleteProperty_shouldDeleteProperty() {
        Integer surveyId = 7;
        assertTrue(propertyDao.deleteProperty("field.256", surveyId));
        Property fromDb = propertyDao.getProperty("field.256", surveyId);
        assertNull(fromDb);
    }

    @Test
    public void deleteProperty_shouldReturnFalseWhenDeleteFailed() {
        assertFalse(propertyDao.deleteProperty("missing.key", null));
    }

    @Configuration
    @PropertySource("classpath:hoji.properties")
    static class TestConfiguration {

        @Bean
        public DataSource dataSource() {
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            return builder
                    .setType(EmbeddedDatabaseType.H2)
                    .setName("test_prop")
                    .addScripts("create-db.sql", "insert-properties.sql")
                    .build();
        }

        @Bean
        public DatabaseConfigBean databaseConfig() {
            return new DatabaseConfigBean();
        }

        @Bean
        public DatabaseDataSourceConnection dbUnitDatabaseConnection() throws Exception {
            DatabaseDataSourceConnectionFactoryBean dataSourceConnectionFactoryBean =
                    new DatabaseDataSourceConnectionFactoryBean();
            dataSourceConnectionFactoryBean.setDataSource(dataSource());
            dataSourceConnectionFactoryBean.setDatabaseConfig(databaseConfig());
            return dataSourceConnectionFactoryBean.getObject();
        }

        @Bean
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        public JdbcTemplate jdbcTemplate() {
            return Mockito.spy(new JdbcTemplate(dataSource()));
        }

        @Bean
        public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
            return new PropertySourcesPlaceholderConfigurer();
        }
    }

}