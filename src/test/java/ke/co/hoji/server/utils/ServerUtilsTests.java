package ke.co.hoji.server.utils;

import ke.co.hoji.core.data.Choice;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

public class ServerUtilsTests {

    @Test
    public void shouldFindOneItemInCollection() throws Exception {
        Choice yes = new Choice(1, "Yes", "Y", 0, true);
        Choice no = new Choice(2, "No", "N", 0, true);

        Choice match = ServerUtils
                .findOne("Y", Arrays.<Choice>asList(yes, no),
                        new ServerUtils.ChoiceByCodeSearchPredicate());

        Assert.assertThat(yes, is(match));
    }

    @Test
    public void shouldReturnFirstMatchInCollection() throws Exception {
        Choice yes1 = new Choice(1, "Yes", "Y", 0, true);
        Choice yes2 = new Choice(2, "Yes", "Y", 0, true);

        Choice match = ServerUtils
                .findOne("Y", Arrays.<Choice>asList(yes1, yes2),
                        new ServerUtils.ChoiceByCodeSearchPredicate());

        Assert.assertThat(yes1, is(match));
    }

    @Test
    public void shouldReturnAllItemsInCollectionThatMatchCriteria() throws Exception {
        Choice yes1 = new Choice(1, "Yes", "Y", 0, true);
        Choice yes2 = new Choice(2, "Yes", "Y", 0, true);

        List<Choice> matches = ServerUtils
                .findMultiple("Y", Arrays.<Choice>asList(yes1, yes2),
                        new ServerUtils.ChoiceByCodeSearchPredicate());

        Assert.assertThat(matches, hasSize(2));
    }

}