package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by geoffreywasilwa on 28/02/2017.
 */
public class ChildrenSortValidatorTests {

    @Test
    public void validationShouldPassForFormFieldsWithoutChildren() {
        Field question1 = new Field(1);
        question1.setOrdinal(new BigDecimal(1));
        Field question2 = new Field(2);
        question2.setOrdinal(new BigDecimal(2));
        Field question3 = new Field(3);
        question3.setOrdinal(new BigDecimal(3));
        List<Field> fields = new ArrayList<>();
        fields.add(question1);
        fields.add(question2);
        fields.add(question3);
        Form form = new Form();
        form.setFields(fields);

        BindingResult result = new BindException(form, "form");
        new ChildrenSortValidator().validate(form, result);

        Assert.assertFalse(result.hasErrors());
    }

    @Test
    public void validationShouldFailWhenFieldIsBetweenParentAndChild() {
        Field question1 = new Field(1);
        question1.setType(new FieldType(2, "Matrix", FieldType.Code.MATRIX));
        question1.setOrdinal(new BigDecimal(1));
        Field child1 = new Field(2);
        child1.setParent(question1);
        child1.setOrdinal(new BigDecimal(3));
        List<Field> children = new ArrayList<>();
        children.add(child1);
        question1.setChildren(children);
        Field question2 = new Field(3);
        question2.setOrdinal(new BigDecimal(2));
        List<Field> fields = new ArrayList<>();
        fields.add(question1);
        fields.add(child1);
        fields.add(question2);
        Form form = new Form();
        form.setFields(fields);

        BindingResult result = new BindException(form, "form");
        new ChildrenSortValidator().validate(form, result);

        Assert.assertTrue(result.hasErrors());
    }

    @Test
    public void validationShouldFailWhenChildFieldIsBeforeParent() {
        Field question1 = new Field(1);
        question1.setType(new FieldType(2, "Matrix", FieldType.Code.MATRIX));
        question1.setOrdinal(new BigDecimal(2));
        Field child1 = new Field(2);
        child1.setParent(question1);
        child1.setOrdinal(new BigDecimal(1));
        List<Field> children = new ArrayList<>();
        children.add(child1);
        question1.setChildren(children);
        Field question2 = new Field(3);
        question2.setOrdinal(new BigDecimal(3));
        List<Field> fields = new ArrayList<>();
        fields.add(child1);
        fields.add(question1);
        fields.add(question2);
        Form form = new Form();
        form.setFields(fields);

        BindingResult result = new BindException(form, "form");
        new ChildrenSortValidator().validate(form, result);

        Assert.assertTrue(result.hasErrors());
    }

}