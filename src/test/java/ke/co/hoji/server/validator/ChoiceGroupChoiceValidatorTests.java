package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroupChoice;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

/**
 * Created by geoffreywasilwa on 10/06/2017.
 */
public class ChoiceGroupChoiceValidatorTests {

    @Test
    public void shouldFailIfChoiceGroupChoiceHasNoChoice() throws Exception {
        ChoiceGroupChoice choiceGroupChoice = new ChoiceGroupChoice();
        BindingResult result = new BindException(choiceGroupChoice, "choiceGroupChoice");

        new ChoiceGroupChoiceValidator().validate(choiceGroupChoice, result);

        Assert.assertTrue(result.hasErrors());
    }

    @Test
    public void shouldPassIfChoiceGroupChoiceHasChoice() throws Exception {
        ChoiceGroupChoice choiceGroupChoice = new ChoiceGroupChoice();
        choiceGroupChoice.setChoice(new Choice());
        BindingResult result = new BindException(choiceGroupChoice, "choiceGroupChoice");

        new ChoiceGroupChoiceValidator().validate(choiceGroupChoice, result);

        Assert.assertFalse(result.hasErrors());
    }

}