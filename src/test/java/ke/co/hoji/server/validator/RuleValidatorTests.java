package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.OperatorDescriptor;
import ke.co.hoji.core.data.model.Rule;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import static org.hamcrest.Matchers.is;

/**
 * Created by geoffreywasilwa on 11/06/2017.
 */
public class RuleValidatorTests {

    @Test
    public void shouldFailIfRuleHasNoTarget() throws Exception {
        Rule rule = new Rule();
        BindingResult result = new BindException(rule, "rule");

        new RuleValidator().validate(rule, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("target").getDefaultMessage(),
                is("This field is required.")
        );
    }

    @Test
    public void shouldFailIfRuleHasNoOperatorDescriptor() throws Exception {
        Rule rule = new Rule();
        rule.setTarget(new Field());
        BindingResult result = new BindException(rule, "rule");

        new RuleValidator().validate(rule, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("operatorDescriptor").getDefaultMessage(),
                is("Specify the operation.")
        );
    }

    @Test
    public void shouldPassIfRuleConfiguredCorrectly() throws Exception {
        Rule rule = new Rule();
        rule.setTarget(new Field());
        rule.setOperatorDescriptor(new OperatorDescriptor());
        BindingResult result = new BindException(rule, "rule");

        new RuleValidator().validate(rule, result);

        Assert.assertFalse(result.hasErrors());
    }

}