package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.Matchers.is;

/**
 * Created by geoffreywasilwa on 11/06/2017.
 */
public class SurveyPublicationValidatorTests {

    private static final boolean DISABLED = false;
    private static final boolean ENABLED = true;

    @Test
    public void shouldFailIfSurveyHasNoForms() throws Exception {
        Survey survey = new Survey();
        BindingResult result = new BindException(survey, "survey");

        new SurveyPublicationValidator(survey).validate(survey, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("id").getDefaultMessage(),
                is("This project has no forms")
        );
    }

    @Test
    public void shouldFailIfAllFormsIsSurveyAreDisabled() throws Exception {
        Form form = new Form();
        form.setEnabled(DISABLED);
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new SurveyPublicationValidator(survey).validate(survey, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("id").getDefaultMessage(),
                is("All forms in this survey are disabled")
        );
    }

    @Test
    public void shouldPassIfAllFormsInSurveyAreEnabled() throws Exception {
        Form form = new Form();
        form.setEnabled(ENABLED);
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new SurveyPublicationValidator(survey).validate(survey, result);

        Assert.assertFalse(result.hasErrors());
    }

    @Test
    public void shouldPassIfAtLeastOnFormIsEnabled() throws Exception {
        Form enabledForm = new Form();
        enabledForm.setEnabled(ENABLED);
        enabledForm.setOrdinal(new BigDecimal(1));
        Form disabledForm = new Form();
        disabledForm.setEnabled(DISABLED);
        disabledForm.setOrdinal(new BigDecimal(2));
        Survey survey = new Survey();
        survey.setForms(Arrays.asList(enabledForm, disabledForm));
        BindingResult result = new BindException(survey, "survey");

        new SurveyPublicationValidator(survey).validate(survey, result);

        Assert.assertFalse(result.hasErrors());
    }

}