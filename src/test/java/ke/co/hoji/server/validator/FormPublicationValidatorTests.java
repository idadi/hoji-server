package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.is;

/**
 * Created by geoffreywasilwa on 10/06/2017.
 */
public class FormPublicationValidatorTests {

    private static final boolean FALSE = false;
    private static final boolean TRUE = true;

    private final FieldType text = new FieldType(1, "Single Line", FieldType.Code.SINGLE_LINE_TEXT);
    private final FieldType choice = new FieldType(1, "Single Choice", FieldType.Code.SINGLE_CHOICE);

    @Test
    public void shouldSkipDisabledForms() throws Exception {
        Form disabled = new Form();
        disabled.setEnabled(FALSE);
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(disabled));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertFalse(result.hasErrors());
    }

    @Test
    public void shouldFailIfFormHasNoFields() throws Exception {
        Form noFields = new Form();
        noFields.setEnabled(TRUE);
        noFields.setFields(Collections.EMPTY_LIST);
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(noFields));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
            result.getFieldError("forms[0].id").getDefaultMessage(),
            is("The form <strong>null</strong> is enabled but it has no fields")
        );
    }

    @Test
    public void shouldFailIfAllFieldsInFormAreDisabled() throws Exception {
        Field disabledField = getTextField();
        disabledField.setEnabled(FALSE);
        Form form = new Form();
        form.setEnabled(TRUE);
        form.setFields(Collections.singletonList(disabledField));
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
            result.getFieldError("forms[0].id").getDefaultMessage(),
            is("All fields in the form <strong>null</strong> are disabled")
        );
    }

    @Test
    public void shouldPassIfAllFieldsInFormAreEnabled() throws Exception {
        Field enabledField = getTextField();
        Form form = new Form();
        form.setEnabled(TRUE);
        form.setFields(Collections.singletonList(enabledField));
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertFalse(result.hasErrors());
    }

    @Test
    public void shouldPassIfAtLeastOneFieldIsEnabled() throws Exception {
        Field enabledField = getTextField();
        enabledField.setOrdinal(new BigDecimal(1));
        Field disabledField = getTextField();
        disabledField.setEnabled(FALSE);
        disabledField.setOrdinal(new BigDecimal(2));
        Form form = new Form();
        form.setEnabled(TRUE);
        form.setFields(Arrays.asList(enabledField, disabledField));
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertFalse(result.hasErrors());
    }

    @Test
    public void shouldFailIfLastFieldIsSearchable() throws Exception {
        Field searchableField = getTextField();
        searchableField.setSearchable(TRUE);
        Form form = new Form();
        form.setEnabled(TRUE);
        form.setFields(Collections.singletonList(searchableField));
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
            result.getFieldError("forms[0].id").getDefaultMessage(),
            is("The last field in the form <strong>null</strong> is searchable, which is not allowed.")
        );
    }

    @Test
    public void shouldFailIfFirstFieldIsMatrix() throws Exception {
        FieldType type = new FieldType(1, "Matrix", FieldType.Code.MATRIX);
        Field matrixField = new Field();
        matrixField.setEnabled(TRUE);
        matrixField.setType(type);
        {
            FieldType childType = new FieldType(2, "Text", FieldType.Code.SINGLE_LINE_TEXT);
            Field child = new Field();
            child.setEnabled(TRUE);
            child.setType(childType);
            matrixField.setChildren(Collections.singletonList(child));
        }
        Form form = new Form();
        form.setEnabled(TRUE);
        form.setFields(Collections.singletonList(matrixField));
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
            result.getFieldError("forms[0].id").getDefaultMessage(),
            is("The form <strong>null</strong> has a sub-form as its first field, which is not allowed.")
        );
    }

    @Test
    public void shouldFailIfMatrixFieldHasNoChildren() {
        List<Field> fields = new ArrayList<>();
        {
            FieldType type = new FieldType(1, "Text", FieldType.Code.SINGLE_LINE_TEXT);
            Field field1 = new Field();
            field1.setOrdinal(new BigDecimal(1));
            field1.setEnabled(TRUE);
            field1.setType(type);
            fields.add(field1);
        }
        {
            FieldType type = new FieldType(2, "Matrix", FieldType.Code.MATRIX);
            Field matrixField = new Field();
            matrixField.setOrdinal(new BigDecimal(2));
            matrixField.setEnabled(TRUE);
            matrixField.setType(type);
            fields.add(matrixField);
        }
        Form form = new Form();
        form.setEnabled(TRUE);
        form.setFields(fields);
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
            result.getFieldError("forms[0].id").getDefaultMessage(),
            is("The field <strong>null</strong> in the form <strong>null</strong> is a sub-form but it has no " +
                "child fields.")
        );
    }

    @Test
    public void shouldFailIfFirstFieldIsImage() throws Exception {
        FieldType type = new FieldType(1, "Image", FieldType.Code.IMAGE);
        Field imageField = new Field();
        imageField.setEnabled(TRUE);
        imageField.setType(type);
        Form form = new Form();
        form.setEnabled(TRUE);
        form.setFields(Collections.singletonList(imageField));
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
            result.getFieldError("forms[0].id").getDefaultMessage(),
            is("The form <strong>null</strong> has an image as its first field, which is not allowed.")
        );
    }

    @Test
    public void shouldFailIfImageFieldMaxValueExceeds1024() throws Exception {
        FieldType image = new FieldType(1, "Image", FieldType.Code.IMAGE);
        FieldType number = new FieldType(2, "Whole Number", FieldType.Code.WHOLE_NUMBER);
        Field imageField = new Field();
        imageField.setEnabled(TRUE);
        imageField.setType(image);
        imageField.setMaxValue("1025");
        imageField.setOrdinal(new BigDecimal(1));
        Form form = new Form();
        form.setEnabled(TRUE);
        Field numberField = new Field();
        numberField.setType(number);
        numberField.setEnabled(TRUE);
        numberField.setOrdinal(new BigDecimal(2));
        form.setFields(Arrays.asList(numberField, imageField));
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertTrue(result.hasErrors());

        Assert.assertThat(
            result.getFieldError("forms[0].id").getDefaultMessage(),
            is("The image field <strong>null</strong> in the form <strong>null</strong> has a maximum size that " +
                "exceeds 1024 pixels. Set a lower value.")
        );
    }

    @Test
    public void shouldFailIfImageFieldMaxValueIsBelow64() throws Exception {
        FieldType image = new FieldType(1, "Image", FieldType.Code.IMAGE);
        FieldType number = new FieldType(2, "Whole Number", FieldType.Code.WHOLE_NUMBER);
        Field imageField = new Field();
        imageField.setEnabled(TRUE);
        imageField.setType(image);
        imageField.setMaxValue("63");
        imageField.setOrdinal(new BigDecimal(1));
        Form form = new Form();
        form.setEnabled(TRUE);
        Field numberField = new Field();
        numberField.setType(number);
        numberField.setEnabled(TRUE);
        numberField.setOrdinal(new BigDecimal(2));
        form.setFields(Arrays.asList(numberField, imageField));
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertTrue(result.hasErrors());

        Assert.assertThat(
            result.getFieldError("forms[0].id").getDefaultMessage(),
            is("The image field <strong>null</strong> in the form <strong>null</strong> has a maximum size that " +
                "is below 64 pixels. Set a higher value.")
        );
    }

    @Test
    public void shouldFailIfImageFieldMinValueExceeds100() throws Exception {
        FieldType image = new FieldType(1, "Image", FieldType.Code.IMAGE);
        FieldType number = new FieldType(2, "Whole Number", FieldType.Code.WHOLE_NUMBER);
        Field imageField = new Field();
        imageField.setEnabled(TRUE);
        imageField.setType(image);
        imageField.setMaxValue("1024");
        imageField.setMinValue("101");
        imageField.setOrdinal(new BigDecimal(1));
        Form form = new Form();
        form.setEnabled(TRUE);
        Field numberField = new Field();
        numberField.setType(number);
        numberField.setEnabled(TRUE);
        numberField.setOrdinal(new BigDecimal(2));
        form.setFields(Arrays.asList(numberField, imageField));
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertTrue(result.hasErrors());

        Assert.assertThat(
            result.getFieldError("forms[0].id").getDefaultMessage(),
            is("The image field <strong>null</strong> in the form <strong>null</strong> has a minimum quality " +
                "setting that is above 100%. Set a lower value.")
        );
    }

    @Test
    public void shouldFailIfImageFieldMinValueIsBelow25() throws Exception {
        FieldType image = new FieldType(1, "Image", FieldType.Code.IMAGE);
        FieldType number = new FieldType(2, "Whole Number", FieldType.Code.WHOLE_NUMBER);
        Field imageField = new Field();
        imageField.setEnabled(TRUE);
        imageField.setType(image);
        imageField.setMaxValue("1024");
        imageField.setMinValue("24");
        imageField.setOrdinal(new BigDecimal(1));
        Form form = new Form();
        form.setEnabled(TRUE);
        Field numberField = new Field();
        numberField.setType(number);
        numberField.setEnabled(TRUE);
        numberField.setOrdinal(new BigDecimal(2));
        form.setFields(Arrays.asList(numberField, imageField));
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertTrue(result.hasErrors());

        String ssssssssss = result.getFieldError("forms[0].id").getDefaultMessage();

        Assert.assertThat(
            result.getFieldError("forms[0].id").getDefaultMessage(),
            is("The image field <strong>null</strong> in the form <strong>null</strong> has a minimum quality " +
                "setting that is below 25%. Set a higher value.")
        );
    }

    @Test
    public void shouldFailIfNumberOfSearchableFieldsExceedsLimit() throws Exception {
        Field field1 = getTextField();
        field1.setSearchable(TRUE);
        field1.setOrdinal(new BigDecimal(1));
        Field field2 = getTextField();
        field2.setSearchable(TRUE);
        field2.setOrdinal(new BigDecimal(2));
        Field field3 = getTextField();
        field3.setSearchable(TRUE);
        field3.setOrdinal(new BigDecimal(3));
        Field field4 = getTextField();
        field4.setSearchable(TRUE);
        field4.setOrdinal(new BigDecimal(4));
        Field field5 = getTextField();
        field5.setSearchable(TRUE);
        field5.setOrdinal(new BigDecimal(5));
        Field field6 = getTextField();
        field6.setSearchable(TRUE);
        field6.setOrdinal(new BigDecimal(6));
        Field field7 = getTextField();
        field7.setOrdinal(new BigDecimal(7));
        Form form = new Form();
        form.setEnabled(TRUE);
        form.setFields(Arrays.asList(field1, field2, field3, field4, field5, field6, field7));
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
            result.getFieldError("forms[0].id").getDefaultMessage(),
            is("The form <strong>null</strong> has 6 searchable fields, which is more than the 5 allowed.")
        );
    }

    @Test
    public void shouldFailIfFilteredChoiceFieldIsBeforeFilterer() throws Exception {
        Choice parent = new Choice(1);
        ChoiceGroup parentCG = new ChoiceGroup(Collections.singletonList(parent));
        Choice filtered = new Choice(2);
        filtered.setParent(parent);
        ChoiceGroup filteredCG = new ChoiceGroup(Collections.singletonList(filtered));
        Field first = getChoiceField();
        first.setChoiceGroup(filteredCG);
        first.setOrdinal(new BigDecimal(1));
        Field second = getChoiceField();
        second.setChoiceGroup(parentCG);
        second.setOrdinal(new BigDecimal(2));
        Form form = new Form();
        form.setEnabled(TRUE);
        form.setFields(Arrays.asList(first, second));
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(form));
        BindingResult result = new BindException(survey, "survey");

        new FormPublicationValidator(survey.getForms()).validate(survey.getForms(), result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
            result.getFieldError("forms[0].id").getDefaultMessage(),
            is("The choice <strong>null</strong> in the field <strong>null</strong> in the form " +
                "<strong>null</strong> has a parent <strong>null</strong> that cannot be found in any field before it.")
        );
    }

    private Field getTextField() {
        Field field = new Field();
        field.setEnabled(TRUE);
        field.setType(text);
        return field;
    }

    private Field getChoiceField() {
        Field field = new Field();
        field.setEnabled(TRUE);
        field.setType(choice);
        return field;
    }

}