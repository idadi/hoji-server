package ke.co.hoji.server.validator;

import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 11/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserValidatorTests {

    @Mock
    private UserService userService;

    @Test
    public void shouldFailIfUsernameIsNotAnEmail() throws Exception {
        User user = new User();
        user.setUsername("username");
        BindingResult result = new BindException(user, "user");

        new UserValidator(userService).validate(user, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("username").getCode(),
                is("user.invalid_username")
        );
    }

    @Test
    public void shouldFailIfUsernameMissing() throws Exception {
        User user = new User();
        BindingResult result = new BindException(user, "user");

        new UserValidator(userService).validate(user, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("username").getCode(),
                is("user.empty_username")
        );
    }

    @Test
    public void shouldFailIfUsernameAlreadyInUse() throws Exception {
        when(userService.userExists(anyString())).thenReturn(true);
        User user = new User();
        user.setUsername("test@hoji.co.ke");
        BindingResult result = new BindException(user, "user");

        new UserValidator(userService).validate(user, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("username").getCode(),
                is("user.username_exists")
        );
    }

    @Test
    public void shouldFailIfPasswordShort() throws Exception {
        User user = new User();
        user.setUsername("test@hoji.co.ke");
        user.setPassword("test");
        BindingResult result = new BindException(user, "user");

        new UserValidator(userService).validate(user, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("password").getCode(),
                is("user.password_too_short")
        );
    }

    @Test
    public void shouldFailIfPasswordDontMatch() throws Exception {
        User user = new User();
        user.setUsername("test@hoji.co.ke");
        user.setPassword("testing1");
        user.setConfirmPassword("testing2");
        BindingResult result = new BindException(user, "user");

        new UserValidator(userService).validate(user, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("confirmPassword").getCode(),
                is("user.password_mismatch")
        );
    }

}