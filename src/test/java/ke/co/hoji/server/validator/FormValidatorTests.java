package ke.co.hoji.server.validator;

import static org.hamcrest.Matchers.is;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import ke.co.hoji.core.data.model.Form;

/**
 * Created by geoffreywasilwa on 11/06/2017.
 */
public class FormValidatorTests {

    @Test
    public void shouldFailIfFormHasNoName() throws Exception {
        Form form = new Form();
        BindingResult result = new BindException(form, "form");

        new FormValidator().validate(form, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("name").getDefaultMessage(),
                is("Enter form name")
        );
    }

    @Test
    public void shouldPassIfFormConfiguredCorrectly() throws Exception {
        Form form = new Form();
        form.setName("Form");
        BindingResult result = new BindException(form, "form");

        new FormValidator().validate(form, result);

        Assert.assertFalse(result.hasErrors());
    }

}