package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import static org.hamcrest.Matchers.is;

/**
 * Created by geoffreywasilwa on 11/06/2017.
 */
public class FieldValidatorTests {

    @Test
    public void shouldFailIfFieldHasNoDescription() throws Exception {
        Field field = new Field();
        BindingResult result = new BindException(field, "field");

        new FieldValidator().validate(field, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("description").getDefaultMessage(),
                is("Enter field text")
        );
    }

    @Test
    public void shouldFailIfFieldHasNoType() throws Exception {
        Field field = new Field();
        field.setDescription("Description");
        BindingResult result = new BindException(field, "field");

        new FieldValidator().validate(field, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("type").getDefaultMessage(),
                is("Select field type")
        );
    }

    @Test
    public void shouldFailIfChoiceFieldWithNoChoiceGroup() throws Exception {
        Field field = new Field();
        field.setDescription("Description");
        field.setType(new FieldType(1, "Choice", FieldType.Code.SINGLE_CHOICE));
        BindingResult result = new BindException(field, "field");

        new FieldValidator().validate(field, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("choiceGroup").getDefaultMessage(),
                is("Select choice group")
        );
    }

    @Test
    public void shouldFailIfMatrixFieldHasParent() throws Exception {
        Field field = new Field();
        field.setDescription("Matrix");
        field.setType(new FieldType(1, "Matrix", FieldType.Code.MATRIX));
        field.setParent(new Field());
        BindingResult result = new BindException(field, "field");

        new FieldValidator().validate(field, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("parent").getDefaultMessage(),
                is("Matrix field cannot have a parent")
        );
    }

    @Test
    public void shouldPassIfFieldConfiguredProperly() throws Exception {
        Field field = new Field();
        field.setDescription("Description");
        field.setType(new FieldType(1, "Text", FieldType.Code.SINGLE_LINE_TEXT));
        BindingResult result = new BindException(field, "field");
        new FieldValidator().validate(field, result);

        Assert.assertFalse(result.hasErrors());
    }

}