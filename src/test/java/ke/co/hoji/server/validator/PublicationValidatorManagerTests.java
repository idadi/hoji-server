package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.model.Survey;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

/**
 * Created by geoffreywasilwa on 11/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class PublicationValidatorManagerTests {

    private static final int CALLED_ONCE = 1;

    private SurveyPublicationValidator surveyPublicationValidator;

    private FormPublicationValidator formPublicationValidator;

    @Before
    public void setup() {
        surveyPublicationValidator = Mockito.spy(new SurveyPublicationValidator(new Survey()));
        formPublicationValidator = Mockito.spy(new FormPublicationValidator(Collections.EMPTY_LIST));
    }

    @Test
    public void shouldNotRunNextValidatorIfPreviousValidatorHasNoError() {
        PublicationValidatorManager manager = new PublicationValidatorManager();
        manager.addPublicationValidator(surveyPublicationValidator);
        manager.addPublicationValidator(formPublicationValidator);
        manager.run(new BindException(new Survey(), "survey"));

        Mockito.verify(surveyPublicationValidator, times(CALLED_ONCE)).validate(any(Object.class), any(Errors.class));
        Mockito.verify(formPublicationValidator, never()).validate(any(Object.class), any(Errors.class));
    }

}