package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Rule;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.is;

/**
 * Created by geoffreywasilwa on 10/06/2017.
 */
public class FieldPublicationValidatorTests {

    private static final boolean DISABLED = false;
    private static final boolean ENABLED = true;
    private final FieldType choice = new FieldType(1, "Single Choice", FieldType.Code.SINGLE_CHOICE);
    private final FieldType text = new FieldType(2, "Text", FieldType.Code.SINGLE_LINE_TEXT);

    @Test
    public void shouldSkipDisabledFields() throws Exception {
        Field disabledField = new Field();
        disabledField.setEnabled(DISABLED);
        List<Field> fields = new ArrayList<>();
        BindingResult result = new BindException(fields, "");

        new FieldPublicationValidator(fields).validate(fields, result);

        Assert.assertFalse(result.hasErrors());
    }

    @Test
    public void shouldFailIfChoiceFieldHasNoChoiceGroup() throws Exception {
        Form form = getFormWithChoiceFieldWithoutChoiceGroup();
        BindingResult result = new BindException(form, "form");

        new FieldPublicationValidator(form.getFields()).validate(form.getFields(), result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("id").getDefaultMessage(),
                is("The field <strong>null</strong> in the form <strong>null</strong> has no choice group.")
        );
    }

    @Test
    public void shouldFailIfChoiceFieldHasChoiceGroupWithNoChoices() throws Exception {
        Field field = getFieldWithChoiceGroupWithoutChoices();
        Form form = new Form();
        form.setFields(Collections.singletonList(field));
        field.setForm(form);
        BindingResult result = new BindException(form, "form");

        new FieldPublicationValidator(form.getFields()).validate(form.getFields(), result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("id").getDefaultMessage(),
                is("The choice group <strong>null</strong> of the field <strong>null</strong> in the form " +
                    "<strong>null</strong> has no choices.")
        );
    }

    @Test
    public void shouldFailIfChoicesInChoiceGroupShareCode() throws Exception {
        Field field = getFieldWithChoicesSharingCodes();
        Form form = new Form();
        form.setFields(Collections.singletonList(field));
        field.setForm(form);
        BindingResult result = new BindException(form, "form");

        new FieldPublicationValidator(form.getFields()).validate(form.getFields(), result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("id").getDefaultMessage(),
                is("The choices <strong>null</strong> and <strong>null</strong> in the choice group " +
                    "<strong>null</strong> associated with the field <strong>null</strong> in the form " +
                    "<strong>null</strong> share the same code <strong>CODE</strong>.")
        );
    }

    @Test
    public void shouldPassIfChoicesInChoiceGroupHaveNoCodeAssigned() throws Exception {
        Field field = getFieldWithChoicesHavingNoCodes();
        Form form = new Form();
        form.setFields(Collections.singletonList(field));
        field.setForm(form);
        BindingResult result = new BindException(form, "form");

        new FieldPublicationValidator(form.getFields()).validate(form.getFields(), result);

        Assert.assertFalse(result.hasErrors());
    }

    @Test
    public void shouldFailIfForwardSkipTargetIsBeforeOwner() throws Exception {
        Field owner = getField();
        owner.setType(text);
        owner.setOrdinal(new BigDecimal(2));
        Field target = getField();
        target.setType(text);
        target.setOrdinal(new BigDecimal(1));
        Rule forwardSkip = getRule(owner, target);
        forwardSkip.setType(Rule.Type.FORWARD_SKIP);
        owner.setForwardSkips(Collections.singletonList(forwardSkip));
        Form form = new Form();
        owner.setForm(form);
        form.setFields(Arrays.asList(owner, target));
        BindingResult result = new BindException(form, "form");

        new FieldPublicationValidator(form.getFields()).validate(form.getFields(), result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("id").getDefaultMessage(),
                is("The field <strong>null</strong> in the form <strong>null</strong> has <strong>if-then-show" +
                    "</strong> skip logic that points to a field before it.")
        );
    }

    @Test
    public void shouldFailIfBackwardSkipTargetIsAfterOwner() throws Exception {
        Field owner = getField();
        owner.setType(text);
        owner.setOrdinal(new BigDecimal(1));
        Field target = getField();
        target.setType(text);
        target.setOrdinal(new BigDecimal(2));
        Rule backSkip = getRule(owner, target);
        backSkip.setType(Rule.Type.BACKWARD_SKIP);
        owner.setBackwardSkips(Collections.singletonList(backSkip));
        Form form = new Form();
        owner.setForm(form);
        form.setFields(Arrays.asList(owner, target));
        BindingResult result = new BindException(form, "form");

        new FieldPublicationValidator(form.getFields()).validate(form.getFields(), result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("id").getDefaultMessage(),
                is("The field <strong>null</strong> in the form <strong>null</strong> has <strong>show-if" +
                    "</strong> skip logic that points to a field after it.")
        );
    }

    private Form getFormWithChoiceFieldWithoutChoiceGroup() {
        Field field = getField();
        Form form = new Form();
        form.setFields(Collections.singletonList(field));
        field.setForm(form);
        return form;
    }

    private Field getFieldWithChoiceGroupWithoutChoices() {
        Field field = getField();
        ChoiceGroup choiceGroup = new ChoiceGroup(Collections.EMPTY_LIST);
        field.setChoiceGroup(choiceGroup);
        return field;
    }

    private Field getFieldWithChoicesSharingCodes() {
        Field field = getField();
        Choice firstChoice = new Choice();
        firstChoice.setCode("CODE");
        Choice secondChoice = new Choice();
        secondChoice.setCode("S_CODE");
        Choice thirdChoice = new Choice();
        thirdChoice.setCode("CODE");
        ChoiceGroup choiceGroup = new ChoiceGroup(Arrays.asList(firstChoice,secondChoice,thirdChoice));
        field.setChoiceGroup(choiceGroup);
        return field;
    }

    private Field getFieldWithChoicesHavingNoCodes() {
        Field field = getField();
        Choice firstChoice = new Choice();
        Choice secondChoice = new Choice();
        Choice thirdChoice = new Choice();
        ChoiceGroup choiceGroup = new ChoiceGroup(Arrays.asList(firstChoice,secondChoice,thirdChoice));
        field.setChoiceGroup(choiceGroup);
        return field;
    }

    private Field getFieldWithParentBeforeIt() {
        Field field = getField();
        Choice firstChoice = new Choice(1);
        Choice secondChoice = new Choice(2);
        secondChoice.setParent(firstChoice);
        ChoiceGroup choiceGroup = new ChoiceGroup(Arrays.asList(firstChoice, secondChoice));
        field.setChoiceGroup(choiceGroup);
        return field;
    }

    private Field getFieldWithoutParentBeforeIt() {
        Field field = getField();
        Choice firstChoice = new Choice(1);
        Choice secondChoice = new Choice(2);
        secondChoice.setParent(firstChoice);
        ChoiceGroup choiceGroup = new ChoiceGroup(Arrays.asList(secondChoice));
        field.setChoiceGroup(choiceGroup);
        return field;
    }

    private Field getField() {
        Field field = new Field();
        field.setEnabled(ENABLED);
        field.setType(choice);
        field.setForwardSkips(Collections.EMPTY_LIST);
        field.setBackwardSkips(Collections.EMPTY_LIST);
        return field;
    }

    private Rule getRule(Field owner, Field target) {
        Rule rule = new Rule();
        rule.setOwner(owner);
        rule.setTarget(target);
        return rule;
    }

}