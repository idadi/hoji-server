package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.model.Survey;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import static org.hamcrest.Matchers.is;

/**
 * Created by geoffreywasilwa on 11/06/2017.
 */
public class SurveyValidatorTests {

    @Test
    public void shouldFailIfSurveyHasNoName() throws Exception {
        Survey survey = new Survey();
        BindingResult result = new BindException(survey, "survey");

        new SurveyValidator().validate(survey, result);

        Assert.assertTrue(result.hasErrors());
        Assert.assertThat(
                result.getFieldError("name").getDefaultMessage(),
                is("Enter project name")
        );
    }

    @Test
    public void shouldPassIfSurveyConfiguredCorrectly() throws Exception {
        Survey survey = new Survey();
        survey.setName("Survey");
        BindingResult result = new BindException(survey, "survey");

        new SurveyValidator().validate(survey, result);

        Assert.assertFalse(result.hasErrors());
    }

}