package ke.co.hoji.server.resolver;

import ke.co.hoji.core.data.Period;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.server.controller.report.model.Filter;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 13/07/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class FilterHandlerMethodArgumentResolverTests {

    @Mock
    private ConfigService configService;

    @Mock
    private MethodParameter methodParameter;

    @Mock
    private ModelAndViewContainer modelAndViewContainer;

    private ServletWebRequest webRequest;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private WebDataBinderFactory webDataBinderFactory;

    @Before
    public void setup() {
        webRequest = new ServletWebRequest(httpServletRequest);
    }

    @Test
    public void shouldResolveFilterWithForm() throws Exception {
        Form form = new Form(1);
        when(httpServletRequest.getRequestURI()).thenReturn("/form");
        when(httpServletRequest.getAttribute(anyString())).thenReturn(Collections.singletonMap("formId", "1"));
        when(configService.getForm(form.getId())).thenReturn(form);

        Filter filter = (Filter) new FilterHandlerMethodArgumentResolver(configService).resolveArgument(
            methodParameter, modelAndViewContainer, webRequest, webDataBinderFactory
        );

        Assert.assertThat(filter.getFieldParent().getId(), is(form.getId()));
    }

    @Test
    public void shouldResolveFilterWithField() throws Exception {
        Field field = new Field(1);
        when(httpServletRequest.getRequestURI()).thenReturn("/field");
        when(httpServletRequest.getAttribute(anyString())).thenReturn(Collections.singletonMap("fieldId", "1"));
        when(configService.getField(field.getId())).thenReturn(field);

        Filter filter = (Filter) new FilterHandlerMethodArgumentResolver(configService).resolveArgument(
            methodParameter, modelAndViewContainer, webRequest, webDataBinderFactory
        );

        Assert.assertThat(filter.getFieldParent().getId(), is(field.getId()));
    }

    @Test
    public void shouldResolveFilterWithUserIds() throws Exception {
        Form form = new Form(1);
        when(httpServletRequest.getRequestURI()).thenReturn("/form");
        when(httpServletRequest.getAttribute(anyString())).thenReturn(Collections.singletonMap("formId", "1"));
        when(httpServletRequest.getParameterValues("selectedUsers[]")).thenReturn(new String[]{"1", "2"});
        when(configService.getForm(form.getId())).thenReturn(form);

        Filter filter = (Filter) new FilterHandlerMethodArgumentResolver(configService).resolveArgument(
            methodParameter, modelAndViewContainer, webRequest, webDataBinderFactory
        );

        Assert.assertThat(filter.getFieldParent().getId(), is(form.getId()));
        Assert.assertThat(filter.getSelectedUsers().size(), is(2));
        Assert.assertThat(filter.getSelectedUsers().get(0), is(1));
        Assert.assertThat(filter.getSelectedUsers().get(1), is(2));
    }

    @Test
    public void shouldResolveFilterWithCompletedDates() throws Exception {
        Form form = new Form(1);
        when(httpServletRequest.getRequestURI()).thenReturn("/form");
        when(httpServletRequest.getAttribute(anyString())).thenReturn(Collections.singletonMap("formId", "1"));
        when(httpServletRequest.getParameter("completedAfter")).thenReturn("2017-07-10");
        when(httpServletRequest.getParameter("completedBefore")).thenReturn("2017-07-13");
        when(httpServletRequest.getParameter("uploadedAfter")).thenReturn("2017-07-10");
        when(httpServletRequest.getParameter("uploadedBefore")).thenReturn("2017-07-13");
        when(configService.getForm(form.getId())).thenReturn(form);

        Filter filter = (Filter) new FilterHandlerMethodArgumentResolver(configService).resolveArgument(
            methodParameter, modelAndViewContainer, webRequest, webDataBinderFactory
        );

        DateTime expectedCompletedFrom = new DateTime(2017, 7, 10, 0, 0);
        DateTime expectedCompletedTo = new DateTime(2017, 7, 13, 23, 59, 59);
        Assert.assertThat(filter.getFieldParent().getId(), is(form.getId()));
        Assert.assertThat(filter.getCompletedFrom(), is(expectedCompletedFrom.toDate()));
        Assert.assertThat(filter.getCompletedTo(), is(expectedCompletedTo.toDate()));
        Assert.assertThat(filter.getUploadedFrom(), is(expectedCompletedFrom.toDate()));
        Assert.assertThat(filter.getUploadedTo(), is(expectedCompletedTo.toDate()));
    }

    @Test
    public void shouldResolveFilterWithCompletedString() throws Exception {
        Form form = new Form(1);
        when(httpServletRequest.getRequestURI()).thenReturn("/form");
        when(httpServletRequest.getAttribute(anyString())).thenReturn(Collections.singletonMap("formId", "1"));
        when(httpServletRequest.getParameter("completed")).thenReturn(Period.TODAY);
        when(configService.getForm(form.getId())).thenReturn(form);

        Filter filter = (Filter) new FilterHandlerMethodArgumentResolver(configService).resolveArgument(
            methodParameter, modelAndViewContainer, webRequest, webDataBinderFactory
        );

        Assert.assertThat(filter.getFieldParent().getId(), is(form.getId()));
        Assert.assertNotNull(filter.getCompletedFrom());
        Assert.assertNotNull(filter.getCompletedTo());
    }

}