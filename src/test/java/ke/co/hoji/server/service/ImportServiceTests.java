package ke.co.hoji.server.service;

import ke.co.hoji.core.data.dto.LiteRecord;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.http.ImportRequest;
import ke.co.hoji.core.data.http.ImportResponse;
import ke.co.hoji.core.data.http.ResultCode;
import ke.co.hoji.core.data.http.SearchRequest;
import ke.co.hoji.core.data.http.SearchResponse;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.core.service.model.SurveyService;
import ke.co.hoji.server.repository.FilterCriteria;
import ke.co.hoji.server.repository.MainRecordRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 09/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class ImportServiceTests {

    private static final int NON_EXISTENT_SURVEY_ID = 3;
    private static final int SURVEY_ID = 1;
    private static final int FORM_ID = 1;
    private static final int FORM_VERSION = 1;
    private static final int USER_ID = 1;
    private static final int MATCH_SENSITIVITY = 0;
    private static final String SURVEY_NAME = "Survey";
    private static final boolean DISABLED = false;
    private static final String SURVEY_CODE = "SVY";
    private static final boolean ENABLED = true;
    private static final String FORM_NAME = "Form";
    private static final BigDecimal FORM_ORDINAL = new BigDecimal(1);
    private static final int MINOR_VERSION = 1;
    private static final int NEW_MAJOR_VERSION = 2;
    private static final String SEARCH_TERMS = "term1 term2";
    private static final String UUID = "uuid";

    @Mock
    private MainRecordRepository recordRepository;

    @Mock
    private SurveyService surveyService;

    @Mock
    private FormService formService;

    @Mock
    private FieldService fieldService;

    private ImportService importService;

    @Before
    public void setup() {
        importService = new ImportService(recordRepository, surveyService, formService, fieldService);
    }

    @Test
    public void search_ShouldReturnFormNotFoundForNonExistentSurvey() {
        SearchRequest request =
            new SearchRequest(
                NON_EXISTENT_SURVEY_ID,
                FORM_ID, FORM_VERSION,
                null,
                null,
                USER_ID,
                null,
                MATCH_SENSITIVITY);

        SearchResponse response = importService.search(request);

        Assert.assertThat(response.getResultCode(), is(ResultCode.FORM_NOT_FOUND));
    }

    @Test
    public void search_ShouldReturnSurveyDisabledIfSurveyIsDisabled() {
        Survey disabledSurvey = new Survey(SURVEY_ID, SURVEY_NAME, DISABLED, SURVEY_CODE);
        Form form = new Form(FORM_ID, FORM_NAME, ENABLED, FORM_ORDINAL, MINOR_VERSION, NEW_MAJOR_VERSION);
        when(surveyService.getSurveyById(SURVEY_ID)).thenReturn(disabledSurvey);
        when(formService.getFormById(FORM_ID)).thenReturn(form);
        SearchRequest request =
            new SearchRequest(
                SURVEY_ID,
                FORM_ID,
                FORM_VERSION,
                null,
                null,
                USER_ID,
                null,
                MATCH_SENSITIVITY);

        SearchResponse response = importService.search(request);

        Assert.assertThat(response.getResultCode(), is(ResultCode.SURVEY_NOT_ENABLED));
    }

    @Test
    public void search_ShouldReturnSurveyUnPublishedIfSurveyNotPublished() {
        Survey unpublishedSurvey = new Survey(SURVEY_ID, SURVEY_NAME, ENABLED, SURVEY_CODE);
        unpublishedSurvey.setStatus(Survey.UNPUBLISHED);
        Form form = new Form(FORM_ID, FORM_NAME, ENABLED, FORM_ORDINAL, MINOR_VERSION, NEW_MAJOR_VERSION);
        when(surveyService.getSurveyById(SURVEY_ID)).thenReturn(unpublishedSurvey);
        when(formService.getFormById(FORM_ID)).thenReturn(form);
        SearchRequest request =
            new SearchRequest(
                SURVEY_ID,
                FORM_ID,
                FORM_VERSION,
                null,
                null,
                USER_ID,
                null,
                MATCH_SENSITIVITY);

        SearchResponse response = importService.search(request);

        Assert.assertThat(response.getResultCode(), is(ResultCode.SURVEY_NOT_PUBLISHED));
    }

    @Test
    public void search_ShouldReturnFormNotFoundForNonExistentForm() {
        Survey publishedSurvey = new Survey(SURVEY_ID, SURVEY_NAME, ENABLED, SURVEY_CODE);
        publishedSurvey.setStatus(Survey.PUBLISHED);
        when(surveyService.getSurveyById(SURVEY_ID)).thenReturn(publishedSurvey);
        SearchRequest request =
            new SearchRequest(
                SURVEY_ID,
                FORM_ID,
                FORM_VERSION,
                null,
                null,
                USER_ID,
                null,
                MATCH_SENSITIVITY);

        SearchResponse response = importService.search(request);

        Assert.assertThat(response.getResultCode(), is(ResultCode.FORM_NOT_FOUND));
    }

    @Test
    public void search_ShouldReturnOutOfDateWhenRequestingAnOlderVersion() {
        Survey publishedSurvey = new Survey(SURVEY_ID, SURVEY_NAME, ENABLED, SURVEY_CODE);
        publishedSurvey.setStatus(Survey.PUBLISHED);
        Form form = new Form(FORM_ID, FORM_NAME, ENABLED, FORM_ORDINAL, MINOR_VERSION, NEW_MAJOR_VERSION);
        when(surveyService.getSurveyById(SURVEY_ID)).thenReturn(publishedSurvey);
        when(formService.getFormById(FORM_ID)).thenReturn(form);
        SearchRequest request =
            new SearchRequest(
                SURVEY_ID,
                FORM_ID,
                FORM_VERSION,
                null,
                null,
                USER_ID,
                null,
                MATCH_SENSITIVITY);

        SearchResponse response = importService.search(request);

        Assert.assertThat(response.getResultCode(), is(ResultCode.SURVEY_OUT_OF_DATE));
    }

    @Test
    public void search_ShouldReturnMatchedRecords() {
        Survey publishedSurvey = new Survey(SURVEY_ID, SURVEY_NAME, ENABLED, SURVEY_CODE);
        publishedSurvey.setStatus(Survey.PUBLISHED);
        Form form = new Form(FORM_ID, FORM_NAME, ENABLED, FORM_ORDINAL, MINOR_VERSION, NEW_MAJOR_VERSION);
        LiteRecord liteRecord = new LiteRecord();
        liteRecord.setCaption("Caption");
        liteRecord.setUuid("uuid");
        liteRecord.setSearchTerms("term1|term2");
        List<LiteRecord> liteRecords = new ArrayList<>();
        liteRecords.add(liteRecord);
        when(surveyService.getSurveyById(SURVEY_ID)).thenReturn(publishedSurvey);
        when(formService.getFormById(FORM_ID)).thenReturn(form);
        when(recordRepository.findLiteRecords(any(FilterCriteria.class))).thenReturn(liteRecords);
        SearchRequest request =
            new SearchRequest(
                SURVEY_ID,
                FORM_ID,
                NEW_MAJOR_VERSION,
                null,
                null,
                USER_ID,
                SEARCH_TERMS,
                MATCH_SENSITIVITY);

        SearchResponse response = importService.search(request);

        Assert.assertThat(response.getResultCode(), is(ResultCode.OKAY));
        Assert.assertThat(response.getLiteRecords(), hasSize(1));
    }

    @Test
    public void getRecord_ShouldReturnRecordGivenUuid() {
        Survey publishedSurvey = new Survey(SURVEY_ID, SURVEY_NAME, ENABLED, SURVEY_CODE);
        publishedSurvey.setStatus(Survey.PUBLISHED);
        Form form = new Form(FORM_ID, FORM_NAME, ENABLED, FORM_ORDINAL, MINOR_VERSION, NEW_MAJOR_VERSION);
        List<MainRecord> mainRecords = new ArrayList<>();
        MainRecord mainRecord = new MainRecord();
        mainRecord.setFormId(FORM_ID);
        mainRecords.add(mainRecord);
        when(surveyService.getSurveyById(SURVEY_ID)).thenReturn(publishedSurvey);
        when(formService.getFormById(FORM_ID)).thenReturn(form);
        when(recordRepository.findMainRecords(UUID, Collections.emptyList())).thenReturn(mainRecords);
        ImportRequest request = new ImportRequest(SURVEY_ID, FORM_ID);
        request.setUuids(Collections.singletonList(UUID));
        request.setFormVersion(NEW_MAJOR_VERSION);

        ImportResponse response = importService.getRecord(request);

        Assert.assertThat(response.getResultCode(), is(ResultCode.OKAY));
        Assert.assertThat(response.getMainRecordMap().get(UUID), hasSize(1));
    }

    @Test
    public void getRecord_ShouldResolveImageFromPath() {
        Survey publishedSurvey = new Survey(SURVEY_ID, SURVEY_NAME, ENABLED, SURVEY_CODE);
        publishedSurvey.setStatus(Survey.PUBLISHED);
        Form form = new Form(FORM_ID, FORM_NAME, ENABLED, FORM_ORDINAL, MINOR_VERSION, NEW_MAJOR_VERSION);
        Field imageField = new Field();
        imageField.setId(1);
        imageField.setType(new FieldType(1, "Image", FieldType.Code.IMAGE));
        MainRecord mainRecord = new MainRecord();
        mainRecord.setFormId(FORM_ID);
        LiveField imageLf = new LiveField();
        imageLf.setFieldId(imageField.getId());
        imageLf.setValue(getClass().getClassLoader().getResource(".").getFile() + File.separator + "test-image.png");
        mainRecord.setLiveFields(Collections.singletonList(imageLf));
        List<MainRecord> mainRecords = new ArrayList<>();
        mainRecords.add(mainRecord);
        when(surveyService.getSurveyById(SURVEY_ID)).thenReturn(publishedSurvey);
        when(formService.getFormById(FORM_ID)).thenReturn(form);
        when(fieldService.getAllFields(form)).thenReturn(Collections.singletonList(imageField));
        when(recordRepository.findMainRecords(UUID, Collections.emptyList())).thenReturn(mainRecords);
        ImportRequest request = new ImportRequest(SURVEY_ID, FORM_ID);
        request.setUuids(Collections.singletonList(UUID));
        request.setFormVersion(NEW_MAJOR_VERSION);

        ImportResponse response = importService.getRecord(request);

        Assert.assertThat(response.getResultCode(), is(ResultCode.OKAY));
        Assert.assertThat(response.getMainRecordMap().get(UUID), hasSize(1));
        MainRecord returnedMainRecord = response.getMainRecordMap().get(UUID).get(0);
        Assert.assertThat(returnedMainRecord.getLiveFields(), hasSize(1));
        LiveField imageLiveField = returnedMainRecord.getLiveFields().get(0);
        Assert.assertThat(imageLiveField.getValue().toString(), startsWith("iVBOR"));
    }

    @Test
    public void getRecord_ShouldIgnoreImageLiveFieldWithNullValue() {
        Survey publishedSurvey = new Survey(SURVEY_ID, SURVEY_NAME, ENABLED, SURVEY_CODE);
        publishedSurvey.setStatus(Survey.PUBLISHED);
        Form form = new Form(FORM_ID, FORM_NAME, ENABLED, FORM_ORDINAL, MINOR_VERSION, NEW_MAJOR_VERSION);
        Field imageField = new Field();
        imageField.setId(1);
        imageField.setType(new FieldType(1, "Image", FieldType.Code.IMAGE));
        MainRecord mainRecord = new MainRecord();
        mainRecord.setFormId(FORM_ID);
        LiveField imageLf = new LiveField();
        imageLf.setFieldId(imageField.getId());
        mainRecord.setLiveFields(Collections.singletonList(imageLf));
        List<MainRecord> mainRecords = new ArrayList<>();
        mainRecords.add(mainRecord);
        when(surveyService.getSurveyById(SURVEY_ID)).thenReturn(publishedSurvey);
        when(formService.getFormById(FORM_ID)).thenReturn(form);
        when(fieldService.getAllFields(form)).thenReturn(Collections.singletonList(imageField));
        when(recordRepository.findMainRecords(UUID, Collections.emptyList())).thenReturn(mainRecords);
        ImportRequest request = new ImportRequest(SURVEY_ID, FORM_ID);
        request.setUuids(Collections.singletonList(UUID));
        request.setFormVersion(NEW_MAJOR_VERSION);

        ImportResponse response = importService.getRecord(request);

        Assert.assertThat(response.getResultCode(), is(ResultCode.OKAY));
        Assert.assertThat(response.getMainRecordMap().get(UUID), hasSize(1));
        MainRecord returnedMainRecord = response.getMainRecordMap().get(UUID).get(0);
        Assert.assertThat(returnedMainRecord.getLiveFields(), hasSize(1));
        LiveField imageLiveField = returnedMainRecord.getLiveFields().get(0);
        Assert.assertNull(imageLiveField.getValue());
    }

    @Test
    public void getRecord_ShouldIgnoreImagePathThatCannotBeResolved() {
        Survey publishedSurvey = new Survey(SURVEY_ID, SURVEY_NAME, ENABLED, SURVEY_CODE);
        publishedSurvey.setStatus(Survey.PUBLISHED);
        Form form = new Form(FORM_ID, FORM_NAME, ENABLED, FORM_ORDINAL, MINOR_VERSION, NEW_MAJOR_VERSION);
        Field imageField = new Field();
        imageField.setId(1);
        imageField.setType(new FieldType(1, "Image", FieldType.Code.IMAGE));
        MainRecord mainRecord = new MainRecord();
        mainRecord.setFormId(FORM_ID);
        LiveField imageLf = new LiveField();
        imageLf.setFieldId(imageField.getId());
        imageLf.setValue("non-existant-path");
        mainRecord.setLiveFields(Collections.singletonList(imageLf));
        List<MainRecord> mainRecords = new ArrayList<>();
        mainRecords.add(mainRecord);
        when(surveyService.getSurveyById(SURVEY_ID)).thenReturn(publishedSurvey);
        when(formService.getFormById(FORM_ID)).thenReturn(form);
        when(fieldService.getAllFields(form)).thenReturn(Collections.singletonList(imageField));
        when(recordRepository.findMainRecords(UUID, Collections.emptyList())).thenReturn(mainRecords);
        ImportRequest request = new ImportRequest(SURVEY_ID, FORM_ID);
        request.setUuids(Collections.singletonList(UUID));
        request.setFormVersion(NEW_MAJOR_VERSION);

        ImportResponse response = importService.getRecord(request);

        Assert.assertThat(response.getResultCode(), is(ResultCode.OKAY));
        Assert.assertThat(response.getMainRecordMap().get(UUID), hasSize(1));
        MainRecord returnedMainRecord = response.getMainRecordMap().get(UUID).get(0);
        Assert.assertThat(returnedMainRecord.getLiveFields(), hasSize(1));
        LiveField imageLiveField = returnedMainRecord.getLiveFields().get(0);
        Assert.assertNull(imageLiveField.getValue());
    }

}