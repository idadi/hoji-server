package ke.co.hoji.server.service;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.dao.model.JdbcFormDao;
import ke.co.hoji.server.dao.model.JdbcSurveyDao;
import ke.co.hoji.server.repository.MainRecordRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Collections;

import static org.hamcrest.Matchers.is;

/**
 * Created by geoffreywasilwa on 20/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class PublicationServiceTests {

    private static final int CALLED_ONCE = 1;
    @Mock
    private MainRecordRepository repository;
    @Mock
    private UploadService uploadService;
    @Mock
    private UserService userService;
    @Mock
    private ApplicationEventPublisher applicationEventPublisher;
    @Mock
    private JdbcSurveyDao surveyDao;
    @Mock
    private JdbcFormDao formDao;
    private PublicationService publicationService;

    @Before
    public void setup() {
        publicationService = new PublicationService(
                repository,
                uploadService,
                userService,
                applicationEventPublisher,
                surveyDao,
                formDao
        );
    }

    @Test
    public void publishedSurveyShouldRemainPublished() throws Exception {
        Survey survey = new Survey();
        survey.setStatus(Survey.PUBLISHED);

        publicationService.publish(survey);

        Assert.assertThat(survey.getStatus(), is(Survey.PUBLISHED));
    }

    @Test
    public void shouldIncrementMajorVersionAndCallUploadServiceVersionChanged() throws Exception {
        Form form = new Form();
        form.setMinorModified(true);
        form.setMajorModified(true);
        Survey survey = new Survey();
        survey.setStatus(Survey.MODIFIED);
        survey.setForms(Collections.singletonList(form));

        publicationService.publish(survey);

        Mockito.verify(formDao, Mockito.times(CALLED_ONCE)).incrementMajorVersion(form);
        Mockito.verify(uploadService, Mockito.times(CALLED_ONCE)).formVersionChanged(form);
        Assert.assertThat(survey.getStatus(), is(Survey.PUBLISHED));
    }

    @Test
    public void shouldIncrementMinorVersionAndCallUploadServiceVersionChanged() throws Exception {
        Form form = new Form();
        form.setMinorModified(true);
        Survey survey = new Survey();
        survey.setStatus(Survey.MODIFIED);
        survey.setForms(Collections.singletonList(form));

        publicationService.publish(survey);

        Mockito.verify(formDao, Mockito.times(CALLED_ONCE)).incrementMinorVersion(form);
        Mockito.verify(uploadService, Mockito.times(CALLED_ONCE)).formVersionChanged(form);
        Assert.assertThat(survey.getStatus(), is(Survey.PUBLISHED));
    }
}