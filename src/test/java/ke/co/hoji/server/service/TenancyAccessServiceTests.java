package ke.co.hoji.server.service;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.exception.NotFoundException;
import ke.co.hoji.server.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.access.AccessDeniedException;

import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 11/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class TenancyAccessServiceTests {

    private static final int UNAUTHORIZED_USER_ID = 1;
    private static final int CHOICE_ID = 1;
    private static final int AUTHORIZED_USER_ID = 2;
    @Mock
    private UserService userService;

    @Test(expected = NotFoundException.class)
    public void shouldThrowNotFoundExceptionIsResourceIsNull() throws Exception {
        new TenancyAccessService(userService).check(null, TenancyAccessService.VIEW);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionIfResourceIsNotUserOwned() throws Exception {
        new TenancyAccessService(userService).check(new NotUserOwnedResource(), TenancyAccessService.VIEW);
    }

    @Test(expected = AccessDeniedException.class)
    public void shouldThrowAccessDeniedExceptionWhenAccessUnAuthorizedResource() throws Exception {
        User user = new User();
        user.setId(UNAUTHORIZED_USER_ID);
        Choice resource = setupTest(user);
        checkAccess(resource);
    }

    @Test
    public void shouldNotThrowExceptionIfUserIsAuthorized() throws Exception {
        User user = new User();
        user.setId(AUTHORIZED_USER_ID);
        Choice resource = setupTest(user);
        checkAccess(resource);
    }

    @Test
    public void shouldBeAbleToViewPublicSurvey() throws Exception {
        Survey publicView = new Survey();
        publicView.setId(1);
        publicView.setUserId(1);
        publicView.setAccess(Survey.Access.PUBLIC_VIEW);
        Survey publicEntryView = new Survey();
        publicEntryView.setId(2);
        publicEntryView.setUserId(1);
        publicEntryView.setAccess(Survey.Access.PUBLIC_ENTRY_AND_VIEW);
        User user = new User();
        user.setId(UNAUTHORIZED_USER_ID);
        when(userService.getCurrentUser()).thenReturn(user);
        checkAccess(publicView);
        checkAccess(publicEntryView);
    }

    @Test
    public void shouldBeAbleToAccessOwnUserObject() throws Exception {
        User user = new User();
        user.setId(UNAUTHORIZED_USER_ID);
        when(userService.getCurrentUser()).thenReturn(user);
        checkAccess(user);
    }

    private Choice setupTest(User user) {
        when(userService.getCurrentUser()).thenReturn(user);
        return getResource();
    }

    private void checkAccess(Object resource) {
        new TenancyAccessService(userService).check(resource, TenancyAccessService.VIEW);
    }

    private Choice getResource() {
        Choice resource = new Choice(CHOICE_ID);
        resource.setUserId(AUTHORIZED_USER_ID);
        return resource;
    }

    public static class NotUserOwnedResource {
    }

}