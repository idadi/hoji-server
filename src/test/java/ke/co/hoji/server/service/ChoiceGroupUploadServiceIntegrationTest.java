package ke.co.hoji.server.service;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.dataset.AbstractDataSetLoader;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.io.InputStream;
import java.util.Arrays;

/**
 * <p>
 * Created by geoffreywasilwa on 24/04/2017.
 * </p>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { UploadTestConfiguration.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
@DbUnitConfiguration(dataSetLoader = ChoiceGroupUploadServiceIntegrationTest.ColumnSensingReplacementDataSetLoader.class)
@Ignore
public class ChoiceGroupUploadServiceIntegrationTest {

    @Autowired
    private ChoiceGroupUploadService choiceGroupUploadService;

    @Test
    @ExpectedDatabase("saved-choice-group.xml")
    public void shouldSaveChoiceGroupWithoutChoices() {
        ChoiceGroup yesNo = new ChoiceGroup(null, "Yes/No");
        yesNo.setOrderingStrategy(1);
        yesNo.setExcludeLast(2);

        choiceGroupUploadService.saveOrUpdate(2, Arrays.asList(yesNo));
    }

    @Test
    @DatabaseSetup("test-data.xml")
    @ExpectedDatabase("saved-choice-group-graph.xml")
    public void shouldSaveChoiceGroupsAndChoices() {
        Choice yes = new Choice(null, "Yes", "Y", 1, true);
        Choice no = new Choice(null, "No", "N", 2, true);
        Choice dk = new Choice(null, "Don't Know", "DK", 3, true);
        ChoiceGroup yesNo = new ChoiceGroup(null, "Yes/No");
        yesNo.setChoices(Arrays.asList(yes, no));
        ChoiceGroup yesNoDk = new ChoiceGroup(null, "Yes/No/DK");
        yesNoDk.setChoices(Arrays.asList(yes, no, dk));

        choiceGroupUploadService.saveOrUpdate(2, Arrays.asList(yesNo, yesNoDk));
    }

    @Test
    @ExpectedDatabase("saved-countries-group-graph.xml")
    public void shouldSaveChoiceGroupsAndChoicesInOrderSpecified() {
        Choice kenya = new Choice(null, "Kenya", "100", 0, false);
        Choice uganda = new Choice(null, "Uganda", "200", 0, false);
        Choice tanzania = new Choice(null, "Tanzania", "300", 0, false);
        Choice nigeria = new Choice(null, "Nigeria", "400", 0, false);
        ChoiceGroup countries = new ChoiceGroup(null, "Countries");
        countries.setChoices(Arrays.asList(kenya, uganda, tanzania, nigeria));

        choiceGroupUploadService.saveOrUpdate(2, Arrays.asList(countries));
    }

    @Test
    @ExpectedDatabase("saved-choices-with-parents.xml")
    public void shouldSaveChoiceParents() {
        Choice parent = new Choice(null, "Parent", "P1", 0, false);
        ChoiceGroup parentChoiceGroup = new ChoiceGroup(null, "Parent");
        parentChoiceGroup.setChoices(Arrays.asList(parent));
        Choice childChoice1 = new Choice(null, "Child 1", "C1", 0, false);
        childChoice1.setParent(parent);
        Choice childChoice2 = new Choice(null, "Child 2", "C2", 0, false);
        childChoice2.setParent(parent);
        ChoiceGroup childChoiceGroup = new ChoiceGroup(null, "Child");
        childChoiceGroup.setChoices(Arrays.asList(childChoice1, childChoice2));

        choiceGroupUploadService.saveOrUpdate(2, Arrays.asList(parentChoiceGroup, childChoiceGroup));
    }

    /**
     * This implementation of {@link AbstractDataSetLoader} provides a way of
     * specify null column values in xml dataset files with the "[null]"
     * placeholder.
     * <p>
     * Code copied from:
     * https://www.petrikainulainen.net/programming/spring-framework/spring-from-the-trenches-using-null-values-in-dbunit-datasets/
     * 
     */
    public static class ColumnSensingReplacementDataSetLoader extends AbstractDataSetLoader {

        @Override
        protected IDataSet createDataSet(Resource resource) throws Exception {
            FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
            builder.setColumnSensing(true);
            try (InputStream inputStream = resource.getInputStream()) {
                return createReplacementDataSet(builder.build(inputStream));
            }
        }

        private ReplacementDataSet createReplacementDataSet(FlatXmlDataSet dataSet) {
            ReplacementDataSet replacementDataSet = new ReplacementDataSet(dataSet);

            // Configure the replacement dataset to replace '[null]' strings with null.
            replacementDataSet.addReplacementObject("[null]", null);

            return replacementDataSet;
        }
    }

}