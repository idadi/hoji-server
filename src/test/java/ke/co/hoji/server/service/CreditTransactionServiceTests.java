package ke.co.hoji.server.service;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.dao.impl.JdbcCreditTransactionDao;
import ke.co.hoji.server.model.CreditTransaction;
import ke.co.hoji.server.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 11/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class CreditTransactionServiceTests {

    private static final int CREDITS = 100;
    private static final String REFERENCE_NO = "ref-0";
    private static final Integer CREDIT_BALANCE = 100;
    @Mock
    private JdbcCreditTransactionDao creditTransactionDao;

    @Test
    public void expenseShouldReturnTrueIfSurveyIsFree() throws Exception {
        User user = getUser();
        Assert.assertTrue(new CreditTransactionService(creditTransactionDao).expense(user, getForm(true), REFERENCE_NO, new Date(), CREDITS));
    }

    @Test
    public void expenseShouldNotCreateTransactionIfSurveyIsFree() throws Exception {
        User user = getUser();
        new CreditTransactionService(creditTransactionDao).expense(user, getForm(true), REFERENCE_NO, new Date(), CREDITS);

        Mockito.verify(creditTransactionDao, Mockito.times(0)).create(any(CreditTransaction.class));
    }

    @Test
    public void expenseShouldReturnTrueIfCreditsAreSufficient() throws Exception {
        User user = getUser();
        user.setType(User.Type.PREPAID);
        when(creditTransactionDao.previouslyPaid(anyString())).thenReturn(0);
        when(creditTransactionDao.balance(eq(user), any(Date.class))).thenReturn(CREDIT_BALANCE);

        Assert.assertTrue(new CreditTransactionService(creditTransactionDao).expense(user, getForm(), REFERENCE_NO, new Date(), 10));
    }

    @Test
    public void expenseShouldReturnFalseIfCreditsAreInsufficient() throws Exception {
        User user = getUser();
        user.setType(User.Type.PREPAID);
        when(creditTransactionDao.previouslyPaid(anyString())).thenReturn(0);
        when(creditTransactionDao.balance(eq(user), any(Date.class))).thenReturn(0);

        Assert.assertFalse(new CreditTransactionService(creditTransactionDao).expense(user, getForm(), REFERENCE_NO, new Date(), 10));
    }

    @Test
    public void expenseShouldReturnTrueForPostPaidUsersEvenWithInsufficientBalance() throws Exception {
        User user = getUser();
        user.setType(User.Type.POSTPAID);
        when(creditTransactionDao.previouslyPaid(anyString())).thenReturn(0);

        Assert.assertTrue(new CreditTransactionService(creditTransactionDao).expense(user, getForm(), REFERENCE_NO, new Date(), 10));
    }

    private User getUser() {
        User user = new User();
        user.setDiscountRate(BigDecimal.ZERO);
        user.setTaxRate(new BigDecimal(0.5));
        return user;
    }

    private Form getForm() {
        return getForm(false);
    }

    private Form getForm(boolean freeSurvey) {
        Survey survey = new Survey();
        survey.setFree(freeSurvey);
        Form form = new Form();
        form.setId(1);
        form.setSurvey(survey);
        return form;
    }
}