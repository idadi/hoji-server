package ke.co.hoji.server.service;

import ke.co.hoji.server.dao.impl.JdbcUserDao;
import ke.co.hoji.server.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Created by geoffreywasilwa on 07/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTests {

    private static final int CALLED_ONCE = 1;
    private static final int USER_ID = 2;

    @Mock
    private JdbcUserDao userDao;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private ApplicationEventPublisher eventPublisher;

    private UserService userService;

    @Before
    public void setup() {
        userService = new UserService(userDao, passwordEncoder, eventPublisher);
    }

    @Test
    public void shouldGetUserFromCache() throws Exception {
        User user = new User();
        user.setId(USER_ID);
        user.setFirstName("Cached");
        user.setLastName("User");
        Mockito.when(userDao.findById(USER_ID)).thenReturn(user);
        //first call to cache user
        userService.findById(USER_ID);

        userService.findById(USER_ID);

        Mockito.verify(userDao, Mockito.times(CALLED_ONCE)).findById(USER_ID);
    }

    @Test
    public void shouldUpdateCacheWhenUserIsModified() throws Exception {
        User user = new User();
        user.setId(USER_ID);
        user.setFirstName("Modified");
        user.setLastName("User");
        userService.updateUser(user);
        userService.findById(USER_ID);

        Mockito.verify(userDao, Mockito.never()).findById(USER_ID);
    }

    @Test
    public void shouldCacheUserOnCreation() throws Exception {
        User user = new User();
        user.setId(USER_ID);
        user.setFirstName("Modified");
        user.setLastName("User");
        userService.createUser(user);

        userService.findById(USER_ID);

        Mockito.verify(userDao, Mockito.never()).findById(USER_ID);
    }

}