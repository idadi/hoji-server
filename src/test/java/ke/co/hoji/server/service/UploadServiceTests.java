package ke.co.hoji.server.service;

import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.http.UploadResult;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.User.SlaveStatus;
import ke.co.hoji.server.repository.MainRecordRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Collections;
import java.util.Date;
import java.util.UUID;

import static ke.co.hoji.core.data.Constants.Device.UNIQUE_NAME_MILESTONE;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 09/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class UploadServiceTests {

    private static final int CALLED_ZERO = 0;
    private static final int CALLED_ONCE = 1;
    @Mock
    private MainRecordRepository recordRepository;

    @Mock
    private CreditTransactionService creditTransactionService;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @Test
    public void uploadRecordShouldSaveMainRecordInTestMode() {
        FormService formService = mock(FormService.class);
        UserService userService = mock(UserService.class);
        UploadService uploadService = new UploadService(userService, formService, creditTransactionService, recordRepository, applicationEventPublisher);
        Survey survey = new Survey();
        survey.setId(1);
        survey.setCode("SVY");
        Form form = new Form(1);
        form.setSurvey(survey);
        survey.setForms(Collections.singletonList(form));
        Field field = new Field(1);
        FieldType numeric = new FieldType(1, "Numeric", FieldType.Code.WHOLE_NUMBER);
        field.setType(numeric);
        form.setFields(Collections.singletonList(field));
        User user = new User();
        user.setId(1);
        user.setSlaveStatus(SlaveStatus.INACTIVE);
        user.setTenantUserId(user.getId());
        survey.setUserId(user.getTenantUserId());
        String uuid = UUID.randomUUID().toString();
        MainRecord record = new MainRecord();
        record.setUserId(1);
        record.setUuid(uuid);
        record.setTest(true);
        record.setVersion(1);
        record.setFormId(1);
        record.setStartLatitude(0D);
        record.setStartLongitude(0D);
        record.setDateCreated(new Date().getTime());
        record.setDateUpdated(new Date().getTime());
        record.setDateCompleted(new Date().getTime());
        record.setDateUploaded(new Date().getTime());

        when(userService.findById(survey.getUserId())).thenReturn(user);
        when(recordRepository.save(record)).thenReturn(record);

        UploadResult result = uploadService.uploadRecord(form, record, UNIQUE_NAME_MILESTONE);

        Mockito.verify(recordRepository, Mockito.times(CALLED_ONCE)).findByUuid(uuid);
        Mockito.verify(recordRepository, Mockito.times(CALLED_ONCE)).save(record);
        Assert.assertThat(result.getCode(), is(UploadResult.SUCCESS));
    }

    @Test
    public void uploadRecordShouldReturnInsufficientFundsIfUserDoesNotHaveEnoughCredits() {
        FormService formService = mock(FormService.class);
        UserService userService = mock(UserService.class);
        UploadService uploadService = new UploadService(userService, formService, creditTransactionService, recordRepository, applicationEventPublisher);
        Survey survey = new Survey();
        survey.setCode("SVY");
        Form form = new Form();
        form.setId(1);
        form.setSurvey(survey);
        User user = new User();
        user.setId(1);
        user.setSlaveStatus(SlaveStatus.INACTIVE);
        user.setTenantUserId(user.getId());
        survey.setUserId(user.getTenantUserId());
        String uuid = UUID.randomUUID().toString();
        MainRecord record = new MainRecord();
        record.setFormId(1);
        record.setUuid(uuid);
        record.setVersion(1);
        int credits = 100;
        when(userService.findById(survey.getUserId())).thenReturn(user);
        when(formService.computeCredits(any(Form.class))).thenReturn(credits);

        UploadResult result = uploadService.uploadRecord(form, record, UNIQUE_NAME_MILESTONE);

        Assert.assertThat(result.getCode(), is(UploadResult.INSUFFICIENT_CREDITS));
    }

    @Test
    public void uploadRecordShouldCallExpenseWithOwnerIfOwnerIsNotSlave() {
        FormService formService = mock(FormService.class);
        UserService userService = mock(UserService.class);
        UploadService uploadService = new UploadService(userService, formService, creditTransactionService, recordRepository, applicationEventPublisher);
        Survey survey = new Survey();
        survey.setId(1);
        survey.setCode("SVY");
        Form form = new Form(1);
        form.setSurvey(survey);
        survey.setForms(Collections.singletonList(form));
        User owner = new User();
        owner.setId(1);
        owner.setSlaveStatus(SlaveStatus.INACTIVE);
        owner.setTenantUserId(owner.getId());
        User payer = new User();
        payer.setId(2);
        payer.setMaster(true);
        owner.setMasterUser(payer);
        survey.setUserId(owner.getTenantUserId());
        String uuid = UUID.randomUUID().toString();
        MainRecord record = new MainRecord();
        record.setUuid(uuid);
        record.setTest(false);
        when(userService.findById(survey.getUserId())).thenReturn(owner);

        uploadService.uploadRecord(form, record, UNIQUE_NAME_MILESTONE);

        Mockito.verify(creditTransactionService, Mockito.times(CALLED_ONCE))
            .expense(eq(owner), eq(form), eq(record.getUuid()), any(Date.class), any(Integer.class));
    }

    @Test
    public void uploadRecordShouldCallExpenseWithPayerIfOwnerIsSlave() {
        FormService formService = mock(FormService.class);
        UserService userService = mock(UserService.class);
        UploadService uploadService = new UploadService(userService, formService, creditTransactionService, recordRepository, applicationEventPublisher);
        Survey survey = new Survey();
        survey.setId(1);
        survey.setCode("SVY");
        Form form = new Form(1);
        form.setSurvey(survey);
        survey.setForms(Collections.singletonList(form));
        User owner = new User();
        owner.setId(1);
        owner.setSlaveStatus(SlaveStatus.ACTIVE);
        owner.setTenantUserId(owner.getId());
        User payer = new User();
        payer.setId(2);
        payer.setMaster(true);
        owner.setMasterUser(payer);
        survey.setUserId(owner.getTenantUserId());
        String uuid = UUID.randomUUID().toString();
        MainRecord record = new MainRecord();
        record.setUuid(uuid);
        record.setTest(false);
        when(userService.findById(survey.getUserId())).thenReturn(owner);

        uploadService.uploadRecord(form, record, UNIQUE_NAME_MILESTONE);

        Mockito.verify(creditTransactionService, Mockito.times(CALLED_ONCE))
            .expense(eq(payer), eq(form), eq(record.getUuid()), any(Date.class), any(Integer.class));
    }

    @Test
    public void uploadRecordShouldNotCallExpenseIfOwnerHasNoMaster() {
        FormService formService = mock(FormService.class);
        UserService userService = mock(UserService.class);
        UploadService uploadService = new UploadService(userService, formService, creditTransactionService, recordRepository, applicationEventPublisher);
        Survey survey = new Survey();
        survey.setId(1);
        survey.setCode("SVY");
        Form form = new Form(1);
        form.setSurvey(survey);
        survey.setForms(Collections.singletonList(form));
        User owner = new User();
        owner.setId(1);
        owner.setSlaveStatus(SlaveStatus.ACTIVE);
        owner.setTenantUserId(owner.getId());
        User payer = new User();
        payer.setId(2);
        payer.setMaster(true);
        survey.setUserId(owner.getTenantUserId());
        String uuid = UUID.randomUUID().toString();
        MainRecord record = new MainRecord();
        record.setUuid(uuid);
        record.setTest(false);
        when(userService.findById(survey.getUserId())).thenReturn(owner);

        uploadService.uploadRecord(form, record, UNIQUE_NAME_MILESTONE);

        Mockito.verify(creditTransactionService, Mockito.times(CALLED_ZERO))
            .expense(eq(payer), eq(form), eq(record.getUuid()), any(Date.class), any(Integer.class));
    }

    @Test
    public void uploadRecordShouldNotCallExpenseIfOwnerMasterIsNotMaster() {
        FormService formService = mock(FormService.class);
        UserService userService = mock(UserService.class);
        UploadService uploadService = new UploadService(userService, formService, creditTransactionService, recordRepository, applicationEventPublisher);
        Survey survey = new Survey();
        survey.setId(1);
        survey.setCode("SVY");
        Form form = new Form(1);
        form.setSurvey(survey);
        survey.setForms(Collections.singletonList(form));
        User owner = new User();
        owner.setId(1);
        owner.setSlaveStatus(SlaveStatus.ACTIVE);
        owner.setTenantUserId(owner.getId());
        User payer = new User();
        payer.setId(2);
        payer.setMaster(false);
        owner.setMasterUser(payer);
        survey.setUserId(owner.getTenantUserId());
        String uuid = UUID.randomUUID().toString();
        MainRecord record = new MainRecord();
        record.setUuid(uuid);
        record.setTest(false);
        when(userService.findById(survey.getUserId())).thenReturn(owner);

        uploadService.uploadRecord(form, record, UNIQUE_NAME_MILESTONE);

        Mockito.verify(creditTransactionService, Mockito.times(CALLED_ZERO))
            .expense(eq(payer), eq(form), eq(record.getUuid()), any(Date.class), any(Integer.class));
    }
}