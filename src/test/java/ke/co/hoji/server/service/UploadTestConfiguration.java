package ke.co.hoji.server.service;

import ke.co.hoji.server.dao.model.JdbcChoiceDao;
import ke.co.hoji.server.dao.model.JdbcChoiceGroupDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * Created by geoffreywasilwa on 27/04/2017.
 */
@Configuration
@ComponentScan(
        basePackageClasses = ChoiceGroupUploadService.class,
        useDefaultFilters = false,
        includeFilters = {
            @ComponentScan.Filter( type = FilterType.ASSIGNABLE_TYPE, value = ChoiceGroupUploadService.class)
        })
public class UploadTestConfiguration {
    @Autowired
    private Environment environment;

    @Bean
    public DataSource dataSource() {
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        EmbeddedDatabase db = builder
                .setType(EmbeddedDatabaseType.H2)
                .addScript("create-db.sql")
                .build();
        return db;
    }

    @Bean
    public JdbcTemplate template() {
        return new JdbcTemplate(dataSource());
    }


    @Bean
    public JdbcChoiceDao choiceDao() {
        return new JdbcChoiceDao(template());
    }


    @Bean
    public JdbcChoiceGroupDao choiceGroupDao() {
        JdbcChoiceGroupDao choiceGroupDao = new JdbcChoiceGroupDao(template(), choiceDao());
        return choiceGroupDao;
    }

}
