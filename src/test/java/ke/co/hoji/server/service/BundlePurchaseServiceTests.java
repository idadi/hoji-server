package ke.co.hoji.server.service;

import ke.co.hoji.server.model.CreditBundle;
import ke.co.hoji.server.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.when;

/**
 * Created by gitahi on 05/01/2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class BundlePurchaseServiceTests {

    private final User user = getUser();
    private final Date date = new Date();
    private final String paymentMode = "Paybill (M-PESA)";
    private final String paymentReference = "12345";
    private final BigDecimal cash = new BigDecimal(400);

    @Mock
    private CashTransactionService cashTransactionService;
    @Mock
    private CreditTransactionService creditTransactionService;

    @Test
    public void purchaseBundleShouldDepositCashExpenseCashAndDepositCreditsIfBalanceIsSufficient() throws Exception {
        BigDecimal cashBalance = new BigDecimal(99);
        CreditBundle creditBundle = getCreditBundle(0, 0);

        when(cashTransactionService.balance(eq(user))).thenReturn(cashBalance);


        BundlePurchaseService bundlePurchaseService = new BundlePurchaseService();
        bundlePurchaseService.setCashTransactionService(cashTransactionService);
        bundlePurchaseService.setCreditTransactionService(creditTransactionService);
        bundlePurchaseService.purchaseBundle(user, creditBundle, new Date(), paymentMode, paymentReference, cash);

        Mockito.verify(cashTransactionService, Mockito.times(1))
                .deposit(eq(user), any(String.class), refEq(date), eq(cash), eq(paymentMode), eq(paymentReference));
        Mockito.verify(cashTransactionService, Mockito.times(1))
                .expense(eq(user), any(String.class), refEq(date), eq(creditBundle.getPrice()));
        Mockito.verify(creditTransactionService, Mockito.times(1))
                .deposit(eq(user), any(String.class), refEq(date), refEq(creditBundle.getCredits()), eq(creditBundle.getExpiry()));
    }

    @Test
    public void purchaseBundleShouldOnlyDepositCashIfBalanceIsInsufficient() throws Exception {
        BigDecimal cashBalance = new BigDecimal(98);
        CreditBundle creditBundle = getCreditBundle(0, 0);

        when(cashTransactionService.balance(eq(user))).thenReturn(cashBalance);


        BundlePurchaseService bundlePurchaseService = new BundlePurchaseService();
        bundlePurchaseService.setCashTransactionService(cashTransactionService);
        bundlePurchaseService.setCreditTransactionService(creditTransactionService);
        bundlePurchaseService.purchaseBundle(user, creditBundle, new Date(), paymentMode, paymentReference, cash);

        Mockito.verify(cashTransactionService, Mockito.times(1))
                .deposit(eq(user), any(String.class), refEq(date), eq(cash), eq(paymentMode), eq(paymentReference));
        Mockito.verify(cashTransactionService, Mockito.times(0))
                .expense(eq(user), any(String.class), refEq(date), eq(creditBundle.getPrice()));
        Mockito.verify(creditTransactionService, Mockito.times(0))
                .deposit(eq(user), any(String.class), refEq(date), refEq(creditBundle.getCredits()), eq(creditBundle.getExpiry()));
    }

    @Test
    public void purchaseBundleShouldExpenseCashEquivalentToDiscountedPrice() throws Exception {
        final BigDecimal tenPercentDiscountedPrice = new BigDecimal("449.1");

        BigDecimal cashBalance = new BigDecimal(99);
        CreditBundle creditBundle = getCreditBundle(10, 0);

        when(cashTransactionService.balance(eq(user))).thenReturn(cashBalance);


        BundlePurchaseService bundlePurchaseService = new BundlePurchaseService();
        bundlePurchaseService.setCashTransactionService(cashTransactionService);
        bundlePurchaseService.setCreditTransactionService(creditTransactionService);
        bundlePurchaseService.purchaseBundle(user, creditBundle, new Date(), paymentMode, paymentReference, cash);

        Mockito.verify(cashTransactionService, Mockito.times(1))
                .expense(eq(user), any(String.class), refEq(date), eq(tenPercentDiscountedPrice));
    }

    @Test
    public void purchaseBundleShouldDepositCreditsWithBonusIncluded() throws Exception {
        final Integer tenPercentBonusCredits = 2200;

        BigDecimal cashBalance = new BigDecimal(99);
        CreditBundle creditBundle = getCreditBundle(0, 10);

        when(cashTransactionService.balance(eq(user))).thenReturn(cashBalance);


        BundlePurchaseService bundlePurchaseService = new BundlePurchaseService();
        bundlePurchaseService.setCashTransactionService(cashTransactionService);
        bundlePurchaseService.setCreditTransactionService(creditTransactionService);
        bundlePurchaseService.purchaseBundle(user, creditBundle, new Date(), paymentMode, paymentReference, cash);

        Mockito.verify(creditTransactionService, Mockito.times(1))
                .deposit(eq(user), any(String.class), refEq(date), refEq(tenPercentBonusCredits), eq(creditBundle.getExpiry()));
    }

    private User getUser() {
        User user = new User();
        user.setId(1);
        return user;
    }

    private CreditBundle getCreditBundle(Integer discount, Integer bonus) {
        CreditBundle creditBundle = new CreditBundle();
        creditBundle.setCredits(2000);
        creditBundle.setPrice(new BigDecimal(499));
        creditBundle.setDiscount(discount);
        creditBundle.setBonus(bonus);
        creditBundle.setExpiry(12);
        return creditBundle;
    }
}