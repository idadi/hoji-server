package ke.co.hoji.server.service;

import static ke.co.hoji.core.data.Constants.Device.CODE_TO_ID_MILESTONE;
import static ke.co.hoji.core.data.http.SurveyResponse.Code.APP_VERSION_TOO_OLD;
import static ke.co.hoji.core.data.http.SurveyResponse.Code.SURVEY_NOT_ENABLED;
import static ke.co.hoji.core.data.http.SurveyResponse.Code.SURVEY_NOT_PUBLISHED;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import ke.co.hoji.core.data.PublicSurveyAccess;
import ke.co.hoji.core.data.dto.Survey;
import ke.co.hoji.core.data.http.SurveyResponse;
import ke.co.hoji.core.data.http.SurveysResponse;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.server.dao.dto.JdbcFieldDao;
import ke.co.hoji.server.dao.dto.JdbcFormDao;
import ke.co.hoji.server.dao.dto.JdbcLanguageDao;
import ke.co.hoji.server.dao.dto.JdbcReferenceDao;
import ke.co.hoji.server.dao.dto.JdbcSurveyDao;
import ke.co.hoji.server.dao.dto.JdbcTranslationDao;
import ke.co.hoji.server.model.User;

/**
 * Created by geoffreywasilwa on 20/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class DownloadServiceTests {

    private static final int PUBLIC_SURVEY_OWNER_ID = 1;
    private static final int CURRENT_OWNER_ID = 2;

    @Mock
    private JdbcSurveyDao surveyDao;

    @Mock
    private UserService userService;

    @Mock
    private JdbcFormDao formDao;

    @Mock
    private JdbcFieldDao fieldDao;

    @Mock
    private JdbcReferenceDao referenceDao;

    @Mock
    private PropertyService propertyService;

    @Mock
    private JdbcLanguageDao languageDao;

    @Mock
    private JdbcTranslationDao translationDao;

    private DownloadService downloadService;

    @Before
    public void setUp() throws Exception {
        downloadService = new DownloadService(
                surveyDao,
                formDao,
                fieldDao,
                referenceDao,
                languageDao,
                propertyService,
                translationDao,
                userService
        );
    }

    @Test
    public void responseShouldHaveAllSurveysIncludingPublicOnesWhenAccessSetToAll() throws Exception {
        Survey publicSurvey = new Survey();
        publicSurvey.setId(1);
        publicSurvey.setUserId(PUBLIC_SURVEY_OWNER_ID);
        User pSurveyOwner = new User();
        pSurveyOwner.setId(PUBLIC_SURVEY_OWNER_ID);
        publicSurvey.setUserId(PUBLIC_SURVEY_OWNER_ID);
        Survey ownedSurvey = new Survey();
        ownedSurvey.setId(2);
        ownedSurvey.setUserId(CURRENT_OWNER_ID);
        User currentUser = new User();
        currentUser.setId(CURRENT_OWNER_ID);
        currentUser.setPublicSurveyAccess(PublicSurveyAccess.ALL);
        when(surveyDao.getPublicSurveys(anyInt())).thenReturn(Collections.singletonList(publicSurvey));
        List<Survey> ownedSurveys = new ArrayList<>();
        ownedSurveys.add(ownedSurvey);
        when(surveyDao.getSurveysByTenantUserId(anyInt(), anyBoolean())).thenReturn(ownedSurveys);
        when(userService.findById(PUBLIC_SURVEY_OWNER_ID)).thenReturn(pSurveyOwner);
        when(userService.findById(CURRENT_OWNER_ID)).thenReturn(currentUser);

        SurveysResponse response = downloadService.getSurveys(currentUser);

        Assert.assertThat(response.getSurveyResponses(), hasSize(2));
    }

    @Test
    public void responseShouldOnlyHaveOwnedSurveysWhenAccessSetToNone() throws Exception {
        Survey ownedSurvey = new Survey();
        ownedSurvey.setId(2);
        ownedSurvey.setUserId(CURRENT_OWNER_ID);
        User currentUser = new User();
        currentUser.setId(CURRENT_OWNER_ID);
        currentUser.setPublicSurveyAccess(PublicSurveyAccess.NONE);
        List<Survey> ownedSurveys = new ArrayList<>();
        ownedSurveys.add(ownedSurvey);
        when(surveyDao.getSurveysByTenantUserId(anyInt(), anyBoolean())).thenReturn(ownedSurveys);
        when(userService.findById(CURRENT_OWNER_ID)).thenReturn(currentUser);

        SurveysResponse response = downloadService.getSurveys(currentUser);

        Assert.assertThat(response.getSurveyResponses(), hasSize(1));
        Mockito.verify(surveyDao, Mockito.never()).getPublicSurveys(anyInt());
    }

    @Test
    public void shouldReturnSurvey() throws Exception {
        Survey survey = new Survey();
        survey.setUserId(1);
        survey.setEnabled(true);
        survey.setStatus(ke.co.hoji.core.data.model.Survey.PUBLISHED);
        User user = new User();
        user.setId(1);
        user.setFullName("User");
        user.setSuspended(true);
        when(surveyDao.getSurveyById(anyInt())).thenReturn(survey);
        when(userService.findById(anyInt())).thenReturn(user);

        SurveyResponse response = downloadService.downloadSurvey(1, CODE_TO_ID_MILESTONE + 1);

        Assert.assertThat(response.getCode(), is(SurveyResponse.Code.SURVEY_FOUND));
        Assert.assertThat(response.getOwnerId(), is(user.getId()));
        Assert.assertThat(response.getOwnerName(), is(user.getFullName()));
        Assert.assertFalse(response.isOwnerVerified());
    }

    @Test
    public void shouldReturnNotFoundCodeWhenSurveyIsNotFound() throws Exception {
        SurveyResponse response = downloadService.downloadSurvey(null, CODE_TO_ID_MILESTONE + 1);

        Assert.assertThat(response.getCode(), is(SurveyResponse.Code.SURVEY_NOT_FOUND));
    }

    @Test
    public void shouldReturnNotEnabledCodeWhenSurveyIsDisabled() throws Exception {
        Survey disabledSurvey = new Survey();
        when(surveyDao.getSurveyById(anyInt())).thenReturn(disabledSurvey);

        SurveyResponse response = downloadService.downloadSurvey(1, CODE_TO_ID_MILESTONE + 1);

        Assert.assertThat(response.getCode(), is(SURVEY_NOT_ENABLED));
    }

    @Test
    public void shouldReturnNotPublishedCodeWhenSurveyIsNotPublished() throws Exception {
        Survey unPublished = new Survey();
        unPublished.setEnabled(true);
        unPublished.setStatus(ke.co.hoji.core.data.model.Survey.UNPUBLISHED);
        when(surveyDao.getSurveyById(anyInt())).thenReturn(unPublished);

        SurveyResponse response = downloadService.downloadSurvey(1, CODE_TO_ID_MILESTONE + 1);

        Assert.assertThat(response.getCode(), is(SURVEY_NOT_PUBLISHED));
    }

    @Test
    public void downloadSurvey_shouldRejectRequestFromOlderVersion() {
        Integer surveyId = 1;
        Integer appVersion = CODE_TO_ID_MILESTONE;

        SurveyResponse response = downloadService.downloadSurvey(surveyId, appVersion);

        Assert.assertThat(response.getCode(), is(APP_VERSION_TOO_OLD));
    }

    @Test
    public void downloadSurvey_shouldRejectRequestWithoutVersion() {
        Integer surveyId = 1;

        SurveyResponse response = downloadService.downloadSurvey(surveyId, null);

        Assert.assertThat(response.getCode(), is(APP_VERSION_TOO_OLD));
    }

}