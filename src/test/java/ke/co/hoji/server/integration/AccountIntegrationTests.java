package ke.co.hoji.server.integration;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class AccountIntegrationTests extends BaseIntegrationTests {

    private static  final String WEB_PATH = "http://localhost:" + PORT + CONTEXT_PATH;

    @Test
    public void login() {
        assertThat(driver.getCurrentUrl(), is(WEB_PATH));
    }

}
