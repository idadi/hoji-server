package ke.co.hoji.server.integration.test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.bean.DatabaseConfigBean;
import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;
import com.mongodb.MongoClientURI;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.core.service.model.RecordService;
import ke.co.hoji.server.repository.MainRecordRepository;
import org.dbunit.database.DatabaseDataSourceConnection;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.io.IOException;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.IsNull.notNullValue;

/**
 * Created by geoffreywasilwa on 31/05/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { RepositoryTestConfiguration.class, RecordServiceIntegrationTests.MongoTestConfiguration.class })
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DbUnitConfiguration(databaseConnection = "dbUnitDatabaseConnection")
@DatabaseSetup("project_dataset.xml")
public class RecordServiceIntegrationTests {

    private static InMemoryMongoDbManager manager =
            new InMemoryMongoDbManager("mainRecord_2.json", 54321, "mainRecord");

    @Autowired
    @Qualifier("serverRecordService")
    private RecordService recordService;

    @Autowired
    private ConfigService configService;

    @BeforeClass
    public static void setup() throws IOException {
        manager.start();
    }

    @Test
    public void findRecordByUuid_shouldReturnLinkedRecord() throws Exception {
        Field field = configService.getField(13);
        Record record = recordService.findRecordByUuid("1657b1e6-50ce-4554-ab98-0401797131c9", field);

        Assert.assertThat(record, notNullValue());
        Assert.assertThat(record.getLiveFields(), hasSize(2));
    }

    @Test
    public void findRecordByUuid_shouldReturnLinkedRecordWhenLinkedFromChildFieldOfMatrix() throws Exception {
        Field field = configService.getField(14);
        Record record = recordService.findRecordByUuid("1657b1e6-50ce-4554-ab98-0401797131c9", field);

        Assert.assertThat(record, notNullValue());
        Assert.assertThat(record.getLiveFields(), hasSize(2));
    }

    @AfterClass
    public static void tearDown() {
        manager.stop();
    }

    @Configuration
    @EnableMongoRepositories(basePackageClasses = { MainRecordRepository.class })
    static class MongoTestConfiguration {

        @Bean
        public DataSource dataSource() {
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            return builder
                    .setType(EmbeddedDatabaseType.H2)
                    .setName("testdb_2")
                    .addScript("create-db.sql")
                    .build();
        }

        @Bean
        public DatabaseConfigBean databaseConfig() {
            return new DatabaseConfigBean();
        }

        @Bean
        public DatabaseDataSourceConnection dbUnitDatabaseConnection() throws Exception {
            DatabaseDataSourceConnectionFactoryBean dataSourceConnectionFactoryBean =
                    new DatabaseDataSourceConnectionFactoryBean();
            dataSourceConnectionFactoryBean.setDataSource(dataSource());
            dataSourceConnectionFactoryBean.setDatabaseConfig(databaseConfig());
            return dataSourceConnectionFactoryBean.getObject();
        }

        @Bean
        public MongoDbFactory mongoDbFactory() throws Exception {
            return new SimpleMongoDbFactory(new MongoClientURI("mongodb://localhost:54321/hoji_test"));
        }

        @Bean
        public MongoTemplate mongoTemplate() throws Exception {
            return new MongoTemplate(mongoDbFactory());
        }
    }

}