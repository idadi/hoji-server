package ke.co.hoji.server.integration.test;

import de.flapdoodle.embed.mongo.Command;
import de.flapdoodle.embed.mongo.MongoImportProcess;
import de.flapdoodle.embed.mongo.MongoImportStarter;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongoImportConfig;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongoImportConfigBuilder;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.config.RuntimeConfigBuilder;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * Created by geoffreywasilwa on 26/05/2017.
 */
class InMemoryMongoDbManager {

    private static final MongodStarter starter = MongodStarter.getDefaultInstance();
    private static final Logger logger = LoggerFactory.getLogger(InMemoryMongoDbManager.class);
    private static final MongoImportStarter importStarter =
            MongoImportStarter.getInstance(
                    new RuntimeConfigBuilder().defaultsWithLogger(Command.MongoImport, logger).build()
            );
    private final String fileName;
    private final int port;
    private final String collection;

    InMemoryMongoDbManager(String fileName, int port, String collection) {
        this.fileName = fileName;
        this.port = port;
        this.collection = collection;
    }

    private MongodProcess mongod;
    private MongoImportProcess mongoImport;

    void start() throws IOException {
        mongod = startMongod();
        mongoImport = startMongoImport();
    }

    void stop() {
        mongod.stop();
        mongoImport.stop();
    }

    private MongoImportProcess startMongoImport()
            throws IOException {
        String jsonFile=Thread.currentThread().getContextClassLoader().getResource(fileName).toString();
        jsonFile=jsonFile.replaceFirst("file:","");
        IMongoImportConfig mongoImportConfig = new MongoImportConfigBuilder()
                .version(Version.Main.PRODUCTION)
                .net(new Net("localhost", port, Network.localhostIsIPv6()))
                .db("hoji_test")
                .collection(collection)
                .upsert(true)
                .dropCollection(true)
                .jsonArray(true)
                .importFile(jsonFile)
                .build();
        return importStarter.prepare(mongoImportConfig).start();
    }

    private MongodProcess startMongod() throws UnknownHostException, IOException {
        IMongodConfig mongoConfigConfig = new MongodConfigBuilder()
                .version(Version.Main.PRODUCTION)
                .net(new Net("localhost", port, Network.localhostIsIPv6()))
                .build();

        MongodExecutable mongodExecutable = starter.prepare(mongoConfigConfig);
        return mongodExecutable.start();
    }

}
