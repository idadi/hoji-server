package ke.co.hoji.server.integration;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.openqa.selenium.By.className;
import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.linkText;
import static org.openqa.selenium.By.name;
import static org.openqa.selenium.By.tagName;
import static org.openqa.selenium.By.xpath;
import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOf;
import static org.openqa.selenium.support.ui.ExpectedConditions.stalenessOf;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProjectIntegrationTests extends BaseIntegrationTests {

    @Test
    public void shouldCreateNewProject() throws InterruptedException {
        driver.navigate().to(WEB_PATH);
        String projectName = "Network Maintenance[**added**]";
        driver.findElement(id("add-button")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        WebElement editForm = driver.findElement(id("edit-form"));
        driver.findElement(name("name")).sendKeys(projectName);
        driver.findElement(className("edit-submit")).click();
        wait.until(invisibilityOf(editForm));

        String projectNames = driver.findElement(tagName("tbody")).getText();
        Assert.assertTrue(projectNames.contains(projectName));
    }

    @Test
    public void shouldNavigateToProjectViewPageFormListPage() throws InterruptedException {
        driver.navigate().to(WEB_PATH);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement projectLink = driver
                .findElement(xpath("//a[text()='Network Maintenance']"));
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(visibilityOf(projectLink));
        js.executeScript("arguments[0].click()", projectLink);

        assertThat(driver.getCurrentUrl(), containsString(WEB_PATH + "project/view/"));
    }

    @Test
    public void shouldDisableAndEnableProject() {
        driver.navigate().to(WEB_PATH + "project/view/2/form");
        driver.findElement(className("more-actions")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        WebElement enable = driver.findElement(className("enable-project"));
        WebElement projectName = driver.findElement(id("project-name"));
        wait.until(visibilityOf(driver.findElement(className("manage-properties"))));
        enable.click();
        wait.until(stalenessOf(projectName));

        assertThat(driver.findElement(id("project-name")).getAttribute("class"), containsString("disabled"));

        driver.findElement(className("more-actions")).click();
        enable = driver.findElement(className("enable-project"));
        projectName = driver.findElement(id("project-name"));
        wait.until(visibilityOf(driver.findElement(id("manage-properties"))));
        enable.click();
        wait.until(stalenessOf(projectName));

        assertThat(driver.findElement(id("project-name")).getAttribute("class"), not(containsString("disabled")));
    }

    @Test
    public void shouldUpdateProjectName() {
        driver.navigate().to(WEB_PATH + "project/view/2/form");
        driver.findElement(className("more-actions")).click();
        driver.findElement(className("edit-project")).click();
        String updatedProjectName = "Network Maintenance [**updated**]";
        WebElement nameInput = driver.findElement(name("name"));
        nameInput.clear();
        nameInput.sendKeys(updatedProjectName);
        driver.findElement(className("edit-submit")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(invisibilityOf(driver.findElement(id("edit-form"))));

        assertThat(driver.findElement(id("project-name")).getText(), is(updatedProjectName));
        // assertThatProjectIsModified(2);
    }

    @Test
    public void shouldNavigateToProjectsListPageUsingBreadcrumb() {
        driver.navigate().to(WEB_PATH + "project/view/2/form");
        driver.findElement(cssSelector(".ui.breadcrumb")).findElement(linkText("Projects")).click();

        assertThat(driver.getCurrentUrl(), is(WEB_PATH));
    }

}
