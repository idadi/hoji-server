package ke.co.hoji.server.integration;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.openqa.selenium.By.className;
import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.linkText;
import static org.openqa.selenium.By.name;
import static org.openqa.selenium.By.tagName;
import static org.openqa.selenium.By.xpath;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOf;
import static org.openqa.selenium.support.ui.ExpectedConditions.numberOfElementsToBeMoreThan;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.io.Resources;

public class ChoiceGroupIntegrationTests extends BaseIntegrationTests {

    @Test
    public void shouldNavigateToChoiceGroupPage() {
        driver.get(WEB_PATH);
        driver.findElement(linkText("Choice Groups")).click();

        assertThat(driver.getCurrentUrl(), containsString("/choice-group"));
    }

    @Test
    public void shouldAddChoiceGroup() {
        driver.navigate().to(WEB_PATH + "choice-group");
        WebElement addButton = driver.findElement(className("add"));
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(elementToBeClickable(addButton));
        addButton.click();
        WebElement editModal = driver.findElement(cssSelector(".ui.modal"));
        wait.until(visibilityOf(editModal));
        String groupName = "Yes/No[**added**]";
        editModal.findElement(name("name")).sendKeys(groupName);
        WebElement searchInput = editModal.findElement(cssSelector("div[name='choices'].search input"));
        searchInput.sendKeys("Yes");
        searchInput.sendKeys(Keys.ENTER);
        editModal.findElement(className("edit-submit")).click();
        wait.until(invisibilityOf(editModal));

        assertThat(driver.findElement(tagName("tbody")).getText(), containsString(groupName));
    }

    @Test
    public void shouldEditChoiceGroup() {
        driver.navigate().to(WEB_PATH + "choice-group");
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(numberOfElementsToBeMoreThan(tagName("tr"), 2));
        WebElement groupToEdit = driver.findElement(linkText("Yes/No[**edit**]"));
        wait.until(elementToBeClickable(groupToEdit));
        groupToEdit.click();
        driver.findElement(className("more-actions")).click();
        driver.findElement(className("edit")).click();
        WebElement editModal = driver.findElement(cssSelector(".ui.modal"));
        wait.until(visibilityOf(editModal));
        editModal.findElement(cssSelector("div[name='orderingStrategy']")).click();
        editModal.findElement(xpath("//div[@class='item' and span/text()='Alphabetical']")).click();
        WebElement searchInput = editModal.findElement(cssSelector("div[name='choices'].search input"));
        searchInput.sendKeys("Yes");
        searchInput.sendKeys(Keys.ENTER);
        searchInput.sendKeys("No");
        searchInput.sendKeys(Keys.ARROW_DOWN);
        searchInput.sendKeys(Keys.ENTER);
        editModal.findElement(className("edit-submit")).click();
        wait.until(invisibilityOf(editModal));

        assertThat(driver.findElement(className("ordering-strategy")).getText(), is("Alphabetical"));
        assertThat(driver.findElement(cssSelector(".choice.table tbody")).getText(), containsString("Yes"));
        assertThat(driver.findElement(cssSelector(".choice.table tbody")).getText(), containsString("No"));
    }

    @Test
    public void shouldDeleteChoiceGroup() {
        driver.navigate().to(WEB_PATH + "choice-group");
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(numberOfElementsToBeMoreThan(tagName("tr"), 2));
        driver.findElement(linkText("Yes/No[**delete**]")).click();
        driver.findElement(className("more-actions")).click();
        driver.findElement(className("delete")).click();
        WebElement deleteModal = driver.findElement(cssSelector(".ui.modal"));
        wait.until(visibilityOf(deleteModal));
        deleteModal.findElement(className("delete-submit")).click();
        wait.until(invisibilityOf(deleteModal));

        assertThat(driver.getCurrentUrl(), endsWith("/choice-group"));
        assertThat(driver.findElement(tagName("tbody")).getText(), not(containsString("Yes[**delete**]")));
    }

    @Test
    public void shouldRemoveChoiceFromGroup() {
        driver.navigate().to(WEB_PATH + "choice-group");
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(numberOfElementsToBeMoreThan(tagName("tr"), 2));
        driver.findElement(linkText("Yes/No[**remove choice**]")).click();
        WebElement choiceToRemove = driver.findElement(xpath("//tr[td = 'No']"));
        choiceToRemove.findElement(className("remove")).click();
        WebElement removeModal = driver.findElement(cssSelector(".ui.modal"));
        wait.until(visibilityOf(removeModal));
        removeModal.findElement(className("delete-submit")).click();
        wait.until(invisibilityOf(removeModal));

        assertThat(driver.findElement(cssSelector(".choice.table tbody")).getText(), containsString("Yes"));
        assertThat(driver.findElement(cssSelector(".choice.table tbody")).getText(), not(containsString("No")));
    }

    @Test
    public void shouldCreateGroupFromChoiceUpload() {
        driver.navigate().to(WEB_PATH + "choice");
        WebDriverWait wait = new WebDriverWait(driver, 2);
        driver.findElement(className("upload")).click();
        WebElement uploadModal = driver.findElement(cssSelector(".ui.modal"));
        wait.until(visibilityOf(uploadModal));
        String uploadFilePath = Resources.getResource("upload-choices.csv").getPath();
        uploadModal.findElement(cssSelector("input[type='file']")).sendKeys(uploadFilePath);
        wait.until(visibilityOf(uploadModal));
        uploadModal.findElement(className("upload-submit")).click();
        wait.until(invisibilityOf(uploadModal));
        driver.navigate().to(WEB_PATH + "choice-group");

        assertThat(driver.findElement(tagName("tbody")).getText(), containsString("Yes/No[**uploaded**]"));
    }

    @Test
    public void shouldCloseEditChoiceGroupModalOnEscape() {
        driver.navigate().to(WEB_PATH + "choice-group");
        WebElement addButton = driver.findElement(className("add"));
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(elementToBeClickable(addButton));
        addButton.click();
        wait.until(visibilityOf(driver.findElement(cssSelector(".ui.modal"))));
        driver.findElement(tagName("body")).sendKeys(Keys.ESCAPE);

        assertThat(driver.findElements(cssSelector(".ui.modal")), hasSize(0));
    }

    @Test
    public void shouldCloseDeleteChoiceGroupModalOnEscape() {
        driver.navigate().to(WEB_PATH + "choice-group");
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(numberOfElementsToBeMoreThan(tagName("tr"), 2));
        driver.findElement(linkText("Yes/No[**edit**]")).click();
        driver.findElement(className("more-actions")).click();
        driver.findElement(className("delete")).click();
        WebElement deleteModal = driver.findElement(cssSelector(".ui.modal"));
        wait.until(visibilityOf(deleteModal));
        driver.findElement(tagName("body")).sendKeys(Keys.ESCAPE);

        assertThat(driver.findElements(cssSelector(".ui.modal")), hasSize(0));
    }

    @Test
    public void shouldCloseRemoveChoiceModalOnEscape() {
        driver.navigate().to(WEB_PATH + "choice-group");
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(numberOfElementsToBeMoreThan(tagName("tr"), 2));
        driver.findElement(linkText("Yes/No[**remove choice**]")).click();
        WebElement choiceToRemove = driver.findElement(xpath("//tr[td = 'Yes']"));
        choiceToRemove.findElement(className("remove")).click();
        WebElement removeModal = driver.findElement(cssSelector(".ui.modal"));
        wait.until(visibilityOf(removeModal));
        driver.findElement(tagName("body")).sendKeys(Keys.ESCAPE);

        assertThat(driver.findElements(cssSelector(".ui.modal")), hasSize(0));
    }

}
