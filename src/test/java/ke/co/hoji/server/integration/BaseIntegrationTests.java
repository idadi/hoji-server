package ke.co.hoji.server.integration;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BaseIntegrationTests {

    static final int PORT = 8999;
    static final String CONTEXT_PATH = "/";
    protected static final String WEB_PATH = "http://localhost:" + PORT + CONTEXT_PATH;
    static WebDriver driver;

    @BeforeClass
    public static void setup() {
        JettyServer.init(PORT, CONTEXT_PATH);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(WEB_PATH + "signIn");
        driver.findElement(By.name("username")).sendKeys("test.user@hoji.co.ke");
        driver.findElement(By.name("password")).sendKeys("t3s4.Us3r");
        driver.findElement(By.className("edit-submit")).click();
        new WebDriverWait(driver, 2).until(ExpectedConditions.urlToBe(WEB_PATH));
    }

    @AfterClass
    public static void cleanup() {
        driver.close();
    }

    protected void assertThatProjectIsModified(int projectId) {
        driver.navigate().to(WEB_PATH + "project/view/" + projectId + "/form");
        try {
            assertThat(driver.findElement(By.id("project-status")).getText(), containsString("Draft"));
        } catch (NoSuchElementException e) {
            fail("Project was not marked as 'MODIFIED'");
        }
    }

}
