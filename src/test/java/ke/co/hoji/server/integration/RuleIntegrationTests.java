package ke.co.hoji.server.integration;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.By.className;
import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.xpath;
import static org.openqa.selenium.support.ui.ExpectedConditions.and;
import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOf;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RuleIntegrationTests extends BaseIntegrationTests {

    @Test
    public void shouldDisplayIfShowRules() {
        driver.navigate().to(WEB_PATH + "field/view/65");

        assertThat(driver.findElements(cssSelector(".rule.table tr")), hasSize(1));
    }

    @Test
    public void shouldDisplayShowIfRules() {
        driver.navigate().to(WEB_PATH + "field/view/70");

        assertThat(driver.findElements(cssSelector(".rule.table tr")), hasSize(1));
    }

    @Test
    public void shouldAddIfShowRule() {
        driver.navigate().to(WEB_PATH + "field/view/58");
        driver.findElement(className("more-actions")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(visibilityOf(driver.findElement(className("add"))));
        driver.findElement(className("add")).click();
        wait.until(visibilityOf(driver.findElement(id("edit-form"))));
        WebElement operatorInput = driver.findElement(cssSelector(".operator input.search"));
        operatorInput.click();
        operatorInput.sendKeys("Is equal");
        operatorInput.sendKeys(Keys.TAB);
        WebElement valueInput = driver.findElement(cssSelector(".rule-value input.search"));
        valueInput.click();
        valueInput.sendKeys("No");
        valueInput.sendKeys(Keys.TAB);
        WebElement targetInput = driver.findElement(cssSelector(".target input.search"));
        targetInput.click();
        targetInput.sendKeys("The end");
        targetInput.sendKeys(Keys.TAB);
        driver.findElement(className("edit-submit")).click();
        wait.until(invisibilityOf(driver.findElement(id("edit-form"))));

        assertThat(driver.findElements(cssSelector(".rule.table tr")), hasSize(1));
        assertThatProjectIsModified(12);
    }

    @Test
    public void shouldAddShowIfRule() {
        driver.navigate().to(WEB_PATH + "field/view/63");
        driver.findElement(className("more-actions")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(visibilityOf(driver.findElement(className("add"))));
        driver.findElement(className("add")).click();
        wait.until(and(presenceOfElementLocated(id("edit-form")), visibilityOf(driver.findElement(id("edit-form")))));
        WebElement typeInput = driver.findElement(cssSelector("#edit-form .type"));
        typeInput.click();
        WebElement showIfOption = typeInput.findElement(xpath("//div[@data-value='2']"));
        wait.until(visibilityOf(showIfOption));
        showIfOption.click();
        wait.until(visibilityOf(driver.findElement(className("show-if"))));
        WebElement targetInput = driver.findElement(cssSelector(".show-if .target input.search"));
        targetInput.click();
        targetInput.sendKeys("Consent");
        targetInput.sendKeys(Keys.TAB);
        WebElement operatorInput = driver.findElement(cssSelector(".show-if .operator input.search"));
        operatorInput.click();
        operatorInput.sendKeys("Is equal");
        operatorInput.sendKeys(Keys.TAB);
        WebElement valueInput = driver.findElement(cssSelector(".show-if .rule-value input.search"));
        valueInput.click();
        valueInput.sendKeys("No");
        valueInput.sendKeys(Keys.TAB);
        driver.findElement(className("edit-submit")).click();
        wait.until(invisibilityOf(driver.findElement(id("edit-form"))));

        assertThat(driver.findElements(cssSelector(".rule.table tr")), hasSize(1));
        assertThatProjectIsModified(12);
    }

    @Test
    public void shouldEditIfShowRule() {
        driver.navigate().to(WEB_PATH + "field/view/65");
        driver.findElement(cssSelector(".rule.table tr:last-child .edit-rule")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(visibilityOf(driver.findElement(id("edit-form"))));
        WebElement operatorInput = driver.findElement(cssSelector(".operator input.search"));
        operatorInput.click();
        operatorInput.sendKeys("Is not equal");
        WebElement valueInput = driver.findElement(cssSelector(".rule-value input.search"));
        valueInput.click();
        valueInput.sendKeys("Yes");
        valueInput.sendKeys(Keys.TAB);
        WebElement targetInput = driver.findElement(cssSelector(".target input.search"));
        targetInput.click();
        targetInput.sendKeys("Service rating");
        targetInput.sendKeys(Keys.TAB);
        driver.findElement(className("edit-submit")).click();
        wait.until(invisibilityOf(driver.findElement(id("edit-form"))));

        assertThat(driver.findElement(cssSelector(".rule.table tr")).getText(),
                containsString("is not equal to Yes then show Service rating"));
        assertThatProjectIsModified(13);
    }

    @Test
    public void shouldEditShowIfRule() {
        driver.navigate().to(WEB_PATH + "field/view/77");
        driver.findElement(cssSelector(".rule.table tr:last-child .edit-rule")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(visibilityOf(driver.findElement(id("edit-form"))));
        WebElement operatorInput = driver.findElement(cssSelector(".show-if .operator input.search"));
        operatorInput.click();
        operatorInput.sendKeys("Is not equal");
        operatorInput.sendKeys(Keys.TAB);
        WebElement valueInput = driver.findElement(cssSelector(".show-if .rule-value input.search"));
        valueInput.click();
        valueInput.sendKeys("No");
        valueInput.sendKeys(Keys.TAB);
        driver.findElement(className("edit-submit")).click();
        wait.until(invisibilityOf(driver.findElement(id("edit-form"))));

        assertThat(driver.findElement(cssSelector(".rule.table tr")).getText(),
                containsString("if Consent is not equal to No"));
        assertThatProjectIsModified(14);
    }

    @Test
    public void shouldDeleteIfShowRule() {
        driver.navigate().to(WEB_PATH + "field/view/79");
        driver.findElement(cssSelector(".rule.table tr:last-child .delete-rule")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(visibilityOf(driver.findElement(cssSelector(".ui.modal"))));
        driver.findElement(className("delete-submit")).click();
        wait.until(invisibilityOf(driver.findElement(cssSelector(".ui.modal"))));

        assertTrue(driver.findElements(cssSelector(".rule.table")).size() == 0);
        assertThatProjectIsModified(15);
    }

    @Test
    public void shouldDeleteShowIfRule() {
        driver.navigate().to(WEB_PATH + "field/view/91");
        driver.findElement(cssSelector(".rule.table tr:last-child .delete-rule")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(visibilityOf(driver.findElement(cssSelector(".ui.modal"))));
        driver.findElement(className("delete-submit")).click();
        wait.until(invisibilityOf(driver.findElement(cssSelector(".ui.modal"))));

        assertTrue(driver.findElements(cssSelector(".rule.table")).size() == 0);
        assertThatProjectIsModified(16);
    }

}
