package ke.co.hoji.server.integration.test;

import com.mongodb.MongoClientURI;
import ke.co.hoji.server.calculation.CalculationResult;
import ke.co.hoji.server.repository.CalculationResultRepository;
import org.bson.Document;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.index.CompoundIndexDefinition;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { CalculationResultRepositoryIntegrationTests.MongoTestConfiguration.class})
public class CalculationResultRepositoryIntegrationTests {

    @Autowired
    private CalculationResultRepository calculationResultRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    private static InMemoryMongoDbManager manager =
            new InMemoryMongoDbManager("calculationResult.json", 12347, "calculationResult");

    @BeforeClass
    public static void setup() throws IOException {
        manager.start();
    }

    @Before
    public void createUniqueIndexes() {
        Document indexOptions = new Document();
        indexOptions.put("recordUuid", 1);
        indexOptions.put("fieldId", 1);
        CompoundIndexDefinition indexDefinition = new CompoundIndexDefinition(indexOptions);
        mongoTemplate.indexOps(CalculationResult.class).ensureIndex(indexDefinition.unique());
    }

    @Test
    public void shouldSaveCalculationResult() {
        CalculationResult calculationResult = new CalculationResult(
                "5c18a34994c5060edc2ba705",
                "457d2d2b-34de-46a1-839f-ea5b6d2e79ef",
                481,
                12508,
                "Test"
        );

        calculationResultRepository.save(calculationResult);

        CalculationResult updated = calculationResultRepository
                .findByObjectIdAndRecordUuidAndFieldId(
                        "5c18a34994c5060edc2ba705",
                        "457d2d2b-34de-46a1-839f-ea5b6d2e79ef",
                        12508
                );

        assertThat(updated.getValue(), is("Test"));
    }

    @Test
    public void shouldFetchCalculationResult() {
        CalculationResult calculationResult = calculationResultRepository
                .findByObjectIdAndRecordUuidAndFieldId(
                        "5c18a34994c5060edc2ba705",
                        "457d2d2b-34de-46a1-839f-ea5b6d2e79ef",
                        12500
                );

        assertThat(calculationResult.getValue(), is(60L));
    }

    @Test
    public void shouldUpdateAlreadyExistingResult() {
        CalculationResult calculationResult = new CalculationResult(
                "5c18a34994c5060edc2ba705",
                "457d2d2b-34de-46a1-839f-ea5b6d2e79ef",
                481,
                12500,
                60L
        );

        calculationResultRepository.save(calculationResult);

        CalculationResult updated = calculationResultRepository
                .findByObjectIdAndRecordUuidAndFieldId(
                        "5c18a34994c5060edc2ba705",
                        "457d2d2b-34de-46a1-839f-ea5b6d2e79ef",
                        12500
                );

        assertThat(updated.getValue(), is(60L));
    }

    @Configuration
    @EnableMongoRepositories(
            basePackageClasses = CalculationResultRepository.class,
            includeFilters = {
            @ComponentScan.Filter( type = FilterType.ASSIGNABLE_TYPE, value = CalculationResultRepository.class)
    }
    )
    static class MongoTestConfiguration {

        @Bean
        public MongoDbFactory mongoDbFactory() {
            return new SimpleMongoDbFactory(new MongoClientURI("mongodb://localhost:12347/hoji_test"));
        }

        @Bean
        public MongoTemplate mongoTemplate() {
            return new MongoTemplate(mongoDbFactory());
        }
    }

}
