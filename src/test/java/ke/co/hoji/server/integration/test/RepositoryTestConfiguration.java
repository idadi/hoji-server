package ke.co.hoji.server.integration.test;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ServiceManager;
import ke.co.hoji.core.calculation.expression.CrossFormValueRetriever;
import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.core.service.model.SurveyService;
import ke.co.hoji.server.dao.model.JdbcChoiceDao;
import ke.co.hoji.server.dao.model.JdbcChoiceGroupDao;
import ke.co.hoji.server.dao.model.JdbcFieldDao;
import ke.co.hoji.server.dao.model.JdbcFormDao;
import ke.co.hoji.server.dao.model.JdbcLanguageDao;
import ke.co.hoji.server.dao.model.JdbcPropertyDao;
import ke.co.hoji.server.dao.model.JdbcReferenceDao;
import ke.co.hoji.server.dao.model.JdbcSurveyDao;
import ke.co.hoji.server.ServerHojiContext;
import ke.co.hoji.server.calculation.Calculator;
import ke.co.hoji.server.dao.impl.JdbcCashTransactionDao;
import ke.co.hoji.server.dao.impl.JdbcCreditTransactionDao;
import ke.co.hoji.server.dao.impl.JdbcLoggableEventDao;
import ke.co.hoji.server.dao.impl.JdbcUserDao;
import ke.co.hoji.server.repository.impl.AnalysisDataMapper;
import ke.co.hoji.server.repository.impl.RecordOverviewMapper;
import ke.co.hoji.server.repository.impl.TabularDataMapper;
import ke.co.hoji.server.service.CreditTransactionService;
import ke.co.hoji.server.service.RecordServiceImpl;
import ke.co.hoji.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

/**
 * Created by geoffreywasilwa on 26/05/2017.
 */
@Configuration
@ComponentScan(
    basePackageClasses = {UserService.class, ServerHojiContext.class, TabularDataMapper.class, Calculator.class},
    useDefaultFilters = false,
    includeFilters = {
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = FieldService.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = FormService.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = UserService.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = RecordServiceImpl.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = SurveyService.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = CreditTransactionService.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = HojiContext.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = ServiceManager.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = AnalysisDataMapper.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = RecordOverviewMapper.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = TabularDataMapper.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = Calculator.class),
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = CrossFormValueRetriever.class)
    })
@PropertySource("classpath:hoji.properties")
public class RepositoryTestConfiguration {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private FormService formService;

    @Autowired
    private FieldService fieldService;

    @Bean
    public JdbcTemplate template() {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public JdbcSurveyDao surveyDao() {
        return new JdbcSurveyDao(template());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public JdbcUserDao userDao() {
        return new JdbcUserDao(template());
    }

    @Bean
    public JdbcCashTransactionDao cashTransactionDao() {
        JdbcCashTransactionDao cashTransactionDao = new JdbcCashTransactionDao(template(), userDao());
        return cashTransactionDao;
    }

    @Bean
    public JdbcCreditTransactionDao creditTransactionDao() {
        JdbcCreditTransactionDao creditTransactionDao = new JdbcCreditTransactionDao(template(), userDao());
        return creditTransactionDao;
    }

    @Bean
    public JdbcLoggableEventDao loggableEventDao() {
        JdbcLoggableEventDao loggableEventDao = new JdbcLoggableEventDao(template(), userDao());
        return loggableEventDao;
    }

    @Bean
    public JdbcFormDao formDao() {
        JdbcFormDao formDao = new JdbcFormDao(template(), surveyDao());
        return formDao;
    }

    @Bean
    public JdbcFieldDao fieldDao() {
        JdbcFieldDao fieldDao = new JdbcFieldDao(template(), formDao(), choiceGroupDao());
        return fieldDao;
    }

    @Bean
    public JdbcChoiceDao choiceDao() {
        return new JdbcChoiceDao(template());
    }

    @Bean
    public JdbcChoiceGroupDao choiceGroupDao() {
        JdbcChoiceGroupDao choiceGroupDao = new JdbcChoiceGroupDao(template(), choiceDao());
        return choiceGroupDao;
    }

    @Bean
    public JdbcReferenceDao referenceDao() {
        return new JdbcReferenceDao(template());
    }

    @Bean
    public JdbcPropertyDao propertyDao() {
        return new JdbcPropertyDao(template());
    }

    @Bean
    JdbcLanguageDao languageDao() {
        return new JdbcLanguageDao(template());
    }

    @Bean
    public ConfigService configService() {
        return new ConfigService(surveyService, formService, fieldService);
    }

    @PostConstruct
    public void startDBManager() {
        //hsqldb
//        DatabaseManagerSwing.main(new String[] { "--url", "jdbc:hsqldb:mem:testdb_2", "--user", "sa", "--password", "" });
    }

}
