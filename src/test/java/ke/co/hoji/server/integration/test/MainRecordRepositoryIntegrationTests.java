package ke.co.hoji.server.integration.test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.bean.DatabaseConfigBean;
import com.github.springtestdbunit.bean.DatabaseDataSourceConnectionFactoryBean;
import com.mongodb.MongoClientURI;
import ke.co.hoji.core.data.dto.LiteRecord;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.MatrixRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.server.model.Chart;
import ke.co.hoji.server.model.DataPoint;
import ke.co.hoji.server.model.DataRow;
import ke.co.hoji.server.model.DataTable;
import ke.co.hoji.server.model.GpsPoint;
import ke.co.hoji.server.model.ImageData;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.repository.FilterCriteria;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder;
import ke.co.hoji.server.repository.MainRecordRepository;
import ke.co.hoji.server.repository.SearchTermFilter;
import ke.co.hoji.server.repository.SearchTermFilter.ImportFilterBuilder;
import org.dbunit.database.DatabaseDataSourceConnection;
import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static ke.co.hoji.server.repository.SearchTermFilter.BROAD_MATCH;
import static ke.co.hoji.server.repository.SearchTermFilter.EXACT_MATCH;
import static ke.co.hoji.server.repository.SearchTermFilter.PHRASE_MATCH;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * Created by geoffreywasilwa on 28/05/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Configuration
@ContextConfiguration(classes = { RepositoryTestConfiguration.class, MainRecordRepositoryIntegrationTests.MongoTestConfiguration.class})
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DbUnitConfiguration(databaseConnection = "dbUnitDatabaseConnection")
@DatabaseSetup("project_dataset.xml")
public class MainRecordRepositoryIntegrationTests {

    @Autowired
    private ConfigService configService;

    @Autowired
    private MainRecordRepository mainRecordRepository;

    private MainRecordPropertiesFilter testModeTrueFilter;

    private static InMemoryMongoDbManager manager =
            new InMemoryMongoDbManager("mainRecord.json", 12345, "mainRecord");

    @BeforeClass
    public static void setup() throws IOException {
        manager.start();
    }

    /**
     * verify that {@link MainRecordRepository#saveAll(List)} saves new and modified {@link MainRecord}s
     */
    @Test
    public void saveAll_shouldSaveAllSavesNewAndModifiedMainRecords() {
        MainRecord newMainRecord = new MainRecord();
        newMainRecord.setUuid("4d8e25ee-d277-11e7-8941-cec278b6b50a");
        newMainRecord.setFormId(1);
        newMainRecord.setUserId(2);
        newMainRecord.setDateCompleted(new Date().getTime());
        newMainRecord.setDateUploaded(new Date().getTime());
        newMainRecord.setVersion(1);
        newMainRecord.setMain(true);
        newMainRecord.setTest(true);
        newMainRecord.setLiveFields(Collections.emptyList());
        Form parent = getForm(1);
        testModeTrueFilter = new MainRecordPropertiesFilterBuilder(parent).testMode(true).build();
        List<MainRecord> mainRecords = mainRecordRepository.findMainRecords(testModeTrueFilter, Collections.emptyList());
        mainRecords.forEach(mainRecord -> {
            mainRecord.getLiveFields().forEach(liveField -> {
                liveField.setDateUpdated(new Date().getTime());
            });
        });
        mainRecords.add(newMainRecord);

        mainRecordRepository.saveAll(mainRecords);

        List<MainRecord> mainRecordsAfterUpdate = mainRecordRepository.findMainRecords(testModeTrueFilter,
                Collections.emptyList());
        Assert.assertThat(mainRecordsAfterUpdate, hasSize(3));
    }

    /**
     * verify that {@link MainRecordRepository#findChartingData(MainRecordPropertiesFilter)} returns
     * pivot {@link DataTable} from numeric responses
     */
    @Test
    public void findChartingData_shouldFindChartingDataForNumericLiveFields() {
        long expectedFieldValue = 30L;
        List<Chart> charts = getChartsForForm(2);

        Assert.assertThat(charts, hasSize(1));
        Chart numberChart = charts.get(0);
        Assert.assertThat(numberChart.getType(), is(Chart.Type.NUMERIC_INTEGER));
        Assert.assertThat(numberChart.getData(), IsCollectionWithSize.hasSize(1));
        DataPoint numberResponse = numberChart.getData().get(0);
        Assert.assertThat(numberResponse.getValue(), is(expectedFieldValue));
    }

    /**
     * verify that {@link MainRecordRepository#findChartingData(MainRecordPropertiesFilter)} returns
     * pivot {@link DataTable} from single choice responses
     */
    @Test
    public void findChartingData_shouldFindChartingDataForSingleBinaryChoiceLiveFields() {
        List<Chart> charts = getChartsForForm(1);

        Assert.assertThat(charts, hasSize(1));
        Chart binaryChart = charts.get(0);
        Assert.assertThat(binaryChart.getType(), is(Chart.Type.BINARY));
        Assert.assertThat(binaryChart.getSampleSize(), is(2));
        Assert.assertThat(binaryChart.getData(), IsCollectionWithSize.hasSize(2));
        DataPoint yesResponses = binaryChart.getData().get(0);
        Assert.assertThat(yesResponses.getLabel(), is("Yes"));
        Assert.assertThat(yesResponses.getValue(), is(1L));
        DataPoint noResponses = binaryChart.getData().get(1);
        Assert.assertThat(noResponses.getLabel(), is("No"));
        Assert.assertThat(noResponses.getValue(), is(1L));
    }

    /**
     * verify that {@link MainRecordRepository#findChartingData(MainRecordPropertiesFilter)} returns
     * pivot {@link DataTable} from choice responses
     */
    @Test
    public void findChartingData_shouldFindChartingDataForSingleCategoricalChoiceLiveFields() {
        List<Chart> charts = getChartsForForm(6);

        Assert.assertThat(charts, hasSize(1));
        Chart categoricalChart = charts.get(0);
        Assert.assertThat(categoricalChart.getType(), is(Chart.Type.CATEGORICAL));
        Assert.assertThat(categoricalChart.getSampleSize(), is(2));
        Assert.assertThat(categoricalChart.getData(), hasSize(3));
        DataPoint yesResponses = categoricalChart.getData().get(0);
        Assert.assertThat(yesResponses.getLabel(), is("Yes"));
        Assert.assertThat(yesResponses.getValue(), is(1L));
        DataPoint noResponses = categoricalChart.getData().get(1);
        Assert.assertThat(noResponses.getLabel(), is("No"));
        Assert.assertThat(noResponses.getValue(), is(1L));
        DataPoint dontKnowResponses = categoricalChart.getData().get(2);
        Assert.assertThat(dontKnowResponses.getLabel(), is("Don't Know"));
        Assert.assertThat(dontKnowResponses.getValue(), is(0L));
    }

    /**
     * verify that {@link MainRecordRepository#findChartingData(MainRecordPropertiesFilter)} returns
     * a list of {@link Chart}s from choice responses with missing values
     */
    @Test
    public void findChartingData_shouldHaveMissingDataPointInChartData() {
        List<Chart> charts = getChartsForForm(9);

        Assert.assertThat(charts, hasSize(1));
        Chart chart = charts.get(0);
        Assert.assertThat(chart.getType(), is(Chart.Type.BINARY));
        Assert.assertThat(chart.getSampleSize(), is(2));
        Assert.assertThat(chart.getData(), hasSize(3));
        DataPoint yesDp = chart.getData().get(0);
        Assert.assertThat(yesDp.getLabel(), is("Yes"));
        Assert.assertThat(yesDp.getValue(), is(0L));
        DataPoint noDp = chart.getData().get(1);
        Assert.assertThat(noDp.getLabel(), is("No"));
        Assert.assertThat(noDp.getValue(), is(1L));
        DataPoint missingDp = chart.getData().get(2);
        Assert.assertThat(missingDp.getLabel(), is("Missing"));
        Assert.assertThat(missingDp.getValue(), is(1L));

    }

    /**
     * verify that {@link MainRecordRepository#findChartingData(MainRecordPropertiesFilter)} returns
     * a list of {@link Chart}s from multiple choice responses
     */
    @Test
    public void findChartingData_shouldFindChartingDataForMultiChoiceLiveFields() {
        List<Chart> charts = getChartsForForm(3);

        Assert.assertThat(charts, hasSize(1));
        Chart multiChoiceChart = charts.get(0);
        Assert.assertThat(multiChoiceChart.getType(), is(Chart.Type.CATEGORICAL));
        Assert.assertThat(multiChoiceChart.getData(), hasSize(4));
        DataPoint rugby = multiChoiceChart.getData().get(0);
        Assert.assertThat(rugby.getLabel(), is("Rugby"));
        Assert.assertThat(rugby.getValue(), is(2L));
        DataPoint football = multiChoiceChart.getData().get(1);
        Assert.assertThat(football.getLabel(), is("Football"));
        Assert.assertThat(football.getValue(), is(0L));
        DataPoint basketball = multiChoiceChart.getData().get(2);
        Assert.assertThat(basketball.getLabel(), is("Basketball"));
        Assert.assertThat(basketball.getValue(), is(1L));
    }

    /**
     * verify that {@link MainRecordRepository#findDataTable(MainRecordPropertiesFilter, String)} returns
     * pivot {@link DataTable} for matrix field
     */
    @Test
    public void findDataTable_shouldFindPivotDataForMatrixField() {
        Field parent = getMatrixField(4);
        testModeTrueFilter = new MainRecordPropertiesFilterBuilder(parent).testMode(true).build();
        DataTable dataTable = mainRecordRepository
                .findDataTable(testModeTrueFilter, Form.OutputStrategy.PIVOT);

        Assert.assertThat(dataTable.getDataRows(), hasSize(4));
        DataRow firstRow = dataTable.getDataRows().get(0);
        DataRow secondRow = dataTable.getDataRows().get(1);
        DataRow thirdRow = dataTable.getDataRows().get(2);
        DataRow fourthRow = dataTable.getDataRows().get(3);
        Assert.assertEquals(firstRow.getDataElements().size(), secondRow.getDataElements().size());
        Assert.assertEquals(firstRow.getDataElements().size(), thirdRow.getDataElements().size());
        Assert.assertEquals(firstRow.getDataElements().size(), fourthRow.getDataElements().size());
    }

    /**
     * verify that {@link MainRecordRepository#findDataTable(MainRecordPropertiesFilter, String)} returns
     * {@link DataTable} for pivot from {@link LiveField} with varying values for
     * the same multiple choice question
     */
    @Test
    public void findDataTable_shouldFindPivotDataWithVaryingLiveFieldValuesForSameMultiChoice() {
        Form parent = getForm(3);
        testModeTrueFilter = new MainRecordPropertiesFilterBuilder(parent).testMode(true).build();
        DataTable dataTable = mainRecordRepository
                .findDataTable(testModeTrueFilter, Form.OutputStrategy.PIVOT);

        Assert.assertThat(dataTable.getDataRows(), hasSize(4));
        DataRow firstRow = dataTable.getDataRows().get(0);
        DataRow secondRow = dataTable.getDataRows().get(1);
        DataRow thirdRow = dataTable.getDataRows().get(2);
        DataRow forthRow = dataTable.getDataRows().get(3);
        Assert.assertEquals(firstRow.getDataElements().size(), secondRow.getDataElements().size());
        Assert.assertEquals(firstRow.getDataElements().size(), thirdRow.getDataElements().size());
        Assert.assertEquals(firstRow.getDataElements().size(), forthRow.getDataElements().size());
    }

    /**
     * verify that {@link MainRecordRepository#findDataTable(MainRecordPropertiesFilter, String)} returns
     * {@link DataTable} with rows sorted by date uploaded descending order
     */
    @Test
    public void findDataTable_shouldFindPivotDataSortedByDateUploadedDescendingOrder() {
        Form parent = getForm(3);
        testModeTrueFilter = new MainRecordPropertiesFilterBuilder(parent).testMode(true).build();
        DataTable dataTable = mainRecordRepository
                .findDataTable(testModeTrueFilter, Form.OutputStrategy.PIVOT);

        Assert.assertThat(dataTable.getDataRows(), hasSize(4));
        DataRow firstRow = dataTable.getDataRows().get(0);
        DataRow secondRow = dataTable.getDataRows().get(1);
        DataRow thirdRow = dataTable.getDataRows().get(2);
        DataRow forthRow = dataTable.getDataRows().get(3);
        Assert.assertThat(firstRow.getDataElements().get(0).getValue(), is("66498bdd-c41e-4372-bc21-1a50e653d1a4"));
        Assert.assertThat(secondRow.getDataElements().get(0).getValue(), is("59f366da-18f1-4c38-b148-0be3cf6416fb"));
        Assert.assertThat(thirdRow.getDataElements().get(0).getValue(), is("a8928708-fe86-4877-ba4b-484ed7f4f4c0"));
        Assert.assertThat(forthRow.getDataElements().get(0).getValue(), is("cb5896f1-2524-4c93-94c6-ceacd645e9ae"));
    }

    /**
     * verify that {@link MainRecordRepository#findDataTable(MainRecordPropertiesFilter, String)} returns
     * {@link DataTable} for pivot from data collected using different field definitions
     */
    @Test
    public void findDataTable_shouldFindPivotDataWhenDataCollectedWithModifiedFieldDefinitions() {
        Form parent = getForm(5);
        testModeTrueFilter = new MainRecordPropertiesFilterBuilder(parent).testMode(true).build();
        DataTable dataTable = mainRecordRepository
                .findDataTable(testModeTrueFilter, Form.OutputStrategy.PIVOT);

        Assert.assertThat(dataTable.getDataRows(), hasSize(2));
        DataRow firstRow = dataTable.getDataRows().get(0);
        DataRow secondRow = dataTable.getDataRows().get(1);
        Assert.assertEquals(firstRow.getDataElements().size(), secondRow.getDataElements().size());
    }

    /**
     * verify that {@link MainRecordRepository#findImageData(MainRecordPropertiesFilter)} returns
     * a collectio of {@link ke.co.hoji.server.model.ImageData}
     */
    @Test
    public void findImageData_shouldReturnCollectionOfImageData() {
        Form parent = getForm(13);
        testModeTrueFilter = new MainRecordPropertiesFilterBuilder(parent).testMode(true).build();
        List<ImageData> images = mainRecordRepository.findImageData(testModeTrueFilter);

        Assert.assertThat(images, hasSize(2));
        Assert.assertThat(images.get(0).getCaption(), is("House B"));
        Assert.assertThat(images.get(1).getCaption(), is("House A"));
    }

    @Test
    public void findImageData_shouldReturnCollectionOfImageDataForMatrixRecord() {
        Field parent = getMatrixField(19);
        testModeTrueFilter = new MainRecordPropertiesFilterBuilder(parent).testMode(true).build();
        List<ImageData> images = mainRecordRepository.findImageData(testModeTrueFilter);

        Assert.assertThat(images, hasSize(2));
        Assert.assertThat(images.get(0).getCaption(), is("Living room"));
        Assert.assertThat(images.get(1).getCaption(), is("Living room"));
    }

    /**
     * verify that {@link MainRecordRepository#findMonitorChartData(MainRecordPropertiesFilter)} returns a collection
     * of {@link Chart}s
     */
    @Test
    public void findMonitorChartData_shouldFindMonitorDataMatchingFilterFieldCriteria() {
        Form parent = getForm(6);
        testModeTrueFilter = new MainRecordPropertiesFilterBuilder(parent)
                .testMode(true)
                .filterFieldId(8)
                .filterFieldValue(1)
                .build();

        List<Chart> monitorCharts = mainRecordRepository.findMonitorChartData(testModeTrueFilter);

        Assert.assertThat(monitorCharts, hasSize(10));
        Assert.assertThat(monitorCharts.get(0).getData(), hasSize(1));
        Assert.assertThat(monitorCharts.get(1).getData(), hasSize(1));
        Assert.assertThat(monitorCharts.get(2).getData(), hasSize(1));
        Assert.assertThat(monitorCharts.get(3).getData(), hasSize(1));
        Assert.assertThat(monitorCharts.get(4).getData(), hasSize(1));
        Assert.assertThat(monitorCharts.get(5).getData(), hasSize(1));
        Assert.assertThat(monitorCharts.get(6).getData(), hasSize(1));
        Assert.assertThat(monitorCharts.get(7).getData(), hasSize(1));
        Assert.assertThat(monitorCharts.get(8).getData(), hasSize(1));
        Assert.assertThat(monitorCharts.get(9).getData(), hasSize(1));

        testModeTrueFilter = new MainRecordPropertiesFilterBuilder(parent)
                .testMode(true)
                .filterFieldId(8)
                .filterFieldValue(3)
                .build();

        monitorCharts = mainRecordRepository.findMonitorChartData(testModeTrueFilter);

        Assert.assertThat(monitorCharts, hasSize(10));
        Assert.assertThat(monitorCharts.get(0).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(1).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(2).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(3).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(4).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(5).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(6).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(7).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(8).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(9).getData(), hasSize(0));
    }

    /**
     * verify that {@link MainRecordRepository#findGpsPoints(MainRecordPropertiesFilter)} returns a collection
     * {@link GpsPoint}s
     */
    @Test
    public void findGpsPoints_shouldFindGpsPointsMatchingFilterFieldCriteria() {
        Form parent = getForm(6);
        testModeTrueFilter = new MainRecordPropertiesFilterBuilder(parent)
                        .testMode(true)
                        .filterFieldId(8)
                        .filterFieldValue(1)
                        .build();
        List<GpsPoint> gpsPoints = mainRecordRepository.findGpsPoints(testModeTrueFilter);

        Assert.assertThat(gpsPoints, hasSize(1));
        Assert.assertThat(gpsPoints.get(0).getAddress(), is("Loresho Grove, Nairobi"));

        testModeTrueFilter = new MainRecordPropertiesFilterBuilder(parent)
                .testMode(true)
                .filterFieldId(8)
                .filterFieldValue(3)
                .build();
        gpsPoints = mainRecordRepository.findGpsPoints(testModeTrueFilter);

        Assert.assertThat(gpsPoints, hasSize(0));
    }

    /**
     * verify that {@link MainRecordRepository#findRecordsWithoutAddress(long, int)} returns a collection
     * {@link MainRecord}s whose addresses have not been resolved yet
     */
    @Test
    public void findRecordsWithoutAddress_shouldFindMainRecordsWithoutAddressesResolved() {
        List<MainRecord> mainRecords = mainRecordRepository.findRecordsWithoutAddress(0, 100);

        Assert.assertThat(mainRecords, hasSize(1));
    }

    /**
     * verify that {@link MainRecordRepository#findUsersWithRecords(Form)} returns a collection
     * {@link User}s who have submitted data for a given form
     */
    @Test
    public void findUsersWithRecords_shouldFindUsersWhoHaveSubmittedData() {
        Form form = getForm(6);
        testModeTrueFilter = new MainRecordPropertiesFilterBuilder(form).testMode(true).build();

        List<User> users = mainRecordRepository.findUsersWithRecords(form);

        Assert.assertThat(users, hasSize(1));
    }

    /**
     * verify that {@link MainRecordRepository#deleteRecord(String, FieldParent)} removes
     * {@link MainRecord}s with the given uuid and {@link Field}
     */
    @Test
    public void deleteRecord_shouldDeleteMainRecord() {
        String recordUuid = "ff23585e-e750-4d13-a8bd-e1924dcce2af";
        Form form = new Form(6);
        form.setFields(Collections.emptyList());
        mainRecordRepository.deleteRecord(recordUuid, form);

        MainRecord deletedRecord = mainRecordRepository.findByUuid(recordUuid);
        Assert.assertThat(deletedRecord, nullValue());
    }

    /**
     * verify that {@link MainRecordRepository#deleteRecord(String, FieldParent)} removes
     * {@link MatrixRecord}s with the given uuid and {@link Field}
     */
    @Test
    public void deleteRecord_shouldDeletesMatrixRecord() {
        String mainRecordUuid = "e6061c56-a02f-4488-9812-4969303e405b";
        MainRecord mainRecord = mainRecordRepository.findByUuid(mainRecordUuid);
        String matrixRecordUuid = "b3933351-606b-434e-bd0d-a923ab01a4ef";
        Form form = new Form(4);
        Field parent = new Field(4);
        parent.setForm(form);
        Field child = new Field(5);
        parent.setChildren(Collections.singletonList(child));

        Assert.assertThat(mainRecord.getMatrixRecords(), hasSize(2));
        mainRecordRepository.deleteRecord(matrixRecordUuid, parent);
        mainRecord = mainRecordRepository.findByUuid(mainRecordUuid);

        Assert.assertThat(mainRecord.getMatrixRecords(), hasSize(1));
    }

    /**
     * verify that {@link MainRecordRepository#clearData(Form)} removes all {@link MainRecord}s
     * linked to the given {@link Form}
     */
    @Test
    public void clearData_shouldClearMainRecordsRelatedToForm() {
        String mainRecordUuid = "c66cf2a4-7642-4b7b-b6d4-056c54432f5e";
        MainRecord mainRecord = mainRecordRepository.findByUuid(mainRecordUuid);

        Assert.assertNotNull(mainRecord);
        mainRecordRepository.clearData(new Form(11));
        mainRecord = mainRecordRepository.findByUuid(mainRecordUuid);

        Assert.assertNull(mainRecord);
    }

    /**
     * verify that {@link MainRecordRepository#clearData(Survey)} removes all {@link MainRecord}s
     * linked to the given {@link Survey}
     */
    @Test
    public void clearData_shouldClearMainRecordsRelatedToSurvey() {
        String mainRecordUuid = "cce54db2-d6cf-4b12-a9db-bacfdbcccc01";
        MainRecord mainRecord = mainRecordRepository.findByUuid(mainRecordUuid);
        Survey survey = new Survey();
        survey.setForms(Collections.singletonList(new Form(10)));

        Assert.assertNotNull(mainRecord);
        mainRecordRepository.clearData(survey);
        mainRecord = mainRecordRepository.findByUuid(mainRecordUuid);

        Assert.assertNull(mainRecord);
    }

    /**
     * verify that {@link MainRecordRepository#clearData(Field)} removes all {@link LiveField}s
     * linked to the given {@link Field}
     */
    @Test
    public void clearData_shouldClearLiveField() {
        String mainRecordUuid = "5dc7f4b3-a362-4e76-816f-24cd2363c329";
        MainRecord mainRecord = mainRecordRepository.findByUuid(mainRecordUuid);

        Assert.assertThat(mainRecord.getLiveFields(), hasSize(1));
        Field field = new Field(16);
        field.setForm(new Form(12));
        mainRecordRepository.clearData(field);
        mainRecord = mainRecordRepository.findByUuid(mainRecordUuid);

        Assert.assertThat(mainRecord.getLiveFields(), hasSize(0));

        mainRecordUuid = "e6061c56-a02f-4488-9812-4969303e405b";
        mainRecord = mainRecordRepository.findByUuid(mainRecordUuid);
        Field parent = new Field(4);
        Field child = new Field(5);
        child.setParent(parent);
        child.setForm(new Form(4));

        Assert.assertThat(mainRecord.getMatrixRecords().get(0).getLiveFields(), hasSize(1));
        Assert.assertThat(mainRecord.getMatrixRecords().get(1).getLiveFields(), hasSize(1));

        mainRecordRepository.clearData(child);
        mainRecord = mainRecordRepository.findByUuid(mainRecordUuid);

        Assert.assertThat(mainRecord.getMatrixRecords().get(0).getLiveFields(), hasSize(0));
        Assert.assertThat(mainRecord.getMatrixRecords().get(1).getLiveFields(), hasSize(0));
    }

    /**
     * verify that {@link MainRecordRepository#findMainRecords(String, List<Form>)} returns a record and other records
     * linked
     * to it given a {@link MainRecord}'s uuid
     */
    @Test
    public void findMainRecords_shouldFindRecordAndRelationsGivenUuid() {
        Form dependentForm = new Form();
        dependentForm.setId(8);
        String maleRecordUuid = "1657b1e6-50ce-4554-ab98-0401797131c9";
        List<MainRecord> mainRecords = mainRecordRepository.findMainRecords(maleRecordUuid,
                Collections.singletonList(dependentForm));

        Assert.assertThat(mainRecords, hasSize(2));
    }

    /**
     * verify that {@link MainRecordRepository#findMainRecords(String, List<Form>)} returns a record and other records linked
     * to it given a filter criteria
     */
    @Test
    public void findMainRecords_shouldFindRecordAndRelationsGivenFilterCriteria() {
        Form dependentForm = new Form();
        dependentForm.setId(8);
        SearchTermFilter searchTermFilter = new ImportFilterBuilder(new Form(7)).build();
        List<MainRecord> mainRecords = mainRecordRepository.findMainRecords(searchTermFilter,
                Collections.singletonList(dependentForm));

        Assert.assertThat(mainRecords, hasSize(2));
    }

    /**
     * verify that {@link MainRecordRepository#findLiteRecords(FilterCriteria)} finds a {@link LiteRecord}
     * given a search phrase and match sensitivity
     * is set to {@link ImportFilterBuilder#EXACT_MATCH}
     */
    @Test
    public void findLiteRecords_shouldFindLiteRecordsWithExactMatchCriteria() {
        SearchTermFilter searchTermFilter = new ImportFilterBuilder(new Form(5))
                .searchTerms(new String[] { "Jane" })
                .matchSensitivity(EXACT_MATCH)
                .build();

        List<LiteRecord> liteRecords = mainRecordRepository.findLiteRecords(searchTermFilter);

        Assert.assertThat(liteRecords, hasSize(1));
        Assert.assertThat(liteRecords.get(0).getUuid(), is("8f03f585-11db-403a-800a-7b5905c0dd53"));
    }

    /**
     * verify that {@link MainRecordRepository#findLiteRecords(FilterCriteria)} does not find a {@link LiteRecord}
     * when search phrase is incomplete and match sensitivity
     * is set to {@link ImportFilterBuilder#EXACT_MATCH}
     */
    @Test
    public void findLiteRecords_shouldNotFindLiteRecordsWithExactMatchIfPhraseIsNotComplete() {
        SearchTermFilter searchTermFilter = new ImportFilterBuilder(new Form(5))
                .searchTerms(new String[] { "Jan", "Doe" })
                .matchSensitivity(EXACT_MATCH)
                .build();

        List<LiteRecord> liteRecords = mainRecordRepository.findLiteRecords(searchTermFilter);

        Assert.assertThat(liteRecords, hasSize(0));
    }

    /**
     * verify that {@link MainRecordRepository#findLiteRecords(FilterCriteria)} finds a {@link LiteRecord}
     * when at least one search phrase matches a {@link MainRecord} and match sensitivity
     * is set to {@link ImportFilterBuilder#PHRASE_MATCH}
     */
    @Test
    public void findLiteRecords_shouldFindLiteRecordsWithPhraseMatchCriteria() {
        SearchTermFilter searchTermFilter = new ImportFilterBuilder(new Form(5))
                .searchTerms(new String[] { "Jane", "Wrong" })
                .matchSensitivity(PHRASE_MATCH)
                .build();

        List<LiteRecord> liteRecords = mainRecordRepository.findLiteRecords(searchTermFilter);

        Assert.assertThat(liteRecords, hasSize(1));
        Assert.assertThat(liteRecords.get(0).getUuid(), is("8f03f585-11db-403a-800a-7b5905c0dd53"));
    }

    /**
     * verify that {@link MainRecordRepository#findLiteRecords(FilterCriteria)} does not match a partial term
     * when match sensitivity is set to {@link ImportFilterBuilder#PHRASE_MATCH}
     */
    @Test
    public void findLiteRecords_shouldNotFindLiteRecordsWithPhraseMatchCriteriaIfPhraseIsNotComplete() {
        SearchTermFilter searchTermFilter = new ImportFilterBuilder(new Form(5))
                .searchTerms(new String[] { "Jan" })
                .matchSensitivity(PHRASE_MATCH)
                .build();

        List<LiteRecord> liteRecords = mainRecordRepository.findLiteRecords(searchTermFilter);

        Assert.assertThat(liteRecords, hasSize(0));
    }

    /**
     * verify that {@link MainRecordRepository#findLiteRecords(FilterCriteria)} finds a {@link LiteRecord}
     * when match sensitivity is set to {@link ImportFilterBuilder#BROAD_MATCH}
     */
    @Test
    public void findLiteRecords_shouldFindLiteRecordsWithBroadMatchCriteria() {
        SearchTermFilter searchTermFilter = new ImportFilterBuilder(new Form(5))
                .searchTerms(new String[] { "Jan", "Do" })
                .matchSensitivity(BROAD_MATCH)
                .build();

        List<LiteRecord> liteRecords = mainRecordRepository.findLiteRecords(searchTermFilter);

        Assert.assertThat(liteRecords, hasSize(1));
        Assert.assertThat(liteRecords.get(0).getUuid(), is("8f03f585-11db-403a-800a-7b5905c0dd53"));
    }

    /**
     * verify that {@link MainRecordRepository#updateTestMode(String, boolean)} updates test mode of a record
     */
    @Test
    public void updateTestMode_shouldSetTestModeAsSpecified() {
        String uuid = "77bc5215-4356-4838-88ca-517b4fc70b1b";
        MainRecord mainRecord = mainRecordRepository.findByUuid(uuid);

        Assert.assertTrue(mainRecord.isTest());

        mainRecordRepository.updateTestMode(uuid, false);
        mainRecord = mainRecordRepository.findByUuid(uuid);

        Assert.assertFalse(mainRecord.isTest());
    }

    private List<Chart> getChartsForForm(Integer formId) {
        Form parent = getForm(formId);
        testModeTrueFilter = new MainRecordPropertiesFilterBuilder(parent).testMode(true).build();
        return mainRecordRepository.findChartingData(testModeTrueFilter);
    }

    private Form getForm(Integer formId) {
        return configService.getForm(formId);
    }

    private Field getMatrixField(Integer fieldId) {
        return configService.getField(fieldId);
    }

    @AfterClass
    public static void tearDown() {
        manager.stop();
    }

    @Configuration
    @EnableMongoRepositories(basePackageClasses = { MainRecordRepository.class })
    static class MongoTestConfiguration {
        @Bean
        public DataSource dataSource() {
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            return builder
                    .setType(EmbeddedDatabaseType.H2)
                    .setName("testdb")
                    .addScript("create-db.sql")
                    .build();
        }

        @Bean
        public DatabaseConfigBean databaseConfig() {
            return new DatabaseConfigBean();
        }

        @Bean
        public DatabaseDataSourceConnection dbUnitDatabaseConnection() throws Exception {
            DatabaseDataSourceConnectionFactoryBean dataSourceConnectionFactoryBean =
                    new DatabaseDataSourceConnectionFactoryBean();
            dataSourceConnectionFactoryBean.setDataSource(dataSource());
            dataSourceConnectionFactoryBean.setDatabaseConfig(databaseConfig());
            return dataSourceConnectionFactoryBean.getObject();
        }

        @Bean
        public MongoDbFactory mongoDbFactory() throws Exception {
            return new SimpleMongoDbFactory(new MongoClientURI("mongodb://localhost:12345/hoji_test"));
        }

        @Bean
        public MongoTemplate mongoTemplate() throws Exception {
            return new MongoTemplate(mongoDbFactory());
        }
    }

}
