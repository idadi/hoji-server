package ke.co.hoji.server.integration;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.openqa.selenium.By.className;
import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.linkText;
import static org.openqa.selenium.By.name;
import static org.openqa.selenium.By.partialLinkText;
import static org.openqa.selenium.By.tagName;
import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOf;
import static org.openqa.selenium.support.ui.ExpectedConditions.stalenessOf;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FormIntegrationTests extends BaseIntegrationTests {

    @Test
    public void shouldListFormsUnderProject() {
        driver.navigate().to(WEB_PATH + "project/view/5/form");

        assertThat(driver.findElement(cssSelector(".ui.form.table tbody")).findElements(tagName("tr")), hasSize(1));
    }

    @Test
    public void shouldNavigateToProjectListPageViaBreadcrumb() {
        driver.navigate().to(WEB_PATH + "form/view/2/field");
        driver.findElement(cssSelector(".ui.breadcrumb")).findElement(linkText("Projects")).click();

        assertThat(driver.getCurrentUrl(), is(WEB_PATH));
    }

    @Test
    public void shouldNavigateToProjectViewPageViaBreadcrumb() {
        driver.navigate().to(WEB_PATH + "form/view/2/field");
        driver.findElement(cssSelector(".ui.breadcrumb")).findElement(partialLinkText("Network maintenance"))
                .click();

        assertThat(driver.getCurrentUrl(), is(WEB_PATH + "project/view/" + 5 + "/form"));
    }

    @Test
    public void shouldAddNewForm() {
        driver.navigate().to(WEB_PATH + "project/view/4/form");
        driver.findElement(className("add-form")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(visibilityOf(driver.findElement(id("edit-form"))));
        String formName = "Maintenance Calls Serviced";
        driver.findElement(name("name")).sendKeys(formName);
        driver.findElement(className("edit-submit")).click();
        wait.until(invisibilityOf(driver.findElement(id("edit-form"))));

        assertThat(driver.findElement(cssSelector(".ui.form.table tbody")).getText(), containsString(formName));
        assertThatProjectIsModified(4);
    }

    @Test
    public void shouldUpdateFormName() {
        driver.navigate().to(WEB_PATH + "form/view/2/field");
        driver.findElement(className("more-actions")).click();
        driver.findElement(className("form-edit")).click();
        String updatedProjectName = "Customer Feedback [**updated**]";
        WebElement nameInput = driver.findElement(name("name"));
        nameInput.clear();
        nameInput.sendKeys(updatedProjectName);
        driver.findElement(className("edit-submit")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(invisibilityOf(driver.findElement(id("edit-form"))));

        assertThat(driver.findElement(id("form-name")).getText(), is(updatedProjectName));
        assertThatProjectIsModified(5);
    }

    @Test
    public void shouldDisableAndEnableForm() {
        driver.navigate().to(WEB_PATH + "form/view/2/field");
        driver.findElement(className("more-actions")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        WebElement enable = driver.findElement(className("form-enable"));
        WebElement formName = driver.findElement(id("form-name"));
        wait.until(visibilityOf(driver.findElement(className("form-manage-visualizations"))));
        enable.click();
        wait.until(stalenessOf(formName));

        assertThat(driver.findElement(id("form-name")).getAttribute("class"), containsString("disabled"));

        driver.findElement(className("more-actions")).click();
        enable = driver.findElement(className("form-enable"));
        formName = driver.findElement(id("form-name"));
        wait.until(visibilityOf(driver.findElement(className("form-manage-visualizations"))));
        enable.click();
        wait.until(stalenessOf(formName));

        assertThat(driver.findElement(id("form-name")).getAttribute("class"), not(containsString("disabled")));
    }

    @Test
    public void shouldDeleteForm() {
        driver.navigate().to(WEB_PATH + "form/view/4/field");
        driver.findElement(className("more-actions")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        WebElement deleteButton = driver.findElement(className("form-delete"));
        wait.until(visibilityOf(deleteButton));
        deleteButton.click();
        driver.findElement(name("name")).sendKeys("Customer Feedback[**delete**]");
        driver.findElement(className("delete-submit")).click();
        WebElement deleteForm = driver.findElement(id("delete-form"));
        wait.until(invisibilityOf(deleteForm));

        assertThat(driver.getCurrentUrl(), is(WEB_PATH + "project/view/7/form"));
        assertTrue(driver.findElement(cssSelector(".ui.placeholder.segment")).isDisplayed());
        assertThatProjectIsModified(7);
    }

}
