package ke.co.hoji.server.integration;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

import java.io.File;
import java.io.IOException;

import static org.eclipse.jetty.webapp.WebInfConfiguration.CONTAINER_JAR_PATTERN;

class JettyServer {

    private static Server server;

    public static void init(int port, String contextPath) {
        if (server != null && hasBeenInitialised()) {
            return;
        }
        if (!contextPath.startsWith("/")) {
            contextPath = "/" + contextPath;
        }
        File webxml = new File("src/main/webapp/WEB-INF/web.xml");
        File webappDir = new File("src/main/webapp");
        String[] configurationClasses = {
                "org.eclipse.jetty.webapp.WebInfConfiguration",
                "org.eclipse.jetty.webapp.WebXmlConfiguration",
                "org.eclipse.jetty.webapp.MetaInfConfiguration",
                "org.eclipse.jetty.webapp.FragmentConfiguration",
                "org.eclipse.jetty.annotations.AnnotationConfiguration",
                "org.eclipse.jetty.webapp.JettyWebXmlConfiguration"
        };
        server = new Server(port);
        WebAppContext context = new WebAppContext();
        context.setContextPath(contextPath);
        context.setConfigurationClasses(configurationClasses);
        context.setDescriptor(webxml.getAbsolutePath());
        context.setResourceBase(webappDir.getAbsolutePath());
        context.setAttribute(CONTAINER_JAR_PATTERN, ".*/spring-[^/]*\\.jar$|.*/target/classes/$");
        context.setParentLoaderPriority(true);
        context.setThrowUnavailableOnStartupException(true);

        server.setHandler(context);
        server.setStopAtShutdown(true);
        try {
            server.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean hasBeenInitialised() {
        return server.isStarting() || server.isStarted() || server.isRunning();
    }
}
