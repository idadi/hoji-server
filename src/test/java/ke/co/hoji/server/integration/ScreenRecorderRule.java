package ke.co.hoji.server.integration;

import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.monte.screenrecorder.ScreenRecorder;


public class ScreenRecorderRule implements MethodRule {

    private final ScreenRecorder screenRecorder;

    public ScreenRecorderRule(ScreenRecorder screenRecorder) {
        this.screenRecorder = screenRecorder;
    }

    @Override
    public Statement apply(Statement base, FrameworkMethod method, Object target) {
        return new Statement() {

            @Override
            public void evaluate() throws Throwable {

                try {
                    base.evaluate();
                } catch (Throwable t) {
                    throw t;
                } finally {
                    screenRecorder.stop();
                }
            }
        };
    }

}
