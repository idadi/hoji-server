package ke.co.hoji.server.integration;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.openqa.selenium.By.className;
import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.name;
import static org.openqa.selenium.By.tagName;
import static org.openqa.selenium.By.xpath;
import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOf;
import static org.openqa.selenium.support.ui.ExpectedConditions.stalenessOf;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FieldIntegrationTests extends BaseIntegrationTests {

    @Test
    public void shouldListFieldsUnderForm() {
        driver.navigate().to(WEB_PATH + "form/view/6/field");

        assertThat(driver.findElement(cssSelector(".ui.field.table tbody")).findElements(tagName("tr")), hasSize(7));
    }

    @Test
    public void shouldAddField() {
        driver.navigate().to(WEB_PATH + "form/view/5/field");
        driver.findElement(className("field-add")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(visibilityOf(driver.findElement(id("edit-form"))));
        WebElement typeInput = driver.findElement(cssSelector(".field-type input.search"));
        typeInput.click();
        typeInput.sendKeys("Label");
        typeInput.sendKeys(Keys.TAB);
        String fieldDescription = "Introduction";
        driver.findElement(name("description")).sendKeys(fieldDescription);
        WebElement saveButton = driver.findElement(cssSelector(".edit-submit"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", saveButton);
        saveButton.click();
        wait.until(invisibilityOf(driver.findElement(id("edit-form"))));

        assertThat(driver.findElement(cssSelector(".ui.field.table tbody")).getText(),
                containsString(fieldDescription));
        assertThatProjectIsModified(8);
    }

    @Test
    public void shouldBulkEditFields() {
        driver.navigate().to(WEB_PATH + "form/view/7/field");
        driver.findElement(xpath("//tr[@data-field-id='46']")).findElement(className("check")).click();
        driver.findElement(className("bulk-actions")).click();
        driver.findElement(className("bulk-edit")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(visibilityOf(driver.findElement(id("edit-form"))));
        WebElement inputFormatInput = driver.findElement(cssSelector(".input-format input.search"));
        inputFormatInput.click();
        inputFormatInput.sendKeys("Lower Case");
        inputFormatInput.sendKeys(Keys.TAB);
        WebElement parentInput = driver.findElement(cssSelector(".parent input.search"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", parentInput);
        parentInput.click();
        parentInput.sendKeys("Service rating");
        parentInput.sendKeys(Keys.TAB);
        WebElement saveButton = driver.findElement(className("edit-submit"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", saveButton);
        saveButton.click();
        wait.until(invisibilityOf(driver.findElement(id("edit-form"))));
        driver.navigate().to(WEB_PATH + "field/view/46");

        assertThat(driver.findElement(className("parent")).getText(), containsString("Service rating"));
        assertThat(driver.findElement(className("input-format")).getText(), containsString("Lower Case"));
        assertThatProjectIsModified(10);
    }

    @Test
    public void shouldEditFieldDescription() {
        driver.navigate().to(WEB_PATH + "field/view/36");
        driver.findElement(className("more-actions")).click();
        driver.findElement(className("edit")).click();
        String updatedDescription = "Introduction[**updated**]";
        WebElement descriptionInput = driver.findElement(name("description"));
        descriptionInput.clear();
        descriptionInput.sendKeys(updatedDescription);
        WebElement saveButton = driver.findElement(className("edit-submit"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", saveButton);
        saveButton.click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(invisibilityOf(driver.findElement(id("edit-form"))));

        assertThat(driver.findElement(id("field-name")).getText(), containsString(updatedDescription));
        assertThatProjectIsModified(9);
    }

    @Test
    public void shouldDisableAndEnableField() {
        driver.navigate().to(WEB_PATH + "field/view/36");
        driver.findElement(className("more-actions")).click();
        WebDriverWait wait = new WebDriverWait(driver, 3);
        WebElement fieldName = driver.findElement(id("field-name"));
        wait.until(visibilityOf(driver.findElement(className("enable"))));
        driver.findElement(className("enable")).click();
        wait.until(stalenessOf(fieldName));

        assertThat(driver.findElement(id("field-name")).getAttribute("class"), containsString("disabled"));

        driver.findElement(className("more-actions")).click();
        fieldName = driver.findElement(id("field-name"));
        wait.until(visibilityOf(driver.findElement(className("enable"))));
        driver.findElement(className("enable")).click();
        wait.until(stalenessOf(fieldName));

        assertThat(driver.findElement(id("field-name")).getAttribute("class"), not(containsString("disabled")));
    }

    @Test
    public void shouldDeleteField() {
        driver.navigate().to(WEB_PATH + "field/view/56");
        driver.findElement(className("more-actions")).click();
        WebElement delete = driver.findElement(className("delete"));
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(visibilityOf(delete));
        delete.click();
        wait.until(visibilityOf(driver.findElement(id("delete-form"))));
        driver.findElement(name("name")).sendKeys("2");
        WebElement deleteForm = driver.findElement(id("delete-form"));
        driver.findElement(className("delete-submit")).click();
        wait.until(invisibilityOf(deleteForm));

        assertThat(driver.getCurrentUrl(), is(WEB_PATH + "form/view/8/field"));
        assertFalse(driver.findElement(cssSelector(".ui.field.table tbody")).getText().contains("[**delete**]"));
        assertThatProjectIsModified(11);
    }

}
