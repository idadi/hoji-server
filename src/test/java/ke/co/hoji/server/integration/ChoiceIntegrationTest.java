package ke.co.hoji.server.integration;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.openqa.selenium.By.className;
import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.linkText;
import static org.openqa.selenium.By.name;
import static org.openqa.selenium.By.tagName;
import static org.openqa.selenium.By.xpath;
import static org.openqa.selenium.support.ui.ExpectedConditions.and;
import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOf;
import static org.openqa.selenium.support.ui.ExpectedConditions.numberOfElementsToBeMoreThan;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.io.Resources;

public class ChoiceIntegrationTest extends BaseIntegrationTests {

    @Test
    public void shouldNavigateToChoicesPage() {
        driver.get(WEB_PATH);
        driver.findElement(linkText("Choices")).click();

        assertThat(driver.getCurrentUrl(), containsString("/choice"));
    }

    @Test
    public void shouldAddChoice() {
        driver.navigate().to(WEB_PATH + "choice");
        driver.findElement(className("add")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        WebElement editModal = driver.findElement(cssSelector(".ui.modal"));
        wait.until(visibilityOf(editModal));
        String choiceName = "Yes[**added**]";
        editModal.findElement(name("name")).sendKeys(choiceName);
        editModal.findElement(className("edit-submit")).click();
        wait.until(invisibilityOf(editModal));

        assertThat(driver.findElement(tagName("tbody")).getText(), containsString(choiceName));
    }

    @Test
    public void shouldEditChoice() {
        driver.navigate().to(WEB_PATH + "choice-group");
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(numberOfElementsToBeMoreThan(tagName("tr"), 2));
        driver.findElement(linkText("Yes/No[**remove choice**]")).click();
        WebElement choiceToRemove = driver.findElement(xpath("//tr[td = 'Yes']"));
        choiceToRemove.findElement(className("edit")).click();
        WebElement editModal = driver.findElement(cssSelector(".ui.modal"));
        wait.until(visibilityOf(editModal));
        editModal.findElement(name("code")).clear();
        editModal.findElement(name("code")).sendKeys("Y[**edited**]");
        editModal.findElement(cssSelector(".ui.checkbox")).click();
        editModal.findElement(className("edit-submit")).click();
        wait.until(invisibilityOf(editModal));

        assertThat(driver.findElement(cssSelector(".choice.table tbody")).getText(),
                containsString("Yes Y[**edited**]"));
    }

    @Test
    public void shouldUploadChoices() {
        driver.navigate().to(WEB_PATH + "choice");
        WebDriverWait wait = new WebDriverWait(driver, 2);
        driver.findElement(className("upload")).click();
        WebElement uploadModal = driver.findElement(cssSelector(".ui.modal"));
        wait.until(visibilityOf(uploadModal));
        String uploadFilePath = Resources.getResource("upload-choices.csv").getPath();
        uploadModal.findElement(cssSelector("input[type='file']")).sendKeys(uploadFilePath);
        uploadModal.findElement(className("upload-submit")).click();
        wait.until(invisibilityOf(uploadModal));

        assertThat(driver.findElement(tagName("tbody")).getText(), containsString("Yes[**uploaded**]"));
    }

    @Test
    public void shouldDeleteChoice() {
        driver.navigate().to(WEB_PATH + "choice");
        WebElement rowToDelete = driver.findElement(xpath("//tr[td = 'Service time[**delete**]']"));
        rowToDelete.findElement(cssSelector("i.delete-choice")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        WebElement deleteModal = driver.findElement(cssSelector(".ui.modal"));
        wait.until(visibilityOf(deleteModal));
        deleteModal.findElement(className("delete-submit")).click();
        wait.until(and(invisibilityOf(deleteModal), invisibilityOf(rowToDelete)));

        assertThat(driver.findElement(tagName("tbody")).getText(), not(containsString("Service time[**delete**]")));
    }

    @Test
    public void shouldCloseEditModalOnEscape() {
        driver.navigate().to(WEB_PATH + "choice");
        WebElement rowToDelete = driver.findElement(xpath("//tr[td[contains(text(),'Yes[**edit')]]"));
        rowToDelete.findElement(cssSelector("i.edit-choice")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        WebElement editModal = driver.findElement(cssSelector(".ui.modal"));
        wait.until(visibilityOf(editModal));
        driver.findElement(tagName("body")).sendKeys(Keys.ESCAPE);

        assertThat(driver.findElements(cssSelector(".ui.modal")), hasSize(0));
    }

    @Test
    public void shouldCloseDeleteModalOnEscape() {
        driver.navigate().to(WEB_PATH + "choice");
        WebElement rowToDelete = driver.findElement(xpath("//tr[td[contains(text(),'Yes[**edit')]]"));
        rowToDelete.findElement(cssSelector("i.delete-choice")).click();
        WebDriverWait wait = new WebDriverWait(driver, 2);
        WebElement editModal = driver.findElement(cssSelector(".ui.modal"));
        wait.until(visibilityOf(editModal));
        driver.findElement(tagName("body")).sendKeys(Keys.ESCAPE);

        assertThat(driver.findElements(cssSelector(".ui.modal")), hasSize(0));
    }
}
