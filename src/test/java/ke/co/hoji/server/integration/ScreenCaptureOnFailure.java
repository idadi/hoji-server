package ke.co.hoji.server.integration;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.util.FileCopyUtils;

public class ScreenCaptureOnFailure implements MethodRule {

    private final WebDriver driver;

    public ScreenCaptureOnFailure(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    public Statement apply(Statement base, FrameworkMethod method, Object target) {
        return new Statement() {

            @Override
            public void evaluate() throws Throwable {
                try {
                    base.evaluate();
                } catch (Throwable t) {
                    // exception will be thrown only when a test fails.
                    captureScreenShot(method.getName());
                    // re throw to allow the failure to be reported by JUnit
                    throw t;
                }
            }

            public void captureScreenShot(String fileName) throws IOException {
                File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                fileName += UUID.randomUUID().toString();
                File targetFile = new File("/tmp/" + fileName + ".png");
                FileCopyUtils.copy(scrFile, targetFile);
            }
        };
    }

}
