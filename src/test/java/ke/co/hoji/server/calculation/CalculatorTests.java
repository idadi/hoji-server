package ke.co.hoji.server.calculation;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.calculation.CalculationEvaluator;
import ke.co.hoji.core.calculation.expression.CrossFormValueRetriever;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.MatrixRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.OperatorDescriptor;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.response.NumberResponse;
import ke.co.hoji.core.response.SingleChoiceResponse;
import ke.co.hoji.core.rule.GreaterThan;
import ke.co.hoji.core.rule.RuleEvaluator;
import ke.co.hoji.server.repository.CalculationResultRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import javax.script.ScriptEngineManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorTests {

    @Mock
    private CalculationResultRepository calculationResultRepository;

    @Mock
    private CrossFormValueRetriever crossFormValueRetriever;

    @Mock
    private HojiContext hojiContext;

    @Mock
    private ApplicationContext applicationContext;

    @Before
    public void setup() {
        when(applicationContext.getBean(HojiContext.class)).thenReturn(hojiContext);
        when(hojiContext.getCalculationEvaluator()).thenReturn(new CalculationEvaluator(new ScriptEngineManager().getEngineByName("nashorn")));
    }

    @Test
    public void calculate_shouldCalculateAndStoreCalculatedFieldValue() {
        FieldType numeric = new FieldType(1, "Numeric", FieldType.Code.DECIMAL_NUMBER);
        numeric.setDataType(FieldType.DataType.NUMBER);
        numeric.setResponseClass(NumberResponse.class.getName());
        Field age = new Field(1);
        age.setOrdinal(BigDecimal.valueOf(1));
        age.setEnabled(true);
        age.setType(numeric);
        Field doubleAge = new Field(2);
        doubleAge.setOrdinal(BigDecimal.valueOf(2));
        doubleAge.setEnabled(true);
        doubleAge.setType(numeric);
        doubleAge.setValueScript("function calculate() { return cu.getValue(1) * 2; }");
        doubleAge.setBackwardSkips(Collections.emptyList());
        Form form = new Form(1);
        form.setFields(Arrays.asList(age, doubleAge));
        doubleAge.setForm(form);
        Survey survey = new Survey(1, "SURVEY", true, "SVR");
        form.setSurvey(survey);
        MainRecord record = getMainRecord(30);
        Calculator calculator = new Calculator(calculationResultRepository, crossFormValueRetriever);
        calculator.setApplicationContext(applicationContext);

        CalculationResult result = calculator.calculate(record, "UUID", doubleAge);

        assertTrue(result.isEvaluated());
        assertThat(result.getValue(), equalTo(60D));
        verify(calculationResultRepository, times(1)).save(result);
    }

    @Test
    public void calculate_shouldCalculateAndStoreCalculatedFieldValueInMatrix() {
        FieldType numeric = new FieldType(1, "Numeric", FieldType.Code.DECIMAL_NUMBER);
        numeric.setDataType(FieldType.DataType.NUMBER);
        numeric.setResponseClass(NumberResponse.class.getName());
        Field age = new Field(2);
        age.setOrdinal(BigDecimal.valueOf(2));
        age.setEnabled(true);
        age.setType(numeric);
        Field doubleAge = new Field(3);
        doubleAge.setOrdinal(BigDecimal.valueOf(3));
        doubleAge.setEnabled(true);
        doubleAge.setType(numeric);
        doubleAge.setValueScript("function calculate() { return cu.getValue(2) * 2; }");
        doubleAge.setBackwardSkips(Collections.emptyList());
        Field parent = new Field(1);
        parent.setOrdinal(BigDecimal.valueOf(1));
        parent.setEnabled(true);
        parent.setChildren(Arrays.asList(age, doubleAge));
        doubleAge.setParent(parent);
        Form form = new Form(1);
        doubleAge.setForm(form);
        form.setFields(Arrays.asList(age, doubleAge));
        Survey survey = new Survey(1, "SURVEY", true, "SVR");
        form.setSurvey(survey);
        LiveField ageLf = new LiveField();
        ageLf.setFieldId(2);
        ageLf.setValue(30);
        MatrixRecord matrixRecord = new MatrixRecord();
        matrixRecord.setUuid("M_UUID");
        matrixRecord.setLiveFields(Collections.singletonList(ageLf));
        matrixRecord.setDateCreated(LocalDate.now().toEpochDay());
        MainRecord record = new MainRecord();
        record.setMatrixRecords(Collections.singletonList(matrixRecord));
        Calculator calculator = new Calculator(calculationResultRepository, crossFormValueRetriever);
        calculator.setApplicationContext(applicationContext);

        CalculationResult result = calculator.calculate(record, "M_UUID", doubleAge);

        assertTrue(result.isEvaluated());
        assertThat(result.getValue(), equalTo(60D));
        verify(calculationResultRepository, times(1)).save(result);
    }

    @Test
    public void calculate_shouldReturnCachedValue() {
        CalculationResult cachedResult = new CalculationResult("ID", "UUID", 1, 2, 60D);
        when(calculationResultRepository.findByObjectIdAndRecordUuidAndFieldId("ID", "UUID", 2))
                .thenReturn(cachedResult);
        Calculator calculator = new Calculator(calculationResultRepository, crossFormValueRetriever);
        calculator.setApplicationContext(applicationContext);
        MainRecord mainRecord = new MainRecord();
        mainRecord.setId("ID");

        CalculationResult result = calculator.calculate(mainRecord, "UUID", new Field(2));

        assertTrue(result.isEvaluated());
        assertThat(result.getValue(), equalTo(60D));
        verify(calculationResultRepository, times(1))
                .findByObjectIdAndRecordUuidAndFieldId("ID", "UUID", 2);
        verify(calculationResultRepository, times(0)).save(result);
    }

    @Test
    public void calculate_shouldReCalculateForForceReCalculation() {
        FieldType numeric = new FieldType(1, "Numeric", FieldType.Code.DECIMAL_NUMBER);
        numeric.setDataType(FieldType.DataType.NUMBER);
        numeric.setResponseClass(NumberResponse.class.getName());
        Field age = new Field(1);
        age.setOrdinal(BigDecimal.valueOf(1));
        age.setEnabled(true);
        age.setType(numeric);
        Field doubleAge = new Field(2);
        doubleAge.setOrdinal(BigDecimal.valueOf(2));
        doubleAge.setEnabled(true);
        doubleAge.setType(numeric);
        doubleAge.setValueScript("function calculate() { return cu.getValue(1) * 2; }");
        doubleAge.setBackwardSkips(Collections.emptyList());
        Form form = new Form(1);
        form.setFields(Arrays.asList(age, doubleAge));
        doubleAge.setForm(form);
        Survey survey = new Survey(1, "SURVEY", true, "SVR");
        form.setSurvey(survey);
        MainRecord record = getMainRecord(30);
        Calculator calculator = new Calculator(calculationResultRepository, crossFormValueRetriever);
        calculator.setApplicationContext(applicationContext);

        CalculationResult result = calculator.calculate(record, "UUID", doubleAge, true);

        assertTrue(result.isEvaluated());
        assertThat(result.getValue(), equalTo(60D));
        verify(calculationResultRepository, times(1)).save(result);
    }

    @Test
    public void calculate_shouldNotCalculateSkippedCalculatedField() {
        FieldType numeric = new FieldType(1, "Numeric", FieldType.Code.DECIMAL_NUMBER);
        numeric.setDataType(FieldType.DataType.NUMBER);
        numeric.setResponseClass(NumberResponse.class.getName());
        Field age = new Field(1);
        age.setOrdinal(BigDecimal.valueOf(1));
        age.setEnabled(true);
        age.setType(numeric);
        Field doubleAge = new Field(2);
        doubleAge.setOrdinal(BigDecimal.valueOf(2));
        doubleAge.setEnabled(true);
        doubleAge.setType(numeric);
        doubleAge.setValueScript("function calculate() { return cu.getValue(1) * 2; }");
        Rule rule = new Rule();
        rule.setOwner(doubleAge);
        rule.setTarget(age);
        OperatorDescriptor gt = new OperatorDescriptor(1, "GT", GreaterThan.class.getName());
        rule.setOperatorDescriptor(gt);
        rule.setValue("10");
        doubleAge.setBackwardSkips(Collections.singletonList(rule));
        Form form = new Form(1);
        form.setFields(Arrays.asList(age, doubleAge));
        doubleAge.setForm(form);
        Survey survey = new Survey(1, "SURVEY", true, "SVR");
        form.setSurvey(survey);
        MainRecord record = getMainRecord(9);
        RuleEvaluator ruleEvaluator = Mockito.mock(RuleEvaluator.class);
        when(hojiContext.getRuleEvaluator()).thenReturn(ruleEvaluator);
        Calculator calculator = new Calculator(calculationResultRepository, crossFormValueRetriever);
        calculator.setApplicationContext(applicationContext);

        CalculationResult result = calculator.calculate(record, "UUID", doubleAge);

        assertFalse(result.isEvaluated());
        assertNull(result.getValue());
        verify(calculationResultRepository, times(0)).save(any());
    }

    @Test
    public void calculate_shouldCalculateForCalculatedFieldReferencingClientServerCalculatedField() {
        FieldType numeric = new FieldType(1, "Numeric", FieldType.Code.DECIMAL_NUMBER);
        numeric.setDataType(FieldType.DataType.NUMBER);
        numeric.setResponseClass(NumberResponse.class.getName());
        Field age = new Field(1);
        age.setOrdinal(BigDecimal.valueOf(1));
        age.setEnabled(true);
        age.setType(numeric);
        age.setValueScript("function calculate() { return 10 * 3; }");
        age.setCalculated(Field.Calculated.SERVER);
        Field doubleAge = new Field(2);
        doubleAge.setOrdinal(BigDecimal.valueOf(2));
        doubleAge.setEnabled(true);
        doubleAge.setType(numeric);
        doubleAge.setValueScript("function calculate() { return cu.getValue(1) * 2; }");
        doubleAge.setCalculated(Field.Calculated.SERVER);
        doubleAge.setBackwardSkips(Collections.emptyList());
        Form form = new Form(1);
        form.setFields(Arrays.asList(age, doubleAge));
        age.setForm(form);
        doubleAge.setForm(form);
        Survey survey = new Survey(1, "SURVEY", true, "SVR");
        form.setSurvey(survey);
        MainRecord record = getMainRecord(25);
        record.setId("REC_ID");
        record.setUuid("REC_UUID");
        record.setDateCreated(LocalDate.now().toEpochDay());
        CalculationResult cachedResult =
                new CalculationResult(record.getId(), record.getUuid(), form.getId(), age.getId(), 30D);
        when(calculationResultRepository
                .findByObjectIdAndRecordUuidAndFieldId(record.getId(), record.getUuid(), age.getId()))
                .thenReturn(cachedResult);
        Calculator calculator = new Calculator(calculationResultRepository, crossFormValueRetriever);
        calculator.setApplicationContext(applicationContext);


        CalculationResult result = calculator.calculate(record, record.getUuid(), doubleAge, true);

        assertTrue(result.isEvaluated());
        assertThat(result.getValue(), equalTo(60D));
        verify(calculationResultRepository, times(1)).save(result);
    }

    @Test
    public void calculate_shouldCalculateForCalculatedFieldReferencingServerCalculatedField() {
        FieldType numeric = new FieldType(1, "Numeric", FieldType.Code.DECIMAL_NUMBER);
        numeric.setDataType(FieldType.DataType.NUMBER);
        numeric.setResponseClass(NumberResponse.class.getName());
        Field age = new Field(1);
        age.setOrdinal(BigDecimal.valueOf(1));
        age.setEnabled(true);
        age.setType(numeric);
        age.setValueScript("function calculate() { return 10 * 3; }");
        age.setCalculated(Field.Calculated.SERVER);
        Field doubleAge = new Field(2);
        doubleAge.setOrdinal(BigDecimal.valueOf(2));
        doubleAge.setEnabled(true);
        doubleAge.setType(numeric);
        doubleAge.setValueScript("function calculate() { return cu.getValue(1) * 2; }");
        doubleAge.setCalculated(Field.Calculated.SERVER);
        doubleAge.setBackwardSkips(Collections.emptyList());
        Form form = new Form(1);
        form.setFields(Arrays.asList(age, doubleAge));
        age.setForm(form);
        doubleAge.setForm(form);
        Survey survey = new Survey(1, "SURVEY", true, "SVR");
        form.setSurvey(survey);
        MainRecord record = new MainRecord();
        record.setId("REC_ID");
        record.setUuid("REC_UUID");
        record.setDateCreated(LocalDate.now().toEpochDay());
        record.setLiveFields(Collections.emptyList());
        CalculationResult cachedResult =
                new CalculationResult(record.getId(), record.getUuid(), form.getId(), age.getId(), 30D);
        when(calculationResultRepository
                .findByObjectIdAndRecordUuidAndFieldId(record.getId(), record.getUuid(), age.getId()))
                .thenReturn(cachedResult);
        Calculator calculator = new Calculator(calculationResultRepository, crossFormValueRetriever);
        calculator.setApplicationContext(applicationContext);


        CalculationResult result = calculator.calculate(record, record.getUuid(), doubleAge, true);

        assertTrue(result.isEvaluated());
        assertThat(result.getValue(), equalTo(60D));
        verify(calculationResultRepository, times(1)).save(result);
    }

    @Test
    public void calculate_shouldCalculateForCalculatedFieldReferencingClientServerCalculatedFieldInMatrixRecord() {
        FieldType numeric = new FieldType(1, "Numeric", FieldType.Code.DECIMAL_NUMBER);
        numeric.setDataType(FieldType.DataType.NUMBER);
        numeric.setResponseClass(NumberResponse.class.getName());
        FieldType singleChoice = new FieldType(2, "Single Choice", FieldType.Code.SINGLE_CHOICE);
        singleChoice.setDataType(FieldType.DataType.NUMBER);
        singleChoice.setResponseClass(SingleChoiceResponse.class.getName());
        Field age = new Field(2);
        age.setOrdinal(BigDecimal.valueOf(2));
        age.setEnabled(true);
        age.setType(numeric);
        age.setValueScript("function calculate() { return 10 * 3; }");
        age.setCalculated(Field.Calculated.SERVER);
        Field actual = new Field(3);
        actual.setOrdinal(BigDecimal.valueOf(3));
        actual.setEnabled(true);
        actual.setType(singleChoice);
        actual.setCalculated(Field.Calculated.CLIENT_SERVER);
        actual.setValueScript("function calculate() { return null; }");
        Field doubleAge = new Field(4);
        doubleAge.setOrdinal(BigDecimal.valueOf(4));
        doubleAge.setEnabled(true);
        doubleAge.setType(numeric);
        doubleAge.setValueScript("function calculate() { var actual = cu.getValue(3);\n" +
                "if (actual.id === 1)\n" +
                "return cu.getValue(2) * 2;\n" +
                "else\n" +
                "return cu.getValue(2); }");
        doubleAge.setCalculated(Field.Calculated.SERVER);
        doubleAge.setBackwardSkips(Collections.emptyList());
        Field parent = new Field(1);
        parent.setOrdinal(BigDecimal.valueOf(1));
        parent.setEnabled(true);
        age.setParent(parent);
        actual.setParent(parent);
        doubleAge.setParent(parent);
        Form form = new Form(1);
        form.setFields(Arrays.asList(age, actual, doubleAge));
        parent.setForm(form);
        age.setForm(form);
        doubleAge.setForm(form);
        Survey survey = new Survey(1, "SURVEY", true, "SVR");
        form.setSurvey(survey);
        MatrixRecord matrixRecord = new MatrixRecord();
        matrixRecord.setUuid("M_UUID");
        matrixRecord.setLiveFields(Collections.emptyList());
        matrixRecord.setDateCreated(LocalDate.now().toEpochDay());
        MainRecord record = new MainRecord();
        record.setId("REC_ID");
        record.setUuid("REC_UUID");
        record.setMatrixRecords(Collections.singletonList(matrixRecord));
        record.setDateCreated(LocalDate.now().toEpochDay());
        CalculationResult ageResult =
                new CalculationResult(record.getId(), matrixRecord.getUuid(), form.getId(), age.getId(), 25D);
        when(calculationResultRepository
                .findByObjectIdAndRecordUuidAndFieldId(record.getId(), matrixRecord.getUuid(), age.getId()))
                .thenReturn(ageResult);
        Choice yes = new Choice(1, "Yes", "Y", 0, false);
        CalculationResult actualResult =
                new CalculationResult(record.getId(), matrixRecord.getUuid(), form.getId(), actual.getId(), yes);
        when(calculationResultRepository
                .findByObjectIdAndRecordUuidAndFieldId(record.getId(), matrixRecord.getUuid(), actual.getId()))
                .thenReturn(actualResult);
        Calculator calculator = new Calculator(calculationResultRepository, crossFormValueRetriever);
        calculator.setApplicationContext(applicationContext);


        CalculationResult result = calculator.calculate(record, matrixRecord.getUuid(), doubleAge, true);

        assertTrue(result.isEvaluated());
        assertThat(result.getValue(), equalTo(50D));
        verify(calculationResultRepository, times(1)).save(result);
    }

    @Test
    public void calculate_shouldCalculateForCalculatedFieldReferencingServerCalculatedFieldInMatrixRecord() {
        FieldType numeric = new FieldType(1, "Numeric", FieldType.Code.DECIMAL_NUMBER);
        numeric.setDataType(FieldType.DataType.NUMBER);
        numeric.setResponseClass(NumberResponse.class.getName());
        Field age = new Field(2);
        age.setOrdinal(BigDecimal.valueOf(2));
        age.setEnabled(true);
        age.setType(numeric);
        age.setValueScript("function calculate() { return 5 * 5; }");
        age.setCalculated(Field.Calculated.SERVER);
        Field doubleAge = new Field(3);
        doubleAge.setOrdinal(BigDecimal.valueOf(3));
        doubleAge.setEnabled(true);
        doubleAge.setType(numeric);
        doubleAge.setValueScript("function calculate() { return cu.getValue(2) * 2; }");
        doubleAge.setCalculated(Field.Calculated.SERVER);
        doubleAge.setBackwardSkips(Collections.emptyList());
        Field parent = new Field(1);
        parent.setOrdinal(BigDecimal.valueOf(1));
        parent.setEnabled(true);
        age.setParent(parent);
        doubleAge.setParent(parent);
        Form form = new Form(1);
        form.setFields(Arrays.asList(age, doubleAge));
        parent.setForm(form);
        parent.setChildren(Arrays.asList(age, doubleAge));
        age.setForm(form);
        doubleAge.setForm(form);
        Survey survey = new Survey(1, "SURVEY", true, "SVR");
        form.setSurvey(survey);
        LiveField ageLf = new LiveField();
        ageLf.setFieldId(2);
        ageLf.setValue(30);
        MatrixRecord matrixRecord = new MatrixRecord();
        matrixRecord.setUuid("M_UUID");
        matrixRecord.setLiveFields(Collections.singletonList(ageLf));
        matrixRecord.setDateCreated(LocalDate.now().toEpochDay());
        MainRecord record = new MainRecord();
        record.setId("REC_ID");
        record.setUuid("REC_UUID");
        record.setMatrixRecords(Collections.singletonList(matrixRecord));
        record.setDateCreated(LocalDate.now().toEpochDay());
        CalculationResult cachedResult =
                new CalculationResult(record.getId(), matrixRecord.getUuid(), form.getId(), age.getId(), 25D);
        when(calculationResultRepository
                .findByObjectIdAndRecordUuidAndFieldId(record.getId(), matrixRecord.getUuid(), age.getId()))
                .thenReturn(cachedResult);
        Calculator calculator = new Calculator(calculationResultRepository, crossFormValueRetriever);
        calculator.setApplicationContext(applicationContext);


        CalculationResult result = calculator.calculate(record, matrixRecord.getUuid(), doubleAge, true);

        assertTrue(result.isEvaluated());
        assertThat(result.getValue(), equalTo(50D));
        verify(calculationResultRepository, times(1)).save(result);
    }

    private MainRecord getMainRecord(Object liveFieldValue) {
        LiveField ageLf = new LiveField();
        ageLf.setFieldId(1);
        ageLf.setValue(liveFieldValue);
        MainRecord record = new MainRecord();
        record.setDateCreated(LocalDate.now().toEpochDay());
        record.setLiveFields(Collections.singletonList(ageLf));
        return record;
    }
}