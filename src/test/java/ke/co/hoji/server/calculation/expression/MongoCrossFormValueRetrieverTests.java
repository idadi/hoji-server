package ke.co.hoji.server.calculation.expression;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.response.NumberResponse;
import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.server.calculation.CalculationResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.util.CloseableIterator;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MongoCrossFormValueRetrieverTests {

    @Test
    public void evaluate_shouldReturnLiveField() {
        FieldType number = new FieldType(1, "Number", FieldType.Code.WHOLE_NUMBER);
        number.setResponseClass(NumberResponse.class.getName());
        number.setDataType(FieldType.DataType.NUMBER);
        Field target = new Field();
        target.setId(1);
        target.setType(number);
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        HojiContext hojiContext = mock(HojiContext.class);
        ModelServiceManager serviceManager = mock(ModelServiceManager.class);
        ConfigService configService = mock(ConfigService.class);
        MongoOperations operations = mock(MongoOperations.class);
        MainRecord mainRecord = mock(MainRecord.class);
        ke.co.hoji.core.data.dto.LiveField liveField = mock(ke.co.hoji.core.data.dto.LiveField.class);

        when(applicationContext.getBean(HojiContext.class)).thenReturn(hojiContext);
        when(hojiContext.getModelServiceManager()).thenReturn(serviceManager);
        when(serviceManager.getConfigService()).thenReturn(configService);
        when(configService.getField(anyInt())).thenReturn(target);
        when(operations.stream(any(Query.class), eq(MainRecord.class))).thenReturn(new CloseableIteratorStub(mainRecord));
        when(mainRecord.getLiveFields()).thenReturn(Collections.singletonList(liveField));
        when(liveField.getFieldId()).thenReturn(target.getId());
        when(liveField.getValue()).thenReturn(2);

        MongoCrossFormValueRetriever retriever = new MongoCrossFormValueRetriever(operations);
        retriever.setApplicationContext(applicationContext);
        MongoEqualsExpression equalsExpression = new MongoEqualsExpression("liveField.value", 4, false);
        List<Object> values = retriever.retrieve(equalsExpression);

        assertThat(values, hasSize(1));
        assertThat(values.get(0), is(2L));
    }

    @Test
    public void evaluate_shouldReturnEmptyListForMissingLiveField() {
        FieldType number = new FieldType(1, "Number", FieldType.Code.WHOLE_NUMBER);
        number.setResponseClass(NumberResponse.class.getName());
        number.setDataType(FieldType.DataType.NUMBER);
        Field target = new Field();
        target.setId(1);
        target.setType(number);
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        HojiContext hojiContext = mock(HojiContext.class);
        ModelServiceManager serviceManager = mock(ModelServiceManager.class);
        ConfigService configService = mock(ConfigService.class);
        MongoOperations operations = mock(MongoOperations.class);
        MainRecord mainRecord = mock(MainRecord.class);

        when(applicationContext.getBean(HojiContext.class)).thenReturn(hojiContext);
        when(hojiContext.getModelServiceManager()).thenReturn(serviceManager);
        when(serviceManager.getConfigService()).thenReturn(configService);
        when(configService.getField(anyInt())).thenReturn(target);
        when(operations.stream(any(Query.class), eq(MainRecord.class))).thenReturn(new CloseableIteratorStub(mainRecord));
        when(mainRecord.getLiveFields()).thenReturn(Collections.emptyList());

        MongoCrossFormValueRetriever retriever = new MongoCrossFormValueRetriever(operations);
        retriever.setApplicationContext(applicationContext);
        MongoEqualsExpression equalsExpression = new MongoEqualsExpression("liveField.value", 4, false);
        List<Object> values = retriever.retrieve(equalsExpression);

        assertThat(values, hasSize(0));
    }

    @Test
    public void evaluate_shouldReturnLiveFieldForCalculatedField() {
        FieldType number = new FieldType(1, "Number", FieldType.Code.WHOLE_NUMBER);
        number.setResponseClass(NumberResponse.class.getName());
        number.setDataType(FieldType.DataType.NUMBER);
        Field target = new Field();
        target.setId(1);
        target.setType(number);
        target.setValueScript("function calculate() { return null; }");
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        HojiContext hojiContext = mock(HojiContext.class);
        ModelServiceManager serviceManager = mock(ModelServiceManager.class);
        ConfigService configService = mock(ConfigService.class);
        MongoOperations operations = mock(MongoOperations.class);
        MainRecord mainRecord = mock(MainRecord.class);
        CalculationResult calculationResult = mock(CalculationResult.class);

        when(applicationContext.getBean(HojiContext.class)).thenReturn(hojiContext);
        when(hojiContext.getModelServiceManager()).thenReturn(serviceManager);
        when(serviceManager.getConfigService()).thenReturn(configService);
        when(configService.getField(anyInt())).thenReturn(target);
        when(operations.stream(any(Query.class), eq(MainRecord.class))).thenReturn(new CloseableIteratorStub(mainRecord));
        when(operations.findOne(any(Query.class), eq(CalculationResult.class))).thenReturn(calculationResult);
        when(calculationResult.isEvaluated()).thenReturn(true);
        when(calculationResult.getValue()).thenReturn(4);

        MongoCrossFormValueRetriever retriever = new MongoCrossFormValueRetriever(operations);
        retriever.setApplicationContext(applicationContext);
        MongoEqualsExpression equalsExpression = new MongoEqualsExpression("liveField.value", 4, false);
        List<Object> values = retriever.retrieve(equalsExpression);

        assertThat(values, hasSize(1));
        assertThat(values.get(0), is(4));
    }

    static class CloseableIteratorStub implements CloseableIterator<MainRecord> {

        private Iterator<MainRecord> iterator;

        CloseableIteratorStub(MainRecord... mainRecords) {
            iterator = Arrays.asList(mainRecords).iterator();
        }

        @Override
        public void close() {
            iterator = null;
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public MainRecord next() {
            return iterator.next();
        }
    }

}