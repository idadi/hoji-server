package ke.co.hoji.server.calculation.expression;

import org.junit.Test;
import org.springframework.data.mongodb.core.query.Criteria;

import java.io.IOException;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.data.mongodb.core.query.Criteria.where;

public class MongoExpressionParserTests {

    @Test
    public void parse_shouldParseSimpleEqualsExpression() throws IOException {
        String expression = "{ \"eq\": { \"fieldId\" : 1} }";

        Criteria criteria = (Criteria) new MongoExpressionParser().parse(expression).toQuery();

        assertThat(criteria, equalTo(where("liveFields.fieldId").is(1)));
    }

    @Test
    public void parse_shouldParseSimpleNotEqualsExpression() throws IOException {
        String expression = "{ \"!eq\": { \"fieldId\" : 1} }";

        Criteria criteria = (Criteria) new MongoExpressionParser().parse(expression).toQuery();

        assertThat(criteria, is(where("liveFields.fieldId").ne(1)));
    }

    @Test
    public void parse_shouldParseAndCompoundExpressions() throws IOException {
        String expression = "{ \"and\": [{ \"eq\": { \"formId\" : 1} }, { \"eq\": { \"fieldId\" : 1} }] }";

        Criteria criteria = (Criteria) new MongoExpressionParser().parse(expression).toQuery();

        assertThat(criteria, is(new Criteria().andOperator(where("formId").is(1), where("liveFields.fieldId").is(1))));
    }

    @Test
    public void parse_shouldParseNestedCompoundExpression() throws IOException {
        String expression = "{ \"and\": [{ \"eq\": { \"formId\": 1 } }, { \"or\": [{ \"eq\": { \"fieldId\" : 1} }, " +
                "{ \"eq\": { \"fieldId\" : 2} }]}]}";

        Criteria criteria = (Criteria) new MongoExpressionParser().parse(expression).toQuery();

        assertThat(criteria, is(new Criteria().andOperator(where("formId").is(1),
                new Criteria().orOperator(where("liveFields.fieldId").is(1), where("liveFields.fieldId").is(2)))));
    }

}
