package ke.co.hoji.server.repository.impl;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.MatrixRecord;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.model.Chart;
import ke.co.hoji.server.model.DataPoint;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder;
import ke.co.hoji.server.service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class MonitorDataMapperTests {

    @Mock
    private UserService userService;
    private User firstUser;
    private User secondUser;
    private MainRecordPropertiesFilter filter = new MainRecordPropertiesFilterBuilder(new Form(1)).build();

    private final String CAPTION_ONE = "MainRecord 1";
    private final String NO_CAPTION = "";

    @Before
    public void setup() {
        firstUser = new User();
        firstUser.setFullName("First User");
        when(userService.findById(1)).thenReturn(firstUser);
        secondUser = new User();
        secondUser.setFullName("Second User");
        when(userService.findById(2)).thenReturn(secondUser);
    }

    @Test
    public void mapChart_shouldReturnRecordsByEnumeratorChart() {
        MainRecord first = createMainRecord();
        first.setUserId(1);
        MainRecord second = createMainRecord();
        second.setUserId(2);
        MainRecord third = createMainRecord();
        third.setUserId(1);

        List<Chart> monitorCharts = new MonitorDataMapper(userService).mapChart(Stream.of(first, second, third), filter);

        Assert.assertThat(monitorCharts, hasItem(hasProperty("data", hasItems(
                new DataPoint(firstUser.getFullName(), 2L),
                new DataPoint(secondUser.getFullName(), 1L)
        ))));
    }

    @Test
    public void mapChart_shouldReturnRecordCompletionDurations() {
        MainRecord first = createMainRecord();
        first.setCaption(CAPTION_ONE);
        first.setUserId(1);
        first.setDateCreated(1521541300000L);
        first.setDateCompleted(1521541360000L);
        MainRecord second = createMainRecord();
        second.setUuid("UUID1");
        second.setUserId(1);
        second.setDateCreated(1521541400000L);
        second.setDateCompleted(1521541520000L);

        List<Chart> monitorCharts = new MonitorDataMapper(userService).mapChart(Stream.of(first, second), filter);

        Assert.assertThat(monitorCharts, hasItem(hasProperty("data", hasItems(
                new DataPoint(CAPTION_ONE, 1D),
                new DataPoint("UUID1", 2D)
        ))));
    }

    @Test
    public void mapChart_shouldReturnRecordUploadDurations() {
        MainRecord first = createMainRecord();
        first.setCaption(CAPTION_ONE);
        first.setUserId(1);
        first.setDateUpdated(1521541300000L);
        first.setDateUploaded(1521541330000L);
        MainRecord second = createMainRecord();
        second.setUserId(1);
        second.setDateUpdated(1521541400000L);
        second.setDateUploaded(1521541420000L);

        List<Chart> monitorCharts = new MonitorDataMapper(userService).mapChart(Stream.of(first, second), filter);

        Assert.assertThat(monitorCharts, hasItem(hasProperty("data", hasItems(
                new DataPoint(CAPTION_ONE, 0.5D),
                new DataPoint(NO_CAPTION, 0.33D)
        ))));
    }

    @Test
    public void mapChart_shouldReturnRecordsByUploadVersion() {
        MainRecord first = createMainRecord();
        first.setUserId(1);
        first.setVersion(1);
        MainRecord second = createMainRecord();
        second.setUserId(1);
        second.setVersion(1);
        MainRecord third = createMainRecord();
        third.setUserId(2);
        third.setVersion(2);

        List<Chart> monitorCharts = new MonitorDataMapper(userService).mapChart(Stream.of(first, second, third), filter);

        Assert.assertThat(monitorCharts, hasItem(hasProperty("data", hasItems(
                new DataPoint("1", 2L),
                new DataPoint("2", 1L)
        ))));
    }

    @Test
    public void mapChart_shouldReturnRecordsByFormVersion() {
        MainRecord first = createMainRecord();
        first.setUserId(1);
        first.setFormVersion("1.0");
        MainRecord second = createMainRecord();
        second.setUserId(1);
        second.setFormVersion("1.2");
        MainRecord third = createMainRecord();
        third.setUserId(2);
        third.setFormVersion("1.0");

        List<Chart> monitorCharts = new MonitorDataMapper(userService).mapChart(Stream.of(first, second, third), filter);

        Assert.assertThat(monitorCharts, hasItem(
                hasProperty("data", hasItems(
                        new DataPoint("1.0", 2L),
                        new DataPoint("1.2", 1L)
                ))
        ));
    }

    @Test
    public void mapChart_shouldReturnRecordsByGeoTagging() {
        MainRecord first = createMainRecord();
        first.setUserId(1);
        first.setStartLatitude(1D);
        first.setStartLongitude(1D);
        MainRecord second = createMainRecord();
        second.setUserId(1);
        second.setStartLatitude(1.2D);
        second.setStartLongitude(2D);
        MainRecord third = createMainRecord();
        third.setUserId(2);
        third.setStartLatitude(0D);
        third.setStartLongitude(0D);

        List<Chart> monitorCharts = new MonitorDataMapper(userService).mapChart(Stream.of(first, second, third), filter);

        Assert.assertThat(monitorCharts, hasItem(
                hasProperty("data", hasItems(
                        new DataPoint("Yes", 2L),
                        new DataPoint("No", 1L)
                ))
        ));
    }

    @Test
    public void mapChart_shouldReturnGpsAccuracyPerRecordPerUser() {
        MainRecord first = createMainRecord();
        first.setCaption(CAPTION_ONE);
        first.setUserId(1);
        first.setStartLatitude(1D);
        first.setStartLongitude(1D);
        first.setStartAccuracy(1200D);
        MainRecord second = createMainRecord();
        second.setCaption("");
        second.setUserId(2);
        second.setStartLatitude(1.2D);
        second.setStartLongitude(1.5D);
        second.setStartAccuracy(800D);
        MainRecord third = createMainRecord();
        third.setStartAccuracy(0D);
        third.setUserId(1);

        List<Chart> monitorCharts = new MonitorDataMapper(userService).mapChart(Stream.of(first, second, third), filter);

        Assert.assertThat(monitorCharts, hasItem(
                allOf(
                        hasProperty("key", equalTo(9)),
                        hasProperty("sampleSize", equalTo(2)),
                        hasProperty("data", hasItems(
                                new DataPoint(CAPTION_ONE, 1200D),
                                new DataPoint(NO_CAPTION, 800D)
                        ))
                )
        ));
    }

    @Test
    public void mapChart_shouldReturnRecordsByDistance() {
        MainRecord first = createMainRecord();
        first.setCaption(CAPTION_ONE);
        first.setUserId(1);
        first.setStartLatitude(1D);
        first.setStartLongitude(1.5D);
        first.setEndLatitude(1.0001D);
        first.setEndLongitude(1.5D);

        List<Chart> monitorCharts = new MonitorDataMapper(userService).mapChart(Stream.of(first), filter);

        Assert.assertThat(monitorCharts, hasItem(
                hasProperty("data", hasItems(
                        new DataPoint(CAPTION_ONE, 11.12D)
                ))
        ));
    }

    @Test
    public void mapChart_shouldReturnAverageEnumeratorSpeed() {
        MainRecord first = createMainRecord();
        first.setCaption(CAPTION_ONE);
        first.setUserId(1);
        first.setLiveFields(Arrays.asList(new LiveField(), new LiveField()));
        first.setDateCreated(1521541300000L);
        first.setDateCompleted(1521541360000L);
        MainRecord second = createMainRecord();
        second.setUserId(1);
        second.setLiveFields(Collections.singletonList(new LiveField()));
        second.setDateCreated(1521541400000L);
        second.setDateCompleted(1521541520000L);
        MatrixRecord matrixRecord = new MatrixRecord();
        matrixRecord.setLiveFields(Arrays.asList(new LiveField(), new LiveField()));
        second.setMatrixRecords(Collections.singletonList(matrixRecord));

        List<Chart> monitorCharts = new MonitorDataMapper(userService).mapChart(Stream.of(first, second), filter);

        Assert.assertThat(monitorCharts, hasItem(
                hasProperty("data", hasItems(
                        new DataPoint(CAPTION_ONE, 2D),
                        new DataPoint(NO_CAPTION, 1.5D)
                ))
        ));
    }

    @Test
    public void mapChart_shouldReturnBlankChartsWhenFilterFieldCriteriaFails() {
        LiveField filterLf = new LiveField();
        filterLf.setFieldId(1);
        filterLf.setValue(2L);
        MainRecord first = createMainRecord();
        first.setLiveFields(Collections.singletonList(filterLf));
        first.setUserId(1);
        first.setDateCreated(1521541300000L);
        first.setDateCompleted(1521541360000L);
        first.setStartLatitude(1D);
        first.setStartLongitude(1D);
        first.setEndLatitude(1.0001D);
        first.setEndLongitude(1.5D);
        first.setVersion(1);
        first.setFormVersion("1.0");

        List<Chart> monitorCharts =
                new MonitorDataMapper(userService).mapChart(
                        Stream.of(first),
                        new MainRecordPropertiesFilterBuilder(new Form(1))
                                .filterFieldId(1)
                                .filterFieldValue(1)
                                .build()
                );

        Assert.assertThat(monitorCharts, hasSize(10));
        Assert.assertThat(monitorCharts.get(0).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(1).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(2).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(3).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(4).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(5).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(6).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(7).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(8).getData(), hasSize(0));
        Assert.assertThat(monitorCharts.get(9).getData(), hasSize(0));
    }

    private MainRecord createMainRecord() {
        MainRecord mainRecord = new MainRecord();
        mainRecord.setStartLatitude(0D);
        mainRecord.setStartLongitude(0D);
        mainRecord.setEndLatitude(0D);
        mainRecord.setEndLongitude(0D);
        return mainRecord;
    }

}