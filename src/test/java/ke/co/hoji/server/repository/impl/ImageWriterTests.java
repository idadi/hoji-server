package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.MatrixRecord;
import net.jcip.annotations.NotThreadSafe;
import org.junit.After;
import org.junit.Test;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@NotThreadSafe
public class ImageWriterTests {

    private String imageLocation = getClass().getClassLoader().getResource(".").getFile() + "images" + File.separator;

    @Test
    public void writeImages_shouldSaveImageToFile() throws FileNotFoundException {
        MainRecord record = new MainRecord();
        record.setFormId(1);
        record.setCaption("Test");
        LiveField q1 = new LiveField();
        q1.setFieldId(1);
        q1.setValue(10);
        LiveField q2 = new LiveField();
        q2.setFieldId(2);
        q2.setUuid("f43d8429-2b73-451a-a821-7ec5184e6825");
        q2.setValue("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkYPhfDwAChwGA60e6kgAAAABJRU5ErkJggg==");
        record.setLiveFields(Arrays.asList(q1, q2));

        new ImageWriter(imageLocation).writeImages(record);

        assertTrue(ResourceUtils.getFile(record.getLiveFields().get(1).getValue().toString()).exists());
    }

    @Test
    public void writeImages_shouldSaveMatrixImageToFile() throws FileNotFoundException {
        MainRecord record = new MainRecord();
        record.setFormId(1);
        record.setCaption("Test");
        MatrixRecord matrixRecord = new MatrixRecord();
        LiveField q1 = new LiveField();
        q1.setFieldId(1);
        q1.setValue(10);
        LiveField q2 = new LiveField();
        q2.setFieldId(2);
        q2.setUuid("f43d8429-2b73-451a-a821-7ec5184e6825");
        q2.setValue("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNkYPhfDwAChwGA60e6kgAAAABJRU5ErkJggg==");
        matrixRecord.setLiveFields(Arrays.asList(q1, q2));
        record.setLiveFields(Collections.emptyList());
        record.setMatrixRecords(Collections.singletonList(matrixRecord));

        new ImageWriter(imageLocation).writeImages(record);

        assertTrue(ResourceUtils.getFile(record.getMatrixRecords().get(0).getLiveFields().get(1).getValue().toString()).exists());
    }

    @Test
    public void writeImages_shouldIgnoreMainRecordWithoutImages() {
        MainRecord mainRecord = new MainRecord();
        LiveField q1 = new LiveField();
        q1.setFieldId(1);
        q1.setValue(10);
        mainRecord.setLiveFields(Collections.singletonList(q1));

        new ImageWriter(imageLocation).writeImages(mainRecord);

        assertThat(mainRecord.getLiveFields(), equalTo(mainRecord.getLiveFields()));
    }

    @After
    public void cleanup() {
        File dir = new File(imageLocation);
        deleteFiles(dir);
    }

    private void deleteFiles(File dir) {
        if (dir.exists()) {
            for (File file : dir.listFiles()) {
                if (file.isDirectory()) {
                    deleteFiles(file);
                } else {
                    file.delete();
                }
            }
            dir.delete();
        }
    }

}