package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.MatrixRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.response.NumberResponse;
import ke.co.hoji.server.calculation.CalculationResult;
import ke.co.hoji.server.calculation.Calculator;
import ke.co.hoji.server.model.RecordOverview;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RecordOverviewMapperTests {

    @Mock
    private UserService userService;

    @Mock
    private Calculator calculator;

    @Before
    public void setup() {
        User user = new User();
        user.setFullName("Test User");
        when(userService.findById(anyInt())).thenReturn(user);
    }

    /**
     * verify that {@link RecordOverviewMapper#mapRecordOverview(MainRecord, FieldParent)}
     * properly sets responses for a given {@link Form}
     */
    @Test
    public void mapRecordOverview_shouldReturnRecordOverviewForForm() {
        Form form = new Form(1);
        FieldType numericType = new FieldType(1, "Numeric", FieldType.Code.WHOLE_NUMBER);
        numericType.setDataType(FieldType.DataType.NUMBER);
        numericType.setResponseClass(NumberResponse.class.getName());
        Field field = new Field(1);
        field.setType(numericType);
        field.setEnabled(true);
        form.setFields(Collections.singletonList(field));
        LiveField liveField = new LiveField();
        liveField.setUuid("L_UID_1");
        liveField.setFieldId(1);
        int value = 20;
        liveField.setValue(value);
        MainRecord mainRecord = new MainRecord();
        mainRecord.setLiveFields(Collections.singletonList(liveField));
        setMetadata(mainRecord);

        RecordOverview overview = new RecordOverviewMapper(userService, null, calculator)
                .mapRecordOverview(mainRecord, form);

        assertNotNull(overview);
        Assert.assertThat(overview.getResponses(), hasSize(1));
        Assert.assertThat(overview.getResponses().get(0).getValue(), is(String.valueOf(value)));
    }

    /**
     * verify that {@link RecordOverviewMapper#mapRecordOverview(MainRecord, FieldParent)}
     * properly sets responses for a given {@link Field}
     */
    @Test
    public void mapRecordOverview_shouldReturnRecordOverviewForField() {
        Field fieldParent = new Field(1);
        FieldType numericType = new FieldType(1, "Numeric", FieldType.Code.WHOLE_NUMBER);
        numericType.setDataType(FieldType.DataType.NUMBER);
        fieldParent.setEnabled(true);
        numericType.setResponseClass(NumberResponse.class.getName());
        Field child = new Field(2);
        child.setEnabled(true);
        child.setType(numericType);
        fieldParent.setChildren(Collections.singletonList(child));
        LiveField liveField = new LiveField();
        liveField.setUuid("L_UID_1");
        liveField.setFieldId(2);
        int value = 20;
        liveField.setValue(value);
        MatrixRecord matrixRecord = new MatrixRecord();
        matrixRecord.setFieldId(1);
        matrixRecord.setLiveFields(Collections.singletonList(liveField));
        MainRecord mainRecord = new MainRecord();
        mainRecord.setMatrixRecords(Collections.singletonList(matrixRecord));
        setMetadata(mainRecord);

        RecordOverview overview = new RecordOverviewMapper(userService, null, calculator)
                .mapRecordOverview(mainRecord, fieldParent);

        assertNotNull(overview);
        Assert.assertThat(overview.getResponses(), hasSize(1));
        Assert.assertThat(overview.getResponses().get(0).getValue(), is(String.valueOf(value)));
    }

    /**
     * verify that {@link RecordOverviewMapper#mapRecordOverview(MainRecord, FieldParent)}
     * properly sets test metadata property
     */
    @Test
    public void mapRecordOverview_shouldSetTestMetadataProperty() {
        Form form = new Form(1);
        FieldType numericType = new FieldType(1, "Numeric", FieldType.Code.WHOLE_NUMBER);
        numericType.setDataType(FieldType.DataType.NUMBER);
        numericType.setResponseClass(NumberResponse.class.getName());
        Field field = new Field(1);
        field.setType(numericType);
        form.setFields(Collections.singletonList(field));
        LiveField liveField = new LiveField();
        liveField.setUuid("L_UID_1");
        liveField.setFieldId(1);
        int value = 20;
        liveField.setValue(value);
        MainRecord mainRecord = new MainRecord();
        mainRecord.setLiveFields(Collections.singletonList(liveField));
        setMetadata(mainRecord);

        RecordOverview overview = new RecordOverviewMapper(userService, null, calculator)
                .mapRecordOverview(mainRecord, form);

        assertNotNull(overview);
        Assert.assertTrue(overview.getRecordMetadata().isTest());
    }

    @Test
    public void mapRecordOverview_shouldReturnRecordWithValueForCalculatedField() {
        Form form = new Form(1);
        FieldType numericType = new FieldType(1, "Numeric", FieldType.Code.WHOLE_NUMBER);
        numericType.setDataType(FieldType.DataType.NUMBER);
        numericType.setResponseClass(NumberResponse.class.getName());
        Field field = new Field(1);
        field.setType(numericType);
        field.setEnabled(true);
        field.setValueScript("function calculate() { return null; }");
        form.setFields(Collections.singletonList(field));
        MainRecord mainRecord = new MainRecord();
        mainRecord.setLiveFields(Collections.emptyList());
        setMetadata(mainRecord);
        when(calculator.calculate(mainRecord, mainRecord.getUuid(), field)).thenReturn(new CalculationResult(null,
                null, null, null, 20));

        RecordOverview overview = new RecordOverviewMapper(userService, null, calculator)
                .mapRecordOverview(mainRecord, form);

        assertNotNull(overview);
        assertThat(overview.getResponses(), hasSize(1));
        assertThat(overview.getResponses().get(0).getValue(), is("20"));
    }

    private void setMetadata(MainRecord mainRecord) {
        mainRecord.setUuid("UID_1");
        mainRecord.setTest(true);
        mainRecord.setUserId(1);
        mainRecord.setFormVersion("1");
        mainRecord.setVersion(1);
        mainRecord.setStartLatitude(0D);
        mainRecord.setStartLongitude(0D);
        mainRecord.setEndLatitude(0D);
        mainRecord.setEndLongitude(0D);
        mainRecord.setDateCreated(1494860970714L);
        mainRecord.setDateCompleted(1494860980714L);
        mainRecord.setDateUpdated(1494860970714L);
        mainRecord.setDateUploaded(1494860975714L);
    }

}