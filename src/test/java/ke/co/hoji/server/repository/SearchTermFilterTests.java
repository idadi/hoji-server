package ke.co.hoji.server.repository;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.repository.SearchTermFilter.ImportFilterBuilder;
import org.junit.Assert;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static ke.co.hoji.server.repository.SearchTermFilter.BROAD_MATCH;
import static ke.co.hoji.server.repository.SearchTermFilter.EXACT_MATCH;
import static ke.co.hoji.server.repository.SearchTermFilter.PHRASE_MATCH;

/**
 * Created by geoffreywasilwa on 05/06/2017.
 */
public class SearchTermFilterTests {

    private static final int FORM_ID = 1;

    @Test
    public void testGetCriteriaForBroadMatch() throws Exception {
        String expectedExpression = "{ \"formId\" : 1, \"searchTerms\" : { \"$regex\" : \"phrase1|phrase2\", \"$options\" : \"i\" } }";

        SearchTermFilter searchTermFilter = new ImportFilterBuilder(new Form(FORM_ID))
                .matchSensitivity(BROAD_MATCH)
                .searchTerms(new String[] {"phrase1", "phrase2"})
                .build();
        String actualExpression = searchTermFilter.getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);

    }

    @Test
    public void testGetCriteriaForPhraseMatch() throws Exception {
        String expectedExpression = "{ \"formId\" : 1, \"searchTerms\" : { \"$regex\" : \"\\\\|phrase1\\\\||\\\\|phrase2\\\\|\", \"$options\" : \"i\" } }";
        SearchTermFilter searchTermFilter = new ImportFilterBuilder(new Form(FORM_ID))
                .matchSensitivity(PHRASE_MATCH)
                .searchTerms(new String[] {"phrase1", "phrase2"})
                .build();
        String actualExpression = searchTermFilter.getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);

    }

    @Test
    public void testGetCriteriaForExactMatch() throws Exception {
        String expectedExpression = "{ \"formId\" : 1, \"searchTerms\" : { \"$regex\" : \"\\\\|phrase1\\\\|(.*?\\\\|)?phrase2\\\\|\", \"$options\" : \"i\" } }";
        SearchTermFilter searchTermFilter = new ImportFilterBuilder(new Form(FORM_ID))
                .matchSensitivity(EXACT_MATCH)
                .searchTerms(new String[] {"phrase1", "phrase2"})
                .build();
        String actualExpression = searchTermFilter.getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);

    }

    @Test
    public void testGetCriteriaWhenFromAndToDatesSupplied() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        Date from = calendar.getTime();
        calendar.add(Calendar.MONTH, 1);
        Date to = calendar.getTime();
        String expectedExpression = "{ \"formId\" : 1, \"dateCreated\" : { \"$gte\" : { \"$numberLong\" : \"" + from.getTime() + "\" }, \"$lte\" : { \"$numberLong\" : \"" + to.getTime() + "\" } } }";
        SearchTermFilter searchTermFilter = new ImportFilterBuilder(new Form(FORM_ID))
                .from(from)
                .to(to)
                .build();
        String actualExpression = searchTermFilter.getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void testGetCriteriaWhenUserIsSupplied() throws Exception {
        Integer userId = 6;
        String expectedExpression = "{ \"formId\" : 1, \"userId\" : " + userId + " }";
        SearchTermFilter searchTermFilter = new ImportFilterBuilder(new Form(FORM_ID))
                .userId(userId)
                .build();
        String actualExpression = searchTermFilter.getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void testGetCriteriaWhenOnlyFromDateIsSupplied() throws Exception {
        Calendar calendar = Calendar.getInstance();
        Date from = calendar.getTime();
        String expectedExpression = "{ \"formId\" : 1, \"dateCreated\" : { \"$gte\" : { \"$numberLong\" : \""+ from.getTime() +"\" } } }";
        SearchTermFilter searchTermFilter = new ImportFilterBuilder(new Form(FORM_ID))
                .from(from)
                .build();
        String actualExpression = searchTermFilter.getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

}