package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.response.NumberResponse;
import ke.co.hoji.core.response.SingleChoiceResponse;
import ke.co.hoji.core.service.model.TranslationService;
import ke.co.hoji.server.model.DataRow;
import ke.co.hoji.server.model.DataTable;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RecordSnapshotMapperTests {

    @Mock
    private UserService userService;

    @Mock
    private HojiContext hojiContext;

    @Mock
    private ModelServiceManager modelServiceManager;

    @Mock
    private TranslationService translationService;

    @Test
    public void mapLiveFieldsState_shouldReturnSnapshotForRecordWithNumericField() {
        Field numericField = new Field(1);
        numericField.setDescription("Q<b>#1</b>");
        numericField.setColumn("Column");
        FieldType numeric = new FieldType(1, "Number", FieldType.Code.WHOLE_NUMBER);
        numeric.setDataType(FieldType.DataType.NUMBER);
        numeric.setResponseClass(NumberResponse.class.getName());
        numericField.setType(numeric);
        numericField.setEnabled(true);
        Form form = new Form(1);
        form.setFields(Collections.singletonList(numericField));
        numericField.setForm(form);

        LiveField liveField = new LiveField();
        liveField.setUuid("L_UID_1");
        liveField.setFieldId(1);
        liveField.setValue(28);
        liveField.setDateCreated(1494860970714L);
        liveField.setDateUpdated(1494860970714L);
        MainRecord mainRecord = new MainRecord();
        mainRecord.setUuid("UID_1");
        mainRecord.setCaption("28");
        mainRecord.setLiveFields(Collections.singletonList(liveField));
        mainRecord.setUserId(1);
        mainRecord.setFormVersion("1");
        mainRecord.setVersion(2);
        mainRecord.setStartAddress("Loresho");
        mainRecord.setStartLatitude(-1.2564698D);
        mainRecord.setStartLongitude(36.7569469D);
        mainRecord.setEndLatitude(-1.2564698D);
        mainRecord.setEndLongitude(36.7569469D);
        mainRecord.setDateCreated(1494860970714L);
        mainRecord.setDateCompleted(1494860980714L);
        mainRecord.setDateUpdated(1494860970714L);
        mainRecord.setDateUploaded(1494860975714L);
        Stream<MainRecord> mainRecordStream = Stream.of(mainRecord);
        User user = new User();
        user.setFullName("Test User");
        when(userService.findById(1)).thenReturn(user);

        DataTable dataTable = new RecordSnapshotMapper(userService, hojiContext).mapDataRows(mainRecordStream, form);

        List<DataRow> dataRows = dataTable.getDataRows();
        assertThat(dataRows, hasSize(1));
        assertThat(dataRows.get(0).getDataElements(), hasSize(15));
        assertThat(dataRows.get(0).getDataElements().get(0).getValue(), is(mainRecord.getUuid()));
        assertThat(dataRows.get(0).getDataElements().get(1).getValue(), is(mainRecord.getCaption()));
        assertThat(dataRows.get(0).getDataElements().get(2).getValue(), is(user.getFullName()));
        assertThat(dataRows.get(0).getDataElements().get(3).getValue(), is("2017-05-15 18:09:30"));
        assertThat(dataRows.get(0).getDataElements().get(4).getValue(), is("2017-05-15 18:09:40"));
        assertThat(dataRows.get(0).getDataElements().get(5).getValue(), is("2017-05-15 18:09:30"));
        assertThat(dataRows.get(0).getDataElements().get(6).getValue(), is("2017-05-15 18:09:35"));
        assertThat(dataRows.get(0).getDataElements().get(7).getValue(), is(mainRecord.getVersion().toString()));
        assertThat(dataRows.get(0).getDataElements().get(8).getValue(), is("Yes"));
        assertThat(dataRows.get(0).getDataElements().get(9).getValue(), is(mainRecord.getStartAddress()));
        assertThat(dataRows.get(0).getDataElements().get(10).getValue(), is("Column"));
        assertThat(dataRows.get(0).getDataElements().get(11).getValue(), is("28"));
        assertThat(dataRows.get(0).getDataElements().get(12).getValue(), is("2017-05-15 18:09:30"));
        assertThat(dataRows.get(0).getDataElements().get(13).getValue(), is("2017-05-15 18:09:30"));
        assertThat(dataRows.get(0).getDataElements().get(14).getValue(), is("No"));
    }

    @Test
    public void mapLiveFieldsState_shouldReturnSnapshotForRecordWithChoiceField() {
        Choice yes = new Choice(1, "Yes", "Y", 0, true);
        Choice no = new Choice(2, "No", "N", 0, true);
        ChoiceGroup yesNo = new ChoiceGroup(Arrays.asList(yes, no));
        Field choiceField = new Field(1);
        choiceField.setDescription("Q<b>#1</b>");
        choiceField.setChoiceGroup(yesNo);
        choiceField.setEnabled(true);
        FieldType choiceType = new FieldType(1, "Choice", FieldType.Code.SINGLE_CHOICE);
        choiceType.setDataType(FieldType.DataType.STRING);
        choiceType.setResponseClass(SingleChoiceResponse.class.getName());
        choiceField.setType(choiceType);
        Form form = new Form(1);
        form.setFields(Collections.singletonList(choiceField));
        choiceField.setForm(form);

        LiveField liveField = new LiveField();
        liveField.setUuid("L_UID_1");
        liveField.setFieldId(1);
        liveField.setValue(1L);
        liveField.setDateCreated(1494860970714L);
        liveField.setDateUpdated(1494860970714L);
        MainRecord mainRecord = new MainRecord();
        mainRecord.setUuid("UID_1");
        mainRecord.setCaption("Yes");
        mainRecord.setLiveFields(Collections.singletonList(liveField));
        mainRecord.setUserId(1);
        mainRecord.setFormVersion("1");
        mainRecord.setVersion(2);
        mainRecord.setStartAddress("Loresho");
        mainRecord.setStartLatitude(-1.2564698D);
        mainRecord.setStartLongitude(36.7569469D);
        mainRecord.setEndLatitude(-1.2564698D);
        mainRecord.setEndLongitude(36.7569469D);
        mainRecord.setDateCreated(1494860970714L);
        mainRecord.setDateCompleted(1494860980714L);
        mainRecord.setDateUpdated(1494860970714L);
        mainRecord.setDateUploaded(1494860975714L);
        Stream<MainRecord> mainRecordStream = Stream.of(mainRecord);
        User user = new User();
        user.setFullName("Test User");
        when(userService.findById(1)).thenReturn(user);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getTranslationService()).thenReturn(translationService);
        when(translationService.translate(any(), any(), any(), any())).thenReturn(yes.getName());

        DataTable dataTable = new RecordSnapshotMapper(userService, hojiContext).mapDataRows(mainRecordStream, form);

        List<DataRow> dataRows = dataTable.getDataRows();
        assertThat(dataRows, hasSize(1));
        assertThat(dataRows.get(0).getDataElements(), hasSize(15));
        assertThat(dataRows.get(0).getDataElements().get(0).getValue(), is(mainRecord.getUuid()));
        assertThat(dataRows.get(0).getDataElements().get(1).getValue(), is(mainRecord.getCaption()));
        assertThat(dataRows.get(0).getDataElements().get(2).getValue(), is(user.getFullName()));
        assertThat(dataRows.get(0).getDataElements().get(3).getValue(), is("2017-05-15 18:09:30"));
        assertThat(dataRows.get(0).getDataElements().get(4).getValue(), is("2017-05-15 18:09:40"));
        assertThat(dataRows.get(0).getDataElements().get(5).getValue(), is("2017-05-15 18:09:30"));
        assertThat(dataRows.get(0).getDataElements().get(6).getValue(), is("2017-05-15 18:09:35"));
        assertThat(dataRows.get(0).getDataElements().get(7).getValue(), is(mainRecord.getVersion().toString()));
        assertThat(dataRows.get(0).getDataElements().get(8).getValue(), is("Yes"));
        assertThat(dataRows.get(0).getDataElements().get(9).getValue(), is(mainRecord.getStartAddress()));
        assertThat(dataRows.get(0).getDataElements().get(10).getValue(), is("Q#1"));
        assertThat(dataRows.get(0).getDataElements().get(11).getValue(), is("Yes"));
        assertThat(dataRows.get(0).getDataElements().get(12).getValue(), is("2017-05-15 18:09:30"));
        assertThat(dataRows.get(0).getDataElements().get(13).getValue(), is("2017-05-15 18:09:30"));
        assertThat(dataRows.get(0).getDataElements().get(14).getValue(), is("No"));
    }

    @Test
    public void mapLiveFieldsState_shouldReturnSnapshotForRecordTwoFields() {
        Choice yes = new Choice(1, "Yes", "Y", 0, true);
        Choice no = new Choice(2, "No", "N", 0, true);
        ChoiceGroup yesNo = new ChoiceGroup(Arrays.asList(yes, no));
        Field choiceField = new Field(1);
        choiceField.setDescription("Q<b>#1</b>");
        choiceField.setChoiceGroup(yesNo);
        choiceField.setEnabled(true);
        FieldType choiceType = new FieldType(1, "Choice", FieldType.Code.SINGLE_CHOICE);
        choiceType.setDataType(FieldType.DataType.STRING);
        choiceType.setResponseClass(SingleChoiceResponse.class.getName());
        choiceField.setType(choiceType);
        Field numericField = new Field(2);
        numericField.setDescription("Q<b>#2</b>");
        FieldType numeric = new FieldType(2, "Number", FieldType.Code.WHOLE_NUMBER);
        numeric.setDataType(FieldType.DataType.NUMBER);
        numeric.setResponseClass(NumberResponse.class.getName());
        numericField.setType(numeric);
        numericField.setEnabled(true);
        Form form = new Form(1);
        form.setFields(Arrays.asList(choiceField, numericField));
        choiceField.setForm(form);
        numericField.setForm(form);

        LiveField choiceLf = new LiveField();
        choiceLf.setUuid("L_UID_1");
        choiceLf.setFieldId(1);
        choiceLf.setValue(1L);
        choiceLf.setDateCreated(1494860970714L);
        choiceLf.setDateUpdated(1494861070714L);
        LiveField numericLf = new LiveField();
        numericLf.setUuid("L_UID_2");
        numericLf.setFieldId(2);
        numericLf.setValue(28);
        numericLf.setDateCreated(1494860970714L);
        numericLf.setDateUpdated(1494861070714L);
        MainRecord mainRecord = new MainRecord();
        mainRecord.setUuid("UID_1");
        mainRecord.setCaption("28");
        mainRecord.setLiveFields(Arrays.asList(choiceLf, numericLf));
        mainRecord.setUserId(1);
        mainRecord.setFormVersion("1");
        mainRecord.setVersion(2);
        mainRecord.setStartAddress("Loresho");
        mainRecord.setStartLatitude(-1.2564698D);
        mainRecord.setStartLongitude(36.7569469D);
        mainRecord.setEndLatitude(-1.2564698D);
        mainRecord.setEndLongitude(36.7569469D);
        mainRecord.setDateCreated(1494860970714L);
        mainRecord.setDateCompleted(1494860980714L);
        mainRecord.setDateUpdated(1494860970714L);
        mainRecord.setDateUploaded(1494860975714L);
        Stream<MainRecord> mainRecordStream = Stream.of(mainRecord);
        User user = new User();
        user.setFullName("Test User");
        when(userService.findById(1)).thenReturn(user);
        when(hojiContext.getModelServiceManager()).thenReturn(modelServiceManager);
        when(modelServiceManager.getTranslationService()).thenReturn(translationService);
        when(translationService.translate(any(), any(), any(), any())).thenReturn(yes.getName());

        DataTable dataTable = new RecordSnapshotMapper(userService, hojiContext).mapDataRows(mainRecordStream, form);

        List<DataRow> dataRows = dataTable.getDataRows();
        assertThat(dataRows, hasSize(2));
        assertThat(dataRows.get(0).getDataElements(), hasSize(15));
        assertThat(dataRows.get(0).getDataElements().get(0).getValue(), is(mainRecord.getUuid()));
        assertThat(dataRows.get(0).getDataElements().get(1).getValue(), is(mainRecord.getCaption()));
        assertThat(dataRows.get(0).getDataElements().get(2).getValue(), is(user.getFullName()));
        assertThat(dataRows.get(0).getDataElements().get(3).getValue(), is("2017-05-15 18:09:30"));
        assertThat(dataRows.get(0).getDataElements().get(4).getValue(), is("2017-05-15 18:09:40"));
        assertThat(dataRows.get(0).getDataElements().get(5).getValue(), is("2017-05-15 18:09:30"));
        assertThat(dataRows.get(0).getDataElements().get(6).getValue(), is("2017-05-15 18:09:35"));
        assertThat(dataRows.get(0).getDataElements().get(7).getValue(), is(mainRecord.getVersion().toString()));
        assertThat(dataRows.get(0).getDataElements().get(8).getValue(), is("Yes"));
        assertThat(dataRows.get(0).getDataElements().get(9).getValue(), is(mainRecord.getStartAddress()));
        assertThat(dataRows.get(0).getDataElements().get(10).getValue(), is("Q#1"));
        assertThat(dataRows.get(0).getDataElements().get(11).getValue(), is("Yes"));
        assertThat(dataRows.get(0).getDataElements().get(12).getValue(), is("2017-05-15 18:09:30"));
        assertThat(dataRows.get(0).getDataElements().get(13).getValue(), is("2017-05-15 18:11:10"));
        assertThat(dataRows.get(0).getDataElements().get(14).getValue(), is("Yes"));
        assertThat(dataRows.get(1).getDataElements(), hasSize(15));
        assertThat(dataRows.get(1).getDataElements().get(0).getValue(), is(mainRecord.getUuid()));
        assertThat(dataRows.get(1).getDataElements().get(1).getValue(), is(mainRecord.getCaption()));
        assertThat(dataRows.get(1).getDataElements().get(2).getValue(), is(user.getFullName()));
        assertThat(dataRows.get(1).getDataElements().get(3).getValue(), is("2017-05-15 18:09:30"));
        assertThat(dataRows.get(1).getDataElements().get(4).getValue(), is("2017-05-15 18:09:40"));
        assertThat(dataRows.get(1).getDataElements().get(5).getValue(), is("2017-05-15 18:09:30"));
        assertThat(dataRows.get(1).getDataElements().get(6).getValue(), is("2017-05-15 18:09:35"));
        assertThat(dataRows.get(1).getDataElements().get(7).getValue(), is(mainRecord.getVersion().toString()));
        assertThat(dataRows.get(1).getDataElements().get(8).getValue(), is("Yes"));
        assertThat(dataRows.get(1).getDataElements().get(9).getValue(), is(mainRecord.getStartAddress()));
        assertThat(dataRows.get(1).getDataElements().get(10).getValue(), is("Q#2"));
        assertThat(dataRows.get(1).getDataElements().get(11).getValue(), is("28"));
        assertThat(dataRows.get(1).getDataElements().get(12).getValue(), is("2017-05-15 18:09:30"));
        assertThat(dataRows.get(1).getDataElements().get(13).getValue(), is("2017-05-15 18:11:10"));
        assertThat(dataRows.get(1).getDataElements().get(14).getValue(), is("Yes"));
    }

}
