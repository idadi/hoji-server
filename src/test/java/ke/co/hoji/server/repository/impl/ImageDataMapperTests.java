package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.MatrixRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.model.ImageData;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class ImageDataMapperTests {

    @Test
    public void mapDataTable_shouldReturnCollectionOfImageData() {
        Field imageField = new Field(1);
        Form form = new Form(1);
        form.setFields(Collections.singletonList(imageField));
        LiveField imageLf = new LiveField();
        imageLf.setFieldId(imageField.getId());
        imageLf.setValue("/tmp/images/image.png");
        MainRecord mainRecord = new MainRecord();
        mainRecord.setCaption("Image Caption");
        mainRecord.setLiveFields(Collections.singletonList(imageLf));

        List<ImageData> images = new ImageDataMapper("/tmp/images/").mapDataTable(Stream.of(mainRecord), form);

        assertThat(images, hasSize(1));
        assertThat(images.get(0).getCaption(), equalTo(mainRecord.getCaption()));
        assertThat(images.get(0).getSrc(), equalTo("/images/image.png"));
    }

    @Test
    public void mapDataTable_shouldReturnCollectionOfImageDataForMatrixRecord() {
        Field parent = new Field(1);
        Field imageField = new Field(2);
        parent.setChildren(Collections.singletonList(imageField));
        LiveField imageLf = new LiveField();
        imageLf.setFieldId(imageField.getId());
        imageLf.setValue("/tmp/images/image.png");
        MatrixRecord matrixRecord = new MatrixRecord();
        matrixRecord.setCaption("Matrix Caption");
        matrixRecord.setLiveFields(Collections.singletonList(imageLf));
        MainRecord mainRecord = new MainRecord();
        mainRecord.setMatrixRecords(Collections.singletonList(matrixRecord));

        List<ImageData> images = new ImageDataMapper("/tmp/images/")
                .mapDataTable(Stream.of(mainRecord), parent);

        assertThat(images, hasSize(1));
        assertThat(images.get(0).getCaption(), equalTo(matrixRecord.getCaption()));
        assertThat(images.get(0).getSrc(), equalTo("/images/image.png"));
    }
}