package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.dto.DataValue;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.MatrixRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.response.MatrixResponse;
import ke.co.hoji.core.response.MultipleChoiceResponse;
import ke.co.hoji.core.response.NumberResponse;
import ke.co.hoji.core.response.ReferenceResponse;
import ke.co.hoji.core.response.SingleChoiceResponse;
import ke.co.hoji.server.model.ApiRecord;
import ke.co.hoji.server.model.Metadata;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.hasValue;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApiRecordMapperTests {

    @Mock
    private UserService userService;

    @Mock
    private HojiContext hojiContext;

    @Before
    public void setup() {
        User user = new User();
        user.setFirstName("Test");
        user.setLastName("User");
        when(userService.findById(anyInt())).thenReturn(user);
    }

    @Test
    public void shouldReturnMetadata() {
        Form form = new Form(1);
        form.setFields(Collections.EMPTY_LIST);
        MainRecord mainRecord = getMainRecord(Collections.EMPTY_LIST);

        Optional<ApiRecord> apiRecord = new ApiRecordMapper(userService, hojiContext)
                .getApiRecord(Collections.singletonList(mainRecord), form);

        Assert.assertTrue(apiRecord.isPresent());
        Assert.assertThat(apiRecord.get().getMetadata(), hasKey(Metadata.Label.FORM_VERSION));
        Assert.assertThat(apiRecord.get().getMetadata(), hasValue(mainRecord.getFormVersion()));
        Assert.assertThat(apiRecord.get().getMetadata(), hasKey(Metadata.Label.APP_VERSION));
        Assert.assertThat(apiRecord.get().getMetadata(), hasValue(mainRecord.getAppVersion()));
        Assert.assertThat(apiRecord.get().getMetadata(), hasKey(Metadata.Label.CREATED_BY));
        Assert.assertThat(apiRecord.get().getMetadata(), hasValue("Test User"));
    }

    @Test
    public void shouldReturnApiRecordForFormWithNumericField() {
        FieldType numeric = getFieldType("Number", FieldType.Code.WHOLE_NUMBER, FieldType.DataType.NUMBER, NumberResponse.class.getName());
        Field field = getField(1, true, "field_1", numeric, null);
        Form form = getForm(field);
        LiveField liveField = new LiveField();
        liveField.setUuid("L_UID_1");
        liveField.setFieldId(1);
        liveField.setValue(20);
        MainRecord mainRecord = getMainRecord(Collections.singletonList(liveField));

        Optional<ApiRecord> apiRecord = new ApiRecordMapper(userService, hojiContext)
                .getApiRecord(Collections.singletonList(mainRecord), form);

        Assert.assertTrue(apiRecord.isPresent());
        Assert.assertThat(apiRecord.get().getData().size(), is(1));
        Assert.assertThat(apiRecord.get().getData(), hasKey(field.getColumn()));
        DataValue expected = new DataValue(liveField.getValue().toString(), DataElement.Type.NUMBER);
        Assert.assertThat(apiRecord.get().getData(), hasValue(expected));
    }

    @Test
    public void shouldReturnApiRecordWithEmptyDataValueForSkippedSimpleField() {
        FieldType numeric = getFieldType("Number", FieldType.Code.WHOLE_NUMBER, FieldType.DataType.NUMBER, NumberResponse.class.getName());
        Field field = getField(1, true, "field_1", numeric, null);
        Form form = getForm(field);
        MainRecord mainRecord = getMainRecord(Collections.EMPTY_LIST);

        Optional<ApiRecord> apiRecord = new ApiRecordMapper(userService, hojiContext)
                .getApiRecord(Collections.singletonList(mainRecord), form);

        Assert.assertTrue(apiRecord.isPresent());
        Assert.assertThat(apiRecord.get().getData().size(), is(1));
        Assert.assertThat(apiRecord.get().getData(), hasKey(field.getColumn()));
        DataValue expected = new DataValue("", DataElement.Type.NUMBER);
        Assert.assertThat(apiRecord.get().getData(), hasValue(expected));
    }

    @Test
    public void shouldReturnApiRecordWithNoDataForDisabledSimpleField() {
        FieldType numeric = getFieldType("Number", FieldType.Code.WHOLE_NUMBER, FieldType.DataType.NUMBER, NumberResponse.class.getName());
        Field field = getField(1, false, "field_1", numeric, null);
        Form form = getForm(field);
        LiveField liveField = new LiveField();
        liveField.setUuid("L_UID_1");
        liveField.setFieldId(1);
        liveField.setValue(20);
        MainRecord mainRecord = getMainRecord(Collections.singletonList(liveField));

        Optional<ApiRecord> apiRecord = new ApiRecordMapper(userService, hojiContext)
                .getApiRecord(Collections.singletonList(mainRecord), form);

        Assert.assertTrue(apiRecord.isPresent());
        Assert.assertThat(apiRecord.get().getData().size(), is(0));
    }

    @Test
    public void shouldReturnApiRecordForFormWithSingleChoiceField() {
        Choice yes = new Choice(1, "Yes", "Y", 0, false);
        Choice no = new Choice(2, "No", "N", 0, false);
        ChoiceGroup yesNo = new ChoiceGroup(Arrays.asList(yes, no));
        FieldType singleChoice = getFieldType("Single Choice", FieldType.Code.SINGLE_CHOICE, FieldType.DataType.STRING, SingleChoiceResponse.class.getName());
        Field field = getField(1, true, "field_1", singleChoice, yesNo);
        Form form = getForm(field);
        LiveField liveField = new LiveField();
        liveField.setUuid("L_UID_1");
        liveField.setFieldId(1);
        liveField.setValue(Long.valueOf(1));
        MainRecord mainRecord = getMainRecord(Collections.singletonList(liveField));

        Optional<ApiRecord> apiRecord = new ApiRecordMapper(userService, hojiContext)
                .getApiRecord(Collections.singletonList(mainRecord), form);

        Assert.assertTrue(apiRecord.isPresent());
        Assert.assertThat(apiRecord.get().getData().size(), is(1));
        Assert.assertThat(apiRecord.get().getData(), hasKey(field.getColumn()));
        DataValue expected = new DataValue(yes.getName(), DataElement.Type.STRING);
        Map<String, Object> map = apiRecord.get().getData();
        Assert.assertThat(map, hasValue(expected));
    }

    @Test
    public void shouldReturnApiRecordForFormWithMultipleChoiceField() {
        Choice basketball = new Choice(1, "Basketball", "1", 0, false);
        Choice football = new Choice(2, "Football", "2", 0, false);
        Choice rugby = new Choice(3, "Rugby", "3", 0, false);
        ChoiceGroup sports = new ChoiceGroup(Arrays.asList(basketball, football, rugby));
        FieldType multipleChoice = getFieldType("Multiple Choice", FieldType.Code.MULTIPLE_CHOICE, FieldType.DataType.STRING, MultipleChoiceResponse.class.getName());
        Field field = getField(1, true, "field_1", multipleChoice, sports);
        Form form = getForm(field);
        LiveField liveField = new LiveField();
        liveField.setUuid("L_UID_1");
        liveField.setFieldId(1);
        liveField.setValue("1#0|2#1|3#1");
        MainRecord mainRecord = getMainRecord(Collections.singletonList(liveField));

        Optional<ApiRecord> apiRecord = new ApiRecordMapper(userService, hojiContext)
                .getApiRecord(Collections.singletonList(mainRecord), form);

        Assert.assertTrue(apiRecord.isPresent());
        Assert.assertThat(apiRecord.get().getData().size(), is(1));
        Assert.assertThat(apiRecord.get().getData(), hasKey(field.getColumn()));
        Assert.assertThat((Map<String, Object>) apiRecord.get().getData().get(field.getColumn()),
                hasValue(new String[]{football.getName(), rugby.getName()}));
    }

    @Test
    public void shouldReturnApiRecordWithEmptyDataValueForSkippedMultipleChoiceField() {
        Choice basketball = new Choice(1, "Basketball", "1", 0, false);
        Choice football = new Choice(2, "Football", "2", 0, false);
        Choice rugby = new Choice(3, "Rugby", "3", 0, false);
        ChoiceGroup sports = new ChoiceGroup(Arrays.asList(basketball, football, rugby));
        FieldType multipleChoice = getFieldType("Multiple Choice", FieldType.Code.MULTIPLE_CHOICE, FieldType.DataType.STRING, MultipleChoiceResponse.class.getName());
        Field field = getField(1, true, "field_1", multipleChoice, sports);
        Form form = getForm(field);
        MainRecord mainRecord = getMainRecord(Collections.EMPTY_LIST);

        Optional<ApiRecord> apiRecord = new ApiRecordMapper(userService, hojiContext)
                .getApiRecord(Collections.singletonList(mainRecord), form);

        Assert.assertTrue(apiRecord.isPresent());
        Assert.assertThat(apiRecord.get().getData().size(), is(1));
        Assert.assertThat(apiRecord.get().getData(), hasKey(field.getColumn()));
        Assert.assertThat(((Map) apiRecord.get().getData().get(field.getColumn())).size(), is(0));
    }

    @Test
    public void shouldReturnApiRecordForFormWithMatrixField() {
        FieldType matrix = getFieldType("Matrix", FieldType.Code.MATRIX, FieldType.DataType.NUMBER, MatrixResponse.class.getName());
        Field parent = getField(1, true, "parent", matrix, null);
        FieldType numeric = getFieldType("Numeric", FieldType.Code.WHOLE_NUMBER, FieldType.DataType.NUMBER, NumberResponse.class.getName());
        Field child = getField(2, true, "child", numeric, null);
        parent.setChildren(Collections.singletonList(child));
        Form form = getForm(parent, child);
        LiveField parentLf = new LiveField();
        parentLf.setUuid("L_UID_1");
        parentLf.setFieldId(1);
        parentLf.setValue("1");
        MainRecord mainRecord = getMainRecord(Collections.singletonList(parentLf));
        LiveField childLf = new LiveField();
        childLf.setUuid("L_UID_2");
        childLf.setFieldId(2);
        childLf.setValue(20);
        MatrixRecord matrixRecord = new MatrixRecord();
        matrixRecord.setFieldId(1);
        matrixRecord.setLiveFields(Collections.singletonList(childLf));
        mainRecord.setMatrixRecords(Collections.singletonList(matrixRecord));

        Optional<ApiRecord> apiRecord = new ApiRecordMapper(userService, hojiContext)
                .getApiRecord(Collections.singletonList(mainRecord), form);

        Assert.assertTrue(apiRecord.isPresent());
        Assert.assertThat(apiRecord.get().getData().size(), is(1));
        Assert.assertThat(apiRecord.get().getData(), hasKey(parent.getColumn()));
        Map<String, Object> data = apiRecord.get().getData();
        List<Map<String, Object>> matrices = (List<Map<String, Object>>) data.get(parent.getColumn());
        Assert.assertThat(matrices, hasSize(1));
        Map<String, Object> matrixData = matrices.get(0);
        Assert.assertThat(matrixData, hasKey(child.getColumn()));
    }

    @Test
    public void shouldSkipReferenceFields() {
        FieldType reference = getFieldType("Reference", FieldType.Code.REFERENCE, FieldType.DataType.NUMBER, ReferenceResponse.class.getName());
        Field field = getField(1, true, "field", reference, null);
        Form form = getForm(field);
        LiveField liveField = new LiveField();
        liveField.setUuid("L_UID_1");
        liveField.setFieldId(1);
        liveField.setValue("R_UID_1");
        MainRecord mainRecord = getMainRecord(Collections.singletonList(liveField));

        Optional<ApiRecord> apiRecord = new ApiRecordMapper(userService, hojiContext)
                .getApiRecord(Collections.singletonList(mainRecord), form);

        Assert.assertTrue(apiRecord.isPresent());
        Assert.assertThat(apiRecord.get().getData().size(), is(1));
        Assert.assertThat(apiRecord.get().getData(), hasKey(field.getColumn()));
        Assert.assertThat(apiRecord.get().getData(), hasValue(liveField.getValue()));
    }

    private FieldType getFieldType(String name, String code, int dataType, String responseClass) {
        FieldType fieldType = new FieldType(1, name, code);
        fieldType.setDataType(dataType);
        fieldType.setResponseClass(responseClass);
        return fieldType;
    }

    private Form getForm(Field... fields) {
        Form form = new Form(1);
        form.setFields(Arrays.asList(fields));
        for (Field field : fields) {
            field.setForm(form);
        }
        return form;
    }

    private Field getField(int id, boolean enabled, String column, FieldType type, ChoiceGroup choiceGroup) {
        Field field = new Field(id);
        field.setEnabled(enabled);
        field.setChoiceGroup(choiceGroup);
        field.setColumn(column);
        field.setType(type);
        return field;
    }

    private MainRecord getMainRecord(List<LiveField> liveFields) {
        MainRecord mainRecord = new MainRecord();
        mainRecord.setLiveFields(liveFields);
        mainRecord.setUuid("UID_1");
        mainRecord.setUserId(1);
        mainRecord.setFormVersion("1");
        mainRecord.setVersion(1);
        mainRecord.setStartLatitude(0D);
        mainRecord.setStartLongitude(0D);
        mainRecord.setEndLatitude(0D);
        mainRecord.setEndLongitude(0D);
        mainRecord.setDateCreated(1494860970714L);
        mainRecord.setDateCompleted(1494860980714L);
        mainRecord.setDateUpdated(1494860970714L);
        mainRecord.setDateUploaded(1494860975714L);
        return mainRecord;
    }
}
