package ke.co.hoji.server.repository;

import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by geoffreywasilwa on 05/06/2017.
 */
public class MainRecordPropertiesFilterTests {

    private static final int FORM_ID = 1;
    private Date from = DateTime.now().minusMonths(1).toDate();
    private Date to = DateTime.now().toDate();

    @Test
    public void testGetCriteriaWhenTestModeIsTrue() throws Exception {
        String expectedExpression = "{ \"formId\" : 1, \"test\" : true }";

        String actualExpression = new MainRecordPropertiesFilterBuilder(new Form(FORM_ID))
                .testMode(true)
                .build().getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void testGetCriteriaWhilePassingInNullValues() throws Exception {
        String expectedExpression = "{ \"formId\" : 1, \"test\" : false }";

        String actualExpression = new MainRecordPropertiesFilterBuilder(new Form(FORM_ID))
                .createdFrom(null)
                .createdTo(null)
                .completedFrom(null)
                .completedTo(null)
                .uploadedFrom(null)
                .uploadedTo(null)
                .userIds(null)
                .recordUuid()
                .filterFieldId(null)
                .filterFieldValue(null)
                .build().getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void testGetCriteriaWhenCreatedDatesAreProvided() throws Exception {
        String expectedExpression = "{ \"formId\" : 1, \"test\" : false, \"dateCreated\" : { \"$gt\" : { \"$numberLong\" : \""+ from.getTime() +"\" }, \"$lt\" : { \"$numberLong\" : \""+ to.getTime() +"\" } } }";

        String actualExpression = new MainRecordPropertiesFilterBuilder(new Form(FORM_ID))
                .createdFrom(from)
                .createdTo(to)
                .build().getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void testGetCriteriaWhenCreatedFromIsProvided() throws Exception {
        String expectedExpression = "{ \"formId\" : 1, \"test\" : false, \"dateCreated\" : { \"$gt\" : { \"$numberLong\" : \""+ from.getTime() +"\" } } }";

        String actualExpression = new MainRecordPropertiesFilterBuilder(new Form(FORM_ID))
                .createdFrom(from)
                .build().getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void testGetCriteriaWhenCreatedToIsProvided() throws Exception {
        String expectedExpression = "{ \"formId\" : 1, \"test\" : false, \"dateCreated\" : { \"$lt\" : { \"$numberLong\" : \""+ from.getTime() +"\" } } }";

        String actualExpression = new MainRecordPropertiesFilterBuilder(new Form(FORM_ID))
                .createdTo(from)
                .build().getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void testGetCriteriaWhenCompletedDatesAreProvided() throws Exception {
        String expectedExpression = "{ \"formId\" : 1, \"test\" : false, \"dateCompleted\" : { \"$gte\" : { \"$numberLong\" : \""+ from.getTime() +"\" }, \"$lte\" : { \"$numberLong\" : \""+ to.getTime() +"\" } } }";

        String actualExpression = new MainRecordPropertiesFilterBuilder(new Form(FORM_ID))
                .completedFrom(from)
                .completedTo(to)
                .build().getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void testGetCriteriaWhenCompletedFromIsProvided() throws Exception {
        String expectedExpression = "{ \"formId\" : 1, \"test\" : false, \"dateCompleted\" : { \"$gte\" : { \"$numberLong\" : \""+ from.getTime() +"\" } } }";

        String actualExpression = new MainRecordPropertiesFilterBuilder(new Form(FORM_ID))
                .completedFrom(from)
                .build().getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void testGetCriteriaWhenCompletedToIsProvided() throws Exception {
        String expectedExpression = "{ \"formId\" : 1, \"test\" : false, \"dateCompleted\" : { \"$lte\" : { \"$numberLong\" : \""+ from.getTime() +"\" } } }";

        String actualExpression = new MainRecordPropertiesFilterBuilder(new Form(FORM_ID))
                .completedTo(from)
                .build().getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void testGetCriteriaWhenUploadedDatesAreProvided() throws Exception {
        String expectedExpression = "{ \"formId\" : 1, \"test\" : false, \"dateUploaded\" : { \"$gte\" : { \"$numberLong\" : \""+ from.getTime() +"\" }, \"$lte\" : { \"$numberLong\" : \""+ to.getTime() +"\" } } }";

        String actualExpression = new MainRecordPropertiesFilterBuilder(new Form(FORM_ID))
                .uploadedFrom(from)
                .uploadedTo(to)
                .build().getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void testGetCriteriaWhenUploadedFromIsProvided() throws Exception {
        String expectedExpression = "{ \"formId\" : 1, \"test\" : false, \"dateUploaded\" : { \"$gte\" : { \"$numberLong\" : \""+ from.getTime() +"\" } } }";

        String actualExpression = new MainRecordPropertiesFilterBuilder(new Form(FORM_ID))
                .uploadedFrom(from)
                .build().getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void testGetCriteriaWhenUploadedToIsProvided() throws Exception {
        String expectedExpression = "{ \"formId\" : 1, \"test\" : false, \"dateUploaded\" : { \"$lte\" : { \"$numberLong\" : \""+ from.getTime() +"\" } } }";

        String actualExpression = new MainRecordPropertiesFilterBuilder(new Form(FORM_ID))
                .uploadedTo(from)
                .build().getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void testGetCriteriaWhenUserIdsAreProvided() throws Exception {
        String expectedExpression =
                "{ \"formId\" : 1, \"test\" : false, \"userId\" : { \"$in\" : [1, 2] } }";

        String actualExpression = new MainRecordPropertiesFilterBuilder(new Form(FORM_ID))
                .userIds(asList(1, 2))
                .build().getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void testGetCriteriaWhenUuidsAreProvided() throws Exception {
        String expectedExpression =
                "{ \"formId\" : 1, \"test\" : false, \"uuid\" : { \"$in\" : [\"UUID1\", \"UUID2\"] } }";

        String actualExpression = new MainRecordPropertiesFilterBuilder(new Form(FORM_ID))
                .recordUuid("UUID1", "UUID2")
                .build().getCriteria().getCriteriaObject().toJson();

        Assert.assertEquals(expectedExpression, actualExpression);
    }

    @Test
    public void passesFilterFieldCriteria_shouldReturnTrueWhenLiveFieldMatchesFieldFilterCriteria() {
        LiveField nameLf = new LiveField();
        nameLf.setFieldId(1);
        nameLf.setValue("Test User");
        LiveField agreeLf = new LiveField();
        agreeLf.setFieldId(2);
        agreeLf.setValue(1);

        MainRecordPropertiesFilter filter =
                new MainRecordPropertiesFilterBuilder(new Form(1)).filterFieldId(2).filterFieldValue(1).build();

        assertTrue(filter.passesFilterFieldCriteria(asList(nameLf, agreeLf)));
    }

    @Test
    public void passesFilterFieldCriteria_shouldReturnFalseWhenLiveFieldDoesNotMatchFieldFilterCriteria() {
        LiveField nameLf = new LiveField();
        nameLf.setFieldId(1);
        nameLf.setValue("Test User");
        LiveField agreeLf = new LiveField();
        agreeLf.setFieldId(2);
        agreeLf.setValue(2L);

        MainRecordPropertiesFilter filter =
                new MainRecordPropertiesFilterBuilder(new Form(1)).filterFieldId(2).filterFieldValue(1).build();

        assertFalse(filter.passesFilterFieldCriteria(asList(nameLf, agreeLf)));
    }

    @Test
    public void passesFilterFieldCriteria_shouldReturnTrueWhenPartialFieldFilterCriteriaIsSupplied() {
        LiveField nameLf = new LiveField();
        nameLf.setFieldId(1);
        nameLf.setValue("Test User");
        LiveField agreeLf = new LiveField();
        agreeLf.setFieldId(2);
        agreeLf.setValue(2L);

        MainRecordPropertiesFilter filter =
                new MainRecordPropertiesFilterBuilder(new Form(1)).filterFieldId(1).build();

        assertTrue(filter.passesFilterFieldCriteria(asList(nameLf, agreeLf)));

        filter = new MainRecordPropertiesFilterBuilder(new Form(1)).filterFieldValue(1).build();

        assertTrue(filter.passesFilterFieldCriteria(asList(nameLf, agreeLf)));
    }

}