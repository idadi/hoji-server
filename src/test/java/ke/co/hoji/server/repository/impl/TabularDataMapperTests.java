package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.MatrixRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.response.MultipleChoiceResponse;
import ke.co.hoji.core.response.NumberResponse;
import ke.co.hoji.core.response.SingleChoiceResponse;
import ke.co.hoji.server.calculation.Calculator;
import ke.co.hoji.server.model.DataTable;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter;
import ke.co.hoji.server.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 01/07/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class TabularDataMapperTests {

    private final static String NUMBER_RESPONSE = NumberResponse.class.getName();
    private final static String SINGLE_CHOICE_RESPONSE = SingleChoiceResponse.class.getName();
    private final static String MULTI_CHOICE_RESPONSE = MultipleChoiceResponse.class.getName();

    @Mock
    private HojiContext hojiContext;

    @Mock
    private UserService userService;

    @Mock
    private Calculator calculator;

    @Before
    public void setup() {
        User user = new User();
        user.setFullName("Test User");
        when(userService.findById(anyInt())).thenReturn(user);
    }

    /**
     * verifies that {@link TabularDataMapper#mapDataTable(Stream, long, MainRecordPropertiesFilter, String)} returns a
     * {@link DataElement}
     * with correct value for default row
     */
    @Test
    public void mapDataTable_shouldReturnTableWithCorrectDefaultRow() {
        Choice yes = new Choice(1, "Yes", "Y", 0, true);
        Choice no = new Choice(2, "No", "N", 0, true);
        ChoiceGroup yesNo = new ChoiceGroup(Arrays.asList(yes, no));
        Field choiceField = new Field(1);
        choiceField.setDescription("Q1");
        choiceField.setChoiceGroup(yesNo);
        choiceField.setEnabled(true);
        FieldType choiceType = new FieldType(1, "Choice", FieldType.Code.SINGLE_CHOICE);
        choiceType.setDataType(FieldType.DataType.STRING);
        choiceType.setResponseClass(SINGLE_CHOICE_RESPONSE);
        choiceField.setType(choiceType);
        Form form = getForm(choiceField);
        String liveFieldValue = "Y";
        List<MainRecord> mainRecords = getMainRecords(liveFieldValue);

        DataTable dataTable = new TabularDataMapper(userService, hojiContext, calculator)
                .mapDataTable(
                        mainRecords.stream(),
                        mainRecords.size(),
                        new MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder(form).build(),
                        Form.OutputStrategy.PIVOT
                );

        Assert.assertThat(dataTable.getDefaultColumn(), is(choiceField.getDescription()));

        choiceField = new Field(1);
        choiceField.setDescription("Q1");
        String columnName = "column_name";
        choiceField.setColumn(columnName);
        choiceField.setChoiceGroup(yesNo);
        choiceField.setEnabled(true);
        choiceType.setDataType(FieldType.DataType.STRING);
        choiceType.setResponseClass(SINGLE_CHOICE_RESPONSE);
        choiceField.setType(choiceType);
        form = getForm(choiceField);

        dataTable = new TabularDataMapper(userService, hojiContext, calculator).mapDataTable(
                mainRecords.stream(),
                mainRecords.size(),
                new MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder(form).build(),
                Form.OutputStrategy.PIVOT
        );

        Assert.assertThat(dataTable.getDefaultColumn(), is(columnName));
    }

    /**
     * verifies that {@link TabularDataMapper#mapDataTable(Stream, long, MainRecordPropertiesFilter, String)} returns a {@link DataElement}
     * with correct value for numeric {@link Field}
     */
    @Test
    public void mapDataTable_shouldReturnTableWithCellDataForNumericField() {
        Field numericField = new Field(1);
        numericField.setDescription("Q1");
        FieldType numeric = new FieldType(1, "Number", FieldType.Code.WHOLE_NUMBER);
        numeric.setDataType(FieldType.DataType.NUMBER);
        numeric.setResponseClass(NUMBER_RESPONSE);
        numericField.setType(numeric);
        numericField.setEnabled(true);
        Form form = getForm(numericField);
        LiveField liveField = new LiveField();
        liveField.setFieldId(1);
        int liveFieldValue = 20;
        List<MainRecord> mainRecords = getMainRecords(liveFieldValue);

        DataTable dataTable = new TabularDataMapper(userService, hojiContext, calculator).mapDataTable(
                mainRecords.stream(),
                mainRecords.size(),
                new MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder(form).build(),
                Form.OutputStrategy.PIVOT
        );

        Assert.assertThat(dataTable.getDataRows(), hasSize(1));
        List<DataElement> dataElements = dataTable.getDataRows().get(0).getDataElements();
        Assert.assertThat(dataElements.size(), is(21));
        Assert.assertThat(dataElements.get(2).getValue(), is(String.valueOf(liveFieldValue)));
    }

    /**
     * verifies that {@link TabularDataMapper#mapDataTable(Stream, long, MainRecordPropertiesFilter, String)} returns a {@link DataElement}
     * with correct value for calculated {@link Field}
     */
    @Test @Ignore
    public void mapDataTable_shouldReturnTableWithCellDataForCalculatedField() {
        FieldType numeric = new FieldType(1, "Number", FieldType.Code.WHOLE_NUMBER);
        numeric.setDataType(FieldType.DataType.NUMBER);
        numeric.setResponseClass(NUMBER_RESPONSE);
        Field fieldA = new Field(1);
        fieldA.setDescription("Field A");
        fieldA.setType(numeric);
        fieldA.setEnabled(true);
        Field fieldB = new Field(2);
        fieldB.setDescription("Field B");
        fieldB.setType(numeric);
        fieldB.setEnabled(true);
        Field fieldC = new Field(3);
        fieldC.setDescription("Field C");
        fieldC.setType(numeric);
        fieldC.setEnabled(true);
        fieldC.setValueScript("calculationUtils.getValue(1) + calculationUtils.getValue(2)");
        Form form = getForm(fieldA, fieldB, fieldC);
        Map<Integer, Object> liveFieldValues = new HashMap<>();
        liveFieldValues.put(1, 20);
        liveFieldValues.put(2, 30);
        List<MainRecord> mainRecords = getMainRecords(liveFieldValues);

        DataTable dataTable = new TabularDataMapper(userService, hojiContext, calculator).mapDataTable(
                mainRecords.stream(),
                mainRecords.size(),
                new MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder(form).build(),
                Form.OutputStrategy.PIVOT
        );

        Assert.assertThat(dataTable.getDataRows(), hasSize(1));
        List<DataElement> dataElements = dataTable.getDataRows().get(0).getDataElements();
        Assert.assertThat(dataElements.size(), is(23));
        Assert.assertThat(dataElements.get(2).getValue(), is(String.valueOf(21)));
        Assert.assertThat(dataElements.get(3).getValue(), is(String.valueOf(30)));
        Assert.assertThat(dataElements.get(4).getValue(), is(String.valueOf(50D)));
    }

    /**
     * verifies that {@link TabularDataMapper#mapDataTable(Stream, long, MainRecordPropertiesFilter, String)} returns a {@link DataElement}
     * of type 'string' for numeric {@link Field} whose output type is configured as 'string'
     */
    @Test
    public void mapDataTable_shouldReturnTableWithCellDataOfTypeStringForNumericFieldSpecifiedAsString() {
        Field numericField = new Field(1);
        numericField.setDescription("Q1");
        FieldType numeric = new FieldType(1, "Number", FieldType.Code.WHOLE_NUMBER);
        numeric.setDataType(FieldType.DataType.NUMBER);
        numeric.setResponseClass(NUMBER_RESPONSE);
        numericField.setType(numeric);
        numericField.setOutputType(Field.OutputType.STRING);
        numericField.setEnabled(true);
        Form form = getForm(numericField);
        form.setOutputStrategy(Collections.singletonList(Form.OutputStrategy.PIVOT));
        LiveField liveField = new LiveField();
        liveField.setFieldId(1);
        int liveFieldValue = 20;
        List<MainRecord> mainRecords = getMainRecords(liveFieldValue);

        DataTable dataTable = new TabularDataMapper(userService, hojiContext, calculator).mapDataTable(
                mainRecords.stream(),
                mainRecords.size(),
                new MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder(form).build(),
                Form.OutputStrategy.PIVOT
        );

        Assert.assertThat(dataTable.getDataRows(), hasSize(1));
        List<DataElement> dataElements = dataTable.getDataRows().get(0).getDataElements();
        Assert.assertThat(dataElements.size(), is(21));
        Assert.assertThat(dataElements.get(2).getValue(), is(String.valueOf(liveFieldValue)));
        Assert.assertThat(dataElements.get(2).getType(), is(DataElement.Type.STRING));
    }

    /**
     * verifies {@link TabularDataMapper#mapDataTable(Stream, long, MainRecordPropertiesFilter, String)} returns a {@link DataElement} with
     * correct value for single choice {@link Field}
     */
    @Test
    public void mapDataTable_shouldReturnTableWithCellDataForSingleChoiceField() {
        Choice yes = new Choice(1, "Yes", "Y", 0, true);
        Choice no = new Choice(2, "No", "N", 0, true);
        ChoiceGroup yesNo = new ChoiceGroup(Arrays.asList(yes, no));
        Field choiceField = new Field(1);
        choiceField.setDescription("Q1");
        choiceField.setChoiceGroup(yesNo);
        choiceField.setEnabled(true);
        FieldType choiceType = new FieldType(1, "Choice", FieldType.Code.SINGLE_CHOICE);
        choiceType.setDataType(FieldType.DataType.STRING);
        choiceType.setResponseClass(SINGLE_CHOICE_RESPONSE);
        choiceField.setType(choiceType);
        Form form = getForm(choiceField);
        Long liveFieldValue = 1L;
        List<MainRecord> mainRecords = getMainRecords(liveFieldValue);

        DataTable dataTable = new TabularDataMapper(userService, hojiContext, calculator).mapDataTable(
                mainRecords.stream(),
                mainRecords.size(),
                new MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder(form).build(),
                Form.OutputStrategy.PIVOT
        );

        Assert.assertThat(dataTable.getDataRows(), hasSize(1));
        List<DataElement> dataElements = dataTable.getDataRows().get(0).getDataElements();
        Assert.assertThat(dataElements.size(), is(21));
        Assert.assertThat(dataElements.get(2).getValue(), is(yes.getName()));
    }

    /**
     * verifies that {@link TabularDataMapper#mapDataTable(Stream, long, MainRecordPropertiesFilter, String)}
     * returns {@link DataElement} with a value equal to the score of selected choice
     */
    @Test
    public void mapDataTable_shouldReturnTableWithCellDataForScoredSingleChoiceField() {
        Choice yes = new Choice(1, "Yes", "Y", 2, true);
        Choice no = new Choice(2, "No", "N", 1, true);
        ChoiceGroup yesNo = new ChoiceGroup(Arrays.asList(yes, no));
        Field choiceField = new Field(1);
        choiceField.setDescription("Q1");
        choiceField.setChoiceGroup(yesNo);
        choiceField.setOutputType(Field.OutputType.NUMBER);
        choiceField.setEnabled(true);
        FieldType choiceType = new FieldType(1, "Choice", FieldType.Code.SINGLE_CHOICE);
        choiceType.setDataType(FieldType.DataType.STRING);
        choiceType.setResponseClass(SINGLE_CHOICE_RESPONSE);
        choiceField.setType(choiceType);
        Form form = getForm(choiceField);
        form.setOutputStrategy(Collections.singletonList(Form.OutputStrategy.PIVOT));
        Long liveFieldValue = 1L;
        List<MainRecord> mainRecords = getMainRecords(liveFieldValue);

        DataTable dataTable = new TabularDataMapper(userService, hojiContext, calculator).mapDataTable(
                mainRecords.stream(),
                mainRecords.size(),
                new MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder(form).build(),
                Form.OutputStrategy.PIVOT
        );

        Assert.assertThat(dataTable.getDataRows(), hasSize(1));
        List<DataElement> dataElements = dataTable.getDataRows().get(0).getDataElements();
        Assert.assertThat(dataElements.size(), is(21));
        Assert.assertThat(dataElements.get(2).getValue(), is(String.valueOf(yes.getScale())));
    }

    /**
     * verifies that {@link TabularDataMapper#mapDataTable(Stream, long, MainRecordPropertiesFilter, String)} returns a collection of
     * {@link DataElement} with values equal to selected choices
     */
    @Test
    public void mapDataTable_shouldReturnTableWithCellDataForMultiChoiceField() {
        Choice basketball = new Choice(1, "Basketball", "1", 0, true);
        Choice rugby = new Choice(2, "Rugby", "2", 0, true);
        Choice football = new Choice(3, "Football", "3", 0, true);
        ChoiceGroup sports = new ChoiceGroup(Arrays.asList(basketball, rugby, football));
        Field multiChoice = new Field(1);
        multiChoice.setDescription("Q1");
        multiChoice.setChoiceGroup(sports);
        multiChoice.setEnabled(true);
        FieldType choiceType = new FieldType(1, "Choice", FieldType.Code.MULTIPLE_CHOICE);
        choiceType.setDataType(FieldType.DataType.STRING);
        choiceType.setResponseClass(MULTI_CHOICE_RESPONSE);
        multiChoice.setType(choiceType);
        Form form = getForm(multiChoice);
        String liveFieldValue = "1#1|2#0|3#0";
        List<MainRecord> mainRecords = getMainRecords(liveFieldValue);

        DataTable dataTable = new TabularDataMapper(userService, hojiContext, calculator).mapDataTable(
                mainRecords.stream(),
                mainRecords.size(),
                new MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder(form).build(),
                Form.OutputStrategy.PIVOT
        );

        Assert.assertThat(dataTable.getDataRows(), hasSize(1));
        List<DataElement> row = dataTable.getDataRows().get(0).getDataElements();
        Assert.assertThat(row.size(), is(23));
        Assert.assertThat(row.get(2).getValue(), is("1"));
        Assert.assertThat(row.get(3).getValue(), is("0"));
        Assert.assertThat(row.get(4).getValue(), is("0"));
    }

    /**
     * verifies that {@link TabularDataMapper#mapDataTable(Stream, long, MainRecordPropertiesFilter, String)}
     * returns a single {@link DataElement} with value equal to sum of scores of selected choices
     */
    @Test
    public void mapDataTable_shouldReturnTableWithCellDataForScoredMultiChoiceField() {
        Choice basketball = new Choice(1, "Basketball", "1", 3, true);
        Choice rugby = new Choice(2, "Rugby", "2", 2, true);
        Choice football = new Choice(3, "Football", "3", 1, true);
        ChoiceGroup sports = new ChoiceGroup(Arrays.asList(basketball, rugby, football));
        Field multiChoice = new Field(1);
        multiChoice.setDescription("Q1");
        multiChoice.setOutputType(Field.OutputType.NUMBER);
        multiChoice.setChoiceGroup(sports);
        multiChoice.setEnabled(true);
        FieldType choiceType = new FieldType(1, "Choice", FieldType.Code.MULTIPLE_CHOICE);
        choiceType.setDataType(FieldType.DataType.STRING);
        choiceType.setResponseClass(MULTI_CHOICE_RESPONSE);
        multiChoice.setType(choiceType);
        Form form = getForm(multiChoice);
        form.setOutputStrategy(Collections.singletonList(Form.OutputStrategy.PIVOT));
        String liveFieldValue = "1#1|2#0|3#1";
        List<MainRecord> mainRecords = getMainRecords(liveFieldValue);

        DataTable dataTable = new TabularDataMapper(userService, hojiContext, calculator).mapDataTable(
                mainRecords.stream(),
                mainRecords.size(),
                new MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder(form).build(),
                Form.OutputStrategy.PIVOT
        );

        Assert.assertThat(dataTable.getDataRows(), hasSize(1));
        List<DataElement> dataElements = dataTable.getDataRows().get(0).getDataElements();
        Assert.assertThat(dataElements.size(), is(21));
        Assert.assertThat(dataElements.get(2).getValue(),
                is(String.valueOf(basketball.getScale() + football.getScale())));
    }

    /**
     * verifies that {@link TabularDataMapper#mapDataTable(Stream, long, MainRecordPropertiesFilter, String)} returns a {@link DataElement}
     * with the correct {@link DataElement#getType()} for unanswered {@link Field}
     */
    @Test
    public void mapDataTable_shouldReturnTypeInformationForBlankScoredFields() {
        Choice yes = new Choice(1, "Yes", "Y", 2, true);
        Choice no = new Choice(2, "No", "N", 1, true);
        ChoiceGroup yesNo = new ChoiceGroup(Arrays.asList(yes, no));
        Field choiceField = new Field(1);
        choiceField.setDescription("Q1");
        choiceField.setChoiceGroup(yesNo);
        choiceField.setOutputType(Field.OutputType.NUMBER);
        choiceField.setEnabled(true);
        FieldType choiceType = new FieldType(1, "Choice", FieldType.Code.SINGLE_CHOICE);
        choiceType.setDataType(FieldType.DataType.STRING);
        choiceType.setResponseClass(SINGLE_CHOICE_RESPONSE);
        choiceField.setType(choiceType);
        Form form = getForm(choiceField);
        Object blank = null;
        List<MainRecord> mainRecords = getMainRecords(blank);

        DataTable dataTable = new TabularDataMapper(userService, hojiContext, calculator).mapDataTable(
                mainRecords.stream(),
                mainRecords.size(),
                new MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder(form).build(),
                Form.OutputStrategy.PIVOT
        );

        Assert.assertThat(dataTable.getDataRows(), hasSize(1));
        List<DataElement> dataElements = dataTable.getDataRows().get(0).getDataElements();
        Assert.assertThat(dataElements.size(), is(21));
        Assert.assertThat(dataElements.get(2).getValue(), is("Missing"));
        Assert.assertThat(dataElements.get(2).getType(), is(DataElement.Type.NUMBER));
    }

    /**
     * verifies that {@link TabularDataMapper#mapDataTable(Stream, long, MainRecordPropertiesFilter, String)} returns a {@link DataElement} with
     * a label value obtained from {@link Field#getColumn()}
     */
    @Test
    public void mapDataTable_shouldReturnColumnAsDataElementLabelWhenColumnIsSpecifiedInField() {
        Field numericField = new Field(1);
        numericField.setDescription("Q1");
        numericField.setColumn("Column");
        FieldType numeric = new FieldType(1, "Number", FieldType.Code.WHOLE_NUMBER);
        numeric.setDataType(FieldType.DataType.NUMBER);
        numeric.setResponseClass(NUMBER_RESPONSE);
        numericField.setType(numeric);
        numericField.setEnabled(true);
        Form form = getForm(numericField);
        int liveFieldValue = 20;
        List<MainRecord> mainRecords = getMainRecords(liveFieldValue);

        DataTable dataTable = new TabularDataMapper(userService, hojiContext, calculator).mapDataTable(
                mainRecords.stream(),
                mainRecords.size(),
                new MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder(form).build(),
                Form.OutputStrategy.PIVOT
        );

        Assert.assertThat(dataTable.getDataRows(), hasSize(1));
        List<DataElement> dataElements = dataTable.getDataRows().get(0).getDataElements();
        Assert.assertThat(dataElements.size(), is(21));
        Assert.assertThat(dataElements.get(2).getLabel(), is(numericField.getColumn()));
    }

    /**
     * verifies that {@link TabularDataMapper#mapDataTable(Stream, long, MainRecordPropertiesFilter, String)} returns a {@link DataElement}
     * with value set to {@link Choice#getName()} by default
     */
    @Test
    public void mapDataTable_shouldReturnLabelDataValueTypesWhenSpecified() {
        Choice yes = new Choice(1, "Yes", "Y", 2, true);
        Choice no = new Choice(2, "No", "N", 1, true);
        ChoiceGroup yesNo = new ChoiceGroup(Arrays.asList(yes, no));
        Field field = new Field(1);
        field.setChoiceGroup(yesNo);
        field.setName("1");
        field.setDescription("Q1");
        FieldType singleChoice = new FieldType(1, "Choice", FieldType.Code.SINGLE_CHOICE);
        singleChoice.setDataType(FieldType.DataType.STRING);
        singleChoice.setResponseClass(SINGLE_CHOICE_RESPONSE);
        field.setType(singleChoice);
        field.setEnabled(true);
        Form form = getForm(field);
        Long liveFieldValue = yes.getId().longValue();
        List<MainRecord> mainRecords = getMainRecords(liveFieldValue);

        DataTable dataTable = new TabularDataMapper(userService, hojiContext, calculator).mapDataTable(
                mainRecords.stream(),
                mainRecords.size(),
                new MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder(form).build(),
                Form.OutputStrategy.PIVOT
        );

        Assert.assertThat(dataTable.getDataRows(), hasSize(1));
        List<DataElement> dataElements = dataTable.getDataRows().get(0).getDataElements();
        Assert.assertThat(dataElements.get(2).getValue(), is(yes.getName()));
    }

    /**
     * verifies that {@link TabularDataMapper#mapDataTable(Stream, long, MainRecordPropertiesFilter, String)} filters
     * out {@link ke.co.hoji.server.model.DataRow} that do not match filter field
     */
    @Test
    public void mapDataTable_shouldFilterOutRowsThatDoNotMatchFilterField() {
        Form form = new Form();
        form.setSurvey(new Survey(1, "Filter test", true, "SVY"));
        Choice yes = new Choice(1, "Yes", "Y", 2, true);
        Choice no = new Choice(2, "No", "N", 1, true);
        ChoiceGroup yesNo = new ChoiceGroup(Arrays.asList(yes, no));
        Field field = new Field(1);
        field.setName("1");
        field.setDescription("Single choice");
        field.setEnabled(true);
        FieldType singleChoice = new FieldType(1, "Choice", FieldType.Code.SINGLE_CHOICE);
        singleChoice.setDataType(FieldType.DataType.STRING);
        singleChoice.setResponseClass(SINGLE_CHOICE_RESPONSE);
        field.setType(singleChoice);
        field.setChoiceGroup(yesNo);
        field.setForm(form);
        form.setFields(Collections.singletonList(field));
        LiveField noLf = new LiveField();
        noLf.setFieldId(field.getId());
        noLf.setValue(no.getId());
        MainRecord noRecord = getMainRecords(Collections.emptyMap()).get(0);
        noRecord.setLiveFields(Collections.singletonList(noLf));
        LiveField yesLf = new LiveField();
        yesLf.setFieldId(field.getId());
        yesLf.setValue(yes.getId());
        MainRecord yesRecord = getMainRecords(Collections.emptyMap()).get(0);
        yesRecord.setLiveFields(Collections.singletonList(yesLf));

        DataTable dataTable = new TabularDataMapper(userService, hojiContext, calculator).mapDataTable(
                Stream.of(noRecord, yesRecord),
                2L,
                new MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder(form)
                        .filterFieldId(field.getId())
                        .filterFieldValue(yes.getId())
                        .build(),
                Form.OutputStrategy.PIVOT
        );

        Assert.assertThat(dataTable.getDataRows(), hasSize(1));

        Field parent = new Field(3);
        parent.setForm(form);
        Field child = new Field(4);
        child.setName("4");
        child.setDescription("Single choice");
        child.setEnabled(true);
        child.setType(singleChoice);
        child.setChoiceGroup(yesNo);
        child.setForm(form);
        parent.setChildren(Collections.singletonList(child));
        List<Field> fields = new ArrayList<>(form.getFields());
        fields.add(parent);
        form.setFields(fields);
        LiveField noMxLf = new LiveField();
        noMxLf.setFieldId(child.getId());
        noMxLf.setValue(no.getId());
        LiveField yesMxLf = new LiveField();
        yesMxLf.setFieldId(child.getId());
        yesMxLf.setValue(yes.getId());
        MatrixRecord noMatrixRecord = new MatrixRecord();
        noMatrixRecord.setLiveFields(Collections.singletonList(noMxLf));
        MatrixRecord yesMatrixRecord = new MatrixRecord();
        yesMatrixRecord.setLiveFields(Collections.singletonList(yesMxLf));
        MainRecord mainRecord = getMainRecords(Collections.emptyMap()).get(0);
        mainRecord.setMatrixRecords(Arrays.asList(noMatrixRecord, yesMatrixRecord));

        dataTable = new TabularDataMapper(userService, hojiContext, calculator).mapDataTable(
                Stream.of(mainRecord),
                2L,
                new MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder(parent)
                        .filterFieldId(child.getId())
                        .filterFieldValue(yes.getId())
                        .build(),
                Form.OutputStrategy.PIVOT
        );

        Assert.assertThat(dataTable.getDataRows(), hasSize(1));
    }

    private List<MainRecord> getMainRecords(Object liveFieldValue) {
        LiveField liveField = new LiveField();
        liveField.setUuid("L_UID_1");
        liveField.setFieldId(1);
        liveField.setValue(liveFieldValue);
        MainRecord mainRecord = new MainRecord();
        mainRecord.setUuid("UID_1");
        mainRecord.setLiveFields(Collections.singletonList(liveField));
        mainRecord.setUserId(1);
        mainRecord.setFormVersion("1");
        mainRecord.setAppVersion("1");
        mainRecord.setVersion(1);
        mainRecord.setStartLatitude(0D);
        mainRecord.setStartLongitude(0D);
        mainRecord.setEndLatitude(0D);
        mainRecord.setEndLongitude(0D);
        mainRecord.setDateCreated(1494860970714L);
        mainRecord.setDateCompleted(1494860980714L);
        mainRecord.setDateUpdated(1494860970714L);
        mainRecord.setDateUploaded(1494860975714L);
        return Collections.singletonList(mainRecord);
    }

    private List<MainRecord> getMainRecords(Map<Integer, Object> liveFieldValues) {
        List<LiveField> liveFields = liveFieldValues.keySet().stream().map(key -> {
            LiveField liveField = new LiveField();
            liveField.setUuid("L_UID_" + key);
            liveField.setFieldId(key);
            liveField.setValue(liveFieldValues.get(key));
            return liveField;
        }).collect(Collectors.toList());
        MainRecord mainRecord = new MainRecord();
        mainRecord.setUuid("UID_1");
        mainRecord.setLiveFields(liveFields);
        mainRecord.setUserId(1);
        mainRecord.setFormVersion("1");
        mainRecord.setAppVersion("1");
        mainRecord.setVersion(1);
        mainRecord.setStartLatitude(0D);
        mainRecord.setStartLongitude(0D);
        mainRecord.setEndLatitude(0D);
        mainRecord.setEndLongitude(0D);
        mainRecord.setDateCreated(1494860970714L);
        mainRecord.setDateCompleted(1494860980714L);
        mainRecord.setDateUpdated(1494860970714L);
        mainRecord.setDateUploaded(1494860975714L);
        return Collections.singletonList(mainRecord);
    }

    private Form getForm(Field... fields) {
        Survey survey = new Survey(1, "Test", true, "TEST");
        Form form = new Form(1);
        form.setFields(Arrays.asList(fields));
        form.setSurvey(survey);
        Stream.of(fields).forEach(field -> field.setForm(form));
        return form;
    }

}