package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.MatrixRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.response.MultipleChoiceResponse;
import ke.co.hoji.core.response.NumberResponse;
import ke.co.hoji.core.response.SingleChoiceResponse;
import ke.co.hoji.core.response.TextResponse;
import ke.co.hoji.server.calculation.CalculationResult;
import ke.co.hoji.server.calculation.Calculator;
import ke.co.hoji.server.model.Chart;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class AnalysisDataMapperTests {

    private HojiContext hojiContext;

    private Calculator calculator;

    private static final int TEXT_FIELD_ID = 1;
    private static final int NUMERIC_FIELD_ID = 2;
    private static final int SINGLE_CHOICE_FIELD_ID = 3;
    private static final int MULTIPLE_CHOICE_FIELD_ID = 4;
    private static final boolean ENABLED = true;

    private static final int YES_ID = 1;
    private static final String YES_NAME = "Yes";
    private static final String YES_CODE = "Y";
    private static final int YES_SCORE = 2;

    private static final int NO_ID = 2;
    private static final String NO_NAME = "No";
    private static final String NO_CODE = "N";
    private static final int NO_SCORE = 1;

    private static final int DONT_KNOW_ID = 3;
    private static final String DONT_KNOW_CODE = "DK";
    private static final String DONT_KNOW_NAME = "Don't Know";

    private static final int BASKETBALL_ID = 1;
    private static final String BASKETBALL_CODE = "1";
    private static final String BASKETBALL_NAME = "Basketball";
    private static final int BASKETBALL_SCORE = 3;

    private static final int RUGBY_ID = 2;
    private static final String RUGBY_CODE = "2";
    private static final String RUGBY_NAME = "Rugby";
    private static final int RUGBY_SCORE = 2;

    private static final int FOOTBALL_ID = 3;
    private static final String FOOTBALL_CODE = "3";
    private static final String FOOTBALL_NAME = "Football";
    private static final int FOOTBALL_SCORE = 1;


    private final FieldType choiceType = new FieldType(1, "Choice", FieldType.Code.SINGLE_CHOICE);
    private final FieldType multiChoiceType = new FieldType(3, "Multi-choice", FieldType.Code.MULTIPLE_CHOICE);
    private final FieldType numericType = new FieldType(2, "Numeric", FieldType.Code.WHOLE_NUMBER);
    private final FieldType textType = new FieldType(2, "TEXT", FieldType.Code.SINGLE_LINE_TEXT);

    private AnalysisDataMapper analysisDataMapper;

    @Before
    public void setup() {
        hojiContext = Mockito.mock(HojiContext.class);
        calculator = Mockito.mock(Calculator.class);
        
        analysisDataMapper = new AnalysisDataMapper(null, calculator);
        choiceType.setDataType(FieldType.DataType.NUMBER);
        choiceType.setResponseClass(SingleChoiceResponse.class.getName());
        multiChoiceType.setDataType(FieldType.DataType.STRING);
        multiChoiceType.setResponseClass(MultipleChoiceResponse.class.getName());
        numericType.setDataType(FieldType.DataType.NUMBER);
        numericType.setResponseClass(NumberResponse.class.getName());
        textType.setDataType(FieldType.DataType.STRING);
        textType.setResponseClass(TextResponse.class.getName());
    }

    @Test
    public void shouldReturnBinaryChart() throws Exception {
        ChoiceGroup yesNo = getYesNoGroup();
        Form form = setupFormWithChoiceField(yesNo, choiceType);
        List<MainRecord> mainRecords = getMainRecords(form.getFields().get(0).getId(), YES_ID, YES_ID, YES_ID, YES_ID);

        List<Chart> charts = analysisDataMapper.mapChart(
                mainRecords.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getType(), is(Chart.Type.BINARY));
        Assert.assertThat(charts.get(0).getSampleSize(), is(4));
        Assert.assertThat(charts.get(0).getData(), hasSize(2));
        Assert.assertThat(charts.get(0).getData().get(0).getLabel(), is(YES_NAME));
        Assert.assertThat(charts.get(0).getData().get(0).getValue(), is(4L));
        Assert.assertThat(charts.get(0).getData().get(1).getLabel(), is(NO_NAME));
        Assert.assertThat(charts.get(0).getData().get(1).getValue(), is(0L));

        Field parent = setupFieldParentWithChoiceField(yesNo, choiceType);
        mainRecords = getMainRecordsWithMatrices(form.getFields().get(0).getId(), YES_ID, YES_ID, YES_ID, YES_ID);

        charts = analysisDataMapper.mapChart(
                mainRecords.stream(),
                new MainRecordPropertiesFilterBuilder(parent).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getType(), is(Chart.Type.BINARY));
        Assert.assertThat(charts.get(0).getSampleSize(), is(4));
        Assert.assertThat(charts.get(0).getData(), hasSize(2));
        Assert.assertThat(charts.get(0).getData().get(0).getLabel(), is(YES_NAME));
        Assert.assertThat(charts.get(0).getData().get(0).getValue(), is(4L));
        Assert.assertThat(charts.get(0).getData().get(1).getLabel(), is(NO_NAME));
        Assert.assertThat(charts.get(0).getData().get(1).getValue(), is(0L));
    }

    @Test
    public void shouldReturnChartWithMissingDataPoint() throws Exception {
        ChoiceGroup yesNo = getYesNoGroup();
        Form form = setupFormWithChoiceField(yesNo, choiceType);
        List<MainRecord> fieldDataPoints = getMainRecords(form.getFields().get(0).getId(), null, null);

        List<Chart> charts = analysisDataMapper.mapChart(
                fieldDataPoints.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getData(), hasSize(3));
        Assert.assertThat(charts.get(0).getData().get(2).getLabel(), is("Missing"));
        Assert.assertThat(charts.get(0).getData().get(2).getValue(), is(2L));
    }

    @Test
    public void shouldReturnChartWithCalculatedDataPoint() {
        Form form = new Form(1);
        Field calculated = new Field(1);
        calculated.setEnabled(true);
        calculated.setType(numericType);
        calculated.setValueScript("function calculate() { return null; }");
        calculated.setForm(form);
        form.setFields(Collections.singletonList(calculated));
        MainRecord mainRecord = new MainRecord();
        mainRecord.setLiveFields(Collections.emptyList());
        List<MainRecord> fieldDataPoints = Collections.singletonList(mainRecord);
        CalculationResult mockCalculationResult = new CalculationResult(null, null, null, null, 10);
        mockCalculationResult.setEvaluated(true);
        when(calculator.calculate(any(MainRecord.class), eq(null), any(Field.class)))
                .thenReturn(mockCalculationResult);

        List<Chart> charts = analysisDataMapper.mapChart(
                fieldDataPoints.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getData(), hasSize(1));
        Assert.assertThat(charts.get(0).getData().get(0).getValue(), is(10));
        Assert.assertThat(charts.get(0).getSampleSize(), is(1));

        Field parent = new Field(2);
        parent.setForm(form);
        Field child = new Field(3);
        child.setEnabled(true);
        child.setType(numericType);
        child.setValueScript("function calculate() { return null; }");
        parent.setChildren(Collections.singletonList(child));
        List<Field> fields = new ArrayList<>(form.getFields());
        fields.add(parent);
        form.setFields(fields);
        MatrixRecord matrixRecord = new MatrixRecord();
        matrixRecord.setLiveFields(Collections.emptyList());
        mainRecord.setMatrixRecords(Collections.singletonList(matrixRecord));

        charts = analysisDataMapper.mapChart(
                fieldDataPoints.stream(),
                new MainRecordPropertiesFilterBuilder(parent).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getData(), hasSize(1));
        Assert.assertThat(charts.get(0).getData().get(0).getValue(), is(10));
        Assert.assertThat(charts.get(0).getSampleSize(), is(1));
    }

    @Test
    public void shouldReturnChartWithCorrectSampleSizeForSkippedFields() {
        ChoiceGroup yesNo = getYesNoGroup();
        Form form = setupFormWithChoiceField(yesNo, choiceType);
        MainRecord mainRecord = new MainRecord();
        mainRecord.setLiveFields(Collections.emptyList());
        List<MainRecord> fieldDataPoints = Collections.singletonList(mainRecord);

        List<Chart> charts = analysisDataMapper.mapChart(
                fieldDataPoints.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getData(), hasSize(2));
        Assert.assertThat(charts.get(0).getSampleSize(), is(0));

        Field calculated = form.getFields().get(0);
        calculated.setValueScript("function calculate() { return null; }");
        CalculationResult mockCalculationResult = new CalculationResult(null, null, null, null, null);
        mockCalculationResult.setEvaluated(false);
        when(calculator.calculate(any(MainRecord.class), eq(null), eq(calculated)))
                .thenReturn(mockCalculationResult);

        charts = analysisDataMapper.mapChart(
                fieldDataPoints.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getData(), hasSize(2));
        Assert.assertThat(charts.get(0).getSampleSize(), is(0));
    }

    @Test
    public void shouldFilterOutRecordsThatDoNotMatchFilterField() {
        Form form = new Form();
        ChoiceGroup yesNo = getYesNoGroup();
        Field filter = new Field(1);
        filter.setEnabled(true);
        filter.setType(choiceType);
        filter.setChoiceGroup(yesNo);
        filter.setForm(form);
        form.setFields(Collections.singletonList(filter));
        LiveField noLf = new LiveField();
        noLf.setFieldId(filter.getId());
        noLf.setValue((long) NO_ID);
        MainRecord mainRecord = new MainRecord();
        mainRecord.setLiveFields(Collections.singletonList(noLf));
        List<MainRecord> fieldDataPoints = Collections.singletonList(mainRecord);

        List<Chart> charts = analysisDataMapper.mapChart(
                fieldDataPoints.stream(),
                new MainRecordPropertiesFilterBuilder(form)
                        .filterFieldId(filter.getId())
                        .filterFieldValue(YES_ID)
                        .build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getData(), hasSize(2));
        Assert.assertThat(charts.get(0).getSampleSize(), is(0));

        Field parent = new Field(3);
        parent.setForm(form);
        Field child = new Field(4);
        child.setEnabled(true);
        child.setType(choiceType);
        child.setChoiceGroup(yesNo);
        parent.setChildren(Collections.singletonList(child));
        List<Field> fields = new ArrayList<>(form.getFields());
        fields.add(parent);
        form.setFields(fields);
        LiveField childLf = new LiveField();
        childLf.setFieldId(child.getId());
        childLf.setValue(YES_ID);
        MatrixRecord matrixRecord = new MatrixRecord();
        matrixRecord.setLiveFields(Collections.singletonList(childLf));
        mainRecord.setMatrixRecords(Collections.singletonList(matrixRecord));

        charts = analysisDataMapper.mapChart(
                fieldDataPoints.stream(),
                new MainRecordPropertiesFilterBuilder(parent)
                        .filterFieldId(filter.getId())
                        .filterFieldValue(YES_ID)
                        .build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getData(), hasSize(2));
        Assert.assertThat(charts.get(0).getSampleSize(), is(0));
    }

    @Test
    public void shouldReturnChartWithMissingDataPointForNumericFieldWithMissingValue() throws Exception {
        Form form = setupFormWithNumericField();
        List<MainRecord> fieldDataPoints = getMainRecords(form.getFields().get(0).getId(), 99, 99);

        List<Chart> charts = analysisDataMapper.mapChart(
                fieldDataPoints.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getData(), hasSize(2));
        Assert.assertThat(charts.get(0).getData().get(0).getLabel(), is("Missing"));
        Assert.assertNull(charts.get(0).getData().get(0).getValue());
        Assert.assertThat(charts.get(0).getData().get(1).getLabel(), is("Missing"));
        Assert.assertNull(charts.get(0).getData().get(1).getValue());
    }

    @Test
    public void shouldReturnCategoricalChart() throws Exception {
        ChoiceGroup yesNoDk = getYesNoDKGroup();
        Form form = setupFormWithChoiceField(yesNoDk, choiceType);
        List<MainRecord> mainRecords = getMainRecords(form.getFields().get(0).getId(), YES_ID, YES_ID, YES_ID, YES_ID);

        List<Chart> charts = analysisDataMapper.mapChart(
                mainRecords.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getType(), is(Chart.Type.CATEGORICAL));
        Assert.assertThat(charts.get(0).getSampleSize(), is(4));
        Assert.assertThat(charts.get(0).getData(), hasSize(3));
        Assert.assertThat(charts.get(0).getData().get(0).getLabel(), is(YES_NAME));
        Assert.assertThat(charts.get(0).getData().get(0).getValue(), is(4L));
        Assert.assertThat(charts.get(0).getData().get(1).getLabel(), is(NO_NAME));
        Assert.assertThat(charts.get(0).getData().get(1).getValue(), is(0L));
        Assert.assertThat(charts.get(0).getData().get(2).getLabel(), is(DONT_KNOW_NAME));
        Assert.assertThat(charts.get(0).getData().get(2).getValue(), is(0L));
    }

    @Test
    public void shouldReturnCategoricalChartForMultiChoiceField() throws Exception {
        ChoiceGroup sportGroup = getSportsGroup();
        Form form = setupFormWithChoiceField(sportGroup, multiChoiceType);
        String rugbySelected = BASKETBALL_ID + "#" + 0 + "|" + RUGBY_ID + "#" + 1 + "|" + FOOTBALL_ID + "#" + 0;
        String rugbyAndBasketBallSelected = BASKETBALL_ID + "#" + 1 + "|" + RUGBY_ID + "#" + 1 + "|" + FOOTBALL_ID + "#" + 0;
        String allSelected = BASKETBALL_ID + "#" + 1 + "|" + RUGBY_ID + "#" + 1 + "|" + FOOTBALL_ID + "#" + 1;
        String noneSelected = BASKETBALL_ID + "#" + 0 + "|" + RUGBY_ID + "#" + 0 + "|" + FOOTBALL_ID + "#" + 0;
        List<MainRecord> mainRecords = getMainRecords(form.getFields().get(0).getId(), rugbySelected);

        List<Chart> charts = analysisDataMapper.mapChart(
                mainRecords.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getType(), is(Chart.Type.CATEGORICAL));
        Assert.assertThat(charts.get(0).getSampleSize(), is(1));
        Assert.assertThat(charts.get(0).getData(), hasSize(3));
        Assert.assertThat(charts.get(0).getData().get(0).getLabel(), is(BASKETBALL_NAME));
        Assert.assertThat(charts.get(0).getData().get(0).getValue(), is(0L));
        Assert.assertThat(charts.get(0).getData().get(1).getLabel(), is(RUGBY_NAME));
        Assert.assertThat(charts.get(0).getData().get(1).getValue(), is(1L));
        Assert.assertThat(charts.get(0).getData().get(2).getLabel(), is(FOOTBALL_NAME));
        Assert.assertThat(charts.get(0).getData().get(2).getValue(), is(0L));

        mainRecords =
                getMainRecords(form.getFields().get(0).getId(), allSelected, rugbyAndBasketBallSelected, rugbySelected);

        charts = analysisDataMapper.mapChart(
                mainRecords.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getType(), is(Chart.Type.CATEGORICAL));
        Assert.assertThat(charts.get(0).getSampleSize(), is(3));
        Assert.assertThat(charts.get(0).getData(), hasSize(3));
        Assert.assertThat(charts.get(0).getData().get(0).getLabel(), is(BASKETBALL_NAME));
        Assert.assertThat(charts.get(0).getData().get(0).getValue(), is(2L));
        Assert.assertThat(charts.get(0).getData().get(1).getLabel(), is(RUGBY_NAME));
        Assert.assertThat(charts.get(0).getData().get(1).getValue(), is(3L));
        Assert.assertThat(charts.get(0).getData().get(2).getLabel(), is(FOOTBALL_NAME));
        Assert.assertThat(charts.get(0).getData().get(2).getValue(), is(1L));

        mainRecords = getMainRecords(form.getFields().get(0).getId(), noneSelected);

        charts = analysisDataMapper.mapChart(
                mainRecords.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getType(), is(Chart.Type.CATEGORICAL));
        Assert.assertThat(charts.get(0).getSampleSize(), is(1));
        Assert.assertThat(charts.get(0).getData(), hasSize(3));
        Assert.assertThat(charts.get(0).getData().get(0).getLabel(), is(BASKETBALL_NAME));
        Assert.assertThat(charts.get(0).getData().get(0).getValue(), is(0L));
        Assert.assertThat(charts.get(0).getData().get(1).getLabel(), is(RUGBY_NAME));
        Assert.assertThat(charts.get(0).getData().get(1).getValue(), is(0L));
        Assert.assertThat(charts.get(0).getData().get(2).getLabel(), is(FOOTBALL_NAME));
        Assert.assertThat(charts.get(0).getData().get(2).getValue(), is(0L));
    }

    @Test
    public void shouldReturnCategoricalChartWithMissingDataPoint() throws Exception {
        ChoiceGroup sportGroup = getSportsGroup();
        Form form = setupFormWithChoiceField(sportGroup, multiChoiceType);
        form.setOutputStrategy(Collections.singletonList(Form.OutputStrategy.ANALYSIS));
        List<MainRecord> mainRecords = getMainRecords(form.getFields().get(0).getId(), null, null);

        List<Chart> charts = analysisDataMapper.mapChart(
                mainRecords.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getSampleSize(), is(2));
        Assert.assertThat(charts.get(0).getData(), hasSize(4));
        Assert.assertThat(charts.get(0).getData().get(3).getLabel(), is("Missing"));
        Assert.assertThat(charts.get(0).getData().get(3).getValue(), is(2L));
    }

    @Test
    public void shouldReturnNumericChart() throws Exception {
        Form form = setupFormWithNumericField();
        List<MainRecord> mainRecords = getMainRecords(form.getFields().get(0).getId(), 1);

        List<Chart> charts = analysisDataMapper.mapChart(
                mainRecords.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getType(), is(Chart.Type.NUMERIC_INTEGER));
        Assert.assertThat(charts.get(0).getData(), hasSize(1));
        Assert.assertThat(charts.get(0).getData().get(0).getValue(), is(1L));
    }

    @Test
    public void shouldReturnNumericChartWithMissingDataPoint() throws Exception {
        Form form = setupFormWithNumericField();
        List<MainRecord> mainRecords = getMainRecords(form.getFields().get(0).getId(), (Object) null);

        List<Chart> charts = analysisDataMapper.mapChart(
                mainRecords.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getType(), is(Chart.Type.NUMERIC_INTEGER));
        Assert.assertThat(charts.get(0).getData(), hasSize(1));
        Assert.assertNull(charts.get(0).getData().get(0).getValue());
    }

    @Test
    public void shouldReturnCategoricalForMultipleChoiceWithTwoOptions() {
        Choice basketball = new Choice(1, BASKETBALL_NAME, BASKETBALL_CODE, BASKETBALL_SCORE, false);
        Choice rugby = new Choice(2, RUGBY_NAME, RUGBY_CODE, RUGBY_SCORE, false);
        ChoiceGroup sports = new ChoiceGroup(asList(basketball, rugby));
        Form form = setupFormWithChoiceField(sports, multiChoiceType);
        List<MainRecord> mainRecords =
                getMainRecords(form.getFields().get(0).getId(), BASKETBALL_ID + "#1", RUGBY_CODE + "#1", RUGBY_CODE + "#1", RUGBY_CODE + "#1");

        List<Chart> charts = analysisDataMapper.mapChart(
                mainRecords.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getType(), is(Chart.Type.CATEGORICAL));
        Assert.assertThat(charts.get(0).getSampleSize(), is(4));
        Assert.assertThat(charts.get(0).getData(), hasSize(2));
        Assert.assertThat(charts.get(0).getData().get(0).getLabel(), is(BASKETBALL_NAME));
        Assert.assertThat(charts.get(0).getData().get(0).getValue(), is(1L));
        Assert.assertThat(charts.get(0).getData().get(1).getLabel(), is(RUGBY_NAME));
        Assert.assertThat(charts.get(0).getData().get(1).getValue(), is(3L));
    }

    @Test
    public void shouldReturnCategoricalForMultipleChoiceWithOneOption() {
        Choice basketball = new Choice(1, BASKETBALL_NAME, BASKETBALL_CODE, BASKETBALL_SCORE, false);
        ChoiceGroup sports = new ChoiceGroup(Collections.singletonList(basketball));
        Form form = setupFormWithChoiceField(sports, multiChoiceType);
        List<MainRecord> mainRecords =
                getMainRecords(form.getFields().get(0).getId(), BASKETBALL_ID + "#1", BASKETBALL_CODE + "#1");

        List<Chart> charts = analysisDataMapper.mapChart(
                mainRecords.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(1));
        Assert.assertThat(charts.get(0).getType(), is(Chart.Type.CATEGORICAL));
        Assert.assertThat(charts.get(0).getSampleSize(), is(2));
        Assert.assertThat(charts.get(0).getData(), hasSize(1));
        Assert.assertThat(charts.get(0).getData().get(0).getLabel(), is(BASKETBALL_NAME));
        Assert.assertThat(charts.get(0).getData().get(0).getValue(), is(2L));
    }

    @Test
    public void shouldNotAnalyseNumericFieldOfTypeString() throws Exception {
        Form form = setupFormWithNumericFieldOfTypeString();
        List<MainRecord> fieldDataPoints = getMainRecords(form.getFields().get(0).getId(), 1);

        List<Chart> charts = analysisDataMapper.mapChart(
                fieldDataPoints.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(0));
    }

    @Test
    public void shouldNotAnalyseSingleLineField() throws Exception {
        Form form = setupFormWithTextField();
        List<MainRecord> fieldDataPoints = getMainRecords(form.getFields().get(0).getId(), 1);

        List<Chart> charts = analysisDataMapper.mapChart(
                fieldDataPoints.stream(),
                new MainRecordPropertiesFilterBuilder(form).build()
        );

        Assert.assertThat(charts, hasSize(0));
    }

    private List<MainRecord> getMainRecords(Integer fieldId, Object... values) {
        List<MainRecord> mainRecords = new ArrayList<>();
        for (Object value : values) {
            MainRecord mainRecord = new MainRecord();
            LiveField liveField = new LiveField();
            liveField.setFieldId(fieldId);
            liveField.setValue(value);
            mainRecord.setLiveFields(Collections.singletonList(liveField));
            mainRecords.add(mainRecord);
        }
        return mainRecords;
    }

    private List<MainRecord> getMainRecordsWithMatrices(Integer fieldId, Object... values) {
        List<MainRecord> mainRecords = new ArrayList<>();
        for (Object value : values) {
            MainRecord mainRecord = new MainRecord();
            mainRecord.setLiveFields(Collections.emptyList());
            MatrixRecord matrixRecord = new MatrixRecord();
            LiveField liveField = new LiveField();
            liveField.setFieldId(fieldId);
            liveField.setValue(value);
            matrixRecord.setLiveFields(Collections.singletonList(liveField));
            mainRecord.setMatrixRecords(Collections.singletonList(matrixRecord));
            mainRecords.add(mainRecord);
        }
        return mainRecords;
    }

    private ChoiceGroup getYesNoGroup() {
        Choice yes = new Choice(YES_ID);
        yes.setCode(YES_CODE);
        yes.setName(YES_NAME);
        yes.setScale(YES_SCORE);
        Choice no = new Choice(NO_ID);
        no.setCode(NO_CODE);
        no.setName(NO_NAME);
        no.setScale(NO_SCORE);
        return new ChoiceGroup(asList(yes, no));
    }

    private ChoiceGroup getYesNoDKGroup() {
        Choice dk = new Choice(DONT_KNOW_ID);
        dk.setCode(DONT_KNOW_CODE);
        dk.setName(DONT_KNOW_NAME);
        ChoiceGroup yesNoDk = getYesNoGroup();
        List<Choice> choices = new ArrayList<>(yesNoDk.getChoices());
        choices.add(dk);
        yesNoDk.setChoices(choices);
        return yesNoDk;
    }

    private ChoiceGroup getSportsGroup() {
        Choice basketball = new Choice(BASKETBALL_ID);
        basketball.setCode(BASKETBALL_CODE);
        basketball.setName(BASKETBALL_NAME);
        basketball.setScale(BASKETBALL_SCORE);
        Choice rugby = new Choice(RUGBY_ID);
        rugby.setCode(RUGBY_CODE);
        rugby.setName(RUGBY_NAME);
        rugby.setScale(RUGBY_SCORE);
        Choice football = new Choice(FOOTBALL_ID);
        football.setCode(FOOTBALL_CODE);
        football.setName(FOOTBALL_NAME);
        football.setScale(FOOTBALL_SCORE);
        return new ChoiceGroup(asList(basketball, rugby, football));
    }

    private Form setupFormWithChoiceField(ChoiceGroup choiceGroup, FieldType type) {
        Field field = setupChoiceField(choiceGroup, type);
        Form form = new Form(1);
        form.setSurvey(new Survey(1, "Test", true, ""));
        field.setForm(form);
        form.setFields(Collections.singletonList(field));
        return form;
    }

    private Field setupFieldParentWithChoiceField(ChoiceGroup choiceGroup, FieldType type) {
        Field field = setupChoiceField(choiceGroup, type);
        Form form = new Form(1);
        form.setSurvey(new Survey(1, "Test", true, ""));
        Field parent = new Field(1);
        field.setForm(form);
        parent.setForm(form);
        parent.setChildren(Collections.singletonList(field));
        form.setFields(Collections.singletonList(parent));
        return parent;
    }

    private Form setupFormWithScoredChoiceField(ChoiceGroup choiceGroup, FieldType type) {
        Field field = setupChoiceField(choiceGroup, type);
        field.setOutputType(Field.OutputType.NUMBER);
        Form form = new Form(1);
        form.setSurvey(new Survey(1, "Test", true, ""));
        field.setForm(form);
        form.setFields(Collections.singletonList(field));
        return form;
    }

    private Field setupChoiceField(ChoiceGroup choiceGroup, FieldType type) {
        Field field;
        if (type.getCode().equals(FieldType.Code.SINGLE_CHOICE)) {
            field = new Field(SINGLE_CHOICE_FIELD_ID);
        } else {
            field =  new Field(MULTIPLE_CHOICE_FIELD_ID);
        }
        field.setType(type);
        field.setChoiceGroup(choiceGroup);
        field.setEnabled(ENABLED);
        return field;
    }

    private Form setupFormWithNumericField() {
        Field field = setupNumericField();
        Form form = new Form(1);
        form.setSurvey(new Survey(1, "Test", true, ""));
        field.setForm(form);
        form.setFields(Collections.singletonList(field));
        return form;
    }

    private Form setupFormWithNumericFieldOfTypeString() {
        Field field = setupNumericField();
        field.setOutputType(Field.OutputType.STRING);
        Form form = new Form(1);
        form.setSurvey(new Survey(1, "Test", true, ""));
        field.setForm(form);
        form.setFields(Collections.singletonList(field));
        return form;
    }

    private Field setupNumericField() {
        Field field = new Field(NUMERIC_FIELD_ID);
        field.setType(numericType);
        field.setEnabled(ENABLED);
        field.setMissingValue("99");
        return field;
    }

    private Form setupFormWithTextField() {
        Field field = new Field(TEXT_FIELD_ID);
        field.setType(textType);
        field.setEnabled(ENABLED);
        field.setMissingValue("John Doe");
        Form form = new Form(1);
        form.setSurvey(new Survey(1, "Test", true, ""));
        field.setForm(form);
        form.setFields(Collections.singletonList(field));
        return form;
    }

}