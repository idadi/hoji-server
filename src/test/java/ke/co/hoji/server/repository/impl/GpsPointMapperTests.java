package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.model.GpsPoint;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter;
import ke.co.hoji.server.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static ke.co.hoji.server.repository.MainRecordPropertiesFilter.*;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GpsPointMapperTests {

    @Mock
    private UserService userService;
    private MainRecordPropertiesFilter filter = new MainRecordPropertiesFilterBuilder(new Form(1)).build();

    @Before
    public void setup() {
        User user = new User();
        user.setFullName("Test User");
        when(userService.findById(anyInt())).thenReturn(user);
    }

    @Test
    public void shouldReturnGpsPointsFromMainRecord() {
        MainRecord mainRecord = createMainRecord();

        List<GpsPoint> gpsPoints = new GpsPointMapper(userService).findGpsPoints(Stream.of(mainRecord), filter);

        Assert.assertThat(gpsPoints, hasSize(1));
    }

    @Test
    public void shouldReturnEmptyListWhenLatLongIsZero() {
        MainRecord mainRecord = createMainRecord();
        mainRecord.setStartLongitude(0D);
        mainRecord.setStartLatitude(0D);

        List<GpsPoint> gpsPoints = new GpsPointMapper(userService).findGpsPoints(Stream.of(mainRecord), filter);

        Assert.assertThat(gpsPoints, hasSize(0));
    }

    @Test
    public void shouldReturnGpsPointsWithUnknownAddressFromMainRecord() {
        MainRecord mainRecord = createMainRecord();
        mainRecord.setStartAddress(null);

        List<GpsPoint> gpsPoints = new GpsPointMapper(userService).findGpsPoints(Stream.of(mainRecord), filter);

        Assert.assertThat(gpsPoints, hasSize(1));
        Assert.assertThat(gpsPoints.get(0).getAddress(), is("Unknown Address"));
    }

    @Test
    public void shouldSkipGpsPointsThatDoNotPassFilterFieldCriteria() {
        MainRecord mainRecord = createMainRecord();
        LiveField liveField = new LiveField();
        liveField.setFieldId(1);
        liveField.setValue(2L);
        mainRecord.setLiveFields(Collections.singletonList(liveField));

        List<GpsPoint> gpsPoints = new GpsPointMapper(userService)
                .findGpsPoints(
                        Stream.of(mainRecord),
                        new MainRecordPropertiesFilterBuilder(new Form(1))
                                .filterFieldId(1)
                                .filterFieldValue(1)
                                .build()
                );

        Assert.assertThat(gpsPoints, hasSize(0));
    }

    private MainRecord createMainRecord() {
        MainRecord mainRecord = new MainRecord();
        mainRecord.setLiveFields(Collections.emptyList());
        mainRecord.setUuid("UID_1");
        mainRecord.setUserId(1);
        mainRecord.setFormVersion("1");
        mainRecord.setVersion(1);
        mainRecord.setStartAddress("Loresho Grove, Nairobi");
        mainRecord.setStartLatitude(-1.2492958);
        mainRecord.setStartLongitude(36.7558566);
        mainRecord.setStartAccuracy(1899.9990234375);
        mainRecord.setStartAge(0L);
        mainRecord.setEndAddress("Loresho Grove, Nairobi");
        mainRecord.setEndLatitude(-1.2492958);
        mainRecord.setEndLongitude(36.7558566);
        mainRecord.setEndAccuracy(1899.9990234375);
        mainRecord.setEndAge(0L);
        mainRecord.setDateCreated(1494860970714L);
        mainRecord.setDateCompleted(1494860980714L);
        mainRecord.setDateUpdated(1494860970714L);
        mainRecord.setDateUploaded(1494860975714L);
        return mainRecord;
    }

    private MainRecord getMetadataWithZeroLatLong() {
        MainRecord mainRecord = new MainRecord();
        mainRecord.setUuid("UID_1");
        mainRecord.setUserId(1);
        mainRecord.setFormVersion("1");
        mainRecord.setVersion(1);
        mainRecord.setStartAddress("Loresho Grove, Nairobi");
        mainRecord.setStartLatitude(0D);
        mainRecord.setStartLongitude(0D);
        mainRecord.setStartAccuracy(0D);
        mainRecord.setStartAge(0L);
        mainRecord.setEndAddress("Loresho Grove, Nairobi");
        mainRecord.setEndLatitude(0D);
        mainRecord.setEndLongitude(0D);
        mainRecord.setEndAccuracy(0D);
        mainRecord.setEndAge(0L);
        mainRecord.setDateCreated(1494860970714L);
        mainRecord.setDateCompleted(1494860980714L);
        mainRecord.setDateUpdated(1494860970714L);
        mainRecord.setDateUploaded(1494860975714L);
        return mainRecord;
    }

}