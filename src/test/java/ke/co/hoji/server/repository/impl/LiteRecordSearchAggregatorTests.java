package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.repository.SearchTermFilter;
import ke.co.hoji.server.repository.SearchTermFilter.ImportFilterBuilder;
import org.junit.Assert;
import org.junit.Test;

import static ke.co.hoji.server.repository.SearchTermFilter.BROAD_MATCH;
import static ke.co.hoji.server.repository.SearchTermFilter.EXACT_MATCH;
import static ke.co.hoji.server.repository.SearchTermFilter.PHRASE_MATCH;
import static org.hamcrest.Matchers.containsString;

/**
 * Created by geoffreywasilwa on 05/06/2017.
 */
public class LiteRecordSearchAggregatorTests {

    private static final int FORM_ID = 1;

    @Test
    public void testGetAggregationForBroadMatching() {
        String expectedPipelineString =
                "[{ \"$match\" : { \"formId\" : 1, \"searchTerms\" : { \"$regex\" : \"caption1|caption2\", \"$options\" : \"i\" } } }, { \"$project\" : { \"uuid\" : 1, \"caption\" : 1, \"dateCreated\" : 1, \"dateUpdated\" : 1, \"searchTerms\" : 1 } }]";
        SearchTermFilter searchTermFilter =
                new ImportFilterBuilder(new Form(FORM_ID))
                        .matchSensitivity(BROAD_MATCH)
                        .searchTerms(new String[]{"caption1", "caption2"})
                        .build();

        String actualPipelineString = LiteRecordSearchAggregator.getAggregation(searchTermFilter).toString();

        Assert.assertThat(actualPipelineString, containsString(expectedPipelineString));
    }

    @Test
    public void testGetAggregationForPhraseMatching() {
        String expectedPipelineString =
                "[{ \"$match\" : { \"formId\" : 1, \"searchTerms\" : { \"$regex\" : \"\\\\|caption1\\\\||\\\\|caption2\\\\|\", \"$options\" : \"i\" } } }, { \"$project\" : { \"uuid\" : 1, \"caption\" : 1, \"dateCreated\" : 1, \"dateUpdated\" : 1, \"searchTerms\" : 1 } }]";
        SearchTermFilter searchTermFilter =
                new ImportFilterBuilder(new Form(FORM_ID))
                        .matchSensitivity(PHRASE_MATCH)
                        .searchTerms(new String[]{"caption1", "caption2"})
                        .build();

        String actualPipelineString = LiteRecordSearchAggregator.getAggregation(searchTermFilter).toString();

        Assert.assertThat(actualPipelineString, containsString(expectedPipelineString));
    }

    @Test
    public void testGetAggregationForExactMatching() {
        String expectedPipelineString =
                "[{ \"$match\" : { \"formId\" : 1, \"searchTerms\" : { \"$regex\" : \"\\\\|caption1\\\\|(.*?\\\\|)?caption2\\\\|\", \"$options\" : \"i\" } } }, { \"$project\" : { \"uuid\" : 1, \"caption\" : 1, \"dateCreated\" : 1, \"dateUpdated\" : 1, \"searchTerms\" : 1 } }]";
        SearchTermFilter searchTermFilter =
                new ImportFilterBuilder(new Form(FORM_ID))
                        .matchSensitivity(EXACT_MATCH)
                        .searchTerms(new String[]{"caption1", "caption2"})
                        .build();

        String actualPipelineString = LiteRecordSearchAggregator.getAggregation(searchTermFilter).toString();

        Assert.assertThat(actualPipelineString, containsString(expectedPipelineString));
    }

}