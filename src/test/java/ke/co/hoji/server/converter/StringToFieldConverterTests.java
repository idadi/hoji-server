package ke.co.hoji.server.converter;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.service.model.FieldService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 09/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class StringToFieldConverterTests {

    @Mock
    private FieldService fieldService;

    @Test
    public void testConvertStringToField() throws Exception {
        int id = 1;
        when(fieldService.getFieldById(id)).thenReturn(new Field(id));

        Field field = new StringToFieldConverter(fieldService).convert(String.valueOf(id));

        Assert.assertNotNull(field);
        Assert.assertThat(field.getId(), is(id));
    }

}