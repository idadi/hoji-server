package ke.co.hoji.server.converter;

import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.service.model.FieldService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 09/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class StringToFieldTypeConverterTests {

    @Mock
    private FieldService fieldService;

    @Test
    public void testConvertStringToFieldType() throws Exception {
        int id = 1;
        String name = "Single Line";
        String code = "SL";
        when(fieldService.getFieldTypeById(id)).thenReturn(new FieldType(id, name, code));

        FieldType fieldType = new StringToFieldTypeConverter(fieldService).convert(String.valueOf(id));

        Assert.assertNotNull(fieldType);
        Assert.assertThat(fieldType.getId(), is(id));
        Assert.assertThat(fieldType.getName(), is(name));
        Assert.assertThat(fieldType.getCode(), is(code));
    }

}