package ke.co.hoji.server.converter;

import ke.co.hoji.core.data.model.OperatorDescriptor;
import ke.co.hoji.core.service.model.FieldService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 09/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class StringToOperatorDescriptorConverterTests {

    @Mock
    private FieldService fieldService;

    @Test
    public void testConvertStringToOperatorDescriptor() throws Exception {
        int id = 1;
        when(fieldService.getOperatorDescriptorById(id)).thenReturn(new OperatorDescriptor(id));

        OperatorDescriptor od = new StringToOperatorDescriptorConverter(fieldService).convert(String.valueOf(id));

        Assert.assertNotNull(od);
        Assert.assertThat(od.getId(), is(id));
    }

}