package ke.co.hoji.server.converter;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by geoffreywasilwa on 09/06/2017.
 */
public class BooleanToStringConverterTests {

    @Test
    public void testConvertTrueToString() throws Exception {
        String expected = "Yes";

        String actual = new BooleanToStringConverter().convert(true);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testConvertFalseToString() throws Exception {
        String expected = "No";

        String actual = new BooleanToStringConverter().convert(false);

        Assert.assertEquals(expected, actual);
    }

}