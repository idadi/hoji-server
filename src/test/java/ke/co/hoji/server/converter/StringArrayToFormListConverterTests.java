package ke.co.hoji.server.converter;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.FormService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 09/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class StringArrayToFormListConverterTests {

    @Mock
    private FormService formService;

    @Test
    public void testConvertStringArrayToForms() throws Exception {
        int id = 1;
        when(formService.getFormById(anyInt())).thenReturn(new Form(id));

        List<Form> forms = new StringArrayToFormListConverter(formService).convert(new String[] { "1" });

        Assert.assertThat(forms, hasSize(1));
        Assert.assertThat(forms.get(0).getId(), is(id));
    }

}