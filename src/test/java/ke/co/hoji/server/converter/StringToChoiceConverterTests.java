package ke.co.hoji.server.converter;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.server.dao.model.JdbcChoiceDao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 09/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class StringToChoiceConverterTests {

    @Mock
    private JdbcChoiceDao choiceDao;

    @Test
    public void testConvertStringToChoice() throws Exception {
        int id = 1;
        String name = "Yes";
        String code = "Y";
        int scale = 0;
        boolean loner = false;
        when(choiceDao.getById(id)).thenReturn(new Choice(id, name, code, scale, loner));

        Choice choice = new StringToChoiceConverter(choiceDao).convert(String.valueOf(id));

        Assert.assertNotNull(choice);
        Assert.assertThat(choice.getId(), is(id));
        Assert.assertThat(choice.getName(), is(name));
    }

}