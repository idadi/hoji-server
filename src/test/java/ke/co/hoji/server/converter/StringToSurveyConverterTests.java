package ke.co.hoji.server.converter;

import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.SurveyService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StringToSurveyConverterTests {

    @Mock
    private SurveyService surveyService;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void convert_shouldFailIfSourceIsNull() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("source should not be null");

        new StringToSurveyConverter(surveyService).convert(null);
    }

    @Test
    public void convert_shouldGetPersistedSurvey() {
        Survey survey = new Survey(1, "Sample", true, "SMPL");
        when(surveyService.getSurveyById(1)).thenReturn(survey);

        Survey converted = new StringToSurveyConverter(surveyService).convert("1");

        assertThat(converted, is(survey));
        verify(surveyService, times(1)).getSurveyById(1);
        verifyZeroInteractions(surveyService);
    }

    @Test
    public void convert_shouldFailIfSourceIsNotInteger() {
        exception.expect(NumberFormatException.class);

        new StringToSurveyConverter(surveyService).convert("survey");
    }

}