package ke.co.hoji.server.converter;

import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.service.model.ChoiceGroupService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 09/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class StringToChoiceGroupConverterTests {

    @Mock
    private ChoiceGroupService choiceGroupService;

    @Test
    public void testConvertStringToChoiceGroup() throws Exception {
        int id = 1;
        String name = "Yes/No";
        when(choiceGroupService.getChoiceGroupById(id)).thenReturn(new ChoiceGroup(id, name));

        ChoiceGroup choiceGroup = new StringToChoiceGroupConverter(choiceGroupService).convert(String.valueOf(id));

        Assert.assertNotNull(choiceGroup);
        Assert.assertThat(choiceGroup.getId(), is(id));
        Assert.assertThat(choiceGroup.getName(), is(name));
    }

}