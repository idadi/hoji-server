package ke.co.hoji.server.converter;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by geoffreywasilwa on 09/06/2017.
 */
public class IntegerToStringConverterTests {

    @Test
    public void testConvertIntegerToString() throws Exception {
        String expected = "1";

        String actual = new IntegerToStringConverter().convert(1);

        Assert.assertEquals(expected, actual);
    }

}