package ke.co.hoji.server.json.deserializer;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.server.dao.model.JdbcChoiceDao;

/**
 * Created by geoffreywasilwa on 11/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class ChoiceDeserializerTests {

        private static final int CHOICE_ID = 1;
        private static final String CODE = "Y";
        private static final String NAME = "Yes";
        private static final String DESCRIPTION = "Yes description";
        private static final int PARENT_ID = 2;
        private static final String PARENT_CODE = "P";
        private static final String PARENT_NAME = "Parent";
        private static final int SCALE = 1;
        private ObjectMapper mapper;

        @Mock
        private JdbcChoiceDao choiceDao;

        @Before
        public void setup() {
                mapper = new ObjectMapper();
                SimpleModule module = new SimpleModule();
                module.addDeserializer(Choice.class, new ChoiceDeserializer(choiceDao));
                mapper.registerModule(module);
        }

        @Test
        public void shouldDeserializeChoiceUsingName() throws Exception {
                Choice choice = mapper.readValue("\"Yes\"", Choice.class);

                Assert.assertThat(choice.getName(), is("Yes"));
        }

        @Test
        public void shouldTrimChoiceName() throws Exception {
                Choice choice = mapper.readValue("\" Yes \"", Choice.class);

                Assert.assertThat(choice.getName(), is("Yes"));
        }

        @Test
        public void shouldReturnNullIfChoiceNameIsBlank() throws Exception {
        Choice choice1 = mapper.readValue("\"\"", Choice.class);
        Choice choice2 = mapper.readValue("\" \"", Choice.class);

        Assert.assertNull(choice1);
        Assert.assertNull(choice2);
    }

    @Test
    public void shouldDeserializeChoiceFromJson() throws Exception {
        Choice choice = new Choice(CHOICE_ID);
        choice.setCode(CODE);
        choice.setName(NAME);
        when(choiceDao.getById(CHOICE_ID)).thenReturn(choice);
        String jsonChoice =
                "{" +
                        "\"id\": "+ CHOICE_ID +"," +
                        " \"code\": \"" + CODE + "\"," +
                        " \"name\": \"" + NAME + " \"," +
                        " \"description\": null," +
                        " \"scale\": \"" + SCALE + "\"," +
                        " \"loner\": \"true\"," +
                        " \"parent\": null," +
                        " \"translatableComponents\": [\"NAME\"]" +
                "}";
        Choice deserialized = mapper.readValue(jsonChoice, Choice.class);

        Assert.assertThat(deserialized.getId(), is(CHOICE_ID));
        Assert.assertThat(deserialized.getCode(), is(CODE));
        Assert.assertThat(deserialized.getName(), is(NAME));
        Assert.assertNull(deserialized.getDescription());
        Assert.assertThat(deserialized.getScale(), is(SCALE));
        Assert.assertTrue(deserialized.isLoner());
        Assert.assertNull(deserialized.getParent());

        jsonChoice =
                "{" +
                        "\"id\": \"\"," +
                        " \"code\": null," +
                        " \"name\": \"" + NAME + "\"," +
                        " \"description\": \"" + DESCRIPTION + "\"," +
                        " \"scale\": \"" + SCALE + "\"," +
                        " \"loner\": \"true\"" +
                "}";
        deserialized = mapper.readValue(jsonChoice, Choice.class);

        Assert.assertNull(deserialized.getId());
        Assert.assertNull(deserialized.getCode());
        Assert.assertThat(deserialized.getName(), is(NAME));
        Assert.assertThat(deserialized.getDescription(), is(DESCRIPTION));
        Assert.assertThat(deserialized.getScale(), is(SCALE));
        Assert.assertTrue(deserialized.isLoner());
    }

    @Test
    public void shouldDeserializeChoiceFromJsonWithParent() throws Exception {
        Choice choice = new Choice(CHOICE_ID);
        choice.setCode(CODE);
        choice.setName(NAME);
        when(choiceDao.getById(CHOICE_ID)).thenReturn(choice);
        Choice parent = new Choice(PARENT_ID);
        parent.setCode(PARENT_CODE);
        parent.setName(PARENT_NAME);
        when(choiceDao.getById(PARENT_ID)).thenReturn(parent);
        String jsonChoice =
                "{" +
                        "\"id\": \""+ CHOICE_ID +"\", " +
                        "\"code\": \"" + CODE + "\", " +
                        "\"name\": \"" + NAME + "\", " +
                        "\"parent\": {" +
                            "\"id\": " + PARENT_ID + "," +
                            "\"name\": \"" + PARENT_NAME + "\"," +
                            "\"code\": \"" + PARENT_CODE + "\"" +
                        "}" +
                "}";
        Choice desChoice = mapper.readValue(jsonChoice, Choice.class);
        Assert.assertThat(desChoice.getParent().getId(), is(PARENT_ID));
        Assert.assertThat(desChoice.getParent().getCode(), is(PARENT_CODE));
        Assert.assertThat(desChoice.getParent().getName(), is(PARENT_NAME));
    }

    @Test
    public void shouldDeserializeChoiceWithCleanedNameAndDescription() throws Exception {
        String jsonChoice =
                "{" +
                        "\"code\": \"Y \", " +
                        "\"name\": \" Yes  \", " +
                        "\"description\": \"Yes  option \" " +
                "}";
        Choice desChoice = mapper.readValue(jsonChoice, Choice.class);
        Assert.assertThat(desChoice.getCode(), is("Y"));
        Assert.assertThat(desChoice.getName(), is("Yes"));
        Assert.assertThat(desChoice.getDescription(), is("Yes option"));
    }

    @Test
    public void shouldSetupParentChoiceWhenAvailableInStoredChoice() throws IOException {
        Choice parent = new Choice(PARENT_ID);
        parent.setCode(PARENT_CODE);
        parent.setName(PARENT_NAME);
        Choice choice = new Choice(CHOICE_ID);
        choice.setCode(CODE);
        choice.setName(NAME);
        choice.setParent(parent);
        when(choiceDao.getById(CHOICE_ID)).thenReturn(choice);
        String jsonChoice =
                "{" +
                        "\"id\": \""+ CHOICE_ID +"\", " +
                        "\"code\": \"" + CODE + "\", " +
                        "\"name\": \"" + NAME + "\" " +
                "}";
        Choice desChoice = mapper.readValue(jsonChoice, Choice.class);
        Assert.assertThat(desChoice.getParent().getId(), is(PARENT_ID));
        Assert.assertThat(desChoice.getParent().getCode(), is(PARENT_CODE));
        Assert.assertThat(desChoice.getParent().getName(), is(PARENT_NAME));
    }

}