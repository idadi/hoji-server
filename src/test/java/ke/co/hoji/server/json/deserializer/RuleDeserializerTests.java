package ke.co.hoji.server.json.deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.OperatorDescriptor;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.service.model.FieldService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 11/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class RuleDeserializerTests {

    private static final int RULE_ID = 1;
    private static final int OWNER_ID = 1;
    private static final int TARGET_ID = 2;
    private static final int DESCRIPTOR_ID = 1;
    private static final int CONTAINS_DESCRIPTOR_ID = 7;
    private ObjectMapper mapper;

    @Mock
    private FieldService fieldService;
    private ChoiceGroup sports;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Rule.class, new RuleDeserializer(fieldService));
        mapper.registerModule(module);

        Choice football = new Choice(1, "Football", "1", 0, false);
        Choice basketball = new Choice(2, "Basketball", "2", 0, false);
        Choice rugby = new Choice(3, "Rugby", "3", 0, false);
        sports = new ChoiceGroup(Arrays.asList(football, basketball, rugby));
    }

    @Test
    public void shouldDeserializeRuleFromJson() throws Exception {
        FieldType text = new FieldType(1, "Text", FieldType.Code.SINGLE_LINE_TEXT);
        Field owner = new Field(OWNER_ID);
        owner.setType(text);
        Field target = new Field(TARGET_ID);
        OperatorDescriptor descriptor = new OperatorDescriptor(DESCRIPTOR_ID);
        Rule rule = new Rule(RULE_ID);
        rule.setOwner(owner);
        rule.setTarget(target);
        rule.setOperatorDescriptor(descriptor);
        when(fieldService.getFieldById(OWNER_ID)).thenReturn(owner);
        when(fieldService.getFieldById(TARGET_ID)).thenReturn(target);
        when(fieldService.getOperatorDescriptorById(DESCRIPTOR_ID)).thenReturn(descriptor);

        String jsonRule =
                "{" +
                        "\"type\": " + Rule.Type.FORWARD_SKIP + ", " +
                        "\"id\": "+ RULE_ID +", " +
                        "\"owner\": "+ OWNER_ID +", " +
                        "\"target\": "+ TARGET_ID +", " +
                        "\"operatorDescriptor\": "+ DESCRIPTOR_ID +
                "}";
        Rule desRule = mapper.readValue(jsonRule, Rule.class);

        Assert.assertThat(desRule.getId(), is(RULE_ID));
        Assert.assertThat(desRule.getOwner().getId(), is(OWNER_ID));
        Assert.assertThat(desRule.getTarget().getId(), is(TARGET_ID));
        Assert.assertThat(desRule.getOperatorDescriptor().getId(), is(DESCRIPTOR_ID));

        jsonRule =
                "{" +
                        "\"type\": " + Rule.Type.FORWARD_SKIP + ", " +
                        "\"id\": \"\", " +
                        "\"owner\":\""+ OWNER_ID +"\", " +
                        "\"target\":\""+ TARGET_ID +"\", " +
                        "\"operatorDescriptor\":\"" + DESCRIPTOR_ID + "\"" +
                "}";
        desRule = mapper.readValue(jsonRule, Rule.class);

        Assert.assertNull(desRule.getId());
        Assert.assertThat(desRule.getOwner().getId(), is(OWNER_ID));
        Assert.assertThat(desRule.getTarget().getId(), is(TARGET_ID));
        Assert.assertThat(desRule.getOperatorDescriptor().getId(), is(DESCRIPTOR_ID));
    }

    /**
     * Ensure that trying to deserialize a contains rule works as expected i.e. the
     * value should be a string of all codes joined by pipe (|) character.
     * @throws IOException @see ObjectMapper#readValue(String, Class)
     */
    @Test
    public void deserialize_shouldCorrectlyDeserializeContainsRuleValue() throws IOException {
        FieldType text = new FieldType(1, "Text", FieldType.Code.SINGLE_LINE_TEXT);
        Field owner = new Field(OWNER_ID);
        owner.setType(text);
        when(fieldService.getFieldById(OWNER_ID)).thenReturn(owner);
        when(fieldService.getOperatorDescriptorById(CONTAINS_DESCRIPTOR_ID))
                .thenReturn(new OperatorDescriptor(CONTAINS_DESCRIPTOR_ID));
        String jsonRule =
                "{" +
                        "\"type\": " + Rule.Type.FORWARD_SKIP + ", " +
                        "\"id\": "+ RULE_ID +", " +
                        "\"owner\": "+ OWNER_ID +", " +
                        "\"target\": "+ TARGET_ID +", " +
                        "\"operatorDescriptor\": "+ CONTAINS_DESCRIPTOR_ID + ", " +
                        "\"value\": \"3,17\"" +
                "}";
        Rule desRule = mapper.readValue(jsonRule, Rule.class);

        Assert.assertThat(desRule.getValue(), is("3|17"));
    }

    /**
     * Ensure that trying to deserialize a logically contains rule works as expected i.e. the
     * value should be a binary string.
     * @throws IOException @see ObjectMapper#readValue(String, Class)
     */
    @Test
    public void deserialize_shouldCorrectlyDeserializeLogicallyContainsRuleValue() throws IOException {
        FieldType multiChoice = new FieldType(1, "Multi choice", FieldType.Code.MULTIPLE_CHOICE);
        Field owner = new Field(OWNER_ID);
        owner.setType(multiChoice);
        owner.setChoiceGroup(sports);
        when(fieldService.getFieldById(OWNER_ID)).thenReturn(owner);
        OperatorDescriptor contains = new OperatorDescriptor(7);
        when(fieldService.getOperatorDescriptorById(CONTAINS_DESCRIPTOR_ID)).thenReturn(contains);
        String jsonRule =
                "{" +
                        "\"type\": " + Rule.Type.FORWARD_SKIP + ", " +
                        "\"owner\": "+ OWNER_ID +", " +
                        "\"target\": "+ TARGET_ID +", " +
                        "\"operatorDescriptor\": "+ CONTAINS_DESCRIPTOR_ID + ", " +
                        "\"value\": \"1,3\"" +
                "}";
        Rule desRule = mapper.readValue(jsonRule, Rule.class);

        Assert.assertThat(desRule.getValue(), is("1#1|2#0|3#1"));
    }

    /**
     * Ensure that forward skip rule value for a ranking field's contains rule is in the forms "Id#1|Id#0...
     * @throws IOException, error while converting json string to object
     */

    @Test
    public void deserialize_shouldCorrectlySetForwardSkipValueOfContainsRuleForRankingField() throws IOException {
        FieldType ranking = new FieldType(1, "Ranking", FieldType.Code.RANKING);
        Field owner = new Field(OWNER_ID);
        owner.setType(ranking);
        owner.setChoiceGroup(sports);
        when(fieldService.getFieldById(OWNER_ID)).thenReturn(owner);
        OperatorDescriptor contains = new OperatorDescriptor(7);
        when(fieldService.getOperatorDescriptorById(CONTAINS_DESCRIPTOR_ID)).thenReturn(contains);
        String jsonRule =
                "{" +
                        "\"type\": " + Rule.Type.FORWARD_SKIP + ", " +
                        "\"owner\": "+ OWNER_ID +", " +
                        "\"target\": "+ TARGET_ID +", " +
                        "\"operatorDescriptor\": "+ CONTAINS_DESCRIPTOR_ID + ", " +
                        "\"value\": \"1\"" +
                        "}";
        Rule desRule = mapper.readValue(jsonRule, Rule.class);

        Assert.assertThat(desRule.getValue(), is("1#1|2#0|3#0"));
    }

    /**
     * Ensure that forward skip rule value is properly set when value in json is an empty array
     * @throws IOException, error while converting json string to object
     */

    @Test
    public void deserialize_shouldCorrectlySetForwardSkipValueOfContainsRuleForMultipleChoiceFieldThatHasEmptyValue() throws IOException {
        FieldType ranking = new FieldType(1, "Ranking", FieldType.Code.RANKING);
        Field owner = new Field(OWNER_ID);
        owner.setType(ranking);
        owner.setChoiceGroup(sports);
        when(fieldService.getFieldById(OWNER_ID)).thenReturn(owner);
        OperatorDescriptor contains = new OperatorDescriptor(7);
        when(fieldService.getOperatorDescriptorById(CONTAINS_DESCRIPTOR_ID)).thenReturn(contains);
        String jsonRule =
                "{" +
                        "\"type\": " + Rule.Type.FORWARD_SKIP + ", " +
                        "\"owner\": "+ OWNER_ID +", " +
                        "\"target\": "+ TARGET_ID +", " +
                        "\"operatorDescriptor\": "+ CONTAINS_DESCRIPTOR_ID + ", " +
                        "\"value\": \"\"" +
                        "}";
        Rule desRule = mapper.readValue(jsonRule, Rule.class);

        Assert.assertThat(desRule.getValue(), is("1#0|2#0|3#0"));
    }

    /**
     * Ensure that backward skip rule value for a ranking field's contains rule is in the forms "Id#1|Id#0...
     * @throws IOException, error while converting json string to object
     */

    @Test
    public void deserialize_shouldCorrectlySetBackwardSkipValueOfContainsRuleForRankingField() throws IOException {
        FieldType ranking = new FieldType(1, "Ranking", FieldType.Code.RANKING);
        Field target = new Field(TARGET_ID);
        target.setType(ranking);
        target.setChoiceGroup(sports);
        when(fieldService.getFieldById(TARGET_ID)).thenReturn(target);
        OperatorDescriptor contains = new OperatorDescriptor(7);
        when(fieldService.getOperatorDescriptorById(CONTAINS_DESCRIPTOR_ID)).thenReturn(contains);
        String jsonRule =
                "{" +
                        "\"type\": " + Rule.Type.BACKWARD_SKIP + ", " +
                        "\"owner\": "+ OWNER_ID +", " +
                        "\"target\": "+ TARGET_ID +", " +
                        "\"operatorDescriptor\": "+ CONTAINS_DESCRIPTOR_ID + ", " +
                        "\"value\": \"1\"" +
                        "}";
        Rule desRule = mapper.readValue(jsonRule, Rule.class);

        Assert.assertThat(desRule.getValue(), is("1#1|2#0|3#0"));
    }

    /**
     * Ensure that backward skip rule value is properly set when value in json is an empty array
     * @throws IOException, error while converting json string to object
     */

    @Test
    public void deserialize_shouldCorrectlySetBackwardSkipValueOfContainsRuleForMultipleChoiceFieldThatHasEmptyValue() throws IOException {
        FieldType ranking = new FieldType(1, "Ranking", FieldType.Code.RANKING);
        Field target = new Field(TARGET_ID);
        target.setType(ranking);
        target.setChoiceGroup(sports);
        when(fieldService.getFieldById(TARGET_ID)).thenReturn(target);
        OperatorDescriptor contains = new OperatorDescriptor(7);
        when(fieldService.getOperatorDescriptorById(CONTAINS_DESCRIPTOR_ID)).thenReturn(contains);
        String jsonRule =
                "{" +
                        "\"type\": " + Rule.Type.BACKWARD_SKIP + ", " +
                        "\"owner\": "+ OWNER_ID +", " +
                        "\"target\": "+ TARGET_ID +", " +
                        "\"operatorDescriptor\": "+ CONTAINS_DESCRIPTOR_ID + ", " +
                        "\"value\": \"\"" +
                        "}";
        Rule desRule = mapper.readValue(jsonRule, Rule.class);

        Assert.assertThat(desRule.getValue(), is("1#0|2#0|3#0"));
    }

}