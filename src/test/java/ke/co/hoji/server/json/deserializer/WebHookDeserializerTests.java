package ke.co.hoji.server.json.deserializer;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.WebHook;

@RunWith(MockitoJUnitRunner.class)
public class WebHookDeserializerTests {

    private ObjectMapper mapper;

    @Mock
    private FormService formService;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(WebHook.class, new WebHookDeserializer(formService));
        mapper.registerModule(module);
        when(formService.getFormById(1)).thenReturn(new Form(1));
    }

    @Test
    public void shouldDeserializeWebHook() throws Exception {
        String json = "{\"id\": null, \"targetUrl\": \"http:localhost:3030\", \"forms\": [\"1\"], "
                + "\"eventTypes\": [\"CREATE\",\"MODIFY\"]}";
        WebHook desWebHook = mapper.readValue(json, WebHook.class);

        assertNull(desWebHook.getId());
        assertThat(desWebHook.getTargetUrl(), is("http:localhost:3030"));
        assertThat(desWebHook.getForms(), contains(hasProperty("id", is(1))));
        assertThat(desWebHook.getEventTypes(), contains(EventType.CREATE, EventType.MODIFY));
    }

    @Test
    public void shouldDeserializeWebHookWithEmptyAndNullForms() throws Exception {
        String json = "{\"id\": null, \"targetUrl\": \"http:localhost:3030\", \"forms\": [], "
                + "\"eventTypes\": [\"CREATE\",\"MODIFY\"]}";
        WebHook desWebHook = mapper.readValue(json, WebHook.class);

        assertNull(desWebHook.getId());
        assertThat(desWebHook.getTargetUrl(), is("http:localhost:3030"));
        assertThat(desWebHook.getForms(), hasSize(0));
        assertThat(desWebHook.getEventTypes(), contains(EventType.CREATE, EventType.MODIFY));

        json = "{\"id\": null, \"targetUrl\": \"http:localhost:3030\", \"forms\": null, \"eventTypes\": [\"CREATE\",\"MODIFY\"]}";
        desWebHook = mapper.readValue(json, WebHook.class);

        assertNull(desWebHook.getId());
        assertThat(desWebHook.getTargetUrl(), is("http:localhost:3030"));
        assertThat(desWebHook.getForms(), hasSize(0));
        assertThat(desWebHook.getEventTypes(), contains(EventType.CREATE, EventType.MODIFY));
    }

    @Test
    public void shouldDeserializeWebHookWithEmptyAndNullEventTypes() throws Exception {
        String json = "{\"id\": null, \"targetUrl\": \"http:localhost:3030\", \"forms\": [\"1\"], "
                + "\"eventTypes\": []}";
        WebHook desWebHook = mapper.readValue(json, WebHook.class);

        assertNull(desWebHook.getId());
        assertThat(desWebHook.getTargetUrl(), is("http:localhost:3030"));
        assertThat(desWebHook.getForms(), contains(hasProperty("id", is(1))));
        assertThat(desWebHook.getEventTypes(), hasSize(0));

        json = "{\"id\": null, \"targetUrl\": \"http:localhost:3030\", \"forms\": [\"1\"], \"eventTypes\": null}";
        desWebHook = mapper.readValue(json, WebHook.class);

        assertNull(desWebHook.getId());
        assertThat(desWebHook.getTargetUrl(), is("http:localhost:3030"));
        assertThat(desWebHook.getForms(), contains(hasProperty("id", is(1))));
        assertThat(desWebHook.getEventTypes(), hasSize(0));
    }

}
