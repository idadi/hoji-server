package ke.co.hoji.server.json.deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.service.model.ChoiceGroupService;
import ke.co.hoji.server.dao.model.JdbcChoiceDao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 11/06/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class ChoiceGroupDeserializerTests {

    private static final int CHOICE_ID = 1;
    private static final int CHOICE_GROUP_ID = CHOICE_ID;
    private static final String NAME = "Group";
    private ObjectMapper mapper;

    @Mock
    private ChoiceGroupService choiceGroupService;

    @Mock
    private JdbcChoiceDao choiceDao;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Choice.class, new ChoiceDeserializer(choiceDao));
        module.addDeserializer(ChoiceGroup.class, new ChoiceGroupDeserializer(choiceGroupService));
        mapper.registerModule(module);

        ChoiceGroup choiceGroup = new ChoiceGroup(CHOICE_GROUP_ID);
        choiceGroup.setName(NAME);
        Choice choice = new Choice(CHOICE_ID);
        when(choiceGroupService.getChoiceGroupById(CHOICE_GROUP_ID)).thenReturn(choiceGroup);
        when(choiceDao.getById(CHOICE_ID)).thenReturn(choice);
    }

    @Test
    public void shouldDeserializeChoiceGroupUsingId() throws Exception {
        ChoiceGroup desChoiceGroup = mapper.readValue(String.valueOf(CHOICE_GROUP_ID), ChoiceGroup.class);

        Assert.assertThat(desChoiceGroup.getId(), is(CHOICE_GROUP_ID));
    }

    @Test
    public void shouldDeserializeChoiceGroupFromJson() throws Exception {
        String jsonGroup =
                "{" +
                        "\"id\": \""+ CHOICE_GROUP_ID +"\", " +
                        "\"name\": \""+ NAME +"\"" +
                "}";
        ChoiceGroup desGroup = mapper.readValue(jsonGroup, ChoiceGroup.class);

        Assert.assertThat(desGroup.getId(), is(CHOICE_GROUP_ID));
        Assert.assertThat(desGroup.getName(), is(NAME));

        jsonGroup =
                "{" +
                        "\"id\": \"\", " +
                        "\"name\": \""+ NAME +"\"" +
                        "}";
        desGroup = mapper.readValue(jsonGroup, ChoiceGroup.class);

        Assert.assertNull(desGroup.getId());
        Assert.assertThat(desGroup.getName(), is(NAME));
    }

    @Test
    public void shouldDeserializeChoiceGroupFromJsonWithChoice() throws Exception {
        String jsonGroup =
                "{" +
                        "\"id\": \""+ CHOICE_GROUP_ID +"\", " +
                        "\"name\": \""+ NAME +"\", " +
                        "\"choices\": [{ \"id\": \""+ CHOICE_ID +"\" }]" +
                "}";
        ChoiceGroup desGroup = mapper.readValue(jsonGroup, ChoiceGroup.class);

        Assert.assertThat(desGroup.getId(), is(CHOICE_GROUP_ID));
        Assert.assertThat(desGroup.getName(), is(NAME));
        Assert.assertThat(desGroup.getChoices(), hasSize(1));
        Assert.assertThat(desGroup.getChoices().get(0).getId(), is(CHOICE_ID));
    }
}