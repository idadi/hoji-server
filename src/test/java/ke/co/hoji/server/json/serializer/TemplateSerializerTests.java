package ke.co.hoji.server.json.serializer;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.Template;

@RunWith(MockitoJUnitRunner.class)
public class TemplateSerializerTests {

    @Mock
    private MessageSource messageSource;

    private ObjectMapper mapper;

    @Before
    public void setup() {
        Mockito.when(messageSource.getMessage(EventType.WELCOME.name(), null, Locale.getDefault()))
                .thenReturn("Welcome");
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(EventType.class, new EventTypeSerializer(messageSource));
        module.addSerializer(Template.class, new TemplateSerializer());
        mapper.registerModule(module);
    }

    @Test
    public void shouldSerializeEmailTemplate() throws JsonProcessingException {
        Template welcomeTemplate = new Template();
        welcomeTemplate.setId(1);
        welcomeTemplate.setEventType(EventType.WELCOME);
        welcomeTemplate.setTemplate("<b>Welcome!</b>");
        String serialized = mapper.writeValueAsString(welcomeTemplate);

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("id", is(welcomeTemplate.getId())));
        assertThat(serialized, hasJsonPath("eventType.label", is("Welcome")));
        assertThat(serialized, hasJsonPath("eventType.value", is(EventType.WELCOME.name())));
        assertThat(serialized, hasJsonPath("template", is(welcomeTemplate.getTemplate())));
    }

}
