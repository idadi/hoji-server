package ke.co.hoji.server.json.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasNoJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 21/02/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class FieldSerializerTests {

    @Mock
    private UserService userService;
    private ObjectMapper mapper;

    @Before
    public void setup() {
        User testUser = new User();
        testUser.setId(2);
        testUser.setFullName("Test User");
        when(userService.findById(anyInt())).thenReturn(testUser);

        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(Survey.class, new SurveySerializer(userService));
        module.addSerializer(Form.class, new FormSerializer());
        module.addSerializer(Field.class, new FieldSerializer());
        mapper.registerModule(module);
    }

    @Test
    public void serializedFieldShouldHaveForm() throws JsonProcessingException {
        String serialized = mapper.writeValueAsString(getChoiceField());

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("form"));
    }

    @Test
    public void serializedFieldShouldHaveParent() throws JsonProcessingException {
        String serialized = mapper.writeValueAsString(getFieldWithParent());

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("parent"));
    }

    @Test
    public void serializedParentFieldNotShouldHaveChildren() throws JsonProcessingException {
        Field matrixField = getMatrixField();
        String serialized = mapper.writeValueAsString(matrixField);

        assertThat(serialized, isJson());
        assertThat(matrixField.getChildren(), hasSize(1));
        assertThat(serialized, hasNoJsonPath("children"));
    }

    @Test
    public void serializeFieldWithoutOutputType() throws JsonProcessingException {
        mapper.writeValueAsString(getFieldWithoutOutputType());
    }

    @Test
    public void serializedFieldShouldHaveChoiceGroup() throws JsonProcessingException {
        String serialized = mapper.writeValueAsString(getChoiceField());

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("choiceGroup"));
    }

    @Test
    public void serializedFieldShouldHaveChoices() throws JsonProcessingException {
        String serialized = mapper.writeValueAsString(getChoiceField());

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("choiceGroup.choices"));
    }

    @Test
    public void serializedFieldShouldHaveChoiceFilterField() throws JsonProcessingException {
        String serialized = mapper.writeValueAsString(getChoiceFieldWithFilter());

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("choiceFilterField"));
    }

    @Test
    public void serializedFieldShouldHaveRecordForms() throws JsonProcessingException {
        String serialized = mapper.writeValueAsString(getFieldWithRecords());

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("recordForms"));
    }

    @Test
    public void serializedFieldShouldHaveMissingValue() throws JsonProcessingException {
        String serialized = mapper.writeValueAsString(getFieldWithRecords());

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("missingValue", is("John Doe")));
    }

    private Field getFieldWithParent() {
        Form form = getForm();
        Field parent = getField(1);
        Field fieldWithParent = getField(2);
        fieldWithParent.setParent(parent);
        fieldWithParent.setType(new FieldType(1, "String", "STR"));
        fieldWithParent.setForm(form);
        return fieldWithParent;
    }

    private Field getMatrixField() {
        Form form = getForm();
        Field matrixField = getField(1);
        List<Field> children = new ArrayList<>();
        FieldType testMatrixType = new FieldType(1, "Matrix", "MTX");
        testMatrixType.setGroup(0);
        testMatrixType.setDataType(FieldType.DataType.STRING);
        matrixField.setType(testMatrixType);
        matrixField.setChildren(children);
        matrixField.setForm(form);
        children.add(getField(2));
        return matrixField;
    }

    private Field getFieldWithoutOutputType() {
        Form form = getForm();
        Field fieldWithoutOutputType = getField(1);
        fieldWithoutOutputType.setOutputType(null);
        fieldWithoutOutputType.setType(new FieldType(1, "Coded", "CD"));
        fieldWithoutOutputType.setForm(form);
        return fieldWithoutOutputType;
    }

    private Field getChoiceField() {
        Form form = getForm();
        Field choiceField = getField(1);
        choiceField.setType(new FieldType(1, "Coded", "CD"));
        Choice choice = new Choice(1, "Choice", "CODE", 0, false);
        ChoiceGroup choiceGroup = new ChoiceGroup(1, "Choice Group");
        choiceGroup.setChoices(Collections.singletonList(choice));
        choiceField.setChoiceGroup(choiceGroup);
        choiceField.setForm(form);
        return choiceField;
    }

    private Field getChoiceFieldWithFilter() {
        Form form = getForm();
        Field choiceField = getField(1);
        choiceField.setType(new FieldType(1, "Coded", "CD"));
        Choice choice = new Choice(1, "Choice", "CODE", 0, false);
        ChoiceGroup choiceGroup = new ChoiceGroup(1, "Choice Group");
        choiceGroup.setChoices(Collections.singletonList(choice));
        choiceField.setChoiceGroup(choiceGroup);
        choiceField.setChoiceFilterField(getField(2));
        choiceField.setForm(form);
        return choiceField;
    }

    private Field getFieldWithRecords() {
        Form form = getForm();
        Field fieldWithRecords = getField(1);
        fieldWithRecords.setType(new FieldType(1, "Coded", "CD"));
        fieldWithRecords.setForm(form);
        Form recordForm = new Form();
        recordForm.setId(1);
        recordForm.setName("Record Form");
        fieldWithRecords.setRecordForms(Collections.singletonList(recordForm));
        return fieldWithRecords;
    }

    private Field getField(int id) {
        String name = String.valueOf(id);
        String description = "Question " + id;
        BigDecimal ordinal = new BigDecimal(id);
        Field field = new Field(
                id, name, true, description, ordinal, false,
                false, true, Field.OutputType.STRING, 1);
        field.setMissingValue("John Doe");
        return field;
    }

    private Form getForm() {
        Survey survey = getSurvey();

        Form form = new Form(1, "Test Form", true, new BigDecimal(1),
                0, 1);
        form.setSurvey(survey);
        return form;
    }

    private Survey getSurvey() {
        Survey survey = new Survey(1, "Test Project", true, "CODE");
        survey.setUserId(2);
        survey.setStatus(2);
        survey.setAccess(Survey.Access.PRIVATE);
        survey.setFree(false);
        return survey;
    }

}