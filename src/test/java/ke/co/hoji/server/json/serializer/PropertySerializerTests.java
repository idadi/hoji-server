package ke.co.hoji.server.json.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Survey;
import org.junit.Before;
import org.junit.Test;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasNoJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class PropertySerializerTests {

    private ObjectMapper mapper;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(Property.class, new PropertySerializer());
        mapper.registerModule(module);
    }

    @Test
    public void serialize_shouldSerializePropertyToKeyValue() throws JsonProcessingException {
        Property property = new Property("form.128", "500", new Survey());
        String serialized = mapper.writeValueAsString(property);

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("key", equalTo("form.128")));
        assertThat(serialized, hasJsonPath("value", equalTo("500")));
        assertThat(serialized, hasNoJsonPath("survey"));
    }

}