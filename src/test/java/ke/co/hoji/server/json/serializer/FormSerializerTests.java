package ke.co.hoji.server.json.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Collections;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasNoJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 21/02/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class FormSerializerTests {

    @Mock
    private UserService userService;
    private ObjectMapper mapper;

    @Before
    public void setup() {
        User testUser = new User();
        testUser.setId(2);
        testUser.setFullName("Test User");
        when(userService.findById(anyInt())).thenReturn(testUser);

        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(Survey.class, new SurveySerializer(userService));
        module.addSerializer(Form.class, new FormSerializer());
        mapper.registerModule(module);
    }

    @Test
    public void serializedFormShouldHaveSurvey() throws JsonProcessingException {
        String serialized = mapper.writeValueAsString(getForm());

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("survey"));
    }

    @Test
    public void serializedFormShouldNotHaveFields() throws JsonProcessingException {
        Form formWithFields = getForm();
        formWithFields.setFields(Collections.singletonList(new Field(1)));
        String serialized = mapper.writeValueAsString(formWithFields);

        assertThat(formWithFields.getFields(), hasSize(1));
        assertThat(serialized, isJson());
        assertThat(serialized, hasNoJsonPath("fields"));
    }
    
    @Test
    public void serializedFormShouldHaveOutputStrategy() throws JsonProcessingException {
        Form formWithFields = getForm();
        formWithFields.setOutputStrategy(Collections.singletonList(Form.OutputStrategy.TABLE));
        String serialized = mapper.writeValueAsString(formWithFields);

        assertThat(formWithFields.getOutputStrategy(), hasSize(1));
        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("outputStrategy"));
    }

    private Form getForm() {
        Survey survey = getSurvey();
        Form form = new Form(1, "Test Form", true, new BigDecimal(1),
                0, 1);
        form.setSurvey(survey);
        return form;
    }

    private Survey getSurvey() {
        Survey survey = new Survey(1, "Test Project", true, "CODE");
        survey.setUserId(2);
        survey.setStatus(2);
        survey.setAccess(Survey.Access.PRIVATE);
        survey.setFree(false);
        return survey;
    }
}