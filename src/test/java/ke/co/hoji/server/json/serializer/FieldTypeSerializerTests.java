package ke.co.hoji.server.json.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import ke.co.hoji.core.data.model.FieldType;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by geoffreywasilwa on 13/03/2017.
 */
public class FieldTypeSerializerTests {

    private ObjectMapper mapper;

    private MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("/i18/strings");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Before
    public void setup() {
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(FieldType.class, new FieldTypeSerializer(messageSource()));
        mapper.registerModule(module);
    }

    @Test
    public void serializedFieldTypeShouldHaveGroupName() throws JsonProcessingException {
        FieldType testMatrixType = new FieldType(1, "Matrix", "MTX");
        testMatrixType.setGroup(0);
        testMatrixType.setDataType(3);
        String serialized = mapper.writeValueAsString(testMatrixType);

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("groupName", equalTo("Categorical")));
    }

}