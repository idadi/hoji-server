package ke.co.hoji.server.json.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasNoJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * Created by geoffreywasilwa on 21/02/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class SurveySerializerTests {

    @Mock
    private UserService userService;
    private ObjectMapper mapper;

    @Before
    public void setup() {
        User testUser = new User();
        testUser.setId(2);
        testUser.setFullName("Test User");
        when(userService.findById(anyInt())).thenReturn(testUser);

        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(Survey.class, new SurveySerializer(userService));
        mapper.registerModule(module);
    }

    @Test public void serializedSurveyShouldNotHaveForms() throws JsonProcessingException {
        String serialized = mapper.writeValueAsString(getSurvey());

        assertThat(serialized, isJson());
        assertThat(serialized, hasNoJsonPath("forms"));
    }

    @Test public void serializedSurveyShouldHaveOwnerName() throws JsonProcessingException {
        String serialized = mapper.writeValueAsString(getSurvey());

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("user.fullName"));
    }

    private Survey getSurvey() {
        Survey survey = new Survey(1, "Test Project", true, "CODE");
        survey.setUserId(2);
        survey.setStatus(2);
        survey.setAccess(Survey.Access.PRIVATE);
        survey.setFree(false);
        return survey;
    }

}