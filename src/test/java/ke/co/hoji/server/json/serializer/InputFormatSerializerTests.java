package ke.co.hoji.server.json.serializer;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import ke.co.hoji.core.data.model.Field.InputFormat;

public class InputFormatSerializerTests {

    private ObjectMapper mapper;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(InputFormat.class, new InputFormatSerializer());
        mapper.registerModule(module);
    }

    @Test
    public void shouldSerializeInputFormatValue() throws JsonProcessingException {
        InputFormat allCaps = InputFormat.ALL_CAPS;
        String serialized = mapper.writeValueAsString(allCaps);

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("label", is(allCaps.getLabel())));
        assertThat(serialized, hasJsonPath("value", is(allCaps.getValue())));
    }

}
