package ke.co.hoji.server.json.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.controller.report.model.Filter;
import ke.co.hoji.server.controller.report.model.Filter.FilterBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by geoffreywasilwa on 11/06/2017.
 */
public class FilterSerializerTests {

    private ObjectMapper mapper;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(Filter.class, new FilterSerializer());
        mapper.registerModule(module);
    }

    @Test
    public void serializedFilterShouldHaveAllProperties() throws Exception {
        Filter filter = new FilterBuilder(new Form(1))
                .selectedUsers(Arrays.asList(1,2))
                .completed("Today")
                .uploaded("Today")
                .build();
        String expectedFromDate = Utils.formatIsoDateTime(filter.getCompletedFrom());
        String expectedToDate = Utils.formatIsoDateTime(filter.getCompletedTo());

        String serialized = mapper.writeValueAsString(filter);

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("selectedUsers", equalTo(Arrays.asList(1,2))));
        assertThat(serialized, hasJsonPath("createdFrom", equalTo(null)));
        assertThat(serialized, hasJsonPath("createdTo", equalTo(null)));
        assertThat(serialized, hasJsonPath("completed", equalTo("Today")));
        assertThat(serialized, hasJsonPath("completedAfter", equalTo(expectedFromDate)));
        assertThat(serialized, hasJsonPath("completedBefore", equalTo(expectedToDate)));
        assertThat(serialized, hasJsonPath("uploaded", equalTo("Today")));
        assertThat(serialized, hasJsonPath("uploadedAfter", equalTo(expectedFromDate)));
        assertThat(serialized, hasJsonPath("uploadedBefore", equalTo(expectedToDate)));
    }

}