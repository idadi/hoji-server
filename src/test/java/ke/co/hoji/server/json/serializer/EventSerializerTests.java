package ke.co.hoji.server.json.serializer;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import ke.co.hoji.server.event.model.EventType;

@RunWith(MockitoJUnitRunner.class)
public class EventSerializerTests {

    @Mock
    private MessageSource messageSource;

    private ObjectMapper mapper;

    @Before
    public void setup() {
        Mockito.when(messageSource.getMessage(EventType.WELCOME.name(), null, Locale.getDefault()))
                .thenReturn("Welcome");
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(EventType.class, new EventTypeSerializer(messageSource));
        mapper.registerModule(module);
    }

    @Test
    public void shouldSerializeEventTypeEnum() throws JsonProcessingException {
        EventType welcome = EventType.WELCOME;
        String serialized = mapper.writeValueAsString(welcome);

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("label", is("Welcome")));
        assertThat(serialized, hasJsonPath("value", is(welcome.name())));
    }

}
