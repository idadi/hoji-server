package ke.co.hoji.server.json.serializer;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import ke.co.hoji.core.data.model.Field.Uniqueness;

public class UniquenessSerializerTests {

    private ObjectMapper mapper;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(Uniqueness.class, new UniquenessSerializer());
        mapper.registerModule(module);
    }

    @Test
    public void shouldSerializeUniquenessValue() throws JsonProcessingException {
        Uniqueness simple = Uniqueness.SIMPLE;
        String serialized = mapper.writeValueAsString(simple);

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("label", is(simple.getLabel())));
        assertThat(serialized, hasJsonPath("value", is(simple.getValue())));
    }

}
