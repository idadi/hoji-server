package ke.co.hoji.server.json.serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

public class ChoiceSerializerTests {

    private ObjectMapper mapper;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(Choice.class, new ChoiceSerializer());
        mapper.registerModule(module);
    }

    @Test
    public void shouldSerializeChoicePointingToItself() throws JsonProcessingException {
        Choice choice = new Choice(1, "Baringo", "B", 0, false);
        choice.setParent(choice);
        String serialized = mapper.writeValueAsString(choice);

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("id", is(1)));
        assertThat(serialized, hasJsonPath("name", is("Baringo")));
        assertThat(serialized, hasJsonPath("parent.id", is(1)));
        assertThat(serialized, hasJsonPath("parent.name", is("Baringo")));
    }

    @Test
    public void shouldSerializeChoiceWithNullCode() throws JsonProcessingException {
        Choice choice = new Choice(1, "Baringo", null, 0, false);
        String serialized = mapper.writeValueAsString(choice);

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("id", is(1)));
        assertThat(serialized, hasJsonPath("code", nullValue()));
    }

    @Test
    public void shouldSerializeFromChoiceGroup() throws JsonProcessingException {
        Choice choice = new Choice(1, "Baringo", null, 0, false);
        choice.setParent(choice);
        ChoiceGroup choiceGroup = new ChoiceGroup(1, "County");
        choiceGroup.setChoices(Collections.singletonList(choice));

        String serialized = mapper.writeValueAsString(choiceGroup);

        assertThat(serialized, isJson());
        assertThat(serialized, hasJsonPath("id", is(1)));
        assertThat(serialized, hasJsonPath("name", is("County")));
        assertThat(serialized, hasJsonPath("choices.[0].id", is(1)));
        assertThat(serialized, hasJsonPath("choices.[0].name", is("Baringo")));
        assertThat(serialized, hasJsonPath("choices.[0].parent.id", is(1)));
        assertThat(serialized, hasJsonPath("choices.[0].parent.name", is("Baringo")));
    }

}