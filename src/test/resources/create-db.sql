SET MODE MySQL;

SET COLLATION ENGLISH STRENGTH SECONDARY;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `code` char(5) NOT NULL,
  `public_survey_access` int(11) NOT NULL DEFAULT '1',
  `tenant_user_id` int(11) DEFAULT NULL,
  `password` char(60) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `middle_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `postal_address` varchar(45) DEFAULT NULL,
  `physical_address` varchar(45) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `default_roles` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `min_credits` int(11) NOT NULL DEFAULT '500',
  `tax_rate` decimal(15,2) NOT NULL DEFAULT '16.00',
  `discount_rate` decimal(15,2) NOT NULL DEFAULT '0.00',
  `master` tinyint(1) NOT NULL DEFAULT '0',
  `slave_status` int(11) NOT NULL DEFAULT '0',
  `master_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_user_tenant_idx` (`tenant_user_id`),
  KEY `fk_user_master` (`master_user_id`),
  CONSTRAINT `fk_user_master` FOREIGN KEY (`master_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_tenant` FOREIGN KEY (`tenant_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `choice`;
CREATE TABLE `choice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `scale` int(11) NOT NULL DEFAULT '0',
  `loner` tinyint(1) NOT NULL DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `choice_name_user_id_index` (`name`,`user_id`),
  KEY `fk_choice_user_idx` (`user_id`),
  KEY `fk_choice_parent_idx` (`parent_id`),
  CONSTRAINT `fk_choice_parent` FOREIGN KEY (`parent_id`) REFERENCES `choice` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_choice_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `choice_group`;
CREATE TABLE `choice_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ordering_strategy` int(11) NOT NULL DEFAULT '0',
  `exclude_last` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `choice_group_name_user_id_index` (`name`,`user_id`),
  KEY `fk_choice_group_username_idx` (`user_id`),
  CONSTRAINT `fk_choice_group_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `choice_group_choice`;
CREATE TABLE `choice_group_choice` (
  `choice_group_id` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL,
  `ordinal` decimal(10,2) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `scale` int(11) NOT NULL DEFAULT '0',
  `loner` tinyint(1) NOT NULL DEFAULT '0',
  `choice_parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`choice_group_id`,`choice_id`),
  KEY `fk_choice_group_choice_choice_idx` (`choice_id`),
  KEY `fk_choice_group_choice_choice_parent` (`choice_parent_id`),
  CONSTRAINT `fk_choice_group_choice_choice` FOREIGN KEY (`choice_id`) REFERENCES `choice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_choice_group_choice_choice_group` FOREIGN KEY (`choice_group_id`) REFERENCES `choice_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_choice_group_choice_choice_parent` FOREIGN KEY (`choice_parent_id`) REFERENCES `choice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `survey`;
CREATE TABLE `survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `test_mode` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '0',
  `free` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_survey_user_idx` (`user_id`),
  CONSTRAINT `fk_survey_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `form`;
CREATE TABLE `form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `ordinal` decimal(6,2) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `gps_capture` int(11) NOT NULL DEFAULT '1',
  `min_gps_accuracy` int(11) NOT NULL DEFAULT '-1',
  `no_of_gps_attempts` int(11) NOT NULL DEFAULT '3',
  `output_strategy` varchar(255) DEFAULT NULL,
  `transposable` tinyint(1) NOT NULL DEFAULT '0',
  `transposition_categories` varchar(255) DEFAULT NULL,
  `transposition_value_label` varchar(45) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `minor_modified` tinyint(1) NOT NULL DEFAULT '0',
  `major_modified` tinyint(1) NOT NULL DEFAULT '0',
  `minor_version` int(11) NOT NULL DEFAULT '0',
  `major_version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_form_survey_idx` (`survey_id`),
  CONSTRAINT `fk_form_survey` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `field_type`;
CREATE TABLE `field_type` (
  `id` int(11) NOT NULL,
  `code` char(3) NOT NULL,
  `name` varchar(45) NOT NULL,
  `ordinal` decimal(6,2) NOT NULL DEFAULT '0.00',
  `data_type` int(11) NOT NULL,
  `bracket` int(11) NOT NULL DEFAULT '0',
  `widget_class` varchar(255) NOT NULL,
  `matrix_widget_class` varchar(255) NOT NULL,
  `response_class` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_field_type_1_idx` (`response_class`)
);

DROP TABLE IF EXISTS `field`;
CREATE TABLE `field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `variable_name` varchar(256) DEFAULT NULL,
  `description` mediumtext,
  `ordinal` decimal(6,2) NOT NULL,
  `instructions` mediumtext,
  `field_type_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `captioning` tinyint(1) NOT NULL DEFAULT '0',
  `searchable` tinyint(1) NOT NULL DEFAULT '0',
  `filterable` tinyint(1) NOT NULL DEFAULT '0',
  `output_type` tinyint(1) DEFAULT NULL,
  `tag` varchar(45) DEFAULT NULL,
  `calculated` int(11) NOT NULL DEFAULT '1',
  `value_script` mediumtext,
  `enable_if_null` tinyint(1) NOT NULL DEFAULT '0',
  `validation_script` mediumtext,
  `default_value` varchar(45) DEFAULT NULL,
  `missing_value` varchar(255) DEFAULT NULL,
  `missing_action` int(11) NOT NULL DEFAULT '0',
  `min_value` varchar(45) DEFAULT NULL,
  `min_action` int(11) NOT NULL DEFAULT '2',
  `max_value` varchar(45) DEFAULT NULL,
  `max_action` int(11) NOT NULL DEFAULT '2',
  `regex_value` varchar(1000) DEFAULT NULL,
  `regex_action` int(11) NOT NULL DEFAULT '2',
  `uniqueness` int(11) NOT NULL DEFAULT '0',
  `unique_action` int(11) DEFAULT '2',
  `choice_group_id` int(11) DEFAULT NULL,
  `choice_filter_field_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `pipe_source_id` int(11) DEFAULT NULL,
  `reference_field_id` int(11) DEFAULT NULL,
  `main_record_field_id` int(11) DEFAULT NULL,
  `matrix_record_field_id` int(11) DEFAULT NULL,
  `response_inheriting` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_field_section_idx` (`form_id`),
  KEY `fk_field_type_idx` (`field_type_id`),
  KEY `fk_field_choiuce_group_idx` (`choice_group_id`),
  KEY `fk_field_parent` (`parent_id`),
  KEY `fk_field_pipe_source` (`pipe_source_id`),
  KEY `fk_field_choice_filter` (`choice_filter_field_id`),
  KEY `fk_field_reference_field` (`reference_field_id`),
  KEY `fk_field_main_record_field` (`main_record_field_id`),
  KEY `fk_field_matrix_record_field` (`matrix_record_field_id`),
  CONSTRAINT `fk_field_choice_filter` FOREIGN KEY (`choice_filter_field_id`) REFERENCES `field` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_field_choice_group` FOREIGN KEY (`choice_group_id`) REFERENCES `choice_group` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_field_form` FOREIGN KEY (`form_id`) REFERENCES `form` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_field_main_record_field` FOREIGN KEY (`main_record_field_id`) REFERENCES `field` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_field_matrix_record_field` FOREIGN KEY (`matrix_record_field_id`) REFERENCES `field` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_field_parent` FOREIGN KEY (`parent_id`) REFERENCES `field` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_field_pipe_source` FOREIGN KEY (`pipe_source_id`) REFERENCES `field` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_field_reference_field` FOREIGN KEY (`reference_field_id`) REFERENCES `field` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_field_type` FOREIGN KEY (`field_type_id`) REFERENCES `field_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `property`;
CREATE TABLE `property` (
  `property_key` varchar(255) NOT NULL,
  `value` mediumtext NOT NULL,
  `survey_id` int(11) NOT NULL,
  PRIMARY KEY (`property_key`,`survey_id`),
  KEY `fk_property_survey` (`survey_id`),
  CONSTRAINT `fk_property_survey` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `user_id` int(11) NOT NULL,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`,`role`),
  CONSTRAINT `fk_role_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `field_record_form`;
CREATE TABLE `field_record_form` (
  `field_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  PRIMARY KEY (`field_id`,`form_id`),
  KEY `fk_record_form_field_idx` (`form_id`),
  CONSTRAINT `fk_field_record_form` FOREIGN KEY (`field_id`) REFERENCES `field` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_record_form_field` FOREIGN KEY (`form_id`) REFERENCES `form` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `operator_descriptor`;
CREATE TABLE `operator_descriptor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `class` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `rule`;
CREATE TABLE `rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `ordinal` decimal(6,2) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `operator_descriptor_id` int(11) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `target_id` int(11) NOT NULL,
  `combiner` int(11) NOT NULL DEFAULT '1',
  `grouper` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_rule_owner_idx` (`owner_id`),
  KEY `fk_rule_target_idx` (`target_id`),
  KEY `fk_rule_operator_descriptor_idx` (`operator_descriptor_id`),
  CONSTRAINT `fk_rule_operator_descriptor` FOREIGN KEY (`operator_descriptor_id`) REFERENCES `operator_descriptor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_rule_owner` FOREIGN KEY (`owner_id`) REFERENCES `field` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_rule_target` FOREIGN KEY (`target_id`) REFERENCES `field` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
