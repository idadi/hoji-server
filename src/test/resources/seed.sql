drop table if exists cash_transaction;

drop table if exists choice_group_choice;

drop table if exists choice;

drop table if exists credit_bundle;

drop table if exists credit_transaction;

drop table if exists dashboard_component;

drop table if exists email_template;

drop table if exists field_record_form;

drop table if exists item;

drop table if exists key_holder;

drop table if exists loggable_event;

drop table if exists missed_message;

drop table if exists property;

drop table if exists reference;

drop table if exists reference_level;

drop table if exists retired_choice;

drop table if exists role;

drop table if exists rule;

drop table if exists field;

drop table if exists choice_group;

drop table if exists field_type;

drop table if exists operator_descriptor;

drop table if exists translation;

drop table if exists language;

drop table if exists verification_token;

drop table if exists web_hook_detail;

drop table if exists form;

drop table if exists survey;

drop table if exists web_hook;

drop table if exists user;

create table credit_bundle
(
    id       int auto_increment
        primary key,
    name     varchar(45)    not null,
    credits  int            not null,
    price    decimal(10, 2) not null,
    expiry   int            not null,
    discount int            not null,
    bonus    int            not null
);

create table email_template
(
    id         int auto_increment
        primary key,
    event_type varchar(45) not null,
    template   text        not null
)
    collate = utf8mb4_unicode_ci;

create table field_type
(
    id                  int                        not null
        primary key,
    code                char(3)                    not null,
    name                varchar(45)                not null,
    ordinal             decimal(6, 2) default 0.00 not null,
    data_type           int                        not null,
    bracket             int           default 0    not null,
    widget_class        varchar(255)               not null,
    matrix_widget_class varchar(255)               not null,
    response_class      varchar(255)               not null
);

create index fk_field_type_1_idx
    on field_type (response_class);

create table item
(
    item_id     int auto_increment
        primary key,
    name        varchar(45)  not null,
    description varchar(255) null,
    price       int          null
)
    engine = MyISAM;

create table key_holder
(
    table_name varchar(50) not null,
    next_id    int         not null,
    constraint key_holder_table_name_uindex
        unique (table_name)
);

alter table key_holder
    add primary key (table_name);

create table operator_descriptor
(
    id    int auto_increment
        primary key,
    name  varchar(45)  not null,
    class varchar(255) not null
);

create table reference
(
    id        int         not null
        primary key,
    survey_id int         not null,
    level_0   varchar(45) not null,
    level_1   varchar(45) null,
    level_2   varchar(45) null,
    level_3   varchar(45) null,
    level_4   varchar(45) null,
    level_5   varchar(45) null,
    level_6   varchar(45) null
);

create index fk_reference_survey_idx
    on reference (survey_id);

create table retired_choice
(
    id          int                  not null
        primary key,
    new_id      int                  not null,
    code        varchar(45)          not null,
    name        text                 not null,
    description varchar(255)         null,
    scale       int        default 0 not null,
    loner       tinyint(1) default 0 not null,
    parent_id   int                  null,
    user_id     int                  not null
);

create table user
(
    id                   int auto_increment
        primary key,
    username             varchar(255)                 not null,
    code                 char(5)                      not null,
    public_survey_access int            default 1     not null,
    tenant_user_id       int                          null,
    password             char(60)                     not null,
    first_name           varchar(45)                  not null,
    middle_name          varchar(45)                  null,
    last_name            varchar(45)                  null,
    enabled              tinyint(1)                   not null,
    verified             tinyint(1)     default 0     not null,
    suspended                tinyint(1)     default 0     not null,
    telephone            varchar(45)                  null,
    postal_address       varchar(45)                  null,
    physical_address     varchar(45)                  null,
    date_created         datetime                     not null,
    default_roles        varchar(255)                 null,
    type                 int            default 0     not null,
    min_credits          int            default 500   not null,
    tax_rate             decimal(15, 2) default 16.00 not null,
    discount_rate        decimal(15, 2) default 0.00  not null,
    master               tinyint(1)     default 0     not null,
    slave_status         int            default 0     not null,
    master_user_id       int                          null,
    constraint code_UNIQUE
        unique (code),
    constraint username_UNIQUE
        unique (username),
    constraint fk_user_master
        foreign key (master_user_id) references user (id)
            on update cascade on delete cascade,
    constraint fk_user_tenant
        foreign key (tenant_user_id) references user (id)
            on update cascade on delete cascade
);

create table cash_transaction
(
    id                int auto_increment
        primary key,
    user_id           int            not null,
    date              datetime       not null,
    reference_no      char(36)       not null,
    type              char           null,
    amount_usd        decimal(10, 2) not null,
    amount_lcl        decimal(10, 2) null,
    currency          char(3)        null,
    payment_mode      varchar(45)    null,
    payment_reference varchar(1000)  null,
    bundle_id         int            null,
    constraint fk_cash_receipt_1
        foreign key (user_id) references user (id),
    constraint fk_cash_receipt_2
        foreign key (bundle_id) references credit_bundle (id)
);

create index fk_cash_receipt_1_idx
    on cash_transaction (user_id);

create index fk_cash_receipt_2_idx
    on cash_transaction (bundle_id);

create table choice
(
    id          int auto_increment
        primary key,
    code        varchar(45)          null,
    name        varchar(500)         not null,
    description varchar(255)         null,
    scale       int        default 0 not null,
    loner       tinyint(1) default 0 not null,
    parent_id   int                  null,
    user_id     int                  not null,
    constraint choice_name_user_id_index
        unique (name, user_id),
    constraint fk_choice_parent
        foreign key (parent_id) references choice (id)
            on update cascade on delete set null,
    constraint fk_choice_user
        foreign key (user_id) references user (id)
            on update cascade on delete cascade
);

create index fk_choice_parent_idx
    on choice (parent_id);

create index fk_choice_user_idx
    on choice (user_id);

create table choice_group
(
    id                int auto_increment
        primary key,
    name              varchar(255)  not null,
    ordering_strategy int default 0 not null,
    exclude_last      int default 0 not null,
    user_id           int           not null,
    constraint fk_choice_group_user
        foreign key (user_id) references user (id)
            on update cascade on delete cascade
);

create index fk_choice_group_username_idx
    on choice_group (user_id);

create table choice_group_choice
(
    choice_group_id  int                  not null,
    choice_id        int                  not null,
    ordinal          decimal(10, 2)       not null,
    code             varchar(45)          null,
    description      varchar(255)         null,
    scale            int        default 0 not null,
    loner            tinyint(1) default 0 not null,
    choice_parent_id int                  null,
    primary key (choice_group_id, choice_id),
    constraint fk_choice_group_choice_choice
        foreign key (choice_id) references choice (id)
            on update cascade on delete cascade,
    constraint fk_choice_group_choice_choice_group
        foreign key (choice_group_id) references choice_group (id)
            on update cascade on delete cascade,
    constraint fk_choice_group_choice_choice_parent
        foreign key (choice_parent_id) references choice (id)
            on update cascade on delete cascade
);

create index fk_choice_group_choice_choice_idx
    on choice_group_choice (choice_id);

create table loggable_event
(
    id              int auto_increment
        primary key,
    name            varchar(45)  not null,
    date            datetime     not null,
    user_id         int          not null,
    tenant_id       int          not null,
    type            varchar(45)  not null,
    subject_name    varchar(45)  not null,
    subject_id      varchar(45)  not null,
    description     varchar(256) not null,
    result          varchar(45)  not null,
    source          varchar(45)  not null,
    originator      varchar(45)  not null,
    failure_message varchar(45)  null,
    constraint fk_loggable_event_1
        foreign key (user_id) references user (id),
    constraint fk_loggable_event_2
        foreign key (user_id) references user (id)
)
    collate = utf8mb4_unicode_ci;

create index fk_loggable_event_1_idx
    on loggable_event (user_id);

create table role
(
    user_id int         not null,
    role    varchar(45) not null,
    primary key (user_id, role),
    constraint fk_role_user
        foreign key (user_id) references user (id)
            on update cascade on delete cascade
);

create table survey
(
    id        int auto_increment
        primary key,
    code      varchar(45)          not null,
    name      varchar(255)         not null,
    user_id   int                  not null,
    enabled   tinyint(1) default 1 not null,
    test_mode tinyint(1) default 0 not null,
    status    int        default 0 not null,
    access    int        default 0 not null,
    free      tinyint(1) default 0 not null,
    constraint fk_survey_user
        foreign key (user_id) references user (id)
            on update cascade on delete cascade
);

create table form
(
    id                        int auto_increment
        primary key,
    name                      varchar(45)           not null,
    description               varchar(255)          null,
    ordinal                   decimal(6, 2)         not null,
    survey_id                 int                   not null,
    gps_capture               int        default 1  not null,
    min_gps_accuracy          int        default -1 not null,
    no_of_gps_attempts        int        default 3  not null,
    output_strategy           varchar(255)          null,
    transposable              tinyint(1) default 0  not null,
    transposition_categories  varchar(255)          null,
    transposition_value_label varchar(45)           null,
    enabled                   tinyint(1) default 1  not null,
    minor_modified            tinyint(1) default 0  not null,
    major_modified            tinyint(1) default 0  not null,
    minor_version             int        default 0  not null,
    major_version             int        default 0  not null,
    constraint fk_form_survey
        foreign key (survey_id) references survey (id)
            on update cascade on delete cascade
);

create table credit_transaction
(
    id           bigint auto_increment
        primary key,
    user_id      int           not null,
    form_id      int           null,
    date         datetime      not null,
    credits      int           not null,
    reference_no varchar(1000) not null,
    type         char          not null,
    expiry       int           null,
    constraint fk_transaction_1
        foreign key (user_id) references user (id),
    constraint fk_transaction_2
        foreign key (form_id) references form (id)
            on update cascade on delete set null
);

create index fk_transaction_1_idx
    on credit_transaction (user_id);

create index fk_transaction_2_idx
    on credit_transaction (form_id);

create table dashboard_component
(
    id            int                        not null
        primary key,
    name          varchar(500)               not null,
    ordinal       decimal(6, 2) default 1.00 not null,
    configuration text                       not null,
    form_id       int                        not null,
    constraint dashboard_component_form_id_fk
        foreign key (form_id) references form (id)
            on update cascade on delete cascade
);

create table field
(
    id                     int auto_increment
        primary key,
    name                   varchar(45)          not null,
    variable_name          varchar(256)         null,
    description            text                 null,
    ordinal                decimal(6, 2)        not null,
    instructions           text                 null,
    field_type_id          int                  not null,
    form_id                int                  not null,
    enabled                tinyint(1) default 1 not null,
    captioning             tinyint(1) default 0 not null,
    searchable             tinyint(1) default 0 not null,
    filterable             tinyint(1) default 0 not null,
    output_type            int(1)               null,
    tag                    varchar(45)          null,
    calculated             int        default 1 not null,
    value_script           text                 null,
    enable_if_null         tinyint(1) default 0 not null,
    validation_script      text                 null,
    default_value          varchar(45)          null,
    missing_value          varchar(255)         null,
    missing_action         int        default 0 not null,
    min_value              varchar(45)          null,
    min_action             int        default 2 not null,
    max_value              varchar(45)          null,
    max_action             int        default 2 not null,
    regex_value            varchar(1000)        null,
    regex_action           int        default 2 not null,
    uniqueness             int        default 0 not null,
    unique_action          int        default 2 null,
    choice_group_id        int                  null,
    choice_filter_field_id int                  null,
    parent_id              int                  null,
    pipe_source_id         int                  null,
    reference_field_id     int                  null,
    main_record_field_id   int                  null,
    matrix_record_field_id int                  null,
    response_inheriting    tinyint(1) default 0 not null,
    constraint fk_field_choice_filter
        foreign key (choice_filter_field_id) references field (id)
            on update cascade on delete set null,
    constraint fk_field_choice_group
        foreign key (choice_group_id) references choice_group (id)
            on update cascade on delete cascade,
    constraint fk_field_form
        foreign key (form_id) references form (id)
            on update cascade on delete cascade,
    constraint fk_field_main_record_field
        foreign key (main_record_field_id) references field (id)
            on update cascade on delete set null,
    constraint fk_field_matrix_record_field
        foreign key (matrix_record_field_id) references field (id)
            on update cascade on delete set null,
    constraint fk_field_parent
        foreign key (parent_id) references field (id)
            on update cascade on delete set null,
    constraint fk_field_pipe_source
        foreign key (pipe_source_id) references field (id)
            on update cascade on delete set null,
    constraint fk_field_reference_field
        foreign key (reference_field_id) references field (id)
            on update cascade on delete set null,
    constraint fk_field_type
        foreign key (field_type_id) references field_type (id)
            on update cascade on delete cascade
);

create index fk_field_choiuce_group_idx
    on field (choice_group_id);

create index fk_field_section_idx
    on field (form_id);

create index fk_field_type_idx
    on field (field_type_id);

create table field_record_form
(
    field_id int not null,
    form_id  int not null,
    primary key (field_id, form_id),
    constraint fk_field_record_form
        foreign key (field_id) references field (id)
            on update cascade on delete cascade,
    constraint fk_record_form_field
        foreign key (form_id) references form (id)
            on update cascade on delete cascade
);

create index fk_record_form_field_idx
    on field_record_form (form_id);

create index fk_form_survey_idx
    on form (survey_id);

create table language
(
    id        int auto_increment
        primary key,
    name      varchar(45) not null,
    survey_id int         null,
    constraint fk_language_survey
        foreign key (survey_id) references survey (id)
            on update cascade on delete cascade
)
    collate = utf8mb4_unicode_ci;

create index fk_language_survey_idx
    on language (survey_id);

create table property
(
    `key`     varchar(255) not null,
    value     text         not null,
    survey_id int          not null,
    primary key (`key`, survey_id),
    constraint fk_property_survey
        foreign key (survey_id) references survey (id)
            on update cascade on delete cascade
);

create table reference_level
(
    id             int                  not null
        primary key,
    name           varchar(45)          not null,
    level          int                  not null,
    level_column   varchar(45)          not null,
    displayable    tinyint(1) default 0 not null,
    input_type     int        default 0 not null,
    rulable        tinyint(1) default 0 not null,
    missing_action int        default 0 not null,
    survey_id      int                  not null,
    enabled        tinyint(1) default 1 not null,
    constraint level
        unique (level),
    constraint level_column
        unique (level_column),
    constraint fk_reference_level_survey
        foreign key (survey_id) references survey (id)
            on update cascade on delete cascade
);

create index fk_reference_level_survey_idx
    on reference_level (survey_id);

create table rule
(
    id                     int auto_increment
        primary key,
    type                   int           not null,
    ordinal                decimal(6, 2) not null,
    owner_id               int           not null,
    operator_descriptor_id int           not null,
    value                  varchar(255)  null,
    target_id              int           not null,
    combiner               int default 1 not null,
    grouper                int default 0 not null,
    constraint fk_rule_operator_descriptor
        foreign key (operator_descriptor_id) references operator_descriptor (id)
            on update cascade on delete cascade,
    constraint fk_rule_owner
        foreign key (owner_id) references field (id)
            on update cascade on delete cascade,
    constraint fk_rule_target
        foreign key (target_id) references field (id)
            on update cascade on delete cascade
);

create index fk_rule_operator_descriptor_idx
    on rule (operator_descriptor_id);

create index fk_rule_owner_idx
    on rule (owner_id);

create index fk_rule_target_idx
    on rule (target_id);

create index fk_survey_user_idx
    on survey (user_id);

create table translation
(
    id              int auto_increment
        primary key,
    translatable    varchar(45) not null,
    translatable_id int         not null,
    language_id     int         not null,
    component       varchar(45) not null,
    value           text        not null,
    constraint unique_columns_idx
        unique (translatable, translatable_id, language_id, component),
    constraint fk_translation_language
        foreign key (language_id) references language (id)
            on update cascade on delete cascade
)
    collate = utf8mb4_unicode_ci;

create index fk_translation_1_idx
    on translation (language_id);

create index fk_user_tenant_idx
    on user (tenant_user_id);

create table verification_token
(
    id          int auto_increment
        primary key,
    token       char(36) null,
    user_id     int      not null,
    expiry_date bigint   not null,
    constraint fk_verification_token_user
        foreign key (user_id) references user (id)
            on update cascade on delete cascade
);

create index fk_verification_token_user_idx
    on verification_token (user_id);

create table web_hook
(
    id         int          not null
        primary key,
    target_url varchar(250) not null,
    user_id    int          not null,
    active     bit          null,
    secret     varchar(250) null,
    constraint web_hook_user_fk
        foreign key (user_id) references user (id)
            on update cascade on delete cascade
);

create table missed_message
(
    id               int auto_increment
        primary key,
    web_hook_id      int         null,
    main_record_uuid varchar(36) not null,
    date_created     datetime    not null,
    constraint missed_messages_web_hook_id_fk
        foreign key (web_hook_id) references web_hook (id)
            on update cascade on delete cascade
);

create table web_hook_detail
(
    web_hook_id int          not null,
    form_id     int          not null,
    event_type  varchar(100) not null,
    constraint web_hook_detail_form_id_fk
        foreign key (form_id) references form (id)
            on update cascade on delete cascade,
    constraint web_hook_detail_web_hook_id_fk
        foreign key (web_hook_id) references web_hook (id)
            on update cascade on delete cascade
);

INSERT INTO key_holder (table_name, next_id) VALUES ('dashboard_component', 1), ('web_hook', 1);

INSERT INTO user (id, username, code, public_survey_access, tenant_user_id, password, first_name, middle_name, last_name, enabled, verified, suspended, telephone, postal_address, physical_address, date_created, default_roles, type, min_credits, tax_rate, discount_rate, master, slave_status, master_user_id) VALUES (4, 'gitahi86@gmail.com', 'H4S8L', 0, 4, '$2a$10$Yi5233o/43Vx5wNI39vqveYyPXrfw0QONlCL0vNu5vqapu8AFlmum', 'Hoji', '', 'Ltd', 1, 1, 1, '+254724993479', null, null, '2015-12-22 12:18:36', 'DEVICE, VIEWER', 2, 500, 16.00, 5.00, 0, 0, null);
INSERT INTO user (id, username, code, public_survey_access, tenant_user_id, password, first_name, middle_name, last_name, enabled, verified, suspended, telephone, postal_address, physical_address, date_created, default_roles, type, min_credits, tax_rate, discount_rate, master, slave_status, master_user_id) VALUES (2, 'test.user@hoji.co.ke', '4QU3W', 1, 2, '$2a$10$Oeo7eE4sCplgG.wP3sbHDe7cJfNgRyAIyc5SDKbCA6H9eZhV9njfi', 'Test', null, 'User', 1, 1, 0, null, null, null, '2019-05-16 12:57:49', null, 1, 500, 16.00, 0.00, 0, 0, null);

INSERT INTO role (user_id, role) VALUES (2, 'CREATOR');
INSERT INTO role (user_id, role) VALUES (2, 'DESTROYER');
INSERT INTO role (user_id, role) VALUES (2, 'DEVICE');
INSERT INTO role (user_id, role) VALUES (2, 'DOWNLOADER');
INSERT INTO role (user_id, role) VALUES (2, 'VIEWER');

INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (1, 'V3NOD', 'Sample Project', 4, 1, 0, 1, 3, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (2, 'TST01', 'Network Maintenance', 2, 1, 0, 1, 0, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (3, 'TST02', 'Network Maintenance[**delete**]', 2, 1, 0, 1, 0, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (4, 'TST03', 'Network Maintenance[**form add**]', 2, 1, 0, 1, 0, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (5, 'TST04', 'Network Maintenance[**form edit**]', 2, 1, 0, 1, 0, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (6, 'TST04', 'Network Maintenance[**form b.edit**]', 2, 1, 0, 1, 0, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (7, 'TST05', 'Network Maintenance[**form delete**]', 2, 1, 0, 1, 0, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (8, 'TST06', 'Network Maintenance[**field add**]', 2, 1, 0, 1, 0, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (9, 'TST07', 'Network Maintenance[**field edit**]', 2, 1, 0, 1, 0, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (10, 'TST07', 'Network Maintenance[**field b.edit**]', 2, 1, 0, 1, 0, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (11, 'TST08', 'Network Maintenance[**field delete**]', 2, 1, 0, 1, 0, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (12, 'TST09', 'Network Maintenance[**rule add**]', 2, 1, 0, 1, 0, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (13, 'TST10', 'Network Maintenance[**rule >edit**]', 2, 1, 0, 1, 0, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (14, 'TST10', 'Network Maintenance[**rule <edit**]', 2, 1, 0, 1, 0, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (15, 'TST11', 'Network Maintenance[**rule >delete**]', 2, 1, 0, 1, 0, 0);
INSERT INTO survey (id, code, name, user_id, enabled, test_mode, status, access, free) VALUES (16, 'TST11', 'Network Maintenance[**rule <delete**]', 2, 1, 0, 1, 0, 0);

INSERT INTO form (id, name, description, ordinal, survey_id, gps_capture, min_gps_accuracy, no_of_gps_attempts, output_strategy, transposable, transposition_categories, transposition_value_label, enabled, minor_modified, major_modified, minor_version, major_version) VALUES (1, 'Sample Form', 'Tap to open', 1.00, 1, 1, -1, 3, 'PIVOT, DOWNLOAD', 0, null, null, 1, 0, 0, 9, 2);
INSERT INTO form (id, name, description, ordinal, survey_id, gps_capture, min_gps_accuracy, no_of_gps_attempts, output_strategy, transposable, transposition_categories, transposition_value_label, enabled, minor_modified, major_modified, minor_version, major_version) VALUES (2, 'Customer Feedback[**edit**]', 'Tap to open', 1.00, 5, 1, -1, 3, null, 0, null, null, 1, 0, 0, 0, 1);
INSERT INTO form (id, name, description, ordinal, survey_id, gps_capture, min_gps_accuracy, no_of_gps_attempts, output_strategy, transposable, transposition_categories, transposition_value_label, enabled, minor_modified, major_modified, minor_version, major_version) VALUES (3, 'Customer Feedback[**b.edit**]', 'Tap to open', 1.00, 6, 1, -1, 3, null, 0, null, null, 1, 0, 0, 0, 1);
INSERT INTO form (id, name, description, ordinal, survey_id, gps_capture, min_gps_accuracy, no_of_gps_attempts, output_strategy, transposable, transposition_categories, transposition_value_label, enabled, minor_modified, major_modified, minor_version, major_version) VALUES (4, 'Customer Feedback[**delete**]', 'Tap to open', 1.00, 7, 1, -1, 3, null, 0, null, null, 1, 0, 0, 0, 1);
INSERT INTO form (id, name, description, ordinal, survey_id, gps_capture, min_gps_accuracy, no_of_gps_attempts, output_strategy, transposable, transposition_categories, transposition_value_label, enabled, minor_modified, major_modified, minor_version, major_version) VALUES (5, 'Customer Feedback[**field add**]', 'Tap to open', 1.00, 8, 1, -1, 3, null, 0, null, null, 1, 0, 0, 0, 1);
INSERT INTO form (id, name, description, ordinal, survey_id, gps_capture, min_gps_accuracy, no_of_gps_attempts, output_strategy, transposable, transposition_categories, transposition_value_label, enabled, minor_modified, major_modified, minor_version, major_version) VALUES (6, 'Customer Feedback[**field edit**]', 'Tap to open', 1.00, 9, 1, -1, 3, null, 0, null, null, 1, 0, 0, 0, 1);
INSERT INTO form (id, name, description, ordinal, survey_id, gps_capture, min_gps_accuracy, no_of_gps_attempts, output_strategy, transposable, transposition_categories, transposition_value_label, enabled, minor_modified, major_modified, minor_version, major_version) VALUES (7, 'Customer Feedback[**field b.edit**]', 'Tap to open', 1.00, 10, 1, -1, 3, null, 0, null, null, 1, 0, 0, 0, 1);
INSERT INTO form (id, name, description, ordinal, survey_id, gps_capture, min_gps_accuracy, no_of_gps_attempts, output_strategy, transposable, transposition_categories, transposition_value_label, enabled, minor_modified, major_modified, minor_version, major_version) VALUES (8, 'Customer Feedback[**field delete**]', 'Tap to open', 1.00, 11, 1, -1, 3, null, 0, null, null, 1, 0, 0, 0, 1);
INSERT INTO form (id, name, description, ordinal, survey_id, gps_capture, min_gps_accuracy, no_of_gps_attempts, output_strategy, transposable, transposition_categories, transposition_value_label, enabled, minor_modified, major_modified, minor_version, major_version) VALUES (9, 'Customer Feedback[**rule add**]', 'Tap to open', 1.00, 12, 1, -1, 3, null, 0, null, null, 1, 0, 0, 0, 1);
INSERT INTO form (id, name, description, ordinal, survey_id, gps_capture, min_gps_accuracy, no_of_gps_attempts, output_strategy, transposable, transposition_categories, transposition_value_label, enabled, minor_modified, major_modified, minor_version, major_version) VALUES (10, 'Customer Feedback[**rule >edit**]', 'Tap to open', 1.00, 13, 1, -1, 3, null, 0, null, null, 1, 0, 0, 0, 1);
INSERT INTO form (id, name, description, ordinal, survey_id, gps_capture, min_gps_accuracy, no_of_gps_attempts, output_strategy, transposable, transposition_categories, transposition_value_label, enabled, minor_modified, major_modified, minor_version, major_version) VALUES (11, 'Customer Feedback[**rule <edit**]', 'Tap to open', 1.00, 14, 1, -1, 3, null, 0, null, null, 1, 0, 0, 0, 1);
INSERT INTO form (id, name, description, ordinal, survey_id, gps_capture, min_gps_accuracy, no_of_gps_attempts, output_strategy, transposable, transposition_categories, transposition_value_label, enabled, minor_modified, major_modified, minor_version, major_version) VALUES (12, 'Customer Feedback[**rule >delete**]', 'Tap to open', 1.00, 15, 1, -1, 3, null, 0, null, null, 1, 0, 0, 0, 1);
INSERT INTO form (id, name, description, ordinal, survey_id, gps_capture, min_gps_accuracy, no_of_gps_attempts, output_strategy, transposable, transposition_categories, transposition_value_label, enabled, minor_modified, major_modified, minor_version, major_version) VALUES (13, 'Customer Feedback[**rule <delete**]', 'Tap to open', 1.00, 16, 1, -1, 3, null, 0, null, null, 1, 0, 0, 0, 1);

INSERT INTO choice (id, code, name, description, scale, loner, parent_id, user_id) VALUES (1, '2', 'Female', '', 0, 0, null, 4);
INSERT INTO choice (id, code, name, description, scale, loner, parent_id, user_id) VALUES (2, '1', 'Red', '', 0, 0, null, 4);
INSERT INTO choice (id, code, name, description, scale, loner, parent_id, user_id) VALUES (3, '2', 'Purple', '', 0, 0, null, 4);
INSERT INTO choice (id, code, name, description, scale, loner, parent_id, user_id) VALUES (4, '3', 'Blue', '', 0, 0, null, 4);
INSERT INTO choice (id, code, name, description, scale, loner, parent_id, user_id) VALUES (5, '4', 'Green', '', 0, 0, null, 4);
INSERT INTO choice (id, code, name, description, scale, loner, parent_id, user_id) VALUES (6, '5', 'Yellow', '', 0, 0, null, 4);
INSERT INTO choice (id, code, name, description, scale, loner, parent_id, user_id) VALUES (7, '6', 'Orange', '', 0, 0, null, 4);
INSERT INTO choice (id, code, name, description, scale, loner, parent_id, user_id) VALUES (8, '99', 'Other', '', 0, 0, null, 4);
INSERT INTO choice (id, code, name, description, scale, loner, parent_id, user_id) VALUES (9, '1', 'Male', '', 0, 0, null, 4);
INSERT INTO choice (id, code, name, description, scale, loner, parent_id, user_id) VALUES (10, '7', 'Black', '', 0, 0, null, 4);
INSERT INTO choice (id, code, name, description, scale, loner, parent_id, user_id) VALUES (11, '8', 'White', '', 0, 0, null, 4);
INSERT INTO choice (id, code , name, description, scale, loner, parent_id, user_id) VALUES (12, null, 'Yes', null, 0, 0, null, 2);
INSERT INTO choice (id, code , name, description, scale, loner, parent_id, user_id) VALUES (13, null, 'Yes, anonymous', null, 0, 0, null, 2);
INSERT INTO choice (id, code , name, description, scale, loner, parent_id, user_id) VALUES (14, null, 'No', null, 0, 0, null, 2);
INSERT INTO choice (id, code , name, description, scale, loner, parent_id, user_id) VALUES (15, null, 'Response time', null, 0, 0, null, 2);
INSERT INTO choice (id, code , name, description, scale, loner, parent_id, user_id) VALUES (16, null, 'Service time', null, 0, 0, null, 2);
INSERT INTO choice (id, code , name, description, scale, loner, parent_id, user_id) VALUES (17, null, 'Service time[**delete**]', null, 0, 0, null, 2);
INSERT INTO choice (id, code , name, description, scale, loner, parent_id, user_id) VALUES (18, null, 'Yes[**edit**]', null, 0, 0, null, 2);

INSERT INTO choice_group (id, name, ordering_strategy, exclude_last, user_id) VALUES (1, 'Gender', 0, 0, 4);
INSERT INTO choice_group (id, name, ordering_strategy, exclude_last, user_id) VALUES (2, 'Colors', 2, 1, 4);
INSERT INTO choice_group (id, name, ordering_strategy, exclude_last, user_id) VALUES (3, 'Consent', 0, 0, 2);
INSERT INTO choice_group (id, name, ordering_strategy, exclude_last, user_id) VALUES (4, 'Rating categories', 0, 0, 2);
INSERT INTO choice_group (id, name, ordering_strategy, exclude_last, user_id) VALUES (5, 'Yes/No[**edit**]', 0, 0, 2);
INSERT INTO choice_group (id, name, ordering_strategy, exclude_last, user_id) VALUES (6, 'Yes/No[**delete**]', 0, 0, 2);
INSERT INTO choice_group (id, name, ordering_strategy, exclude_last, user_id) VALUES (7, 'Yes/No[**remove choice**]', 0, 0, 2);

INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (1, 1, 2.00, '2', '', 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (1, 9, 1.00, '1', '', 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (2, 2, 1.00, '1', '', 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (2, 3, 2.00, '2', '', 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (2, 4, 3.00, '3', '', 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (2, 5, 4.00, '4', '', 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (2, 6, 5.00, '5', '', 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (2, 7, 6.00, '6', '', 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (2, 8, 9.00, '99', '', 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (2, 10, 7.00, '7', '', 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (2, 11, 8.00, '8', '', 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (3, 12, 1.00, '1', null, 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (3, 13, 2.00, '2', null, 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (3, 14, 3.00, '3', null, 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (4, 15, 1.00, '1', null, 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (4, 16, 2.00, '2', null, 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (7, 12, 1.00, '1', null, 0, 0, null);
INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) VALUES (7, 14, 2.00, '2', null, 0, 0, null);

INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (1, 'SCH', 'Single choice', 1.00, 1, 0, 'ke.co.hoji.android.widget.RadioWidget', 'ke.co.hoji.android.widget.DropdownWidget', 'ke.co.hoji.core.response.SingleChoiceResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (3, 'MCH', 'Multiple choices', 3.00, 3, 0, 'ke.co.hoji.android.widget.MultipleChoiceWidget', 'ke.co.hoji.android.widget.PopupMultipleChoiceWidget', 'ke.co.hoji.core.response.MultipleChoiceResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (4, 'DNM', 'Decimal', 5.00, 2, 1, 'ke.co.hoji.android.widget.DecimalWidget', 'ke.co.hoji.android.widget.DecimalWidget', 'ke.co.hoji.core.response.DecimalResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (5, 'WNM', 'Integer', 4.00, 1, 1, 'ke.co.hoji.android.widget.NumberWidget', 'ke.co.hoji.android.widget.NumberWidget', 'ke.co.hoji.core.response.NumberResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (6, 'SCL', 'Sliding scale', 6.00, 1, 1, 'ke.co.hoji.android.widget.ScaleWidget', 'ke.co.hoji.android.widget.ScaleWidget', 'ke.co.hoji.core.response.NumberResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (7, 'ETX', 'Encrypted', 9.00, 3, 2, 'ke.co.hoji.android.widget.TextWidget', 'ke.co.hoji.android.widget.TextWidget', 'ke.co.hoji.core.response.EncryptedTextResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (8, 'MTX', 'Multiple lines', 8.00, 3, 2, 'ke.co.hoji.android.widget.MultiTextWidget', 'ke.co.hoji.android.widget.MultiTextWidget', 'ke.co.hoji.core.response.TextResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (9, 'STX', 'Single line', 7.00, 3, 2, 'ke.co.hoji.android.widget.TextWidget', 'ke.co.hoji.android.widget.TextWidget', 'ke.co.hoji.core.response.TextResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (10, 'DAT', 'Date', 10.00, 1, 3, 'ke.co.hoji.android.widget.DateWidget', 'ke.co.hoji.android.widget.DateWidget', 'ke.co.hoji.core.response.DateResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (11, 'IMG', 'Image', 13.00, 3, 4, 'ke.co.hoji.android.widget.ImageWidget', 'ke.co.hoji.android.widget.ImageWidget', 'ke.co.hoji.core.response.ImageResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (12, 'LBL', 'Label', 12.00, 0, 4, 'ke.co.hoji.android.widget.LabelWidget', 'ke.co.hoji.android.widget.LabelWidget', 'ke.co.hoji.core.response.LabelResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (13, 'TIM', 'Time', 11.00, 1, 3, 'ke.co.hoji.android.widget.TimeWidget', 'ke.co.hoji.android.widget.TimeWidget', 'ke.co.hoji.core.response.TimeResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (14, 'MRX', 'Sub-form', 14.00, 1, 5, 'ke.co.hoji.android.widget.MatrixWidget', 'ke.co.hoji.android.widget.MatrixWidget', 'ke.co.hoji.core.response.MatrixResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (15, 'RCD', 'Form link', 15.00, 3, 5, 'ke.co.hoji.android.widget.RecordWidget', 'ke.co.hoji.android.widget.RecordWidget', 'ke.co.hoji.core.response.RecordResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (16, 'REF', 'Sampling frame', 16.00, 1, 5, 'ke.co.hoji.android.widget.ReferenceWidget', 'ke.co.hoji.android.widget.ReferenceWidget', 'ke.co.hoji.core.response.ReferenceResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (18, 'RNK', 'Ranked choices', 3.60, 3, 0, 'ke.co.hoji.android.widget.RankingWidget', 'ke.co.hoji.android.widget.RankingWidget', 'ke.co.hoji.core.response.RankingResponse');
INSERT INTO field_type (id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class) VALUES (19, 'LOC', 'Location', 13.10, 3, 4, 'ke.co.hoji.android.widget.LocationWidget', 'ke.co.hoji.android.widget.LocationWidget', 'ke.co.hoji.core.response.LocationResponse');

INSERT INTO operator_descriptor (id, name, class) VALUES (1, 'Is equal to', 'ke.co.hoji.core.rule.Equals');
INSERT INTO operator_descriptor (id, name, class) VALUES (2, 'Is not equal to', 'ke.co.hoji.core.rule.NotEquals');
INSERT INTO operator_descriptor (id, name, class) VALUES (3, 'Is greater than', 'ke.co.hoji.core.rule.GreaterThan');
INSERT INTO operator_descriptor (id, name, class) VALUES (4, 'Is less than', 'ke.co.hoji.core.rule.LessThan');
INSERT INTO operator_descriptor (id, name, class) VALUES (5, 'Is greater than or equal to', 'ke.co.hoji.core.rule.GreaterThanOrEquals');
INSERT INTO operator_descriptor (id, name, class) VALUES (6, 'Is less than or equal to', 'ke.co.hoji.core.rule.LessThanOrEquals');
INSERT INTO operator_descriptor (id, name, class) VALUES (7, 'Contains', 'ke.co.hoji.core.rule.Contains');
INSERT INTO operator_descriptor (id, name, class) VALUES (8, 'Includes', 'ke.co.hoji.core.rule.BitwiseAnd');

INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (1, 'LABEL', '', 'INSTRUCTIONS', 1.00, 'Please fill the 5 questions in this form. Your answers do not have to be accurate, but they will be visible to other Hoji users. Swipe left to proceed.', 12, 1, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, '', 2, '', 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (2, '1', 'Name', 'Name', 2.00, 'Enter your name', 9, 1, 1, 0, 1, 0, 0, '3', 1, null, 0, null, null, null, 2, '', 2, '', 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (3, '2', 'Gender', 'Gender', 3.00, 'Are you male or female?', 1, 1, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, '', 2, '', 2, null, 2, 0, 2, 1, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (4, '3', 'Age', 'Age', 4.00, 'Enter your age in years', 5, 1, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, '0', 2, '100', 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (5, '4', 'Favorite Colors', 'Favorite colors', 5.00, 'Tick all that apply', 3, 1, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, '', 2, '', 2, null, 2, 0, 2, 2, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (6, '5', 'Other Colors', 'Other colors', 6.00, 'Specify your <i>other</i> favorite colors', 9, 1, 1, 0, 0, 0, 0, '3', 1, null, 0, null, null, null, 2, '', 2, '', 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (7, 'LABEL', '', 'THE END', 8.00, 'This is the end of the form. Swipe to complete, then check your email for instructions for viewing a real-time analysis of the data.', 12, 1, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, '', 2, '', 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (8, 'LABEL', null, 'Introduction', 1.00, null, 12, 2, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (9, '1', null, 'Consent', 2.00, null, 1, 2, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 3, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (10, '3', null, 'Service rating', 4.00, null, 14, 2, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (11, '1', null, 'Category', 5.00, null, 1, 2, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 4, null, 10, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (12, '2', null, 'Rating', 6.00, null, 6, 2, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, 10, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (13, 'LABEL', null, 'The end', 7.00, null, 12, 2, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (14, '2', null, 'Client name', 3.00, null, 9, 2, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (15, 'LABEL', null, 'Introduction', 1.00, null, 12, 3, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (16, '1', null, 'Consent', 2.00, null, 1, 3, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 3, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (17, '3', null, 'Service rating', 4.00, null, 14, 3, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (18, '1', null, 'Category', 5.00, null, 1, 3, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 4, null, 17, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (19, '2', null, 'Rating', 6.00, null, 6, 3, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, 17, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (20, 'LABEL', null, 'The end', 7.00, null, 12, 3, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (21, '2', null, 'Client name', 3.00, null, 9, 3, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (22, 'LABEL', null, 'Introduction', 1.00, null, 12, 4, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (23, '1', null, 'Consent', 2.00, null, 1, 4, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 3, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (24, '3', null, 'Service rating', 4.00, null, 14, 4, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (25, '1', null, 'Category', 5.00, null, 1, 4, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 4, null, 24, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (26, '2', null, 'Rating', 6.00, null, 6, 4, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, 24, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (27, 'LABEL', null, 'The end', 7.00, null, 12, 4, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (28, '2', null, 'Client name', 3.00, null, 9, 4, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (30, '1', null, 'Consent', 2.00, null, 1, 5, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 3, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (31, '3', null, 'Service rating', 4.00, null, 14, 5, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (32, '1', null, 'Category', 5.00, null, 1, 5, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 4, null, 31, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (33, '2', null, 'Rating', 6.00, null, 6, 5, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, 31, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (34, 'LABEL', null, 'The end', 7.00, null, 12, 5, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (35, '2', null, 'Client name', 3.00, null, 9, 5, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (36, 'LABEL', null, 'Introduction', 1.00, null, 12, 6, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (37, '1', null, 'Consent', 2.00, null, 1, 6, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 3, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (38, '3', null, 'Service rating', 4.00, null, 14, 6, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (39, '1', null, 'Category', 5.00, null, 1, 6, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 4, null, 38, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (40, '2', null, 'Rating', 6.00, null, 6, 6, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, 38, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (41, 'LABEL', null, 'The end', 7.00, null, 12, 6, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (42, '2', null, 'Client name', 3.00, null, 9, 6, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (43, 'LABEL', null, 'Introduction', 1.00, null, 12, 7, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (44, '1', null, 'Consent', 2.00, null, 1, 7, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 3, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (45, '3', null, 'Service rating', 4.00, null, 14, 7, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (46, '1', null, 'Category', 5.00, null, 1, 7, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 4, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (47, '2', null, 'Rating', 6.00, null, 6, 7, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, 45, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (48, 'LABEL', null, 'The end', 7.00, null, 12, 7, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (49, '2', null, 'Client name', 3.00, null, 9, 7, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (50, 'LABEL', null, 'Introduction', 1.00, null, 12, 8, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (51, '1', null, 'Consent', 2.00, null, 1, 8, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 3, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (52, '3', null, 'Service rating', 4.00, null, 14, 8, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (53, '1', null, 'Category', 5.00, null, 1, 8, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 4, null, 52, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (54, '2', null, 'Rating', 6.00, null, 6, 8, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, 52, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (55, 'LABEL', null, 'The end', 7.00, null, 12, 8, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (56, '2', null, 'Client name[**delete**]', 3.00, null, 9, 8, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (57, 'LABEL', null, 'Introduction', 1.00, null, 12, 9, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (58, '1', null, 'Consent', 2.00, null, 1, 9, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 3, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (59, '3', null, 'Service rating', 4.00, null, 14, 9, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (60, '1', null, 'Category', 5.00, null, 1, 9, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 4, null, 59, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (61, '2', null, 'Rating', 6.00, null, 6, 9, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, 59, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (62, 'LABEL', null, 'The end', 7.00, null, 12, 9, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (63, '2', null, 'Client name', 3.00, null, 9, 9, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (64, 'LABEL', null, 'Introduction', 1.00, null, 12, 10, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (65, '1', null, 'Consent', 2.00, null, 1, 10, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 3, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (66, '3', null, 'Service rating', 4.00, null, 14, 10, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (67, '1', null, 'Category', 5.00, null, 1, 10, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 4, null, 66, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (68, '2', null, 'Rating', 6.00, null, 6, 10, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, 66, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (69, 'LABEL', null, 'The end', 7.00, null, 12, 10, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (70, '2', null, 'Client name', 3.00, null, 9, 10, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (71, 'LABEL', null, 'Introduction', 1.00, null, 12, 11, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (72, '1', null, 'Consent', 2.00, null, 1, 11, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 3, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (73, '3', null, 'Service rating', 4.00, null, 14, 11, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (74, '1', null, 'Category', 5.00, null, 1, 11, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 4, null, 73, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (75, '2', null, 'Rating', 6.00, null, 6, 11, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, 73, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (76, 'LABEL', null, 'The end', 7.00, null, 12, 11, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (77, '2', null, 'Client name', 3.00, null, 9, 11, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (78, 'LABEL', null, 'Introduction', 1.00, null, 12, 12, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (79, '1', null, 'Consent', 2.00, null, 1, 12, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 3, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (80, '3', null, 'Service rating', 4.00, null, 14, 12, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (81, '1', null, 'Category', 5.00, null, 1, 12, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 4, null, 80, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (82, '2', null, 'Rating', 6.00, null, 6, 12, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, 80, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (83, 'LABEL', null, 'The end', 7.00, null, 12, 12, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (84, '2', null, 'Client name', 3.00, null, 9, 12, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (85, 'LABEL', null, 'Introduction', 1.00, null, 12, 13, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (86, '1', null, 'Consent', 2.00, null, 1, 13, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 3, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (87, '3', null, 'Service rating', 4.00, null, 14, 13, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (88, '1', null, 'Category', 5.00, null, 1, 13, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, 4, null, 87, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (89, '2', null, 'Rating', 6.00, null, 6, 13, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, 87, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (90, 'LABEL', null, 'The end', 7.00, null, 12, 13, 1, 0, 0, 0, 0, null, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);
INSERT INTO field (id, name, variable_name, description, ordinal, instructions, field_type_id, form_id, enabled, captioning, searchable, filterable, output_type, tag, calculated, value_script, enable_if_null, validation_script, default_value, missing_value, missing_action, min_value, min_action, max_value, max_action, regex_value, regex_action, uniqueness, unique_action, choice_group_id, choice_filter_field_id, parent_id, pipe_source_id, reference_field_id, main_record_field_id, matrix_record_field_id, response_inheriting) VALUES (91, '2', null, 'Client name', 3.00, null, 9, 13, 1, 0, 0, 0, 0, 3, 1, null, 0, null, null, null, 2, null, 2, null, 2, null, 2, 0, 2, null, null, null, null, null, null, null, 0);

INSERT INTO rule (id, type, ordinal, owner_id, operator_descriptor_id, value, target_id, combiner, grouper) VALUES (1, 1, 0.00, 65, 1, '14', 69, 0, 0);
INSERT INTO rule (id, type, ordinal, owner_id, operator_descriptor_id, value, target_id, combiner, grouper) VALUES (2, 2, 0.00, 70, 1, '13', 65, 1, 0);
INSERT INTO rule (id, type, ordinal, owner_id, operator_descriptor_id, value, target_id, combiner, grouper) VALUES (3, 1, 0.00, 72, 1, '14', 76, 0, 0);
INSERT INTO rule (id, type, ordinal, owner_id, operator_descriptor_id, value, target_id, combiner, grouper) VALUES (4, 2, 0.00, 77, 1, '13', 72, 1, 0);
INSERT INTO rule (id, type, ordinal, owner_id, operator_descriptor_id, value, target_id, combiner, grouper) VALUES (5, 1, 0.00, 79, 1, '14', 83, 0, 0);
INSERT INTO rule (id, type, ordinal, owner_id, operator_descriptor_id, value, target_id, combiner, grouper) VALUES (6, 2, 0.00, 84, 1, '13', 79, 1, 0);
INSERT INTO rule (id, type, ordinal, owner_id, operator_descriptor_id, value, target_id, combiner, grouper) VALUES (7, 1, 0.00, 86, 1, '14', 90, 0, 0);
INSERT INTO rule (id, type, ordinal, owner_id, operator_descriptor_id, value, target_id, combiner, grouper) VALUES (8, 2, 0.00, 91, 1, '13', 86, 1, 0);

