INSERT INTO user (id, username, first_name, last_name, password, enabled, date_created, code) VALUES (1,'test@hoji.co.ke', 'Hoji', 'Test', '1234',1, '2017-05-26 22:43:29', 'COD1');

INSERT INTO survey (id, code, name, user_id) VALUES (1, 'Create', 'Test creating one property', 1);
INSERT INTO survey (id, code, name, user_id) VALUES (2, 'Create2', 'Test creating multiple properties', 1);
INSERT INTO survey (id, code, name, user_id) VALUES (3, 'Read', 'Test read', 1);
INSERT INTO survey (id, code, name, user_id) VALUES (4, 'Read2', 'Test read multiple', 1);
INSERT INTO survey (id, code, name, user_id) VALUES (5, 'Update', 'Test updating one property', 1);
INSERT INTO survey (id, code, name, user_id) VALUES (6, 'Update2', 'Test updating multiple properties', 1);
INSERT INTO survey (id, code, name, user_id) VALUES (7, 'Delete', 'Test delete', 1);

INSERT INTO property (property_key, value, survey_id) VALUES ('field.256', '500', 1);
INSERT INTO property (property_key, value, survey_id) VALUES ('field.256', '250', 3);
INSERT INTO property (property_key, value, survey_id) VALUES ('field.256', '250', 4);
INSERT INTO property (property_key, value, survey_id) VALUES ('field.257', '300', 4);
INSERT INTO property (property_key, value, survey_id) VALUES ('field.256', '300', 5);
INSERT INTO property (property_key, value, survey_id) VALUES ('field.256', '250', 6);
INSERT INTO property (property_key, value, survey_id) VALUES ('field.256', '250', 7);