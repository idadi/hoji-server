create table user
(
	id int identity
		primary key,
	username varchar(255) not null,
	public_survey_access int default '1' not null,
	tenant_user_id int null,
	password char(60) not null,
	first_name varchar(45) not null,
	middle_name varchar(45) null,
	last_name varchar(45) not null,
	enabled boolean not null,
	verified boolean default 0 not null,
	suspended boolean default 0 not null,
	telephone varchar(45) null,
	postal_address varchar(45) null,
	physical_address varchar(45) null,
	date_created datetime not null,
	default_roles varchar(255) null,
	type int default 1 not null,
	min_credits int default 500 not null,
	tax_rate decimal(15,2) default 16.00 not null,
	discount_rate decimal(15,2) default 0.00 not null,
	master boolean default 0 not null,
	slave_status int default 0 not null,
	master_user_id int null,
	code char(5) not null,
	constraint code_UNIQUE
		unique (code),
	constraint fk_user_tenant
		foreign key (tenant_user_id) references user (id)
			on update cascade on delete cascade
)
;

create index fk_user_tenant_idx
	on user (tenant_user_id)
;

create table role
(
	user_id int not null,
	role varchar(45) not null,
	primary key (user_id, role),
	constraint fk_role_user
		foreign key (user_id) references user (id)
			on update cascade on delete cascade
)
;