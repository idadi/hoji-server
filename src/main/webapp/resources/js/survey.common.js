function editProject(survey, onSaved) {
    var openModal = function () {
        $('.ui.modals').remove();
        var editSurveyTemplate = _.template($('script.survey-edit-template').html());
        $('#modal-container').empty().append(editSurveyTemplate({survey: survey}));
        $('#modal-container select').dropdown();
        $('.ui.modal').modal({closable: false}).modal('show');
        $('.ui.modal').on('click', '.edit-submit', () => {
            var editSurvey = function () {
                var onSuccess = function (postResult) {
                    onSaved(postResult.value);
                }
                editObject(formToJsonString($("#edit-form")), contextPath + '/project/edit', onSuccess);
            }
            handleAuthenticatedRequest(editSurvey);
        });
    };
    handleAuthenticatedRequest(openModal);
}

function deleteSurvey(survey, postDelete) {
    var openModal = function () {
        $('.ui.modals').remove();
        var objectType = ' project';
        var deleteMessage = "<strong>WARNING: </strong>This action CANNOT be undone. It will permanently delete " +
            "<strong>" + survey.name + "</strong> and all associated records including forms, fields and data. " +
            "If you are ABSOLUTELY SURE this is what you want, please enter the name of the project to" +
            " confirm.";
        var deleteHelpText = "Enter the name of this project to delete it.";
        var deleteHelpLink = "https://www.hoji.co.ke/support/#reamaze#0#/kb/digitization/how-to-delete-a-project";
        var deleteHelpLinkText = "More about deleting a project.";
        openDeleteWithTemplateModal(
            {
                'configObject': survey,
                'label': 'Project name',
                'objectType': objectType,
                'deleteMessage': deleteMessage,
                'deleteHelpText': deleteHelpText,
                'deleteHelpLink': deleteHelpLink,
                'deleteHelpLinkText': deleteHelpLinkText
            },
            $('script.resource-delete-template'));
        $('.ui.modal')
            .on('click', '.delete-submit', () => {
                var deleteSurvey = function () {
                    deleteObject(postDelete, contextPath + '/project/delete');
                };
                handleAuthenticatedRequest(deleteSurvey);
            });
    };
    handleAuthenticatedRequest(openModal);
}