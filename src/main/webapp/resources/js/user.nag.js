function refreshView() {

    var userNagTemplate = _.template($('script.user-nag-template').html());
    $('#user-nag-view').empty().append(userNagTemplate(user));

    $('.ui.dropdown').dropdown({action: 'hide'});

    $('#email-change-link')
        .off('click')
        .on('click', function () {
            changeEmail();
        });
    $('#email-verification-link')
        .off('click')
        .on('click', function () {
            verifyEmail();
        });
}