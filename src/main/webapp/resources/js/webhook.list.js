
function refreshView() {
    var webhookListTemplate = _.template($('script.webhook-list-template').html());
    $('#webhook-list-container').empty().append(webhookListTemplate({ 'webhooks': webhooks }));

    $('.message .close')
        .on('click', function () {
            $(this).closest('.message').transition('fade');
        });

    $('.ui.dropdown').dropdown({ action: 'hide' });

    $('[data-tooltip]').popup();

    $('.add-webhook, .edit-webhook')
        .off('click')
        .on('click', function () {
            var webhookId = $(this).closest('tr').data('webhookId');
            var webhook = null;
            if (webhookId) {
                webhook = webhooks.find(function(w) { return w.id === webhookId; });
            }
            var openModal = function () {
                $('.ui.modals').remove();
                var webhookEditTemplate = _.template($('script.webhook-edit-template').html());
                $('#modal-container').empty().append(webhookEditTemplate({ 'webhook': webhook }));
                var values = forms.map(function (form) {
                    var value = { 'name': form.name, 'value': form.id };
                    if (webhook && webhook.forms.findIndex(function (f) { return f.id === form.id; }) > -1) {
                        value.selected = true;
                    }
                    return value;
                });
                $(".ui.dropdown.forms").dropdown({ 'values': values });
                $('.ui.modal').modal({ closable: false }).modal('show');
                $('.ui.modal').on('click', '.edit-submit', function () {
                    var editWebhook = function () {
                        var onSuccess = function (postResult) {
                            var saved = postResult.value;
                            var position = webhooks.findIndex(function (w) { return w.id === saved.id; });
                            if (position > -1) {
                                webhooks.splice(position, 1, saved);
                            } else {
                                saved.added = true;
                                webhooks.push(saved);
                            }
                            refreshView();
                        }
                        editObject(formToJsonString($("#edit-form"),["forms", "eventTypes"]), contextPath + '/hook/edit', onSuccess);
                    }
                    handleAuthenticatedRequest(editWebhook);
                });
            };
            handleAuthenticatedRequest(openModal);
        });

    $('.delete-webhook')
        .off('click')
        .on('click', function () {
            var webhookId = $(this).closest('tr').data('webhookId');
            var webhook = webhooks.find(function (w) { return w.id === webhookId; });
            var openModal = function () {
                var deleteMessage = "Are you sure you want to delete the hook to: <strong>" + webhook.targetUrl + "</strong>?";
                openDeleteWithTemplateModal(
                    {
                        'configObject': webhook,
                        'label': null,
                        'objectType': 'webhook',
                        'deleteMessage': deleteMessage
                    },
                    $('script.resource-delete-template'));
                $('.ui.modal')
                    .on('click', '.delete-submit', function () {
                        var deleteWebhook = function () {
                            var onSuccess = function () {
                                if (webhooks.length > 1) {
                                    webhook.deleted = true;
                                } else {
                                    webhooks = [];
                                }
                                refreshView();
                            }
                            deleteObject(onSuccess, contextPath + '/hook/delete?id=' + webhook.id);
                        }
                        handleAuthenticatedRequest(deleteWebhook);
                    });
            };
            handleAuthenticatedRequest(openModal);
        });
}