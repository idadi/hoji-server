var infoMessage = null;
var errors = null;

function refreshView() {
    var formViewTemplate = _.template($('script.form-view-template').html());
    $('#form-view-container').empty().append(formViewTemplate(form));

    $('.ui.dropdown').dropdown({action: 'hide'});

    $('.form-add, .form-edit')
        .off('click')
        .on('click', function () {
            editForm(form, form.survey.id, function (saved) {
                form = saved;
                refreshView();
            });
        });

    $('.form-delete')
        .off('click')
        .on('click', function () {
            deleteForm(form, contextPath + '/project/view/' + form.survey.id + '/form');
        });

    $('.form-enable')
        .off('click')
        .on('click', function () {
            var onSuccess = function (toggledForm) {
                form = toggledForm;
                refreshView();
            };
            $('.more-actions').addClass('loading');
            toggleEnabled(form, "form", onSuccess);
        });

    $('.form-manage-visualizations')
        .off('click')
        .on('click', function () {
            window.location.href = contextPath + '/form/view/' + form.id + '/visualization'
        });

    $('.form-manage-fields')
        .off('click')
        .on('click', function () {
            window.location.href = contextPath + '/form/view/' + form.id + '/field'
        });

    $('.field-add, .field-edit')
        .off('click')
        .on('click', function () {
            event.preventDefault();
            var fieldId = $(this).closest('tr').data('fieldId');
            var field = fields.find(f => f.id == fieldId);
            editField(
                field,
                form.id,
                function (saved) {
                    saved.added = true;
                    var position = fields.findIndex(f => f.id === saved.id);
                    if (position === -1) {
                        fields.push(saved);
                    } else {
                        fields.splice(position, 1, saved)
                    }
                    refreshView();
                });
        });

    $('.field-delete')
        .off('click')
        .on('click', function (event) {
            event.preventDefault();
            var fieldId = $(this).closest('tr').data('fieldId');
            var field = fields.find(f => f.id == fieldId);
            deleteField(field, function () {
                if (fields.length === 1) {
                    fields = [];
                    refreshView();
                    return;
                }
                var deleted = fields.find(f => f.id == field.id);
                if (deleted) {
                    deleted.deleted = true;
                }
                refreshView();
            });
        });

    $('.rule-add')
        .off('click')
        .on('click', function (e) {
            var fieldId = $(this).closest('tr').data('fieldId');
            var field = fields.find(f => f.id == fieldId);
            addSkipLogic(
                field,
                null,
                function () {
                    infoMessage = 'Added skip logic to the field: <em>' + field.description + '</em>';
                    field.added = true;
                    refreshView();
                })
        });

    $('.script-attach')
        .off('click')
        .on('click', function (e) {
            var fieldId = $(this).closest('tr').data('fieldId');
            var field = fields.find(f => f.id == fieldId);
            attachScript(
                field,
                form.id,
                function () {
                    infoMessage = 'Attached a script to the field: <em>' + field.description + '</em>';
                    field.added = true;
                    refreshView();
                })
        });

    $('.field.table tr.added')
        .transition('glow', function () {
            fields = fields.map(f => {
                delete f["added"];
                return f;
            });
        });

    $('.field.table tr.deleted')
        .transition('glow', '500ms')
        .transition('fade', 500, function () {
            fields = fields.filter(f => !f.deleted);
        });


    if ($('.field.table tbody').hasClass('ui-sortable')) {
        $('.field.table tbody').sortable('destroy');
    }

    $('.field.table tbody')
        .sortable({
            handle: '.sort-handle',
            stop: function () {
                var sortable = $(this);
                var userSort = function () {
                    var sortedFieldIds = [];
                    $('tr', sortable).each(function (index, tr) {
                        var fieldId = $(tr).data('fieldId');
                        if (fieldId) {
                            sortedFieldIds.push(fieldId);
                        }
                    });

                    var sortedFields = [];
                    sortedFieldIds.forEach(function (id, index) {
                        var found = fields.find(function (field) {
                            return field.id == id;
                        });
                        found.ordinal = index + 1;
                        sortedFields.push(found);
                    });

                    var children = sortedFields
                        .filter(function (sortedField) {
                            return sortedField.parent;
                        })
                        .reduce(function (children, child) {
                            var parentId = child.parent.id;
                            var container = children.find(function (container) {
                                return container.parentId == parentId;
                            });
                            if (!container) {
                                container = {parentId: parentId, children: []};
                            }
                            container.children.push(child);
                            children.push(container);
                            return children;
                        }, []);

                    children.forEach(function (container) {
                        container.children.forEach(function (child) {
                            var childIndex = sortedFields.indexOf(child);
                            if (childIndex > -1) {
                                sortedFields.splice(childIndex, 1);
                            }
                        });
                        var parentIndex = -1;
                        sortedFields.find(function (sortedField, index) {
                            if (sortedField.id == container.parentId) {
                                parentIndex = index;
                                return true;
                            } else {
                                return false;
                            }
                        });
                        if (parentIndex > -1) {
                            var args = [parentIndex + 1, 0].concat(container.children);
                            Array.prototype.splice.apply(sortedFields, args);
                        }
                    });

                    var isValidSort = sortedFields.every(function (field, index) {
                        return validateSort(field, index, sortedFields);
                    });
                    if (isValidSort) {
                        fields = sortedFields;
                    }
                    return isValidSort;
                }

                var validateSort = function (field, currentFieldIndex, sortedFields) {
                    var isValid = true;
                    if (field.parent) {
                        var indexOfParent = 0;
                        var parent = sortedFields.find(function (sortedField, index) {
                            if (sortedField.id == field.parent.id) {
                                indexOfParent = index;
                                return true;
                            }
                            return false;
                        });
                        if (parent) {
                            if (indexOfParent < currentFieldIndex) {
                                var firstChildIndex = indexOfParent + 1;
                                var foundInvalidChildField = false;
                                for (var i = firstChildIndex; i <= currentFieldIndex; i++) {
                                    if (!sortedFields[i].parent || sortedFields[i].parent.id !== field.parent.id) {
                                        foundInvalidChildField = true;
                                        break;
                                    }
                                }
                                if (foundInvalidChildField) {
                                    isValid = false;
                                }
                            } else {
                                isValid = false;
                            }
                        }
                    }
                    return isValid;
                }
                var onFail = function () {
                    forms = originalForms;
                    refreshView();
                };
                var originalFieldList = $.extend(true, [], fields);
                var valid = userSort();
                var sorted = !originalFieldList.every(function (field, index) {
                    return field.id === fields[index].id;
                });
                if (valid && sorted) {
                    refreshView();
                    var sortedFieldIds = fields.map(function (field) {
                        return field.id;
                    });
                    var url = contextPath + '/field/sort';
                    var data = {formId: form.id, sortedFieldIds: sortedFieldIds}
                    submitSortOrder(url, data, onFail)
                } else {
                    onFail()
                }
            }
        });
    var selectedFields = [];

    $('input.check')
        .off('click')
        .on('click', function () {
            var fieldId = $(this).closest('tr').data('fieldId');
            if ($(this).is(':checked')) {
                selectedFields.push(fieldId);
            } else {
                var pos = selectedFields.indexOf(fieldId);
                selectedFields.splice(pos, 1);
            }
        });

    $('.bulk-edit')
        .off('click')
        .on('click', function () {
            if (selectedFields.length === 0) {
                errors = {title: 'Please select one or more fields to edit'};
                refreshView();
                return;
            }
            var openModal = function () {
                $('.ui.modals').remove();
                var fieldBulkEditTemplate = _.template($('script.field-bulk-edit-template').html());
                $('#modal-container').empty().append(fieldBulkEditTemplate({'fieldIds': selectedFields}));
                $('.ui.modal').modal({autofocus: false, closable: false}).modal('show');
                $('.ui.dropdown', '.ui.modal').dropdown();
                initializeBulkEditDropdowns();
                $('.ui.modal').on('click', '.edit-submit', () => {
                    var addField = function () {
                        var onSuccess = function (postResult) {
                            postResult.value.forEach(field => {
                                var pos = fields.findIndex(elem => field.id === elem.id);
                                fields.splice(pos, 1, field);
                            });
                            refreshView();
                        }
                        var url = contextPath + '/field/' + form.id + '/bulk-save';
                        editObject(formToJsonString($("#edit-form"), ['ids', 'recordFormIds']), url, onSuccess);
                    }
                    handleAuthenticatedRequest(addField);
                });
            };
            handleAuthenticatedRequest(openModal);
        });

    $('.use-as-caption')
        .off('click')
        .on('click', function () {
            if (selectedFields.length === 0) {
                errors = {title: 'Please select one or more fields to use as caption'};
                refreshView();
                return;
            }
            var url = contextPath + "/field/" + form.id + "/bulk-save?attribute=captionable&" + toQueryString({fieldIds: selectedFields});
            var onSuccess = function (savedFields) {
                savedFields.forEach(field => {
                    var pos = fields.findIndex(elem => field.id === elem.id);
                    fields.splice(pos, 1, field);
                });
                var name = savedFields.length == 1 ? 'field' : 'fields';
                infoMessage = savedFields.length + ' ' + name + ' marked to be used as caption.';
                refreshView();
            };
            Utils.fetch({method: 'post', url: url, onSuccess: onSuccess})
        });

    $('.mark-as-searchable')
        .off('click')
        .on('click', function () {
            if (selectedFields.length === 0) {
                errors = {title: 'Please select one or more fields to mark as searchable'};
                refreshView();
                return;
            }
            var url = contextPath + "/field/" + form.id + "/bulk-save?attribute=searchable&" + toQueryString({fieldIds: selectedFields});
            var onSuccess = function (savedFields) {
                savedFields.forEach(field => {
                    var pos = fields.findIndex(elem => field.id === elem.id);
                    fields.splice(pos, 1, field);
                });
                var name = savedFields.length == 1 ? 'field' : 'fields';
                infoMessage = savedFields.length + ' ' + name + ' marked as searchable.';
                refreshView();
            };
            Utils.fetch({method: 'post', url: url, onSuccess: onSuccess})
        });

    $('.mark-as-inherits-response')
        .off('click')
        .on('click', function () {
            if (selectedFields.length === 0) {
                errors = {title: 'Please select one or more fields to mark as response inheriting'};
                refreshView();
                return;
            }
            var url = contextPath + "/field/" + form.id + "/bulk-save?attribute=responseInheriting&" + toQueryString({fieldIds: selectedFields});
            var onSuccess = function (savedFields) {
                savedFields.forEach(field => {
                    var pos = fields.findIndex(elem => field.id === elem.id);
                    fields.splice(pos, 1, field);
                });
                var name = savedFields.length == 1 ? 'field' : 'fields';
                infoMessage = savedFields.length + ' ' + name + ' marked as response inheriting.';
                refreshView();
            };
            Utils.fetch({method: 'post', url: url, onSuccess: onSuccess})
        });

    var initializeBulkEditDropdowns = function (settings) {
        (function () {
            // initialize input format options
            var onComplete = function (formatOptions) {
                var values = formatOptions.map(function (option, index) {
                    return {'name': option.label, 'value': String(option.value)}
                });
                values.unshift({'name': '< Keep existing >', 'value': '-999', 'selected': true});
                $(".ui.dropdown.input-format").dropdown({'values': values, 'fullTextSearch': true});
            }
            Utils.fetch({url: contextPath + "/field/input-format", onSuccess: onComplete});
        })();
        (function () {
            // initialize missing action options
            var onComplete = function (actionOptions) {
                var values = actionOptions.map(function (action) {
                    return {'name': action.label, 'value': String(action.value)};
                });
                values.unshift({'name': '< Keep existing >', 'value': '-999', 'selected': true});
                $(".ui.dropdown.missing-action").dropdown({'values': values});
            }
            Utils.fetch({url: contextPath + "/field/field-action", onSuccess: onComplete});
        })();
        (function () {
            // initialize parent options
            var onComplete = function (parents) {
                var values = parents.map(function (parent) {
                    return {'name': parent.description, 'value': String(parent.id)}
                });
                values.unshift({'name': '< Keep existing >', 'value': '-999', 'selected': true});
                $(".ui.dropdown.parent").dropdown({'values': values, 'fullTextSearch': true});
            }
            Utils.fetch({url: contextPath + "/field/parent?formId=" + form.id, onSuccess: onComplete});
        })();
    };

    (function () {
        Utils.renderMessage({info: infoMessage, error: errors});
        infoMessage = null;
        errors = null;
    })();

    $('#take-a-tour')
        .off('click')
        .on('click', function () {
            var tourSteps = getTourSteps();
            introJs()
                .setOptions({
                    'skipLabel': 'Exit',
                    'hidePrev': true,
                    'hideNext': true,
                    'exitOnEsc': false,
                    'exitOnOverlayClick': false,
                    'disableInteraction': true,
                    'steps': tourSteps
                })
                .start();
        });

    function getTourSteps() {
        var tourSteps = [];
        tourSteps[0] = {
            element: '.breadcrumb',
            intro: 'This menu shows you where you are on the application.'
        };
        tourSteps[1] = {
            element: '#form-name',
            intro: 'This is the form that you currently have open.'
        };
        tourSteps[2] = {
            element: '.object-attributes',
            intro: 'The attributes of the current form are shown here.'
        };
        tourSteps[3] = {
            element: '#form-report-button',
            intro: 'Use this button to view data reports for this form.'
        }
        tourSteps[4] = {
            element: '.more-actions',
            intro: 'additional actions for the current form.'
        };
        if (fields.length == 0) {
            tourSteps[5] = {
                element: '.add-field',
                intro: 'Click here to add a field to the current form.'
            };
            tourSteps[6] = {
                element: '.field-link',
                intro: 'Fields that you add to this form will be listed here.'
            };
        } else {
            tourSteps[7] = {
                element: '#fields-title',
                intro: 'The fields under this form are listed here.'
            };
            tourSteps[8] = {
                element: '.field-add',
                intro: 'Click here to add a field to the current form.'
            };
            tourSteps[9] = {
                element: '.field-link',
                intro: 'Click on an existing field to open it and add skip logic.'
            };
            tourSteps[10] = {
                element: '.field-link',
                intro: 'If the field already has skip logic, click here to see it.',
                position: 'right'
            };
            tourSteps[11] = {
                element: '.field-actions',
                intro: 'Use this button to access more field actions.'
            };
            tourSteps[12] = {
                element: '.bulk-actions',
                intro: 'Use this button to edit multiple fields at once.'
            };
        }
        tourSteps[13] = {
            intro: '<strong>IMPORTANT:</strong> You must always deploy a project in order to collect data on it.'
        };
        return tourSteps.filter(function (el) {
            return el != null;
        });
    }
}
