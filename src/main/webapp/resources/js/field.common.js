function editField(field, formId, onSaved) {
    var openModal = function () {
        $('.ui.modals').remove();
        setupModal({'field': field, 'formId': formId});
        $('.content>form>.menu>.item').tab();
        $('.ui.modal').on('click', '.edit-submit', () => {
            var addField = function () {
                var onSuccess = function (postResult) {
                    onSaved(postResult.value.field);
                }
                var onError = function (postErrors) {
                    $('.ui.empty.circular.label').remove();
                    var tabsWithErrors = postErrors
                        .map(function (postError) {
                            return $('#' + postError.parameter + '-error').closest('.tab').data('tab');
                        })
                        .reduce(function (acc, tabName) {
                            if (!acc.includes(tabName)) {
                                acc.push(tabName);
                            }
                            return acc;
                        }, []);
                    tabsWithErrors.forEach(function (tabName) {
                        $('.item[data-tab="' + tabName + '"]').append('<a class="ui red empty circular label"></a>');
                    })
                    if (tabsWithErrors.length) {
                        $('.content>form>.menu>.item').tab('change tab', tabsWithErrors[0]);
                    }
                }
                var url = contextPath + '/field/' + formId + '/save';
                editObject(formToJsonString($("#edit-form"), ["recordFormIds"]), url, onSuccess, onError);
            }
            handleAuthenticatedRequest(addField);
        });
    };
    handleAuthenticatedRequest(openModal);
}

function deleteField(field, postDelete) {
    var openModal = function () {
        var objectType = ' field';
        var deleteMessage = '<strong>WARNING: </strong>This action CANNOT be undone. It will permanently delete' +
            ' Question No <strong>' + field.name + '</strong> and all its data. If you are ABSOLUTELY SURE this is' +
            ' what you want, please enter the Field Number to confirm.';
        var deleteHelpText = "Enter the field number to delete this field.";
        var deleteHelpLink = "https://www.hoji.co.ke/support/#reamaze#0#/kb/digitization/how-to-delete-a-field";
        var deleteHelpLinkText = "More about deleting a field.";
        openDeleteWithTemplateModal(
            {
                'configObject': field,
                'label': 'Field number',
                'objectType': objectType,
                'deleteMessage': deleteMessage,
                'deleteHelpText': deleteHelpText,
                'deleteHelpLink': deleteHelpLink,
                'deleteHelpLinkText': deleteHelpLinkText
            },
            $('script.resource-delete-template'));
        $('.ui.modal')
            .on('click', '.delete-submit', () => {
                var deleteField = function () {
                    deleteObject(postDelete, contextPath + '/field/delete');
                }
                handleAuthenticatedRequest(deleteField);
            });
    };
    handleAuthenticatedRequest(openModal);
}

function addSkipLogic(field, rule, onSave) {
    event.preventDefault();
    var openModal = function () {
        var initializeValueInput = function (resource, $context) {
            $('.ui.dropdown.operator')
                .off('change')
                .on('change', function (event) {
                    initializeValueInput(resource, $context);
                });
            var ruleValueTemplate = _.template($('script.rule-value-template').html());
            var isChoice = resource && resource.choiceGroup;
            var isMultiChoice = resource && resource.type.multiChoice;
            var operator = $('input[name="operatorDescriptor"]', $context).val();
            var value = rule ? rule.value : null;
            $('.value-container', $context).empty().append(ruleValueTemplate({
                isChoice: isChoice,
                isMultiChoice: isMultiChoice,
                operator: operator,
                value: value
            }));
            if (isChoice) {
                var values = resource.choiceGroup.choices.map(c => {
                    var option = {'name': c.name, 'value': String(c.id)};
                    if (rule && typeof rule.value !== "string" && rule.value.find(v => v.id === c.id)) {
                        option.selected = true;
                    }
                    return option;
                });
                if (operator != 7) {
                    values.unshift({
                        'name': 'Empty',
                        'value': '{empty}',
                        'selected': rule && rule.value === '{empty}'
                    });
                } else {
                    $('.ui.dropdown.rule-value').addClass('multiple');
                }
                $('.ui.dropdown.rule-value').dropdown({'values': values, 'fullTextSearch': true});
            }
        };
        var onSuccess = function (operators) {
            // initialize operator options
            var values = operators.map(function (operator) {
                var option = {'name': operator.name, 'value': String(operator.id)};
                if (rule && rule.operatorDescriptor.id == operator.id) {
                    option.selected = true;
                }
                return option;
            });
            if (!rule) {
                // select first operator
                values[0].selected = true;
            }
            $(".ui.dropdown.operator").dropdown({'values': values, 'fullTextSearch': true});
        };
        Utils.fetch({url: contextPath + '/rule/' + field.id + '/operator', onSuccess: onSuccess});
        $('.ui.modals').remove();
        var ruleEditTemplate = _.template($('script.rule-edit-template').html());
        $('#modal-container').empty().append(ruleEditTemplate({'rule': rule, owner: field}));
        $('.ui.modal').modal({autofocus: false, closable: false}).modal('show');
        $('.ui.dropdown', '.ui.modal').dropdown();
        $('.ui.dropdown.type').dropdown({
            onChange: function (value) {
                var potentialTargets;
                var $context;
                var onSuccess = function (targets, settingsExtras) {
                    // initialize target options
                    potentialTargets = targets;
                    var values = targets.map(function (target) {
                        var option = {'name': target.description, 'value': String(target.id)};
                        if (rule && rule.target.id == target.id) {
                            option.selected = true;
                        }
                        return option;
                    });
                    var settings = $.extend({'values': values, 'fullTextSearch': true}, settingsExtras);
                    $(".ui.dropdown.target", $context).dropdown(settings);
                };
                if (value == 1) {
                    $context = $('.if-show');
                    $('.show-if').transition('hide');
                    $('.show-if :input').prop('disabled', true);
                    $('.if-show :input').removeAttr('disabled');
                    $('.if-show').transition('show');
                    Utils.fetch({
                        url: contextPath + '/rule/' + field.id + '/target?type=1',
                        onSuccess: onSuccess
                    });
                    initializeValueInput(field, $context);
                }
                if (value == 2) {
                    $context = $('.show-if');
                    $('.if-show').transition('hide');
                    $('.if-show :input').prop('disabled', true);
                    $('.show-if :input').removeAttr('disabled');
                    $('.show-if').transition('show');
                    var _onSuccess = function (targets) {
                        onSuccess(targets, {
                            onChange: function (value) {
                                var target = potentialTargets.find(function (t) {
                                    return t.id == value
                                });
                                initializeValueInput(target, $context);
                            }
                        });
                        if (!rule && targets.length) {
                            // select last target when creating an new show-if rule
                            var lastIndex = targets.length - 1;
                            $('.ui.dropdown.target').dropdown('set selected', targets[lastIndex].id);
                        }
                    }
                    Utils.fetch({
                        url: contextPath + '/rule/' + field.id + '/target?type=2',
                        onSuccess: _onSuccess
                    });
                    initializeValueInput(rule ? rule.target : null, $context);
                }
            }
        });
        $('.ui.dropdown.type').dropdown('set selected', rule ? rule.type : 2);
        $('.ui.modal').on('click', '.edit-submit', function () {
            var saveRule = function () {
                var onSuccess = function (postResult) {
                    var savedRule = postResult.value;
                    onSave(savedRule);
                }
                var url = contextPath + '/rule/' + field.id + '/save';
                editObject(formToJsonString($('#edit-form')), url, onSuccess);
            }
            handleAuthenticatedRequest(saveRule);
        });
    };
    handleAuthenticatedRequest(openModal);
}

function attachScript(field, formId, onSaved) {
    var openModal = function (hojiAutoCompleteList) {
        $('.ui.modals').remove();
        var fieldScriptTemplate = _.template($('script.field-script-template').html());
        $('#modal-container').empty().append(fieldScriptTemplate({'field': field}));
        $('.ui.modal').modal({closable: false}).modal('show');
        var codeTextArea = $('#valueScript')[0];
        codeTextArea.value = field.valueScript;
        if (!codeTextArea.value) {
            codeTextArea.value = "/*\n" +
                "  You can write JavaScript functions here for calculated fields, custom validation \n" +
                "  logic, custom error messages and so on. For more details, click on \"Learn more\" \n" +
                "  at the bottom of this window.\n" +
                "*/\n";
        }
        var codeEditor = CodeMirror.fromTextArea(codeTextArea, {
            lineNumbers: true,
            mode: "javascript",
            theme: "base16-light",
            matchBrackets: true,
            autoCloseBrackets: true,
            extraKeys: {
                "Ctrl-Space": "autocomplete"
            },
            lineWrapping: true,
            indentUnit: 4,
            indentWithTabs: true,
            styleActiveLine: true,
            continueComments: true,
            tabMode: "shift"
        });

        {
            /*
             * This code is responsible for adding Hoji objects and methods to the list of items available for
             * auto-completion in the code editor.
             *
             * It is adapted from this Stack Overflow answer:
             *
             * https://stackoverflow.com/questions/19244449/codemirror-autocomplete-custom-list/19269913
             */

            var javaScriptHint = CodeMirror.hint.javascript;
            var anyWordHint = CodeMirror.hint.anyword;

            CodeMirror.hint.javascript = function (editor) {
                const javaScriptAutoCompleteList = javaScriptHint(editor).list;
                const anyWordAutoCompleteList = anyWordHint(editor).list;
                var combinedAutoCompleteList = [
                    ...new Set([
                        ...javaScriptAutoCompleteList,
                        ...anyWordAutoCompleteList
                    ])
                ];

                var cursor = editor.getCursor();
                var currentLine = editor.getLine(cursor.line);
                var start = cursor.ch;
                var end = start;

                while (end < currentLine.length && /[\w$]+/.test(currentLine.charAt(end))) {
                    end++;
                }
                while (start && /[\w$]+/.test(currentLine.charAt(start - 1))) {
                    start--;
                }

                var curWord = start != end && currentLine.slice(start, end);
                var regex = new RegExp('^' + curWord, 'i');

                var filteredHojiList = (!curWord ? hojiAutoCompleteList : hojiAutoCompleteList.filter(function (item) {
                    return item.match(regex);
                })).sort();

                var fullAutoCompleteList = [
                    ...new Set([
                        ...combinedAutoCompleteList,
                        ...filteredHojiList
                    ])
                ];

                var result = {
                    list: fullAutoCompleteList,
                    from: CodeMirror.Pos(cursor.line, start),
                    to: CodeMirror.Pos(cursor.line, end)
                };
                return result;
            };

            CodeMirror.commands.autocomplete = function (cm) {
                CodeMirror.showHint(cm, CodeMirror.hint.javascript);
            };
        }

        $('.ui.modal').on('click', '.edit-submit', () => {
            var attachScript = function () {
                codeTextArea.value = codeEditor.getValue();
                var onSuccess = function (postResult) {
                    onSaved(postResult.value.field);
                }
                var url = contextPath + '/field/' + formId + '/attachScript';
                editObject(formToJsonString($("#edit-form")), url, onSuccess);
            }
            handleAuthenticatedRequest(attachScript);
        });
        $('#code-comment')
            .off('click')
            .on('click', function (e) {
                e.preventDefault();
                codeEditor.toggleComment({indent: true});
            });
    };
    handleAuthenticatedRequest(function () {
        var onSuccess = function (autocompleteList) {
            openModal(autocompleteList);
        }
        Utils.fetch({url: contextPath + "/field/autocomplete", onSuccess: onSuccess});
    });
}

function setupModal(settings) {
    // Show loader when preparing edit form i.e. setting up options
    const dimmerId = Math.random().toString(36).substring(2); // SO: https://stackoverflow.com/a/8084248
    $('body').append("<div id='" + dimmerId + "' class='ui dimmer active'><div class='ui text loader'>Preparing edit form</div></div>");
    var field = settings.field;
    var formId = settings.formId;
    var fieldEditTemplate = _.template($('script.field-edit-template').html());
    $('#modal-container').empty().append(fieldEditTemplate({'field': field}));
    $('.ui.modal').modal({closable: false});
    $('.ui.dropdown', '.ui.modal').dropdown();
    (function () {
        var onComplete = function (options) {
            var types = options.types.map(function (type) {
                var value = {'name': type.name, 'value': String(type.id)};
                if (field && field.type && field.type.id === type.id) {
                    value.selected = true;
                }
                return value;
            });
            $(".ui.dropdown.field-type").dropdown({'values': types, 'fullTextSearch': true});

            var choiceGroups = options.choiceGroups.map(function (choiceGroup) {
                var value = {'name': choiceGroup.name, 'value': String(choiceGroup.id)};
                if (field && field.choiceGroup && field.choiceGroup.id === choiceGroup.id) {
                    value.selected = true;
                }
                return value;
            });
            var message = {noResults: "No choices available. To add some, close this window and click on Choices on the main menu"}
            $(".ui.dropdown.choice-group").dropdown({
                'values': choiceGroups,
                'clearable': true,
                'fullTextSearch': true,
                message: message
            });

            var inputFormats = options.inputFormats.map(function (format) {
                var value = {'name': format.label, 'value': String(format.value)};
                if (field && field.tag == format.value) {
                    value.selected = true;
                }
                return value;
            });
            $(".ui.dropdown.input-format").dropdown({'values': inputFormats, 'fullTextSearch': true});

            var fieldActions = options.fieldActions.map(function (action) {
                var value = {'name': action.label, 'value': String(action.value)};
                if (field && field.missingAction == action.value) {
                    value.selected = true;
                }
                return value;
            });
            $(".ui.dropdown.missing-action").dropdown({'values': fieldActions});

            var uniqueSettings = options.uniqueSettings.map(function (action) {
                var value = {'name': action.label, 'value': String(action.value)};
                if (field && field.uniqueness === action.value) {
                    value.selected = true;
                }
                return value;
            });
            $(".ui.dropdown.unique-setting").dropdown({'values': uniqueSettings});

            var parents = options.parents.map(function (parent) {
                var value = {'name': parent.description, 'value': String(parent.id)};
                if (field && field.parent && field.parent.id === parent.id) {
                    value.selected = true;
                }
                return value;
            });
            $(".ui.dropdown.parent").dropdown({'values': parents, 'clearable': true, 'fullTextSearch': true});

            var choiceFilters = options.choiceFilters.map(function (choiceFilter) {
                var value = {'name': choiceFilter.description, 'value': String(choiceFilter.id)};
                if (field && field.choiceFilterField && field.choiceFilterField.id === choiceFilter.id) {
                    value.selected = true;
                }
                return value;
            });
            $(".ui.dropdown.choice-filter").dropdown({
                'values': choiceFilters,
                'clearable': true,
                'fullTextSearch': true
            });

            var recordSources = options.recordSources.map(function (recordSource) {
                var value = {'name': recordSource.name, 'value': String(recordSource.id)};
                if (field && field.recordForms && field.recordForms.findIndex(rs => rs.id === recordSource.id) > -1) {
                    value.selected = true;
                }
                return value;
            });
            $(".ui.dropdown.record-source").dropdown({
                'values': recordSources,
                'clearable': true,
                'fullTextSearch': true
            });

            var referenceFields = options.referenceFields.map(function (referenceField) {
                var value = {'name': referenceField.description, 'value': String(referenceField.id)};
                if (field && field.referenceField && field.referenceField.id === choiceFilter.id) {
                    value.selected = true;
                }
                return value;
            });
            $(".ui.dropdown.reference-field").dropdown({
                'values': referenceFields,
                'clearable': true,
                'fullTextSearch': true
            });

            //Show modal after initializing dropdown options
            $('#' + dimmerId).remove();
            $('.ui.modal').modal('show');
        }

        var url = contextPath + "/field/editor-options?formId=" + formId;
        if (field) {
            url += "&fieldId=" + field.id;
        }
        Utils.fetch({url: url, onSuccess: onComplete});
    })();
}