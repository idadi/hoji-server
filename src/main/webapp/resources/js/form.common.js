function editForm(form, surveyId, onSaved) {
    var openModal = function () {
        $('.ui.modals').remove();
        var editFormTemplate = _.template($('script.form-edit-template').html());
        $('#modal-container').empty().append(editFormTemplate({'form': form}));
        $('.ui.dropdown', '.ui.modal').dropdown();
        $('.ui.modal').modal({closable: false}).modal('show');
        $('.ui.modal').on('click', '.edit-submit', () => {
            var addForm = function () {
                var onSuccess = function (postResult) {
                    onSaved(postResult.value);
                }
                var url = contextPath + '/form/' + surveyId + '/save';
                editObject(formToJsonString($("#edit-form")), url, onSuccess);
            }
            handleAuthenticatedRequest(addForm);
        });
    };
    handleAuthenticatedRequest(openModal);
}

function deleteForm(form, postDelete) {
    var openModal = function () {
        var objectType = ' form';
        var deleteMessage = "<strong>WARNING: </strong>This action CANNOT be undone. It will permanently delete " +
            "<strong>" + form.name + "</strong> and all associated records including fields and data. " +
            "If you are ABSOLUTELY SURE this is what you want, please enter the name of the form to confirm.";
        var deleteHelpText = "Enter the name of this form to delete it.";
        var deleteHelpLink = "https://www.hoji.co.ke/support/#reamaze#0#/kb/digitization/how-to-delete-a-form";
        var deleteHelpLinkText = "More about deleting a form.";
        openDeleteWithTemplateModal(
            {
                'configObject': form,
                'label': 'Form name',
                'objectType': objectType,
                'deleteMessage': deleteMessage,
                'deleteHelpText': deleteHelpText,
                'deleteHelpLink': deleteHelpLink,
                'deleteHelpLinkText': deleteHelpLinkText
            },
            $('script.resource-delete-template'));
        $('.ui.modal')
            .on('click', '.delete-submit', () => {
                var deleteForm = function () {
                    deleteObject(postDelete, contextPath + '/form/delete');
                }
                handleAuthenticatedRequest(deleteForm);
            });
    };
    handleAuthenticatedRequest(openModal);
}