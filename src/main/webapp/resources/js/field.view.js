function refreshView() {
    var fieldViewTemplate = _.template($('script.field-view-template').html());
    $('#field-view-container').empty().append(fieldViewTemplate({'field': field}));

    $('.ui.dropdown').dropdown({action: 'hide'});

    $('[data-content],[data-tooltip]').popup();

    $('.rule.table tr.added')
        .transition('glow', function () {
            rules = rules.map(r => {
                delete r["added"]
                return r;
            });
        });

    $('.rule.table tr.deleted')
        .transition('glow', '500ms')
        .transition('fade', 500, function () {
            rules = rules.filter(r => !r.deleted);
        });

    $('div.edit')
        .off('click')
        .on('click', function (e) {
            e.preventDefault();
            editField(
                field,
                field.form.id,
                function (saved) {
                    field = saved;
                    refreshView();
                });
        });

    $('.delete')
        .off('click')
        .on('click', function (e) {
            e.preventDefault();
            deleteField(field, contextPath + '/form/view/' + field.form.id + '/field');
        });

    $('.enable')
        .off('click')
        .on('click', function () {
            var onSuccess = function (toggledField) {
                field = toggledField;
                refreshView();
            };
            $('.more-actions').addClass('loading');
            toggleEnabled(field, "field", onSuccess);
        });

    $('.rule-add, .edit-rule')
        .off('click')
        .on('click', function (event) {
            event.preventDefault();
            var rule = null;
            if ($(event.target).hasClass('icon')) {
                var ruleId = $(this).closest('tr').data('ruleId');
                rule = rules.find(r => r.id === ruleId);
            }

            function onSaved(savedRule) {
                var position = rules.findIndex(r => r.id === savedRule.id);
                if (position === -1) {
                    savedRule.added = true;
                    rules.push(savedRule);
                } else {
                    rules.splice(position, 1, savedRule)
                }
                rules.sort(function (a, b) {
                    return a.type + (a.ordinal - b.ordinal)
                })
                refreshView();
            }

            addSkipLogic(field, rule, onSaved);
        });

    $('.delete-rule')
        .off('click')
        .on('click', function (event) {
            event.preventDefault();
            var ruleId = $(this).closest('tr').data('ruleId');
            var openModal = function () {
                var deleteMessage = 'Are you sure you want to delete this skip logic statement?';
                openDeleteWithTemplateModal(
                    {
                        'configObject': field,
                        'label': null,
                        'objectType': 'rule',
                        'deleteMessage': deleteMessage
                    },
                    $('script.resource-delete-template'));
                $('.ui.modal')
                    .on('click', '.delete-submit', () => {
                        var deleteField = function () {
                            var onSuccess = function () {
                                if (rules.length > 1) {
                                    var deletedRule = rules.find(r => r.id == ruleId);
                                    if (deletedRule) {
                                        deletedRule.deleted = true;
                                    }
                                } else {
                                    rules = [];
                                }
                                refreshView();
                            }
                            deleteObject(onSuccess, contextPath + '/rule/' + field.id + '/' + ruleId + '/delete');
                        }
                        handleAuthenticatedRequest(deleteField);
                    });
            };
            handleAuthenticatedRequest(openModal);
        });


    if ($('.rule.table tbody').hasClass('ui-sortable')) {
        $('.rule.table tbody').sortable('destroy');
    }

    $('.rule.table tbody')
        .sortable({
            handle: '.sort-handle',
            stop: function () {
                var onFail = function () {
                    rules = originalRules;
                    list();
                };
                var userSort = function () {
                    var sortedRuleIds = [];
                    $('tbody tr').each(function (index, tr) {
                        var ruleId = $(tr).data('ruleId');
                        if (ruleId) {
                            sortedRuleIds.push(ruleId);
                        }
                    });

                    var sortedRules = [];
                    sortedRuleIds.forEach(function (id, index) {
                        var found = rules.find(function (rule) {
                            return rule.id == id;
                        });
                        // update ordinal
                        found.ordinal = index + 1;
                        sortedRules.push(found);
                    });
                    rules = sortedRules;
                };
                var originalRules = $.extend(true, [], rules);
                userSort();
                var sorted = !originalRules.every(function (originalRule, index) {
                    return originalRule.id === rules[index].id;
                });
                if (sorted) {
                    list();
                    var url = contextPath + '/rule/sort';
                    var sortedRuleIds = rules.map(function (rule) {
                        return rule.id;
                    });
                    var data = {ownerId: owner.id, type: type, sortedRuleIds: sortedRuleIds};
                    submitSortOrder(url, data, onFail);
                }
            }
        });

    $('.script-attach')
        .off('click')
        .on('click', function (e) {
            e.preventDefault();
            attachScript(
                field,
                field.form.id,
                function (saved) {
                    field = saved;
                    refreshView();
                })
        });

    $('#take-a-tour')
        .off('click')
        .on('click', function () {
            var tourSteps = getTourSteps();
            introJs()
                .setOptions({
                    'skipLabel': 'Exit',
                    'hidePrev': true,
                    'hideNext': true,
                    'exitOnEsc': false,
                    'exitOnOverlayClick': false,
                    'disableInteraction': true,
                    'steps': tourSteps
                })
                .start();
        });

    function getTourSteps() {
        var tourSteps = [];
        tourSteps[0] = {
            element: '.breadcrumb',
            intro: 'This menu shows you where you are on the application.'
        };
        tourSteps[1] = {
            element: '#field-name',
            intro: 'This is the field that you currently have open.'
        };
        tourSteps[2] = {
            element: '.object-attributes',
            intro: 'The attributes of the current field are shown here.'
        };
        if (field.type.code === 'MRX') {
            tourSteps[3] = {
                element: '#report-button',
                intro: 'Use this button to view data reports for this field.'
            };
        }
        if (rules.length == 0) {
            tourSteps[4] = {
                element: '.more-actions',
                intro: 'Click here for more actions, including skip logic.'
            };
        } else {
            tourSteps[4] = {
                element: '.more-actions',
                intro: 'Additional actions for the currently open field.'
            };
            tourSteps[5] = {
                element: '#rules-title',
                intro: 'The skip logic under this form is listed here.'
            };
            tourSteps[6] = {
                element: '.rule-row',
                intro: 'Skip logic statements are expressed here in plain English.'
            };
            tourSteps[7] = {
                element: '#rule-add',
                intro: 'Click here to add skip logic to the current field.'
            };
            tourSteps[8] = {
                element: '.rule-edit',
                intro: 'Use this button to edit skip logic.'
            };
            tourSteps[9] = {
                element: '.rule-delete',
                intro: 'Use this button to delete skip logic you no longer need.'
            };
        }
        return tourSteps.filter(function (el) {
            return el != null;
        });
    }
}
