/**
 * Created by gitahi on 1/10/17.
 */

function refreshView() {

    function renderRoles(roles) {
        var ret = [];
        for (i = 0; i < roles.length; i++) {
            ret[i] = roles[i].description;
        }
        if (ret.length === 0) {
            return 'None';
        } else {
            return ret.toString().replace(/,/g, ', ');
        }
    };

    var userViewTemplate = _.template($('script.user-view-template').html());
    $('#user-view').empty().append(userViewTemplate({user: user, renderRoles: renderRoles}));

    $('[data-content],[data-tooltip]').popup();

    $('.ui.dropdown').dropdown({action: 'hide'});

    $('.collaborate.button')
        .off('click')
        .on('click', function () {
            var openModal = function () {
                $('.ui.modals').remove();
                var collaborateTemplate = _.template($('script.collaborate-template').html());
                $('#modal-container').empty().append(collaborateTemplate({'user': null}));
                $('.ui.modal').modal({closable: false}).modal('show');
                $('.use-own-code')
                    .off('change')
                    .on('change', function () {
                        if ($(this).is(':checked')) {
                            $('input[name="tenantCode"]').val(user.code).prop('readonly', true);
                        } else {
                            $('input[name="tenantCode"]').val('').prop("readonly", false);
                        }
                    });
                $('.ui.modal').on('click', '.collaborate', function () {
                    var changeTenant = function () {
                        var url = contextPath + '/user/changeTenant';
                        var data = formToJsonString($("#edit-form"));
                        var onSuccess = function (postResult) {
                            infoMessage = postResult.value.message;
                            user = postResult.value.user;
                            tenant = postResult.value.tenant;
                            refreshView();
                        }
                        editObject(data, url, onSuccess);
                    }
                    handleAuthenticatedRequest(changeTenant);
                });
            };
            handleAuthenticatedRequest(openModal);
        });

    $('#edit-user-button')
        .off('click')
        .on('click', function () {
            var openModal = function (editType) {
                $('.ui.modals').remove();
                var userEditTemplate = _.template($('script.user-edit-template').html());
                $('#modal-container').empty().append(userEditTemplate({user: user}));
                $('#modal-container select').dropdown();
                $('.ui.modal').modal({closable: false}).modal('show');
                $('.ui.modal').on('click', '.edit-submit', function () {
                    var editUser = function () {
                        var url = contextPath + '/user/edit';
                        var data = formToJsonString($("#edit-form"), ['roles', 'defaultRoles']);
                        var onSuccess = function (postResult) {
                            user = postResult.value.user;
                            tenant = postResult.value.tenant;
                            refreshView();
                        }
                        editObject(data, url, onSuccess);
                    }
                    handleAuthenticatedRequest(editUser);
                });
            };
            handleAuthenticatedRequest(openModal);
        });

    $('#verify-email-button')
        .off('click')
        .on('click', function () {
            verifyEmail();
        });

    $('#change-email-button')
        .off('click')
        .on('click', function () {
            changeEmail();
        });

    $('#change-password-button')
        .off('click')
        .on('click', function () {
            var openModal = function () {
                $('.ui.modals').remove();
                var passwordChangeTemplate = _.template($('script.password-change-template').html());
                $('#modal-container').empty().append(passwordChangeTemplate());
                $('.ui.modal').modal({closable: false}).modal('show');
                $('.ui.modal').on('click', '.edit-submit', function () {
                    var passwordChange = function () {
                        var url = contextPath + '/user/changePassword';
                        var data = formToJsonString($("#edit-form"));
                        var onSuccess = function (postResult) {
                            infoMessage = postResult.value.message;
                            refreshView();
                        }
                        editObject(data, url, onSuccess);
                    }
                    handleAuthenticatedRequest(passwordChange);
                });
            };
            handleAuthenticatedRequest(openModal);
        });

    $('#activate-sample-project-button')
        .off('click')
        .on('click', function () {
            var activateSampleProject = function () {
                var url = contextPath + '/user/activateSampleProject';
                var data = '{ "publicSurveyAccess": ' + (user.publicSurveyAccess == 0 ? 1 : 0) + ' }';
                var onSuccess = function (postResult) {
                    infoMessage = postResult.value.message;
                    user = postResult.value.user;
                    refreshView();
                }
                editObject(data, url, onSuccess);
            };
            handleAuthenticatedRequest(activateSampleProject);
        });

    $('.invite-collaborators-button')
        .off("click")
        .on("click", function () {
            var openModal = function () {
                $('.ui.modals').remove();
                var collaboratorInvitationTemplate = _.template($('script.collaborator-invitation-template').html());
                $('#modal-container').empty().append(collaboratorInvitationTemplate(user));
                $('.ui.modal').modal({closable: false}).modal('show');
            };
            handleAuthenticatedRequest(openModal);
        });

    var selectedUserIds = [];

    $('.check-user')
        .off('click')
        .on('click', function () {
            var userId = $(this).data('userId');
            if ($(this).is(':checked')) {
                selectedUserIds.push(userId);
            } else {
                var pos = selectedUserIds.indexOf(userId);
                selectedUserIds.splice(pos, 1);
            }
        })

    $('#assign-roles-button')
        .off("click")
        .on("click", function () {
            assignPermissions();
        });

    $('#expel-colkaborators-button')
        .off('click')
        .on('click', function () {
            expelCollaborators();
        });

    $('.assign-permissions-one')
        .off('click')
        .on('click', function () {
            $('.check-user').prop('checked', false);
            selectedUserIds = [];
            var collaboratorId = $(this).closest('tr').data('collaboratorId');
            selectedUserIds[0] = collaboratorId;
            $('#checkbox-' + collaboratorId).prop('checked', true);
            assignPermissions();
        });

    $('.expel-one')
        .off('click')
        .on('click', function () {
            $('.check-user').prop('checked', false);
            selectedUserIds = [];
            var collaboratorId = $(this).closest('tr').data('collaboratorId');
            selectedUserIds[0] = collaboratorId;
            $('#checkbox-' + collaboratorId).prop('checked', true);
            expelCollaborators();
        });

    function assignPermissions() {
        var openModal = function () {
            if (selectedUserIds.length === 0) {
                errors = {title: 'Please select one or more collaborators to grant permissions to'};
                refreshView();
                return;
            }
            $('.ui.modals').remove();
            var roleAssignmentTemplate = _.template($('script.role-assignment-template').html());
            $('#modal-container').empty().append(roleAssignmentTemplate({
                'userIds': selectedUserIds,
                'firstCollaborator': collaborators.filter(c => c.id === selectedUserIds[0])[0]
            }));
            $('.ui.modal').modal({closable: false}).modal('show');
            $('.ui.dropdown', '.ui.modal').not('select').dropdown({
                values: [{name: '< Keep existing >', value: -999, selected: true}],
                allowAdditions: true
            });
            $('select.dropdown').dropdown({showOnFocus: false});
            $('.ui.modal').on('click', '.edit-submit', () => {
                $('#edit-form').addClass('loading');
                var assignRoles = function () {
                    var onSuccess = function (postResult) {
                        postResult.value.forEach(collaborator => {
                            var pos = collaborators.findIndex(elem => collaborator.id === elem.id);
                            collaborators.splice(pos, 1, collaborator);
                        });
                        refreshView();
                    }
                    var url = contextPath + '/user/assignRoles';
                    var data = formToJsonString($('#edit-form'), ['ids', 'roles']);
                    editObject(data, url, onSuccess);
                }
                handleAuthenticatedRequest(assignRoles);
            });
        };
        handleAuthenticatedRequest(openModal);
    }

    function expelCollaborators() {
        var openModal = function () {
            if (selectedUserIds.length === 0) {
                errors = {title: 'Please select one or more collaborators to expel'};
                refreshView();
                return;
            }
            $('.ui.modals').remove();
            var confirmExpulsionTemplate = _.template($('script.confirm-expulsion-template').html());
            $('#modal-container').empty().append(confirmExpulsionTemplate({
                'noOfCollaborators': selectedUserIds.length,
                'firstCollaborator': collaborators.filter(c => c.id === selectedUserIds[0])[0]
            }));
            $('.ui.modal').modal({closable: false}).modal('show');
            $('.ui.modal').on('click', '.confirm-yes', function () {
                var url = contextPath + "/user/expel?" + toQueryString({userIds: selectedUserIds});
                var onSuccess = function (resultValue) {
                    collaborators = resultValue.collaborators;
                    var expelled = resultValue.expelled;
                    var name = expelled == 1 ? 'collaborator' : 'collaborators';
                    infoMessage = expelled + ' ' + name + ' expelled!';
                    $('.ui.modal').modal('hide');
                    refreshView();
                };
                Utils.fetch({method: 'post', url: url, onSuccess: onSuccess})
            });
        };
        handleAuthenticatedRequest(openModal);
    }

    $('#manage-webhooks-button')
        .off('click')
        .on('click', function () {
            window.location.href = contextPath + '/hook/list'
        });

    (function() {
        Utils.renderMessage({ info: infoMessage, error: errors });
        infoMessage = null;
        errors = null;
    })();

    $('#take-a-tour')
        .off('click')
        .on('click', function () {
            var tourSteps = getTourSteps();
            introJs()
                .setOptions({
                    'skipLabel': 'Exit',
                    'hidePrev': true,
                    'hideNext': true,
                    'exitOnEsc': false,
                    'exitOnOverlayClick': false,
                    'disableInteraction': true,
                    'steps': tourSteps
                })
                .start();
        });

    function getTourSteps() {
        var tourSteps = [];
        tourSteps[0] = {
            element: '#user-name',
            intro: 'This is your name, obviously.'
        };
        tourSteps[1] = {
            element: '.object-attributes',
            intro: 'The attributes of your user account are shown here.'
        };
        if (user.id === tenant.id) {
            tourSteps[2] = {
                element: '#current-host',
                intro: 'You are currently hosting personal projects. You can invite collaborators to work with you.'
            };
            tourSteps[3] = {
                element: '#my-permissions',
                intro: 'These are the permissions you have under your personal account.'
            };
        } else {
            tourSteps[2] = {
                element: '#current-host',
                intro: 'You are currently collaborating with ' + tenant.fullName + '.'
            };
            tourSteps[3] = {
                element: '#my-permissions',
                intro: 'These are the permissions you have been granted by ' + tenant.fullName + '.'
            };
        }
        tourSteps[5] = {
            element: '#default-permissions',
            intro: 'These are the permissions that are automatically assigned to your collaborators.'
        };
        tourSteps[6] = {
            element: '.more-actions',
            intro: 'Additional actions like changing your email and resetting your password.'
        };
        if (collaborators.length == 0) {
            tourSteps[7] = {
                element: '.invite-collaborators-button',
                intro: 'Click here for instructions on how to invite collaborators.'
            };
            tourSteps[8] = {
                element: '.invite-collaborators-button',
                intro: 'People collaborating with you will be listed here.'
            };
        } else {
            tourSteps[9] = {
                element: '#collaborators-title',
                intro: 'Other people collaborating with you on your projects are shown here.'
            };
            tourSteps[10] = {
                element: '.invite-collaborators-button',
                intro: 'Click here for instructions on how to invite collaborators.'
            };
            tourSteps[11] = {
                element: '.bulk-actions',
                intro: 'Grant permissions to or expel multiple collaborators.'
            };
            tourSteps[12] = {
                element: '.assign-permissions-one',
                intro: 'Grant permissions to an individual collaborator.'
            };
            tourSteps[13] = {
                element: '.expel-one',
                intro: 'Expel an individual collaborator.'
            };
            tourSteps[14] = {
                element: '.collaborator-row',
                intro: 'Click on a collaborators email to contact them.'
            };
        }
        return tourSteps.filter(function (el) {
            return el != null;
        });
    }
}
