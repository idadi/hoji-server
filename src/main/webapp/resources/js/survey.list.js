function refreshView() {
    var listSurveyTemplate = _.template($('script.survey-list-template').html());
    $('#list-survey-container').empty().append(listSurveyTemplate(surveys));

    if (hoursToExpiry < 24) {
        var message = 'Please verify your email <strong>' + user.username + '</strong> to avoid interruption. If' +
            ' it is not yours, ' +
            '<a href="#" onclick="changeEmail()">change it</a>. ' +
            'If you would like us to resend the verification link, ' +
            '<a href="#" onclick="verifyEmail()">click here</a>.';
        $('#banner-message-content')
            .html('<p>' + message + '</p>')
            .closest('.message')
            .attr('class', 'ui hidden warning message')
            .transition('fade');
    }

    $('#add-button, .edit-project')
        .off('click')
        .on('click', function (event) {
            event.preventDefault();
            var surveyId = $(this).closest('tr').data('surveyId');
            var survey = surveys.find(s => s.id == surveyId);
            editProject(survey, function (saved) {
                saved.added = true;
                var position = surveys.findIndex(s => s.id === saved.id);
                if (position === -1) {
                    window.location.href = contextPath + '/project/view/' + saved.id + '/form';
                } else {
                    surveys.splice(position, 1, saved)
                }
                refreshView();
            });
        });

    $('.delete-project')
        .off('click')
        .on('click', function (event) {
            event.preventDefault();
            var surveyId = $(this).closest('tr').data('surveyId');
            var survey = surveys.find(s => s.id == surveyId);
            deleteSurvey(survey, function () {
                var deleted = surveys.find(s => s.id == survey.id);
                if (deleted) {
                    deleted.deleted = true;
                }
                refreshView();
            });
        });

    $('.project.table tr.added')
        .transition('glow', function () {
            surveys = surveys.map(s => {
                delete s["added"]
                return s;
            });
        });

    $('.project.table tr.deleted')
        .transition('glow', '500ms')
        .transition('fade', 500, function () {
            surveys = surveys.filter(s => !s.deleted);
        });

    $('.ui.dropdown').dropdown({action: 'hide'});

    (function(){
        Utils.renderMessage({ info: infoMessage });
        infoMessage = null;
    })();

    $('#take-a-tour')
        .off('click')
        .on('click', function () {
            introJs()
                .setOptions({
                    'skipLabel': 'Exit',
                    'hidePrev': true,
                    'hideNext': true,
                    'exitOnEsc': false,
                    'exitOnOverlayClick': false,
                    'disableInteraction': true,
                    'steps': getTourSteps()
                })
                .start();
        });

    function getTourSteps() {
        var tourSteps = [
            {
                element: '#project-title',
                intro: 'Your projects are listed here. A project is a way to group related forms.'
            },
            {
                element: '#add-button',
                intro: 'Use this button to create a new project.'
            },
            {
                element: '.project-link',
                intro: 'Click on an existing project to open it and add some forms.'
            },
            {
                element: '.project-link',
                intro: 'If the project already has forms, click here to see them.',
                position: 'right'
            },
            {
                element: '.edit-project',
                intro: 'Use this button to edit a project.'
            },
            {
                element: '.delete-project',
                intro: 'Use this button to delete a project you no longer need.'
            },
            {
                intro: '<strong>IMPORTANT:</strong> Only projects that are deployed and not disabled are accessible' +
                    ' for data entry through the Hoji mobile app.'
            }
        ];
        return tourSteps;
    }
}