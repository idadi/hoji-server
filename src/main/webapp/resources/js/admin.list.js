function refreshView() {
    var emailTemplateListTemplate = _.template($('script.email-template-list-template').html());
    $('#email-template-list-container').empty().append(emailTemplateListTemplate({ 'emailTemplates': emailTemplates }));

    $('.message .close')
        .on('click', function () {
            $(this).closest('.message').transition('fade');
        });

    $('.ui.dropdown').dropdown({ action: 'hide' });

    $('.add-email-template, .edit-email-template')
        .off('click')
        .on('click', function () {
            var emailTemplateId = $(this).closest('tr').data('emailTemplateId');
            var emailTemplate = null;
            if (emailTemplateId) {
                emailTemplate = emailTemplates.find(et => et.id === emailTemplateId);
            }
            var openModal = function () {
                $('.ui.modals').remove();
                var emailTemplateEditTemplate = _.template($('script.email-template-edit-template').html());
                $('#modal-container').empty().append(emailTemplateEditTemplate({ emailTemplate: emailTemplate }));
                (function () {
                    var onSuccess = function (eventTypes) {
                        var values = eventTypes.map(function (eventType) {
                            return { 'name': eventType.label, 'value': eventType.value };
                        });
                        if (emailTemplate) {
                            values.unshift({ 'name': emailTemplate.eventType.label, 'value': emailTemplate.eventType.value, selected: true });
                        }
                        $(".ui.dropdown.event-type").dropdown({ 'values': values });
                    }
                    Utils.fetch({url: contextPath + "/admin/template/event-type", onSuccess: onSuccess});
                })()
                $('textarea[name="template"]').markItUp(mySettings);
                $('.ui.modal').modal({ autofocus: false, closable: false }).modal('show');
                $('.ui.modal').on('click', '.edit-submit', () => {
                    var editEmailTemplate = function () {
                        var onSuccess = function (postResult) {
                            var saved = postResult.value;
                            var position = emailTemplates.findIndex(et => et.id === saved.id);
                            if (position > -1) {
                                emailTemplates.splice(position, 1, saved);
                            } else {
                                saved.added = true;
                                emailTemplates.push(saved);
                            }
                            refreshView();
                        }
                        editObject(formToJsonString($("#edit-form")), contextPath + '/admin/template/edit', onSuccess);
                    }
                    handleAuthenticatedRequest(editEmailTemplate);
                });
            };
            handleAuthenticatedRequest(openModal);
        });
}