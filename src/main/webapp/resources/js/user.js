/**
 * Created by geoffreywasilwa on 02/16/17.
 */

var refreshInterval = null;

/*
 * ---------------------------------------------------------
 * PRIMARY FUNCTIONS
 * ---------------------------------------------------------
 */
function view() {
    var viewTemplate = _.template($('script.view-template').html());
    $('#view-container').empty().append(viewTemplate());
    $('#view-container .tooltipped').tooltip({delay: 50});
    $(".dropdown-button").dropdown();

    $('#refresh-button')
        .off("click")
        .on("click", function () {
        queryBalance();
    });

    if (refreshInterval) {
        window.clearInterval(refreshInterval);
    }
    refreshInterval = setInterval(queryBalance, 60000);
}

function edit() {
    var url = contextPath + '/user/edit';
    var formData = formToJsonString($('#edit-form'), ['roles', 'defaultRoles']);
    editObject(formData, url, onEditSuccess);
}

function onEditSuccess(postResult) {
    user = postResult.value.user;
    tenant = postResult.value.tenant;
    view();
}

function prepareBill() {
    var url = contextPath + '/user/buy';
    var credits = $('#credits-form #credits').val();
    if (credits > 0) {
        $.ajax({
            type: 'POST',
            beforeSend: function (request) {
                request.setRequestHeader(header, token);
            },
            url: url,
            data: 'credits=' + credits,
            dataType: 'json'
        }).done(function (postResult) {
            if (postResult.success) {
                var purchase = postResult.value.purchase;
                var canAfford = postResult.value.canAfford;
                var cashBalance = postResult.value.cashBalance;
                var billTemplate = _.template($('script.bill-template').html());
                $('#bill-form')
                    .empty()
                    .append(billTemplate(
                        {credits: credits, purchase: purchase, canAfford: canAfford, cashBalance: cashBalance}));
            }
        }).fail(function (jqXHR) {
            showSystemError(jqXHR);
        }).always(function () {
            $('.progress').hide();
        });
    } else {
        $('.progress').hide();
        var billTemplate = _.template($('script.bill-template').html());
        $('#bill-form')
            .empty()
            .append(billTemplate({credits: credits, purchase: null, canAfford: null, cashBalance: null}));
    }
}

function buyCredits() {
    var url = contextPath + '/user/buy2';
    $.ajax({
        type: 'POST',
        beforeSend: function(request) {
            request.setRequestHeader(header, token);
        },
        url: url,
        data: 'credits=' + $('#bill-form input[name="credits"]').val(),
        dataType: 'json'
    }).done(function (postResult) {
        if (postResult.success) {
            window.location.href = contextPath + '/user/view?bought';
        } else {
            $('#buy-credits-submit').attr('disabled', false);
            Materialize.toast(postResult.value, 3000);
        }
    }).fail(function(jqXHR) {
        $('#buy-credits-submit').attr('disabled', false);
        showSystemError(jqXHR);
    }).always(function() {
        $('.progress').hide();
    });
}

function queryBalance() {
    var url = contextPath + '/user/cash/balance';
    $('#refresh-button').addClass('refresh-animate');
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json'
    }).done(function (getResult) {
        if (getResult.success) {
            cashBalance = getResult.value;
            view();
        } else {
            Materialize.toast("An error occured while querying for cash balance!", 3000);
        }
    }).fail(function(jqXHR) {
        showSystemError(jqXHR);
    }).always(function() {
        $('#refresh-button').removeClass('refresh-animate');
    });
}

/*
 * ---------------------------------------------------------
 * HELPER FUNCTIONS
 * ---------------------------------------------------------
 */
function openEditModal() {
    var editTemplate = _.template($('script.edit-template').html());
    $('#edit-container').empty().append(editTemplate());
    Materialize.updateTextFields();
    $('select').material_select();
    $('#edit-modal').openModal({dismissible: false});
    registerEscapeKey($('#edit-modal'));
}