/**
 * Created by gitahi on 1/10/17.
 */

function refreshView() {

    var viewSurveyTemplate = _.template($('script.survey-view-template').html());

    $('#survey-view').empty().append(viewSurveyTemplate(survey));

    $('.ui.dropdown').dropdown({action: 'hide'});

    $('.edit-project')
        .off('click')
        .on('click', function () {
            editProject(survey, function (saved) {
                survey = saved;
                refreshView();
            });
        });

    $('.deploy-project')
        .off('click')
        .on('click', function () {
            if (survey.status === 1) {
                infoMessage = survey.name + ' has already been deployed. Sign into the ' +
                    '<a  href="https://play.google.com/store/apps/details?id=ke.co.hoji.android&hl=en" target="_blank">' +
                    'Hoji mobile app' +
                    '</a>' +
                    ' to enter' +
                    ' data.';
                refreshView();
                return;
            }
            $(this).addClass('loading');
            var publish = function () {
                var url = contextPath + '/project/publish';
                var data = 'id=' + survey.id;
                var onSuccess = function (response) {
                    survey = response.survey;
                    forms = response.forms;
                    infoMessage = 'Project deployed! Sign into the ' +
                        '<a href="https://play.google.com/store/apps/details?id=ke.co.hoji.android&hl=en" target="_blank">' +
                        'Hoji mobile app' +
                        '</a>' +
                        ' to enter data.';
                    refreshView();
                };
                var onError = function (deploymentErrors) {
                    errors = {
                        'title': 'The following errors occurred while trying to deploy the project',
                        'details': deploymentErrors
                    }
                    refreshView();
                };
                var onFail = function (jqXHR) {
                    showSystemError(jqXHR);
                    refreshView();
                };
                Utils.fetch({
                    method: 'post',
                    url: url,
                    data: data,
                    onSuccess: onSuccess,
                    onError: onError,
                    onFail: onFail
                });
            };
            handleAuthenticatedRequest(publish);
        });

    $('.enable-project')
        .off('click')
        .on('click', function () {
            var onSuccess = function (toggledSurvey) {
                survey = toggledSurvey;
                refreshView();
            };
            $('.more-actions').addClass('loading');
            toggleEnabled(survey, "project", onSuccess);
        });

    $('.clear-data')
        .off('click')
        .on('click', function () {
            var openModal = function () {
                $('.ui.modals').remove();
                var clearDataTemplate = _.template($('script.survey-clear-template').html());
                $('#modal-container').empty().append(clearDataTemplate(survey));
                $('.ui.modal').modal({closable: false}).modal('show');
                $('.ui.modal')
                    .on('click', '.clear-submit', () => {
                        $('.ui.modal .ui.dimmer').addClass('active');
                        var clearData = function () {
                            var url = contextPath + '/project/clear';
                            var data = $("#clear-data-form").serialize();
                            var onSuccess = function (postResult) {
                                survey.status = postResult.value;
                                infoMessage = 'Data cleared!';
                                $('.ui.modal').modal('hide');
                                refreshView();
                            }
                            var onError = function (errors) {
                                $('.ui.modal .ui.dimmer').removeClass('active');
                                $.each(errors, function (index, postError) {
                                    showParameterError($('#clear-data-form'), postError);
                                });
                            }
                            var onFail = function (jqXHR) {
                                showSystemError(jqXHR);
                                $(".ui.modal .ui.dimmer").removeClass("active");
                            }
                            Utils.fetch({
                                method: 'post',
                                url: url,
                                data: data,
                                onSuccess: onSuccess,
                                onError: onError,
                                onFail: onFail
                            });
                        }
                        handleAuthenticatedRequest(clearData);
                    });
            };
            handleAuthenticatedRequest(openModal);
        });

    $('.delete-project')
        .off('click')
        .on('click', function () {
            deleteSurvey(survey, "/");
        });

    $('.manage-properties')
        .off('click')
        .on('click', function () {
            window.location.href = contextPath + '/project/view/' + survey.id + '/property';
        });

    $('.manage-forms')
        .off('click')
        .on('click', function () {
            window.location.href = contextPath + '/project/view/' + survey.id + '/form';
        });

    $('.add-form, .edit-form')
        .off('click')
        .on('click', function () {
            var formId = $(this).closest('tr').data('formId');
            var form = forms.find(f => f.id == formId);
            editForm(form, survey.id, function (saved) {
                saved.edited = true;
                var position = forms.findIndex(f => f.id === saved.id);
                if (position === -1) {
                    window.location.href = contextPath + '/form/view/' + saved.id + '/field';
                } else {
                    forms.splice(position, 1, saved)
                }
                survey = saved.survey;
                refreshView();
            });
        });

    $('.delete-form')
        .off('click')
        .on('click', function (event) {
            event.preventDefault();
            var formId = $(this).closest('tr').data('formId');
            var form = forms.find(f => f.id == formId);
            deleteForm(form, function () {
                if (forms.length === 1) {
                    forms = [];
                    refreshView();
                    return;
                }
                var deleted = forms.find(f => f.id == form.id);
                if (deleted) {
                    deleted.deleted = true;
                }
                survey.status = 2;
                refreshView();
            });
        });

    $('.form.table tr.edited')
        .transition('glow', function () {
            forms = forms.map(f => {
                delete f["edited"]
                return f;
            });
        });

    $('.form.table tr.deleted')
        .transition('glow', '500ms')
        .transition('fade', 500, function () {
            forms = forms.filter(f => !f.deleted);
        });

    if ($('.form.table tbody').hasClass('ui-sortable')) {
        $('.form.table tbody').sortable('destroy');
    }

    $('.form.table tbody')
        .sortable({
            handle: '.sort-handle',
            stop: function () {
                var sortable = $(this);
                var userSort = function () {
                    var sortedFormIds = [];
                    $('tr', sortable).each(function (index, tr) {
                        var formId = $(tr).data('formId');
                        if (formId) {
                            sortedFormIds.push(formId);
                        }
                    });

                    var sortedForms = [];
                    sortedFormIds.forEach(function (id, index) {
                        var found = forms.find(function (form) {
                            return form.id == id;
                        });
                        //update ordinal
                        found.ordinal = index + 1;
                        sortedForms.push(found);
                    });
                    forms = sortedForms;
                };
                var onFail = function () {
                    forms = originalForms;
                    refreshView();
                };
                var originalForms = $.extend(true, [], forms);
                userSort();
                var sorted = !originalForms.every(function (sortedForm, index) {
                    return sortedForm.id === forms[index].id;
                });
                if (sorted) {
                    var url = contextPath + '/form/sort';
                    var sortedFormIds = forms.map(function (form) {
                        return form.id;
                    });
                    var data = {surveyId: survey.id, sortedFormIds: sortedFormIds};
                    submitSortOrder(url, data, onFail);
                }
            }
        });

    $('.add-property, .edit-property')
        .off('click')
        .on('click', function () {
            var propertyKey = null;
            var property = null;
            if ($('.property.table').length) {
                propertyKey = $(this).closest('tr').data('propertyKey');
                property = properties.find(p => p.key === propertyKey);
            }
            var openModal = function () {
                $('.ui.modals').remove();
                var propertyEditTemplate = _.template($('script.property-edit-template').html());
                $('#modal-container').empty().append(propertyEditTemplate({property: property}));
                $('.ui.modal').modal({closable: false}).modal('show');
                $('.ui.modal')
                    .on('click', '.edit-submit', () => {
                        var saveProperty = function () {
                            var url = contextPath + '/project/' + survey.id + '/property';
                            if (property) {
                                url += '/' + property.key + '/';
                            }
                            var onSuccess = function (response) {
                                var saved = response.value;
                                saved.added = true;
                                var position = properties.findIndex(p => p.key === propertyKey);
                                if (position === -1) {
                                    properties.push(saved);
                                } else {
                                    properties.splice(position, 1, saved);
                                }
                                survey.status = 2;;
                                refreshView();
                            }
                            editObject(formToJsonString($("#edit-form")), url, onSuccess);
                        }
                        handleAuthenticatedRequest(saveProperty);
                    });
            };
            handleAuthenticatedRequest(openModal);
        });

    $('#upload-properties')
        .off('change')
        .on('change', function (event) {
            var formData = new FormData();
            formData.append("propertiesCsv", event.target.files[0]);
            var url = contextPath + '/project/' + survey.id + '/property?action=upload'
            var onSuccess = function (uploaded) {
                properties = uploaded;
                survey.status = 2;
                refreshView();
            }
            var onError = function (message) {
                errors = {title: message};
                refreshView();
            }
            Utils.fetch({
                method: 'post',
                url: url,
                enctype: 'multipart/form-data',
                contentType: false,
                processData: false,
                data: formData,
                onSuccess: onSuccess,
                onError: onError
            });
        });

    $('.property.table tr.added')
        .transition('glow', function () {
            properties = properties.map(p => {
                delete p["added"]
                return p;
            });
        });

    $('.delete-property')
        .off('click')
        .on('click', function () {
            var propertyKey = $(this).closest('tr').data('propertyKey');
            var property = properties.find(p => p.key == propertyKey);
            var openModal = function () {
                $('.ui.modals').remove();
                var objectType = ' property';
                var deleteMessage = "Are you sure you want to delete the property: <strong>" + propertyKey + "</strong>?";
                openDeleteWithTemplateModal(
                    {
                        'configObject': property,
                        'label': null,
                        'objectType': objectType,
                        'deleteMessage': deleteMessage
                    },
                    $('script.resource-delete-template'));
                $('.ui.modal')
                    .on('click', '.delete-submit', () => {
                        var deleteSurvey = function () {
                            url = contextPath + '/project/' + survey.id + '/property/' + propertyKey + '/';
                            var onSuccess = function () {
                                $('.ui.modal').modal('hide');
                                property.deleted = true;
                                survey.status = 2;
                                refreshView();
                            };
                            Utils.fetch({method: 'delete', url: url, onSuccess: onSuccess});
                        };
                        handleAuthenticatedRequest(deleteSurvey);
                    });
            };
            handleAuthenticatedRequest(openModal);
        });

    $('.property.table tr.deleted')
        .transition('glow', '500ms')
        .transition('fade', 500, function () {
            properties = properties.filter(p => !p.deleted);
        });

    (function() {
        Utils.renderMessage({ info: infoMessage, error: errors });
        infoMessage = null;
        errors = null;
    })();

    $('#take-a-tour')
        .off('click')
        .on('click', function () {
            var tourSteps = getTourSteps();
            introJs()
                .setOptions({
                    'skipLabel': 'Exit',
                    'hidePrev': true,
                    'hideNext': true,
                    'exitOnEsc': false,
                    'exitOnOverlayClick': false,
                    'disableInteraction': true,
                    'steps': tourSteps
                })
                .start();
        });

    function getTourSteps() {
        var tourSteps = [];
        tourSteps[0] = {
            element: '.breadcrumb',
            intro: 'This menu shows you where you are on the application.'
        };
        tourSteps[1] = {
            element: '#project-name',
            intro: 'This is the project that you currently have open.'
        };
        tourSteps[2] = {
            element: '.object-attributes',
            intro: 'The attributes of the current project are shown here.'
        };
        tourSteps[3] = {
            element: '.deploy-project',
            intro: 'Use this button to deploy the project for data collection.'
        };
        tourSteps[4] = {
            element: '.more-actions',
            intro: 'Additional actions for the current project.'
        };
        if (forms.length == 0) {
            tourSteps[5] = {
                element: '.add-form',
                intro: 'Click here to add a form to the current project.'
            };
            tourSteps[6] = {
                element: '.form-link',
                intro: 'Forms that you add to the project will be listed here.'
            };
        } else {
            tourSteps[7] = {
                element: '#form-title',
                intro: 'The forms under this project are listed here.'
            };
            tourSteps[8] = {
                element: '.add-form',
                intro: 'Click here to add a form to the current project.'
            };
            tourSteps[9] = {
                element: '.form-link',
                intro: 'Click on a form to open it and add some fields.'
            };
            tourSteps[10] = {
                element: '.form-link',
                intro: 'If the form already has fields, click here to see them.',
                position: 'right'
            };
            tourSteps[11] = {
                element: '.edit-form',
                intro: 'Use this button to edit a form.'
            };
            tourSteps[12] = {
                element: '.delete-form',
                intro: 'Use this button to delete a form you no longer need.'
            };
        }
        tourSteps[13] = {
            intro: '<strong>IMPORTANT:</strong> You must always deploy a project in order to collect data on it'
        };
        return tourSteps.filter(function (el) {
            return el != null;
        });
    }
}
