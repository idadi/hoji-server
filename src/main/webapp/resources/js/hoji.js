//TODO: Move methods in this file to the Utils scope. Global scope functions and objects are difficult to work with
//TODO: Rename Utils scope to be similar to file name (will make it easier to locate the methods)

/**
 * The pointer (finger) cursor type.
 */
var POINTER_CURSOR = 'pointer';

/**
 * Signals that an object is about to be added, rather than edited.
 */
var ADD = 'add';

/**
 * Signals that an object is about to be edited, rather than added.
 */
var EDIT = 'edit';

/**
 * The list page, which lists multiple objects e.g surveys, forms or fields.
 */
var LIST_PAGE = 'list';

/**
 * The view page, which displays a single object e.g. survey, form or field.
 */
var VIEW_PAGE = 'view';

/**
 * Serializes the parameters of the form passed to a JSON object.
 *
 * @param form - the form from which to serialize the parameters
 * @param treatAsArray (optional) - an array containing any form parameters that should be treated as an array
 * @param ignore (optional) - an array containing any form parameters that should be ignored
 *
 * @return a JSON representation of the parameters.
 */
function formToJson(form, treatAsArray, ignore) {
    var jsonObject = {};
    var parameterArray = form.serializeArray();
    $.each(parameterArray, function () {
        if (ignore && ignore.indexOf(this.name) > -1) {
            return;
        } else if (treatAsArray && treatAsArray.indexOf(this.name) > -1) {
            if (!jsonObject[this.name]) {
                jsonObject[this.name] = [];
            }
            this.value && jsonObject[this.name].push(this.value);
        } else {
            jsonObject[this.name] = this.value || '';
        }
    });
    return jsonObject;
}

/**
 * Serializes the parameters of the form passed to a JSON string.
 *
 * @param form - the form from which to serialize the parameters
 * @param treatAsArray (optional) - an array containing any form parameters that should be treated as an array
 * @param ignore (optional) - an array containing any form parameters that should be ignored
 *
 * @return a JSON string representation of the object.
 */
function formToJsonString(form, treatAsArray, ignore) {
    return JSON.stringify(formToJson(form, treatAsArray, ignore));
}

/**
 * Sort a list of objects by the attribute specified.
 *
 * @param list - the list of objects to sort
 * @param by - the attribute by which to sort the list
 */
function sort(list, by) {
    list.sort(function (a, b) {
        if (a[by] < b[by]) {
            return -1;
        } else if (a[by] > b[by]) {
            return 1;
        } else {
            return 0;
        }
    });
}

/**
 * Set the appropriate UI components to reflect the enabled/disabled status of an enablable object.
 *
 * @param enablable - an object capable of being enabled or disabled
 */
function setEnabled(enablable) {
    if (!$('#enable-checkbox').length) {
        return;
    }
    if (enablable.enabled) {
        $('#enable-checkbox').prop('checked', true);
        $('#enabled-value').html("Enabled");
    } else {
        $('#enable-checkbox').prop('checked', false);
        $('#enabled-value').html("Disabled");
    }
}

/**
 * Toggle the enabled status of an Enablable object via AJAX.
 *
 * @param resource - the resource to be enabled or disabled
 * @param resourceName - the name of the resource
 */
function toggleEnabled(resource, resourceName, success) {
    if ($('#enabled-value').length) {
        if (resource.enabled) {
            $('#enabled-value').html('Disabling ...');
        } else {
            $('#enabled-value').html('Enabling ...');
        }
    }
    var url = contextPath + "/" + resourceName + "/enable";
    $.get(url, { id: resource.id, enabled: !resource.enabled })
        .done((status) => {
            resource.enabled = status;
            if (success) {
                success.call(null, resource);
            } else {
                setEnabled(resource);
            }
        });
}

function changeEmail() {
    var openModal = function () {
        $('.ui.modals').remove();
        var emailChangeTemplate = _.template($('script.email-change-template').html());
        $('#modal-container').empty().append(emailChangeTemplate());
        $('.ui.modal').modal({ closable: false }).modal('show');
        $('#newEmail').val('');
        $('#currentPassword').val('');
        $('.ui.modal').on('click', '.edit-submit', function () {
            var emailChange = function () {
                var url = contextPath + '/user/changeEmail';
                var data = formToJsonString($("#edit-form"));
                var onSuccess = function (postResult) {
                    user = postResult.value.user;
                    infoMessage = postResult.value.message;
                    refreshView();
                }
                editObject(data, url, onSuccess);
            }
            handleAuthenticatedRequest(emailChange);
        });
    };
    handleAuthenticatedRequest(openModal);
}

function verifyEmail() {
    var verifyEmail = function () {
        var url = contextPath + '/resendToken';
        var onSuccess = function (postResult) {
            window.location.href = '/verificationTokenSent?initial=false';
        }
        editObject({}, url, onSuccess);
    };
    handleAuthenticatedRequest(verifyEmail);
}

//TODO: delete in favor of openDeleteWithTemplateModal
/**
 * Opens the generic deletion form.
 *
 * @param deleteObjectConfig - an object in the form {'configObject': '', 'label': '', 'objectType': '', 'deleteMessage': ''}
 */
function openDeleteModal(deleteObjectConfig) {
    var deleteObjectTemplate = _.template($('script.delete-object-template').html());
    $('#delete-object-container').empty().append(deleteObjectTemplate(deleteObjectConfig));
    if ($('.ui.modal').length) {
        $('.ui.modals').remove();
        $('.ui.modal').modal({ closable: false }).modal('show');
    } else {
        Materialize.updateTextFields();
        $('#delete-modal').openModal({ dismissible: false });
        registerEscapeKey($('#delete-modal'));
        $('#delete-errors').html('');
    }
}

//TODO: rename to openModal
/**
 * Opens a delete modal with specified template form.
 *
 * @param deleteObjectConfig - an object in the form {'configObject': '', 'label': '', 'deleteMessage': '', 'title': ''}
 * @param template, template to render in the delete modal
 */
function openDeleteWithTemplateModal(deleteObjectConfig, template) {
    var deleteObjectTemplate = _.template($(template).html());
    if ($('#modal-container').length) {
        $('#modal-container').empty().append(deleteObjectTemplate(deleteObjectConfig));
    } else {
        $('#delete-object-container').empty().append(deleteObjectTemplate(deleteObjectConfig));
    }
    if ($('.ui.modal').length) {
        $('.ui.modals').remove();
        $('.ui.modal').modal({ closable: false }).modal('show');
        $('#delete-form').on('submit', function (e) {
            e.preventDefault();
            $('.delete-submit').click();
        })
    } else {
        $('#delete-modal').openModal({ dismissible: false });
        registerEscapeKey($('#delete-modal'));
    }
}

/**
 * Opens list page customize modal
 */
function openCustomizeModal() {
    var customizeTemplate = _.template($('script.customize-template').html());
    $('#customize-container').empty().append(customizeTemplate(tableSettings));
    $('#customize-list').openModal({ dismissible: false });
    registerEscapeKey($('#customize-list'));
}


/**
 * Deletes an object.
 *
 * @param onSuccess - a url or function to be followed/executed on success
 * @param deleteUrl - the delete url of object being deleted
 * @param args - array list of arguments to call the onSuccess function
 */
function deleteObject(onSuccess, deleteUrl, args) {
    $('.ui.modal .ui.dimmer').addClass('active');
    var _args = args || [];
    var data = $("#delete-form").serialize();
    var onDone = function (postResult) {
        if (postResult.success) {
            if (typeof onSuccess === 'string') {
                window.location.href = onSuccess;
            } else {
                _args.push(postResult.value);
                onSuccess.apply(null, args);
                $('.ui.modal').modal('hide');
            }
        } else {
            $('.ui.modal .ui.dimmer').removeClass('active');
            $.each(postResult.postErrors, function (index, postError) {
                if (postError.parameter) {
                    showParameterError($('#delete-form'), postError);
                } else {
                    showGeneralError($('.ui.modal .ui.message .content'), postError.message);
                }
            });
        }
    };
    var onFail = function (jqXHR) {
        showSystemError(jqXHR);
    }
    Utils.fetch({ method: 'post', url: deleteUrl, data: data, onDone: onDone, onFail: onFail });
}

function showSystemError(jqXHR) {
    if (!jqXHR.status) {
        return;
    }
    if (jqXHR.status === 401) {
        window.location.reload();
    } else {
        window.location.href = "/error?statusCode=" + jqXHR.status;
    }
}

/**
 * Utility method to append error messages to error message container. Caller is in charge of clearing the error message
 * container.
 * @param generalErrorDiv, error message container
 * @param message, the error message
 */
function showGeneralError(generalErrorDiv, message) {
    generalErrorDiv.append('<span class="red-text invalid">' + message + '</span><br/>');
}

function showParameterError(form, postError) {
    var parameterErrorSpan = form.find('#' + postError.parameter + '-error');
    if (parameterErrorSpan) {
        if ($('.ui.modal').length > 0 || $('#edit-form').length > 0) {
            parameterErrorSpan
                .addClass('ui red basic left pointing label')
                .html(postError.message)
                .closest('.field').addClass('error');
        } else {
            parameterErrorSpan.html(' <span class="red-text invalid">' + postError.message + '</span>');
        }
    }
}

/**
 * Checks that the user is authenticated before proceeding to execute a request
 *
 * @param request, the request function to be called
 * @param args, the arguments to pass to the request function (should be an array)
 */
var handleAuthenticatedRequest = (function () {
    /**
     * Total idle time in minutes that has to elapse for client to ping server to check if
     * session is still active
     * @type {number}
     */
    var MAX_IDLE_TIME = 30;

    /**
     * Amount of time (in minutes) page has been idle
     * @type {number}
     */
    var idleTime = 0;

    setInterval(incrementIdleTime, 60000);

    function incrementIdleTime() {
        idleTime += 1;
    }

    return function (request, args) {
        if (idleTime >= MAX_IDLE_TIME) {
            var onDone = function (status) {
                if (status) {
                    request.apply(null, args);
                } else {
                    window.location.reload();
                }
            };
            Utils.fetch({ url: contextPath + '/loggedIn', onDone: onDone, onFail: showSystemError });
        } else {
            request.apply(null, args);
        }
        idleTime = 0;
    }
})();

/**
 * Sends a request to update an object
 * @param formData json string of form data to be posted
 * @param postUrl the url to post the data to
 * @param onSuccess function to execute on successful update
 * @param onError function to execute on error
 */
function editObject(formData, postUrl, onSuccess, onError) {
    $('.ui.modal .ui.dimmer').addClass('active');
    var onDone = function (postResult) {
        if (postResult.success) {
            $('.ui.modal').off('click', '.edit-submit');
            $('.ui.modal').modal('hide');
            onSuccess(postResult);
        } else {
            $('.ui.modal .ui.dimmer').removeClass('active');
            $('.field.error').removeClass('error');
            $('label span[id$="-error"]').removeAttr('class').empty();
            $.each(postResult.postErrors, function (index, postError) {
                showParameterError($('#edit-form'), postError);
            });
            if (onError) {
                onError(postResult.postErrors);
            }
        }
    };
    var onFail = function (jqXHR) {
        showSystemError(jqXHR);
    }
    Utils.fetch({ method: 'post', url: postUrl, contentType: 'application/json', data: formData, onDone: onDone, onFail: onFail });
}

/**
 * Converts an object to query string
 * @param obj the object to convert
 * @returns {string}
 */
function toQueryString(obj) {
    var parts = [];
    for (var prop in obj) {
        if (obj[prop]) {
            if (obj.hasOwnProperty(prop) && obj[prop] instanceof Array) {
                for (var index = 0; index < obj[prop].length; index++) {
                    parts.push(encodeURIComponent(prop + "[]") + "=" + encodeURIComponent(obj[prop][index]));
                }
            } else if (obj.hasOwnProperty(prop) && obj[prop] instanceof Object) {
                parts.push(encodeURIComponent(prop) + "." + toQueryString(obj[prop]));
            } else if (obj.hasOwnProperty(prop)) {
                parts.push(encodeURIComponent(prop) + "=" + encodeURIComponent(obj[prop]));
            }
        }
    }
    return parts.join("&");
}

/**
 * Submits resource ids to be sorted in the order they appear
 * @param url, url that processes the sorting
 * @param data, data including an array or sorted resource ids to be used for sorting
 * @param onFail, function to call when a server error occurs i.e. reverting the order of the resources
 */
function submitSortOrder(url, data, onFail) {
    $.ajax({
        type: 'POST',
        beforeSend: function (request) {
            request.setRequestHeader(header, token);
        },
        dataType: "json",
        url: url,
        data: data
    }).done(function (postResult) {
        if (!postResult.success) {
            Materialize.toast("Illegal position!", 3000);
            onFail.call(null);
        }
    }).fail(function (jqXHR) {
        showSystemError(jqXHR);
        onFail.call(null);
    });
}

$(document).on('ready', function () {
    // Intercepts any form submission made when a user presses 'Enter' key when filling in a modal form.
    // Assumption: modal is submitted by pressing an element with 'edit-submit' class
    $(document).on('submit', '#edit-form', function (e) {
        if ($('.ui.modal').is(':visible')) {
            e.preventDefault();
            $('.edit-submit').click();
        }
    });
    // Closes any open modal when a user presses the escape key
    $(document).on('keyup', function (event) {
        var stick = $('.ui.modal').data('stick') || false;
        if ($('.ui.modal').is(':visible') && !stick && event.keyCode === 27) {
            $('.ui.modal').modal('hide');
        }
    });
})

var Utils = {
    fetch: function (settings) {
        /**
         * Handles communications with the server. Expects a settings object with the following parameters
         *  - method, the HTTP method. Default is 'get'
         *  - url, the url to call this is required and has to be provided by the caller
         *  - data, the message being sent to the server could be a form encoded string or a JSON object
         *  - contentType, this specifies the data above being sent. Defaults is form encoded string
         *  - dataType, the type of data the server should send back. Defaults is JSON
         *  - onSuccess, a function to be called when the server sends back a message indicating that the operation was a success. This is done by checking the status of the message sent by the server not the HTTP status code
         *  - onError, a function to be called when the server sends back a message indicating that the operation fails, checks status same as onSuccess
         *  - onDone, a function to be called when the server responds with a HTTP 200 status code. The caller is responsible for checking where the operation was a success
         *  - onFail, a function to be called when the server responds with an error HTTP status code
         *  NOTE:
         *  The caller can specify onSuccess and onError or onDone. When all are specified only onDone is called.
         */
        var method = settings.method || 'get';
        var url = settings.url;
        var data = settings.data || null;
        var enctype = settings.enctype;
        var contentType = settings.contentType;
        var processData = settings.processData;
        var dataType = settings.dataType || 'json';
        var onSuccess = settings.onSuccess || null;
        var onError = settings.onError || null;
        var onDone = settings.onDone || null;
        var onFail = settings.onFail || function () { };
        var ajaxSettings = {
            type: method,
            beforeSend: function (request) {
                request.setRequestHeader(header, token);
            },
            dataType: dataType,
            url: url,
            data: data
        };
        enctype !== null ? ajaxSettings.enctype = enctype : '';
        contentType !== null ? ajaxSettings.contentType = contentType : '';
        processData !== null ? ajaxSettings.processData = processData : '';
        $.ajax(ajaxSettings).done(function (postResult) {
            if (onDone) {
                onDone(postResult)
            } else if (onSuccess) {
                if (postResult.success) {
                    onSuccess(postResult.value);
                } else {
                    if (postResult.postErrors) {
                        onError(postResult.postErrors);
                    } else {
                        onError(postResult.value);
                    }
                }
            }
        }).fail(function (jqXHR) {
            onFail(jqXHR);
        });
    },
    renderMessage: function(message) {
        /*
         * message is an object with info, warn or error property.
         * The info and warn properties are string with or without html element.
         * The error property should be an object with the following properties:
         *   1. title, a summary of the error message
         *   2. details, an array containing a list of objects with the property message.
         * An example of error message is:
         * {
         *   error: {
         *     title: 'The following errors occurred while deploying a project',
         *     details: [
         *       { message: 'Project has no forms' }
         *     ]
         *   }
         * }
         */

        if (message.info) {
            $('.ui.message .content')
                .html('<p>' + message.info + '</p>')
                .closest('.ui.message')
                .attr('class', 'ui green message transition hidden')
                .transition('fly up');
        }

        if (message.warn) {
            $('.ui.message .content')
                .html('<p>' + message.warn + '</p>')
                .closest('.ui.message')
                .attr('class', 'ui warning message transition hidden')
                .transition('fly up');
        }

        if (message.error) {
            $('.ui.message .content')
                .empty()
                .append('<div>' + message.error.title + '</div>')
                .append('<ul class="list"></ul>')
                .closest('.ui.message')
                .attr('class', 'ui red message transition hidden')
            if (message.error.details) {
                $.each(message.error.details, function (index, postError) {
                    $('.ui.message .list').append('<li>' + postError.message + '</li>');
                });
            }
            $('.ui.message').transition('fly up');
        }

        $('.ui.message .close')
            .off('click')
            .on('click', function () {
                $(this).closest('.ui.message').transition('fly up');
            });
    }
}