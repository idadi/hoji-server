package ke.co.hoji.server.validator;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Ensures that the {@link Form}s about to be published are valid for publication.
 * <p>
 * Created by geoffreywasilwa on 04/10/2016.
 */
public class FormPublicationValidator extends PublicationValidator {

    public static final int SEARCHABLE_FIELDS_LIMIT = 5;

    /**
     * The {@link Form}s about to be published.
     */
    private List<Form> forms;

    public FormPublicationValidator(List<Form> forms) {
        this.forms = new ArrayList<>(forms);
    }

    @Override
    public void validate(Object target, Errors errors) {
        List<Form> forms = (List<Form>) target;
        int index = 0;
        for (Form form : forms) {
            errors.pushNestedPath("forms[" + index++ + "]");
            if (form.isEnabled()) {
                if (form.getFields().isEmpty()) {
                    errors.rejectValue("id", "", "The form <strong>" + form.getShortName() + "</strong> is enabled " +
                        "but it has no fields");
                } else {
                    validateFormHasEnabledFields(form, errors);
                    validateFirstFieldNotMatrix(form, errors);
                    validateFirstFieldNotImage(form, errors);
                    validateLastFieldIsNotSearchable(form, errors);
                    validateSearchableFieldsConfig(form, errors);
                    validateMatrixFieldsConfig(form, errors);
                    validateImageSizeConfig(form, errors);
                    validateImageQualityConfig(form, errors);
                    validateChoiceFilters(form, errors);
                }
            }
            errors.popNestedPath();
        }
    }

    private void validateFormHasEnabledFields(Form form, Errors errors) {
        int noOfFields = form.getFields().size();
        int noOfDisabledFields = 0;
        for (Field field : form.getFields()) {
            if (!field.isEnabled()) {
                noOfDisabledFields++;
            }
        }
        if (noOfFields == noOfDisabledFields) {
            errors.rejectValue("id", "", "All fields in the form <strong>" + form.getShortName() + "</strong> are disabled");
        }
    }

    private void validateFirstFieldNotMatrix(Form form, Errors errors) {
        if (form.getFields().size() == 0) {
            return;
        }
        Field firstField = form.getFields().get(0);
        if (firstField.getType().isParent()) {
            errors.rejectValue("id", "", "The form <strong>" + form.getShortName() +
                "</strong> has a sub-form as its first field, which is not allowed.");
        }
    }

    private void validateFirstFieldNotImage(Form form, Errors errors) {
        if (form.getFields().size() == 0) {
            return;
        }
        Field firstField = form.getFields().get(0);
        if (firstField.getType().isImage()) {
            errors.rejectValue("id", "", "The form <strong>" + form.getShortName() +
                "</strong> has an image as its first field, which is not allowed.");
        }
    }

    private void validateLastFieldIsNotSearchable(Form form, Errors errors) {
        Field lastField = form.getFields().get(form.getFields().size() - 1);
        if (lastField.isSearchable()) {
            errors.rejectValue("id", "", "The last field in the form <strong>" + form.getShortName() + "</strong> is " +
                "searchable, which is not allowed.");
        }
    }

    private void validateSearchableFieldsConfig(Form form, Errors errors) {
        int searchableFieldCount = 0;
        for (Field field : form.getFields()) {
            if (field.isSearchable()) {
                searchableFieldCount++;
            }
        }
        if (searchableFieldCount > SEARCHABLE_FIELDS_LIMIT) {
            errors.rejectValue("id", "",
                "The form <strong>" + form.getShortName() + "</strong> has " + searchableFieldCount
                    + " searchable fields, which is more than the " + SEARCHABLE_FIELDS_LIMIT + " allowed.");
        }
    }

    private void validateMatrixFieldsConfig(Form form, Errors errors) {
        for (Field field : form.getFields()) {
            if (field.isMatrix()) {
                List<Field> children = field.getChildren();
                if (children == null || children.isEmpty()) {
                    errors.rejectValue("id", "",
                        "The field <strong>" + field.getShortName() + "</strong> in the form <strong>" + form.getShortName() +
                            "</strong> is a sub-form but it has no child fields.");
                }
            }
        }
    }

    private void validateImageSizeConfig(Form form, Errors errors) {
        for (Field field : form.getFields()) {
            if (field.getType().isImage()) {
                Integer maxValue = Utils.parseInteger(field.getMaxValue());
                if (maxValue != null) {
                    if (maxValue < Constants.ImageManagement.MIN_DIMENSION) {
                        errors.rejectValue("id", "",
                            "The image field <strong>" + field.getShortName() + "</strong> in the form <strong>" +
                                form.getShortName() + "</strong> has a maximum size that is below " +
                                Constants.ImageManagement.MIN_DIMENSION + " pixels. Set a higher value.");
                    } else if (maxValue > Constants.ImageManagement.MAX_DIMENSION) {
                        errors.rejectValue("id", "",
                            "The image field <strong>" + field.getShortName() + "</strong> in the form <strong>" +
                                form.getShortName() + "</strong> has a maximum size that exceeds " +
                                Constants.ImageManagement.MAX_DIMENSION + " pixels. Set a lower value.");
                    }
                }
            }
        }
    }

    private void validateImageQualityConfig(Form form, Errors errors) {
        for (Field field : form.getFields()) {
            if (field.getType().isImage()) {
                Integer minValue = Utils.parseInteger(field.getMinValue());
                if (minValue != null) {
                    if (minValue < Constants.ImageManagement.MIN_QUALITY) {
                        errors.rejectValue("id", "",
                            "The image field <strong>" + field.getShortName() + "</strong> in the form <strong>"
                                + form.getShortName() + "</strong> has a minimum quality setting that is below "
                                + Constants.ImageManagement.MIN_QUALITY + "%. Set a higher value.");
                    } else if (minValue > Constants.ImageManagement.MAX_QUALITY) {
                        errors.rejectValue("id", "",
                            "The image field <strong>" + field.getShortName() + "</strong> in the form <strong>" +
                                form.getShortName() + "</strong> has a minimum quality setting that is above " +
                                Constants.ImageManagement.MAX_QUALITY + "%. Set a lower value.");
                    }
                }
            }
        }
    }

    private void validateChoiceFilters(Form form, Errors errors) {
        List<Field> fields = form.getFields();
        Collections.sort(fields);
        List<Choice> traversedChoices = new ArrayList<>();
        for (Field field : fields) {
            if (field.isEnabled() && field.getChoiceGroup() != null && field.getChoiceGroup().getChoices() != null) {
                for (Choice choice : field.getChoiceGroup().getChoices()) {
                    if (choice.getParent() != null) {
                        if (!traversedChoices.contains(choice.getParent())) {
                            errors.rejectValue("id", "", "The choice <strong>" + choice.getShortName() +
                                "</strong> in the field <strong>" + field.getShortName() + "</strong> in the form " +
                                "<strong>" + form.getShortName() + "</strong> has a parent <strong>" + choice.getParent().getShortName()
                                + "</strong> that cannot be found in any field before it.");
                            break;
                        }
                    }
                }
                traversedChoices.addAll(field.getChoiceGroup().getChoices());
            }
        }
    }

    @Override
    public Object getTarget() {
        return forms;
    }
}
