package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.ChoiceGroupChoice;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Validates a grouping of choices. Ensures that the group actually contains a choice i.e. Not null
 * <p>
 * Created by geoffreywasilwa on 04/10/2016.
 */

@Component
public class ChoiceGroupChoiceValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return ChoiceGroupChoice.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ChoiceGroupChoice choiceGroupChoice = (ChoiceGroupChoice) target;
        if (choiceGroupChoice.getChoice() == null) {
            ValidationHelper.reject("choice", errors, "Select a choice");
        }
    }
}
