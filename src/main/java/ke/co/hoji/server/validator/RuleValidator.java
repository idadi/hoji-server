package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.model.Rule;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by geoffreywasilwa on 04/10/2016.
 */
@Component
public class RuleValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Rule.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Rule rule = (Rule)target;
        if (rule.getTarget() == null) {
            ValidationHelper.reject("target", errors, "This field is required.");
        }
        if (rule.getOperatorDescriptor() == null) {
            ValidationHelper.reject("operatorDescriptor", errors, "Specify the operation.");
        }
    }
}
