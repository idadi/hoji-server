package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by gitahi on 31/10/15.
 */
@Component
public class FieldValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Field.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Field field = (Field) target;
        if (field.getDescription() == null || StringUtils.isBlank(field.getDescription())) {
            ValidationHelper.reject("description", errors, "Enter field text");
        }
        if (field.getType() == null) {
            ValidationHelper.reject("type", errors, "Select field type");
        } else if (field.getType().isChoice()) {
            ChoiceGroup choiceGroup = field.getChoiceGroup();
            if (choiceGroup == null) {
                ValidationHelper.reject("choiceGroup", errors, "Select choice group");
            }
        } else if (field.getType().isParent()) {
            if (field.getParent() != null) {
                ValidationHelper.reject("parent", errors, "Matrix field cannot have a parent");
            }
        } else {
            if (field.getType().getCode().equals(FieldType.Code.REFERENCE)) {
                ValidationHelper.reject("type", errors, "Field type not supported yet");
            }
        }
    }
}
