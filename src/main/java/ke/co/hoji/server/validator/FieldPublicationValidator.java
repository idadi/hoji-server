package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Rule;
import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Ensures that the {@link Field}s about to be published are valid for publication.
 * <p>
 * Created by geoffreywasilwa on 04/10/2016.
 */
public class FieldPublicationValidator extends PublicationValidator {

    private List<Field> fields;

    public FieldPublicationValidator(List<Field> fields) {
        this.fields = new ArrayList<>(fields);
    }

    @Override
    public Object getTarget() {
        return fields;
    }

    @Override
    public void validate(Object target, Errors errors) {
        List<Field> fields = (List<Field>) target;
        for (Field field : fields) {
            if (field.isEnabled()) {
                if (field.getType().isChoice()) {
                    validateChoices(field, errors);
                }
                validateRules(field, errors);
            }
        }
    }

    private void validateChoices(Field field, Errors errors) {
        if (field.getChoiceGroup() == null) {
            errors.rejectValue("id", "", "The field <strong>" + field.getShortName() + "</strong> in the " +
                "form <strong>" + field.getForm().getShortName() + "</strong> has no choice group.");
        } else {
            List<Choice> choices = field.getChoiceGroup().getChoices();
            if (choices.isEmpty()) {
                errors.rejectValue("id", "", "The choice group <strong>" + field.getChoiceGroup().getShortName()
                    + "</strong> of the field <strong>" + field.getShortName() + "</strong> in the form <strong>" +
                    field.getForm().getShortName() + "</strong> has no choices.");
            } else {
                Map<String, Choice> choiceMap = new HashMap<>();
                for (Choice choice : choices) {
                    if (choice.getCode() == null) {
                        continue;
                    }
                    if (!choiceMap.containsKey(choice.getCode())) {
                        choiceMap.put(choice.getCode(), choice);
                    } else {
                        Choice previous = choiceMap.get(choice.getCode());
                        errors.rejectValue("id", "", "The choices <strong>" + previous.getShortName() + "</strong> and <strong>" + choice.getShortName()
                            + "</strong> in the choice group <strong>" + field.getChoiceGroup().getShortName()
                            + "</strong> associated with the field <strong>" + field.getShortName() + "</strong> in the form <strong>"
                            + field.getForm().getShortName() + "</strong> share the same code <strong>" + choice.getCode() + "</strong>.");
                        return;
                    }

                }
            }
        }
    }

    private void validateRules(Field field, Errors errors) {
        if (field.hasForwardSkips()) {
            for (Rule rule : field.getForwardSkips()) {
                if (rule.getOwner().compareTo(rule.getTarget()) > 0) {
                    errors.rejectValue("id", "", "The field <strong>" + rule.getOwner().getShortName()
                        + "</strong> in the form <strong>" + field.getForm().getShortName() + "</strong> has <strong>" +
                        "if-then-show</strong> skip logic that points to a field before it.");
                }
            }
        }
        if (field.hasBackwardSkips()) {
            for (Rule rule : field.getBackwardSkips()) {
                if (rule.getOwner().compareTo(rule.getTarget()) < 0) {
                    errors.rejectValue("id", "", "The field <strong>" + rule.getOwner().getShortName()
                        + "</strong> in the form <strong>" + field.getForm().getShortName() + "</strong> has <strong>" +
                        "show-if</strong> skip logic that points to a field after it.");
                }
            }
        }
    }
}
