package ke.co.hoji.server.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ke.co.hoji.core.data.model.Form;

/**
 * Created by gitahi on 31/10/15.
 */
@Component
public class FormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Form.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Form form = (Form) target;
        if (form.getName() == null || StringUtils.isBlank(form.getName())) {
            ValidationHelper.reject("name", errors, "Enter form name");
        }
    }
}
