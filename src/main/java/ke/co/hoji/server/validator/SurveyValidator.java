package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.model.Survey;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by gitahi on 31/10/15.
 */
@Component
public class SurveyValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Survey.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Survey survey = (Survey) target;
        if (survey.getName() == null || StringUtils.isBlank(survey.getName())) {
            ValidationHelper.reject("name", errors, "Enter project name");
        }
    }
}
