package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.PublicSurveyAccess;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.core.service.model.SurveyService;
import ke.co.hoji.server.model.WebHook;
import ke.co.hoji.server.service.TenancyAccessService;
import ke.co.hoji.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.ArrayList;
import java.util.List;

@Component
public class WebHookValidator implements Validator {

    private final UserService userService;
    private final SurveyService surveyService;
    private final FormService formService;
    private final TenancyAccessService tenancyAccessService;

    @Autowired
    public WebHookValidator(
            UserService userService,
            SurveyService surveyService, FormService formService,
            TenancyAccessService tenancyAccessService
    ) {
        this.userService = userService;
        this.surveyService = surveyService;
        this.formService = formService;
        this.tenancyAccessService = tenancyAccessService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return WebHook.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        WebHook webHook = (WebHook)target;
        if (webHook.getTargetUrl() == null || !webHook.getTargetUrl().matches("https?://.+")) {
            ValidationHelper.reject("targetUrl", errors, "Enter a valid url in the format: 'http(s)://...'");
        }
        if (webHook.getForms().size() == 0) {
            ValidationHelper.reject("forms", errors, "Select at least one form");
        } else {
            List<Survey> surveys = surveyService
                    .getSurveysByTenantUserId(userService.getCurrentUser().getTenantUserId(), PublicSurveyAccess.ALL);
            List<Form> accessibleForms = new ArrayList<>();
            for (Survey survey : surveys) {
                try {
                    tenancyAccessService.check(survey, TenancyAccessService.VIEW);
                    accessibleForms.addAll(formService.getFormsBySurvey(survey));
                } catch (AccessDeniedException e) {
                    //user has no access to surveys in this form so continue
                }
            }
            if (accessibleForms.size() == 0) {
                ValidationHelper.reject("forms", errors, "Access not allow to specified forms");
            }
            for (Form form : webHook.getForms()) {
                if (!accessibleForms.contains(form)) {
                    ValidationHelper.reject("forms", errors, "Access not allow to specified forms");
                    break;
                }
            }
        }
        if (webHook.getEventTypes().size() == 0) {
            ValidationHelper.reject("eventTypes", errors, "Select at least one event");
        }
    }
}
