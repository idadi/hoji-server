package ke.co.hoji.server.validator;

import org.springframework.validation.Errors;

/**
 * Created by gitahi on 11/12/15.
 */
public class ValidationHelper {

    public static void reject(String field, Errors errors, String message) {
        errors.rejectValue(field, message, message);
    }
}
