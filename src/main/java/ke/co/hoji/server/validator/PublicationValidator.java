package ke.co.hoji.server.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ke.co.hoji.core.data.model.Survey;

/**
 * The super class for all publication validators needed to validate a {@link Survey} before publication.
 * <p>
 * Created by gitahi on 10/5/16.
 */
public abstract class PublicationValidator {

    /**
     * The object to be validated.
     *
     * @return the target of the validation.
     */
    protected abstract Object getTarget();

    /**
     * Sub-classes should implement this method to provide custom vaidation logic.
     *
     * @param target the object to be validated
     * @param errors the {@link Errors} object in which to add the errors.
     */
    protected abstract void validate(Object target, Errors errors);

    /**
     * An alternative overloaded method for validating {@link PublicationValidator}s. This is the method that should be
     * called by clients instead of the classic {@link Validator#validate(Object, Errors)} method to which it delegates.
     *
     * @param errors the {@link Errors} object that is passed to {@link Validator#validate(Object, Errors)}.
     * @return true if the validation results in errors i.e. fails and false otherwise.
     */
    public final boolean validate(Errors errors) {
        int errorsBefore = errors.getErrorCount();
        validate(getTarget(), errors);
        int errorsAfter = errors.getErrorCount();
        return errorsAfter > errorsBefore;
    }
}
