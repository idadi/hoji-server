package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.Constants;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.UserService;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by gitahi on 19/10/15.
 */
@Component
public class UserValidator implements Validator {

    private final UserService userService;

    @Autowired
    public UserValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "user.empty_username");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "user.empty_firstName");
        User user = (User) target;
        if (!EmailValidator.getInstance().isValid(user.getUsername())) {
            errors.rejectValue("username", "user.invalid_username");
        }
        if (!user.isEditMode() && userService.userExists(user.getUsername())) {
            errors.rejectValue("username", "user.username_exists");
        }
        if (user.getPassword() != null) {
            if (user.getPassword().length() < Constants.Server.MIN_PASSWORD_LENGTH) {
                errors.rejectValue("password", "user.password_too_short");
            }
            if (user.getConfirmPassword() != null && !user.getPassword().equals(user.getConfirmPassword())) {
                errors.rejectValue("confirmPassword", "user.password_mismatch");
            }
        }
    }
}
