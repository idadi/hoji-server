package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import org.springframework.validation.Errors;

/**
 * Ensures that the {@link Survey}s about to be published is valid for publication.
 * <p>
 * Created by geoffreywasilwa on 04/10/2016.
 */
public class SurveyPublicationValidator extends PublicationValidator {

    private Survey survey;

    public SurveyPublicationValidator(Survey survey) {
        this.survey = survey;
    }

    @Override
    public void validate(Object target, Errors errors) {
        Survey survey = (Survey) target;
        if (survey.getForms() == null || survey.getForms().isEmpty()) {
            errors.rejectValue("id", "", "This project has no forms");
        } else {
            int noOfForms = survey.getForms().size();
            int noOfDisabledForms = 0;
            for (Form form : survey.getForms()) {
                if (!form.isEnabled()) {
                    noOfDisabledForms++;
                }
            }
            if (noOfDisabledForms == noOfForms) {
                errors.rejectValue("id", "", "All forms in this survey are disabled");
            }
        }
    }

    @Override
    public Object getTarget() {
        return survey;
    }
}
