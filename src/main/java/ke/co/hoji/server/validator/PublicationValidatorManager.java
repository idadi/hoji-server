package ke.co.hoji.server.validator;

import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * This class registers and sequentially runs all {@link PublicationValidator} objects. PublicationValidators should be added
 * (registered) in an instance of this class in the order in which they should be ran.
 * </p>
 * Created by gitahi on 31/10/15.
 */
public class PublicationValidatorManager {

    /**
     * The ordered list of {@link PublicationValidator} to be ran.
     */
    private List<PublicationValidator> publicationValidators;

    /**
     * Add/register a {@link PublicationValidator}.
     * @param publicationValidator the validator to be registered
     * @return true if validator has been successfully registered
     */
    public boolean addPublicationValidator(PublicationValidator publicationValidator) {
        if (publicationValidators == null) {
            publicationValidators = new ArrayList<>();
        }
        return publicationValidators.add(publicationValidator);
    }

    /**
     * Sequentially run all registered {@link PublicationValidator}s.
     * @param errors container for all validation errors
     */
    public void run(Errors errors) {
        for (PublicationValidator publicationValidator : publicationValidators) {
            if (publicationValidator.validate(errors)) {
                break;
            }
        }
    }
}
