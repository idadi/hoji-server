package ke.co.hoji.server.validator;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by geoffreywasilwa on 28/02/2017.
 */
@Component
public class ChildrenSortValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Form.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Form form = (Form)o;
        for (Field field : form.getFields()) {
            if (field.isMatrix()) {
                BigDecimal minChildPosition = field.getOrdinal().add(BigDecimal.ONE);
                List<Field> children = field.getChildren();
                BigDecimal maxChildPosition = minChildPosition.add(new BigDecimal(children.size() - 1));
                for (Field child : children) {
                    if (child.getOrdinal().compareTo(minChildPosition) == -1
                            || child.getOrdinal().compareTo(maxChildPosition) == 1) {
                        ValidationHelper.reject("fields", errors,"The field " + child.getDescription()
                                + " has been placed in an illegal position");
                    }
                }
            }
        }
    }
}
