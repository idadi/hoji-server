package ke.co.hoji.server.model;

import ke.co.hoji.core.data.ConfigObject;
import ke.co.hoji.server.event.model.EventType;

/**
 * Email templates to be used while sending messages to clients
 */
public class Template extends ConfigObject {
    private EventType eventType;
    private String template;

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
}
