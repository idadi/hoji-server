package ke.co.hoji.server.model;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.HojiObject;
import ke.co.hoji.core.data.model.EventSubject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by gitahi on 02/10/15.
 */
public class User implements HojiObject, UserDetails, EventSubject {

    public static class Role implements Comparable<Role> {

        public static final Role DEVICE = new Role("DEVICE", "Enter data", 1);
        public static final Role IMPORTER = new Role("IMPORTER", "Import own records", 2);
        public static final Role SMUGGLER = new Role("SMUGGLER", "Import any records", 3);
        public static final Role VIEWER = new Role("VIEWER", "View reports", 4);
        public static final Role DOWNLOADER = new Role("DOWNLOADER", "Download data", 5);
        public static final Role CREATOR = new Role("CREATOR", "Create forms", 6);
        public static final Role DESTROYER = new Role("DESTROYER", "Delete forms", 7);
        public static final Role SUPER = new Role("SUPER", "Do anything", 8);

        private final String name;
        private final String description;
        private final Integer ordinal;

        Role(String name, String description, Integer ordinal) {
            this.name = name;
            this.description = description;
            this.ordinal = ordinal;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public Integer getOrdinal() {
            return ordinal;
        }

        public String role() {
            return "ROLE_" + getName();
        }

        @Override
        public String toString() {
            return description;
        }

        @Override
        public int compareTo(Role that) {
            return this.ordinal.compareTo(that.ordinal);
        }

        public static Role valueOf(String label) {
            switch (label) {
                case "DEVICE":
                    return DEVICE;
                case "IMPORTER":
                    return IMPORTER;
                case "SMUGGLER":
                    return SMUGGLER;
                case "VIEWER":
                    return VIEWER;
                case "DOWNLOADER":
                    return DOWNLOADER;
                case "CREATOR":
                    return CREATOR;
                case "DESTROYER":
                    return DESTROYER;
                case "SUPER":
                    return SUPER;
                default:
                    return DEVICE;
            }
        }
    }

    /**
     * Type of user.
     */
    public static class Type {

        /**
         * A prepaid user.
         */
        public static final int PREPAID = 1;

        /**
         * A postpaid user.
         */
        public static final int POSTPAID = 2;
    }

    /**
     * The slave status of a user..
     */
    public static class SlaveStatus {

        /**
         * This user is not a slave.
         */
        public static final int INACTIVE = 0;

        /**
         * This user has been requested to become a slave..
         */
        public static final int REQUESTED = 1;

        /**
         * This user is a slave.
         */
        public static final int ACTIVE = 2;

        /**
         * This user was a slave but has since been suspended from slave status by
         * the master user.
         */
        public static final int SUSPENDED = 2;
    }

    private Integer id;
    private String username;
    private String code;
    private Integer tenantUserId;
    private String password;
    private String firstName;
    private String middleName;
    private String lastName;
    private boolean enabled;

    /**
     * Whether or not the user has verified their current email address.
     */
    private boolean verified;

    /**
     * Whether or not the user is known to (has been verified by) Hoji Ltd.
     */
    private boolean suspended;
    private List<Role> roles = new ArrayList<>();
    private String telephone;
    private Date dateCreated = new Date();
    private List<Role> defaultRoles = new ArrayList<>();
    private Integer type;
    private Integer publicSurveyAccess;
    private Integer minCredits;
    private BigDecimal taxRate;
    private BigDecimal discountRate;

    /**
     * Whether or not this is a master user i.e. capable of being charged credits for slave users under him.
     */
    private boolean master;

    /**
     * The @{@link SlaveStatus} of this user.
     */
    private Integer slaveStatus;

    /**
     * The master user of this user.
     */
    private User masterUser;
    private transient String confirmPassword;
    private transient boolean editMode;

    /**
     * Whether the user has been nagged to verify their account.
     */
    private transient boolean nagged;

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTenantUserId() {
        if (tenantUserId == null || tenantUserId == 0) {
            return id;
        }
        return tenantUserId;
    }

    public void setTenantUserId(Integer tenantUserId) {
        this.tenantUserId = tenantUserId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public List<String> getRoleStrings() {
        return getRoleStrings(false);
    }

    public List<String> getRoleStrings(boolean defaultOnes) {
        List<String> roleStrings = new ArrayList<>();
        List<Role> roleList = defaultOnes ? defaultRoles : roles;
        for (Role role : roleList) {
            roleStrings.add(role.getName());
        }
        return roleStrings;
    }

    public void setRoles(List<Role> roles) {
        Collections.sort(roles);
        this.roles = roles;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Concatenates {@link #firstName}, {@link #middleName} and {@link #lastName} into a full name.
     *
     * @return the full name.
     */
    public String getFullName() {
        String fullName = firstName;
        if (middleName != null && !StringUtils.isBlank(middleName)) {
            fullName += (" " + middleName);
        }
        if (lastName != null && !StringUtils.isBlank(lastName)) {
            fullName += (" " + lastName);
        }
        return fullName;
    }

    /**
     * Parses a full name (by the space character) for the constituent components {@link #firstName},
     * {@link #middleName} and {@link #lastName}.
     * <p>
     * If only 1 name is present, it is assigned to first name. If only 2 names are present, the first one is assigned
     * to first name and the second to last name. If 3 or more are present, the first one is assigned to first name,
     * the last one is assigned to last name and all the rest to middle name.
     *
     * @param fullNameName the full name
     */
    public void setFullName(String fullNameName) {
        List<String> tokens = Utils.tokenizeString(fullNameName, " ", true);
        if (tokens.size() > 0) {
            firstName = tokens.get(0);
            if (tokens.size() == 2) {
                lastName = tokens.get(1);
            } else if (tokens.size() > 2) {
                middleName = tokens.get(1);
                for (int i = 2; i <= tokens.size() - 2; i++) {
                    middleName += (" " + tokens.get(i));
                }
                lastName = tokens.get(tokens.size() - 1);
            }
        }
    }

    public String getEmail() {
        return username;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public List<Role> getDefaultRoles() {
        return defaultRoles;
    }

    public void setDefaultRoles(List<Role> defaultRoles) {
        Collections.sort(defaultRoles);
        this.defaultRoles = defaultRoles;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPublicSurveyAccess() {
        return publicSurveyAccess;
    }

    public void setPublicSurveyAccess(Integer publicSurveyAccess) {
        this.publicSurveyAccess = publicSurveyAccess;
    }

    public Integer getMinCredits() {
        return minCredits;
    }

    public void setMinCredits(Integer minCredits) {
        this.minCredits = minCredits;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(BigDecimal taxRate) {
        this.taxRate = taxRate;
    }

    public BigDecimal getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(BigDecimal discountRate) {
        this.discountRate = discountRate;
    }

    public boolean isMaster() {
        return master;
    }

    public void setMaster(boolean master) {
        this.master = master;
    }

    public Integer getSlaveStatus() {
        return slaveStatus;
    }

    public void setSlaveStatus(Integer slaveStatus) {
        this.slaveStatus = slaveStatus;
    }

    public User getMasterUser() {
        return masterUser;
    }

    public void setMasterUser(User masterUser) {
        this.masterUser = masterUser;
    }

    /**
     * @return the initial set of roles to be assigned to a new user.
     */
    public static List<Role> initialRoles() {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.DEVICE);
        roles.add(Role.IMPORTER);
        roles.add(Role.SMUGGLER);
        roles.add(Role.VIEWER);
        roles.add(Role.DOWNLOADER);
        roles.add(Role.CREATOR);
        roles.add(Role.DESTROYER);
        return roles;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public boolean isNagged() {
        return nagged;
    }

    public void setNagged(boolean nagged) {
        this.nagged = nagged;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (Role role : this.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.role()));
        }
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public Object getUniqueId() {
        return getId();
    }

    /**
     * Is this user a super user i.e. has the {@link Role#SUPER} role.
     *
     * @return true if user is super and false otherwise.
     */
    public boolean isSuper() {
        return hasRole(Role.SUPER);
    }

    /**
     * Is this user a smuggler i.e. has the {@link Role#SMUGGLER} role.
     *
     * @return true if user is super and false otherwise.
     */
    public boolean isSmuggler() {
        return hasRole(Role.SMUGGLER);
    }

    /**
     * Is this user a smuggler i.e. has the {@link Role#IMPORTER} role.
     *
     * @return true if user is super and false otherwise.
     */
    public boolean isImporter() {
        return hasRole(Role.IMPORTER);
    }

    /**
     * Checks whether this user has the role specified.
     *
     * @param role the role to check
     * @return true if the user has the role and false otherwise.
     */
    public boolean hasRole(Role role) {
        List<Role> roles = getRoles();
        return roles != null && roles.contains(role);
    }

    public boolean isSlave() {
        return slaveStatus == SlaveStatus.ACTIVE;
    }

    @Override
    public LinkedHashMap<String, Object> descriptiveProperties() {
        LinkedHashMap descriptiveProperties = new LinkedHashMap();
        descriptiveProperties.put("Email", username);
        descriptiveProperties.put("Tenant ID", tenantUserId);
        descriptiveProperties.put("Roles", roles);
        return descriptiveProperties;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (getClass() != other.getClass()) {
            return false;
        }
        User that = (User) other;
        if (this.id == null && that.id == null) {
            return true;
        }
        if (this.id != null && that.id != null) {
            return this.id.equals(that.id);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public static User newInstance(User user) {
        User copy = new User();
        copy.id = user.id;
        copy.username = user.username;
        copy.code = user.code;
        copy.tenantUserId = user.tenantUserId;
        copy.firstName = user.firstName;
        copy.middleName = user.middleName;
        copy.lastName = user.lastName;
        copy.enabled = user.enabled;
        copy.verified = user.verified;
        copy.suspended = user.suspended;
        copy.roles = new ArrayList<>(user.roles);
        copy.telephone = user.telephone;
        copy.dateCreated = new Date(user.dateCreated.getTime());
        copy.defaultRoles = new ArrayList<>(user.defaultRoles);
        copy.type = user.type;
        copy.publicSurveyAccess = user.publicSurveyAccess;
        copy.minCredits = user.minCredits;
        copy.taxRate = user.taxRate;
        copy.discountRate = user.discountRate;
        copy.master = user.master;
        copy.slaveStatus = user.slaveStatus;
        copy.masterUser = user.masterUser;
        return copy;
    }
}
