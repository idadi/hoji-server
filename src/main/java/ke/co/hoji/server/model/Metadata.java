package ke.co.hoji.server.model;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.server.service.UserService;
import org.apache.commons.lang3.StringUtils;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static ke.co.hoji.core.Utils.formatIsoDateTime;
import static ke.co.hoji.core.data.dto.DataElement.Type.DATE;
import static ke.co.hoji.core.data.dto.DataElement.Type.NUMBER;
import static ke.co.hoji.core.data.dto.DataElement.Type.STRING;
import static ke.co.hoji.server.model.Metadata.Label.ACCURACY;
import static ke.co.hoji.server.model.Metadata.Label.APP_VERSION;
import static ke.co.hoji.server.model.Metadata.Label.CREATED_BY;
import static ke.co.hoji.server.model.Metadata.Label.DATE_COMPLETED;
import static ke.co.hoji.server.model.Metadata.Label.DATE_CREATED;
import static ke.co.hoji.server.model.Metadata.Label.DATE_UPDATED;
import static ke.co.hoji.server.model.Metadata.Label.DATE_UPLOADED;
import static ke.co.hoji.server.model.Metadata.Label.DISTANCE;
import static ke.co.hoji.server.model.Metadata.Label.FORM_VERSION;
import static ke.co.hoji.server.model.Metadata.Label.GEO_TAGGED;
import static ke.co.hoji.server.model.Metadata.Label.LATITUDE;
import static ke.co.hoji.server.model.Metadata.Label.LOCATION;
import static ke.co.hoji.server.model.Metadata.Label.LONGITUDE;
import static ke.co.hoji.server.model.Metadata.Label.RECORD_UUID;
import static ke.co.hoji.server.model.Metadata.Label.RECORD_VERSION;
import static ke.co.hoji.server.model.Metadata.Label.SPEED;
import static ke.co.hoji.server.model.Metadata.Label.TIME_TO_COMPLETE;
import static ke.co.hoji.server.model.Metadata.Label.TIME_TO_UPLOAD;
import static ke.co.hoji.server.model.Metadata.Label.USER_ID;

/**
 * Additional information associated with a record such as location, date created/updated and more
 */
public class Metadata {
    private static final int MILLISECONDS_IN_MINUTE = 60000;
    public static final String ISO_DATE_TIME_FORMAT = "YYYY-MM-DD HH:mm:ss";
    private final MainRecord mainRecord;
    private final UserService userService;

    public Metadata(MainRecord mainRecord, UserService userService) {
        this.mainRecord = mainRecord;
        this.userService = userService;
    }

    public List<DataElement> getIdentifierMetadata() {
        List<DataElement> identifierMetadata = new ArrayList<>();
        identifierMetadata.addAll(getUuids());
        return identifierMetadata;
    }

    public List<DataElement> getMetadata() {
        List<DataElement> metadataElements = new ArrayList<>();
        metadataElements.add(getUserId());
        metadataElements.add(getUser());
        metadataElements.add(getDateCreated());
        metadataElements.add(getDateCompleted());
        metadataElements.add(getDateUpdated());
        metadataElements.add(getDateUploaded());
        metadataElements.add(getSpeed());
        metadataElements.add(getTimeToComplete());
        metadataElements.add(getTimeToUpload());
        metadataElements.add(getFormVersion());
        metadataElements.add(getAppVersion());
        metadataElements.add(getRecordVersion());
        metadataElements.add(getGeoTaggedStatus());
        metadataElements.add(getLocation());
        metadataElements.add(getLatitude());
        metadataElements.add(getLongitude());
        metadataElements.add(getAccuracy());
        metadataElements.add(getDistance());
        return metadataElements;
    }

    private List<DataElement> getUuids() {
        List<DataElement> uuidDataElements = Collections.singletonList(
                new DataElement(RECORD_UUID, mainRecord.getUuid(), STRING, "")
        );
        return uuidDataElements;
    }

    private DataElement getUserId() {
        String userId = mainRecord.getUserId().toString();
        return new DataElement(USER_ID, userId, NUMBER, userId);
    }

    private DataElement getUser() {
        String user = userService.findById(mainRecord.getUserId()).getFullName();
        return new DataElement(CREATED_BY, user, STRING, "");
    }

    private DataElement getDateCreated() {
        long dateCreated = mainRecord.getDateCreated();
        return new DataElement(
                DATE_CREATED, formatIsoDateTime(new Date(dateCreated)), DATE, ISO_DATE_TIME_FORMAT
        );
    }

    private DataElement getDateCompleted() {
        return new DataElement(DATE_COMPLETED, formatIsoDateTime(new Date(mainRecord.getDateCompleted())), DATE, ISO_DATE_TIME_FORMAT);
    }

    private DataElement getDateUpdated() {
        long dateUpdated = mainRecord.getDateUpdated();
        return new DataElement(
                DATE_UPDATED, formatIsoDateTime(new Date(dateUpdated)), DATE, ISO_DATE_TIME_FORMAT
        );
    }

    private DataElement getDateUploaded() {
        return new DataElement(DATE_UPLOADED, formatIsoDateTime(new Date(mainRecord.getDateUploaded())), DATE, ISO_DATE_TIME_FORMAT);
    }

    private DataElement getSpeed() {
        Double timeToComplete = calculateTimeToComplete();
        int mLiveFields = mainRecord.getLiveFields().size();
        long mxLiveFields = mainRecord.getMatrixRecords() != null
                ? mainRecord
                        .getMatrixRecords()
                        .stream()
                        .mapToLong(matrixRecord -> matrixRecord.getLiveFields().size())
                        .sum()
                : 0L;
        Double speed = Utils.roundDouble((mLiveFields + mxLiveFields) / timeToComplete);
        return new DataElement(SPEED, speed.toString(), NUMBER, "");
    }

    private DataElement getTimeToComplete() {
        return new DataElement(TIME_TO_COMPLETE, calculateTimeToComplete().toString(), NUMBER, "");
    }

    private Double calculateTimeToComplete() {
        long dateCompleted = mainRecord.getDateCompleted();
        long dateCreated = mainRecord.getDateCreated();
        return getElapsedTime(dateCreated, dateCompleted);
    }

    private DataElement getTimeToUpload() {
        long dateUploaded = mainRecord.getDateUploaded();
        long dateUpdated = mainRecord.getDateUpdated();
        return new DataElement(
                TIME_TO_UPLOAD, getElapsedTime(dateUpdated, dateUploaded).toString(), NUMBER, ""
        );
    }

    private Double getElapsedTime(long from, long to) {
        return Utils.roundDouble(((double) to - (double) from) / MILLISECONDS_IN_MINUTE);
    }

    private DataElement getFormVersion() {
        return new DataElement(FORM_VERSION, mainRecord.getFormVersion(), NUMBER, "");
    }

    private DataElement getAppVersion() {
        return new DataElement(APP_VERSION, mainRecord.getAppVersion(), STRING, "");
    }

    private DataElement getRecordVersion() {
        return new DataElement(RECORD_VERSION, String.valueOf(mainRecord.getVersion()), NUMBER, "");
    }

    private DataElement getLocation() {
        return new DataElement(LOCATION, mainRecord.getStartAddress(), STRING, "");
    }

    private DataElement getLatitude() {
        return new DataElement(LATITUDE, String.valueOf(getStartLatitude()), NUMBER, "");
    }

    private DataElement getLongitude() {
        return new DataElement(LONGITUDE, String.valueOf(getStartLongitude()), NUMBER, "");
    }

    private DataElement getGeoTaggedStatus() {
        String tagged = getStartLatitude() != 0 && !Double.valueOf(0).equals(getStartLongitude()) ? "Yes" : "No";
        return new DataElement(GEO_TAGGED, tagged, STRING, "");
    }

    private DataElement getAccuracy() {
        if (StringUtils.equals(getGeoTaggedStatus().getValue(), "No")) {
            return new DataElement(ACCURACY, "", NUMBER, getAccuracyFormat());
        } else {
            return new DataElement(ACCURACY, String.valueOf(mainRecord.getStartAccuracy()), NUMBER, getAccuracyFormat());
        }
    }

    private String getAccuracyFormat() {
        if (mainRecord.getStartAccuracy() == null) {
            return "";
        } else {
            return NumberFormat.getInstance().format(mainRecord.getStartAccuracy().intValue());
        }
    }

    private DataElement getDistance() {
        double distance = calculateDistance();
        return new DataElement(DISTANCE, String.valueOf(distance), NUMBER, getDistanceFormat());
    }

    private String getDistanceFormat() {
        return NumberFormat.getInstance().format(calculateDistance().intValue());
    }

    private Double calculateDistance() {
        Double unknown = (double) -1;
        if (getStartLatitude() == 0
                && getStartLongitude() == 0
                && getEndLatitude() == 0
                && getEndLongitude() == 0) {
            return unknown;
        }
        if (getStartLatitude() != 0
                && getStartLongitude() != 0
                && getEndLatitude() == 0
                && getEndLongitude() == 0) {
            return unknown;
        }
        if (getStartLatitude() == 0
                && getStartLongitude() == 0
                && getEndLatitude() != 0
                && getEndLongitude() != 0) {
            return unknown;
        }
        return Utils.distance(getStartLatitude(), getEndLatitude(),
                getStartLongitude(), getEndLongitude());
    }

    private double getStartLongitude() {
        return mainRecord.getStartLongitude() != null ? mainRecord.getStartLongitude() : 0;
    }

    private double getStartLatitude() {
        return mainRecord.getStartLatitude() != null ? mainRecord.getStartLatitude() : 0;
    }

    private double getEndLongitude() {
        return mainRecord.getEndLongitude() != null ? mainRecord.getEndLongitude() : 0;
    }

    private double getEndLatitude() {
        return mainRecord.getEndLatitude() != null ? mainRecord.getEndLatitude() : 0;
    }

    public static class Label {
        public static final String MAIN_RECORD_UUID = "Main Record UUID";
        public static final String MAIN_RECORD_ID = "Main Record ID";
        public static final String RECORD_UUID = "Record UUID";
        public static final String RECORD_ID = "Record ID";
        public static final String CREATED_BY = "Created By";
        public static final String USER_ID = "User ID";
        public static final String DATE_CREATED = "Date Created";
        public static final String DATE_COMPLETED = "Date Completed";
        public static final String DATE_UPDATED = "Date Updated";
        public static final String DATE_UPLOADED = "Date Uploaded";
        public static final String TIME_TO_COMPLETE = "Time to Complete (Minutes)";
        public static final String SPEED = "Speed (Fields per Minute)";
        public static final String TIME_TO_UPLOAD = "Time to Upload (Minutes)";
        public static final String RECORD_VERSION = "Record Version";
        public static final String FORM_VERSION = "Form Version";
        public static final String APP_VERSION = "App Version";
        public static final String LOCATION = "Location";
        public static final String LATITUDE = "Latitude";
        public static final String LONGITUDE = "Longitude";
        public static final String GEO_TAGGED = "Geo-tagged";
        public static final String ACCURACY = "GPS Accuracy";
        public static final String DISTANCE = "Start/End Distance (Meters)";
    }
}
