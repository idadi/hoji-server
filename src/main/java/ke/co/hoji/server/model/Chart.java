package ke.co.hoji.server.model;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.response.MultipleChoiceResponse;
import ke.co.hoji.core.response.RankingResponse;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.core.response.SingleChoiceResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.stat.Frequency;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>
 * Prepares and contains all the necessary information to plot a chart i.e graphical representation of data.
 * </p>
 * Created by gitahi on 26/11/15.
 */
public class Chart {

    /**
     * The type of data involved.
     */
    public static class Type {

        /**
         * Binary data i.e. takes only 2 values.
         */
        public static final int BINARY = 0;

        /**
         * Categorical data with counts as values.
         */
        public static final int CATEGORICAL = 1;

        /**
         * Numeric data.
         */
        public static final int NUMERIC_INTEGER = 2;

        /**
         * Numeric data.
         */
        public static final int NUMERIC_DECIMAL = 4;

        /**
         * Textual data.
         */
        public static final int TEXTUAL = 3;
    }

    /**
     * Unique key for this chart.
     */
    private Object key;

    /**
     * The ordinal position of this chart.
     */
    private BigDecimal ordinal;

    /**
     * The type of data contained in this chart.
     */
    private int type;

    /**
     * The label for the chart.
     */
    private String label;

    /**
     * The data for the chart.
     */
    private List<DataPoint> data;

    /**
     * The sample size n for the data in this chart.
     */
    private Integer sampleSize;

    /**
     * The label for the numbers in this chart, if applicable.
     */
    private String numberLabel = "Value";

    /**
     * A placeholder {@link Choice} to indicate missing data.
     */
    private Choice MISSING_CHOICE = new Choice(-1, "Missing", "M", -1, false);

    public Chart(Object key, BigDecimal ordinal, int type, String label) {
        this.key = key;
        this.ordinal = ordinal;
        this.type = type;
        this.label = label;
    }

    public Chart(Field field, List<LiveField> liveFields) {
        this.key = field.getId();
        this.label = field.getName() + ". " + field.getResolvedHeader();
        this.ordinal = field.getOrdinal();
        this.sampleSize = liveFields.size();
        if (field.getType().isChoice()) {
            this.type = Type.CATEGORICAL;
            final Frequency frequency = new Frequency();
            final AtomicBoolean hasMissingChoice = new AtomicBoolean(false);
            liveFields.forEach(liveField -> {
                Response response = liveField.getResponse();
                if (response instanceof SingleChoiceResponse) {
                    if (field.getChoiceGroup().getChoices().size() <= 2) {
                        this.type = Type.BINARY;
                    }
                    Choice choice = (Choice) response.getActualValue();
                    if (choice != null) {
                        frequency.addValue(choice.getId());
                    } else {
                        frequency.addValue(MISSING_CHOICE.getId());
                        hasMissingChoice.set(true);
                    }
                } else if (response instanceof MultipleChoiceResponse) {
                    LinkedHashMap<Choice, Boolean> choiceMap =
                        (LinkedHashMap<Choice, Boolean>) response.getActualValue();
                    if (choiceMap != null) {
                        choiceMap.keySet().forEach(choice -> {
                            Boolean checked = choiceMap.get(choice);
                            if (checked != null && checked) {
                                frequency.addValue(choice.getId());
                            }
                        });
                    } else {
                        frequency.addValue(MISSING_CHOICE.getId());
                        hasMissingChoice.set(true);
                    }
                } else if (response instanceof RankingResponse) {
                    LinkedHashMap<Choice, Integer> choiceMap =
                        (LinkedHashMap<Choice, Integer>) response.getActualValue();
                    if (choiceMap != null) {
                        choiceMap.keySet().forEach(choice -> {
                            Integer rank = choiceMap.get(choice);
                            if (rank != null && rank != 0) {
                                frequency.addValue(choice.getId());
                            }
                        });
                    } else {
                        frequency.addValue(MISSING_CHOICE.getId());
                        hasMissingChoice.set(true);
                    }
                }
            });
            List<Choice> choices = new ArrayList<>(field.getChoiceGroup().getChoices());
            if (hasMissingChoice.get()) {
                choices.add(MISSING_CHOICE);
            }
            this.data = choices
                .stream()
                .map(choice -> new DataPoint(choice.getName(), frequency.getCount(choice.getId())))
                .collect(Collectors.toList());
        } else if (field.getType().isInteger()) {
            this.type = Type.NUMERIC_INTEGER;
            this.data = liveFields
                .stream()
                .filter(liveField -> liveField.getResponse().getActualValue() != null)
                .map(liveField -> {
                    if (liveField.getResponse().displayString(true).equals("Missing")) {
                        return new DataPoint("Missing", null);
                    }
                    return new DataPoint(String.valueOf(liveField.getResponse().getActualValue()),
                        liveField.getResponse().getActualValue());
                })
                .collect(Collectors.toList());
            this.data.addAll(
                liveFields
                    .stream()
                    .filter(liveField -> liveField.getResponse().getActualValue() == null)
                    .map(liveField -> new DataPoint("Missing", null))
                    .collect(Collectors.toList())
            );
        } else if (field.getType().isDecimal()) {
            this.type = Type.NUMERIC_DECIMAL;
            this.data = liveFields
                .stream()
                .filter(liveField -> liveField.getResponse().getActualValue() != null)
                .map(liveField -> {
                    if (liveField.getResponse().displayString(true).equals("Missing")) {
                        return new DataPoint("Missing", null);
                    }
                    return new DataPoint(String.valueOf(liveField.getResponse().getActualValue()),
                        liveField.getResponse().getActualValue());
                })
                .collect(Collectors.toList());
            this.data.addAll(
                liveFields
                    .stream()
                    .filter(liveField -> liveField.getResponse().getActualValue() == null)
                    .map(liveField -> new DataPoint("Missing", null))
                    .collect(Collectors.toList())
            );
        } else if (field.getType().isTextual()) {
            this.type = Type.TEXTUAL;
            Set<String> missingValues = new HashSet<>();
            if (field.getMissingValue() != null) {
                missingValues.addAll(
                    Stream.of(field.getMissingValue().replaceAll("( )+", "").split(","))
                        .collect(Collectors.toList())
                );
            }
            this.data = liveFields
                .stream()
                .filter(liveField ->
                    liveField.getResponse().getActualValue() != null
                        && !liveField.getResponse().displayString(true).equals("Missing")
                )
                .map(liveField ->
                    new DataPoint(
                        String.valueOf(liveField.getField().getId()),
                        StringUtils.normalizeSpace(
                            liveField
                                .getResponse()
                                .getActualValue()
                                .toString()
                                .toLowerCase()
                                .replaceAll("[^A-Za-z\\- ]+", "")
                        )
                    )
                )
                .collect(Collectors.toList());
        }
    }

    public Object getKey() {
        return key;
    }

    public BigDecimal getOrdinal() {
        return ordinal;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<DataPoint> getData() {
        return data;
    }

    public void setData(List<DataPoint> data) {
        this.data = data;
    }

    public Integer getSampleSize() {
        return sampleSize;
    }

    public void setSampleSize(Integer sampleSize) {
        this.sampleSize = sampleSize;
    }

    public String getNumberLabel() {
        return numberLabel;
    }

    public void setNumberLabel(String numberLabel) {
        this.numberLabel = numberLabel;
    }

    public List<DataPoint> getSummaryStats() {
        DecimalFormat decimalFormat;
        if (type == Type.NUMERIC_INTEGER) {
            decimalFormat = new DecimalFormat("#,###,##0");
        } else if (type == Type.NUMERIC_DECIMAL) {
            decimalFormat = new DecimalFormat("#,###,##0.00");
        } else {
            return new ArrayList<>();
        }
        DescriptiveStatistics descriptiveStatistics = new DescriptiveStatistics();
        int noMissing = 0;
        for (DataPoint dataPoint : this.data) {
            Object objectValue = dataPoint.getValue();
            if (objectValue == null) {
                noMissing++;
                continue;
            } else {
                double value = 0;
                if (objectValue instanceof Double) {
                    value = (Double) dataPoint.getValue();
                } else if (objectValue instanceof BigDecimal) {
                    value = ((BigDecimal) objectValue).doubleValue();
                } else if (objectValue instanceof Integer) {
                    value = Double.valueOf((int) objectValue);
                } else if (objectValue instanceof Long) {
                    value = Double.valueOf((long) objectValue);
                }
                descriptiveStatistics.addValue(value);
            }
        }
        List<DataPoint> summaryStats = new ArrayList<>();
        summaryStats.add(new DataPoint("MEAN", Utils.roundDouble(descriptiveStatistics.getMean())));
        summaryStats.add(new DataPoint("SD", Utils.roundDouble(descriptiveStatistics.getStandardDeviation())));
        summaryStats.add(new DataPoint("MEDIAN", Utils.roundDouble(descriptiveStatistics.getPercentile(50))));
        summaryStats.add(new DataPoint("IQR", decimalFormat.format(descriptiveStatistics.getPercentile(25))
            + " - " + decimalFormat.format(descriptiveStatistics.getPercentile(75))));
        summaryStats.add(new DataPoint("SKEWNESS", Utils.roundDouble(descriptiveStatistics.getSkewness())));
        summaryStats.add(new DataPoint("KURTOSIS", Utils.roundDouble(descriptiveStatistics.getKurtosis())));
        summaryStats.add(new DataPoint("VAR", Utils.roundDouble(descriptiveStatistics.getVariance())));
        summaryStats.add(new DataPoint("RANGE", decimalFormat.format(descriptiveStatistics.getMin())
            + " - " + decimalFormat.format(descriptiveStatistics.getMax())));
        summaryStats.add(new DataPoint("SUM", Utils.roundDouble(descriptiveStatistics.getSum())));
        if (noMissing != 0) {
            summaryStats.add(new DataPoint("MISSING", noMissing));
        }
        return summaryStats;
    }
}
