package ke.co.hoji.server.model;

import ke.co.hoji.core.data.HojiObject;

import java.math.BigDecimal;

/**
 * A bundle of credits sold as a single unit.
 * <p>
 * Created by gitahi on 05/01/18.
 */
public class CreditBundle implements HojiObject {

    /**
     * The unique identifier for this CreditBundle.
     */
    private Integer id;

    /**
     * The name of this CreditBundle.
     */
    private String name;

    /**
     * The number of credits in this CreditBundle.
     */
    private Integer credits;

    /**
     * The price of this CreditBundle in USD.
     */
    private BigDecimal price;

    /**
     * The expiry period of this CreditBundle in days.
     */
    private Integer expiry;

    /**
     * The discount applicable to the price of this CreditBundle.
     */
    private Integer discount;

    /**
     * The bonus applicable to the credits in this CreditBundle.
     */
    private Integer bonus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getExpiry() {
        return expiry;
    }

    public void setExpiry(Integer expiry) {
        this.expiry = expiry;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getBonus() {
        return bonus;
    }

    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    @Override
    public Object getUniqueId() {
        return getId();
    }
}
