package ke.co.hoji.server.model;

import java.util.Set;

/**
 * Created by geoffreywasilwa on 20/05/2017.
 */
public class FieldDataPoint extends DataPoint {
    private final Integer fieldId;
    private final Set<String> uuids;

    public FieldDataPoint(Integer fieldId, Set<String> uuids, String label, Object value) {
        super(label, value);
        this.fieldId = fieldId;
        this.uuids = uuids;
    }

    public Integer getFieldId() {
        return fieldId;
    }

    public Set<String> getUuids() {
        return uuids;
    }
}
