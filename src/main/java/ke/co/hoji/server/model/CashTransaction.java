package ke.co.hoji.server.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * A transaction involving the movement of cash.
 * <p>
 * Created by gitahi on 05/01/18.
 */
public class CashTransaction extends Transaction {

    /**
     * The amount of cash involved in USD.
     */
    private final BigDecimal amountUsd;

    /**
     * The amount of cash involved in local currency.
     */
    private BigDecimal amountLcl;

    /**
     * The name of the local currency involved.
     */
    private String currency;

    /**
     * The mode of payment.
     */
    private String paymentMode;

    /**
     * The payment reference number e.g. the M-PESA code.
     */
    private String paymentReference;

    /**
     * The @{@link CreditBundle} involved.
     */
    private CreditBundle creditBundle;

    public CashTransaction(User user, Date date, String referenceNo, String type, BigDecimal amountUsd) {
        super(user, date, referenceNo, type);
        this.amountUsd = amountUsd;
    }

    public BigDecimal getAmountUsd() {
        return amountUsd;
    }

    public BigDecimal getAmountLcl() {
        return amountLcl;
    }

    public void setAmountLcl(BigDecimal amountLcl) {
        this.amountLcl = amountLcl;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    public CreditBundle getCreditBundle() {
        return creditBundle;
    }

    public void setCreditBundle(CreditBundle creditBundle) {
        this.creditBundle = creditBundle;
    }
}
