package ke.co.hoji.server.model;

import ke.co.hoji.core.data.HojiObject;

import java.util.Date;

/**
 * A cash or credit transaction reflecting the movement of cash or credits.
 * <p>
 * Created by gitahi on 06/01/18.
 */
public abstract class Transaction implements HojiObject {

    /**
     * The type of transaction.
     */
    public static class Type {

        /**
         * Indicates a deposit of cash or credits.
         */
        public static final String DEPOSIT = "D";

        /**
         * Indicates an expense of cash or credits.
         */
        public static final String EXPENSE = "E";

        /**
         * Indicates the expiry of credits.
         */
        public static final String EXPIRY = "X";

        /**
         * Indicates a credit reset for pre-paid clients with -ve credit balances.
         */
        public static final String RESET = "T";

        /**
         * Indicates a refund of cash.
         */
        public static final String REFUND = "R";

        /**
         * Indicates FREE credits deposited by Hoji into a @{@link User}'s account.
         */
        public static final String FREE = "F";
    }

    /**
     * The unique id for this Transaction.
     */
    protected Long id;

    /**
     * The @{@link User} associated with this Transaction.
     */
    protected final User user;

    /**
     * The date of this Transaction.
     */
    protected final Date date;

    /**
     * A unique, internal identifier for Transaction(s) that are executed as a single unit.
     */
    protected final String referenceNo;

    /**
     * The @{@link Type} of this Transaction.
     */
    protected final String type;

    public Transaction(User user, Date date, String referenceNo, String type) {
        this.user = user;
        this.date = date;
        this.referenceNo = referenceNo;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public Date getDate() {
        return date;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public String getType() {
        return type;
    }

    @Override
    public Object getUniqueId() {
        return getId();
    }
}
