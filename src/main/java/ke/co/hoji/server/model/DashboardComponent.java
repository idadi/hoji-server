package ke.co.hoji.server.model;

import com.fasterxml.jackson.databind.JsonNode;
import ke.co.hoji.core.data.Nameable;
import ke.co.hoji.core.data.Orderable;
import ke.co.hoji.core.data.model.Form;

import java.math.BigDecimal;

public class DashboardComponent extends Nameable implements Orderable<DashboardComponent> {

    private BigDecimal ordinal;
    private JsonNode configuration;
    private Form form;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JsonNode getConfiguration() {
        return configuration;
    }

    public void setConfiguration(JsonNode configuration) {
        this.configuration = configuration;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    @Override
    public BigDecimal getOrdinal() {
        return ordinal;
    }

    @Override
    public void setOrdinal(BigDecimal ordinal) {
        this.ordinal = ordinal;
    }

    @Override
    public int compareTo(DashboardComponent other) {
        return this.ordinal.compareTo(other.ordinal);
    }
}
