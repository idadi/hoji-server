package ke.co.hoji.server.model;

import ke.co.hoji.core.StringInterpolator;
import ke.co.hoji.core.data.Constants.Command;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.Response;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by geoffreywasilwa on 19/12/2016.
 */
public class RecordOverview {

    private final String caption;
    private final List<ResponseWrapper> responses;
    private final RecordMetadata recordMetadata;
    private final StringInterpolator interpolator = new StringInterpolator();

    public RecordOverview(Record record, RecordMetadata recordMetadata) {
        this.caption = record.getCaption();
        this.responses = record.getLiveFields()
            .stream()
            .map(liveField -> new ResponseWrapper(liveField, record))
            .collect(Collectors.toList());
        this.recordMetadata = recordMetadata;
    }

    public String getCaption() {
        return caption;
    }

    public List<ResponseWrapper> getResponses() {
        return responses;
    }

    public RecordMetadata getRecordMetadata() {
        return recordMetadata;
    }

    public class ResponseWrapper {

        private final LiveField liveField;
        private final Record record;

        ResponseWrapper(LiveField liveField, Record record) {
            this.liveField = liveField;
            this.record = record;
        }

        public String getDescription() {
        	String input = liveField.getField().getDescription();
        	input = input.replace(Command.USERNAME, liveField.getResponse().getHojiContext().getNameOfUser());
            String output = interpolator.interpolate(input, record, StringInterpolator.InterpolationType.LABEL_VALUE);
            if (isLabel()) {
                return output;
            }
            return liveField.getField().getName() + ". " + output;
        }

        public Object getValue() {
            if (isLabel()) {
                return liveField.getField().getInstructions();
            }
            if (isImage()) {
                List<DataElement> dataElements = liveField.getResponse().getValue(Form.OutputStrategy.OVERVIEW);
                if (dataElements != null && !dataElements.isEmpty()) {
                    return dataElements.get(0).getValue();
                }
            }
            return liveField.getResponse().displayString(true);
        }

        public boolean isLabel() {
            return liveField.getField().getType().isLabel();
        }

        public boolean isImage() {
            return liveField.getField().getType().isImage() &&
                liveField.getResponse().getActualValue() != null;
        }
    }
}
