package ke.co.hoji.server.model;

/**
 * <p>
 * Indicates which attributes of a {@link ke.co.hoji.core.data.model.Form} are to be bulk edited.
 * </p>
 * Created by geoffreywasilwa on 27/03/2017.
 */
public class FormsEditorConfig {
    private boolean name;
    private boolean description;
    private boolean gpsCapture;
    private boolean minGpsAccuracy;
    private boolean noOfGpsAttempts;
    private boolean outputStrategy;
    private boolean transposable;
    private boolean transpositionCategories;
    private boolean transpositionValueLabel;

    public boolean isName() {
        if (noFormAttributeConfigured()) {
            return true;
        }
        return name;
    }

    public void setName(boolean name) {
        this.name = name;
    }

    public boolean isDescription() {
        if (noFormAttributeConfigured()) {
            return true;
        }
        return description;
    }

    public void setDescription(boolean description) {
        this.description = description;
    }

    public boolean isGpsCapture() {
        return gpsCapture;
    }

    public void setGpsCapture(boolean gpsCapture) {
        this.gpsCapture = gpsCapture;
    }

    public boolean isMinGpsAccuracy() {
        return minGpsAccuracy;
    }

    public void setMinGpsAccuracy(boolean minGpsAccuracy) {
        this.minGpsAccuracy = minGpsAccuracy;
    }

    public boolean isNoOfGpsAttempts() {
        return noOfGpsAttempts;
    }

    public void setNoOfGpsAttempts(boolean noOfGpsAttempts) {
        this.noOfGpsAttempts = noOfGpsAttempts;
    }

    public boolean isOutputStrategy() {
        return outputStrategy;
    }

    public void setOutputStrategy(boolean outputStrategy) {
        this.outputStrategy = outputStrategy;
    }

    public boolean isTransposable() {
        return transposable;
    }

    public void setTransposable(boolean transposable) {
        this.transposable = transposable;
    }

    public boolean isTranspositionCategories() {
        return transpositionCategories;
    }

    public void setTranspositionCategories(boolean transpositionCategories) {
        this.transpositionCategories = transpositionCategories;
    }

    public boolean isTranspositionValueLabel() {
        return transpositionValueLabel;
    }

    public void setTranspositionValueLabel(boolean transpositionValueLabel) {
        this.transpositionValueLabel = transpositionValueLabel;
    }

    private boolean noFormAttributeConfigured() {
        return !name && !description && !gpsCapture && !minGpsAccuracy && !noOfGpsAttempts && !outputStrategy;
    }
}
