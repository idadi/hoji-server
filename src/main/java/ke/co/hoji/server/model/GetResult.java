package ke.co.hoji.server.model;

/**
 * Created by geoffreywasilwa on 08/02/2017.
 */
public class GetResult {
    /**
     * Whether or not the GET request succeeded.
     */
    private boolean success;

    /**
     * An arbitrary object returned to the client by the server. Varies depending on context and may be null.
     */
    private Object value;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
