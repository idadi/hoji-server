package ke.co.hoji.server.model;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.dto.DataValue;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.MatrixRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.response.MultipleChoiceResponse;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.server.converter.DtoToModelLiveFieldConverter;
import ke.co.hoji.server.service.UserService;
import ke.co.hoji.server.utils.ServerUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.apache.commons.lang3.StringUtils;

/**
 * Represents information for a single {@link ke.co.hoji.core.data.dto.MainRecord} in a
 * format that is "easy" to consume. {@link ke.co.hoji.core.data.dto.LiveField}
 * information is transformed into key/value pairs. And metadata is separated from
 * data recorded by clients.
 */
public class ApiRecord {

    private final Map<String, Object> data = new LinkedHashMap<>();
    private final Metadata metadata;

    public ApiRecord(Metadata metadata) {
        this.metadata = metadata;
    }

    public void addData(String key, Object value) {
        data.put(key, value);
    }

    public Map<String, Object> getData() {
        return data;
    }

    public Map<String, String> getMetadata() {
        Map<String, String> data = new LinkedHashMap<>();
        if (metadata == null) {
            return data;
        }
        for (DataElement dataElement : metadata.getIdentifierMetadata()) {
            data.put(dataElement.getLabel(), dataElement.getValue());
        }
        for (DataElement dataElement : metadata.getMetadata()) {
            data.put(dataElement.getLabel(), dataElement.getValue());
        }
        return data;
    }

    public static ApiRecord fromMainRecord(
            Form form, MainRecord mainRecord, UserService userService, HojiContext hojiContext
    ) {
        ApiRecord apiRecord = new ApiRecord(new Metadata(mainRecord, userService));
        Set<Integer> processedFieldIds = new HashSet<>();
        for (Field field : form.getFields()) {
            if (!field.isEnabled() || field.getType().isLabel() || processedFieldIds.contains(field.getId())) {
                continue;
            }
            LiveField liveField =
                    ServerUtils.findOne(field, mainRecord.getLiveFields(), new ServerUtils.LiveFieldSearchPredicate());
            liveField = liveField != null ? liveField : new LiveField();
            String key = StringUtils.isNotBlank(field.getColumn()) ? field.getColumn() : field.getId().toString();
            if (field.getType().isReference()) {
                apiRecord.addData(key, liveField.getValue());
            } else if (field.isMatrix()) {
                apiRecord.addData(key, getMatrixData(field, mainRecord, processedFieldIds, hojiContext));
            } else {
                apiRecord.addData(key, getValue(field, liveField, hojiContext));
            }
            processedFieldIds.add(field.getId());
        }
        return apiRecord;
    }

    private static List<Map<String, Object>> getMatrixData(
            FieldParent fieldParent, MainRecord mainRecord, Set<Integer> processedFieldIds, HojiContext hojiContext
    ) {
        List<Map<String, Object>> matrixData = new ArrayList<>();
        List<MatrixRecord> matrixRecords =
                ServerUtils.findMultiple(fieldParent, mainRecord.getMatrixRecords(), new ServerUtils.MatrixRecordSearchPredicate());
        for (MatrixRecord matrixRecord : matrixRecords) {
            Map<String, Object> row = new HashMap<>();
            for (Field field : fieldParent.getChildren()) {
                LiveField liveField =
                        ServerUtils.findOne(field, matrixRecord.getLiveFields(), new ServerUtils.LiveFieldSearchPredicate());
                liveField = liveField != null ? liveField : new LiveField();
                String key = field.getColumn() != null ? field.getColumn() : field.getId().toString();
                if (field.getType().isReference()) {
                    row.put(key, liveField.getValue());
                } else {
                    row.put(key, getValue(field, liveField, hojiContext));
                }
                processedFieldIds.add(field.getId());
            }
            matrixData.add(row);
        }
        return matrixData;
    }

    private static Object getValue(Field field, LiveField liveField, HojiContext hojiContext) {
        DtoToModelLiveFieldConverter liveFieldConverter =
                new DtoToModelLiveFieldConverter(hojiContext, field);
        Response response = liveFieldConverter.convert(liveField).getResponse();
        List<DataElement> dataElements = response.getValue(Form.OutputStrategy.API);
        if (field.getType().isMultiChoice()) {
            Map<String, Object> value = new HashMap<>();
            Map<Choice, Boolean> choiceMap = ((MultipleChoiceResponse) response).getActualValue();
            if (choiceMap != null) {
                List<String> selectedChoices = new ArrayList<>();
                for (Choice choice : choiceMap.keySet()) {
                    Boolean state = choiceMap.get(choice);
                    if (state != null && state) {
                        selectedChoices.add(choice.getName());
                    }
                }
                String type = dataElements.get(0).getType();
                value.put("value", selectedChoices.toArray(new String[0]));
                value.put("type", type);
            }
            return value;
        } else if (field.getType().isImage()) {
            return new DataValue(dataElements.get(0).getValue(), "image");
        } else {
            return new DataValue(dataElements.get(0).getValue(), dataElements.get(0).getType());
        }
    }

}
