package ke.co.hoji.server.model;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * Created by geoffreywasilwa on 22/12/2016.
 */
public class RecordMetadata {
    private String uuid;
    private boolean test;
    private String createdBy;
    private Long dateCreated;
    private String address;
    private Double latitude;
    private Double longitude;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateCreated() {
        return new Date(dateCreated);
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getLocation() {
        if (StringUtils.isNotBlank(address)) {
            return address;
        } else {
            return latitude + ", " + longitude;
        }
    }

    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }
}
