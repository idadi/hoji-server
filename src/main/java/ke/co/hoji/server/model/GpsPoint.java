package ke.co.hoji.server.model;

/**
 * Created by gitahi on 02/12/15.
 */
public class GpsPoint {

    private final String uuid;
    private final double latitude;
    private final double longitude;
    private final String address;
    private final double accuracy;
    private final int age;
    private final String user;
    private final String deviceId;
    private final int version;
    private String caption;
    private final String dateCreated;
    private final String dateCompleted;
    private final String dateUpdated;
    private final String dateUploaded;

    public GpsPoint(String uuid, double latitude, double longitude, String address, double accuracy, int age,
            String user, String deviceId, int version, String caption, String dateCreated, String dateCompleted,
            String dateUpdated, String dateUploaded) {
        this.uuid = uuid;
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        this.accuracy = accuracy;
        this.age = age;
        this.user = user;
        this.deviceId = deviceId;
        this.version = version;
        this.caption = caption;
        this.dateCreated = dateCreated;
        this.dateCompleted = dateCompleted;
        this.dateUpdated = dateUpdated;
        this.dateUploaded = dateUploaded;
    }

    public String getUuid() {
        return uuid;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getAddress() {
        return address;
    }

    public int getVersion() {
        return version;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public int getAge() {
        return age;
    }

    public String getUser() {
        return user;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getCaption() {
        return caption;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public String getDateCompleted() {
        return dateCompleted;
    }

    public String getDateUpdated() {
        return dateUpdated;
    }

    public String getDateUploaded() {
        return dateUploaded;
    }

    /**
     * Work around to allow marking a caption as encrypted. 
     * A caption obtained from an encrypted {@link ke.co.hoji.core.data.model.Field} is saved
     * as plain text.
     */
    public void setCaption(String caption) {
        this.caption = caption;
    }
}
