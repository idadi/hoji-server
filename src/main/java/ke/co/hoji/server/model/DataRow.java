package ke.co.hoji.server.model;

import ke.co.hoji.core.data.dto.DataElement;

import java.util.List;

/**
 * <p>
 * A collection of {@link DataElement} to represent a single record for a given {@link ke.co.hoji.core.data.model.Form}
 * </p>
 * Created by geoffreywasilwa on 18/04/2017.
 */
public class DataRow {
    private final List<DataElement> dataElements;

    public DataRow(List<DataElement> dataElements) {
        this.dataElements = dataElements;
    }

    public List<DataElement> getDataElements() {
        return dataElements;
    }
}
