package ke.co.hoji.server.model;

import java.util.List;

/**
 * The result of a HTTP POST request.
 * <p>
 * Created by gitahi on 1/9/17.
 */
public class PostResult {

    /**
     * Whether or not the POST request succeeded. If it failed, expect some {@link #postErrors}.
     */
    private boolean success;

    /**
     * An arbitrary object returned to the client by the server. Varies depending on context and may be null.
     */
    private Object value;

    /**
     * A list of @{@link PostError}s returned if the server fails to honor the request due to invalid data.
     */
    private List<PostError> postErrors;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public List<PostError> getPostErrors() {
        return postErrors;
    }

    public void setPostErrors(List<PostError> postErrors) {
        this.postErrors = postErrors;
    }
}
