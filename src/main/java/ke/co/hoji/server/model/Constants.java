package ke.co.hoji.server.model;

/**
 * Constants used only on the server.
 * <p>
 * Created by gitahi on 25/06/16.
 */
public class Constants {

    public static class DataValueType {

        public static final String CODES = "codes";
        public static final String LABELS = "labels";
    }
}
