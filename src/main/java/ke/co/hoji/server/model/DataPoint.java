package ke.co.hoji.server.model;

/**
 * Created by gitahi on 03/12/15.
 */
public class DataPoint implements Comparable<DataPoint> {

    private final String label;
    private final Object value;

    public DataPoint(String label, Object value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public int compareTo(DataPoint that) {
        if (this.value != null && that.value == null) {
            return 1;
        } else if (this.value == null && that.value != null) {
            return -1;
        } else {
            if (this.value instanceof Comparable && that.value instanceof Comparable) {
                return ((Comparable) this.value).compareTo(that.value);
            } else {
                return 0;
            }
        }
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result
                + (this.label == null ? 0 : this.label.hashCode())
                + (this.value == null ? 0 : this.value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof DataPoint)) {
            return false;
        }
        DataPoint o = (DataPoint) other;
        return (this.label == null ? o.label == null : this.label.equals(o.label))
                && (this.value == null ? o.value == null : this.value.equals(o.value));
    }

    @Override
    public String toString() {
        return String.format("%s: %s", label, value);
    }
}
