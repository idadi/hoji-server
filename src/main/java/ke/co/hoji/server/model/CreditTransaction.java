package ke.co.hoji.server.model;

import ke.co.hoji.core.data.model.Form;

import java.util.Date;

/**
 * <p>A transaction involving the movement of credits.</p>
 *
 * <p>Created by gitahi on 16/04/18.</p>
 */
public class CreditTransaction extends Transaction {

    /**
     * The @{@link Form} associated with this CreditTransaction, if any.
     */
    private Form form;

    /**
     * The number of credits purchased (+ve) or expensed (-ve) in this CreditTransaction.
     */
    private Integer credits;

    /**
     * The expiry period of the @{@link CreditBundle} purchased.
     */
    private Integer expiry;

    public CreditTransaction(User user, Date date, String referenceNo, String type) {
        super(user, date, referenceNo, type);
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Integer getExpiry() {
        return expiry;
    }

    public void setExpiry(Integer expiry) {
        this.expiry = expiry;
    }
}
