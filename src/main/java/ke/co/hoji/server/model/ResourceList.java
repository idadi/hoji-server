package ke.co.hoji.server.model;

import ke.co.hoji.core.data.ConfigObject;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * A model object for sending a list of resources back and forth between the view and the controller.
 * </p>
 * Created by gitahi on 19/03/16.
 */
public abstract class ResourceList<T extends ConfigObject> {

    /**
     * The list of resources.
     */
    protected final List<T> resources;
    transient protected Map<Integer, T> resourceStore;

    public ResourceList(List<T> resources) {
        this.resources = resources;
    }

    public List<T> getResources() {
        return resources;
    }

    public T getResource(Integer resourceId) {
        if (resourceId == null) {
            return null;
        }
        if (resourceStore == null) {
            resourceStore = new LinkedHashMap<>();
            for (T resource : resources) {
                resourceStore.put(resource.getId(), resource);
            }
        }
        return resourceStore.get(resourceId);
    }
}
