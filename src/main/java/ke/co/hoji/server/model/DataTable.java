package ke.co.hoji.server.model;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.Form;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * A representation of tabular data.
 */
public class DataTable {
    /**
     * The default label to use for the transposed column if non is specified at {@link Form#transpositionValueLabel}.
     */
    private final static String DEFAULT_TRANSPOSITION_VALUE_LABEL = "Value";

    /**
     * The default category to use for the transposed categories if non is specified at {@link Form#transpositionCategories}.
     */
    private final static String DEFAULT_TRANSPOSITION_CATEGORY = "Category";

    /**
     * The {@link DataRow}s in the table.
     */
    private final List<DataRow> dataRows;

    /**
     * The column to render by default for this table. Applies to pivot table output.
     */
    private final String defaultColumn;

    /**
     * True if the data in this table has been transposed and false otherwise.
     */
    private final boolean transposed;

    public DataTable(List<DataRow> dataRows) {
        this(dataRows, null);
    }

    public DataTable(List<DataRow> dataRows, String defaultColumn) {
        this(dataRows, defaultColumn, false);
    }

    public DataTable(List<DataRow> dataRows, String defaultColumn, boolean transposed) {
        this.dataRows = dataRows;
        this.defaultColumn = defaultColumn;
        this.transposed = transposed;
    }

    public List<DataRow> getDataRows() {
        return dataRows;
    }

    public String getDefaultColumn() {
        return defaultColumn;
    }

    public boolean isTransposed() {
        return transposed;
    }

    /**
     * This method attempts to requestTranspose the data and return the transposed format of it.
     * The transposition is not guaranteed to succeed. Transposition may fail if the value labels are not set correctly,
     * e.g. the separating pipe characters do not match up i.e. they are not the same number across the value labels. If
     * transposition fails, the regular/raw/un-transposed data table is returned.
     *
     * @param form the {@link Form} whose data is to be acted upon
     * @return the data table, either raw or transposed.
     */
    public DataTable requestTranspose(Form form) {
        List<TransposableDataRow> transposableDataRows = new ArrayList<>();
        for (DataRow dataRow : getDataRows()) {
            TransposableDataRow transposableDataRow = new TransposableDataRow();
            boolean left = true;
            boolean onFirstPipeEncounter = true;
            int initialNoOfCategories = -1;
            for (DataElement dataElement : dataRow.getDataElements()) {
                int noOfCategories = 0;
                if (dataElement.getLabel().contains("|")) {
                    List<String> categories = Utils.tokenizeString(dataElement.getLabel(), "\\|", true, true);
                    noOfCategories = categories.size();
                }
                if (noOfCategories > 0) {
                    if (onFirstPipeEncounter) {
                        initialNoOfCategories = noOfCategories;
                        onFirstPipeEncounter = false;
                    }
                    if (noOfCategories == initialNoOfCategories) {
                        left = false;
                        transposableDataRow.middleDataElements.add(dataElement);
                    } else {
                        return this;
                    }
                } else {
                    if (left) {
                        transposableDataRow.leftDataElements.add(dataElement);
                    } else {
                        transposableDataRow.rightDataElements.add(dataElement);
                    }
                }
            }
            transposableDataRows.add(transposableDataRow);
        }
        return createTransposedDataTable(transposableDataRows, form, getDefaultColumn());
    }

    private DataTable createTransposedDataTable(List<TransposableDataRow> transposableDataRows, Form form, String defaultColumn) {
        List<DataRow> dataRows = new ArrayList<>();
        for (TransposableDataRow transposableDataRow : transposableDataRows) {
            for (DataElement middleDataElement : transposableDataRow.middleDataElements) {
                List<DataElement> dataElements = new ArrayList<>();
                dataElements.addAll(transposableDataRow.leftDataElements);
                {
                    String tokens[] = middleDataElement.getLabel().split("\\|");
                    int i = 0;
                    for (String token : tokens) {
                        DataElement dataElement = new DataElement(
                                getCategoryName(i, form),
                                token,
                                DataElement.Type.STRING,
                                ""
                        );
                        dataElements.add(dataElement);
                        i++;
                    }
                    DataElement dataElement = new DataElement(
                            StringUtils.isNotBlank(form.getTranspositionValueLabel())
                                    ? form.getTranspositionValueLabel() : DEFAULT_TRANSPOSITION_VALUE_LABEL,
                            middleDataElement.getValue(),
                            middleDataElement.getType(),
                            middleDataElement.getFormat()
                    );
                    dataElements.add(dataElement);
                }
                dataElements.addAll(transposableDataRow.rightDataElements);
                DataRow dataRow = new DataRow(dataElements);
                dataRows.add(dataRow);

            }
        }
        return new DataTable(dataRows, defaultColumn, true);
    }

    private String getCategoryName(int index, Form form) {
        if (form.getTranspositionCategories() != null) {
            List<String> categoryNameTokens = Utils.tokenizeString(form.getTranspositionCategories(), ",", true, true);
            return categoryNameTokens.get(index);
        }
        return DEFAULT_TRANSPOSITION_CATEGORY + " " + ++index;
    }

    private class TransposableDataRow {

        private List<DataElement> leftDataElements = new ArrayList<>();
        private List<DataElement> middleDataElements = new ArrayList<>();
        private List<DataElement> rightDataElements = new ArrayList<>();
    }
}
