package ke.co.hoji.server.model;

import java.math.BigDecimal;

/**
 * Constants for credit transaction management.
 * <p>
 * Created by gitahi on 04/01/16.
 */
public class CreditTransactionConstants {

    /**
     * Default minimum no of credits before a user is notified that they are running low.
     */
    public static final Integer DEFAULT_MIN_CREDITS = 500;

    /**
     * The number of credits that a user must have to qualify for a higher test records quota. See
     * {@link #BASIC_TEST_RECORDS_QUOTA} and {@link #PRIVILEGED_TEST_RECORDS_QUOTA}.
     */
    public static final Integer PRIVILEGED_NUMBER_OF_CREDITS = 100;

    /**
     * Default tax rate.
     */
    public static final BigDecimal DEFAULT_TAX_RATE = BigDecimal.valueOf(16);

    /**
     * Default discount rate.
     */
    public static final BigDecimal DEFAULT_DISCOUNT_RATE = BigDecimal.valueOf(0);

    /**
     * Universal discount rate.
     */
    public static final BigDecimal UNIVERSAL_DISCOUNT_RATE = BigDecimal.valueOf(0);

    /**
     * The total number of records per survey allowed for testing for a user with less than the
     * {@link #PRIVILEGED_NUMBER_OF_CREDITS}. If and when this number is exceeded, the oldest
     * record in the survey is deleted to make room.
     */
    public static final Integer BASIC_TEST_RECORDS_QUOTA = 10;

    /**
     * The total number of records per survey allowed for testing for a user with more than the
     * {@link #PRIVILEGED_NUMBER_OF_CREDITS}. If and when this number is exceeded, the oldest
     * record in the survey is deleted to make room.
     */
    public static final Integer PRIVILEGED_TEST_RECORDS_QUOTA = 50;
}
