package ke.co.hoji.server.model;

/**
 * A convenience class for storing start and end GPS coordinates.
 * <p>
 * Created by gitahi on 03/12/15.
 */
public class StartAndEndGps {

    private String user;
    private double startLatitude;
    private double endLatitude;
    private double startLongitude;
    private double endLongitude;

    public StartAndEndGps() {
    }

    public StartAndEndGps(String user, double startLatitude, double endLatitude, double startLongitude, double endLongitude) {
        this.user = user;
        this.startLatitude = startLatitude;
        this.endLatitude = endLatitude;
        this.startLongitude = startLongitude;
        this.endLongitude = endLongitude;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public double getStartLatitude() {
        return startLatitude;
    }

    public void setStartLatitude(double startLatitude) {
        this.startLatitude = startLatitude;
    }

    public double getEndLatitude() {
        return endLatitude;
    }

    public void setEndLatitude(double endLatitude) {
        this.endLatitude = endLatitude;
    }

    public double getStartLongitude() {
        return startLongitude;
    }

    public void setStartLongitude(double startLongitude) {
        this.startLongitude = startLongitude;
    }

    public double getEndLongitude() {
        return endLongitude;
    }

    public void setEndLongitude(double endLongitude) {
        this.endLongitude = endLongitude;
    }

    /**
     * Checks to see if this {@link StartAndEndGps} has proper start and end coordinates so that we are able to
     * calculate the distance in between.
     * @return true if gps is usable
     */
    public boolean isUsable() {
        return (startLatitude != 0 && startLongitude != 0 && endLatitude != 0 && endLongitude != 0);
    }
}
