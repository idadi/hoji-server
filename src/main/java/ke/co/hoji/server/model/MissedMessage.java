package ke.co.hoji.server.model;

import ke.co.hoji.core.data.model.Form;

public class MissedMessage {

    private final Integer id;
    private final WebHook webHook;
    private final Form form;
    private final String mainRecordUuid;
    private final String eventDescription;

    public MissedMessage(Integer id, WebHook webHook, Form form, String mainRecordUuid, String eventDescription) {
        this.id = id;
        this.webHook = webHook;
        this.form = form;
        this.mainRecordUuid = mainRecordUuid;
        this.eventDescription = eventDescription;
    }

    public Integer getId() {
        return id;
    }

    public WebHook getWebHook() {
        return webHook;
    }

    public Form getForm() {
        return form;
    }

    public String getMainRecordUuid() {
        return mainRecordUuid;
    }

    public String getEventDescription() {
        return eventDescription;
    }

}