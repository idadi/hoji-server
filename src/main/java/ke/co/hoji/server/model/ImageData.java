package ke.co.hoji.server.model;

public class ImageData {

    private final String label;
    private final String src;
    private final String caption;

    public ImageData(String label, String src, String caption) {
        this.label = label;
        this.src = src;
        this.caption = caption;
    }

    public String getLabel() {
        return label;
    }

    public String getSrc() {
        return src;
    }

    public String getCaption() {
        return caption;
    }
}
