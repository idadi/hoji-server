package ke.co.hoji.server.model;

import ke.co.hoji.core.data.model.Form;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * A model object for sending a list of {@link Form} back and forth between the view and the controller.
 * </p>
 * Created by geoffreywasilwa on 27/03/2017.
 */
public class FormList extends ResourceList<Form> {

    //Required so that an instance of this class can be generated from the request parameters
    public FormList() {
        super(new ArrayList<Form>());
    }

    public FormList(List<Form> resources) {
        super(resources);
    }

    @Override
    public List<Form> getResources() {
        return resources;
    }

}
