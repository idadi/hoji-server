package ke.co.hoji.server.model;

import ke.co.hoji.core.data.ConfigObject;
import ke.co.hoji.core.data.UserOwned;
import ke.co.hoji.core.data.model.EventSubject;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.event.model.EventType;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class WebHook extends ConfigObject implements UserOwned, EventSubject {

    private static final long serialVersionUID = 979759444627193279L;

    private final List<Form> forms;
    private final List<EventType> eventTypes;
    private final String targetUrl;
    private final Integer userId;
    private final boolean verified;
    private final boolean active;
    private final String secret;

    public WebHook() {
        this(null, new ArrayList<>(), new ArrayList<>(), null, null, false, false, null);
    }

    public WebHook(Integer id, List<Form> forms, List<EventType> eventTypes, String targetUrl,
                   Integer userId, boolean verified, boolean active, String secret) {
        super(id);
        this.forms = new ArrayList<>(forms);
        this.eventTypes = new ArrayList<>(eventTypes);
        this.targetUrl = targetUrl;
        this.userId = userId;
        this.verified = verified;
        this.active = active;
        this.secret = secret;
    }

    public void addForm(Form form) {
        forms.add(form);
    }

    public List<Form> getForms() {
        return forms;
    }

    public void addEventType(EventType eventType) {
        eventTypes.add(eventType);
    }

    public List<EventType> getEventTypes() {
        return eventTypes;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    @Override
    public Integer getUserId() {
        return userId;
    }

    @Override
    public void setUserId(Integer username) {
        throw new UnsupportedOperationException();
    }

	public boolean isVerified() {
		return verified;
	}

    public boolean isActive() {
        return verified && active;
    }

    public String getSecret() {
        return secret;
    }

    @Override
    public LinkedHashMap<String, Object> descriptiveProperties() {
        LinkedHashMap descriptiveProperties = super.descriptiveProperties();
        descriptiveProperties.put("Target URL", targetUrl);
        descriptiveProperties.put("Verified", verified);
        descriptiveProperties.put("Active", active);
        return descriptiveProperties;
    }
}
