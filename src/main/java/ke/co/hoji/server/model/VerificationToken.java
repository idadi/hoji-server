package ke.co.hoji.server.model;

import ke.co.hoji.core.data.Constants;

import java.util.Date;

/**
 * <p>
 * A token used to verify a user's email account, either in order to confirm their registration or to reset their
 * password.
 * </p>
 * Created by gitahi on 17/12/15.
 */
public class VerificationToken {

    /**
     * The expiration period of a token in seconds.
     */
    public static final int EXPIRATION_PERIOD = (60 * Constants.Server.VERIFICATION_EMAIL_EXPIRY) * 60;

    /**
     * The type of verification token.
     */
    public static class Type {

        /**
         * A verification token issued for email verification.
         */
        public static final int EMAIL_VERIFICATION = 0;

        /**
         * A verification token issued for password resetting.
         */
        public static final int PASSWORD_RESET = 1;
    }

    /**
     * The primary key of the token.
     */
    private int id;

    /**
     * The value of the token itself.
     */
    private final String token;

    /**
     * The #Type of verification token this is.
     */
    private final int type;

    /**
     * The user associated with this token.
     */
    private final User user;

    /**
     * The date of expiry of this token.
     */
    private final long expiryDate;

    /**
     * Initializes a new token.
     *
     * @param token      the token associated with a {@link User}
     * @param type       the type of verification
     * @param user       the {@link User} assigned to the token
     * @param expiryDate the date that the token expires
     */
    public VerificationToken(String token, int type, User user, long expiryDate) {
        this.token = token;
        this.type = type;
        this.user = user;
        this.expiryDate = expiryDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public int getType() {
        return type;
    }

    public User getUser() {
        return user;
    }

    public long getExpiryDate() {
        return expiryDate;
    }

    /**
     * @return true if token is expired and false otherwise.
     */
    public boolean isExpired() {
        long now = new Date().getTime();
        return now > expiryDate;
    }
}
