package ke.co.hoji.server.interceptor;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.VerificationToken;
import ke.co.hoji.server.service.UserService;
import org.joda.time.Hours;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Intercepts requests to check whether a user has stayed too long without verifying their account and they need to
 * be suspended.
 * <p>
 * Also enforces the suspension by redirecting suspended users to the nag page.
 */
@Component
public final class SuspensionInterceptor implements AsyncHandlerInterceptor {

    private final String ANONYMOUS_USERNAME = "anonymousUser";

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    private int hoursToExpiry;

    @Override
    public boolean preHandle(
        HttpServletRequest request,
        HttpServletResponse response,
        Object handler
    ) throws Exception {
        hoursToExpiry = Integer.MAX_VALUE;
        String username = ANONYMOUS_USERNAME;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            username = authentication.getName();
        }
        if (!ANONYMOUS_USERNAME.equals(username)) {
            User user = (User) userService.loadUserByUsername(username);
            if (!user.isVerified() && !user.isSuspended()) {
                boolean suspend = false;
                VerificationToken token = userService.getLatestVerificationToken(user);
                if (token == null) {
                    suspend = true;
                } else {
                    hoursToExpiry = Hours.hoursBetween(
                        new LocalDateTime(),
                        new LocalDateTime(token.getExpiryDate())
                    ).getHours();
                    if (hoursToExpiry < 0) {
                        suspend = true;
                    }
                }
                if (suspend) {
                    user.setSuspended(true);
                    userService.suspendUser(user);
                    applicationEventPublisher.publishEvent(
                        new UserEvent(EventSource.WEB, user, user, EventType.SUSPEND)
                    );
                }
            }
            String uri = request.getRequestURI();
            if (user.isSuspended()) {
                uri = uri.replace("//", "/");
                if (!uri.startsWith("/user/")
                    && !uri.startsWith("/confirm")
                    && !uri.startsWith("/resources")
                    && !uri.equals("/logout")
                    && !uri.equals("/resendToken")
                    && !uri.equals("/verificationTokenSent")
                    && !uri.startsWith("/api/")) {
                    response.sendRedirect("/user/nag");
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void postHandle(
        HttpServletRequest request,
        HttpServletResponse response,
        Object handler,
        ModelAndView modelAndView
    ) throws Exception {
        String username = ANONYMOUS_USERNAME;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            username = authentication.getName();
        }
        if (!ANONYMOUS_USERNAME.equals(username)) {
            User user = (User) userService.loadUserByUsername(username);
            String uri = request.getRequestURI();
            if (user.isSuspended()
                && response != null
                && uri.startsWith("/api/")
            ) {
                if (!uri.equals("/api/user/authenticate") && !uri.equals("/api/download/projects")) {
                    response.setStatus(451);
                }
            }
        }
        if (modelAndView != null) {
            modelAndView.getModel().put("hoursToExpiry", hoursToExpiry);
        }
    }
}
