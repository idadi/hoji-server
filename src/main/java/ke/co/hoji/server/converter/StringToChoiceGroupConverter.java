package ke.co.hoji.server.converter;

import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.service.model.ChoiceGroupService;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by gitahi on 30/10/15.
 */
public class StringToChoiceGroupConverter implements Converter<String, ChoiceGroup> {

    private final ChoiceGroupService choiceGroupService;

    public StringToChoiceGroupConverter(ChoiceGroupService choiceGroupService) {
        this.choiceGroupService = choiceGroupService;
    }

    @Override
    public ChoiceGroup convert(String choiceGroupId) {
        if (!"".equals(choiceGroupId)) {
            return choiceGroupService.getChoiceGroupById(Integer.parseInt(choiceGroupId));
        }
        return null;
    }
}
