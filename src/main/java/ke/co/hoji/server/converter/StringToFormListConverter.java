package ke.co.hoji.server.converter;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.FormService;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gitahi on 30/10/15.
 */
public class StringToFormListConverter implements Converter<String, List<Form>> {

    private final FormService formService;

    public StringToFormListConverter(FormService formService) {
        this.formService = formService;
    }

    @Override
    public List<Form> convert(String formId) {
        List<Form> formList = null;
        if (formId != null && !formId.equals("")) {
            formList = new ArrayList<>();
            Form form = formService.getFormById(Integer.parseInt(formId));
            formList.add(form);
        }
        return formList;
    }
}
