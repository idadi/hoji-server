package ke.co.hoji.server.converter;

import ke.co.hoji.core.data.model.OperatorDescriptor;
import ke.co.hoji.core.service.model.FieldService;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by gitahi on 30/10/15.
 */
public class StringToOperatorDescriptorConverter implements Converter<String, OperatorDescriptor> {

    private final FieldService fieldService;

    public StringToOperatorDescriptorConverter(FieldService fieldService) {
        this.fieldService = fieldService;
    }

    @Override
    public OperatorDescriptor convert(String operatorDescriptorId) {
        if (!"".equals(operatorDescriptorId)) {
            return fieldService.getOperatorDescriptorById(Integer.parseInt(operatorDescriptorId));
        }
        return null;
    }
}
