package ke.co.hoji.server.converter;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Record;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.List;

public class DtoToModelRecordConverter implements Converter<ke.co.hoji.core.data.dto.Record, Record> {

    private final HojiContext hojiContext;
    private final FieldParent fieldParent;

    public DtoToModelRecordConverter(HojiContext hojiContext, FieldParent fieldParent) {
        this.hojiContext = hojiContext;
        this.fieldParent = fieldParent;
    }

    @Override
    public Record convert(ke.co.hoji.core.data.dto.Record source) {
        Record target = new Record(fieldParent, source.isMain(), source.isTest(), source.getCaption());
        target.setUuid(source.getUuid());
        List<ke.co.hoji.core.data.model.LiveField> liveFields = new ArrayList<>();
        for (LiveField liveField : source.getLiveFields()) {
            Field field = getField(liveField.getFieldId());
            if (field != null) {
                DtoToModelLiveFieldConverter converter = new DtoToModelLiveFieldConverter(
                    hojiContext,
                    field,
                    source
                );
                ke.co.hoji.core.data.model.LiveField lf = converter.convert(liveField);
                liveFields.add(lf);
            }
        }
        target.setLiveFields(liveFields);
        target.setDateCreated(source.getDateCreated());
        return target;
    }

    private Field getField(Integer fieldId) {
        return fieldParent.getChildren().stream()
            .filter(field -> field.getId().equals(fieldId))
            .findFirst()
            .orElse(null);
    }
}
