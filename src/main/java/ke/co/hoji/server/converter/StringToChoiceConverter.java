package ke.co.hoji.server.converter;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.server.dao.model.JdbcChoiceDao;

import org.springframework.core.convert.converter.Converter;

/**
 * Created by gitahi on 30/10/15.
 */
public class StringToChoiceConverter implements Converter<String, Choice> {

    private final JdbcChoiceDao choiceDao;

    public StringToChoiceConverter(JdbcChoiceDao choiceDao) {
        this.choiceDao = choiceDao;
    }

    @Override
    public Choice convert(String choiceId) {
        if (!"".equals(choiceId)) {
            return choiceDao.getById(Integer.parseInt(choiceId));
        }
        return null;
    }
}
