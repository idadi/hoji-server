package ke.co.hoji.server.converter;

import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.service.model.FieldService;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by gitahi on 30/10/15.
 */
public class StringToFieldTypeConverter implements Converter<String, FieldType> {

    private final FieldService fieldService;

    public StringToFieldTypeConverter(FieldService fieldService) {
        this.fieldService = fieldService;
    }

    @Override
    public FieldType convert(String fieldTypeId) {
        if (!"".equals(fieldTypeId)) {
            return fieldService.getFieldTypeById(Integer.parseInt(fieldTypeId));
        }
        return null;
    }
}
