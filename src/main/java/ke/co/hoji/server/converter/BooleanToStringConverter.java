package ke.co.hoji.server.converter;

import org.springframework.core.convert.converter.Converter;

/**
 * Created by gitahi on 30/10/15.
 */
public class BooleanToStringConverter implements Converter<Boolean, String> {

    @Override
    public String convert(Boolean actual) {
        return actual ? "Yes" : "No";
    }
}
