package ke.co.hoji.server.converter;

import org.springframework.core.convert.converter.Converter;

/**
 * Created by gitahi on 30/10/15.
 */
public class IntegerToStringConverter implements Converter<Integer, String> {

    @Override
    public String convert(Integer actual) {
//        switch (actual) {
//            case 0:
//                return "Allow";
//            case 1:
//                return "Warn but allow";
//            case 2:
//                return "Do not allow";
//        }
        return String.valueOf(actual);
    }
}
