package ke.co.hoji.server.converter;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.FormService;
import org.springframework.core.convert.converter.Converter;

public class StringToFormConverter implements Converter<String, Form> {

    private final FormService formService;
    public StringToFormConverter(FormService formService) {
        this.formService = formService;
    }

    @Override
    public Form convert(String source) {
        Form form = null;
        Integer formId;
        if (!"".equals(source) && (formId = Utils.parseInteger(source)) != null) {
            form = formService.getFormById(formId);
        }
        return form;
    }
}
