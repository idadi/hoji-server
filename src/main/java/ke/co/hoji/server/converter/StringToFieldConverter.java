package ke.co.hoji.server.converter;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.service.model.FieldService;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by gitahi on 30/10/15.
 */
public class StringToFieldConverter implements Converter<String, Field> {

    private final FieldService fieldService;

    public StringToFieldConverter(FieldService fieldService) {
        this.fieldService = fieldService;
    }

    @Override
    public Field convert(String fieldId) {
        Field field = null;
        if (!"".equals(fieldId)) {
            field = fieldService.getFieldById(Integer.parseInt(fieldId));
        }
        return field;
    }
}
