package ke.co.hoji.server.converter;

import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.SurveyService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.util.Assert;

public class StringToSurveyConverter implements Converter<String, Survey> {

    private final SurveyService surveyService;

    public StringToSurveyConverter(SurveyService surveyService) {
        this.surveyService = surveyService;
    }

    @Override
    public Survey convert(String source) {
        Assert.notNull(source, "source should not be null");

        return surveyService.getSurveyById(Integer.parseInt(source));
    }
}
