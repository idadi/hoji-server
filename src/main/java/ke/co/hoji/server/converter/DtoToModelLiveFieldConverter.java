package ke.co.hoji.server.converter;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.MatrixRecord;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.Response;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by geoffreywasilwa on 31/05/2017.
 */
public class DtoToModelLiveFieldConverter implements Converter<LiveField, ke.co.hoji.core.data.model.LiveField> {

    private final HojiContext hojiContext;
    private final Field field;
    private final ke.co.hoji.core.data.dto.Record mainRecord;

    public DtoToModelLiveFieldConverter(HojiContext hojiContext, Field field) {
        this(hojiContext, field, null);
    }


    public DtoToModelLiveFieldConverter(
            HojiContext hojiContext, Field field, ke.co.hoji.core.data.dto.Record mainRecord) {
        this.hojiContext = hojiContext;
        this.field = field;
        this.mainRecord = mainRecord;
    }

    @Override
    public ke.co.hoji.core.data.model.LiveField convert(LiveField source) {
        ke.co.hoji.core.data.model.LiveField target = new ke.co.hoji.core.data.model.LiveField(field);
        target.setUuid(source.getUuid());
        Object value;
        if (field.isMatrix()) {
            value = getMatrixRecords(field);
        } else {
            value = source.getValue();
            int dataType = field.getType().getDataType();
            if (value != null) {
                if (dataType == FieldType.DataType.NUMBER) {
                    value = Utils.parseLong(value.toString());
                } else if (dataType == FieldType.DataType.DECIMAL) {
                    value = Utils.parseDouble(value.toString());
                }
            }
        }
        Response response = Response.create(hojiContext, target, value, false);
        target.setResponse(response);
        return target;
    }

    /**
     * Returns a collection of matrix records linked to the supplied matrix field.
     *
     * @param field, the matrix field
     * @return all matrix records linked to the supplied field
     */
    private List<MatrixRecord> getMatrixRecords(Field field) {
        List<MatrixRecord> matrixRecords = new ArrayList<>();
        Map<Integer, Field> childrenMap = field.getChildren().stream().collect(Collectors.toMap(Field::getId, child -> child));
        if (mainRecord != null) {
            for (ke.co.hoji.core.data.dto.MatrixRecord matrixRecord : ((MainRecord) mainRecord).getMatrixRecords()) {
                if (field.getId().equals(matrixRecord.getFieldId())) {
                    Record mainRecordModel = new Record(field.getForm(), true, false, mainRecord.getCaption());
                    mainRecordModel.setUuid(mainRecord.getUuid());
                    Record matrixRecordModel = new Record(field, false, false, matrixRecord.getCaption());
                    matrixRecordModel.setUuid(matrixRecord.getUuid());
                    List<ke.co.hoji.core.data.model.LiveField> convertedLfs = new ArrayList<>();
                    for (LiveField liveField : matrixRecord.getLiveFields()) {
                        Field child = childrenMap.get(liveField.getFieldId());
                        if (child != null) {
                            convertedLfs.add(new DtoToModelLiveFieldConverter(hojiContext, child).convert(liveField));
                        }
                    }
                    matrixRecordModel.setLiveFields(convertedLfs);
                    matrixRecords.add(new MatrixRecord(matrixRecordModel, mainRecordModel));
                }
            }
        }
        return matrixRecords;
    }

}
