package ke.co.hoji.server.service;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.server.dao.model.JdbcFormDao;
import ke.co.hoji.server.dao.model.JdbcPropertyDao;
import ke.co.hoji.server.dao.model.JdbcSurveyDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PropertyServiceImpl implements PropertyService {

    private final JdbcPropertyDao propertyDao;
    private final JdbcSurveyDao surveyDao;
    private final JdbcFormDao formDao;

    @Autowired
    public PropertyServiceImpl(JdbcPropertyDao propertyDao, JdbcSurveyDao surveyDao, JdbcFormDao formDao) {
        if (propertyDao == null) {
            throw new IllegalArgumentException("propertyDao should not be null");
        }
        if (surveyDao == null) {
            throw new IllegalArgumentException("surveyDao should not be null");
        }
        if (formDao == null) {
            throw new IllegalArgumentException("formDao should not be null");
        }
        this.propertyDao = propertyDao;
        this.surveyDao = surveyDao;
        this.formDao = formDao;
    }

    /**
     * A cache of {@link Property} objects that have already been loaded from the database.
     */
    private final Map<String, Property> properties = new HashMap<>();

    /**
     * Gets the {@link Property} with the given key from the database.
     *
     * @param propertyKey the key of the Property to get.
     * @param surveyId    id of the survey associated with the Property to get.
     * @return the Property with the given key, if any, and null otherwise..
     */
    @Override
    public Property getProperty(String propertyKey, Integer surveyId) {
        String key = constructKey(propertyKey, surveyId);
        Property property = properties.get(key);
        if (property == null) {
            property = propertyDao.getProperty(propertyKey, surveyId);
            if (property != null) {
                properties.put(key, property);
            }
        }
        return property;
    }

    private String constructKey(String propertyKey, Integer surveyId) {
        if (surveyId != null) {
            return propertyKey + "_" + surveyId;
        }
        return propertyKey;
    }

    /**
     * Gets {@link Property properties} associated with a given survey.
     *
     * @param surveyId id of the {@link Survey survey} to get properties for.
     * @return a list of properties.
     */
    @Override
    public List<Property> getProperties(Integer surveyId) {
        List<Property> props = propertyDao.getProperties(surveyId);
        updateCache(props, surveyId);
        return props;
    }

    /**
     * Sets a {@link Property}. The property is persisted in the database.
     *
     * @param property the Property to set.
     */
    @Override
    public void setProperty(Property property) {
        if (property == null) {
            throw new IllegalArgumentException("property should not be null");
        }

        propertyDao.saveProperty(property);
        markFormsAsMinorModified(property.getSurvey());
        properties.put(property.getKey(), property);
    }

    @Override
    public void setProperties(Property... propertyArray) {
        setProperties(Arrays.asList(propertyArray));
    }

    /**
     * Sets more than one {@link Property}.
     *
     * @param props properties to be set
     */
    @Override
    public void setProperties(Collection<Property> props) {
        if (props == null) {
            throw new IllegalArgumentException("props should not be null");
        }
        if (props.size() == 0) {
            throw new IllegalArgumentException("props should not be an empty collection");
        }

        propertyDao.saveProperties(props);
        Survey survey = props.iterator().next().getSurvey();
        markFormsAsMinorModified(survey);
        if (survey != null) {
            updateCache(props, survey.getId());
        }
    }

    private void updateCache(Collection<Property> props, Integer surveyId) {
        for (Property property : props) {
            String key = constructKey(property.getKey(), surveyId);
            properties.put(key, property);
        }
    }

    /**
     * Deletes a {@link Property property} with given key.
     *
     * @param propertyKey the key of the Property to get.
     * @param surveyId    id of the survey associated with the Property to get.
     * @return true/false. true indicates operation was successful, otherwise false.
     */
    @Override
    public boolean deleteProperty(String propertyKey, Integer surveyId) {
        if (propertyDao.deleteProperty(propertyKey, surveyId)) {
            properties.remove(constructKey(propertyKey, surveyId));
            markFormsAsMinorModified(surveyDao.getSurveyById(surveyId));
            return true;
        }
        return false;
    }

    /**
     * Convenience method to get the value of a property given it's key.
     *
     * @param propertyKey the key of the Property to get.
     * @param surveyId    id of the survey associated with the Property to get.
     * @return the value of that property as a String. May be null if key does not exist or the property itself is null.
     */
    @Override
    public String getValue(String propertyKey, Integer surveyId) {
        Property property = getProperty(propertyKey, surveyId);
        if (property != null && property.getValue() != null) {
            return property.getValue();
        }
        return null;
    }

    /**
     * Same as {@link #getValue(String, Integer)} but defaults the second argument (surveyId) to 0.
     *
     * @param propertyKey the key of the Property to get.
     * @return the value of that property as a String. May be null if key does not exist or the property itself is null.
     */
    @Override
    public String getValue(String propertyKey) {
        return getValue(propertyKey, null);
    }

    /**
     * Convenience method to check if a property with the given key exists and has a non null value.
     *
     * @param propertyKey the key of the Property to get.
     * @param surveyId    id of the survey associated with the Property to get.
     * @return true if a property with the given key exists and has a non null value and false otherwise.
     */
    @Override
    public boolean hasValue(String propertyKey, Integer surveyId) {
        return getValue(propertyKey, surveyId) != null;
    }

    /**
     * Same as {@link #hasValue(String, Integer)} but defaults the second argument (surveyId) to 0.
     *
     * @param propertyKey the key of the Property to get.
     * @return true if a property with the given key exists and has a non null value and false otherwise.
     */
    @Override
    public boolean hasValue(String propertyKey) {
        return getValue(propertyKey) != null;
    }

    /**
     * Clears cached properties so that future calls to any of the methods for getting properties are forced to fetch
     * fresh values from the database.
     */
    @Override
    public void clearCache() {
        properties.clear();
    }

    private void markFormsAsMinorModified(Survey survey) {
        List<Form> forms = formDao.getFormsBySurvey(survey);
        for (Form form : forms) {
            formDao.markAsMinorModified(form);
        }
    }
}
