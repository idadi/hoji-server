package ke.co.hoji.server.service;

import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.dao.impl.JdbcCreditTransactionDao;
import ke.co.hoji.server.model.CreditBundle;
import ke.co.hoji.server.model.CreditTransaction;
import ke.co.hoji.server.model.CreditTransactionConstants;
import ke.co.hoji.server.model.Transaction.Type;
import ke.co.hoji.server.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

/**
 * <p>Manages @{@link CreditTransaction}s.</p>
 *
 * <p>Created by gitahi on 04/01/16.</p>
 */
@Service
public class CreditTransactionService {

    private final JdbcCreditTransactionDao creditTransactionDao;

    @Autowired
    public CreditTransactionService(JdbcCreditTransactionDao creditTransactionDao) {
        this.creditTransactionDao = creditTransactionDao;
    }

    /**
     * {@code free} defaults to false
     *
     * @param user        the @{@link User} for whom to deposit credits.
     * @param referenceNo the @{@link CreditTransaction#referenceNo}.
     * @param date        the @{@link CreditTransaction#date}.
     * @param credits     the number of credits to deposit.
     * @param expiry      the expiry period (in days) of the credits deposited.
     */
    public void deposit(User user, String referenceNo, Date date, Integer credits, Integer expiry) {
        deposit(user, referenceNo, date, credits, expiry, false);
    }

    /**
     * Deposit credits into a @{@link User}s account. This occurs when the User purchases credits.
     *
     * @param user        the @{@link User} for whom to deposit credits.
     * @param referenceNo the @{@link CreditTransaction#referenceNo}.
     * @param date        the @{@link CreditTransaction#date}.
     * @param credits     the number of credits to deposit.
     * @param expiry      the expiry period (in days) of the credits deposited.
     * @param free        true if the credits deposited are granted for free by Hoji and false otherwise.
     */
    public void deposit(User user, String referenceNo, Date date, Integer credits, Integer expiry, boolean free) {
        CreditTransaction creditTransaction = new CreditTransaction
                (
                        user,
                        date,
                        referenceNo,
                        free ? Type.FREE : Type.DEPOSIT
                );
        creditTransaction.setCredits(credits);
        creditTransaction.setExpiry(expiry);
        creditTransactionDao.create(creditTransaction);
    }

    /**
     * Expense credits from a {@link User}'s account. This occurs when a {@link MainRecord} is submitted to the server..
     *
     * @param user        the User against whose account to expense the credits
     * @param form        the Form for which credits are being charged.
     * @param referenceNo the @{@link CreditTransaction#referenceNo}.
     * @param date        the @{@link CreditTransaction#date}.
     * @param credits     the number of credits to be expensed for this {@link CreditTransaction}.
     * @return true if this expense transaction is successful (i.e. the User has sufficient credits to effect it) and
     * false otherwise.
     */
    public boolean expense(User user, Form form, String referenceNo, Date date, Integer credits) {
        Integer toPayNow = credits;
        Integer previouslyPaid = previouslyPaid(referenceNo);
        if (previouslyPaid > 0) {
            toPayNow = toPayNow - previouslyPaid;
        }
        if (toPayNow > 0) {
            if (!form.getSurvey().isFree()) {
                if (user.getType() == User.Type.PREPAID) {
                    Integer balance = balance(user);
                    if (balance < toPayNow) {
                        return false;
                    }
                }
                CreditTransaction creditTransaction = new CreditTransaction
                        (
                                user,
                                new Date(),
                                referenceNo,
                                Type.EXPENSE
                        );
                creditTransaction.setCredits(-toPayNow);
                creditTransaction.setForm(form);
                creditTransactionDao.create(creditTransaction);
            }
        }
        return true;
    }

    /**
     * Calculate the credit balance in a @{@link User}'s account.
     *
     * @param user the User for whom to calculate the balance.
     * @return the credit balance.
     */
    public Integer balance(User user) {
        return creditTransactionDao.balance(user, new Date());
    }

    /**
     * Find the last date when a @{@link User} incurred a credit expense i.e. submitted a @{@link MainRecord}.
     *
     * @param user the User for whom to find the last date.
     * @return the last date.
     */
    public Date dateOfLastExpense(User user) {
        return creditTransactionDao.dateOfLastExpense(user);
    }

    /**
     * Find the applicable expiry period for a @{@link User}'s credit balance. This is based on the
     * last @{@link CreditBundle} they purchased.
     *
     * @param user the User for whom to find the expiry period.
     * @return the expiry period in days.
     */
    public Integer expiryPeriod(User user) {
        return creditTransactionDao.expiryPeriod(user);
    }

    /**
     * Checks if a @{@link User} has ever bought a @{@link CreditBundle}.
     *
     * @param user the User to check
     * @return true if the User has ever bought a CreditBundle and false otherwise.
     */
    public Boolean hasEverBoughtABundle(User user) {
        return creditTransactionDao.hasEverBoughtABundle(user);
    }

    /**
     * Expires all of a @{@link User}'s outstanding credit balance.
     *
     * @param user the User for whom to expire credits.
     */
    public void expireCredits(User user) {
        CreditTransaction creditTransaction = new CreditTransaction
                (
                        user,
                        new Date(),
                        UUID.randomUUID().toString(),
                        Type.EXPIRY
                );
        creditTransaction.setCredits(-balance(user));
        creditTransactionDao.create(creditTransaction);
    }

    public Integer testRecordsQuota(User user) {
        Integer balance = balance(user);
        Integer testRecordsQuota = CreditTransactionConstants.BASIC_TEST_RECORDS_QUOTA;
        if (balance > CreditTransactionConstants.PRIVILEGED_NUMBER_OF_CREDITS) {
            testRecordsQuota = CreditTransactionConstants.PRIVILEGED_TEST_RECORDS_QUOTA;
        }
        return testRecordsQuota;
    }

    private Integer previouslyPaid(String recordUuid) {
        return creditTransactionDao.previouslyPaid(recordUuid);
    }
}
