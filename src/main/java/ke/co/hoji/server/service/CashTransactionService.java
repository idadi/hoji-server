package ke.co.hoji.server.service;

import ke.co.hoji.server.dao.impl.JdbcCashTransactionDao;
import ke.co.hoji.server.model.CashTransaction;
import ke.co.hoji.server.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Manages @{@link CashTransaction}s.
 * <p>
 * Created by gitahi on 05/01/18.
 */
@Service
public class CashTransactionService {

    private final JdbcCashTransactionDao cashTransactionDao;

    @Autowired
    public CashTransactionService(JdbcCashTransactionDao cashTransactionDao) {
        this.cashTransactionDao = cashTransactionDao;
    }

    /**
     * Deposit cash into a @{@link User}s account. This occurs when the User pays money to Hoji.
     *
     * @param user             the @{@link User} for whom to deposit the cash.
     * @param referenceNo      the @{@link CashTransaction#referenceNo}.
     * @param date             the @{@link CashTransaction#date}.
     * @param cash             the amount of cash to be deposited.
     * @param paymentMode      the mode of payment
     * @param paymentReference the reference number of this transaction
     */
    public void deposit(
            User user,
            String referenceNo,
            Date date,
            BigDecimal cash,
            String paymentMode,
            String paymentReference
    ) {
    }

    /**
     * Expense cash from a @{@link User} account. This occurs when a user purchases credits.
     *
     * @param user        the @{@link User} against whose account to execute the expense.
     * @param referenceNo the @{@link CashTransaction#referenceNo}.
     * @param date        the @{@link CashTransaction#date}.
     * @param cash        the amount of cash to be expensed.
     */
    public void expense(User user, String referenceNo, Date date, BigDecimal cash) {
    }


    /**
     * Calculate the cash balance (in USD) for the given @{@link User}.
     *
     * @param user the @{@link User} for whom to calculate the balance.
     * @return the cash balance in USD.
     */
    public BigDecimal balance(User user) {
        return cashTransactionDao.balance(user, new Date());
    }

    public boolean transactionExists(User user, Date date, String referenceNo) {
        return cashTransactionDao.transactionExists(user, date, referenceNo);
    }
}
