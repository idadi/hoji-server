package ke.co.hoji.server.service;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.server.dao.model.JdbcFormDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class FormServiceImpl implements FormService {

    private final JdbcFormDao formDao;

    private final FieldService fieldService;

    @Autowired
    public FormServiceImpl(JdbcFormDao formDao, FieldService fieldService) {
        this.formDao = formDao;
        this.fieldService = fieldService;
    }

    @Override
    public Form getFormById(Integer formId) {
        return formDao.getFormById(formId);
    }

    @Override
    public List<Form> getFormsBySurvey(Survey survey) {
        List<Form> forms = formDao.getFormsBySurvey(survey);
        if (forms != null && !forms.isEmpty()) {
            Collections.sort(forms);
        }
        return forms;
    }

    @Override
    public List<Form> getFormsBySurvey(Survey survey, boolean enabled) {
        List<Form> forms = formDao.getFormsBySurvey(survey, enabled);
        if (forms != null && !forms.isEmpty()) {
            Collections.sort(forms);
        }
        return forms;
    }

    @Override
    public void saveForm(Form form) {
        formDao.saveForm(form);
    }

    @Override
    public List<Form> modifyForms(List<Form> forms) {
        return formDao.modifyForms(forms);
    }

    @Override
    public void deleteForm(Integer formId) {
        formDao.deleteForm(formId);
    }

    @Override
    public void autoNumber(List<Field> fields) {
        autoAssignNumbers(fields);
        for (Field field : fields) {
            if (field.isMatrix()) {
                List<Field> children = fieldService.getChildren(field);
                autoAssignNumbers(children);
            }
        }
    }

    @Override
    public void enableForm(Form form, boolean enabled) {
        formDao.enableForm(form, enabled);
        form.setEnabled(enabled);
    }


    @Override
    public void updateOrdinal(List<Form> forms) {
        formDao.updateOrdinal(forms);
    }

    /**
     * Compute the number credits chargeable for a single submission of this form in it's current version. Typically,
     * each form costs 1 credit to submit. But if it contains one or more images, then we charge an additional credit
     * for each image.
     *
     * @param form the {@link Form}
     * @return the credits
     */
    @Override
    public int computeCredits(Form form) {
        Integer credits = 1;
        for (Field field : fieldService.getFields(form)) {
            if (field.getType().isImage() && field.isEnabled()) {
                credits++;
            }
        }
        return credits;
    }

    private void autoAssignNumbers(List<Field> fields) {
        Collections.sort(fields);
        int questionNo = 1;
        for (Field field : fields) {
            if (!field.getType().isLabel()) {
                fieldService.setQuestionNumber(field, String.valueOf(questionNo));
                if (field.isEnabled()) {
                    questionNo++;
                }
            } else {
                fieldService.setQuestionNumber(field, "LABEL");
            }
        }
    }

}
