package ke.co.hoji.server.service;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ke.co.hoji.server.model.ApiRecord;
import ke.co.hoji.server.model.WebHook;

@Service
public class WebHookMessenger {

    private final static Logger logger = LoggerFactory.getLogger(WebHookMessenger.class);

    private final ObjectMapper objectMapper;
    @Value("${crypto.algorithm}")
    private String cryptoAlgorithm;

    @Autowired
    public WebHookMessenger(final ObjectMapper objectMapper){
        this.objectMapper = objectMapper;
    }

    /**
     * Send messages to a web hook
     * @param webHook           the web hook to communicate with
     * @param apiRecord         the message being sent
     * @param eventDescription  type of message e.g. 'record.created'
     * @return success or error status
     * @throws JsonProcessingException when apiRecord cannot be successfully written as JSON
     */
    public Status send(final WebHook webHook, final ApiRecord apiRecord, String eventDescription)
            throws JsonProcessingException {
        final Payload payload = new Payload(webHook.getId(), webHook.getTargetUrl(), eventDescription);
        try {
            final byte[] message = objectMapper.writeValueAsBytes(new PushContent(payload, apiRecord));
            final HttpHeaders headers = new HttpHeaders();
            final byte[] decodedKey = Base64.getDecoder().decode(webHook.getSecret());
            final String digest = hmacDigest(message, decodedKey);
            headers.set("X-Hook-Signature", digest);
            final HttpEntity<String> entity = new HttpEntity<>(Base64.getEncoder().encodeToString(message), headers);
            final RestTemplate restTemplate = new RestTemplate();
            final ResponseEntity<String> responseEntity =
                    restTemplate.postForEntity(webHook.getTargetUrl(), entity, String.class);
            if (responseEntity.getStatusCode().equals(HttpStatus.NOT_FOUND)
                    || responseEntity.getStatusCode().equals(HttpStatus.FORBIDDEN)
                    || responseEntity.getStatusCode().is5xxServerError()) {
                return Status.ERROR;
            }
        } catch (ResourceAccessException | HttpServerErrorException e) {
            logger.warn(e.getMessage());
            logger.debug(e.getMessage(), e);
            return Status.ERROR;
        } catch (final JsonProcessingException e) {
            logger.error(e.getMessage());
            logger.debug(e.getMessage(), e);
            throw e;
        }
        return Status.SUCCESS;
    }

    /**
     * This is called to set up configuration of web hook at configured url by
     * generating and storing a key if the server (at url) is alive.
     * @param url, the location of the server
     * @return the generated key upon successful response from server otherwise returns null
     */
    public String verify(String url) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(cryptoAlgorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
        SecretKey secretKey = keyGenerator.generateKey();
        String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
        headers.set("X-Hook-Secret", encodedKey);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                return encodedKey;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return null;
        }
    }

    private String hmacDigest(final byte[] msg, final byte[] decodedKey) {
        String digest;
        try {
            final SecretKeySpec key = new SecretKeySpec(decodedKey, 0, decodedKey.length, cryptoAlgorithm);
            final Mac mac = Mac.getInstance(cryptoAlgorithm);
            mac.init(key);
            final byte[] bytes = mac.doFinal(msg);
            final StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                final String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (NoSuchAlgorithmException | InvalidKeyException exception) {
            throw new RuntimeException(exception.getMessage());
        }
        return digest;
    }

    static class PushContent {
        private final Payload payload;
        private final ApiRecord record;

        PushContent(final Payload payload, final ApiRecord record) {
            this.payload = payload;
            this.record = record;
        }

        public Payload getPayload() {
            return payload;
        }

        public ApiRecord getRecord() {
            return record;
        }
    }

    static class Payload {
        private final Integer id;
        private final String target;
        private final String event;

        Payload(final Integer id, final String target, final String event) {
            this.id = id;
            this.target = target;
            this.event = event;
        }

        public Integer getId() {
            return id;
        }

        public String getTarget() {
            return target;
        }

        public String getEvent() {
            return event;
        }
    }

    public static enum Status {
        ERROR,
        SUCCESS
    }

}