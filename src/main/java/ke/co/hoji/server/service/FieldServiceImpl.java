package ke.co.hoji.server.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ke.co.hoji.core.data.ConfigObjectCache;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.OperatorDescriptor;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.server.dao.model.JdbcFieldDao;
import ke.co.hoji.server.dao.model.JdbcFormDao;

@Service
public class FieldServiceImpl implements FieldService {

    private final JdbcFieldDao fieldDao;

    private final JdbcFormDao formDao;

    @Autowired
    public FieldServiceImpl(JdbcFieldDao fieldDao, JdbcFormDao formDao) {
        this.fieldDao = fieldDao;
        this.formDao = formDao;
    }

    @Override
    public List<Field> getFields(Form form) {
        return fieldDao.getFields(form.getId());
    }

    @Override
    public List<Field> getAllFields(Form form) {
        List<Field> fields = new ArrayList<>();
        for (Field field : getFields(form)) {
            fields.add(field);
            if (field.isMatrix()) {
                List<Field> children = getChildren(field);
                field.setChildren(children);
                for (Field child : children) {
                    child.setParent(field);
                    fields.add(child);
                }
            }
        }
        return fields;
    }

    @Override
    public List<Field> getParentFields(Form form) {
        List<Field> fields = new ArrayList<>();
        for (Field field : getFields(form)) {
            if (field.isMatrix()) {
                fields.add(field);
            }
        }
        return fields;
    }

    @Override
    public List<Field> getReferenceFields(Form form) {
        List<Field> fields = new ArrayList<>();
        for (Field field : getFields(form)) {
            if (field.getType().getCode().equals(FieldType.Code.REFERENCE)) {
                fields.add(field);
            }
        }
        return fields;
    }

    @Override
    public List<Rule> getForwardSkips(Field source, ConfigObjectCache<Field> fieldCache) {
        return fieldDao.getRules(source, Rule.Type.FORWARD_SKIP, fieldCache);
    }

    @Override
    public List<Rule> getBackwardSkips(Field source, ConfigObjectCache<Field> fieldCache) {
        return fieldDao.getRules(source, Rule.Type.BACKWARD_SKIP, fieldCache);
    }

    @Override
    public List<Rule> getFilterRules(Field source, ConfigObjectCache<Field> fieldCache) {
        return fieldDao.getRules(source, Rule.Type.FILTER_RULE, fieldCache);
    }

    @Override
    public List<Field> getChildren(Field parent) {
        return fieldDao.getChildren(parent.getId());
    }

    @Override
    public Field getFieldById(Integer id) {
        return fieldDao.getFieldById(id);
    }

    @Override
    public void deleteField(Integer id) {
        fieldDao.deleteField(id);
    }

    @Override
    public Field createField(Field field) {
        return fieldDao.createField(field);
    }

    @Override
    public Field modifyField(Field field) {
        return fieldDao.modifyField(field);
    }

    @Override
    public List<Field> modifyFields(List<Field> fields) {
        return fieldDao.modifyFields(fields);
    }

    @Override
    public void enableField(Field field, boolean enabled) {
        fieldDao.enableField(field, enabled);
        field.setEnabled(enabled);
    }

    @Override
    public List<FieldType> getAllFieldTypes() {
        return fieldDao.getAllFieldTypes();
    }

    @Override
    public List<OperatorDescriptor> getAllOperatorDescriptors() {
        return fieldDao.getAllOperatorDescriptors();
    }

    @Override
    public FieldType getFieldTypeById(Integer fieldTypeId) {
        return fieldDao.getFieldTypeById(fieldTypeId);
    }

    @Override
    public OperatorDescriptor getOperatorDescriptorById(Integer operatorDescriptorId) {
        return fieldDao.getOperatorDescriptorById(operatorDescriptorId);
    }

    @Override
    public void deleteRule(Integer id) {
        fieldDao.deleteRule(new Rule(id));
    }

    @Override
    public void deleteRule(Rule rule) {
        fieldDao.deleteRule(rule);
    }

    @Override
    public Rule createRule(Rule rule) {
        return fieldDao.createRule(rule);
    }

    @Override
    public Rule modifyRule(Rule rule) {
        return fieldDao.modifyRule(rule);
    }

    @Override
    public List<Rule> getRules(Field owner, Integer type) {
        return fieldDao.getRules(owner, type);
    }

    @Override
    public Rule getRuleById(Field owner, Integer ruleId) {
        return fieldDao.getRuleById(owner, ruleId);
    }

    @Override
    public void setQuestionNumber(Field field, String questionNo) {
        fieldDao.setQuestionNumber(field, questionNo);
    }

    @Override
    public void updateOrdinal(List<Field> fields) {
        fieldDao.updateOrdinal(fields);
    }

    @Override
    public void updateRuleOrdinal(List<Rule> rules) {
        fieldDao.updateRuleOrdinal(rules);
    }

    @Override
    public Field getFieldFromDto(ke.co.hoji.core.data.dto.Field fieldDto) {
        Field field = new Field();
        field.setId(fieldDto.getId());
        field.setOrdinal(fieldDto.getOrdinal());
        field.setEnabled(fieldDto.isEnabled());
        field.setDescription(fieldDto.getDescription());
        if (fieldDto.getTypeId() != null) {
            field.setType(fieldDao.getFieldTypeById(fieldDto.getTypeId()));
        }
        field.setChoiceGroup(fieldDto.getChoiceGroup());
        field.setInstructions(fieldDto.getInstructions());
        field.setColumn(fieldDto.getColumn());
        field.setTag(fieldDto.getTag());
        field.setName(fieldDto.getName());
        field.setDefaultValue(fieldDto.getDefaultValue());
        field.setCaptioning(fieldDto.isCaptioning());
        field.setSearchable(fieldDto.isSearchable());
        field.setResponseInheriting(fieldDto.isResponseInheriting());
        field.setMissingValue(fieldDto.getMissingValue());
        field.setMissingAction(fieldDto.getMissingAction());
        field.setCalculated(fieldDto.getCalculated());
        field.setValueScript(fieldDto.getValueScript());
        field.setMinValue(fieldDto.getMinValue());
        field.setMaxValue(fieldDto.getMaxValue());
        field.setRegexValue(fieldDto.getRegexValue());
        field.setUniqueness(fieldDto.getUniqueness());
        if (fieldDto.getParentId() != null) {
            field.setParent(fieldDao.getFieldById(fieldDto.getParentId()));
        }
        if (fieldDto.getChoiceFilterFieldId() != null) {
            field.setChoiceFilterField(fieldDao.getFieldById(fieldDto.getChoiceFilterFieldId()));
        }
        if (fieldDto.getRecordFormIds() != null && fieldDto.getRecordFormIds().size() > 0) {
            List<Form> recordForms = new ArrayList<>();
            for (Integer recordFormId : fieldDto.getRecordFormIds()) {
                Form recordForm = formDao.getFormById(recordFormId);
                recordForms.add(recordForm);
            }
            field.setRecordForms(recordForms);
        }
        if (fieldDto.getReferenceFieldId() != null) {
            field.setReferenceField(fieldDao.getFieldById(fieldDto.getChoiceFilterFieldId()));
        }
        if (fieldDto.getMainRecordFieldId() != null) {
            field.setMainRecordField(fieldDao.getFieldById(fieldDto.getMainRecordFieldId()));
        }
        if (fieldDto.getMatrixRecordFieldId() != null) {
            field.setMatrixRecordField(fieldDao.getFieldById(fieldDto.getMatrixRecordFieldId()));
        }
        if (fieldDto.getFormId() != null) {
            field.setForm(formDao.getFormById(fieldDto.getFormId()));
        }
        return field;
    }
}
