package ke.co.hoji.server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.dao.impl.JdbcWebHookDao;
import ke.co.hoji.server.model.MissedMessage;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.WebHook;

@Service
public class WebHookService {

    private final JdbcWebHookDao webHookDao;

    @Autowired
    public WebHookService(JdbcWebHookDao webHookDao) {
        this.webHookDao = webHookDao;
    }

    public WebHook findWebHook(Integer id) {
        return webHookDao.findWebHook(id);
    }

    public List<WebHook> findWebHooksFor(Form form) {
        return webHookDao.findWebHooksFor(form);
    }

    public List<WebHook> findWebHooksFor(User user) {
        return webHookDao.findWebHooksFor(user);
    }

    public WebHook saveWebHook(WebHook webHook) {
        if (webHook.getId() != null) {
            WebHook original = findWebHook(webHook.getId());
            boolean verified = webHook.isVerified();
            boolean active = webHook.isActive();
            if (!original.getTargetUrl().equals(webHook.getTargetUrl())) {
                // verify if target url has changed.
                verified = false;
                active = false;
            }
            String secretKey = webHook.getSecret();
            if (original.getSecret() != null ) {
                secretKey = original.getSecret();
            }
            webHook = new WebHook(webHook.getId(), webHook.getForms(), webHook.getEventTypes(),
                    webHook.getTargetUrl(), webHook.getUserId(), verified, active, secretKey);
        }
        return webHookDao.saveWebHook(webHook);
    }

    public void deleteWebHook(Integer webHookId) {
        webHookDao.deleteWebHook(webHookId);
    }

    public List<MissedMessage> getMissedMessages() {
        return webHookDao.getMissedMessages();
    }

    public void saveMissedMessage(WebHook webHook, MainRecord mainRecord, String eventDescription) {
        MissedMessage missedMessage = new MissedMessage(null, webHook, new Form(mainRecord.getFormId()), mainRecord.getUuid(), eventDescription);
        webHookDao.saveMissedMessage(missedMessage);
    }

    public void deleteMissedMessage(Integer messageId) {
        webHookDao.deleteMissedMessage(messageId);
    }
}
