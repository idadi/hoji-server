package ke.co.hoji.server.service;

import ke.co.hoji.server.dao.impl.JdbcTemplateDao;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class TemplateService {

    private final JdbcTemplateDao templateDao;

    @Autowired
    public TemplateService(JdbcTemplateDao templateDao) {
        this.templateDao = templateDao;
    }

    public List<Template> getAllTemplates() {
        return templateDao.getAllTemplates();
    }

    public Template getTemplateByEvent(EventType eventType) {
        List<Template> templates = templateDao.getTemplateByEvent(eventType);
        if (templates.size() > 0) {
            return templates.get(0);
        }
        return null;
    }

    public Template saveTemplate(Template template) {
        return templateDao.saveTemplate(template);
    }

    public Set<EventType> getAssociatedEvents() {
        return templateDao.getAssociatedEvents();
    }
}
