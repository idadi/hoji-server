package ke.co.hoji.server.service;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.model.Chart;
import ke.co.hoji.server.model.DataRow;
import ke.co.hoji.server.model.DataTable;
import ke.co.hoji.server.model.GpsPoint;
import ke.co.hoji.server.model.ImageData;
import ke.co.hoji.server.model.RecordOverview;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter;
import ke.co.hoji.server.repository.MainRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gitahi on 17/09/15.
 */
@Service
public class DataService {

    @Autowired
    private MainRecordRepository mainRecordRepository;

    public DataTable getPrettyData(MainRecordPropertiesFilter mainRecordPropertiesFilter) {
        return mainRecordRepository.findDataTable(mainRecordPropertiesFilter, Form.OutputStrategy.TABLE);
    }

    /**
     * Prepares and returns data for the Flexmonster pivot table in the form of a {@link DataTable}.
     *
     * @param mainRecordPropertiesFilter the {@link MainRecordPropertiesFilter} containing the properties for generating
     *                                   the data table.
     * @return the data table.
     */
    public DataTable getPivotData(MainRecordPropertiesFilter mainRecordPropertiesFilter) {
        return mainRecordRepository.findDataTable(mainRecordPropertiesFilter, Form.OutputStrategy.PIVOT);
    }

    public DataTable getEvolutionData(MainRecordPropertiesFilter mainRecordPropertiesFilter) {
        return mainRecordRepository.findRecordSnapshot(mainRecordPropertiesFilter);
    }

    public List<ImageData> getImages(MainRecordPropertiesFilter mainRecordPropertiesFilter) {
        return mainRecordRepository.findImageData(mainRecordPropertiesFilter);
    }

    public List<List<String>> getRawData(MainRecordPropertiesFilter mainRecordPropertiesFilter, boolean cleanHeaders) {
        DataTable dataTable = mainRecordRepository
            .findDataTable(mainRecordPropertiesFilter, Form.OutputStrategy.DOWNLOAD);
        List<List<String>> data = new ArrayList<>();
        if (dataTable.getDataRows().size() > 0) {
            List<String> headers = new ArrayList<>();
            for (DataElement de : dataTable.getDataRows().get(0).getDataElements()) {
                String header = de.getLabel();
                if (cleanHeaders) {
                    header = Utils.toCleanIdentifier(header);
                }
                headers.add(header);
            }
            data.add(headers);
            for (DataRow dataRow : dataTable.getDataRows()) {
                List<String> row = new ArrayList<>();
                for (DataElement de : dataRow.getDataElements()) {
                    row.add(de.getValue());
                }
                data.add(row);
            }
        }
        return data;
    }

    public List<GpsPoint> getGpsPoints(MainRecordPropertiesFilter mainRecordPropertiesFilter) {
        return mainRecordRepository.findGpsPoints(mainRecordPropertiesFilter);
    }

    /*
     * Returns a list of {@link Chart} data used for charting
     * @param outputFilter, filter to apply on data
     * @return a list of {@link Chart}
     */

    public List<Chart> getChartingData(MainRecordPropertiesFilter mainRecordPropertiesFilter) {
        return mainRecordRepository.findChartingData(mainRecordPropertiesFilter);
    }

    public List<Chart> getMonitoringChartData(MainRecordPropertiesFilter mainRecordPropertiesFilter) {
        return mainRecordRepository.findMonitorChartData(mainRecordPropertiesFilter);
    }

    /*
     * Gets the list of {@link User}s who have at least 1 record for the given {@link Form}.
     *
     * @param form The Form for which to count records by User.
     * @return the list of Users.
     */

    public List<User> getUsersWithRecords(Form form) {
        return mainRecordRepository.findUsersWithRecords(form);
    }

    /*
     * Gets responses for a given record id.
     *
     * @param fieldParent, the fieldParent that the record is for
     * @param recordUuid,  the id of the record to fetch
     * @return a {@link RecordOverview}
     */
    public RecordOverview getRecordOverview(FieldParent fieldParent, String recordUuid) {
        return mainRecordRepository.findRecordOverview(fieldParent, recordUuid);
    }

    public void deleteRecord(FieldParent fieldParent, String recordUuid) {
        mainRecordRepository.deleteRecord(recordUuid, fieldParent);
    }

    public void updateTestMode(String recordUuid, boolean test) {
        mainRecordRepository.updateTestMode(recordUuid, test);
    }
}
