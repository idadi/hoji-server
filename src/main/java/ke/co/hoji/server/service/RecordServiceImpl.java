package ke.co.hoji.server.service;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.RecordSearch;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.MatrixRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.service.model.RecordService;
import ke.co.hoji.server.converter.DtoToModelLiveFieldConverter;
import ke.co.hoji.server.repository.MainRecordRepository;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by geoffreywasilwa on 30/05/2017.
 */
@Service("serverRecordService")
public class RecordServiceImpl implements RecordService, ApplicationContextAware {

    private ApplicationContext context;

    @Autowired
    public RecordServiceImpl() {
    }

    @Override
    public boolean isSimpleKeyUnique(Record record, Record mainRecord, Field field, Object uniqueValue, boolean testMode) {
        return false;
    }

    @Override
    public boolean isCompoundKeyUnique(Record record, Record mainRecord, String uniqueValue, boolean testMode) {
        return false;
    }

    @Override
    public List<Record> findRecords(ke.co.hoji.core.data.model.LiveField liveField, int testMode) {
        return null;
    }

    @Override
    public Record findRecordByUuid(String uuid, Field field) {
        MainRecordRepository mainRecordRepository = context.getBean(MainRecordRepository.class);
        MainRecord mainRecord;
        if (field.getMatrixRecordField() == null) {
        	mainRecord = mainRecordRepository.findByUuid(uuid);
        } else {
        	Integer formId = field.getMatrixRecordField().getForm().getId();
        	mainRecord = mainRecordRepository.findByMatrixUuid(uuid, formId);
        }
        Record record = null;
        if (mainRecord != null) {
            record = toRecord(mainRecord, field);
        }
        return record;
    }

    @Override
    public List<Record> search(RecordSearch recordSearch, int limit) {
        return null;
    }

    @Override
    public void save(Record record) {

    }

    @Override
    public void updateStage(ke.co.hoji.core.data.model.MainRecord mainRecord, boolean updateDateCompleted) {

    }

    @Override
    public int count(RecordSearch recordSearch) {
        return 0;
    }

    @Override
    public void setLiveFields(Record record) {

    }

    @Override
    public int clear(List<ke.co.hoji.core.data.model.LiveField> liveFields, Record record) {
        return 0;
    }

    @Override
    public void delete(Record record) {

    }

    private Record toRecord(ke.co.hoji.core.data.dto.MainRecord mainRecord, Field field) {
        Record record;
        List<Field> children = new ArrayList<>();
        if (field.getMatrixRecordField() == null) {
            Optional<Form> form = field.getRecordForms()
                    .stream()
                    .filter(f -> f.getId().equals(mainRecord.getFormId()))
                    .findFirst();
            form.ifPresent(f -> children.addAll(f.getChildren()));
            record = new Record(form.orElse(null), true, mainRecord.isTest(), mainRecord.getCaption());
        } else {
            Field parent = field.getMatrixRecordField();
            Optional<MatrixRecord> matrixRecord =
                    mainRecord.getMatrixRecords().stream().filter(matrix -> parent.getId().equals(matrix.getFieldId())).findFirst();
            String caption = "";
            boolean isTest = mainRecord.isTest();
            if (matrixRecord.isPresent()) {
                caption = matrixRecord.get().getCaption();
                isTest = matrixRecord.get().isTest();
            }
            children.addAll(parent.getChildren());
            record = new Record(parent, false, isTest, caption);
        }
        record.setUuid(mainRecord.getUuid());
        List<ke.co.hoji.core.data.model.LiveField> converted = new ArrayList<>();
        List<LiveField> allLiveFields = new ArrayList<>();
        allLiveFields.addAll(mainRecord.getLiveFields());
        allLiveFields.addAll(
                mainRecord.getMatrixRecords()
                        .stream()
                        .flatMap(matrixRecord -> matrixRecord.getLiveFields().stream())
                        .collect(Collectors.toList())
        );
        for (LiveField liveField : allLiveFields) {
            Field child = findField(liveField.getFieldId(), children);
            if (child != null) {
                HojiContext hojiContext = context.getBean(HojiContext.class);
                DtoToModelLiveFieldConverter converter = new DtoToModelLiveFieldConverter(hojiContext, child, mainRecord);
                converted.add(converter.convert(liveField));
            }
        }
        record.setLiveFields(converted);
        return record;
    }

    private Field findField(Integer fieldId, List<Field> fields) {
        for (Field field : fields) {
            if (field.getId().equals(fieldId)) {
                return field;
            }
        }
        return null;
    }

    @Override
    public String getImageData(String uuid) {
        return null;
    }

    @Override
    public void portDataIfNecessary() {
    }

    @Override
    public Set<Integer> getUsedChoiceIds(Field field, Record record, Record mainRecord) {
        return null;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}
