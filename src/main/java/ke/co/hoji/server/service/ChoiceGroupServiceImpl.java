package ke.co.hoji.server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.service.model.ChoiceGroupService;
import ke.co.hoji.server.dao.model.JdbcChoiceGroupDao;

@Service
public class ChoiceGroupServiceImpl implements ChoiceGroupService {

    private final JdbcChoiceGroupDao choiceGroupDao;

    @Autowired
    public ChoiceGroupServiceImpl(JdbcChoiceGroupDao choiceGroupDao) {
        this.choiceGroupDao = choiceGroupDao;
    }

    @Override
    public List<ChoiceGroup> getChoiceGroupsByTenant(Integer tenantId) {
        List<ChoiceGroup> choiceGroups = choiceGroupDao.getChoiceGroupsByTenant(tenantId);
        choiceGroups.forEach(choiceGroup -> sortChoices(choiceGroup));
        return choiceGroups;
    }

    @Override
    public ChoiceGroup getChoiceGroupById(Integer id) {
        ChoiceGroup choiceGroup = choiceGroupDao.getChoiceGroupById(id);
        sortChoices(choiceGroup);
        return choiceGroup;
    }

    @Override
    public void deleteChoiceGroup(Integer id) {
        choiceGroupDao.deleteChoiceGroup(id);
    }

    @Override
    public ChoiceGroup saveChoiceGroup(ChoiceGroup choiceGroup) {
        ChoiceGroup saved = choiceGroupDao.saveChoiceGroup(choiceGroup);
        sortChoices(saved);
        return saved;
    }

    private void sortChoices(ChoiceGroup choiceGroup) {
        if (choiceGroup.getOrderingStrategy() == ChoiceGroup.OrderingStrategy.ALPHABETICAL) {
            Utils.sortChoicesAlphabetically(choiceGroup.getChoices());
        }
        if (choiceGroup.getOrderingStrategy() == ChoiceGroup.OrderingStrategy.RANDOM) {
            Utils.sortChoicesRandomly(choiceGroup.getChoices());
        }
    }
}
