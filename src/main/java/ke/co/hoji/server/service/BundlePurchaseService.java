package ke.co.hoji.server.service;

import ke.co.hoji.server.model.CreditBundle;
import ke.co.hoji.server.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

/**
 * Manages the purchase of credits using cash i.e. the conversion of cash into credits.
 * <p>
 * Created by gitahi on 05/01/2018.
 */
@Service
public class BundlePurchaseService {

    private CashTransactionService cashTransactionService;

    private CreditTransactionService creditTransactionService;

    @Autowired
    public void setCashTransactionService(CashTransactionService cashTransactionService) {
        this.cashTransactionService = cashTransactionService;
    }

    @Autowired
    public void setCreditTransactionService(CreditTransactionService creditTransactionService) {
        this.creditTransactionService = creditTransactionService;
    }

    /**
     * Allocate a free @{@link CreditBundle} to a @{@link User}.
     *
     * @param user         the User to allocate the bundle to.
     * @param creditBundle the CreditBundle to allocate.
     * @return the expiry of the CreditBundle allocated.
     */
    public Integer allocateFreeCreditBundle(User user, CreditBundle creditBundle) {
        Integer expiry = creditBundle.getExpiry();
        creditTransactionService.deposit(
                user,
                UUID.randomUUID().toString(),
                new Date(),
                creditBundle.getCredits(),
                expiry,
                true
        );
        return expiry;
    }

    /**
     * Purchase a @{@link CreditBundle} using cash.
     *
     * @param user             the @{@link User} purchasing credits.
     * @param creditBundle     the @{@link CreditBundle} to purchase.
     * @param date             the date of this transaction.
     * @param paymentMode      the mode of payment e.g. MPESA
     * @param paymentReference the reference number of this transaction.
     * @param newCash          the amount of cash (in USD) the @{link User} is paying now.
     */
    @Transactional
    public void purchaseBundle(
            User user,
            CreditBundle creditBundle,
            Date date,
            String paymentMode,
            String paymentReference,
            BigDecimal newCash
    ) {
        BigDecimal currentCashBalance = cashTransactionService.balance(user);
        BigDecimal newCashBalance = currentCashBalance.add(newCash);
        BigDecimal price = calculatePrice(creditBundle);
        String referenceNo = UUID.randomUUID().toString();
        if (newCashBalance.compareTo(price) >= 0) {
            Integer credits = calculateCredits(creditBundle);
            cashTransactionService.deposit(user, referenceNo, date, newCash, paymentMode, paymentReference);
            cashTransactionService.expense(user, referenceNo, date, price);
            creditTransactionService.deposit(user, referenceNo, date, credits, creditBundle.getExpiry());
        } else {
            cashTransactionService.deposit(user, referenceNo, date, newCash, paymentMode, paymentReference);
        }
    }

    /**
     * Checks if a cash deposit with the attributes provided has previously been processed. This is necessary to avoid
     * processing the same deposit twice.
     *
     * @param user        the @{@link User} who made the deposit.
     * @param date        the date the deposit was made.
     * @param referenceNo the reference number of the deposit transaction e.g. M-PESA code.
     * @return true if the deposit has previously been processed and false otherwise.
     */
    public boolean depositProcessed(User user, Date date, String referenceNo) {
        return cashTransactionService.transactionExists(user, date, referenceNo);
    }

    /*
     * Calculate the prevailing cost of a @{@link CreditBundle}, taking into account any applicable discount.
     */
    private BigDecimal calculatePrice(CreditBundle creditBundle) {
        BigDecimal bundlePrice = creditBundle.getPrice();
        if (creditBundle.getDiscount() > 0) {
            BigDecimal discount = new BigDecimal(creditBundle.getDiscount()).divide(new BigDecimal(100)).multiply(bundlePrice);
            bundlePrice = bundlePrice.subtract(discount);
        }
        return bundlePrice;
    }

    /*
     * Calculate the prevailing credits of a @{@link CreditBundle}, taking into account any applicable bonus.
     */
    private Integer calculateCredits(CreditBundle creditBundle) {
        BigDecimal bundleCredits = new BigDecimal(creditBundle.getCredits());
        if (creditBundle.getBonus() > 0) {
            BigDecimal bonus = new BigDecimal(creditBundle.getBonus()).divide(new BigDecimal(100)).multiply(bundleCredits);
            bundleCredits = bundleCredits.add(bonus);
        }
        return bundleCredits.toBigInteger().intValue();
    }
}
