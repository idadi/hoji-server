package ke.co.hoji.server.service;

import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.server.dao.model.JdbcChoiceGroupDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by geoffreywasilwa on 24/04/2017.
 */
@Service
public class ChoiceGroupUploadService {

    @Autowired
    private JdbcChoiceGroupDao choiceGroupDao;

    @Transactional
    public List<ChoiceGroup> saveOrUpdate(int userId, Collection<ChoiceGroup> choiceGroups) {
        List<ChoiceGroup> localGroups = new ArrayList<>();
        for (ChoiceGroup choiceGroup : choiceGroups) {
            ChoiceGroup copy = new ChoiceGroup(choiceGroup);
            copy.setUserId(userId);
            localGroups.add(copy);
        }

        localGroups = choiceGroupDao.saveChoiceGroups(localGroups);
        return localGroups;
    }

}
