package ke.co.hoji.server.service;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.errors.OverDailyLimitException;
import com.google.maps.errors.OverQueryLimitException;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.Record;
import ke.co.hoji.server.repository.MainRecordRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Uses the google maps api to try and resolve address information for {@link MainRecord}s collected by client devices.
 * To ensure that all {@link MainRecord}s get at chance at having their address information resolved, this class keeps
 * track of the last {@link MainRecord}'s date created that was processed. The date is used to fetch the next set of
 * {@link MainRecord}s that need to be processed.
 * </p>
 * Created by geoffreywasilwa on 31/03/2017.
 */
@Service
public class GeoAddressResolverService {

    private Logger logger = LoggerFactory.getLogger(getClass());
    private final MainRecordRepository mainRecordRepository;
    private final GeoApiContext context;

    public GeoAddressResolverService(
            @Autowired MainRecordRepository mainRecordRepository,
            @Value("${google.api.key}") String apiKey) {
        this.mainRecordRepository = mainRecordRepository;
        this.context = new GeoApiContext.Builder().apiKey(apiKey).build();
    }

    /**
     * The {@link Record#dateCreated} value for the last {@link MainRecord} we fetched.
     */
    private long lastFetchTime = 0;

    /**
     * Use google Geocoding api to try and resolve records without address information
     * @param batchSize the number of records to try and resolve
     */
    public void resolveAddresses(int batchSize) {
        List<MainRecord> mainRecordsWithoutAddresses = getMainRecords(batchSize);
        List<MainRecord> updatedMainRecords = new ArrayList<>();
        if (mainRecordsWithoutAddresses.size() > 0) {
            for (MainRecord mainRecord : mainRecordsWithoutAddresses) {
                GeocodingResult[] results = new GeocodingResult[0];
                try {
                    results = GeocodingApi.reverseGeocode(
                            context,
                            new LatLng(mainRecord.getStartLatitude(), mainRecord.getStartLongitude())
                    ).await();
                    if (results.length > 0) {
                        mainRecord.setStartAddress(results[0].formattedAddress);
                        updatedMainRecords.add(mainRecord);
                    }
                } catch (ApiException e) {
                    logger.error(e.getMessage());
                    if (e instanceof OverDailyLimitException || e instanceof OverQueryLimitException) {
                        return;
                    }
                } catch (InterruptedException | IOException e) {
                    logger.error(e.getMessage());
                }
            }
        }
        if (updatedMainRecords.size() > 0) {
            mainRecordRepository.saveAll(updatedMainRecords);
        }
    }

    private List<MainRecord> getMainRecords(int batchSize) {
        List<MainRecord> mainRecords = mainRecordRepository.findRecordsWithoutAddress(lastFetchTime, batchSize);
        if (mainRecords.size() == 0) {
            lastFetchTime = 0;
        } else {
            lastFetchTime = mainRecords.get(mainRecords.size() - 1).getDateCreated();
        }
        return mainRecords;
    }
}
