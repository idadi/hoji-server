package ke.co.hoji.server.service;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.Constants.Server;
import ke.co.hoji.core.data.PublicSurveyAccess;
import ke.co.hoji.server.dao.impl.JdbcUserDao;
import ke.co.hoji.server.model.CreditTransactionConstants;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.User.SlaveStatus;
import ke.co.hoji.server.model.VerificationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextListener;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gitahi on 02/10/15.
 */
@Service
public class UserService implements UserDetailsManager {

    private final JdbcUserDao userDao;
    private final PasswordEncoder passwordEncoder;

    private final Map<Integer, User> cache = new HashMap<>();

    /**
     * The URL of the current running instance of the Hoji web application.
     */
    private String applicationUrl;

    @Autowired
    public UserService(JdbcUserDao userDao, PasswordEncoder passwordEncoder, ApplicationEventPublisher applicationEventPublisher) {
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
    }

    public User getCurrentUser() {
        User user = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            user = ((User) principal);
        }
        return user;
    }

    /**
     * Obtains the {@link HttpServletRequest} and prepares the application url. Formerly used
     * {@link RequestContextListener} as declared in web.xml. Now just {@link Constants.Server#URL} because
     * the former method is not possible when processing events asynchronously.
     *
     * @return application url
     */
    public String getApplicationUrl() {
        if (applicationUrl == null) {
            applicationUrl = Server.URL;
        }
        return applicationUrl;
    }

    @Override
    @Transactional
    public void createUser(UserDetails userDetails) {
        User user = (User) userDetails;
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setCode(Utils.ueid());
        user.setRoles(User.initialRoles());
        user.setEnabled(true);
        user.setVerified(false);
        user.setDateCreated(new Date());
        if (user.getPublicSurveyAccess() == null) {
            //TODO: Once we implement some way to activate the Sample Project server side we should change this to PublicSurveyAccess.NONE
            user.setPublicSurveyAccess(PublicSurveyAccess.VERIFIED);
        }
        user.setTenantUserId(user.getId());
        user.setType(User.Type.PREPAID);
        user.setMinCredits(CreditTransactionConstants.DEFAULT_MIN_CREDITS);
        user.setTaxRate(CreditTransactionConstants.DEFAULT_TAX_RATE);
        user.setDiscountRate(CreditTransactionConstants.DEFAULT_DISCOUNT_RATE);
        user.setMaster(Boolean.FALSE);
        user.setSlaveStatus(SlaveStatus.INACTIVE);
        try {
            userDao.createUser(user);
            cacheUser(user);
        } catch (DuplicateKeyException ex) {
            //Error presumably happens because the user code set above already exists
            //Re-call this method to generate new code
            createUser(userDetails);
        }
    }

    @Override
    @Transactional
    public void updateUser(UserDetails userDetails) {
        User user = (User) userDetails;
        userDao.modifyUser(user);
        cacheUser(user);
    }

    public void changeType(User user) {
        userDao.changeType(user);
    }

    public void verifyUser(UserDetails userDetails) {
        User user = (User) userDetails;
        userDao.verifyUser(user);
        cacheUser(user);
    }

    public void suspendUser(UserDetails userDetails) {
        User user = (User) userDetails;
        userDao.suspendUser(user);
        cacheUser(user);
    }

    @Override
    public void deleteUser(String username) {
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
    }

    public void changeEmail(User user) {
        userDao.changeEmail(user);
        cacheUser(user);
    }

    public void changePassword(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userDao.changePassword(user);
    }

    @Override
    public boolean userExists(String username) {
        try {
            loadUserByUsername(username);
            return true;
        } catch (UsernameNotFoundException ex) {
            return false;
        }
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username)
        throws UsernameNotFoundException {
        User user = userDao.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Username not found");
        }
        return user;
    }

    public User findById(Integer id) {
        if (!cache.containsKey(id)) {
            cacheUser(userDao.findById(id));
        }
        return cache.get(id);
    }

    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }

    public User findByCode(String code) {
        return userDao.findByCode(code);
    }

    /**
     * Find all users operating under the given tenant account.
     *
     * @param tenantId the id of the tenant
     * @return the users under this tenant.
     */
    public List<User> findByTenantId(Integer tenantId) {
        List<User> users = userDao.findByTenantId(tenantId);
        List<User> ret = new ArrayList<>();
        for (User user : users) {
            if (!user.getId().equals(tenantId)) {
                ret.add(user);
            }
        }
        return ret;
    }

    public List<User> findByIds(Collection<Integer> ids) {
        return userDao.findByIds(ids);
    }

    /**
     * Find all Users.
     *
     * @param enabled true to return only enabled users, false to return only disabled users and null to return all.
     * @return a list of all users meeting the enabled criteria above.
     */
    public List<User> findAllUsers(Boolean enabled) {
        return userDao.findAllUsers(enabled);
    }

    public boolean verifyPassword(User user, String password) {
        return passwordEncoder.matches(password, user.getPassword());
    }

    public void createVerificationToken(VerificationToken verificationToken) {
        userDao.createVerificationToken(verificationToken);
    }

    public void deleteVerificationTokens(int type, User user) {
        userDao.deleteVerificationTokens(type, user);
    }

    public VerificationToken getVerificationToken(String token, int type) {
        return userDao.getVerificationToken(token, type);
    }

    public VerificationToken getLatestVerificationToken(User user) {
        return userDao.getLatestVerificationToken(user);
    }

    @Transactional
    public void expelUser(User user) {
        userDao.expelUser(user);
    }

    private void cacheUser(User user) {
        User userToCache = User.newInstance(user);
        cache.put(user.getId(), userToCache);
    }
}
