package ke.co.hoji.server.service;

import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.PublicSurveyAccess;
import ke.co.hoji.core.data.dto.Field;
import ke.co.hoji.core.data.dto.Form;
import ke.co.hoji.core.data.dto.Language;
import ke.co.hoji.core.data.dto.Reference;
import ke.co.hoji.core.data.dto.Survey;
import ke.co.hoji.core.data.dto.Translation;
import ke.co.hoji.core.data.http.FieldsResponse;
import ke.co.hoji.core.data.http.FormsResponse;
import ke.co.hoji.core.data.http.LanguagesResponse;
import ke.co.hoji.core.data.http.PropertiesResponse;
import ke.co.hoji.core.data.http.ReferencesResponse;
import ke.co.hoji.core.data.http.SurveyResponse;
import ke.co.hoji.core.data.http.SurveysResponse;
import ke.co.hoji.core.data.http.TranslationsResponse;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.server.dao.dto.JdbcFieldDao;
import ke.co.hoji.server.dao.dto.JdbcFormDao;
import ke.co.hoji.server.dao.dto.JdbcLanguageDao;
import ke.co.hoji.server.dao.dto.JdbcReferenceDao;
import ke.co.hoji.server.dao.dto.JdbcSurveyDao;
import ke.co.hoji.server.dao.dto.JdbcTranslationDao;
import ke.co.hoji.server.model.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gitahi on 27/08/15.
 */
@Service
public class DownloadService {

    @Qualifier("dtoJdbcSurveyDao")
    private final JdbcSurveyDao surveyDao;
    @Qualifier("dtoJdbcFormDao")
    private final JdbcFormDao formDao;
    @Qualifier("dtoJdbcFieldDao")
    private final JdbcFieldDao fieldDao;
    @Qualifier("dtoJdbcReferenceDao")
    private final JdbcReferenceDao referenceDao;
    @Qualifier("dtoJdbcLanguageDao")
    private final JdbcLanguageDao languageDao;
    private final PropertyService propertyService;
    @Qualifier("dtoJdbcTranslationDao")
    private final JdbcTranslationDao translationDao;
    private final UserService userService;

    public DownloadService(
        JdbcSurveyDao surveyDao,
        JdbcFormDao formDao,
        JdbcFieldDao fieldDao,
        JdbcReferenceDao referenceDao,
        JdbcLanguageDao languageDao,
        PropertyService propertyService,
        JdbcTranslationDao translationDao,
        UserService userService
    ) {
        this.surveyDao = surveyDao;
        this.formDao = formDao;
        this.fieldDao = fieldDao;
        this.referenceDao = referenceDao;
        this.languageDao = languageDao;
        this.propertyService = propertyService;
        this.translationDao = translationDao;
        this.userService = userService;
    }

    public SurveysResponse getSurveys(User user) {
        User tenant = userService.findById(user.getTenantUserId());
        List<Survey> tenantSurveys = surveyDao.getSurveysByTenantUserId(tenant.getId(), Boolean.TRUE);
        if (tenant.getPublicSurveyAccess() != PublicSurveyAccess.NONE) {
            List<Survey> publicSurveys = surveyDao.getPublicSurveys(tenant.getPublicSurveyAccess());
            for (Survey publicSurvey : publicSurveys) {
                if (publicSurvey.getAccess() != ke.co.hoji.core.data.model.Survey.Access.PUBLIC_VIEW) {
                    if (!tenantSurveys.contains(publicSurvey)) {
                        tenantSurveys.add(publicSurvey);
                    }
                }
            }
        }
        List<SurveyResponse> surveyResponses = new ArrayList<>();
        for (Survey survey : tenantSurveys) {
            User owner = userService.findById(survey.getUserId());
            SurveyResponse surveyResponse = new SurveyResponse();
            surveyResponse.setCode(SurveyResponse.Code.SURVEY_FOUND);
            surveyResponse.setSurvey(survey);
            surveyResponse.setOwnerId(owner.getId());
            surveyResponse.setOwnerName(owner.getFullName());
            surveyResponse.setOwnerVerified(false);
            if (survey.getAccess() != ke.co.hoji.core.data.model.Survey.Access.PUBLIC_VIEW) {
                surveyResponses.add(surveyResponse);
            }
        }
        return new SurveysResponse(surveyResponses);
    }

    public SurveyResponse downloadSurvey(Integer surveyId) {
        Integer appVersion = (Constants.Device.CODE_TO_ID_MILESTONE + 1);
        return downloadSurvey(surveyId, appVersion);
    }

    public SurveyResponse downloadSurvey(Integer surveyId, Integer appVersion) {
        SurveyResponse response = new SurveyResponse();
        if (appVersion == null || appVersion <= Constants.Device.CODE_TO_ID_MILESTONE) {
            response.setCode(SurveyResponse.Code.APP_VERSION_TOO_OLD);
        } else {
            Survey survey = surveyDao.getSurveyById(surveyId);
            if (survey == null) {
                response.setCode(SurveyResponse.Code.SURVEY_NOT_FOUND);
            } else {
                if (!survey.isEnabled()) {
                    response.setCode(SurveyResponse.Code.SURVEY_NOT_ENABLED);
                } else if (survey.getStatus() != ke.co.hoji.core.data.model.Survey.PUBLISHED) {
                    response.setCode(SurveyResponse.Code.SURVEY_NOT_PUBLISHED);
                } else {
                    response.setCode(SurveyResponse.Code.SURVEY_FOUND);
                    response.setSurvey(survey);
                }
            }
            if (survey != null) {
                if (survey.isEnabled() && survey.getStatus() == ke.co.hoji.core.data.model.Survey.PUBLISHED) {
                    User user = userService.findById(survey.getUserId());
                    response.setSurvey(survey);
                    response.setOwnerId(user.getId());
                    response.setOwnerName(user.getFullName());
                    response.setOwnerVerified(false);
                }
            }
        }
        return response;
    }

    public FormsResponse downloadForms(Integer surveyId) {
        FormsResponse response = new FormsResponse();
        List<Form> forms = formDao.getFormsBySurvey(surveyId);
        response.setForms(forms);
        return response;
    }

    public FieldsResponse downloadFields(Integer formId) {
        FieldsResponse response = new FieldsResponse();
        List<Field> fields = fieldDao.getFieldsByForm(formId);
        response.setFields(fields);
        return response;
    }

    public ReferencesResponse downloadReferences(Integer surveyId) {
        ReferencesResponse response = new ReferencesResponse();
        List<Reference> references = referenceDao.getReferencesBySurvey(surveyId);
        response.setReferences(references);
        return response;
    }

    public LanguagesResponse downloadLanguages(Integer surveyId) {
        LanguagesResponse response = new LanguagesResponse();
        response.setLanguages(languageDao.getLanguagesBySurvey(surveyId));
        return response;
    }

    public TranslationsResponse downloadTranslations(List<Language> languages) {
        TranslationsResponse response = new TranslationsResponse();
        List<Translation> translations = new ArrayList<>();
        for (Language language : languages) {
            translations.addAll(translationDao.getTranslationsByLanguage(language.getId()));
        }
        response.setTranslations(translations);
        return response;
    }

    public PropertiesResponse downloadProperties(Integer surveyId) {
        List<Property> properties = propertyService.getProperties(surveyId);
        PropertiesResponse response = new PropertiesResponse();
        response.setProperties(properties);
        return response;
    }
}
