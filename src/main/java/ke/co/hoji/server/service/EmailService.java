package ke.co.hoji.server.service;

import ke.co.hoji.server.event.event.HojiApplicationEvent;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

/**
 * The parent for all classes used for emailing.
 */
public abstract class EmailService {

    /**
     * The different addresses from which we might send emails.
     */
    public enum SourceEmail {

        /**
         * Unmonitored email address for informational messages e.g. account verification and password resets.
         */
        NO_REPLY("no-reply@hoji.co.ke", "Hoji"),

        /**
         * Monitored email for providing customer support to our clients.
         */
        SUPPORT("support@hoji.co.ke", "Hoji"),

        /**
         * CEO's email for sending a welcome message upon sign-up..
         */
        CEO("gitahi@hoji.co.ke", "Gitahi Ng'ang'a"),

        /**
         * Customer success officer's email for onboarding users.
         */
        CUSTOMER_SUCCESS("dkaranja@hoji.co.ke", "David Karanja");

        /**
         * The actual email address for this source email.
         */
        private String address;

        /**
         * The name associated with this source email.
         */
        private String name;

        SourceEmail(String address, String name) {
            this.address = address;
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public String getName() {
            return name;
        }
    }

    @Value("${server.url}")
    private String serverUrl;

    @Autowired
    private LoggableEventService loggableEventService;

    /**
     * The default {@link SourceEmail} to use when none is specified.
     */
    private static final SourceEmail DEFAULT_SOURCE_EMAIL = SourceEmail.NO_REPLY;

    public String getServerUrl() {
        return serverUrl;
    }

    /**
     * Checks whether a given {@link EventType} has previously been logged for the given {@link User}. This is used
     * as a proxy for whether an email has previously been sent in order to avoid sending once in a life time emails
     * a second time. See: {@link ke.co.hoji.server.event.model.EventType#onceInALifeTime}.
     *
     * @param eventTypeName the name of the {@link EventType}.
     * @param userId        the id of the {@link User}.
     * @return true if the event exists and false otherwise.
     */
    protected final boolean isFirstForUser(String eventTypeName, Integer userId) {
        return loggableEventService.isFirstForUser(eventTypeName, userId);
    }

    public abstract void sendEmail(
        User to,
        SourceEmail sourceEmail,
        String subject,
        String body,
        HojiApplicationEvent event
    );

    public void sendEmail(User to, String subject, String body, HojiApplicationEvent event) {
        sendEmail(to, DEFAULT_SOURCE_EMAIL, subject, body, event);
    }
}
