package ke.co.hoji.server.service;

import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.http.UploadResult;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.MainRecord.Stage;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.server.event.event.MainRecordEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.repository.MainRecordRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Handles uploading records by persisting them on the server database one at a time.
 * <p>
 * Created by gitahi on 05/07/15.
 */

@Service
public class UploadService {

    private static final Logger logger = LoggerFactory.getLogger(UploadService.class);

    /**
     * A cache that stores IDs of {@link Form}s and how many credits those Forms cost, so that we do not have to
     * recompute the credits every time, unless the form version has changed.
     */
    private final Map<Integer, Integer> creditsCache = new HashMap<>();

    /**
     * A cache that stores the survey codes of {@link Survey}s and which user owns those Surveys, so that we do not
     * have to query teh database every time.
     */
    private final Map<String, User> surveyCodeUserMap = new HashMap<>();
    private final UserService userService;
    private final FormService formService;
    private final CreditTransactionService creditTransactionService;
    private final MainRecordRepository mainRecordRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public UploadService( UserService userService,
        FormService formService,
        CreditTransactionService creditTransactionService,
        MainRecordRepository mainRecordRepository,
        ApplicationEventPublisher applicationEventPublisher
    ) {
        this.userService = userService;
        this.formService = formService;
        this.creditTransactionService = creditTransactionService;
        this.mainRecordRepository = mainRecordRepository;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    /**
     * Uploads a {@link MainRecord}.
     *
     * @param form           the @{@link Form} associated with the {@link MainRecord} here.
     * @param mainRecord     the {@link MainRecord} to be uploaded.
     * @param appVersionCode the version of the app trying to upload a record
     * @return the @{@link UploadResult}.
     */
    @Transactional
    public UploadResult uploadRecord(Form form, MainRecord mainRecord, int appVersionCode) {
        boolean canUpload = true;
        Survey survey = form.getSurvey();
        User owner = getUser(survey);
        User payer = owner;
        if (owner.isSlave()) {
            if (owner.getMasterUser() == null) {
                return rejectUpload(survey, mainRecord, "No master user");
            } else {
                if (owner.getMasterUser().isMaster()) {
                    payer = owner.getMasterUser();
                } else {
                    return rejectUpload(survey, mainRecord, "Invalid master user");
                }
            }
        }
        Integer credits = computeCredits(form);
        Date date = new Date();
        if (!mainRecord.isTest()) {
            logger.info("Checking if user [id= {}] has sufficient credits to upload a record.", payer.getId());
            canUpload = creditTransactionService.expense(
                payer,
                form,
                mainRecord.getUuid(),
                date,
                credits
            );
        }
        if (canUpload) {
            return proceedWithUpload(survey, mainRecord);
        } else {
            logger.error(
                    "Unable to process upload for record [id= {}, formId= {}] because of insufficient funds",
                    mainRecord.getUuid(),
                    mainRecord.getFormId()
            );
            return rejectUpload(survey, mainRecord, "Insufficient credits");
        }
    }

    private UploadResult proceedWithUpload(Survey survey, MainRecord mainRecord) {
        mainRecord.setVersion(mainRecord.getVersion() + 1);
        mainRecord.setDateUploaded(new Date().getTime());
        mainRecord.setStage(Stage.UPLOADED);
        User currentUser = userService.getCurrentUser();
        EventType eventType = EventType.MAIN_RECORD_MODIFY;
        if (mainRecord.getId() == null) {
            MainRecord existing = mainRecordRepository.findByUuid(mainRecord.getUuid());
            if (existing != null) {
                mainRecord.setId(existing.getId());
            } else {
                eventType = EventType.MAIN_RECORD_CREATE;
            }
        }
        MainRecord saved = mainRecordRepository.save(mainRecord);
        MainRecordEvent mainRecordEvent = new MainRecordEvent(
            EventSource.MOBILE,
            currentUser,
            mainRecord,
            eventType
        );
        mainRecordEvent.putProperty(MainRecordEvent.SURVEY, survey);
        applicationEventPublisher.publishEvent(mainRecordEvent);
        logger.info(
                "Successfully uploaded record [id= {}, formId= {}]",
                mainRecord.getUuid(),
                mainRecord.getFormId()
        );
        return new UploadResult(UploadResult.SUCCESS, saved.getId(), saved.getDateUploaded());
    }

    private UploadResult rejectUpload(Survey survey, MainRecord mainRecord, String rejectionMessage) {
        MainRecordEvent mainRecordEvent = new MainRecordEvent(
            EventSource.MOBILE,
            userService.getCurrentUser(),
            mainRecord,
            EventType.MAIN_RECORD_CREATE
        );
        mainRecordEvent.putProperty(MainRecordEvent.SURVEY, survey);
        mainRecordEvent.setResult(EventResult.FAILURE);
        mainRecordEvent.setFailureMessage(rejectionMessage);
        applicationEventPublisher.publishEvent(mainRecordEvent);
        return new UploadResult(UploadResult.INSUFFICIENT_CREDITS);
    }

    private User getUser(Survey survey) {
        User user = surveyCodeUserMap.get(survey.getCode());
        if (user == null) {
            user = userService.findById(survey.getUserId());
            surveyCodeUserMap.put(survey.getCode(), user);
        }
        return user;
    }

    private int computeCredits(Form form) {
        Integer credits = creditsCache.get(form.getId());
        if (credits == null) {
            credits = formService.computeCredits(form);
            creditsCache.put(form.getId(), credits);
        }
        return credits;
    }

    /**
     * Called by the {@link PublicationService} whenever the major or minor version of a {@link Form} changes so that
     * {@link #creditsCache} may be refreshed.
     *
     * @param form the Form whose major or minor version has changed.
     */
    public void formVersionChanged(Form form) {
        creditsCache.remove(form.getId());
    }
}
