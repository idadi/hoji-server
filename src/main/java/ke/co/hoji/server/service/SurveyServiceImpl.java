package ke.co.hoji.server.service;

import ke.co.hoji.core.data.PublicSurveyAccess;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.SurveyService;
import ke.co.hoji.server.dao.model.JdbcSurveyDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SurveyServiceImpl implements SurveyService {

    private final JdbcSurveyDao surveyDao;

    @Autowired
    public SurveyServiceImpl(JdbcSurveyDao surveyDao) {
        this.surveyDao = surveyDao;
    }

    @Override
    public Survey getSurveyById(Integer surveyId) {
        return surveyDao.getSurveyById(surveyId);
    }

    @Override
    public List<Survey> getAllSurveys() {
        return surveyDao.getAllSurveys();
    }

    @Override
    public List<Survey> getSurveysByTenantUserId(Integer tenantUserId, int publicSurveyAccess) {
        List<Survey> tenantSurveys = surveyDao.getSurveysByTenantUserId(tenantUserId, null);
        if (publicSurveyAccess != PublicSurveyAccess.NONE) {
            List<Survey> publicSurveys = surveyDao.getPublicSurveys(publicSurveyAccess);
            for (Survey publicSurvey : publicSurveys) {
                if (publicSurvey.isEnabled() && publicSurvey.getAccess() != Survey.Access.PUBLIC_ENTRY) {
                    if (!tenantSurveys.contains(publicSurvey)) {
                        tenantSurveys.add(publicSurvey);
                    }
                }
            }
        }
        return tenantSurveys;
    }

    @Override
    public Survey saveSurvey(Survey survey) {
        return surveyDao.saveSurvey(survey);
    }

    @Override
    public void deleteSurvey(Integer surveyId) {
        surveyDao.deleteSurvey(surveyId);
    }

    @Override
    public void enableSurvey(Survey survey, boolean enabled) {
        surveyDao.enableSurvey(survey, enabled);
        survey.setEnabled(enabled);
    }

    @Override
    public boolean surveyExists(String surveyCode) {
        Survey survey = surveyDao.getSurveyByCode(surveyCode);
        return survey != null;
    }

    @Override
    public void setPublishedStatus(Survey survey, Integer status) {
        surveyDao.setPublishedStatus(survey, status);
    }

}
