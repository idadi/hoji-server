package ke.co.hoji.server.service;

import ke.co.hoji.server.dao.impl.JdbcLoggableEventDao;
import ke.co.hoji.server.event.model.LoggableEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class LoggableEventService {

    private final JdbcLoggableEventDao loggableEventDao;

    @Autowired
    public LoggableEventService(JdbcLoggableEventDao loggableEventDao) {
        this.loggableEventDao = loggableEventDao;
    }

    public void log(LoggableEvent loggableEvent) {
        loggableEventDao.log(loggableEvent);
    }

    public List<LoggableEvent> findEventsForReviewSolicitation(Date from, Date to) {
        return loggableEventDao.findEventsForReviewSolicitation(from, to);
    }

    public boolean isFirstForUser(String eventTypeName, Integer userId) {
        return loggableEventDao.isFirstForUser(eventTypeName, userId);
    }
}
