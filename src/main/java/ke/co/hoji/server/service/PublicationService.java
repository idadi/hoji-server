package ke.co.hoji.server.service;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.dao.model.JdbcFormDao;
import ke.co.hoji.server.dao.model.JdbcSurveyDao;
import ke.co.hoji.server.event.event.FormEvent;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.repository.MainRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A service class that allows:
 * <p>
 * 1) publishing {@link Survey} that has been modified
 * 2) clearing data collected for a given {@link Survey}
 * 3) clearing specific data when a {@link Form} or {@link Field} are deleted
 * </p>
 * Created by geoffreywasilwa on 24/05/2017.
 */
@Service
public class PublicationService {

    private final MainRecordRepository mainRecordRepository;
    private final UploadService uploadService;
    private UserService userService;
    private ApplicationEventPublisher applicationEventPublisher;
    private final JdbcSurveyDao surveyDao;
    private final JdbcFormDao formDao;

    @Autowired
    public PublicationService(
            MainRecordRepository mainRecordRepository,
            UploadService uploadService,
            UserService userService,
            ApplicationEventPublisher applicationEventPublisher,
            JdbcSurveyDao surveyDao,
            JdbcFormDao formDao
    ) {

        this.mainRecordRepository = mainRecordRepository;
        this.uploadService = uploadService;
        this.userService = userService;
        this.applicationEventPublisher = applicationEventPublisher;
        this.surveyDao = surveyDao;
        this.formDao = formDao;
    }

    @Transactional
    public void publish(Survey survey) {
        if (survey.getStatus() != Survey.PUBLISHED) {
            surveyDao.setPublishedStatus(survey, Survey.PUBLISHED);
            User user = userService.getCurrentUser();
            for (Form form : survey.getForms()) {
                if (form.isMajorModified()) {
                    formDao.incrementMajorVersion(form);
                    applicationEventPublisher.publishEvent(new FormEvent(EventSource.WEB, user,
                            form, EventType.INCREMENT_MAJOR_VERSION));
                } else {
                    if (form.isMinorModified()) {
                        formDao.incrementMinorVersion(form);
                        applicationEventPublisher.publishEvent(new FormEvent(EventSource.WEB, user,
                                form, EventType.INCREMENT_MINOR_VERSION));
                    }
                }
                uploadService.formVersionChanged(form);
            }
            survey.setStatus(Survey.PUBLISHED);
        }
    }

    public void clearData(Survey survey) {
        mainRecordRepository.clearData(survey);
    }

    public void clearData(Form form) {
        mainRecordRepository.clearData(form);
    }

    public void clearData(Field field) {
        mainRecordRepository.clearData(field);
    }
}
