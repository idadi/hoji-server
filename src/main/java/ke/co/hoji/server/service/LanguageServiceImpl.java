package ke.co.hoji.server.service;

import ke.co.hoji.core.data.model.Language;
import ke.co.hoji.core.service.model.LanguageService;
import ke.co.hoji.server.dao.model.JdbcLanguageDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LanguageServiceImpl implements LanguageService {

    private final JdbcLanguageDao languageDao;

    @Autowired
    public LanguageServiceImpl(JdbcLanguageDao languageDao) {
        this.languageDao = languageDao;
    }

    @Override
    public List<Language> findAllLanguages() {
        return languageDao.getAllLanguages();
    }

}
