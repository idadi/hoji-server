package ke.co.hoji.server.service;

import ke.co.hoji.core.data.Translatable;
import ke.co.hoji.core.service.model.TranslationService;
import org.springframework.stereotype.Service;

@Service
public class TranslationServiceImpl implements TranslationService {

    @Override
    public String translate(Translatable translatable, Integer languageId, String component, String untranslated) {
        return untranslated;
    }

    @Override
    public void clearCache() {

    }
}
