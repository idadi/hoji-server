package ke.co.hoji.server.service;

import ke.co.hoji.core.data.ConfigObject;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.http.ImportRequest;
import ke.co.hoji.core.data.http.ImportResponse;
import ke.co.hoji.core.data.http.ResultCode;
import ke.co.hoji.core.data.http.SearchRequest;
import ke.co.hoji.core.data.http.SearchResponse;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.core.service.model.SurveyService;
import ke.co.hoji.server.repository.MainRecordRepository;
import ke.co.hoji.server.repository.SearchTermFilter;
import ke.co.hoji.server.repository.SearchTermFilter.ImportFilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by geoffreywasilwa on 16/11/2016.
 */
@Service
public class ImportService {

    private final Logger logger = LoggerFactory.getLogger(ImportService.class);
    private final MainRecordRepository mainRecordRepository;
    private final SurveyService surveyService;
    private final FormService formService;
    private final FieldService fieldService;

    @Autowired
    public ImportService(MainRecordRepository mainRecordRepository, SurveyService surveyService, FormService formService, FieldService fieldService) {
        this.mainRecordRepository = mainRecordRepository;
        this.surveyService = surveyService;
        this.formService = formService;
        this.fieldService = fieldService;
    }

    public SearchResponse search(SearchRequest searchRequest) {
        RequestValidator validator = new RequestValidator(searchRequest.getSurveyId(), searchRequest.getFormId(),
            searchRequest.getFormVersion());
        int resultCode = validator.validate();
        SearchResponse searchResponse = new SearchResponse();
        searchResponse.setResultCode(resultCode);
        if (resultCode == ResultCode.OKAY) {
            String[] searchTerms = searchRequest.getSearchTerms().split(" ");
            long from = searchRequest.getFromDate() != null ? searchRequest.getFromDate() : 0L;
            long to = searchRequest.getToDate() != null ? searchRequest.getToDate() : new Date().getTime();
            SearchTermFilter filter = new ImportFilterBuilder(validator.getForm())
                .from(new Date(from))
                .to(new Date(to))
                .matchSensitivity(searchRequest.getMatchSensitivity())
                .searchTerms(searchTerms)
                .userId(searchRequest.getUserId())
                .build();
            searchResponse.setLiteRecords(mainRecordRepository.findLiteRecords(filter));
        }
        return searchResponse;
    }

    public ImportResponse getRecord(ImportRequest importRequest) {
        RequestValidator validator = new RequestValidator(importRequest.getSurveyId(), importRequest.getFormId(),
            importRequest.getFormVersion());
        int resultCode = validator.validate();
        ImportResponse importResponse = new ImportResponse();
        importResponse.setResultCode(resultCode);
        importResponse.setMainRecordMap(new LinkedHashMap<>());
        if (resultCode == ResultCode.OKAY) {
            Form form = formService.getFormById(importRequest.getFormId());
            for (String uuid : importRequest.getUuids()) {
                List<MainRecord> mainRecords = mainRecordRepository.findMainRecords(uuid, form.getDependentForms());
                for (MainRecord mainRecord : mainRecords) {
                    if (!mainRecord.getFormId().equals(form.getId())) {
                        continue;
                    }
                    Map<Integer, Field> imageFields = fieldService.getAllFields(form)
                            .stream()
                            .filter(field -> field.getType().isImage())
                            .collect(Collectors.toMap(ConfigObject::getId, field -> field));
                    if (imageFields.size() == 0) {
                        continue;
                    }
                    Set<Integer> imageFieldIds = imageFields.keySet();
                    List<LiveField> allLiveFields = new ArrayList<>(mainRecord.getLiveFields());
                    if (mainRecord.getMatrixRecords() != null) {
                        allLiveFields.addAll(
                                mainRecord.getMatrixRecords()
                                        .stream()
                                        .flatMap(matrixRecord -> matrixRecord.getLiveFields().stream())
                                        .collect(Collectors.toList())
                        );
                    }
                    allLiveFields
                            .stream()
                            .filter(liveField -> liveField.getValue() != null && imageFieldIds.contains(liveField.getFieldId()))
                            .forEach(liveField -> {
                                String imageString = null;
                                File imageFile = new File(liveField.getValue().toString());
                                try {
                                    imageString = Base64.getEncoder().encodeToString(Files.readAllBytes(imageFile.toPath()));
                                } catch (IOException e) {
                                    logger.error("Unable to read image: " + imageFile, e);
                                }
                                liveField.setValue(imageString);
                            });
                }
                importResponse.getMainRecordMap().put(uuid, mainRecords);
            }
        }
        return importResponse;
    }

    private class RequestValidator {

        private final Integer surveyId;
        private final Integer formId;
        private final Integer formVersion;
        private Survey survey;
        private Form form;

        RequestValidator(Integer surveyId, Integer formId, Integer formVersion) {
            this.surveyId = surveyId;
            this.formId = formId;
            this.formVersion = formVersion;
        }

        int validate() {
            survey = surveyService.getSurveyById(surveyId);
            form = formService.getFormById(formId);
            if (survey == null || form == null) {
                return ResultCode.FORM_NOT_FOUND;
            } else if (!survey.isEnabled()) {
                return ResultCode.SURVEY_NOT_ENABLED;
            } else if (survey.getStatus() != Survey.PUBLISHED) {
                return ResultCode.SURVEY_NOT_PUBLISHED;
            } else if (form.getMajorVersion() > formVersion) {
                return ResultCode.SURVEY_OUT_OF_DATE;
            } else {
                return ResultCode.OKAY;
            }
        }

        public Form getForm() {
            return form;
        }
    }
}
