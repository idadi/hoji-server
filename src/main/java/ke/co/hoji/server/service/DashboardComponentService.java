package ke.co.hoji.server.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ke.co.hoji.server.dao.impl.JdbcDashboardComponentDao;
import ke.co.hoji.server.model.DashboardComponent;

@Service
public class DashboardComponentService {

    private final JdbcDashboardComponentDao dashboardComponentDao;

    @Autowired
    public DashboardComponentService(JdbcDashboardComponentDao dashboardComponentDao) {
        this.dashboardComponentDao = dashboardComponentDao;
    }

    public DashboardComponent getDashboardComponent(Integer componentId) {
        return dashboardComponentDao.findDashboardComponentById(componentId);
    }

    public List<DashboardComponent> findDashboardComponentsFor(Integer formId) {
        return dashboardComponentDao.findDashboardComponentsFor(formId);
    }

    public DashboardComponent findDashboardComponentById(Integer componentId) {
        return dashboardComponentDao.findDashboardComponentById(componentId);
    }

    public DashboardComponent saveDashboardComponent(DashboardComponent dashboardComponent) {
        return dashboardComponentDao.saveDashboardComponent(dashboardComponent);
    }

    public void updateAllDashboardComponents(List<DashboardComponent> dashboardComponents) {
        dashboardComponentDao.updateAllDashboardComponents(dashboardComponents);
    }

    public void deleteDashboardComponent(Integer componentId) {
        dashboardComponentDao.deleteDashboardComponent(componentId);
    }

    public DashboardComponent duplicateDashboardComponent(Integer componentId) {
        DashboardComponent componentToDuplicate = findDashboardComponentById(componentId);
        componentToDuplicate.setId(null);
        componentToDuplicate.setName("Copy of " + componentToDuplicate.getName());
        componentToDuplicate.setOrdinal(componentToDuplicate.getOrdinal().add(BigDecimal.ONE));
        DashboardComponent savedComponent = dashboardComponentDao.saveDashboardComponent(componentToDuplicate);
        updateDashboardComponentsOrder(savedComponent);
        return savedComponent;
    }

    private void updateDashboardComponentsOrder(DashboardComponent savedComponent) {
        List<DashboardComponent> componentsToUpdate = findDashboardComponentsFor(savedComponent.getForm().getId())
                .stream()
                .filter(component -> component.getOrdinal().intValue() >= savedComponent.getOrdinal().intValue()
                        && !component.getId().equals(savedComponent.getId()))
                .map(componentToUpdate -> {
                    componentToUpdate.setOrdinal(componentToUpdate.getOrdinal().add(BigDecimal.ONE));
                    return componentToUpdate;
                }).collect(Collectors.toList());
        if (componentsToUpdate.size() > 0) {
            updateAllDashboardComponents(componentsToUpdate);
        }
    }
}
