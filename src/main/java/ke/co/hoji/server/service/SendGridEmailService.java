package ke.co.hoji.server.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import ke.co.hoji.server.event.event.HojiApplicationEvent;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * An implementation of {@link EmailService} that uses the SendGrid API.
 */
@Component("sendGridEmailService")
public class SendGridEmailService extends EmailService {

    private final static Logger logger = LoggerFactory.getLogger(SendGridEmailService.class);

    private static final String API_KEY = "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";

    @Override
    public void sendEmail(
            User to,
            SourceEmail sourceEmail,
            String subject,
            String body,
            HojiApplicationEvent event
    ) {
        try {
            if (event.getType().isOnceInALifeTime() &&
                    !isFirstForUser(event.getType().toString(), event.getUser().getId())) {
                return;
            }
            if (event.getType() == EventType.SIGN_UP) {
                createUser(to);
            }
            Email from = new Email(sourceEmail.getAddress(), sourceEmail.getName());
            Email recipient = new Email(to.getEmail(), to.getFullName());
            Content content = new Content("text/html", body);
            Mail mail = new Mail(from, subject, recipient, content);
            SendGrid sg = new SendGrid(API_KEY);
            Request request = new Request();
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            if (response.getStatusCode() >= 200 && response.getStatusCode() < 300) {
                logger.info("{} email sent successfully with Response Code: {}", event.getType(), response.getStatusCode());
            } else {
                logger.info("Email sending failed. Status: {}", response.getStatusCode());
                logger.info("The response body is: {}", response.getBody());
            }
        } catch (IOException e) {
            logger.error("Error sending email: {}", e.getMessage());
        }
    }

    private void createUser(User user) {
        try {
            Map<String, Object> data = new HashMap<>();
            data.put("email", user.getEmail());
            data.put("first_name", user.getFirstName());
            data.put("last_name", user.getLastName());

            Map<String, Object> payload = new HashMap<>();
            payload.put("contacts", new Map[]{data});
            payload.put("list_ids", new String[]{"4f8ba16d-09f4-4b50-9612-0b859f63a79a"});

            Request request = new Request();
            request.setMethod(Method.PUT);
            request.setEndpoint("marketing/contacts");
            request.setBody(new ObjectMapper().writeValueAsString(payload));

            Response response = new SendGrid(API_KEY).api(request);
            if (response.getStatusCode() >= 200 && response.getStatusCode() < 300) {
                logger.info("Created contact with response code: {}", response.getStatusCode());
            } else {
                logger.info("Failed to create contact. Status: {}", response.getStatusCode());
                logger.info("Response body: {}", response.getBody());
            }
        } catch (IOException e) {
            logger.error("Error creating/updating contact: {}", e.getMessage());
        }
    }
}
