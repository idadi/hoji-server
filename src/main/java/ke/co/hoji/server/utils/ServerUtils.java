package ke.co.hoji.server.utils;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MatrixRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.server.model.DataPoint;

import java.util.ArrayList;
import java.util.List;

public class ServerUtils {

    public interface SearchPredicate<T, ID> {
        boolean matches(ID id, T t);
    }

    public static <T, ID> T findOne(ID criteria, List<T> collection, SearchPredicate<T, ID> predicate) {
        T match = null;
        for (T item : collection) {
            if (predicate.matches(criteria, item)) {
                match = item;
                break;
            }
        }
        return match;
    }

    public static <T, ID> List<T> findMultiple(ID criteria, List<T> collection, SearchPredicate<T, ID> predicate) {
        List<T> matches = new ArrayList<>();
        for (T item : collection) {
            if (predicate.matches(criteria, item)) {
                matches.add(item);
            }
        }
        return matches;
    }

    public static class ChoiceByCodeSearchPredicate implements SearchPredicate<Choice, String> {
        @Override
        public boolean matches(String searchTerm, Choice choice) {
            return choice.getCode().equals(searchTerm);
        }
    }

    public static class ChoiceByIdSearchPredicate implements SearchPredicate<Choice, Integer> {
        @Override
        public boolean matches(Integer id, Choice choice) {
            return choice.getId().equals(id);
        }
    }

    public static class LiveFieldSearchPredicate implements SearchPredicate<LiveField, Field> {
        @Override
        public boolean matches(Field field, LiveField liveField) {
            return field.getId().equals(liveField.getFieldId());
        }
    }

    public static class MatrixRecordSearchPredicate implements SearchPredicate<MatrixRecord, FieldParent> {
        @Override
        public boolean matches(FieldParent fieldParent, MatrixRecord matrixRecord) {
            return fieldParent.getId().equals(matrixRecord.getFieldId());
        }
    }

    public static Integer getDataPointsSampleSize(List<DataPoint> dataPoints) {
        return dataPoints.size();
    }

    public static Integer getAggregatedDataPointsSampleSize(List<DataPoint> dataPoints) {
        Integer sampleSize = 0;
        for (DataPoint dataPoint : dataPoints) {
            sampleSize += Integer.valueOf(dataPoint.getValue().toString());
        }
        return sampleSize;
    }

    /**
     * Recursively sets children for given {@link FieldParent}. This code is very expensive since it makes database
     * trips to fetch children for each {@link FieldParent}, it also sets children for linked {@link Form}s.
     * @param fieldParent, the parent to set children for
     * @param fieldService, service class to help fetch children
     */
    public static void setChildren(FieldParent fieldParent, FieldService fieldService) {
        if (fieldParent instanceof Form) {
            Form form = (Form) fieldParent;
            form.setFields(fieldService.getFields(form));
            for (Field field : form.getFields()) {
                field.setForm(form);
                if (field.isMatrix()) {
                    setChildren(field, fieldService);
                }
            }
        } else {
            Field field = (Field) fieldParent;
            field.setChildren(fieldService.getChildren(field));
        }
        for (Field field : fieldParent.getChildren()) {
            if (field.getType().isRecord()) {
                for (Form linkedForm : field.getRecordForms()) {
                    setChildren(linkedForm, fieldService);
                }
            }
        }
    }
}
