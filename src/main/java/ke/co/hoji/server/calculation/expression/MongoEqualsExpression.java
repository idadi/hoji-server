package ke.co.hoji.server.calculation.expression;

import ke.co.hoji.core.calculation.expression.EqualsExpression;
import org.springframework.data.mongodb.core.query.Criteria;

import static org.springframework.data.mongodb.core.query.Criteria.where;

public class MongoEqualsExpression extends EqualsExpression {

    public MongoEqualsExpression(String leftOperand, Object rightOperand, boolean negated) {
        super(leftOperand, rightOperand, negated);
    }

    @Override
    public Criteria toQuery() {
        String fieldName = leftOperand;
        if (!leftOperand.equals("formId")) {
            fieldName = "liveFields." + leftOperand;
        }
        return !negated
                ? where(fieldName).is(rightOperand)
                : where(fieldName).ne(rightOperand);
    }
}
