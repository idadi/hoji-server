package ke.co.hoji.server.calculation.expression;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import ke.co.hoji.core.calculation.expression.CompoundExpression;
import ke.co.hoji.core.calculation.expression.Expression;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MongoExpressionDeserializer extends StdDeserializer<Expression> {

    MongoExpressionDeserializer() {
        super(Expression.class);
    }

    public Expression deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        Expression expression = null;
        if (p.getCurrentToken() == JsonToken.START_OBJECT) {
            int fieldId = 0;
            while (p.nextToken() != JsonToken.END_OBJECT) {
                if (p.getCurrentName().equals("foreignValue")) {
                    fieldId = p.getValueAsInt();
                    continue;
                }
                expression = parseExpression(p);
            }
            if (expression != null) {
                expression.setFieldId(fieldId);
            }
        }
        return expression;
    }

    private Expression parseExpression(JsonParser p) throws IOException {
        if (p.getCurrentToken() == JsonToken.START_OBJECT) {
            p.nextToken();
        }
        String currentOperator = p.getCurrentName();
        switch (currentOperator) {
            case "and":
            case "or":
                return parseCompoundExpression(p);
            case "eq":
                return parseEqualsExpression(p);
            case "!eq":
                return parseNotEqualsExpression(p);
            default:
                return null;
        }
    }

    private Object readValue(JsonParser p) throws IOException {
        switch (p.currentToken()) {
            case VALUE_NUMBER_INT:
                return p.getIntValue();
            case VALUE_NUMBER_FLOAT:
                return p.getDoubleValue();
            case VALUE_TRUE:
            case VALUE_FALSE:
                return p.getBooleanValue();
            default:
                return p.getText();
        }
    }

    private Expression parseCompoundExpression(JsonParser p) throws IOException {
        String leftOperand = p.getCurrentName();
        CompoundExpression compoundExpression = null;
        List<Expression> rightOperand = new ArrayList<>();
        if (p.nextToken() == JsonToken.START_ARRAY) {
            while (p.nextToken() != JsonToken.END_ARRAY) {
                rightOperand.add(parseExpression(p));
                p.nextToken();
            }
            compoundExpression = new MongoCompoundExpression(leftOperand, rightOperand);
        }
        return compoundExpression;
    }

    private Expression parseEqualsExpression(JsonParser p) throws IOException {
        Expression equalsExpression = null;
        while (p.nextToken() != JsonToken.END_OBJECT) {
            if (p.getCurrentToken() == JsonToken.FIELD_NAME) {
                continue;
            }
            String leftOperand = p.getCurrentName();
            Object rightOperand = readValue(p);
            equalsExpression = new MongoEqualsExpression(leftOperand, rightOperand, false);
        }
        return equalsExpression;
    }

    private Expression parseNotEqualsExpression(JsonParser p) throws IOException {
        Expression notEqualsExpression = null;
        while (p.nextToken() != JsonToken.END_OBJECT) {
            if (p.getCurrentToken() == JsonToken.FIELD_NAME) {
                continue;
            }
            String leftOperand = p.getCurrentName();
            Object rightOperand = readValue(p);
            notEqualsExpression = new MongoEqualsExpression(leftOperand, rightOperand, true);
        }
        return notEqualsExpression;
    }
}
