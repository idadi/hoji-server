package ke.co.hoji.server.calculation.expression;

import com.fasterxml.jackson.databind.ObjectMapper;
import ke.co.hoji.core.calculation.expression.Expression;
import ke.co.hoji.core.calculation.expression.ExpressionParser;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.io.IOException;

public class MongoExpressionParser implements ExpressionParser {

    private ObjectMapper objectMapper =
            new Jackson2ObjectMapperBuilder()
                    .deserializerByType(Expression.class, new MongoExpressionDeserializer())
                    .build();

    public Expression parse(String expression) throws IOException {
        return objectMapper.readValue(expression, Expression.class);
    }
}
