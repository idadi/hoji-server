package ke.co.hoji.server.calculation.expression;

import ke.co.hoji.core.calculation.expression.CompoundExpression;
import ke.co.hoji.core.calculation.expression.Expression;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.List;

public class MongoCompoundExpression extends CompoundExpression {

    public MongoCompoundExpression(String leftOperand, List<Expression> rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public Criteria toQuery() {
        Criteria criteria;
        if ("or".equals(leftOperand)) {
            criteria = new Criteria()
                    .orOperator(rightOperand.stream().map(Expression::toQuery).toArray(Criteria[]::new));
        } else {
            criteria = new Criteria()
                    .andOperator(rightOperand.stream().map(Expression::toQuery).toArray(Criteria[]::new));
        }
        return criteria;
    }
}
