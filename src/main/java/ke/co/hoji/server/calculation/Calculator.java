package ke.co.hoji.server.calculation;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.calculation.CalculationException;
import ke.co.hoji.core.calculation.CalculationUtils;
import ke.co.hoji.core.calculation.expression.CrossFormValueRetriever;
import ke.co.hoji.core.calculation.expression.ExpressionParser;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.MatrixRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.server.calculation.expression.MongoExpressionParser;
import ke.co.hoji.server.converter.DtoToModelRecordConverter;
import ke.co.hoji.server.repository.CalculationResultRepository;
import org.apache.commons.collections.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Manages the computation of calculated values server-side.
 */
@Component
public class Calculator implements ApplicationContextAware {

    private static final Logger LOGGER = LoggerFactory.getLogger(Calculator.class);
    private final CalculationResultRepository calculationResultRepository;
    private final CrossFormValueRetriever crossFormValueRetriever;
    private ApplicationContext context;
    private final ExpressionParser expressionParser = new MongoExpressionParser();

    @Autowired
    public Calculator(CalculationResultRepository calculationResultRepository, CrossFormValueRetriever crossFormValueRetriever) {
        this.calculationResultRepository = calculationResultRepository;
        this.crossFormValueRetriever = crossFormValueRetriever;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    private HojiContext getHojiContext() {
        return context.getBean(HojiContext.class);
    }

    /**
     * Computes the value of a calculated field. This does not guarantee that the calculation will be executed afresh.
     * A cached result may be returned if available.
     *
     * @param mainRecordDto the MainRecord for which the value is to be calculated.
     * @param recordUuid    the uuid of the MainRecord for which the value is to be calculated.
     * @param field         the {@link Field} for which the value is to be calculated.
     * @return the calculated result.
     */
    public CalculationResult calculate(MainRecord mainRecordDto, String recordUuid, Field field) {
        return calculate(mainRecordDto, recordUuid, field, false);
    }

    /**
     * Computes the value of a calculated field. This method allows the caller to specify whether the calculation
     * must be executed afresh, regardless of the availability of a cached result.
     *
     * @param mainRecordDto      the MainRecord for which the value is to be calculated.
     * @param recordUuid         the uuid of the MainRecord for which the value is to be calculated.
     * @param field              the {@link Field} for which the value is to be calculated.
     * @param forceRecalculation if true, the calculation must be executed afresh. No cached result may be returned. If
     *                           false, the calculation may still be calculated afresh, but if a cached result is
     *                           found, that is what is returned instead.
     * @return the calculated result.
     */
    public CalculationResult calculate(MainRecord mainRecordDto, String recordUuid, Field field, boolean forceRecalculation) {
        CalculationResult calculationResult = null;
        if (!forceRecalculation) {
            calculationResult
                = calculationResultRepository.findByObjectIdAndRecordUuidAndFieldId(
                mainRecordDto.getId(),
                recordUuid,
                field.getId()
            );
        }
        if (calculationResult == null) {
            FieldParent fieldParent = field.getParent() != null ? field.getParent() : field.getForm();
            DtoToModelRecordConverter converter = new DtoToModelRecordConverter(getHojiContext(), fieldParent);
            Record record = null;
            if (field.getParent() != null) {
                MatrixRecord matrixRecordDto = mainRecordDto.getMatrixRecords()
                    .stream()
                    .filter(mRecord -> mRecord.getUuid().equals(recordUuid))
                    .findFirst()
                    .orElse(null);
                if (matrixRecordDto != null) {
                    Stream<Field> calculatedFields = field.getForm().getFields()
                        .stream().filter(f -> f.compareTo(field) < 0 && f.hasScriptFunction(Field.ScriptFunctions.CALCULATE) && f.getParent() != null && f.isEnabled());
                    List<LiveField> calculatedLiveFields =
                        getPreviouslyCalculatedLiveFields(calculatedFields, mainRecordDto.getId(), recordUuid);
                    record = converter.convert(matrixRecordDto);
                    record.setLiveFields(ListUtils.union(calculatedLiveFields, record.getLiveFields()));
                }
            } else {
                Stream<Field> calculatedFields = field.getForm().getFields()
                    .stream().filter(f -> f.compareTo(field) < 0 && f.hasScriptFunction(Field.ScriptFunctions.CALCULATE) && f.getParent() == null && f.isEnabled());
                List<LiveField> calculatedLiveFields =
                    getPreviouslyCalculatedLiveFields(calculatedFields, mainRecordDto.getId(), recordUuid);
                record = converter.convert(mainRecordDto);
                record.setLiveFields(ListUtils.union(calculatedLiveFields, record.getLiveFields()));
            }
            if (record != null && shouldBeCalculated(field, record)) {
                try {
                    Object executionResult = executeCalculation(field, record);
                    if (executionResult != null && executionResult instanceof ke.co.hoji.core.calculation.CalculationResult) {
                        // We store the calculation result value, not the calculation result object.
                        executionResult = ((ke.co.hoji.core.calculation.CalculationResult) executionResult).getValue();
                    }
                    calculationResult = new CalculationResult(
                        mainRecordDto.getId(),
                        recordUuid,
                        field.getForm().getId(),
                        field.getId(),
                        executionResult
                    );
                    saveCalculatedValue(calculationResult);
                } catch (CalculationException ex) {
                    calculationResult = createNullCalculationResult(mainRecordDto, recordUuid, field);
                    calculationResult.setEvaluated(false);
                    String errorMessage = "\nCalculation Error: " + ex.getMessage() + "\n" +
                        "Form ID: " + calculationResult.getFormId() + "\n" +
                        "Field ID: " + calculationResult.getFieldId() + "\n" +
                        "Record UUID: " + calculationResult.getRecordUuid() + "\n" +
                        "Main Record ID: " + calculationResult.getObjectId() + "\n" +
                        "Calculation Script: " + field.getValueScript() + "\n";
                    LOGGER.error(errorMessage);
                }
            } else {
                calculationResult = createNullCalculationResult(mainRecordDto, recordUuid, field);
                calculationResult.setEvaluated(false);
            }
        }
        return calculationResult;
    }

    private boolean shouldBeCalculated(Field field, Record record) {
        if (field.hasBackwardSkips()) {
            return getHojiContext().getRuleEvaluator().evaluateRules(field.getBackwardSkips(), record, field);
        }
        return true;
    }

    private Object executeCalculation(Field field, Record record) {
        Integer surveyId = field.getForm().getSurvey().getId();
        CalculationUtils calculationUtils = new CalculationUtils(
            surveyId,
            getHojiContext().getModelServiceManager(),
            record.getLiveFields(),
            field,
            new Date(record.getDateCreated()),
            HojiContext.SERVER_CONTEXT
        );
        calculationUtils.setExpressionParser(expressionParser);
        calculationUtils.setCrossFormValueRetriever(crossFormValueRetriever);
        calculationUtils.setRecordUuid(record.getUuid());
        return getHojiContext().getCalculationEvaluator().evaluateScript(
            field.getValueScript(),
            Field.ScriptFunctions.CALCULATE,
            calculationUtils,
            field
        );
    }

    private void saveCalculatedValue(CalculationResult calculationResult) {
        calculationResultRepository.deleteByRecordUuidAndFormIdAndFieldId(
            calculationResult.getRecordUuid(),
            calculationResult.getFormId(),
            calculationResult.getFieldId()
        );
        calculationResultRepository.save(calculationResult);
    }

    private List<LiveField> getPreviouslyCalculatedLiveFields(
        Stream<Field> calculatedFields,
        String objectId,
        String recordUuid
    ) {
        return calculatedFields.map(field -> {
            LiveField calculatedLf = new LiveField(field);
            calculatedLf.setResponse(Response.create(getHojiContext(), calculatedLf, null, false));
            CalculationResult result = calculationResultRepository
                .findByObjectIdAndRecordUuidAndFieldId(objectId, recordUuid, field.getId());
            if (result != null) {
                calculatedLf.setResponse(
                    Response.create(getHojiContext(), calculatedLf, result.getValue(), true)
                );
            }
            return calculatedLf;
        }).collect(Collectors.toList());
    }

    private CalculationResult createNullCalculationResult(MainRecord mainRecord, String recordUuid, Field field) {
        return new CalculationResult(
            mainRecord.getId(),
            recordUuid,
            field.getForm().getId(),
            field.getId(),
            null
        );
    }
}
