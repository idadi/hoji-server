package ke.co.hoji.server.calculation.expression;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.calculation.expression.Expression;
import ke.co.hoji.core.calculation.expression.CrossFormValueRetriever;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.server.calculation.CalculationResult;
import ke.co.hoji.server.converter.DtoToModelLiveFieldConverter;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class MongoCrossFormValueRetriever implements CrossFormValueRetriever, ApplicationContextAware {

    private final MongoOperations mongoOperations;
    private ApplicationContext context;

    public MongoCrossFormValueRetriever(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Object> retrieve(Expression expression) {
        Field field = getHojiContext().getModelServiceManager().getConfigService().getField(expression.getFieldId());
        Criteria criteria = Criteria.where("test").is(false).andOperator((Criteria) expression.toQuery());
        List<Object> values = new ArrayList<>();
        mongoOperations.stream(Query.query(criteria), MainRecord.class)
                .forEachRemaining(mainRecord -> {
                    Optional<LiveField> found = mainRecord.getLiveFields()
                            .stream()
                            .filter(lf -> field.getId().equals(lf.getFieldId()))
                            .findFirst();
                    DtoToModelLiveFieldConverter liveFieldConverter =
                            new DtoToModelLiveFieldConverter(getHojiContext(), field, mainRecord);
                    ke.co.hoji.core.data.model.LiveField converted =
                            liveFieldConverter.convert(found.orElse(new ke.co.hoji.core.data.dto.LiveField()));
                    if (field.hasScriptFunction(Field.ScriptFunctions.CALCULATE)) {
                        Query calculatedQ = Query.query(Criteria.where("recordUuid").is(mainRecord.getUuid()).and("fieldId").is(field.getId()));
                        CalculationResult result = mongoOperations.findOne(calculatedQ, CalculationResult.class);
                        if (result.isEvaluated() && result.getValue() != null) {
                            converted.setResponse(
                                    Response.create(getHojiContext(), converted, result.getValue(), true)
                            );
                        }
                    }
                    if (converted.getResponse().getActualValue() != null) {
                        values.add(converted.getResponse().getActualValue());
                    }
                });
        return values;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    private HojiContext getHojiContext() {
        return context.getBean(HojiContext.class);
    }

}
