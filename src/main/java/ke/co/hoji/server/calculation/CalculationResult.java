package ke.co.hoji.server.calculation;

import org.springframework.data.annotation.Transient;

public class CalculationResult {

    private String objectId;
    private String recordUuid;
    private Integer fieldId;
    private Integer formId;
    private Object value;
    @Transient
    private boolean evaluated = true;

    public CalculationResult(String objectId, String recordUuid, Integer formId, Integer fieldId, Object value) {
        this.objectId = objectId;
        this.recordUuid = recordUuid;
        this.formId = formId;
        this.fieldId = fieldId;
        this.value = value;
    }

    public String getObjectId() {
        return objectId;
    }

    public String getRecordUuid() {
        return recordUuid;
    }

    public Integer getFieldId() {
        return fieldId;
    }

    public Integer getFormId() {
        return formId;
    }

    public Object getValue() {
        return value;
    }

    public boolean isEvaluated() {
        return evaluated;
    }

    public void setEvaluated(boolean evaluated) {
        this.evaluated = evaluated;
    }
}
