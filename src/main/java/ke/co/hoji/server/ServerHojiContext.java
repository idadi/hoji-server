package ke.co.hoji.server;

import ke.co.hoji.core.DtoServiceManager;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.calculation.CalculationEvaluator;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.response.helper.Base64Encoder;
import ke.co.hoji.core.rule.RuleEvaluator;
import ke.co.hoji.core.widget.WidgetManager;
import ke.co.hoji.server.response.ServerBase64Encoder;
import ke.co.hoji.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.script.ScriptEngineManager;

/**
 * Created by gitahi on 04/07/15.
 */
@Component
public class ServerHojiContext implements HojiContext {

    @Autowired
    @Qualifier("serviceManager")
    private ModelServiceManager modelServiceManager;

    @Autowired
    private UserService userService;

    private DtoServiceManager dtoServiceManager;

    private RuleEvaluator ruleEvaluator;

    private CalculationEvaluator calculationEvaluator;

    @Override
    public ModelServiceManager getModelServiceManager() {
        return modelServiceManager;
    }

    @Override
    public DtoServiceManager getDtoServiceManager() {
        return dtoServiceManager;
    }

    @Override
    public WidgetManager getWidgetManager() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public RuleEvaluator getRuleEvaluator() {
        if (ruleEvaluator == null) {
            ruleEvaluator = new RuleEvaluator();
        }
        return ruleEvaluator;
    }

    @Override
    public CalculationEvaluator getCalculationEvaluator() {
        if (calculationEvaluator == null) {
            calculationEvaluator = new CalculationEvaluator(new ScriptEngineManager().getEngineByName("nashorn"));
        }
        return calculationEvaluator;
    }

    @Override
    public Base64Encoder getBase64Encoder() {
        return new ServerBase64Encoder();
    }

    @Override
    public Survey getSurvey() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setSurvey(Survey survey) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public Form getForm() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public void setForm(Form form) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public Record getRecord() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public void setRecord(Record record) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public String getDeviceIdentifier() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public String getUsername() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public Integer getUserId() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public String getUserCode() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public String getNameOfUser() {
        return userService.getCurrentUser().getFullName();
    }

    @Override
    public Integer getOwnerId() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public String getOwnerName() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public boolean getOwnerVerified() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public String getTenantName() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public String getTenantCode() {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public Integer getLanguageId() {
        return -1; //This essentially disables project language translation server-side.
    }

    @Override
    public String getType() {
        return SERVER_CONTEXT;
    }

    @Override
    public String appVersion() {
        throw new UnsupportedOperationException();
    }

    @PostConstruct
    public void portDataIfNecessary() {
        getModelServiceManager().getRecordService().portDataIfNecessary();
    }
}
