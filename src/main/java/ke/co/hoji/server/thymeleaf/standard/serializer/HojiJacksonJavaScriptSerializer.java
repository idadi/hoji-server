package ke.co.hoji.server.thymeleaf.standard.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.SerializableString;
import com.fasterxml.jackson.core.io.CharacterEscapes;
import com.fasterxml.jackson.core.io.SerializedString;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.thymeleaf.exceptions.ConfigurationException;
import org.thymeleaf.exceptions.TemplateProcessingException;
import org.thymeleaf.standard.serializer.IStandardJavaScriptSerializer;
import org.thymeleaf.util.ClassLoaderUtils;

import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class is a mirror of org.thymeleaf.standard.serializer.StandardJavaScriptSerializer.JacksonStandardJavaScriptSerializer
 * and it allows injection of {@link ObjectMapper}. Since the {@link ObjectMapper} used in this class
 * has a different configuration than that used in the project we make a copy.
 * <p>
 * Created by geoffreywasilwa on 21/02/2017.
 */
public class HojiJacksonJavaScriptSerializer implements IStandardJavaScriptSerializer {

    private ObjectMapper mapper;

    public HojiJacksonJavaScriptSerializer(ObjectMapper mapper) {
        this.mapper = mapper.copy();
        this.mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        this.mapper.disable(new JsonGenerator.Feature[]{JsonGenerator.Feature.AUTO_CLOSE_TARGET});
        this.mapper.enable(new JsonGenerator.Feature[]{JsonGenerator.Feature.ESCAPE_NON_ASCII});
        this.mapper.getFactory().setCharacterEscapes(new HojiJacksonJavaScriptSerializer.JacksonThymeleafCharacterEscapes());
        this.mapper.setDateFormat(new HojiJacksonJavaScriptSerializer.JacksonThymeleafISO8601DateFormat());
        Class javaTimeModuleClass = ClassLoaderUtils.findClass("com.fasterxml.jackson.datatype.jsr310.JavaTimeModule");
        if(javaTimeModuleClass != null) {
            try {
                this.mapper.registerModule((Module)javaTimeModuleClass.newInstance());
                this.mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            } catch (InstantiationException var3) {
                throw new ConfigurationException("Exception while trying to initialize JSR310 support for Jackson", var3);
            } catch (IllegalAccessException var4) {
                throw new ConfigurationException("Exception while trying to initialize JSR310 support for Jackson", var4);
            }
        }
    }

    @Override
    public void serializeValue(Object o, Writer writer) {
        try {
            mapper.writeValue(writer, o);
        } catch (IOException e) {
            throw new TemplateProcessingException("An exception was raised while trying to serialize object to JavaScript using Jackson", e);
        }
    }

    private static final class JacksonThymeleafCharacterEscapes extends CharacterEscapes {
        private static final int[] CHARACTER_ESCAPES = CharacterEscapes.standardAsciiEscapesForJSON();
        private static final SerializableString SLASH_ESCAPE;
        private static final SerializableString AMPERSAND_ESCAPE;

        JacksonThymeleafCharacterEscapes() {
        }

        public int[] getEscapeCodesForAscii() {
            return CHARACTER_ESCAPES;
        }

        public SerializableString getEscapeSequence(int ch) {
            return ch == 47?SLASH_ESCAPE:(ch == 38?AMPERSAND_ESCAPE:null);
        }

        static {
            CHARACTER_ESCAPES[47] = -2;
            CHARACTER_ESCAPES[38] = -2;
            SLASH_ESCAPE = new SerializedString("\\/");
            AMPERSAND_ESCAPE = new SerializedString("\\u0026");
        }
    }

    private static final class JacksonThymeleafISO8601DateFormat extends DateFormat {
        private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss.SSSZZZ");

        JacksonThymeleafISO8601DateFormat() {
            this.setCalendar(this.dateFormat.getCalendar());
            this.setNumberFormat(this.dateFormat.getNumberFormat());
        }

        public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition fieldPosition) {
            StringBuffer formatted = this.dateFormat.format(date, toAppendTo, fieldPosition);
            formatted.insert(26, ':');
            return formatted;
        }

        public Date parse(String source, ParsePosition pos) {
            throw new UnsupportedOperationException("JacksonThymeleafISO8601DateFormat should never be asked for a \'parse\' operation");
        }
    }
}
