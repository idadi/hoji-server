package ke.co.hoji.server.config;

import com.vladmihalcea.flexypool.FlexyPoolDataSource;
import com.vladmihalcea.flexypool.adaptor.HikariCPPoolAdapter;
import com.vladmihalcea.flexypool.config.Configuration;
import com.vladmihalcea.flexypool.connection.ConnectionDecoratorFactoryResolver;
import com.vladmihalcea.flexypool.event.ConnectionAcquireTimeThresholdExceededEvent;
import com.vladmihalcea.flexypool.event.ConnectionAcquireTimeoutEvent;
import com.vladmihalcea.flexypool.event.ConnectionLeaseTimeThresholdExceededEvent;
import com.vladmihalcea.flexypool.event.Event;
import com.vladmihalcea.flexypool.event.EventListener;
import com.vladmihalcea.flexypool.event.EventListenerResolver;
import com.vladmihalcea.flexypool.strategy.IncrementPoolOnTimeoutConnectionAcquiringStrategy;
import com.vladmihalcea.flexypool.strategy.RetryConnectionAcquiringStrategy;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import liquibase.integration.spring.SpringLiquibase;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by gitahi on 01/10/15.
 */
@org.springframework.context.annotation.Configuration
@ComponentScan(basePackages = "ke.co.hoji.server.dao")
@EnableTransactionManagement(proxyTargetClass = true)
@PropertySource({"classpath:jdbc.properties", "classpath:hoji.properties"})
@Order(1)
public class DatabaseConfiguration {

    @Value("${dataSource.driveClassName}")
    private String driverClassName;

    @Value("${dataSource.url}")
    private String dataSourceUrl;

    @Value("${dataSource.username}")
    private String dataSourceUsername;

    @Value("${dataSource.password}")
    private String dataSourcePassword;

    @Value("${dataSource.cachePrepStmts}")
    private Boolean dataSourceCachePrepStmts;

    @Value("${dataSource.prepStmtCacheSize}")
    private String dataSourcePrepStmtCacheSize;

    @Value("${dataSource.prepStmtCacheSqlLimit}")
    private String dataSourcePrepStmtCacheSqlLimit;

    @Value("${dataSource.nullCatalogMeansCurrent}")
    private Boolean nullCatalogMeansCurrent;

    @Value("${dataSource.nullNamePatternMatchesAll}")
    private Boolean nullNamePatternMatchesAll;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public HikariDataSource hikariDataSource() {
        HikariConfig config = new HikariConfig();
        config.setDriverClassName(driverClassName);
        config.setJdbcUrl(dataSourceUrl);
        config.setUsername(dataSourceUsername);
        config.setPassword(dataSourcePassword);
        config.addDataSourceProperty("cachePrepStmts", dataSourceCachePrepStmts);
        config.addDataSourceProperty("prepStmtCacheSize", dataSourcePrepStmtCacheSize);
        config.addDataSourceProperty("prepStmtCacheSqlLimit", dataSourcePrepStmtCacheSqlLimit);
        config.addDataSourceProperty("nullCatalogMeansCurrent", nullCatalogMeansCurrent);
        config.addDataSourceProperty("nullNamePatternMatchesAll", nullNamePatternMatchesAll);

        HikariDataSource hikariDataSource = new HikariDataSource(config);
        return hikariDataSource;
    }

    @Bean
    public DataSourceTransactionManager transactionManager() {
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
        transactionManager.setDataSource(dataSource());
        return transactionManager;
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    public static class ConnectionAcquireTimeThresholdExceededEventListener
            extends EventListener<ConnectionAcquireTimeThresholdExceededEvent> {

        public static final Logger LOGGER = LoggerFactory.getLogger(
                ConnectionAcquireTimeThresholdExceededEventListener.class);

        public ConnectionAcquireTimeThresholdExceededEventListener() {
            super(ConnectionAcquireTimeThresholdExceededEvent.class);
        }

        @Override
        public void on(ConnectionAcquireTimeThresholdExceededEvent event) {
            LOGGER.info("Caught event {}", event);
        }
    }

    public static class ConnectionLeaseTimeThresholdExceededEventListener
            extends EventListener<ConnectionLeaseTimeThresholdExceededEvent> {

        public static final Logger LOGGER = LoggerFactory.getLogger(
                ConnectionLeaseTimeThresholdExceededEventListener.class);

        public ConnectionLeaseTimeThresholdExceededEventListener() {
            super(ConnectionLeaseTimeThresholdExceededEvent.class);
        }

        @Override
        public void on(ConnectionLeaseTimeThresholdExceededEvent event) {
            LOGGER.info("Caught event {}", event);
        }
    }

    public static class ConnectionAcquireTimeoutEventListener
            extends EventListener<ConnectionAcquireTimeoutEvent> {

        public static final Logger LOGGER = LoggerFactory.getLogger(
                ConnectionAcquireTimeoutEventListener.class);

        public ConnectionAcquireTimeoutEventListener() {
            super(ConnectionAcquireTimeoutEvent.class);
        }

        @Override
        public void on(ConnectionAcquireTimeoutEvent event) {
            LOGGER.info("Caught event {}", event);
        }
    }

    @Value("${flexy.pool.uniqueId}")
    private String uniqueId;

    @Bean
    public Configuration<HikariDataSource> configuration() {
        return new Configuration.Builder<>(
                uniqueId,
                hikariDataSource(),
                HikariCPPoolAdapter.FACTORY
        )
                .setConnectionProxyFactory(ConnectionDecoratorFactoryResolver.INSTANCE.resolve())
                .setMetricLogReporterMillis(TimeUnit.SECONDS.toMillis(5))
                .setJmxEnabled(true)
                .setJmxAutoStart(true)
                .setConnectionAcquireTimeThresholdMillis(50L)
                .setConnectionLeaseTimeThresholdMillis(250L)
                .setEventListenerResolver(new EventListenerResolver() {
                    @Override
                    public List<EventListener<? extends Event>> resolveListeners() {
                        return Arrays.asList(
                                new ConnectionAcquireTimeoutEventListener(),
                                new ConnectionAcquireTimeThresholdExceededEventListener(),
                                new ConnectionLeaseTimeThresholdExceededEventListener()
                        );
                    }
                })
                .build();
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    public FlexyPoolDataSource dataSource() {
        Configuration<HikariDataSource> configuration = configuration();
        return new FlexyPoolDataSource<>(configuration,
                new IncrementPoolOnTimeoutConnectionAcquiringStrategy.Factory(5),
                new RetryConnectionAcquiringStrategy.Factory(2)
        );
    }

    @Bean
    public SpringLiquibase liquibase() {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setChangeLog("classpath:/changelog/db.changelog-master.xml");
        liquibase.setDataSource(dataSource());
        return liquibase;
    }

}
