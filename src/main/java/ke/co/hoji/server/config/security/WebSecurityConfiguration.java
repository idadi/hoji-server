package ke.co.hoji.server.config.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import ke.co.hoji.server.controller.AjaxAccessDeniedExceptionHandler;
import ke.co.hoji.server.model.User;

/**
 * Created by gitahi on 01/10/15.
 */
@Order(2)
@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationProvider authenticationProvider;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
        auth.authenticationProvider(authenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/resources/**", "/signIn", "/signUp", "/logout", "/password-recovery", "/resetPassword",
                        "/confirm", "/confirmationTokenSent", "/resendToken", "/message", "/passwordTokenSent",
                        "/favicon.ico", "/loggedIn")
                .permitAll().antMatchers(HttpMethod.GET, "/hoji-websocket/**", "/project/747/form/807/report/dashboard")
                .permitAll().antMatchers("/verificationTokenSent", "/emailVerified", "/passwordReset", "/user/**")
                .authenticated()
                .antMatchers("/", "/**/view/**", "/**/list/**", "/field/auto-number", "/**/report/filter",
                        "/**/report/dashboard/component", "/**/report/analysis", "/**/report/table",
                        "/**/report/monitor", "/**/report/pivot", "/**/report/evolution", "/**/report/gallery",
                        "/**/report/dashboard", "/**/report/map", "/choice-group", "/choice-group/{id}",
                        "/rule/field/choices", "/**/record", "/hoji-websocket/**", "/images/**")
                .access("hasRole('" + User.Role.VIEWER.role() + "')")
                .antMatchers("/**/create/**", "/**/edit/**", "/**/attachScript/**", "/**/save*/**", "/**/enable/**",
                        "/**/changeTestMode/**", "/**/validation/**", "/**/advanced/**", "/**/publish/**",
                        "/**/upload/**", "/form/forms", "/field/fields", "/**/sort", "/hook/**",
                        "/project/{surveyId}/property", "/project/{surveyId}/property/{key}/", "/**/dashboard/**",
                        "/**/field/editor-options", "/**/field/autocomplete", "/**/field/{fieldId}/bulk-save",
                        "/**/field/field-action", "/**/field/input-format", "/**/field/parent",
                        "/**/rule/{fieldId}/target", "/**/rule/{fieldId}/operator")
                .access("hasRole('" + User.Role.CREATOR.role() + "')")
                .antMatchers("/**/delete*/**", "/**/clear/**", "/project/{surveyId}/property/{key}/")
                .access("hasRole('" + User.Role.DESTROYER.role() + "')").anyRequest()
                .access("hasRole('" + User.Role.SUPER.role() + "')").and().formLogin().loginPage("/signIn")
                .successHandler(new AjaxAuthenticationSuccessHandler(requestCache()))
                .failureHandler(new AjaxAuthenticationFailureHandler()).and().requestCache()
                .requestCache(requestCache()).and().csrf().and().exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler());
    }

    @Bean(name = "authenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    RequestCache requestCache() {
        return new HttpSessionRequestCache();
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        AjaxAccessDeniedExceptionHandler accessDeniedHandler = new AjaxAccessDeniedExceptionHandler();
        // To be used by non-ajax handler
        accessDeniedHandler.setErrorPage("/error");
        return accessDeniedHandler;
    }

    /**
     * This configuration disables Spring's
     * {@link org.springframework.security.web.firewall.StrictHttpFirewall} which
     * rejects un-normalized URLs. See:
     * https://docs.spring.io/spring-security/site/docs/5.0.0.RELEASE/reference/htmlsingle/#request-matching
     * <p>
     * We are doing this because we always used URLs with // instead of / because we
     * have {@link ke.co.hoji.core.data.Constants.Server#URL} suffixed with a / and
     * the API endpoints e.g.
     * {@link ke.co.hoji.core.data.Constants.Api#AUTHENTICATE} prefixed with a /.
     * The combination of both to construct the URL results in a URL with // wich
     * the StrictHttpFirewall rejects.
     * <p>
     * In order to continue supporting older app versions, we'll temporarily disable
     * the StrictHttpFirewall for the next 6 months or so.
     */
    // TODO: Remove this block any time after Jan 1, 2020.
    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
        web.httpFirewall(new DefaultHttpFirewall());
    }

    /**
     * Handles successful ajax login requests by sending JSON responses. The payload
     * contains a target url if the request was initiated by a redirect.
     */
    static class AjaxAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

        private final RequestCache requestCache;

        public AjaxAuthenticationSuccessHandler(RequestCache requestCache) {
            this.requestCache = requestCache;
        }

        @Override
        public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                Authentication authentication) throws IOException, ServletException {

            SavedRequest savedRequest = requestCache.getRequest(request, response);
            String targetUrl = "";

            if (savedRequest != null) {
                targetUrl = savedRequest.getRedirectUrl();
            }

            clearAuthenticationAttributes(request);

            if (!response.isCommitted() && StringUtils.containsIgnoreCase(request.getHeader(HttpHeaders.ACCEPT),
                    MediaType.APPLICATION_JSON_VALUE)) {
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().append("{\"status\": \"" + HttpServletResponse.SC_OK + "\""
                        + ", \"targetUrl\": \"" + targetUrl + "\" }");
            }
        }
    }

    /**
     * Handles failed authentication ajax request by sending JSON feedback.
     */
    static class AjaxAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

        @Override
        public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                AuthenticationException exception) throws IOException, ServletException {
            if (!response.isCommitted() && StringUtils.containsIgnoreCase(request.getHeader(HttpHeaders.ACCEPT),
                    MediaType.APPLICATION_JSON_VALUE)) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().append("{\"status\": \"" + HttpServletResponse.SC_BAD_REQUEST + "\""
                        + ", \"message\": \"" + exception.getMessage() + "\" }");
            }
        }
    }
}
