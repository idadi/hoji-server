package ke.co.hoji.server.config.main;

import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.core.service.model.SurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories({"ke.co.hoji.server.repository"})
@Order(3)
public class ServiceConfiguration {

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private FormService formService;

    @Autowired
    private FieldService fieldService;

    @Bean
    public ConfigService configService() {
        return new ConfigService(surveyService, formService, fieldService);
    }

}
