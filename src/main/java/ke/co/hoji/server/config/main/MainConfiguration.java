package ke.co.hoji.server.config.main;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Field.FieldAction;
import ke.co.hoji.core.data.model.Field.InputFormat;
import ke.co.hoji.core.data.model.Field.Uniqueness;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.ChoiceGroupService;
import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.core.service.model.SurveyService;
import ke.co.hoji.server.calculation.Calculator;
import ke.co.hoji.server.controller.report.model.Filter;
import ke.co.hoji.server.converter.BooleanToStringConverter;
import ke.co.hoji.server.converter.IntegerToStringConverter;
import ke.co.hoji.server.converter.StringArrayToFormListConverter;
import ke.co.hoji.server.converter.StringToChoiceConverter;
import ke.co.hoji.server.converter.StringToChoiceGroupConverter;
import ke.co.hoji.server.converter.StringToFieldConverter;
import ke.co.hoji.server.converter.StringToFieldTypeConverter;
import ke.co.hoji.server.converter.StringToFormConverter;
import ke.co.hoji.server.converter.StringToFormListConverter;
import ke.co.hoji.server.converter.StringToOperatorDescriptorConverter;
import ke.co.hoji.server.converter.StringToSurveyConverter;
import ke.co.hoji.server.dao.model.JdbcChoiceDao;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.event.processor.AccessRequestEmailer;
import ke.co.hoji.server.event.processor.AccountSuspendedEmailer;
import ke.co.hoji.server.event.processor.AccountVerificationTokenEmailer;
import ke.co.hoji.server.event.processor.CreditsExpiredEmailer;
import ke.co.hoji.server.event.processor.EventProcessorManager;
import ke.co.hoji.server.event.processor.ExpirationNoticeEmailer;
import ke.co.hoji.server.event.processor.FreeCreditsExpiredEmailer;
import ke.co.hoji.server.event.processor.FreebieExpirationNoticeEmailer;
import ke.co.hoji.server.event.processor.MainRecordCreatedCalculator;
import ke.co.hoji.server.event.processor.MainRecordCreatedEmailer;
import ke.co.hoji.server.event.processor.MainRecordWebHookPusher;
import ke.co.hoji.server.event.processor.MainRecordWebSocketPusher;
import ke.co.hoji.server.event.processor.PasswordResetTokenEmailer;
import ke.co.hoji.server.event.processor.PublicProjectAccessActivatedEmailer;
import ke.co.hoji.server.event.processor.PublicationNotificationPusher;
import ke.co.hoji.server.event.processor.RolesModifiedEmailer;
import ke.co.hoji.server.event.processor.SolicitEnumeratorReviewEmailer;
import ke.co.hoji.server.event.processor.SolicitOfficerReviewEmailer;
import ke.co.hoji.server.event.processor.TenantChangedEmailer;
import ke.co.hoji.server.event.processor.UserExpelledEmailer;
import ke.co.hoji.server.event.processor.WelcomeEmailer;
import ke.co.hoji.server.interceptor.SuspensionInterceptor;
import ke.co.hoji.server.json.deserializer.ChoiceDeserializer;
import ke.co.hoji.server.json.deserializer.ChoiceGroupDeserializer;
import ke.co.hoji.server.json.deserializer.RuleDeserializer;
import ke.co.hoji.server.json.deserializer.WebHookDeserializer;
import ke.co.hoji.server.json.serializer.ChoiceSerializer;
import ke.co.hoji.server.json.serializer.DashboardComponentSerializer;
import ke.co.hoji.server.json.serializer.EventTypeSerializer;
import ke.co.hoji.server.json.serializer.FieldActionSerializer;
import ke.co.hoji.server.json.serializer.FieldSerializer;
import ke.co.hoji.server.json.serializer.FieldTypeSerializer;
import ke.co.hoji.server.json.serializer.FilterSerializer;
import ke.co.hoji.server.json.serializer.FormSerializer;
import ke.co.hoji.server.json.serializer.InputFormatSerializer;
import ke.co.hoji.server.json.serializer.PropertySerializer;
import ke.co.hoji.server.json.serializer.SurveySerializer;
import ke.co.hoji.server.json.serializer.TemplateSerializer;
import ke.co.hoji.server.json.serializer.UniquenessSerializer;
import ke.co.hoji.server.json.serializer.WebHookSerializer;
import ke.co.hoji.server.model.DashboardComponent;
import ke.co.hoji.server.model.Template;
import ke.co.hoji.server.model.WebHook;
import ke.co.hoji.server.resolver.FilterHandlerMethodArgumentResolver;
import ke.co.hoji.server.resolver.WebHookHandlerMethodArgumentResolver;
import ke.co.hoji.server.service.BundlePurchaseService;
import ke.co.hoji.server.service.EmailService;
import ke.co.hoji.server.service.TemplateService;
import ke.co.hoji.server.service.UserService;
import ke.co.hoji.server.service.WebHookMessenger;
import ke.co.hoji.server.service.WebHookService;
import ke.co.hoji.server.templateresolver.ThymeleafDatabaseResourceResolver;
import ke.co.hoji.server.thymeleaf.standard.serializer.HojiJacksonJavaScriptSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.resource.VersionResourceResolver;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.dialect.SpringStandardDialect;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.standard.StandardDialect;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by gitahi on 01/10/15.
 */
@Configuration
@EnableWebMvc
@EnableScheduling
@ComponentScan(
    basePackages = "ke.co.hoji.server",
    excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = ".*Tests?.*")
)
@PropertySource("classpath:hoji.properties")
public class MainConfiguration implements WebMvcConfigurer, ApplicationContextAware {

    @Value("${crypto.algorithm}")
    private String cryptoAlgorithm;

    @Value("${application.version}")
    private String applicationVersion;

    @Value("${fcm.api.key}")
    private String fcmApiKey;

    @Value("${fcm.url}")
    private String fcmUrl;

    @Value("${image.location}")
    private String imageLocation;

    private ApplicationContext applicationContext;

    @Autowired
    private SuspensionInterceptor suspensionInterceptor;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private FormService formService;

    @Autowired
    private FieldService fieldService;

    @Autowired
    private ChoiceGroupService choiceGroupService;

    @Autowired
    private JdbcChoiceDao choiceDao;

    @Autowired
    private TemplateService templateService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private WebHookService webHookService;

    @Autowired
    private WebHookMessenger webHookMessenger;

    @Autowired
    private BundlePurchaseService bundlePurchaseService;

    @Autowired
    private Calculator calculator;

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Bean
    public ITemplateResolver emailTemplateResolver() {
        ThymeleafDatabaseResourceResolver templateResolver = new ThymeleafDatabaseResourceResolver(templateService);
        templateResolver.setOrder(1);
        return templateResolver;
    }

    @Bean
    public ITemplateResolver webTemplateResolver() {
        SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
        templateResolver.setApplicationContext(applicationContext);
        templateResolver.setPrefix("/WEB-INF/templates/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);
        templateResolver.setOrder(2);
        return templateResolver;
    }

    @Bean
    public MultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }

    @Bean
    public SpringTemplateEngine templateEngine() {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        StandardDialect standardDialect = new SpringStandardDialect();
        standardDialect.setJavaScriptSerializer(new HojiJacksonJavaScriptSerializer(objectMapper()));
        templateEngine.setDialect(standardDialect);
        templateEngine.addDialect(new SpringSecurityDialect());
        templateEngine.addTemplateResolver(emailTemplateResolver());
        templateEngine.addTemplateResolver(webTemplateResolver());
        templateEngine.setMessageSource(messageSource());
        return templateEngine;
    }

    @Bean
    public ViewResolver viewResolver() {
        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(templateEngine());
        viewResolver.setCharacterEncoding("UTF-8");
        return viewResolver;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToFieldTypeConverter(fieldService));
        registry.addConverter(new StringToChoiceConverter(choiceDao));
        registry.addConverter(new StringToChoiceGroupConverter(choiceGroupService));
        registry.addConverter(new StringToFieldConverter(fieldService));
        registry.addConverter(new StringToOperatorDescriptorConverter(fieldService));
        registry.addConverter(new StringToFormConverter(formService));
        registry.addConverter(new StringArrayToFormListConverter(formService));
        registry.addConverter(new StringToFormListConverter(formService));
        registry.addConverter(new StringToSurveyConverter(surveyService));
        registry.addConverter(new BooleanToStringConverter());
        registry.addConverter(new IntegerToStringConverter());
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/js/**", "/resources/font/roboto/**")
            .addResourceLocations("/resources/js/", "/resources/font/roboto/")
            .setCacheControl(CacheControl.maxAge(365, TimeUnit.DAYS))
            .resourceChain(false)
            .addResolver(new VersionResourceResolver().addContentVersionStrategy("/**"));
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
        registry.addResourceHandler("/favicon.ico").addResourceLocations("/resources/favicon.ico");
        registry.addResourceHandler("/images/**").addResourceLocations("file:" + imageLocation);
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("/i18/strings");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public LocaleResolver localeResolver() {
        CookieLocaleResolver resolver = new CookieLocaleResolver();
        resolver.setDefaultLocale(new Locale("en"));
        resolver.setCookieName("myLocaleCookie");
        resolver.setCookieMaxAge(4800);
        return resolver;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
        interceptor.setParamName("mylocale");
        registry.addInterceptor(interceptor);
        registry.addInterceptor(suspensionInterceptor);
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(new MappingJackson2HttpMessageConverter(objectMapper()));
        converters.add(byteArrayHttpMessageConverter());
    }

    @Bean
    public ObjectMapper objectMapper() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.featuresToEnable(JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS);
        builder.deserializerByType(ChoiceGroup.class, new ChoiceGroupDeserializer(choiceGroupService));
        builder.deserializerByType(Choice.class, new ChoiceDeserializer(choiceDao));
        builder.deserializerByType(Rule.class, new RuleDeserializer(fieldService));
        builder.deserializerByType(WebHook.class, new WebHookDeserializer(formService));
        builder.serializerByType(Choice.class, new ChoiceSerializer());
        builder.serializerByType(Survey.class, new SurveySerializer((UserService) userDetailsService));
        builder.serializerByType(Form.class, new FormSerializer());
        builder.serializerByType(Property.class, new PropertySerializer());
        builder.serializerByType(Field.class, new FieldSerializer());
        builder.serializerByType(FieldType.class, new FieldTypeSerializer(messageSource()));
        builder.serializerByType(Filter.class, new FilterSerializer());
        builder.serializerByType(WebHook.class, new WebHookSerializer());
        builder.serializerByType(DashboardComponent.class, new DashboardComponentSerializer());
        builder.serializerByType(Uniqueness.class, new UniquenessSerializer());
        builder.serializerByType(InputFormat.class, new InputFormatSerializer());
        builder.serializerByType(FieldAction.class, new FieldActionSerializer());
        builder.serializerByType(EventType.class, new EventTypeSerializer(messageSource()));
        builder.serializerByType(Template.class, new TemplateSerializer());
        return builder.build();
    }

    @Bean
    public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
        ByteArrayHttpMessageConverter converter = new ByteArrayHttpMessageConverter();
        List<MediaType> mediaTypes = new LinkedList<MediaType>();
        mediaTypes.add(MediaType.APPLICATION_JSON);
        mediaTypes.add(MediaType.IMAGE_JPEG);
        mediaTypes.add(MediaType.IMAGE_PNG);
        mediaTypes.add(MediaType.IMAGE_GIF);
        mediaTypes.add(MediaType.TEXT_PLAIN);
        converter.setSupportedMediaTypes(mediaTypes);
        return converter;
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new FilterHandlerMethodArgumentResolver(configService));
        argumentResolvers.add(new WebHookHandlerMethodArgumentResolver(formService, (UserService) userDetailsService));
        PageableHandlerMethodArgumentResolver pageableHandlerMethodArgumentResolver = new PageableHandlerMethodArgumentResolver();
        pageableHandlerMethodArgumentResolver.setOneIndexedParameters(true);
        argumentResolvers.add(pageableHandlerMethodArgumentResolver);
    }

    /**
     * Tells our {@link ApplicationEvent}s to run asynchronously.
     *
     * @return the event multicaster
     */
    @Bean(name = "applicationEventMulticaster")
    public ApplicationEventMulticaster simpleApplicationEventMulticaster() {
        SimpleApplicationEventMulticaster eventMulticaster = new SimpleApplicationEventMulticaster();
        eventMulticaster.setTaskExecutor(taskExecutor());
        return eventMulticaster;
    }

    @Bean
    public TaskExecutor taskExecutor() {
        return new ThreadPoolTaskExecutor();
    }

    @Bean
    public EventProcessorManager eventProcessorManager() {
        UserService userService = (UserService) userDetailsService;
        EventProcessorManager eventProcessorManager = new EventProcessorManager();
        eventProcessorManager.registerEventProcessor(new AccountVerificationTokenEmailer(
            emailService,
            templateEngine(),
            userService)
        );
        eventProcessorManager.registerEventProcessor(new WelcomeEmailer(
            emailService,
            templateEngine(),
            bundlePurchaseService)
        );
        eventProcessorManager.registerEventProcessor(new PasswordResetTokenEmailer(
            emailService,
            templateEngine(),
            userService)
        );
        eventProcessorManager.registerEventProcessor(new PublicProjectAccessActivatedEmailer(emailService, templateEngine()));
        eventProcessorManager.registerEventProcessor(new AccessRequestEmailer(
            emailService,
            templateEngine(),
            userService)
        );
        eventProcessorManager.registerEventProcessor(new TenantChangedEmailer(emailService, templateEngine(), userService));
        eventProcessorManager.registerEventProcessor(new AccountSuspendedEmailer(emailService, templateEngine(), userService));
        eventProcessorManager.registerEventProcessor(new RolesModifiedEmailer(emailService, templateEngine()));
        eventProcessorManager.registerEventProcessor(new UserExpelledEmailer(emailService, templateEngine()));
        eventProcessorManager.registerEventProcessor(new CreditsExpiredEmailer(emailService, templateEngine()));
        eventProcessorManager.registerEventProcessor(new FreeCreditsExpiredEmailer(emailService, templateEngine()));
        eventProcessorManager.registerEventProcessor(new ExpirationNoticeEmailer(emailService, templateEngine()));
        eventProcessorManager.registerEventProcessor(new FreebieExpirationNoticeEmailer(emailService, templateEngine()));
        eventProcessorManager.registerEventProcessor(new MainRecordCreatedEmailer(emailService, templateEngine()));
        eventProcessorManager.registerEventProcessor(new MainRecordCreatedCalculator(configService, calculator));
        eventProcessorManager.registerEventProcessor(
            new MainRecordWebHookPusher(
                applicationContext,
                webHookService,
                formService,
                fieldService,
                (UserService) userDetailsService,
                webHookMessenger
            )
        );
        eventProcessorManager.registerEventProcessor(
            new MainRecordWebSocketPusher(simpMessagingTemplate, applicationContext, userService, formService, fieldService)
        );
        eventProcessorManager.registerEventProcessor(new SolicitOfficerReviewEmailer(emailService, templateEngine()));
        eventProcessorManager.registerEventProcessor(new SolicitEnumeratorReviewEmailer(emailService, templateEngine()));
        eventProcessorManager.registerEventProcessor(new PublicationNotificationPusher(fcmApiKey, fcmUrl));
        return eventProcessorManager;
    }
}
