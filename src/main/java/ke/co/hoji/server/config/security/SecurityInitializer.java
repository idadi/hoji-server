package ke.co.hoji.server.config.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by gitahi on 01/10/15.
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
