package ke.co.hoji.server.config.security;

import ke.co.hoji.server.controller.AjaxAccessDeniedExceptionHandler;
import ke.co.hoji.server.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by gitahi on 01/10/15.
 */
@Order(1)
@Configuration
@EnableWebSecurity
public class ApiSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationProvider authenticationProvider;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
        auth.authenticationProvider(authenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.antMatcher("/api/**").sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER)
            .and().httpBasic().authenticationEntryPoint(restAuthenticationEntryPoint)
            .and().authorizeRequests()
            .antMatchers("/api/user/authenticate", "/api/lipisha/ipn", "/api/user/signUp").permitAll()
            .antMatchers(HttpMethod.GET,
                    "/api/project/747/form/807/report/dashboard/component",
                    "/api/project/747/form/807/report/pivot").permitAll()
            .antMatchers(
                "/api/user/changeTenant",
                "/api/user/requestAccess",
                "/api/project/{projectId}/form/{formId}").authenticated()
            .antMatchers(
                "/api/download/surveys",
                "/api/download/projects",
                "/api/download/survey",
                "/api/download/project",
                "/api/download/forms",
                "/api/download/fields",
                "/api/download/references",
                "/api/download/properties",
                "/api/download/languages",
                "/api/download/translations",
                "/api/upload/data",
                "/api/import/search",
                "/api/import/record",
                "/api/import/records"
            )
            .access("hasRole('" + User.Role.DEVICE.role() + "')")
            .antMatchers(HttpMethod.GET,
                "/",
                "/api/groups/**",
                "/api/choices/**",
                "/**/view/**",
                "/**/list/**",
                "/field/autoNumber",
                "/**/report/filter",
                "/**/report/dashboard/component",
                "/**/report/analysis",
                "/**/report/data",
                "/**/report/monitor",
                "/**/report/pivot",
                "/**/report/gallery",
                "/**/report/evolution",
                "/**/report/dashboard",
                "/**/report/map",
                "/rule/field/choices",
                "/**/record",
                "/hoji-websocket/**"
            ).access("hasRole('" + User.Role.VIEWER.role() + "')")
            .antMatchers("/**/report/download").access("hasRole('" + User.Role.DOWNLOADER.role() + "')")
            .antMatchers(
                "/api/groups/**",
                "/api/bulk/groups",
                "/api/hook/{id}/message",
                "/api/**/dashboard/**",
                "/api/record/**"
            )
            .access("hasRole('" + User.Role.CREATOR.role() + "')")
            .anyRequest().denyAll()
            .and().csrf().disable()
            .exceptionHandling().accessDeniedHandler(accessDeniedHandler());
    }

    @Bean(name = "authenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * Created by gitahi on 14/10/15.
     */
    @Component
    static class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

        @Override
        public void commence(HttpServletRequest request, HttpServletResponse response,
                             AuthenticationException authException) throws IOException {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage());
        }
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new AjaxAccessDeniedExceptionHandler();
    }
}
