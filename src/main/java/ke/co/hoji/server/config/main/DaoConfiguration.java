package ke.co.hoji.server.config.main;

import com.mongodb.MongoClientURI;
import ke.co.hoji.server.dao.model.JdbcFormDao;
import ke.co.hoji.server.dao.impl.JdbcCashTransactionDao;
import ke.co.hoji.server.dao.impl.JdbcCreditTransactionDao;
import ke.co.hoji.server.dao.impl.JdbcDashboardComponentDao;
import ke.co.hoji.server.dao.impl.JdbcLoggableEventDao;
import ke.co.hoji.server.dao.impl.JdbcTemplateDao;
import ke.co.hoji.server.dao.impl.JdbcUserDao;
import ke.co.hoji.server.dao.impl.JdbcWebHookDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.annotation.Order;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
@ComponentScan(basePackages = "ke.co.hoji.server.dao")
@PropertySource("classpath:mongo.properties")
@Order(2)
public class DaoConfiguration {

    @Autowired
    JdbcTemplate template;

    @Autowired
    JdbcFormDao formDao;

    @Bean
    public JdbcUserDao userDao() {
        return new JdbcUserDao(template);
    }

    @Bean
    public JdbcCashTransactionDao cashTransactionDao() {
        JdbcCashTransactionDao cashTransactionDao = new JdbcCashTransactionDao(template, userDao());
        return cashTransactionDao;
    }

    @Bean
    public JdbcCreditTransactionDao creditTransactionDao() {
        JdbcCreditTransactionDao creditTransactionDao = new JdbcCreditTransactionDao(template, userDao());
        return creditTransactionDao;
    }

    @Bean
    public JdbcLoggableEventDao loggableEventDao() {
        JdbcLoggableEventDao loggableEventDao = new JdbcLoggableEventDao(template, userDao());
        return loggableEventDao;
    }

    @Bean
    public JdbcTemplateDao jdbcTemplateDao() {
        return new JdbcTemplateDao(template);
    }

    @Bean
    public JdbcWebHookDao webHookDao() {
        return new JdbcWebHookDao(template, formDao);
    }

    @Bean
    public JdbcDashboardComponentDao dashboardComponentDao() {
        return new JdbcDashboardComponentDao(template);
    }

    @Value("${mongo.uri}")
    private String mongoUri;

    @Bean
    public MongoDbFactory mongoDbFactory() throws Exception {
        return new SimpleMongoDbFactory(new MongoClientURI(mongoUri));
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory());
        return mongoTemplate;
    }
}
