package ke.co.hoji.server.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;

public class KeyGenerator {

    private JdbcTemplate template;
    private String table;
    private Integer nextId;
    private Integer maxId;
    private Integer incrementBy;

    public KeyGenerator(JdbcTemplate template, String table, Integer incrementBy) {
        this.template = template;
        this.table = table;
        this.incrementBy = incrementBy;
        this.nextId = this.maxId = 0;
    }

    public synchronized Integer nextKey() {
        if (nextId.equals(maxId)) {
            reserveIds();
        }
        return nextId++;
    }

    private void reserveIds() {
        String select = "SELECT next_id FROM key_holder WHERE table_name = ? FOR UPDATE";
        Integer newNextId = template.queryForObject(select, new Object[]{table}, Integer.class);
        Integer newMaxId = newNextId + incrementBy;

        String update = "UPDATE key_holder SET next_id = ? WHERE table_name = ?";
        template.update(update, newMaxId, table);
        nextId = newNextId;
        maxId = newMaxId;
    }
}
