package ke.co.hoji.server.dao.dto;

import ke.co.hoji.core.data.ReferenceLevel;
import ke.co.hoji.core.data.dto.Reference;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by gitahi on 28/08/15.
 */
@Component("dtoJdbcReferenceDao")
public class JdbcReferenceDao {

    private final JdbcTemplate template;

    public JdbcReferenceDao(JdbcTemplate template) {
        this.template = template;
    }

    public List<ReferenceLevel> getReferenceLevels() {
        String select = "SELECT "
                + "id, name, level, level_column, displayable, input_type, rulable, missing_action, survey_id, enabled "
                + "FROM reference_level "
                + "ORDER BY level";
        List<ReferenceLevel> referenceLevels = template.query(select, new ReferenceLevelRowMapper());
        return referenceLevels;
    }

    public List<Reference> getReferencesBySurvey(Integer surveyId) {
        String select = "SELECT id, level_0, level_1, level_2, level_3, level_4, level_5, level_6 "
                + "FROM reference "
                + "WHERE survey_id = ?";
        List<Reference> references = template.query(select, new Object[]{surveyId}, new ReferenceRowMapper());
        return references;
    }

    private class ReferenceRowMapper implements RowMapper<Reference> {

        public Reference mapRow(ResultSet resultSet, int i) throws SQLException {
            Reference reference = new Reference
                    (
                            resultSet.getInt("id"),
                            getReferenceLevels()
                    );
            reference.setValues(new LinkedHashMap<Integer, String>());
            for (ReferenceLevel referenceLevel : reference.getReferenceLevels()) {
                reference.getValues().put(referenceLevel.getLevel(), resultSet.getString(referenceLevel.getLevelColumn()));
            }
            return reference;
        }
    }

    private class ReferenceLevelRowMapper implements RowMapper<ReferenceLevel> {

        public ReferenceLevel mapRow(ResultSet resultSet, int i) throws SQLException {
            ReferenceLevel referenceLevel = new ReferenceLevel
                    (
                            resultSet.getInt("id"),
                            resultSet.getString("name"),
                            resultSet.getBoolean("enabled"),
                            resultSet.getInt("level"),
                            resultSet.getString("level_column"),
                            resultSet.getBoolean("displayable"),
                            resultSet.getInt("input_type"),
                            resultSet.getBoolean("rulable"),
                            resultSet.getInt("missing_action")
                    );
            return referenceLevel;
        }
    }
}
