package ke.co.hoji.server.dao.model;

import ke.co.hoji.core.data.ReferenceLevel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by gitahi on 30/06/15.
 */
@Component
public class JdbcReferenceDao {

    private final JdbcTemplate template;


    public JdbcReferenceDao(JdbcTemplate template) {
        this.template = template;
    }

    public List<ReferenceLevel> getReferenceLevels() {
        String select = "SELECT "
                + "id, name, level, level_column, displayable, input_type, rulable, missing_action, survey_id, enabled "
                + "FROM reference_level "
                + "ORDER BY level";
        List<ReferenceLevel> referenceLevels = template.query(select, new ReferenceLevelRowMapper());
        return referenceLevels;
    }

    private class ReferenceLevelRowMapper implements RowMapper<ReferenceLevel> {

        public ReferenceLevel mapRow(ResultSet resultSet, int i) throws SQLException {
            ReferenceLevel referenceLevel = new ReferenceLevel
                    (
                            resultSet.getInt("id"),
                            resultSet.getString("name"),
                            resultSet.getBoolean("enabled"),
                            resultSet.getInt("level"),
                            resultSet.getString("level_column"),
                            resultSet.getBoolean("displayable"),
                            resultSet.getInt("input_type"),
                            resultSet.getBoolean("rulable"),
                            resultSet.getInt("missing_action")
                    );
            return referenceLevel;
        }
    }
}
