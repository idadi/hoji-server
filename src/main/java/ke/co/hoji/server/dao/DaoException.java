package ke.co.hoji.server.dao;

import java.util.Map;

public class DaoException extends RuntimeException {
    private String statement;
    private Map<String, Object> params;

    public DaoException() {
    }

    public DaoException(String msg) {
        super(msg);
    }

    public DaoException(Throwable cause, String statement) {
        super(cause);
        this.statement = statement;
    }

    public DaoException(Throwable cause, String statement, Map<String, Object> params) {
        super(cause);
        this.statement = statement;
        this.params = params;
    }

    public String getStatement() {
        return this.statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public Map<String, Object> getParams() {
        return this.params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

}
