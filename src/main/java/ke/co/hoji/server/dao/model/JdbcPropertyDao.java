package ke.co.hoji.server.dao.model;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.dao.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by gitahi on 29/06/15.
 */
@Component
public class JdbcPropertyDao {

    private final JdbcTemplate template;

    @Autowired
    public JdbcPropertyDao(JdbcTemplate template) {
        this.template = template;
    }

    public String selectProperty() {
        return "SELECT property_key, value, survey_id FROM property ";
    }

    public Property getProperty(String key, Integer surveyId) {
        String sql = selectProperty() + "WHERE property_key = ? AND survey_id = ?";
        List<Property> properties = template.query(sql, new Object[]{key, surveyId}, new PropertyMapper());
        if (properties.size() > 0) {
            return properties.get(0);
        } else {
            return null;
        }
    }

    public List<Property> getProperties(Integer surveyId) {
        String sql = selectProperty() + "WHERE survey_id = ?";
        return template.query(sql, new Object[]{surveyId}, new PropertyMapper());
    }

    public Property saveProperty(Property property) throws DaoException {
        Property fromDb = getProperty(property.getKey(), property.getSurvey().getId());
        if (fromDb == null) {
            insert(property);
        } else {
            update(property);
        }
        return property;
    }

    public List<Property> saveProperties(Collection<Property> properties) throws DaoException {
        if (properties.size() == 0) {
            return Collections.emptyList();
        }
        Integer surveyId = properties.iterator().next().getSurvey().getId();
        List<Object> allKeys = properties.stream().map(Property::getKey).collect(Collectors.toList());
        String select = "SELECT property_key FROM property WHERE property_key IN ("
                + Utils.string(allKeys.stream().map(id -> "").collect(Collectors.toList()), ", ", false, "?", true)
                + ") AND survey_id = ?";
        List<Object> parameters = new ArrayList<>(allKeys);
        parameters.add(surveyId);
        List<String> existingKeys = template.queryForList(select, parameters.toArray(), String.class);
        List<Property> propsToCreate = new ArrayList<>();
        List<Property> propsToUpdate = new ArrayList<>();
        properties.forEach(property -> {
            if (existingKeys.contains(property.getKey())) {
                propsToUpdate.add(property);
            } else {
                propsToCreate.add(property);
            }
        });
        if (propsToCreate.size() > 0) {
            String sql =
                "INSERT INTO property (property_key, value, survey_id) VALUES (?, ?, ?)";
            template.batchUpdate(sql, properties, 500, (preparedStatement, property) -> {
                preparedStatement.setString(1, property.getKey());
                preparedStatement.setString(2, property.getValue());
                preparedStatement.setInt(3, property.getSurvey().getId());
            });
        }
        if (propsToUpdate.size() > 0) {
            String sql = "UPDATE property SET value = ? WHERE property_key = ? AND survey_id = ?";
            template.batchUpdate(sql, properties, 500, (preparedStatement, property) -> {
                preparedStatement.setString(1, property.getValue());
                preparedStatement.setString(2, property.getKey());
                preparedStatement.setInt(3, property.getSurvey().getId());
            });
        }
        return new ArrayList<>(properties);
    }

    public boolean deleteProperty(String key, Integer surveyId) {
        Property property = getProperty(key, surveyId);
        if (property == null) {
            return false;
        }
        String sql = "DELETE FROM property WHERE property_key = ? AND survey_id = ?";
        return template.update(sql, key, surveyId) > 0;
    }

    private Property insert(Property property) throws DaoException {
        String sql = "INSERT INTO property (property_key, value, survey_id) VALUES (?, ?, ?)";
        template.update(sql, property.getKey(), property.getValue(), property.getSurvey().getId());
        return property;
    }

    private Property update(Property property) throws DaoException {
        String sql = "UPDATE property SET value = ? WHERE property_key = ? AND survey_id = ?";
        template.update(sql, property.getValue(), property.getKey(), property.getSurvey().getId());
        return property;
    }

    private class PropertyMapper implements RowMapper<Property> {

        public Property mapRow(ResultSet resultSet, int i) throws SQLException {
            String key = resultSet.getString("property_key");
            String value = resultSet.getString("value");
            Integer surveyId = resultSet.getInt("survey_id");
            Survey survey = new Survey();
            survey.setId(surveyId);
            return new Property(key, value, survey);
        }
    }
}
