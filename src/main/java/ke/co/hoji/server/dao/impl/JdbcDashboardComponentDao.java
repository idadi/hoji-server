package ke.co.hoji.server.dao.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.dao.DaoException;
import ke.co.hoji.server.model.DashboardComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class JdbcDashboardComponentDao {

    private final JdbcTemplate template;

    private final ObjectMapper mapper;

    private final KeyGenerator keyGenerator;

    private final Logger logger = LoggerFactory.getLogger(JdbcDashboardComponentDao.class);

    public JdbcDashboardComponentDao(JdbcTemplate template) {
        this.template = template;
        this.mapper = new ObjectMapper();
        this.keyGenerator = new KeyGenerator(template, "dashboard_component", 1);
    }

    public DashboardComponent findDashboardComponentById(Integer componentId) {
        String sql = "SELECT id, name, ordinal, configuration, form_id FROM dashboard_component WHERE id = ?";
        List<DashboardComponent> dashboardComponents =
                template.query(sql, new Object[]{componentId}, new DashboardComponentMapper());;
        if (dashboardComponents.size() == 1) {
            DashboardComponent dashboardComponent = dashboardComponents.get(0);
            return dashboardComponent;
        } else {
            throw new DaoException(
                    "More than one DashboardComponent returned by the database where a single DashboardComponent was expected."
            );
        }
    }

    public List<DashboardComponent> findDashboardComponentsFor(Integer formId) {
        String sql = "SELECT id, name, ordinal, configuration, form_id FROM dashboard_component WHERE form_id = ?";
        return template.query(sql, new Object[]{formId}, new DashboardComponentMapper());
    }

    public DashboardComponent saveDashboardComponent(DashboardComponent dashboardComponent) {
        if (dashboardComponent.getId() == null) {
            dashboardComponent.setId(keyGenerator.nextKey());
            String insert = "INSERT INTO dashboard_component  (id, name, ordinal, configuration, form_id) "
                    + "VALUES (?, ?, ?, ?, ?)";
            Object[] parameters = new Object[]{
                    dashboardComponent.getId(),
                    dashboardComponent.getName(),
                    dashboardComponent.getOrdinal(),
                    dashboardComponent.getConfiguration().toString(),
                    dashboardComponent.getForm().getId()
            };
            template.update(insert, parameters);
        } else {
            String update = "UPDATE dashboard_component SET name = ?, ordinal = ?, configuration = ?, form_id = ? WHERE id = ?";
            Object[] parameters = new Object[]{
                    dashboardComponent.getName(),
                    dashboardComponent.getOrdinal(),
                    dashboardComponent.getConfiguration().toString(),
                    dashboardComponent.getForm().getId(),
                    dashboardComponent.getId()
            };
            template.update(update, parameters);
        }
        return dashboardComponent;
    }

    public void updateAllDashboardComponents(List<DashboardComponent> components) {
        String update = "UPDATE dashboard_component SET name = ?, ordinal = ?, configuration = ?, form_id = ? WHERE id = ?";

        template.batchUpdate(update, components, components.size(),
                (ps, component) -> {
                    ps.setString(1, component.getName());
                    ps.setBigDecimal(2, component.getOrdinal());
                    ps.setString(3, component.getConfiguration().toString());
                    ps.setInt(4, component.getForm().getId());
                    ps.setInt(5, component.getId());
                }
        );
    }

    public void deleteDashboardComponent(Integer componentId) {
        String delete = "DELETE FROM dashboard_component WHERE id = ?";
        template.update(delete, componentId);
    }

    private class DashboardComponentMapper implements RowMapper<DashboardComponent> {
        @Override
        public DashboardComponent mapRow(ResultSet rs, int rowNum) throws SQLException {
            DashboardComponent component = new DashboardComponent();
            component.setId(rs.getInt("id"));
            component.setName(rs.getString("name"));
            component.setOrdinal(rs.getBigDecimal("ordinal"));
            try {
                component.setConfiguration(mapper.readTree(rs.getString("configuration")));
            } catch (IOException e) {
                logger.error("Corrupted dashboard component configuration");
                logger.error(e.getMessage(), e);
            }
            component.setForm(new Form(rs.getInt("form_id")));
            return component;
        }
    }
}
