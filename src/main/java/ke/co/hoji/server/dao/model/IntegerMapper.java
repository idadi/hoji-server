package ke.co.hoji.server.dao.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class IntegerMapper implements RowMapper<Integer> {

    private final String column;

    IntegerMapper(String column) {
        this.column = column;
    }

    public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
        return resultSet.getInt(column);
    }
}
