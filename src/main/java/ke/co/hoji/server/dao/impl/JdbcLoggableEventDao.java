package ke.co.hoji.server.dao.impl;

import ke.co.hoji.server.event.model.EventOriginator;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.event.model.LoggableEvent;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class JdbcLoggableEventDao {

    private JdbcTemplate template;

    private JdbcUserDao userDao;

    private final String selectLoggableEvent = "SELECT id, name, date, user_id, tenant_id, type, subject_name, "
        + "subject_id, description, result, source, originator, failure_message FROM loggable_event ";

    public JdbcLoggableEventDao(JdbcTemplate template, JdbcUserDao userDao) {
        this.template = template;
        this.userDao = userDao;
    }

    public void log(LoggableEvent loggableEvent) {
        String sql = "INSERT INTO loggable_event (name, date, user_id, tenant_id, type, subject_name, "
            + "subject_id, description, result, source, originator, failure_message) "
            + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Object[] parameters = new Object[]{
            loggableEvent.getName(),
            loggableEvent.getDate(),
            loggableEvent.getUser().getId(),
            loggableEvent.getTenant().getId(),
            loggableEvent.getType().toString(),
            loggableEvent.getSubjectName(),
            loggableEvent.getSubjectId(),
            loggableEvent.getDescription(),
            loggableEvent.getResult().toString(),
            loggableEvent.getSource().toString(),
            loggableEvent.getOriginator().toString(),
            loggableEvent.getFailureMessage()
        };
        template.update(sql, parameters);
    }

    public List<LoggableEvent> findEventsForReviewSolicitation(Date from, Date to) {
        String sql = selectLoggableEvent
            + "WHERE type IN ('MAIN_RECORD_CREATE','ANALYSIS','TABLE','PIVOT','DASHBOARD','CSV','MAP','MONITOR') "
            + "AND result = 'SUCCESS' AND date >= ? AND date <= ? AND user_id NOT IN ( "
            + "      SELECT user_id FROM loggable_event "
            + "      WHERE type IN('SOLICIT_OFFICER_REVIEW','SOLICIT_ENUMERATOR_REVIEW')"
            + ")";
        return template.query(sql, new Object[]{from, to}, new LoggableEventRowMapper());
    }

    public boolean isFirstForUser(String eventTypeName, Integer userId) {
        String sql = "SELECT COUNT(*) FROM loggable_event WHERE type = ? AND user_id = ?";
        Integer count = template.queryForObject(sql, new Object[]{eventTypeName, userId}, Integer.class);
        return count != null && count == 1;
    }

    private class LoggableEventRowMapper implements RowMapper<LoggableEvent> {

        @Override
        public LoggableEvent mapRow(ResultSet resultSet, int i) throws SQLException {
            LoggableEvent loggableEvent = new LoggableEvent
                (
                    resultSet.getLong("id"),
                    resultSet.getString("name"),
                    resultSet.getDate("date"),
                    userDao.findById(resultSet.getInt("user_id")),
                    userDao.findById(resultSet.getInt("tenant_id")),
                    EventType.valueOf(resultSet.getString("type")),
                    resultSet.getString("subject_name"),
                    resultSet.getString("subject_id"),
                    resultSet.getString("description"),
                    EventResult.valueOf(resultSet.getString("result")),
                    EventSource.valueOf(resultSet.getString("source")),
                    EventOriginator.valueOf(resultSet.getString("originator")),
                    resultSet.getString("failure_message")

                );
            return loggableEvent;
        }
    }
}
