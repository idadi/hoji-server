package ke.co.hoji.server.dao.impl;

import ke.co.hoji.server.model.CashTransaction;
import ke.co.hoji.server.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by gitahi on 05/01/18.
 */
public class JdbcCashTransactionDao {

    private final JdbcTemplate template;

    private final JdbcUserDao userDao;

    public JdbcCashTransactionDao(JdbcTemplate template, JdbcUserDao userDao) {
        this.template = template;
        this.userDao = userDao;
    }

    protected List<CashTransaction> select(Map<String, Object> params) {
        String select = "SELECT " +
                "id, user_id, date, reference_no, type, amount_usd, amount_lcl, currency, payment_mode, payment_reference, bundle_id " +
                "FROM cash_transaction ";
        StringBuilder filter = new StringBuilder();
        for (String column : params.keySet()) {
            if (filter.length() > 0) {
                filter.append("AND ");
            }
            filter.append(column).append(" = ? ");
        }
        String sql = filter.length() > 0 ? select + filter : select;
        return template.query(sql, params.values().toArray(), new CashTransactionRowMapper());
    }

    public void create(ke.co.hoji.server.model.CashTransaction cashTransaction) {
        String sql = "INSERT INTO cash_transaction (user_id, date, reference_no, type, amount_usd)" +
                "VALUES (?, ?, ?, ?, ?)";
        Object[] parameters = new Object[5];
        parameters[0] = cashTransaction.getUser().getId();
        parameters[1] = cashTransaction.getDate();
        parameters[2] = cashTransaction.getReferenceNo();
        parameters[3] = cashTransaction.getType();
        parameters[4] = cashTransaction.getAmountUsd();
        template.update(sql, parameters);
    }

    public BigDecimal balance(User user, Date date) {
        String select = "SELECT SUM(amount_usd) FROM cash_transaction WHERE user_id = ? AND date <= ?";
        Object[] parameters = new Object[2];
        parameters[0] = user.getId();
        parameters[1] = date;
        BigDecimal balance = template.queryForObject(select, parameters, BigDecimal.class);
        if (balance == null) {
            balance = BigDecimal.ZERO;
        }
        return balance;
    }

    public boolean transactionExists(User user, Date date, String referenceNo) {
        String select = "SELECT COUNT(id) FROM cash_transaction WHERE user_id = ? AND date = ? AND reference_no = ?";
        Object[] parameters = new Object[3];
        parameters[0] = user.getId();
        parameters[1] = date;
        parameters[2] = referenceNo;
        Integer count = template.queryForObject(select, parameters, Integer.class);
        return !Integer.valueOf(0).equals(count);
    }

    private class CashTransactionRowMapper implements RowMapper<ke.co.hoji.server.model.CashTransaction> {

        @Override
        public ke.co.hoji.server.model.CashTransaction mapRow(ResultSet resultSet, int i) throws SQLException {
            ke.co.hoji.server.model.CashTransaction cashTransaction = new ke.co.hoji.server.model.CashTransaction
                    (
                            userDao.findById(resultSet.getInt("user_id")),
                            resultSet.getDate("date"),
                            resultSet.getString("reference_no"),
                            resultSet.getString("type"),
                            resultSet.getBigDecimal("amount_usd")
                    );
            cashTransaction.setId(resultSet.getLong("id"));
            cashTransaction.setAmountLcl(resultSet.getBigDecimal("amount_lcl"));
            cashTransaction.setCurrency(resultSet.getString("currency"));
            cashTransaction.setPaymentMode(resultSet.getString("payment_mode"));
            return cashTransaction;
        }
    }
}
