package ke.co.hoji.server.dao.impl;

import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.Template;
import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class JdbcTemplateDao {

    private JdbcTemplate template;

    public JdbcTemplateDao(JdbcTemplate template) {
        this.template = template;
    }

    public List<Template> getAllTemplates() {
        String sql = "SELECT id, event_type, template FROM email_template";
        return template.query(sql, new TemplateRowMapper());
    }

    public List<Template> getTemplateByEvent(EventType eventType) {
        String sql = "SELECT id, event_type, template FROM email_template WHERE event_type = ?";
        return template.query(sql, new Object[]{eventType.toString()}, new TemplateRowMapper());
    }

    public Template saveTemplate(Template emailTemplate) {
        if (emailTemplate.getId() == null) {
            String sql = "INSERT INTO email_template (event_type, template) VALUES (?, ?)";
            Object[] parameters = new Object[]{
                    emailTemplate.getEventType().toString(),
                    emailTemplate.getTemplate()
            };
            KeyHolder keyHolder = new GeneratedKeyHolder();
            template.update(connection -> {
                        ArgumentPreparedStatementSetter argumentPreparedStatementSetter
                                = new ArgumentPreparedStatementSetter(parameters);
                        String[] parameterNames = {"event_type", "template"};
                        PreparedStatement ps = connection.prepareStatement(sql, parameterNames);
                        argumentPreparedStatementSetter.setValues(ps);
                        return ps;
                    },
                    keyHolder);
            emailTemplate.setId(keyHolder.getKey().intValue());
        } else {
            String sql = "UPDATE email_template SET event_type = ?, template = ? WHERE id = ?";
            Object[] parameters = new Object[]{
                    emailTemplate.getEventType().toString(),
                    emailTemplate.getTemplate(),
                    emailTemplate.getId()
            };
            template.update(sql, parameters);
        }
        return emailTemplate;
    }

    public Set<EventType> getAssociatedEvents() {
        String select = "SELECT DISTINCT event_type FROM email_template";
        List<EventType> eventTypes = template.query(select, new EventTypeRowMapper());
        return new HashSet<>(eventTypes);
    }

    private class TemplateRowMapper implements RowMapper<Template> {

        @Override
        public Template mapRow(ResultSet rs, int rowNum) throws SQLException {
            Template template = new Template();
            template.setId(rs.getInt("id"));
            template.setEventType(EventType.valueOf(rs.getString("event_type")));
            template.setTemplate(rs.getString("template"));
            return template;
        }
    }

    private class EventTypeRowMapper implements RowMapper<EventType> {

        @Override
        public EventType mapRow(ResultSet rs, int rowNum) throws SQLException {
            return EventType.valueOf(rs.getString("event_type"));
        }
    }
}
