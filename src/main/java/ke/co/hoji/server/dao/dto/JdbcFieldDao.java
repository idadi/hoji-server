package ke.co.hoji.server.dao.dto;

import ke.co.hoji.core.data.dto.Field;
import ke.co.hoji.core.data.dto.Rule;
import ke.co.hoji.server.dao.model.JdbcChoiceGroupDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by gitahi on 28/08/15.
 */
@Component("dtoJdbcFieldDao")
public class JdbcFieldDao {

    private final JdbcTemplate template;

    private final JdbcChoiceGroupDao choiceGroupDao;

    private final String selectRule = "SELECT "
            + "id, type, ordinal, owner_id, operator_descriptor_id, value, target_id, combiner, grouper "
            + "FROM rule ";

    @Autowired
    public JdbcFieldDao(
            JdbcTemplate template,
            JdbcChoiceGroupDao choiceGroupDao
    ) {
        this.template = template;
        this.choiceGroupDao = choiceGroupDao;
    }

    private String selectField() {
        return "SELECT "
                + "id, "
                + "calculated, "
                + "captioning, "
                + "choice_filter_field_id, "
                + "choice_group_id, "
                + "variable_name, "
                + "default_value, "
                + "description, "
                + "instructions, "
                + "enable_if_null, "
                + "enabled, "
                + "field_type_id, "
                + "filterable, "
                + "form_id, "
                + "main_record_field_id, "
                + "matrix_record_field_id, "
                + "max_action, "
                + "max_value, "
                + "min_action, "
                + "min_value, "
                + "missing_action, "
                + "missing_value, "
                + "name, "
                + "ordinal, "
                + "output_type, "
                + "parent_id, "
                + "pipe_source_id, "
                + "reference_field_id, "
                + "regex_action, "
                + "regex_value, "
                + "response_inheriting, "
                + "searchable, "
                + "tag, "
                + "unique_action, "
                + "uniqueness, "
                + "validation_script, "
                + "value_script "
                + "FROM field ";
    }

    public List<Field> getFieldsByForm(Integer formId) {
        String sql = selectField() + "WHERE form_id = ? ORDER BY ordinal";
        List<Field> fields = template.query(sql, new Object[]{formId}, new FieldMapper());
        return fields;
    }

    private List<Integer> getRecordFormIds(Field field) {
        String select = "SELECT form_id FROM field_record_form WHERE field_id = ?";
        List<Integer> formIds = template.query(select, new Object[]{field.getId()}, new IntegerMapper("form_id"));
        return formIds;
    }

    private List<Rule> getRules(Field owner) {
        String sql = selectRule + "WHERE owner_id = ? ORDER BY ordinal";
        return template.query(sql, new Object[]{ owner.getId() }, new RuleMapper(owner));
    }

    private class FieldMapper implements RowMapper<Field> {

        public Field mapRow(ResultSet resultSet, int i) throws SQLException {
            Field field = new Field();
            field.setId(resultSet.getInt("id"));
            field.setName(resultSet.getString("name"));
            field.setEnabled(resultSet.getBoolean("enabled"));
            field.setDescription(resultSet.getString("description"));
            field.setOrdinal(resultSet.getBigDecimal("ordinal"));
            field.setCaptioning(resultSet.getBoolean("captioning"));
            field.setSearchable(resultSet.getBoolean("searchable"));
            field.setFilterable(resultSet.getBoolean("filterable"));
            field.setOutputType(resultSet.getInt("output_type"));
            field.setMissingAction(resultSet.getInt("missing_action"));
            field.setMissingValue(resultSet.getString("missing_value"));
            field.setColumn(resultSet.getString("variable_name"));
            field.setInstructions(resultSet.getString("instructions"));
            field.setTag(resultSet.getString("tag"));
            field.setCalculated(resultSet.getInt("calculated"));
            field.setValueScript(resultSet.getString("value_script"));
            field.setDefaultValue(resultSet.getString("default_value"));
            field.setMissingValue(resultSet.getString("missing_value"));
            field.setMinValue(resultSet.getString("min_value"));
            field.setMaxValue(resultSet.getString("max_value"));
            field.setRegexValue(resultSet.getString("regex_value"));
            field.setUniqueness(resultSet.getInt("uniqueness"));
            field.setTypeId(resultSet.getInt("field_type_id"));
            field.setPipeSourceId(resultSet.getInt("pipe_source_id"));
            field.setChoiceFilterFieldId(resultSet.getInt("choice_filter_field_id"));
            field.setReferenceFieldId(resultSet.getInt("reference_field_id"));
            field.setMainRecordFieldId(resultSet.getInt("main_record_field_id"));
            field.setMatrixRecordFieldId(resultSet.getInt("matrix_record_field_id"));
            field.setFormId(resultSet.getInt("form_id"));
            field.setParentId(resultSet.getInt("parent_id"));
            field.setChoiceGroup(choiceGroupDao.getChoiceGroupById(resultSet.getInt("choice_group_id")));
            field.setRecordFormIds(getRecordFormIds(field));
            field.setRules(getRules(field));
            field.setResponseInheriting(resultSet.getBoolean("response_inheriting"));
            return field;
        }
    }

    private class RuleMapper implements RowMapper<Rule> {

        private final Field owner;

        RuleMapper(Field owner) {
            this.owner = owner;
        }

        public Rule mapRow(ResultSet resultSet, int i) throws SQLException {
            Rule rule = new Rule
                    (
                            resultSet.getInt("id"),
                            resultSet.getBigDecimal("ordinal"),
                            resultSet.getString("value"),
                            resultSet.getInt("type"),
                            resultSet.getInt("grouper")
                    );
            rule.setOwnerId(owner.getId());
            rule.setTargetId(resultSet.getInt("target_id"));
            rule.setOperatorDescriptorId(resultSet.getInt("operator_descriptor_id"));
            rule.setCombiner(resultSet.getInt("combiner"));
            return rule;
        }
    }

    private class IntegerMapper implements RowMapper<Integer> {

        private final String column;

        IntegerMapper(String column) {
            this.column = column;
        }

        public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
            return resultSet.getInt(column);
        }
    }
}
