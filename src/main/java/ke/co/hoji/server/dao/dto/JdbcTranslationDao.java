package ke.co.hoji.server.dao.dto;

import ke.co.hoji.core.data.dto.Translation;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component("dtoJdbcTranslationDao")
public class JdbcTranslationDao {

    private final JdbcTemplate template;

    public JdbcTranslationDao(JdbcTemplate template) {
        this.template = template;
    }

    public List<Translation> getTranslationsByLanguage(Integer languageId) {
        String sql = "SELECT id, component, value, translatable, language_id, translatable_id FROM translation WHERE language_id = ?";
        return template.query(sql, new Object[]{languageId}, new TranslationRowMapper());
    }

    private class TranslationRowMapper implements RowMapper<Translation> {

        public Translation mapRow(ResultSet resultSet, int i) throws SQLException {
            Translation translation = new Translation(
                    resultSet.getInt("id"),
                    resultSet.getString("component"),
                    resultSet.getString("value")
            );
            translation.setTranslatableName(resultSet.getString("translatable"));
            translation.setLanguageId(resultSet.getInt("language_id"));
            translation.setTranslatableId(resultSet.getInt("translatable_id"));
            return translation;
        }
    }
}
