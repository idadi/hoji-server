package ke.co.hoji.server.dao.model;

import ke.co.hoji.core.data.model.Language;
import ke.co.hoji.server.dao.DaoException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class JdbcLanguageDao {

    private final JdbcTemplate template;

    public JdbcLanguageDao(JdbcTemplate template) {
        this.template = template;
    }

    public List<Language> getAllLanguages() throws DaoException {
        String sql = "SELECT id, name, survey_id FROM language ";
        return template.query(sql, new LanguageRowMapper());
    }

    private class LanguageRowMapper implements RowMapper<Language> {

        public Language mapRow(ResultSet resultSet, int i) throws SQLException {
            Language language = new Language(
                    resultSet.getInt("id"),
                    resultSet.getString("name")
            );
            return language;
        }
    }
}
