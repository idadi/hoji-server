package ke.co.hoji.server.dao.dto;

import ke.co.hoji.core.data.dto.Language;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component("dtoJdbcLanguageDao")
public class JdbcLanguageDao {

    private final JdbcTemplate template;

    public JdbcLanguageDao(JdbcTemplate template) {
        this.template = template;
    }

    public List<Language> getLanguagesBySurvey(Integer surveyId) {
        String sql = "SELECT id, name, survey_id FROM language WHERE survey_id = ?";
        return template.query(sql, new Object[]{surveyId}, new LanguageRowMapper());
    }

    private class LanguageRowMapper implements RowMapper<Language> {

        public Language mapRow(ResultSet resultSet, int i) throws SQLException {
            Language language = new Language(
                    resultSet.getInt("id"),
                    resultSet.getString("name")
            );
            language.setSurveyId(resultSet.getInt("survey_id"));
            return language;
        }
    }
}
