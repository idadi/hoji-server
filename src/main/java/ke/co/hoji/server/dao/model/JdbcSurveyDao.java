package ke.co.hoji.server.dao.model;

import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.dao.DaoException;
import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gitahi on 15/04/15.
 */
@Component
public class JdbcSurveyDao {

    private final JdbcTemplate template;

    private final String selectSurvey =
        "SELECT survey.id, survey.code, name, user_id, survey.enabled, test_mode, status, access, free FROM survey ";

    public JdbcSurveyDao(JdbcTemplate template) {
        this.template = template;
    }

    public List<Survey> getAllSurveys() {
        String sql = selectSurvey + "ORDER BY survey.enabled desc, survey.id desc";
        List<Survey> surveys = template.query(sql, new SurveyRowMapper());
        return surveys;
    }

    public List<Survey> getSurveysByTenantUserId(Integer tenantUserId, Boolean enabled) {
        List<Object> parameters = new ArrayList<>();
        parameters.add(tenantUserId);
        if (enabled != null) {
            parameters.add(enabled);
        }
        String sql = selectSurvey
            + "WHERE user_id = ? " + (enabled != null ? "AND enabled = ? " : "")
            + "ORDER BY survey.enabled desc, survey.id desc";
        List<Survey> surveys = template.query(sql, parameters.toArray(), new SurveyRowMapper());
        return surveys;
    }

    public Survey getSurveyById(Integer surveyId) {
        String sql = selectSurvey + "WHERE id = ?";
        return singleSurvey(template.query(sql, new Object[]{surveyId}, new SurveyRowMapper()));
    }

    public Survey getSurveyByCode(String code) {
        String sql = selectSurvey + "WHERE code = ?";
        return singleSurvey(template.query(sql, new Object[]{code}, new SurveyRowMapper()));
    }

    public Survey saveSurvey(Survey survey) {
        Survey s;
        if (survey.getId() == null) {
            s = createSurvey(survey);
        } else {
            s = modifySurvey(survey);
        }
        return s;
    }

    @Transactional
    protected Survey createSurvey(Survey survey) {
        String sql = "INSERT INTO survey (code, name, user_id, enabled, status, access, free) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?)";
        Object[] parameters = new Object[7];
        parameters[0] = survey.getCode();
        parameters[1] = survey.getName();
        parameters[2] = survey.getUserId();
        parameters[3] = survey.isEnabled();
        parameters[4] = survey.getStatus();
        parameters[5] = survey.getAccess();
        parameters[6] = survey.isFree();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
                ArgumentPreparedStatementSetter argumentPreparedStatementSetter
                    = new ArgumentPreparedStatementSetter(parameters);
                String[] parameterNames = {
                    "code", "name", "user_id", "enabled", "status", "access", "free"
                };
                PreparedStatement ps = connection.prepareStatement(sql, parameterNames);
                argumentPreparedStatementSetter.setValues(ps);
                return ps;
            },
            keyHolder);
        survey.setId(keyHolder.getKey().intValue());
        return survey;
    }

    protected Survey modifySurvey(Survey survey) {
        String sql = "UPDATE survey SET name = ?, access = ? WHERE id = ?";
        template.update(sql, survey.getName(), survey.getAccess(), survey.getId());
        return survey;
    }

    @Transactional
    public void setPublishedStatus(Survey survey, Integer status) {
        Survey fromDb = getSurveyById(survey.getId());
        if (fromDb.getStatus() == Survey.UNPUBLISHED && status == Survey.MODIFIED) {
            return;
        }
        String sql = "UPDATE survey SET status = ? WHERE id = ?";
        template.update(sql, status, survey.getId());
        fromDb.setStatus(status);
    }

    @Transactional
    public void deleteSurvey(Integer id) {
        Survey survey = getSurveyById(id);
        if (survey != null) {
            String sql = "DELETE FROM survey WHERE id = ?";
            template.update(sql, id);
        }
    }

    public void enableSurvey(Survey survey, boolean enabled) {
        String sql = "UPDATE survey SET enabled = ? WHERE id = ?";
        template.update(sql, enabled, survey.getId());
    }

    public List<Survey> getPublicSurveys(int publicSurveyAccess) {
        Map<String, Object> params = new HashMap<>();
        params.put("access", Survey.Access.PRIVATE);
        String query = selectSurvey
            + "INNER JOIN user ON survey.user_id = user.id "
            + "WHERE access != ? ";
        List<Survey> surveys = template.query(query, params.values().toArray(), new JdbcSurveyDao.SurveyRowMapper());
        return surveys;
    }

    private Survey singleSurvey(List<Survey> surveys) {
        if (surveys.size() <= 1) {
            Survey survey = surveys.size() == 1 ? surveys.get(0) : null;
            return survey;
        } else {
            throw new DaoException("More than one Survey returned by the database where a single Survey was expected.");
        }
    }

    private class SurveyRowMapper implements RowMapper<Survey> {

        public Survey mapRow(ResultSet resultSet, int i) throws SQLException {
            Survey survey = new Survey
                (
                    resultSet.getInt("id"),
                    resultSet.getString("name"),
                    resultSet.getBoolean("enabled"),
                    resultSet.getString("code")
                );
            survey.setUserId(resultSet.getInt("user_id"));
            survey.setStatus(resultSet.getInt("status"));
            survey.setAccess(resultSet.getInt("access"));
            survey.setFree(resultSet.getBoolean("free"));
            return survey;
        }
    }
}
