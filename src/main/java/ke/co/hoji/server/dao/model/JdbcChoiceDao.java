package ke.co.hoji.server.dao.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.server.dao.DaoException;
import ke.co.hoji.server.exception.DuplicateChoiceException;
import ke.co.hoji.server.exception.MissingChoiceParentException;

/**
 * Handles CRUD operations of the {@link Choice} object.
 * 
 * Created by gitahi on 15/04/15.
 */
@Component
public class JdbcChoiceDao {

    private final int DEFAULT_PAGE_SIZE = 5;

    private final JdbcTemplate template;

    //Holds pointer to procedure for removing a choice from a group
    private SimpleJdbcCall simpleJdbcCall;

    private final String selectChoice = "SELECT cp.id id_p, cp.name name_p, cp.user_id user_id_p, c.id id, c.name name, cgc.description description, cgc.ordinal ordinal, cgc.code code, cgc.scale scale, cgc.loner loner, cgc.choice_parent_id parent_id, c.user_id user_id FROM choice_group_choice cgc JOIN choice c ON cgc.choice_id = c.id LEFT JOIN choice cp ON cgc.choice_parent_id = cp.id ";

    public JdbcChoiceDao(JdbcTemplate template) {
        this.template = template;
    }

    @PostConstruct
    void init() {
        simpleJdbcCall = new SimpleJdbcCall(template).withProcedureName("remove_from_group");
    }

    /**
     * Fetches a choice by id. The fetched choice only contains basic information:
     * name and description.
     * 
     * @param choiceId id of the choice
     * @return a matched choice
     * @see Choice
     * @throws DaoException if more than one choice share an id
     */
    public Choice getById(Integer choiceId) {
        String sql = "SELECT id, name, description, user_id FROM choice WHERE id = ?";
        List<Choice> choices = template.query(sql, new Object[] { choiceId }, new ChoiceRowMapper());
        if (choices.size() <= 1) {
            return choices.size() == 1 ? choices.get(0) : null;

        } else {
            throw new DaoException("More than one Choice returned by the database where a single Choice was expected.");
        }
    }

    /**
     * Fetches a subset of choices under a given group. The pageable argument must
     * be specified since no assumptions are made about the size of the subset that
     * should be returned.
     * 
     * @param groupId  id of the group
     * @param pageable page and size information of the subset to be fetched
     * @return a subset of choices
     * @see Page
     */
    public Page<Choice> getByGroup(int groupId, Pageable pageable) {
        if (pageable == null) {
            throw new NullPointerException("The argument pageable must not be null.");
        }
        int total = getGroupCount(groupId);
        String orderBy = " ORDER BY " + 
            pageable.getSortOr(Sort.by(Direction.ASC, "ordinal"))
                .get()
                .map(order -> {
                    String val = "";
                    if (order.getProperty().equals("name")) {
                        val += "c.name";
                    } else {
                        val += "cgc." + order.getProperty();
                    }
                    val += " " + order.getDirection();
                    return val;
                })
                .collect(Collectors.joining(", "));
        String fetchSql = selectChoice + " WHERE cgc.choice_group_id = ? " + orderBy + " LIMIT ? OFFSET ?";
        List<Choice> choices = template.query(fetchSql,
                new Object[] { groupId, pageable.getPageSize(), pageable.getOffset() }, new ChoiceResultSetExtractor());
        return new PageImpl<>(choices, pageable, total);
    }

    /**
     * Searches for choices within a group with names matching the searchTerm
     * argument. The match is either the whole name or a substring of the name.
     * <p>
     * The pageable argument can be used to control the size of the returned subset.
     * However, if it is null the size is determined by this method and only the
     * first page is returned.
     * 
     * @param groupId    the group to search
     * @param searchTerm the search term
     * @param pageable   page and size information
     * @return a subset of choices matching the search term
     * @see Page
     */
    public Page<Choice> searchInGroupByName(Integer groupId, String searchTerm, Pageable pageable) {
        if (pageable == null) {
            pageable = PageRequest.of(0, DEFAULT_PAGE_SIZE);
        }
        int total = getGroupCount(groupId);
        String query = searchTerm + "%";
        String fetchSql = selectChoice + " WHERE cgc.choice_group_id = ? AND c.name LIKE ? ORDER BY c.name LIMIT ? OFFSET ?";
        List<Choice> choices = template.query(fetchSql,
                new Object[] { groupId, query, pageable.getPageSize(), pageable.getOffset() },
                new ChoiceResultSetExtractor());
        return new PageImpl<>(choices, pageable, total);
    }

    /**
     * Searches for choices owned by a user with names matching the searchTerm
     * argument. The match is either the whole name or a substring of the name.
     * <p>
     * The pageable argument can be used to control the size of the returned subset.
     * However, if it is null the size is determined by this method and only the
     * first page is returned.
     * 
     * @param userId     id of the owner
     * @param searchTerm the search term
     * @param pageable   page and size information
     * @return a subset of choices matching the search term
     * @see Page
     */
    public Page<Choice> searchByName(int userId, String searchTerm, Pageable pageable) {
        if (pageable == null) {
            pageable = PageRequest.of(0, DEFAULT_PAGE_SIZE);
        }
        int total = template.queryForObject("SELECT COUNT(id) FROM choice WHERE user_id = ?", new Object[] { userId },
                int.class);
        String match = searchTerm + "%";
        String sql = "SELECT id, name, description, user_id FROM choice WHERE user_id = ? AND name LIKE ? ORDER BY name LIMIT ? OFFSET ?";
        List<Choice> choices = template.query(sql, new Object[] { userId, match, pageable.getPageSize(), pageable.getOffset() }, new ChoiceRowMapper());
        return new PageImpl<>(choices, pageable, total);
    }

    /**
     * Adds a choice into a group.
     * 
     * @param groupId id of the group
     * @param choice  choice to be created or updated
     */
    public void saveOrUpdate(int groupId, Choice choice) {
        int total = getGroupCount(groupId);
        Double maxOrdinal = template.query(
                "SELECT ordinal FROM choice_group_choice WHERE choice_group_id = ? AND choice_id = ?",
                new Object[] { groupId, choice.getId() }, (resultSet) -> {
                    double pos = new Double(total);
                    while (resultSet.next()) {
                        pos = resultSet.getDouble("ordinal");
                    }
                    return new Double(pos);
                });

        saveOrUpdate(groupId, Collections.singletonList(choice));

        int choiceId = template.queryForObject("SELECT id FROM choice WHERE name = ? AND user_id = ?",
                new Object[] { choice.getName(), choice.getUserId() }, int.class);

        String duplicateOrdinalSql = "SELECT COUNT(choice_id) FROM choice_group_choice WHERE choice_group_id = ? AND ordinal = ?";
        Object[] parameters = new Object[2];
        parameters[0] = groupId;
        parameters[1] = choice.getOrdinal();
        long duplicateCount = template.queryForObject(duplicateOrdinalSql, parameters, long.class);

        if (duplicateCount > 1) {
            String updateOrdinalSql = "UPDATE choice_group_choice SET ordinal = ordinal + 1 WHERE ordinal >= ? AND ordinal <= ? AND choice_id != ? AND choice_group_id = ?";
            parameters = new Object[4];
            parameters[0] = choice.getOrdinal();
            parameters[1] = maxOrdinal;
            parameters[2] = choiceId;
            parameters[3] = groupId;
            template.update(updateOrdinalSql, parameters);
        }
    }

    /**
     * Adds a set of choices into a group.
     * 
     * @param groupId id of the group
     * @param choices a set of choices
     */
    public void saveOrUpdate(int groupId, List<Choice> choices) {
        int userId = template.queryForObject("SELECT user_id FROM choice_group WHERE id = ?", new Object[] { groupId },
                int.class);

        try {
            String insertSql = "INSERT INTO choice (id, name, description, user_id) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE name = ?, description = ?";
            template.batchUpdate(insertSql, choices, choices.size(), (ps, choice) -> {
                if (choice.getId() != null) {
                    ps.setInt(1, choice.getId());
                } else {
                    ps.setNull(1, Types.INTEGER);
                }
                ps.setString(2, choice.getName());
                ps.setString(3, choice.getDescription());
                ps.setInt(4, userId);
                ps.setString(5, choice.getName());
                ps.setString(6, choice.getDescription());
            });
        } catch (DuplicateKeyException ex) {
            throw new DuplicateChoiceException("A choice already exists with that name", ex);
        }

        List<String> choiceNames = choices.stream().map(choice -> {
            List<String> namesIncludingParents = new ArrayList<>();
            namesIncludingParents.add(choice.getName());
            if (choice.getParent() != null) {
                namesIncludingParents.add(choice.getParent().getName());
            }
            return namesIncludingParents;
        }).flatMap(names -> names.stream()).collect(Collectors.toList());
        Map<String, Integer> nameToIdMap = getChoiceNameToIdMap(userId, choiceNames);

        checkParentExistence(choices, nameToIdMap);

        long lastOrdinal = 0;
        if (getGroupCount(groupId) > 0) {
            lastOrdinal = template.queryForObject("SELECT ordinal FROM choice_group_choice WHERE choice_group_id = ? ORDER BY ORDINAL DESC LIMIT 1", new Object[]{groupId}, long.class);
        }
        AtomicLong nextOrdinal = new AtomicLong(lastOrdinal);

        String upsertSql = "INSERT INTO choice_group_choice (choice_group_id, choice_id, ordinal, code, description, scale, loner, choice_parent_id) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)  "
                + "ON DUPLICATE KEY UPDATE ordinal = ?, code = ?, description = ?, scale = ?, loner = ?, choice_parent_id = ?";
        template.batchUpdate(upsertSql, choices, choices.size(), (ps, choice) -> {
            BigDecimal ordinal = choice.getOrdinal() != null ? choice.getOrdinal()
                    : new BigDecimal(nextOrdinal.incrementAndGet());
            String code = choice.getCode() != null ? choice.getCode() : ordinal.toString();
            ps.setInt(1, groupId);
            ps.setInt(2, nameToIdMap.get(choice.getName()));
            ps.setBigDecimal(3, ordinal);
            ps.setString(4, code);
            ps.setString(5, choice.getDescription());
            ps.setInt(6, choice.getScale());
            ps.setBoolean(7, choice.isLoner());
            if (choice.getParent() != null) {
                ps.setInt(8, nameToIdMap.get(choice.getParent().getName()));
            } else {
                ps.setNull(8, Types.INTEGER);
            }
            ps.setBigDecimal(9, ordinal);
            ps.setString(10, code);
            ps.setString(11, choice.getDescription());
            ps.setInt(12, choice.getScale());
            ps.setBoolean(13, choice.isLoner());
            if (choice.getParent() != null) {
                ps.setInt(14, nameToIdMap.get(choice.getParent().getName()));
            } else {
                ps.setNull(14, Types.INTEGER);
            }
        });
    }

    private int getGroupCount(Integer groupId) {
        String totalSql = "SELECT COUNT(choice_id) FROM choice_group_choice cgc WHERE cgc.choice_group_id = ?";
        int total = template.queryForObject(totalSql, new Object[] { groupId }, int.class);
        return total;
    }

    private Map<String, Integer> getChoiceNameToIdMap(int userId, List<String> choiceNames) {
        String placeholders = choiceNames.stream().map(choice -> "?").collect(Collectors.joining(", "));
        String selectSql = "SELECT id, name FROM choice WHERE user_id = ? AND name IN (" + placeholders + ")";
        Object[] parameters = new Object[choiceNames.size() + 1];
        parameters[0] = userId;
        for (int index = 1; index < parameters.length; index++) {
            parameters[index] = choiceNames.get(index - 1);
        }
        return template.query(selectSql, parameters, resultSet -> {
            Map<String, Integer> nameToIdMap = new HashMap<>();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                nameToIdMap.put(name, id);
            }
            return nameToIdMap;
        });
    }

    private void checkParentExistence(List<Choice> choices, Map<String, Integer> nameToIdMap) {
        choices.stream().filter(c -> c.getParent() != null).forEach(c -> {
            String parentName = c.getParent().getName();
            if (nameToIdMap.get(parentName) == null) {
                throw new MissingChoiceParentException(c, parentName,
                        String.format("Error: could not find the parent choice '%s'.", parentName));
            }
        });
    }

    /**
     * Updates a group by purging the specified choice. To avoid fragmented ordinals the remaining ordinals are updated.
     * NOTE: Moved logic for deleting and defragmenting ordinals to stored procedure because of failure
     * while executing the defrag statements.
     * 
     * @param groupId  id of the group
     * @param choiceId id of the choice
     */
    @Transactional
    public void removeFromGroup(int groupId, int choiceId) {
        SqlParameterSource parameterSource = new MapSqlParameterSource()
            .addValue("cg_id", groupId)
            .addValue("c_id", choiceId);
        simpleJdbcCall.execute(parameterSource);
    }

    private class ChoiceRowMapper implements RowMapper<Choice> {

        @Override
        public Choice mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            int id = resultSet.getInt("id");
            String choiceName = resultSet.getString("name");
            String description = resultSet.getString("description");
            int userId = resultSet.getInt("user_id");
            Choice choice = new Choice(id, choiceName, description, 0, false);
            choice.setUserId(userId);
            return choice;
        }

    }

    private class ChoiceResultSetExtractor implements ResultSetExtractor<List<Choice>> {

        @Override
        public List<Choice> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
            List<Choice> choices = new ArrayList<>();
            Map<Integer, Choice> parentMap = new HashMap<>();
            while (resultSet.next()) {
                int parentId = resultSet.getInt("parent_id");
                if (parentId != 0 && !parentMap.containsKey(parentId)) {
                    int choiceId = resultSet.getInt("id_p");
                    String name = resultSet.getString("name_p");
                    int userId = resultSet.getInt("user_id_p");
                    Choice parent = new Choice(choiceId, name, null, null, 0, false);
                    parent.setUserId(userId);
                    parentMap.put(parentId, parent);
                }
                int choiceId = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                BigDecimal ordinal = resultSet.getBigDecimal("ordinal");
                String code = resultSet.getString("code") != null ? resultSet.getString("code") : String.valueOf(ordinal.intValue());
                int scale = resultSet.getInt("scale");
                boolean loner = resultSet.getBoolean("loner");
                int userId = resultSet.getInt("user_id");
                Choice choice = new Choice(choiceId, name, description, code, scale, loner);
                choice.setUserId(userId);
                choice.setOrdinal(ordinal);
                if (parentId != 0) {
                    choice.setParent(parentMap.get(parentId));
                }
                choices.add(choice);
            }
            return choices;
        }
    }
}
