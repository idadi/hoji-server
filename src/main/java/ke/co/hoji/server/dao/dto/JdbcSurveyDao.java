package ke.co.hoji.server.dao.dto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import ke.co.hoji.core.data.dto.Survey;
import ke.co.hoji.server.dao.DaoException;

/**
 * Created by gitahi on 27/08/15.
 */
@Component("dtoJdbcSurveyDao")
public class JdbcSurveyDao {

    private final JdbcTemplate template;

    private final String selectSurvey =
            "SELECT survey.id, survey.code, name, user_id, survey.enabled, test_mode, status, access, free FROM survey ";

    public JdbcSurveyDao(JdbcTemplate template) {
        this.template = template;
    }

    public List<Survey> getSurveysByTenantUserId(int tenantId, boolean enabled) {
        String sql = selectSurvey
            + "WHERE user_id = ? AND enabled = ? "
            + "ORDER BY survey.enabled desc, survey.id desc";
        return template.query(sql, new Object[]{tenantId, enabled}, new SurveyRowMapper());
    }

    public List<Survey> getPublicSurveys(int publicSurveyAccess) {
        Map<String, Object> params = new HashMap<>();
        params.put("access", ke.co.hoji.core.data.model.Survey.Access.PRIVATE);
        String query = selectSurvey
                + "INNER JOIN user ON survey.user_id = user.id "
                + "WHERE access != ? "
                + "ORDER BY survey.enabled desc, survey.id desc";
        return template.query(query, params.values().toArray(), new SurveyRowMapper());
    }

    public Survey getSurveyById(Integer surveyId) {
        String sql = selectSurvey + "WHERE id = ?";
        List<Survey> surveys = template.query(sql, new Object[]{surveyId}, new SurveyRowMapper());
        if (surveys.size() <= 1) {
            return surveys.size() == 1 ? surveys.get(0) : null;
        } else {
            throw new DaoException("More than one Survey returned by the database where a single Survey was expected.");
        }
    }

    private class SurveyRowMapper implements RowMapper<Survey> {

        public Survey mapRow(ResultSet resultSet, int i) throws SQLException {
            Survey survey = new Survey
                    (
                            resultSet.getInt("id"),
                            resultSet.getString("name"),
                            resultSet.getBoolean("enabled"),
                            resultSet.getString("code")
                    );
            survey.setUserId(resultSet.getInt("user_id"));
            survey.setStatus(resultSet.getInt("status"));
            survey.setAccess(resultSet.getInt("access"));
            survey.setFree(resultSet.getBoolean("free"));
            return survey;
        }
    }
}
