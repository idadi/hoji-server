package ke.co.hoji.server.dao.model;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.dao.DaoException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gitahi on 15/04/15.
 */
@Component
public class JdbcFormDao {

    private final JdbcTemplate template;

    private final JdbcSurveyDao surveyDao;

    public JdbcFormDao(JdbcTemplate template, JdbcSurveyDao surveyDao) {
        this.template = template;
        this.surveyDao = surveyDao;
    }

    private final String selectForm = "SELECT "
        + "form.id, "
        + "form.name, "
        + "form.description, "
        + "form.ordinal, "
        + "form.survey_id, "
        + "form.gps_capture, "
        + "form.min_gps_accuracy, "
        + "form.no_of_gps_attempts, "
        + "form.output_strategy, "
        + "form.transposable, "
        + "form.transposition_categories, "
        + "form.transposition_value_label, "
        + "form.enabled, "
        + "form.minor_modified, "
        + "form.major_modified, "
        + "form.minor_version, "
        + "form.major_version, "
        + "survey.name survey_name, "
        + "survey.enabled survey_enabled, "
        + "survey.code survey_code, "
        + "survey.free survey_free, "
        + "survey.status survey_status, "
        + "survey.access survey_access, "
        + "survey.user_id user_id, "
        + "dependent_form.id dependent_id, "
        + "dependent_form.name dependent_name, "
        + "dependent_form.enabled dependent_enabled, "
        + "dependent_form.description dependent_description, "
        + "dependent_form.ordinal dependent_ordinal, "
        + "dependent_form.minor_version dependent_minor_version, "
        + "dependent_form.major_version dependent_major_version "
        + "FROM form INNER JOIN survey ON form.survey_id = survey.id "
        + "LEFT JOIN field_record_form ON form.id = field_record_form.form_id "
        + "LEFT JOIN field ON field_record_form.field_id = field.id "
        + "LEFT JOIN form dependent_form ON field.form_id = dependent_form.id ";

    public List<Form> getFormsBySurvey(Survey survey) {
        String sql = selectForm + "WHERE form.survey_id = ?";
        return template.query(sql, new Object[]{survey.getId()}, new FormResultSetExtractor());
    }

    public List<Form> getFormsBySurvey(Survey survey, boolean enabled) {
        String sql = selectForm + "WHERE form.survey_id = ? AND form.enabled = ?";
        return template.query(sql, new Object[]{survey.getId(), enabled}, new FormResultSetExtractor());
    }

    public Form getFormById(Integer formId) {
        String sql = selectForm + "WHERE form.id = ?";
        List<Form> forms = template.query(sql, new Object[]{formId}, new FormResultSetExtractor());
        if (forms.isEmpty()) {
            return null;
        } else if (forms.size() == 1) {
            return forms.get(0);
        } else {
            throw new DaoException("More than one Form returned by the database where a single Form was expected.");
        }
    }

    public Form saveForm(Form form) {
        if (form.getId() == null) {
            return createForm(form);
        } else {
            return modifyForm(form);
        }
    }

    @Transactional
    public Form createForm(Form form) {
        String sql = "INSERT INTO form (name, ordinal, survey_id, gps_capture, output_strategy, transposable, " +
            "transposition_categories, transposition_value_label, enabled, minor_modified, major_modified, " +
            "minor_version, major_version)"
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Object[] parameters = new Object[13];
        parameters[0] = form.getName();
        parameters[1] = form.getOrdinal();
        parameters[2] = form.getSurvey().getId();
        parameters[3] = form.getGpsCapture();
        parameters[4] = Utils.string(form.getOutputStrategy());
        parameters[5] = form.isTransposable();
        parameters[6] = form.getTranspositionCategories();
        parameters[7] = form.getTranspositionValueLabel();
        parameters[8] = form.isEnabled();
        parameters[9] = form.isMinorModified();
        parameters[10] = form.isMajorModified();
        parameters[11] = form.getMinorVersion();
        parameters[12] = form.getMajorVersion();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
                ArgumentPreparedStatementSetter argumentPreparedStatementSetter
                    = new ArgumentPreparedStatementSetter(parameters);
                String[] parameterNames = {
                    "name", "ordinal", "survey_id", "gps_capture", "output_strategy", "transposable", "transposition_categories",
                    "transposition_value_label", "enabled", "minor_modified", "major_modified", "minor_version", "major_version"
                };
                PreparedStatement ps = connection.prepareStatement(sql, parameterNames);
                argumentPreparedStatementSetter.setValues(ps);
                return ps;
            },
            keyHolder);
        form.setId(keyHolder.getKey().intValue());
        surveyDao.setPublishedStatus(form.getSurvey(), Survey.MODIFIED);
        return form;
    }

    @Transactional
    public Form modifyForm(Form form) {
        List<Form> result = modifyForms(Arrays.asList(form));
        return result.get(0);
    }

    @Transactional
    public List<Form> modifyForms(List<Form> forms) {
        String sql = "UPDATE form SET "
            + "name = ?, ordinal = ?, survey_id = ?, gps_capture = ?, output_strategy = ?, transposable = ?, " +
            "transposition_categories = ?, transposition_value_label = ?, enabled = ? WHERE id = ?";

        int batchSize = forms.size();
        int[][] status = template.batchUpdate(sql, forms, batchSize,
            (ps, form) -> {
                ps.setString(1, form.getName());
                ps.setBigDecimal(2, form.getOrdinal());
                ps.setInt(3, form.getSurvey().getId());
                ps.setInt(4, form.getGpsCapture());
                ps.setString(5, Utils.string(form.getOutputStrategy()));
                ps.setBoolean(6, form.isTransposable());
                ps.setString(7, form.getTranspositionCategories());
                ps.setString(8, form.getTranspositionValueLabel());
                ps.setBoolean(9, form.isEnabled());
                ps.setInt(10, form.getId());
            });

        handleModifiedForms(forms, batchSize, status);

        return forms;
    }

    private void handleModifiedForms(List<Form> forms, int batchSize, int[][] status) {
        for (int batch = 0; batch < status.length; batch++) {
            for (int row = 0; row < status[batch].length; row++) {
                if (status[batch][row] == 1) {
                    int modifiedFormIndex = batchSize * batch + row;
                    markAsMinorModified(forms.get(modifiedFormIndex));
                }
            }
        }
    }

    public void enableForm(Form form, boolean enabled) {
        String sql = "UPDATE form SET enabled = ? WHERE id = ?";
        int affected = template.update(sql, enabled, form.getId());
        if (affected != 0) {
            markAsMinorModified(form);
        }
    }

    @Transactional
    public void deleteForm(Integer id) {
        Form form = getFormById(id);
        if (form != null) {
            String sql = "DELETE FROM form WHERE id = ?";
            int affected = template.update(sql, id);
            if (affected != 0) {
                surveyDao.setPublishedStatus(form.getSurvey(), Survey.MODIFIED);
            }
        }
    }

    public void markAsMinorModified(Form form) {
        Survey survey = form.getSurvey();
        if (survey != null) {
            form.setMinorModified(true);
            String sql = "UPDATE form SET minor_modified = ? WHERE id = ?";
            template.update(sql, form.isMinorModified(), form.getId());
            surveyDao.setPublishedStatus(survey, Survey.MODIFIED);
        }
    }

    public void markAsMajorModified(Form form) {
        Survey survey = form.getSurvey();
        if (survey != null) {
            form.setMajorModified(true);
            form.setMinorModified(true);
            String sql = "UPDATE form SET minor_modified = ?, major_modified = ? WHERE id = ?";
            template.update(sql, form.isMinorModified(), form.isMajorModified(), form.getId());
            surveyDao.setPublishedStatus(survey, Survey.MODIFIED);
        }
    }

    public void incrementMinorVersion(Form form) {
        form.setMinorVersion(form.getMinorVersion() + 1);
        form.setMinorModified(false);
        String sql = "UPDATE form SET minor_modified = ?, minor_version = ? WHERE id = ?";
        template.update(sql, form.isMinorModified(), form.getMinorVersion(), form.getId());
    }

    public void incrementMajorVersion(Form form) {
        form.setMinorVersion(0);
        form.setMajorVersion(form.getMajorVersion() + 1);
        form.setMajorModified(false);
        form.setMinorModified(false);
        String sql = "UPDATE form SET minor_modified = ?, major_modified = ?, minor_version = ?, major_version = ? WHERE id = ?";
        Object[] parameters = new Object[]{
            form.isMinorModified(), form.isMajorModified(), form.getMinorVersion(), form.getMajorVersion(), form.getId()
        };
        template.update(sql, parameters);
    }

    private class FormResultSetExtractor implements ResultSetExtractor<List<Form>> {

        @Override
        public List<Form> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
            Map<Integer, Form> forms = new HashMap<>();
            Map<Integer, Survey> surveys = new HashMap<>();
            while (resultSet.next()) {
                int formId = resultSet.getInt("id");
                if (!forms.containsKey(formId)) {
                    int surveyId = resultSet.getInt("survey_id");
                    if (!surveys.containsKey(surveyId)) {
                        String surveyName = resultSet.getString("survey_name");
                        boolean surveyEnabled = resultSet.getBoolean("survey_enabled");
                        String surveyCode = resultSet.getString("survey_code");
                        boolean surveyFree = resultSet.getBoolean("survey_free");
                        int surveyStatus = resultSet.getInt("survey_status");
                        int surveyAccess = resultSet.getInt("survey_access");
                        int userId = resultSet.getInt("user_id");
                        Survey survey = new Survey(surveyId, surveyName, surveyEnabled, surveyCode);
                        survey.setFree(surveyFree);
                        survey.setStatus(surveyStatus);
                        survey.setAccess(surveyAccess);
                        survey.setUserId(userId);
                        surveys.put(surveyId, survey);
                    }
                    String name = resultSet.getString("name");
                    String description = resultSet.getString("description");
                    BigDecimal ordinal = resultSet.getBigDecimal("ordinal");
                    int gpsCapture = resultSet.getInt("gps_capture");
                    int minGpsAccuracy = resultSet.getInt("min_gps_accuracy");
                    int noOfGpsAttempts = resultSet.getInt("no_of_gps_attempts");
                    List<String> outputStrategy = Utils.tokenizeString(resultSet.getString("output_strategy"),
                        ",", true);
                    boolean transposable = resultSet.getBoolean("transposable");
                    String transpositionCategories = resultSet.getString("transposition_categories");
                    String transpositionValueLabel = resultSet.getString("transposition_value_label");
                    boolean enabled = resultSet.getBoolean("enabled");
                    boolean minorModified = resultSet.getBoolean("minor_modified");
                    boolean majorModified = resultSet.getBoolean("major_modified");
                    int minorVersion = resultSet.getInt("minor_version");
                    int majorVersion = resultSet.getInt("major_version");
                    Form form = new Form(formId, name, enabled, ordinal, minorVersion, majorVersion);
                    form.setSurvey(surveys.get(surveyId));
                    form.setGpsCapture(gpsCapture);
                    form.setOutputStrategy(outputStrategy);
                    form.setTransposable(transposable);
                    form.setTranspositionCategories(transpositionCategories);
                    form.setTranspositionValueLabel(transpositionValueLabel);
                    form.setMinorModified(minorModified);
                    form.setMajorModified(majorModified);
                    forms.put(formId, form);
                }
                int dependentId = resultSet.getInt("dependent_id");
                if (dependentId > 0) {
                    String dependentName = resultSet.getString("dependent_name");
                    boolean dependentEnabled = resultSet.getBoolean("dependent_enabled");
                    BigDecimal dependentOrdinal = resultSet.getBigDecimal("dependent_ordinal");
                    int dependentMinorVersion = resultSet.getInt("dependent_minor_version");
                    int dependentMajorVersion = resultSet.getInt("dependent_major_version");
                    Form dependentForm = new Form(dependentId, dependentName, dependentEnabled,
                        dependentOrdinal, dependentMinorVersion, dependentMajorVersion);
                    forms.get(formId).addDependentForm(dependentForm);
                }
            }
            return new ArrayList<>(forms.values());
        }
    }

    public void updateOrdinal(List<Form> forms) {
        String sql = "UPDATE form SET ordinal = ? WHERE id = ?";

        int batchSize = forms.size();
        int[][] batchStatus = template.batchUpdate(sql, forms, batchSize,
            (ps, form) -> {
                ps.setBigDecimal(1, form.getOrdinal());
                ps.setInt(2, form.getId());
            }
        );
        handleModifiedForms(forms, batchSize, batchStatus);
    }
}
