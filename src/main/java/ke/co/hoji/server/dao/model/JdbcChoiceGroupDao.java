package ke.co.hoji.server.dao.model;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.server.dao.DaoException;
import ke.co.hoji.server.exception.ChoiceGroupIntegrityViolationException;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by gitahi on 15/04/15.
 */
@Component
public class JdbcChoiceGroupDao {

    private final JdbcTemplate template;

    private final JdbcChoiceDao choiceDao;

    private final String selectChoiceGroup = "SELECT "
            + "cg.id, "
            + "cg.name, "
            + "cg.ordering_strategy, "
            + "cg.exclude_last, "
            + "cg.user_id, "
            + "cp.id choice_id_p, "
            + "cp.name choice_name_p, "
            + "cp.user_id choice_user_id_p, "
            + "cgc.loner choice_loner_p, "
            + "c.id choice_id, "
            + "c.name choice_name, "
            + "cgc.description choice_description, "
            + "cgc.ordinal choice_ordinal, "
            + "cgc.code choice_code, "
            + "cgc.scale choice_scale, "
            + "cgc.loner choice_loner, "
            + "cgc.choice_parent_id choice_parent_id, "
            + "c.user_id choice_user_id "
            + "FROM choice_group cg "
            + "LEFT JOIN choice_group_choice cgc ON cg.id = cgc.choice_group_id "
            + "LEFT JOIN choice c ON cgc.choice_id = c.id "
            + "LEFT JOIN choice cp ON cgc.choice_parent_id = cp.id ";

    public JdbcChoiceGroupDao(JdbcTemplate template, JdbcChoiceDao choiceDao) {
        this.template = template;
        this.choiceDao = choiceDao;
    }

    public List<ChoiceGroup> getChoiceGroupsByTenant(Integer tenantId) {
        String sql = selectChoiceGroup + "WHERE cg.user_id = ? ORDER BY name, choice_ordinal";
        List<ChoiceGroup> choiceGroups = template.query(sql, new Object[]{tenantId}, new ChoiceGroupResultSetExtractor());
        return choiceGroups;
    }

    public ChoiceGroup getChoiceGroupById(Integer choiceGroupId) {
        String sql = selectChoiceGroup + "WHERE cg.id = ? ORDER BY name, choice_ordinal";
        List<ChoiceGroup> choiceGroups = template.query(sql, new Object[]{choiceGroupId}, new ChoiceGroupResultSetExtractor());
        if (choiceGroups.size() <= 1) {
            return choiceGroups.size() == 1 ? choiceGroups.get(0) : null;
        } else {
            throw new DaoException("More than one Choice Group returned by the database where a single Choice Group was expected.");
        }
    }

    @Transactional
    public void deleteChoiceGroup(Integer... ids) {
        if (ids.length == 0) {
            throw new IllegalArgumentException("At least one id must be specified.");
        }
        String idPlaceholders = Utils.string(Stream.of(ids).map(id -> "").collect(Collectors.toList()), ", ", false, "?", true);
        String delete = "DELETE FROM choice_group WHERE id IN (" + idPlaceholders + ")";
        try {
            template.update(delete, (Object[])ids);
        } catch (DataIntegrityViolationException e) {
            throw new ChoiceGroupIntegrityViolationException("The choice group is associated with a field and cannot be deleted.");
        }
    }

    @Transactional
    public ChoiceGroup saveChoiceGroup(ChoiceGroup choiceGroup) {
        String insertSql = "INSERT INTO choice_group (name, ordering_strategy, exclude_last, user_id) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE ordering_strategy = ?, exclude_last = ?";
        Object[] parameters = new Object[6];
        parameters[0] = choiceGroup.getName();
        parameters[1] = choiceGroup.getOrderingStrategy();
        parameters[2] = choiceGroup.getExcludeLast();
        parameters[3] = choiceGroup.getUserId();
        parameters[4] = choiceGroup.getOrderingStrategy();
        parameters[5] = choiceGroup.getExcludeLast();
        template.update(insertSql, parameters);
        int groupId = template.queryForObject("SELECT id FROM choice_group WHERE user_id = ? AND name = ?", new Object[]{choiceGroup.getUserId(), choiceGroup.getName()}, int.class);
        if (choiceGroup.getChoices() != null && choiceGroup.getChoices().size() > 0) {
            choiceDao.saveOrUpdate(groupId, choiceGroup.getChoices());
        }
        return getChoiceGroupById(groupId);
    }

    @Transactional
    public List<ChoiceGroup> saveChoiceGroups(Collection<ChoiceGroup> choiceGroups) {
        String insertSql = "INSERT INTO choice_group (name, ordering_strategy, exclude_last, user_id) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE ordering_strategy = ?, exclude_last = ?";
        template.batchUpdate(insertSql, choiceGroups, choiceGroups.size(),
                (ps, choiceGroup) -> {
                    ps.setString(1, choiceGroup.getName());
                    ps.setInt(2, choiceGroup.getOrderingStrategy());
                    ps.setInt(3, choiceGroup.getExcludeLast());
                    ps.setInt(4, choiceGroup.getUserId());
                    ps.setInt(5, choiceGroup.getOrderingStrategy());
                    ps.setInt(6, choiceGroup.getExcludeLast());
                }
        );
        List<String> groupNames = choiceGroups.stream().map(ChoiceGroup::getName).collect(Collectors.toList());
        int userId = choiceGroups.size() > 0 ? choiceGroups.iterator().next().getUserId() : 0;
        Map<String, Integer> nameToIdMap = getGroupNameToIdMap(userId, groupNames);
        choiceGroups.forEach(choiceGroup -> choiceDao.saveOrUpdate(nameToIdMap.get(choiceGroup.getName()),  choiceGroup.getChoices()));
        return getChoiceGroupsByName(userId, groupNames);
    }

    private Map<String, Integer> getGroupNameToIdMap(int userId, List<String> groupNames) {
        String placeholders = groupNames.stream().map(name -> "?").collect(Collectors.joining(", "));
        String selectSql = "SELECT id, name FROM choice_group WHERE user_id = ? AND name IN (" + placeholders + ")";
        Object[] parameters = new Object[groupNames.size() + 1];
        parameters[0] = userId;
        for (int index = 1; index < parameters.length; index++) {
            parameters[index] = groupNames.get(index - 1);
        }
        return template.query(selectSql, parameters, resultSet -> {
            Map<String, Integer> nameToIdMap = new HashMap<>();
            while(resultSet.next()) {
                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                nameToIdMap.put(name, id);
            }
            return nameToIdMap;
        });
    }

    private List<ChoiceGroup> getChoiceGroupsByName(int userId, List<String> groupNames) {
        String placeholders = groupNames.stream().map(name -> "?").collect(Collectors.joining(", "));
        Object[] parameters = new Object[groupNames.size() + 1];
        parameters[0] = userId;
        for (int index = 1; index < parameters.length; index++) {
            parameters[index] = groupNames.get(index - 1);
        }
        String selectSql = selectChoiceGroup + "WHERE cg.user_id = ? AND cg.name IN ("+ placeholders +")";
        List<ChoiceGroup> choiceGroups = template.query(selectSql, parameters, new ChoiceGroupResultSetExtractor());
        return new ArrayList<ChoiceGroup>(choiceGroups);
    }

    private class ChoiceGroupResultSetExtractor implements ResultSetExtractor<List<ChoiceGroup>> {

        @Override
        public List<ChoiceGroup> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
            Map<Integer, ChoiceGroup> choiceGroupMap = new LinkedHashMap<>();
            Map<Integer, Choice> parentMap = new HashMap<>();
            while (resultSet.next()) {
                Integer choiceGroupId = resultSet.getInt("id");
                if (!choiceGroupMap.containsKey(choiceGroupId)) {
                    ChoiceGroup choiceGroup = new ChoiceGroup(choiceGroupId);
                    choiceGroup.setName(resultSet.getString("name"));
                    choiceGroup.setUserId(resultSet.getInt("user_id"));
                    choiceGroup.setExcludeLast(resultSet.getInt("exclude_last"));
                    choiceGroup.setOrderingStrategy(resultSet.getInt("ordering_strategy"));
                    choiceGroup.setChoices(new ArrayList<>());
                    choiceGroupMap.put(choiceGroupId, choiceGroup);
                }
                int parentId = resultSet.getInt("choice_parent_id");
                if (parentId != 0 && !parentMap.containsKey(parentId)) {
                    int choiceId = resultSet.getInt("choice_id_p");
                    String name = resultSet.getString("choice_name_p");
                    int userId = resultSet.getInt("choice_user_id_p");
                    Choice parent = new Choice(choiceId, name, null, 0, false);
                    parent.setUserId(userId);
                    parentMap.put(parentId, parent);
                }
                int choiceId = resultSet.getInt("choice_id");
                if (choiceId > 0) {
                    String name = resultSet.getString("choice_name");
                    String description = resultSet.getString("choice_description");
                    String code = resultSet.getString("choice_code");
                    if (code == null) {
                        code = resultSet.getString("choice_ordinal");
                        if (code != null) {
                            code = code.replace(".00", "");
                        }
                    }
                    BigDecimal ordinal = resultSet.getBigDecimal("choice_ordinal");
                    int scale = resultSet.getInt("choice_scale");
                    boolean loner = resultSet.getBoolean("choice_loner");
                    int userId = resultSet.getInt("choice_user_id");
                    Choice choice = new Choice(choiceId, name, description, code, scale, loner);
                    choice.setUserId(userId);
                    choice.setOrdinal(ordinal);
                    if (parentId != 0) {
                        choice.setParent(parentMap.get(parentId));
                    }
                    choiceGroupMap.get(choiceGroupId).getChoices().add(choice);
                }
            }
            return new ArrayList<>(choiceGroupMap.values());
        }
    }
}
