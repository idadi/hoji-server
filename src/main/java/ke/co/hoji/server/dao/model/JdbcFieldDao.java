package ke.co.hoji.server.dao.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ke.co.hoji.core.data.ConfigObjectCache;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.OperatorDescriptor;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.server.dao.DaoException;

/**
 * Created by gitahi on 15/04/15.
 */

@Component
public class JdbcFieldDao {

    private final JdbcTemplate template;

    private final JdbcFormDao formDao;

    private final JdbcChoiceGroupDao choiceGroupDao;

    private final ConfigObjectCache<FieldType> fieldTypeCache = new ConfigObjectCache<>();
    private final ConfigObjectCache<OperatorDescriptor> operatorDescriptorCache = new ConfigObjectCache<>();

    private final String selectRule = "SELECT "
            + "id, type, ordinal, owner_id, operator_descriptor_id, value, target_id, combiner, grouper "
            + "FROM rule ";

    @Autowired
    public JdbcFieldDao(JdbcTemplate template, JdbcFormDao formDao, JdbcChoiceGroupDao choiceGroupDao) {
        this.template = template;
        this.formDao = formDao;
        this.choiceGroupDao = choiceGroupDao;
    }

    private String selectField() {
        return "SELECT " + "id, " + "calculated, " + "captioning, " + "choice_filter_field_id, " + "choice_group_id, "
                + "variable_name, " + "default_value, " + "description, " + "instructions, " + "enable_if_null, "
                + "enabled, " + "field_type_id, " + "filterable, " + "form_id, " + "main_record_field_id, "
                + "matrix_record_field_id, " + "max_action, " + "max_value, " + "min_action, " + "min_value, "
                + "missing_action, " + "missing_value, " + "name, " + "ordinal, " + "output_type, " + "parent_id, "
                + "reference_field_id, " + "regex_action, " + "regex_value, "
                + "response_inheriting, " + "searchable, " + "tag, " + "unique_action, " + "uniqueness, "
                + "validation_script, " + "value_script " + "FROM field ";
    }

    private String selectFieldType() {
        return "SELECT "
                + "id, code, name, ordinal, data_type, bracket, widget_class, matrix_widget_class, response_class "
                + "FROM field_type ";
    }

    public List<Field> getFields(Integer formId) {
        String sql = selectField() + "WHERE form_id = ? AND parent_id IS NULL ORDER BY ordinal";
        List<Field> fields = template.query(sql, new Object[] { formId }, new FieldMapper());
        return fields;
    }

    public List<Field> getChildren(Integer fieldId) {
        String sql = selectField() + "WHERE parent_id = ? ORDER BY ordinal";
        List<Field> fields = template.query(sql, new Object[] { fieldId }, new FieldMapper());
        return fields;
    }

    public Field getFieldById(Integer fieldId) {
        if (fieldId.equals(0)) {
            return null;
        }
        String sql = selectField() + "WHERE id = ? ORDER BY ordinal";
        List<Field> fields = template.query(sql, new Object[] { fieldId }, new FieldMapper());
        if (fields.size() <= 1) {
            return fields.size() == 1 ? fields.get(0) : null;
        } else {
            throw new DaoException("More than one Field returned by the database where a single Field was expected.");
        }
    }

    @Transactional
    public Field createField(Field field) {
        String sql = "INSERT INTO field" + "(name, variable_name, description, ordinal, instructions, field_type_id, "
                + "form_id, enabled, captioning, searchable, filterable, tag, calculated, "
                + "value_script, default_value, missing_action, missing_value, "
                + "min_value, max_value, regex_value, uniqueness, choice_group_id, "
                + "choice_filter_field_id, parent_id, reference_field_id, "
                + "main_record_field_id, matrix_record_field_id, response_inheriting) "
                + "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Object[] parameters = new Object[28];
        parameters[0] = field.getName();
        parameters[1] = field.getColumn();
        parameters[2] = field.getDescription();
        parameters[3] = field.getOrdinal();
        parameters[4] = field.getInstructions();
        parameters[5] = field.getType().getId();
        parameters[6] = field.getForm().getId();
        parameters[7] = field.isEnabled();
        parameters[8] = field.isCaptioning();
        parameters[9] = field.isSearchable();
        parameters[10] = field.isFilterable();
        parameters[11] = field.getTag();
        parameters[12] = field.getCalculated();
        parameters[13] = field.getValueScript();
        parameters[14] = field.getDefaultValue();
        parameters[15] = field.getMissingAction();
        parameters[16] = field.getMissingValue();
        parameters[17] = field.getMinValue();
        parameters[18] = field.getMaxValue();
        parameters[19] = field.getRegexValue();
        parameters[20] = field.getUniqueness();
        parameters[21] = field.getChoiceGroup() != null ? field.getChoiceGroup().getId() : null;
        parameters[22] = field.getChoiceFilterField() != null ? field.getChoiceFilterField().getId() : null;
        parameters[23] = field.getParent() != null ? field.getParent().getId() : null;
        parameters[24] = field.getReferenceField() != null ? field.getReferenceField().getId() : null;
        parameters[25] = field.getMainRecordField() != null ? field.getMainRecordField().getId() : null;
        parameters[26] = field.getMatrixRecordField() != null ? field.getMatrixRecordField().getId() : null;
        parameters[27] = field.isResponseInheriting();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
            ArgumentPreparedStatementSetter argumentPreparedStatementSetter = new ArgumentPreparedStatementSetter(
                    parameters);
            String[] parameterNames = { "name", "variable_name", "description", "ordinal", "instructions",
                    "field_type_id", "form_id", "enabled", "captioning", "searchable", "filterable", "tag",
                    "calculated", "value_script", "enable_if_null", "default_value", "missing_action",
                    "missing_value", "min_value", "max_value", "regex_value", "uniqueness", "choice_group_id",
                    "choice_filter_field_id", "parent_id", "reference_field_id",
                    "main_record_field_id", "matrix_record_field_id", "response_inheriting" };
            PreparedStatement ps = connection.prepareStatement(sql, parameterNames);
            argumentPreparedStatementSetter.setValues(ps);
            return ps;
        }, keyHolder);
        field.setId(keyHolder.getKey().intValue());
        if (field.getRecordForms() != null && field.getRecordForms().size() > 0) {
            String linkedFormsSql = "INSERT INTO field_record_form (field_id, form_id) VALUES (?, ?)";
            template.batchUpdate(linkedFormsSql, field.getRecordForms(), field.getRecordForms().size(),
                    new ParameterizedPreparedStatementSetter<Form>() {

                        @Override
                        public void setValues(PreparedStatement ps, Form form) throws SQLException {
                            ps.setInt(1, field.getId());
                            ps.setInt(2, form.getId());
                        }
                    });
        }
        formDao.markAsMajorModified(field.getForm());
        return field;
    }

    @Transactional
    public Field modifyField(Field field) {
        return modifyFields(Arrays.asList(field)).get(0);
    }

    @Transactional
    public List<Field> modifyFields(List<Field> fields) {
        String sql = "UPDATE field SET "
                + "name = ?, description = ?, tag = ?, calculated = ?, value_script = ?, "
                + "variable_name = ?, instructions = ?, default_value = ?, "
                + "missing_value = ?, missing_action = ?, min_value = ?, max_value = ?, regex_value = ?, uniqueness = ?, "
                + "response_inheriting = ?, captioning = ?, searchable = ?, filterable = ?, "
                + "ordinal = ?, choice_group_id = ?, parent_id = ?, "
                + "choice_filter_field_id = ?, reference_field_id = ?, main_record_field_id = ?, "
                + "matrix_record_field_id = ?, form_id = ?, field_type_id = ?, enabled = ? " + "WHERE id = ?";

        int batchSize = fields.size();
        int[][] status = template.batchUpdate(sql, fields, batchSize, (ps, field) -> {
            ps.setString(1, field.getName());
            ps.setString(2, field.getDescription());
            ps.setString(3, field.getTag());
            ps.setInt(4, field.getCalculated());
            ps.setString(5, field.getValueScript());
            ps.setString(6, field.getColumn());
            ps.setString(7, field.getInstructions());
            ps.setString(8, field.getDefaultValue());
            ps.setString(9, field.getMissingValue());
            ps.setInt(10, field.getMissingAction());
            ps.setString(11, field.getMinValue());
            ps.setString(12, field.getMaxValue());
            ps.setString(13, field.getRegexValue());
            ps.setInt(14, field.getUniqueness());
            ps.setObject(15, field.isResponseInheriting(), Types.BOOLEAN);
            ps.setBoolean(16, field.isCaptioning());
            ps.setBoolean(17, field.isSearchable());
            ps.setBoolean(18, field.isFilterable());
            ps.setBigDecimal(19, field.getOrdinal());
            if (field.getChoiceGroup() != null) {
                ps.setInt(20, field.getChoiceGroup().getId());
            } else {
                ps.setNull(20, Types.INTEGER);
            }
            if (field.getParent() != null) {
                ps.setInt(21, field.getParent().getId());
            } else {
                ps.setNull(21, Types.INTEGER);
            }
            if (field.getChoiceFilterField() != null) {
                ps.setInt(22, field.getChoiceFilterField().getId());
            } else {
                ps.setNull(22, Types.INTEGER);
            }
            if (field.getReferenceField() != null) {
                ps.setInt(23, field.getReferenceField().getId());
            } else {
                ps.setNull(23, Types.INTEGER);
            }
            if (field.getMainRecordField() != null) {
                ps.setInt(24, field.getMainRecordField().getId());
            } else {
                ps.setNull(24, Types.INTEGER);
            }
            if (field.getMatrixRecordField() != null) {
                ps.setInt(25, field.getMatrixRecordField().getId());
            } else {
                ps.setNull(25, Types.INTEGER);
            }
            ps.setInt(26, field.getForm().getId());
            ps.setInt(27, field.getType().getId());
            ps.setBoolean(28, field.isEnabled());
            ps.setInt(29, field.getId());
        });

        String deleteScript = "DELETE FROM field_record_form WHERE field_id = ?";
        template.batchUpdate(deleteScript, fields, batchSize, (ps, field) -> ps.setInt(1, field.getId()));
        for (final Field field : fields) {
            if (field.getRecordForms() != null) {
                String recordFormScript = "INSERT INTO field_record_form (field_id, form_id) VALUES (?, ?)";
                template.batchUpdate(recordFormScript, field.getRecordForms(), field.getRecordForms().size(),
                        (ps, form) -> {
                            ps.setInt(1, field.getId());
                            ps.setInt(2, form.getId());
                        });
            }
        }

        if (anyFieldHasBeenModified(status)) {
            formDao.markAsMinorModified(fields.get(0).getForm());
        }
        return fields;
    }

    @Transactional
    public void setQuestionNumber(Field field, String questionNo) {
        String sql = "UPDATE field SET name = ? WHERE id = ?";
        Object[] parameters = new Object[2];
        parameters[0] = questionNo;
        parameters[1] = field.getId();
        int affected = template.update(sql, parameters);
        if (affected != 0) {
            field.setName(questionNo);
            formDao.markAsMinorModified(field.getForm());
        }
    }

    @Transactional
    public void deleteField(Integer id) {
        Field field = getFieldById(id);
        if (field != null) {
            String sql = "DELETE FROM field WHERE id = ?";
            int affected = template.update(sql, id);
            if (affected != 0) {
                formDao.markAsMajorModified(field.getForm());
            }
        }
    }

    public void enableField(Field field, boolean enabled) {
        String sql = "UPDATE field SET enabled = ? WHERE id = ?";
        Object[] parameters = new Object[2];
        parameters[0] = enabled;
        parameters[1] = field.getId();
        int affected = template.update(sql, parameters);
        if (affected != 0) {
            field.setEnabled(enabled);
            formDao.markAsMajorModified(field.getForm());
        }
    }

    @Transactional
    public Rule createRule(Rule rule) {
        String sql = "INSERT INTO rule "
                + "(ordinal, owner_id, operator_descriptor_id, value, target_id, type, combiner, grouper)"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        Object[] parameters = new Object[8];
        parameters[0] = rule.getOrdinal() != null ? rule.getOrdinal() : BigDecimal.ONE;
        parameters[1] = rule.getOwner().getId();
        parameters[2] = rule.getOperatorDescriptor().getId();
        parameters[3] = rule.getValue();
        parameters[4] = rule.getTarget().getId();
        parameters[5] = rule.getType();
        parameters[6] = rule.getCombiner() != null ? rule.getCombiner() : 0;
        parameters[7] = rule.getGrouper();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
            ArgumentPreparedStatementSetter argumentPreparedStatementSetter = new ArgumentPreparedStatementSetter(
                    parameters);
            String[] parameterNames = { "ordinal", "owner_id", "operator_descriptor_id", "value", "target_id", "type",
                    "combiner", "grouper" };
            PreparedStatement ps = connection.prepareStatement(sql, parameterNames);
            argumentPreparedStatementSetter.setValues(ps);
            return ps;
        }, keyHolder);
        rule.setId(keyHolder.getKey().intValue());
        Field field = getFieldById(rule.getOwner().getId());
        formDao.markAsMinorModified(field.getForm());
        return rule;
    }

    @Transactional
    public Rule modifyRule(Rule rule) {
        String sql = "UPDATE rule SET "
                + "ordinal = ?, owner_id = ?, operator_descriptor_id = ?, value = ?, target_id = ?, type = ?, "
                + "combiner = ?, grouper = ? WHERE id = ?";
        Object[] parameters = new Object[9];
        parameters[0] = rule.getOrdinal() != null ? rule.getOrdinal() : BigDecimal.ONE;
        parameters[1] = rule.getOwner().getId();
        parameters[2] = rule.getOperatorDescriptor().getId();
        parameters[3] = rule.getValue();
        parameters[4] = rule.getTarget().getId();
        parameters[5] = rule.getType();
        parameters[6] = rule.getCombiner() != null ? rule.getCombiner() : 0;
        parameters[7] = rule.getGrouper();
        parameters[8] = rule.getId();

        int affected = template.update(sql, parameters);
        if (affected != 0) {
            Field field = getFieldById(rule.getOwner().getId());
            formDao.markAsMinorModified(field.getForm());
        }
        return rule;
    }

    public void deleteRule(Rule rule) {
        String sql = "DELETE FROM rule WHERE id = ?";
        int affected = template.update(sql, rule.getId());
        if (affected != 0) {
            Field field = getFieldById(rule.getOwner().getId());
            formDao.markAsMinorModified(field.getForm());
        }
    }

    public List<Rule> getRules(Field owner, Integer type, ConfigObjectCache<Field> configCache) throws DaoException {
        String select = selectRule + "WHERE owner_id = ? AND type = ? ORDER BY ordinal";
        return template.query(select, new Object[] { owner.getId(), type }, new RuleMapper(owner, configCache));
    }

    public List<Rule> getRules(Field owner, Integer type) throws DaoException {
        return getRules(owner, type, null);
    }

    public OperatorDescriptor getOperatorDescriptorById(Integer id) {
        OperatorDescriptor cached = operatorDescriptorCache.get(id);
        if (cached != null) {
            return cached;
        }
        String select = "SELECT id, name, class FROM operator_descriptor WHERE id = ?";
        OperatorDescriptor operatorDescriptor = template.queryForObject(select, new Object[] { id },
                new OperatorDescriptorMapper());
        operatorDescriptorCache.put(operatorDescriptor);
        return operatorDescriptor;
    }

    public FieldType getFieldTypeById(Integer id) {
        FieldType cached = fieldTypeCache.get(id);
        if (cached != null) {
            return cached;
        }
        String select = selectFieldType() + "WHERE id = ?";
        FieldType fieldType = template.queryForObject(select, new Object[] { id }, new FieldTypeMapper());
        fieldTypeCache.put(fieldType);
        return fieldType;
    }

    private List<Form> getRecordForms(Field field) {
        List<Form> forms = new ArrayList<>();
        String select = "SELECT form_id FROM field_record_form WHERE field_id = ?";
        List<Integer> formIds = template.query(select, new Object[] { field.getId() }, new IntegerMapper("form_id"));
        for (Integer formId : formIds) {
            Form form = formDao.getFormById(formId);
            forms.add(form);
        }
        return forms;
    }

    private class FieldMapper implements RowMapper<Field> {

        public Field mapRow(ResultSet resultSet, int i) throws SQLException {
            Field field = new Field(resultSet.getInt("id"), resultSet.getString("name"),
                    resultSet.getBoolean("enabled"), resultSet.getString("description"),
                    resultSet.getBigDecimal("ordinal"), resultSet.getBoolean("captioning"),
                    resultSet.getBoolean("searchable"), resultSet.getBoolean("filterable"),
                    resultSet.getInt("output_type"), resultSet.getInt("missing_action"));
            field.setMissingValue(resultSet.getString("missing_value"));
            field.setColumn(resultSet.getString("variable_name"));
            field.setInstructions(resultSet.getString("instructions"));
            field.setTag(resultSet.getString("tag"));
            field.setCalculated(resultSet.getInt("calculated"));
            field.setValueScript(resultSet.getString("value_script"));
            field.setDefaultValue(resultSet.getString("default_value"));
            field.setMinValue(resultSet.getString("min_value"));
            field.setMaxValue(resultSet.getString("max_value"));
            field.setRegexValue(resultSet.getString("regex_value"));
            field.setUniqueness(resultSet.getInt("uniqueness"));
            field.setType(getFieldTypeById(resultSet.getInt("field_type_id")));
            field.setParent(getFieldById(resultSet.getInt("parent_id")));
            field.setChoiceFilterField(getFieldById(resultSet.getInt("choice_filter_field_id")));
            field.setReferenceField(getFieldById(resultSet.getInt("reference_field_id")));
            field.setMainRecordField(getFieldById(resultSet.getInt("main_record_field_id")));
            field.setMatrixRecordField(getFieldById(resultSet.getInt("matrix_record_field_id")));
            if (field.getType().isChoice()) {
                field.setChoiceGroup(choiceGroupDao.getChoiceGroupById(resultSet.getInt("choice_group_id")));
            }
            if (field.getType().isRecord()) {
                field.setRecordForms(getRecordForms(field));
            }
            field.setResponseInheriting(resultSet.getBoolean("response_inheriting"));
            field.setForm(formDao.getFormById(resultSet.getInt("form_id")));
            return field;
        }
    }

    public List<FieldType> getAllFieldTypes() {
        String select = selectFieldType() + "ORDER BY ordinal";
        List<FieldType> fieldTypes = template.query(select, new FieldTypeMapper());
        fieldTypeCache.putAll(fieldTypes);
        return fieldTypes;
    }

    public List<OperatorDescriptor> getAllOperatorDescriptors() {
        String select = "SELECT id, name, class FROM operator_descriptor WHERE class != '"
                + OperatorDescriptor.BITWISE_AND + "'";
        List<OperatorDescriptor> operatorDescriptors = template.query(select, new OperatorDescriptorMapper());
        operatorDescriptorCache.putAll(operatorDescriptors);
        return operatorDescriptors;
    }

    public Rule getRuleById(Field owner, Integer ruleId) {
        String select = selectRule + "WHERE id = ? ORDER BY grouper, ordinal";
        List<Rule> rules = template.query(select, new Object[] { ruleId }, new RuleMapper(owner, null));
        if (rules != null && !rules.isEmpty()) {
            return rules.get(0);
        }
        return null;
    }

    public void updateOrdinal(List<Field> fields) {
        String sql = "UPDATE field SET ordinal = ? WHERE id = ?";

        int[][] status = template.batchUpdate(sql, fields, fields.size(), (ps, field) -> {
            ps.setBigDecimal(1, field.getOrdinal());
            ps.setInt(2, field.getId());
        });

        if (anyFieldHasBeenModified(status)) {
            formDao.markAsMinorModified(fields.get(0).getForm());
        }
    }

    private boolean anyFieldHasBeenModified(int[][] status) {
        boolean anyFieldHasBeenModified = false;
        for (int[] s : status) {
            for (int value : s) {
                if (value == 1) {
                    anyFieldHasBeenModified = true;
                    break;
                }
            }
            if (anyFieldHasBeenModified) {
                break;
            }
        }
        return anyFieldHasBeenModified;
    }

    public void updateRuleOrdinal(List<Rule> rules) {
        String sql = "UPDATE rule SET ordinal = ? WHERE id = ?";

        template.batchUpdate(sql, rules, rules.size(), (ps, rule) -> {
            ps.setBigDecimal(1, rule.getOrdinal());
            ps.setInt(2, rule.getId());
        });

        Field owner = rules.get(0).getOwner();
        formDao.markAsMinorModified(owner.getForm());
    }

    private class RuleMapper implements RowMapper<Rule> {

        private final Field owner;
        private final ConfigObjectCache<Field> configCache;

        public RuleMapper(Field owner, ConfigObjectCache<Field> configCache) {
            this.owner = owner;
            this.configCache = configCache;
        }

        public Rule mapRow(ResultSet resultSet, int i) throws SQLException {
            Rule rule = new Rule(resultSet.getInt("id"), resultSet.getBigDecimal("ordinal"),
                    resultSet.getString("value"), resultSet.getInt("type"), resultSet.getInt("grouper"));
            rule.setOwner(owner);
            if (configCache != null) {
                rule.setTarget(configCache.get(resultSet.getInt("target_id")));
            } else {
                rule.setTarget(JdbcFieldDao.this.getFieldById(resultSet.getInt("target_id")));
            }
            rule.setOperatorDescriptor(getOperatorDescriptorById(resultSet.getInt("operator_descriptor_id")));
            rule.setCombiner(resultSet.getInt("combiner"));
            return rule;
        }
    }

    private class FieldTypeMapper implements RowMapper<FieldType> {

        public FieldType mapRow(ResultSet resultSet, int i) throws SQLException {
            FieldType fieldType = new FieldType(resultSet.getInt("id"), resultSet.getString("name"),
                    resultSet.getString("code"));
            fieldType.setOrdinal(resultSet.getBigDecimal("ordinal"));
            fieldType.setDataType(resultSet.getInt("data_type"));
            fieldType.setGroup(resultSet.getInt("bracket"));
            fieldType.setWidgetClass((resultSet.getString("widget_class")));
            fieldType.setMatrixWidgetClass((resultSet.getString("matrix_widget_class")));
            fieldType.setResponseClass((resultSet.getString("response_class")));
            return fieldType;
        }
    }

    private class OperatorDescriptorMapper implements RowMapper<OperatorDescriptor> {

        public OperatorDescriptor mapRow(ResultSet resultSet, int i) throws SQLException {
            OperatorDescriptor operatorDescriptor = new OperatorDescriptor(resultSet.getInt("id"),
                    resultSet.getString("name"), resultSet.getString("class"));
            return operatorDescriptor;
        }
    }
}
