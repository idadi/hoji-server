package ke.co.hoji.server.dao.impl;

import ke.co.hoji.core.Utils;
import ke.co.hoji.server.dao.DaoException;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.VerificationToken;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by gitahi on 02/10/15.
 */
public class JdbcUserDao {

    private final JdbcTemplate template;

    private final String selectUser = "SELECT "
        + "id, "
        + "username, "
        + "code, "
        + "public_survey_access, "
        + "tenant_user_id, "
        + "password, "
        + "first_name, "
        + "middle_name, "
        + "last_name, enabled, "
        + "verified, "
        + "suspended, "
        + "telephone, "
        + "postal_address, "
        + "physical_address, "
        + "date_created, "
        + "default_roles, type, "
        + "min_credits, tax_rate, "
        + "discount_rate, master, "
        + "slave_status, "
        + "master_user_id "
        + "FROM user ";

    public JdbcUserDao(JdbcTemplate template) {
        this.template = template;
    }

    public User findById(Integer id) {
        String sql = selectUser + "WHERE id = ? ORDER BY first_name";
        List<User> users = template.query(sql, new Object[]{id}, new UserRowMapper());
        for (User user : users) {
            setRoles(user);
        }
        return singleUser(users);
    }

    public User findByUsername(String username) {
        String sql = selectUser + "WHERE username = ? ORDER BY first_name";
        List<User> users = template.query(sql, new Object[]{username}, new UserRowMapper());
        for (User user : users) {
            setRoles(user);
        }
        return singleUser(users);
    }

    public List<User> findByTenantId(Integer tenantUserId) {
        String sql = selectUser + "WHERE tenant_user_id = ? ORDER BY first_name";
        List<User> users = template.query(sql, new Object[]{tenantUserId}, new UserRowMapper());
        for (User user : users) {
            setRoles(user);
        }
        return users;
    }

    public List<User> findAllUsers(Boolean enabled) {
        List<Object> parameters = new ArrayList<>();
        if (enabled != null) {
            parameters.add(enabled);
        }
        String sql = selectUser + (enabled != null ? "WHERE enabled = ? " : " ") + "ORDER BY first_name";
        List<User> users = template.query(sql, parameters.toArray(), new UserRowMapper());
        for (User user : users) {
            setRoles(user);
        }
        return users;

    }

    public List<User> findByIds(Collection<Integer> ids) {
        if (ids.isEmpty()) {
            return new ArrayList<>();
        }
        String idPlaceholders = Utils.string(ids.stream().map(id -> "").collect(Collectors.toList()), ", ", false, "?", true);
        String sql = selectUser + " WHERE id IN (" + idPlaceholders + ")";
        return template.query(sql, ids.toArray(), new UserRowMapper());
    }

    public void createUser(User user) throws DuplicateKeyException {
        user = insertUser(user);
        insertRoles(user);
    }

    @Transactional
    public void modifyUser(User user) {
        updateUser(user);
        insertRoles(user);
    }

    public void changeType(User user) {
        String sql = "UPDATE user SET type = ? WHERE id = ?";
        template.update(sql, user.getType(), user.getId());
    }

    public void verifyUser(User user) {
        String sql = "UPDATE user SET verified = ?, suspended = ? WHERE id = ?";
        template.update(sql, user.isVerified(), false, user.getId());
    }

    public void suspendUser(User user) {
        String sql = "UPDATE user SET verified = ?, suspended = ? WHERE id = ?";
        template.update(sql, false, user.isSuspended(), user.getId());
    }

    public void changeEmail(User user) {
        String sql = "UPDATE user SET username = ?, verified = ? WHERE id = ?";
        template.update(sql, user.getEmail(), false, user.getId());
    }

    public void changePassword(User user) {
        String sql = "UPDATE user SET password = ? WHERE id = ?";
        template.update(sql, user.getPassword(), user.getId());
    }

    public void createVerificationToken(VerificationToken verificationToken) {
        User u = findByUsername(verificationToken.getUser().getUsername());
        verificationToken.getUser().setId(u.getId());
        String sql = "INSERT INTO verification_token (token, type, user_id, expiry_date) VALUES(?, ?, ?, ?)";
        Object[] parameters = new Object[]{
            verificationToken.getToken(),
            verificationToken.getType(),
            verificationToken.getUser().getId(),
            verificationToken.getExpiryDate()
        };
        template.update(sql, parameters);
    }

    public void deleteVerificationTokens(int type, User user) {
        String sql = "DELETE FROM verification_token WHERE type = ? AND user_id = ?";
        template.update(sql, new Object[]{type, user.getId()});
    }

    public VerificationToken getVerificationToken(String token, int type) {
        VerificationToken verificationToken = null;
        String sql = "SELECT id, token, type, user_id, expiry_date FROM verification_token WHERE token = ? AND type =" +
            " ?";
        List<VerificationToken> tokens = template.query(sql, new Object[]{token, type}, new VerificationTokenRowMapper());
        if (tokens != null && !tokens.isEmpty()) {
            verificationToken = tokens.get(0);
        }
        return verificationToken;
    }

    public VerificationToken getLatestVerificationToken(User user) {
        VerificationToken verificationToken = null;
        String sql = "SELECT id, token, type ,user_id, expiry_date FROM verification_token WHERE user_id = ? ORDER BY" +
            " expiry_date DESC LIMIT 1";
        List<VerificationToken> tokens = template.query(sql, new Object[]{user.getId()}, new VerificationTokenRowMapper());
        if (tokens != null && !tokens.isEmpty()) {
            verificationToken = tokens.get(0);
        }
        return verificationToken;
    }

    public void expelUser(User user) {
        user.setRoles(User.initialRoles());
        insertRoles(user);
        String sql = "UPDATE user SET tenant_user_id = id WHERE id = ?";
        template.update(sql, user.getId());
    }

    public User findByCode(String code) {
        String sql = selectUser + "WHERE code = ? ORDER BY first_name";
        List<User> users = template.query(sql, new Object[]{code}, new UserRowMapper());
        for (User user : users) {
            setRoles(user);
        }
        return singleUser(users);
    }

    private User singleUser(List<User> users) {
        if (users.size() <= 1) {
            return users.size() == 1 ? users.get(0) : null;
        } else {
            throw new DaoException(
                "More than one User returned by the database where a single User was expected."
            );
        }
    }

    private User insertUser(User user) {
        String sql = "INSERT INTO user ("
            + "username, code, tenant_user_id, password, first_name, middle_name, last_name, "
            + "enabled, verified, telephone, default_roles, type, public_survey_access, min_credits, "
            + "tax_rate, discount_rate, master, slave_status, master_user_id, date_created "
            + ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Object[] parameters = new Object[]{
            user.getEmail(),
            user.getCode(),
            user.getTenantUserId(),
            user.getPassword(),
            user.getFirstName(),
            user.getMiddleName(),
            user.getLastName(),
            user.isEnabled(),
            user.isVerified(),
            user.getTelephone(),
            user.getDefaultRoles() != null && user.getDefaultRoles().size() > 0 ? StringUtils.collectionToCommaDelimitedString(user.getDefaultRoles()) : null,
            user.getType(),
            user.getPublicSurveyAccess(),
            user.getMinCredits(),
            user.getTaxRate(),
            user.getDiscountRate(),
            user.isMaster(),
            user.getSlaveStatus(),
            user.getMasterUser() != null ? user.getMasterUser().getId() : null,
            user.getDateCreated()
        };
        KeyHolder keyHolder = new GeneratedKeyHolder();
        template.update(connection -> {
                ArgumentPreparedStatementSetter argumentPreparedStatementSetter
                    = new ArgumentPreparedStatementSetter(parameters);
                String[] parameterNames = {
                    "username", "code", "tenant_user_id", "password", "first_name", "middle_name", "last_name",
                    "enabled", "verified", "telephone", "default_roles", "type", "public_survey_access",
                    "min_credits", "tax_rate", "discount_rate", "master", "slave_status", "master_user_id",
                    "date_created"
                };
                PreparedStatement ps = connection.prepareStatement(sql, parameterNames);
                argumentPreparedStatementSetter.setValues(ps);
                return ps;
            },
            keyHolder);
        user.setId(keyHolder.getKey().intValue());
        return user;
    }

    private void updateUser(User user) {
        String sql = "UPDATE user SET first_name = ?, middle_name = ?, last_name = ?, tenant_user_id = ?, "
            + "public_survey_access = ?, telephone = ?, default_roles = ? WHERE id = ?";
        Object[] parameters = new Object[]{
            user.getFirstName(),
            user.getMiddleName(),
            user.getLastName(),
            user.getTenantUserId(),
            user.getPublicSurveyAccess(),
            user.getTelephone(),
            user.getDefaultRoles() != null && !user.getDefaultRoles().isEmpty() ?
                Utils.string(user.getRoleStrings(true)) : null,
            user.getId()
        };
        template.update(sql, parameters);
    }

    private void insertRoles(User user) {
        String delete = "DELETE FROM role WHERE user_id  = ?";
        template.update(delete, user.getId());

        String sql = "INSERT INTO role (user_id, role) VALUES(?, ?)";
        int batchSize = user.getRoles().size();
        template.batchUpdate(sql, user.getRoleStrings(), batchSize, new ParameterizedPreparedStatementSetter<String>() {
            @Override
            public void setValues(PreparedStatement ps, String role) throws SQLException {
                ps.setInt(1, user.getId());
                ps.setString(2, role);
            }
        });
    }

    private void setRoles(User user) {
        String select = "SELECT role FROM role WHERE user_id = ?";
        List<User.Role> roles = template.query(select, new Object[]{user.getId()}, new RoleMapper());
        user.setRoles(roles);
    }

    private class UserRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet resultSet, int i) throws SQLException {
            User user = new User();
            user.setId(resultSet.getInt("id"));
            user.setUsername(resultSet.getString("username"));
            user.setCode(resultSet.getString("code"));
            user.setTenantUserId(resultSet.getInt("tenant_user_id"));
            user.setPassword(resultSet.getString("password"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setMiddleName(resultSet.getString("middle_name"));
            user.setLastName(resultSet.getString("last_name"));
            user.setEnabled(resultSet.getBoolean("enabled"));
            user.setVerified(resultSet.getBoolean("verified"));
            user.setSuspended(resultSet.getBoolean("suspended"));
            user.setTelephone(resultSet.getString("telephone"));
            List<User.Role> defaultRoles = new ArrayList<>();
            String defaultRolesString = resultSet.getString("default_roles");
            if (defaultRolesString != null && !"".equals(defaultRolesString)) {
                List<String> roleStrings = Utils.tokenizeString(defaultRolesString, ",", true);
                for (String roleString : roleStrings) {
                    defaultRoles.add(User.Role.valueOf(roleString));
                }
            }
            user.setDefaultRoles(defaultRoles);
            user.setDateCreated(resultSet.getDate("date_created"));
            user.setType(resultSet.getInt("type"));
            user.setPublicSurveyAccess(resultSet.getInt("public_survey_access"));
            user.setMinCredits(resultSet.getInt("min_credits"));
            user.setTaxRate(resultSet.getBigDecimal("tax_rate"));
            user.setDiscountRate(resultSet.getBigDecimal("discount_rate"));
            user.setMaster(resultSet.getBoolean("master"));
            user.setSlaveStatus(resultSet.getInt("slave_status"));
            Integer masterUserId = resultSet.getInt("master_user_id");
            user.setMasterUser(findById(masterUserId));
            return user;
        }
    }

    private class VerificationTokenRowMapper implements RowMapper<VerificationToken> {

        @Override
        public VerificationToken mapRow(ResultSet resultSet, int i) throws SQLException {
            VerificationToken verificationToken = new VerificationToken
                (
                    resultSet.getString("token"),
                    resultSet.getInt("type"),
                    findById(resultSet.getInt("user_id")),
                    resultSet.getLong("expiry_date")
                );
            verificationToken.setId(resultSet.getInt("id"));
            return verificationToken;
        }
    }

    private class RoleMapper implements RowMapper<User.Role> {

        @Override
        public User.Role mapRow(ResultSet resultSet, int i) throws SQLException {
            return User.Role.valueOf(resultSet.getString("role"));
        }
    }
}
