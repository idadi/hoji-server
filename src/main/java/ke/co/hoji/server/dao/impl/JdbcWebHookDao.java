package ke.co.hoji.server.dao.impl;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.dao.DaoException;
import ke.co.hoji.server.dao.model.JdbcFormDao;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.MissedMessage;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.WebHook;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JdbcWebHookDao {

    private final JdbcTemplate template;

    private final JdbcFormDao formDao;

    private final KeyGenerator keyGenerator;

    public JdbcWebHookDao(JdbcTemplate template, JdbcFormDao formDao) {
        this.template = template;
        this.formDao = formDao;
        this.keyGenerator = new KeyGenerator(template, "web_hook", 1);
    }


    public List<WebHook> findWebHooksFor(Form form) {
        String select = "SELECT id, target_url, user_id, verified, active, secret, form_id, event_type "
                + "FROM web_hook JOIN web_hook_detail ON web_hook.id = web_hook_detail.web_hook_id "
                + "WHERE form_id = ?";

        return template.query(select, new Object[]{form.getId()}, new WebHookResultSetExtractor());
    }

    public List<WebHook> findWebHooksFor(User user) {
        String select = "SELECT id, target_url, user_id, verified, active, secret, form_id, event_type "
                + "FROM web_hook JOIN web_hook_detail ON web_hook.id = web_hook_detail.web_hook_id "
                + "WHERE user_id = ?";

        return template.query(select, new Object[]{user.getId()}, new WebHookResultSetExtractor());
    }

    public WebHook findWebHook(Integer webHookId) {
        String select = "SELECT id, target_url, user_id, verified, active, secret, form_id, event_type "
                + "FROM web_hook JOIN web_hook_detail ON web_hook.id = web_hook_detail.web_hook_id "
                + "WHERE id = ?";

        List<WebHook> webHooks = template.query(select, new Object[]{webHookId}, new WebHookResultSetExtractor());
        if (webHooks.size() == 1) {
            return webHooks.get(0);
        } else {
            throw new DaoException(
                    "More than one WebHook returned by the database where a single WebHook was expected."
            );
        }
    }

    @Transactional
    public WebHook saveWebHook(WebHook webHook) {
        if (webHook.getId() == null) {
            Integer webHookId = keyGenerator.nextKey();
            String insert = "INSERT INTO web_hook (id, target_url, user_id, verified, active, secret) VALUES(?, ?, ?, ?, ?, ?)";
            Object[] parameters = new Object[]{
                    webHookId,
                    webHook.getTargetUrl(),
                    webHook.getUserId(),
                    webHook.isVerified(),
                    webHook.isActive(),
                    webHook.getSecret()
            };
            template.update(insert, parameters);
            webHook = new WebHook(webHookId, webHook.getForms(), webHook.getEventTypes(),
                    webHook.getTargetUrl(), webHook.getUserId(), webHook.isVerified(), webHook.isActive(), webHook.getSecret());
        } else {
            String update = "UPDATE web_hook SET target_url = ?, user_id = ?, verified = ?, active = ?, secret = ? WHERE id = ?";
            Object[] parameters = new Object[]{
                    webHook.getTargetUrl(),
                    webHook.getUserId(),
                    webHook.isVerified(),
                    webHook.isActive(),
                    webHook.getSecret(),
                    webHook.getId(),
            };
            template.update(update, parameters);
        }
        saveDetails(webHook);
        return webHook;
    }

    public void deleteWebHook(Integer webHookId) {
        String delete = "DELETE FROM web_hook WHERE id = ?";
        template.update(delete, webHookId);
    }

    public List<MissedMessage> getMissedMessages() {
        String select = "SELECT id, web_hook_id, form_id, main_record_uuid, event_description FROM missed_message "
                +"WHERE form_id IS NOT NULL AND event_description IS NOT NULL";
        return template.query(select, new RowMapper<MissedMessage>() {
            @Override
            public MissedMessage mapRow(ResultSet rs, int rowNum) throws SQLException {
                Integer id = rs.getInt("id");
                String mainRecordUuid = rs.getString("main_record_uuid");
                String eventDescription = rs.getString("event_description");
                WebHook webHook = findWebHook(rs.getInt("web_hook_id"));
                Form form = formDao.getFormById(rs.getInt("form_id"));
                return new MissedMessage(id, webHook, form, mainRecordUuid, eventDescription);
            }
        });
    }

    public void saveMissedMessage(MissedMessage missedMessage) {
        String insert = "INSERT INTO missed_message (web_hook_id, form_id, main_record_uuid, event_description, date_created) VALUES(?, ?, ?, ?, ?)";
        template.update(insert, missedMessage.getWebHook().getId(), missedMessage.getForm().getId(), missedMessage.getMainRecordUuid(), missedMessage.getEventDescription(), new Date());
    }

	public void deleteMissedMessage(Integer messageId) {
        String delete = "DELETE FROM missed_message WHERE id = ?";
        template.update(delete, messageId);
	}

    private void saveDetails(WebHook webHook) {
        String delete = "DELETE FROM web_hook_detail WHERE web_hook_id = ?";
        template.update(delete, webHook.getId());
        String insert = "INSERT INTO web_hook_detail (web_hook_id, form_id, event_type) VALUES (?, ?, ?)";
        List<WebHookDetail> webHookDetails = WebHookDetail.getDetails(webHook);
        final Integer webHookId = webHook.getId();
        template.batchUpdate(insert, webHookDetails, webHookDetails.size(), (ps, detail) -> {
            ps.setInt(1, webHookId);
            ps.setInt(2, detail.getForm().getId());
            ps.setString(3, detail.getEventType().toString());
        });
    }

    private class WebHookResultSetExtractor implements ResultSetExtractor<List<WebHook>> {

        @Override
        public List<WebHook> extractData(ResultSet rs) throws SQLException, DataAccessException {
            Map<Integer, WebHook> webHookMap = new LinkedHashMap<>();
            Map<Integer, Set<Integer>> processedForms = new LinkedHashMap<>();
            Map<Integer, Set<EventType>> processedEvents = new LinkedHashMap<>();
            while (rs.next()) {
                Integer webHookId = rs.getInt("id");
                Form form = formDao.getFormById(rs.getInt("form_id"));
                EventType eventType = EventType.valueOf(rs.getString("event_type"));
                if (!webHookMap.containsKey(webHookId)) {
                    String targetUrl = rs.getString("target_url");
                    Integer userId = rs.getInt("user_id");
                    boolean verified = rs.getBoolean("verified");
                    boolean active = rs.getBoolean("active");
                    String secret = rs.getString("secret");
                    List<Form> forms = new ArrayList<>();
                    List<EventType> eventTypes = new ArrayList<>();
                    webHookMap.put(webHookId, new WebHook(webHookId, forms, eventTypes, targetUrl, userId, verified, active, secret));
                }
                WebHook current = webHookMap.get(webHookId);
                if (!processedForms.containsKey(webHookId)
                        || !processedForms.get(webHookId).contains(form.getId())) {
                    current.addForm(form);
                    if (!processedForms.containsKey(webHookId)) {
                        processedForms.put(webHookId, new LinkedHashSet<>());
                    }
                    processedForms.get(webHookId).add(form.getId());
                }
                if (!processedEvents.containsKey(webHookId)
                        || !processedEvents.get(webHookId).contains(eventType)) {
                    current.addEventType(eventType);
                    if (!processedEvents.containsKey(webHookId)) {
                        processedEvents.put(webHookId, new LinkedHashSet<>());
                    }
                    processedEvents.get(webHookId).add(eventType);
                }
            }
            return new ArrayList<>(webHookMap.values());
        }
    }

    /**
     * Helper class to flatten forms and events in {@link WebHook} so that it can be easily saved into the db
     */
    private static class WebHookDetail {
        private final Form form;
        private final EventType eventType;

        private WebHookDetail(Form form, EventType eventType) {
            this.form = form;
            this.eventType = eventType;
        }

        public Form getForm() {
            return form;
        }

        public EventType getEventType() {
            return eventType;
        }

        static List<WebHookDetail> getDetails(WebHook webHook) {
            List<WebHookDetail> webHookDetails = new ArrayList<>();
            for (EventType eventType : webHook.getEventTypes()) {
                for (Form form : webHook.getForms()) {
                    webHookDetails.add(new WebHookDetail(form, eventType));
                }
            }
            return webHookDetails;
        }
    }
}
