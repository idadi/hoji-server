package ke.co.hoji.server.dao.dto;

import ke.co.hoji.core.data.dto.Form;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by gitahi on 27/08/15.
 */
@Component("dtoJdbcFormDao")
public class JdbcFormDao {

    private final JdbcTemplate template;

    public JdbcFormDao(JdbcTemplate template) {
        this.template = template;
    }

    public List<Form> getFormsBySurvey(Integer surveyId) {
        String sql = "SELECT id, name, description, ordinal, survey_id, gps_capture, min_gps_accuracy, "
                + "no_of_gps_attempts, output_strategy, transposable, transposition_categories, "
                + "transposition_value_label, enabled, minor_modified, major_modified, minor_version, major_version "
                + "FROM form WHERE survey_id = ? "
                + "ORDER BY ordinal";
        List<Form> forms = template.query(sql, new Object[]{surveyId}, new FormRowMapper());
        return forms;
    }

    private class FormRowMapper implements RowMapper<Form> {

        public Form mapRow(ResultSet resultSet, int i) throws SQLException {
            Form form = new Form();
            form.setId(resultSet.getInt("id"));
            form.setName(resultSet.getString("name"));
            form.setEnabled(resultSet.getBoolean("enabled"));
            form.setOrdinal(resultSet.getBigDecimal("ordinal"));
            form.setSurveyId(resultSet.getInt("survey_id"));
            form.setMinorVersion(resultSet.getInt("minor_version"));
            form.setMajorVersion(resultSet.getInt("major_version"));
            form.setGpsCapture(resultSet.getInt("gps_capture"));
            return form;
        }
    }
}
