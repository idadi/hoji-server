package ke.co.hoji.server.dao.impl;

import ke.co.hoji.server.model.Transaction.Type;
import ke.co.hoji.server.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by gitahi on 04/01/16.
 */
public class JdbcCreditTransactionDao {

    private final JdbcTemplate template;

    private final JdbcUserDao userDao;

    public JdbcCreditTransactionDao(JdbcTemplate template, JdbcUserDao userDao) {
        this.template = template;
        this.userDao = userDao;
    }

    public void create(ke.co.hoji.server.model.CreditTransaction creditTransaction) {
        String sql = "INSERT INTO credit_transaction (user_id, form_id, date, credits, reference_no, type, expiry)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Object[] parameters = new Object[] {
                creditTransaction.getUser().getId(),
                creditTransaction.getForm() != null ? creditTransaction.getForm().getId() : null,
                creditTransaction.getDate(),
                creditTransaction.getCredits(),
                creditTransaction.getReferenceNo(),
                creditTransaction.getType(),
                creditTransaction.getExpiry()
        };
        template.update(sql, parameters);
    }

    public Integer balance(User user, Date date) {
        String select = "SELECT SUM(credits) FROM credit_transaction WHERE user_id = ? AND date <= ?";
        Integer balance = template.queryForObject(select, new Object[]{ user.getId(), date }, Integer.class);
        if (balance == null) {
            balance = 0;
        }
        return balance;
    }

    public Integer previouslyPaid(String recordUuid) {
        String select = "SELECT SUM(credits) FROM credit_transaction WHERE reference_no = ? AND type = ?";
        Integer previouslyPaid = template.queryForObject(select, new Object[]{ recordUuid, Type.EXPENSE }, Integer.class);
        if (previouslyPaid == null) {
            previouslyPaid = 0;
        }
        return -previouslyPaid;
    }

    public Date dateOfLastExpense(User user) {
        String select = "SELECT MAX(date) FROM credit_transaction WHERE type = ? AND user_id = ?";
        Date lastExpenseDate = template.queryForObject(select, new Object[]{Type.EXPENSE, user.getId()}, Date.class);
        return lastExpenseDate;
    }

    public Integer expiryPeriod(User user) {
        String select = "SELECT MAX(expiry) FROM credit_transaction WHERE expiry IS NOT NULL AND user_id = ?";
        Integer expiry = template.queryForObject(select, new Object[]{user.getId()}, Integer.class);
        return expiry;
    }

    public Boolean hasEverBoughtABundle(User user) {
        String select = "SELECT COUNT(id) FROM credit_transaction WHERE type = ? AND user_id = ?";
        Integer expiry = template.queryForObject(select, new Object[]{Type.DEPOSIT, user.getId()}, Integer.class);
        return expiry > 0;
    }

    private class CreditTransactionRowMapper implements RowMapper<ke.co.hoji.server.model.CreditTransaction> {

        @Override
        public ke.co.hoji.server.model.CreditTransaction mapRow(ResultSet resultSet, int i) throws SQLException {
            ke.co.hoji.server.model.CreditTransaction creditTransaction = new ke.co.hoji.server.model.CreditTransaction
                    (
                            userDao.findById(resultSet.getInt("user_id")),
                            resultSet.getDate("date"),
                            resultSet.getString("reference_no"),
                            resultSet.getString("type")
                    );
            creditTransaction.setId(resultSet.getLong("id"));
            creditTransaction.setForm(null);
            creditTransaction.setCredits(resultSet.getInt("credits"));
            creditTransaction.setExpiry(resultSet.getInt("expiry"));
            return creditTransaction;
        }
    }
}
