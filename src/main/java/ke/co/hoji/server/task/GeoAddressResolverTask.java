package ke.co.hoji.server.task;

import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.server.service.GeoAddressResolverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * <p>
 * A task that runs {@link GeoAddressResolverService} to resolve address information for records that are missing that
 * piece of information.
 * </p>
 * Created by geoffreywasilwa on 30/03/2017.
 */
@Profile("production")
@Component
public class GeoAddressResolverTask {

    /**
     * The maximum number of queries Google allows us to make per day.
     */
    private static final int DAILY_QUERY_LIMIT = 2500;

    /**
     * The number of {@link MainRecord} we shall process during each run of the service.
     */
    private static final int BATCH_SIZE = 50;

    /**
     * The maximum number of "batches" of {@link MainRecord}s we can process per run of this service, based on
     * {@link #DAILY_QUERY_LIMIT} and {@link #BATCH_SIZE}.
     */
    private int maxBatchesPerRun = (int) Math.ceil((double) DAILY_QUERY_LIMIT / BATCH_SIZE);

    /**
     * The number of "batches" of {@link MainRecord}s we have processed during the current run of this service.
     */
    private int batchesInCurrentRun = 0;

    @Autowired
    GeoAddressResolverService geoAddressResolverService;

    @Scheduled(cron = "0 */20 * * * * ")
    public void reverseGeoCode() {
        if (batchesInCurrentRun < maxBatchesPerRun) {
            geoAddressResolverService.resolveAddresses(BATCH_SIZE);
            batchesInCurrentRun++;
        } else {
            batchesInCurrentRun = 0;
            return;
        }
    }
}
