package ke.co.hoji.server.task;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.event.model.LoggableEvent;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.LoggableEventService;
import ke.co.hoji.server.service.UserService;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Runs periodically to solicit reviews from qualified users.
 * <p>
 * Created by gitahi on 17/05/2018.
 */
@Profile("production")
@Component
public class ReviewSolicitorTask {

    /**
     * The minimum number of events a user needs to have logged during the
     * {@link #SOLICITATION_PERIOD} before they are entered into the
     * solicitation qualification algorithm.
     */
    private static final Integer MIN_NO_OF_EVENTS = 5;

    /**
     * The number of days a user must have existed on the platform since
     * joining before they are entered into the solicitation qualification
     * algorithm.
     */
    private static final Integer SOLICITATION_LEAD_TIME = 5;

    /**
     * The number of days before the {@link #SOLICITATION_LEAD_TIME} that
     * we shall consider when evaluating a users level of activity for the sake
     * of qualifying them for review solicitation.
     */
    private static final Integer SOLICITATION_PERIOD = 14;

    private LoggableEventService loggableEventService;
    private ApplicationEventPublisher applicationEventPublisher;
    private UserService userService;

    @Autowired
    public void setLoggableEventService(LoggableEventService loggableEventService) {
        this.loggableEventService = loggableEventService;
    }

    @Autowired
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Autowired
    public void setLoggableEventService(UserService userService) {
        this.userService = userService;
    }

    @Scheduled(cron = "0 0 15 * * *")
    public void solicitReviews() {
        List<LoggableEvent> loggableEvents = loggableEventService.findEventsForReviewSolicitation(
                getFromDate(),
                getToDate()
        );
        Map<User, EventCollection> eventCollectionHashMap = new HashMap<>();
        for (LoggableEvent loggableEvent : loggableEvents) {
            User user = loggableEvent.getUser();
            if (!eventCollectionHashMap.containsKey(user)) {
                eventCollectionHashMap.put(user, new EventCollection());
            }
            if (loggableEvent.getSource().equals(EventSource.MOBILE)) {
                eventCollectionHashMap.get(user).addMobileEvent(loggableEvent);
            } else if (loggableEvent.getSource().equals(EventSource.WEB)) {
                eventCollectionHashMap.get(user).addWebEvent(loggableEvent);
            }
        }
        for (User user : eventCollectionHashMap.keySet()) {
            UserEvent userEvent = null;
            EventCollection eventCollection = eventCollectionHashMap.get(user);
            if (eventCollection.webEvents.size() >= MIN_NO_OF_EVENTS) {
                userEvent = new UserEvent(EventSource.WEB, user, user, EventType.SOLICIT_OFFICER_REVIEW);
            } else if (eventCollection.mobileEvents.size() >= MIN_NO_OF_EVENTS) {
                userEvent = new UserEvent(EventSource.WEB, user, user, EventType.SOLICIT_ENUMERATOR_REVIEW);
            }
            if (userEvent != null) {
                userEvent.putProperty(UserEvent.DAYS_SINCE_SIGN_UP, getDaysSinceSignUp(user));
                userEvent.putProperty(UserEvent.TENANT_NAME, userService.findById(user.getTenantUserId()).getFullName());
                applicationEventPublisher.publishEvent(userEvent);
            }
        }
    }

    private Date getFromDate() {
        return new LocalDate(new Date()).minusDays(SOLICITATION_PERIOD + SOLICITATION_LEAD_TIME).toDate();
    }

    private Date getToDate() {

        return new LocalDate(new Date()).minusDays(SOLICITATION_LEAD_TIME).toDate();
    }

    private int getDaysSinceSignUp(User user) {
        return Days.daysBetween(new LocalDate(user.getDateCreated()), new LocalDate(new Date())).getDays();
    }

    private class EventCollection {

        private List<LoggableEvent> mobileEvents = new ArrayList<>();
        private List<LoggableEvent> webEvents = new ArrayList<>();

        public void addMobileEvent(LoggableEvent loggableEvent) {
            mobileEvents.add(loggableEvent);
        }

        public void addWebEvent(LoggableEvent loggableEvent) {
            webEvents.add(loggableEvent);
        }
    }
}
