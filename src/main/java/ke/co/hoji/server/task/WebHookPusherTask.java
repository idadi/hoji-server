package ke.co.hoji.server.task;

import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.server.model.ApiRecord;
import ke.co.hoji.server.model.MissedMessage;
import ke.co.hoji.server.repository.MainRecordRepository;
import ke.co.hoji.server.service.WebHookMessenger;
import ke.co.hoji.server.service.WebHookService;
import ke.co.hoji.server.utils.ServerUtils;

/**
 * <p>Resends failed records to registered end points. Failed records are those that the endpoints
 * have not acknowledged receipt</p>
 * <p>The period between runs is configured in <i>hoji.properties</i> file by the property: <i>webhook.message.interval</i></p>
 * @author geoffreywasilwa
 *
 */
@Profile("production")
@Component
public class WebHookPusherTask {

    private final static Logger logger = LoggerFactory.getLogger(WebHookPusherTask.class);

    private final WebHookService webHookService;
    private final MainRecordRepository mainRecordRepository;
    private final FieldService fieldService;
    private final WebHookMessenger webHookMessenger;

    @Autowired
    public WebHookPusherTask(
        WebHookService webHookService,
        MainRecordRepository mainRecordRepository,
        FieldService fieldService,
        WebHookMessenger webHookMessenger){
        this.webHookService = webHookService;
        this.webHookMessenger = webHookMessenger;
        this.mainRecordRepository = mainRecordRepository;
        this.fieldService = fieldService;
    }

    @Scheduled(fixedDelayString = "${webhook.message.interval}")
    public void sendMessage() {
        List<MissedMessage> missedMessages = webHookService.getMissedMessages();
        for (MissedMessage missedMessage : missedMessages) {
            ServerUtils.setChildren(missedMessage.getForm(), fieldService);
            Optional<ApiRecord> apiRecord = mainRecordRepository.findApiRecord(missedMessage.getMainRecordUuid(), missedMessage.getForm());
            if (!apiRecord.isPresent()) {
                continue;
            }
            try {
                WebHookMessenger.Status status = webHookMessenger.send(missedMessage.getWebHook(), apiRecord.get(), missedMessage.getEventDescription());
                if (status == WebHookMessenger.Status.SUCCESS) {
                    webHookService.deleteMissedMessage(missedMessage.getId());
                }
            } catch (JsonProcessingException e) {
                logger.error(e.getMessage(), e);
            }
        }
    }
}