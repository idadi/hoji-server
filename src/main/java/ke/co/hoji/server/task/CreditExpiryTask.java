package ke.co.hoji.server.task;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.User.Type;
import ke.co.hoji.server.service.CreditTransactionService;
import ke.co.hoji.server.service.UserService;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Runs periodically to expire credits or notify users of imminent credit expiration.
 * <p>
 * Created by gitahi on 06/01/2018.
 */
@Profile("production")
@Component
public class CreditExpiryTask {

    /**
     * The number of days to expiration at which the @{@link User} is first notified.
     */
    private static final Integer FIRST_EXPIRATION_NOTICE_POINT = 28;

    /**
     * The number of days to expiration at which the @{@link User} is last notified.
     */
    private static final Integer LAST_EXPIRATION_NOTICE_POINT = 7;

    private UserService userService;
    private CreditTransactionService creditTransactionService;
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setCreditTransactionService(CreditTransactionService creditTransactionService) {
        this.creditTransactionService = creditTransactionService;
    }

    @Autowired
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    /**
     * Loop through all enabled @{@link User}s and for all who have positive credit balances, check how long before
     * their credits are due for expiration and react accordingly..
     */
    @Scheduled(cron = "0 0 6 * * *")
    public void processExpiry() {
        Date now = new Date();
        List<User> users = userService.findAllUsers(true);
        for (User user : users) {
            if (user.getType() == Type.POSTPAID) {
                continue;
            }
            Integer creditBalance = creditTransactionService.balance(user);
            if (creditBalance > 0) {
                Boolean hasEverBoughtABundle = creditTransactionService.hasEverBoughtABundle(user);
                Integer expiryPeriod = creditTransactionService.expiryPeriod(user);
                Date lastExpenseDate = creditTransactionService.dateOfLastExpense(user);
                Integer daysSinceLastExpense = daysSinceLastExpense(user, now, lastExpenseDate);
                Integer daysToExpiry = expiryPeriod - daysSinceLastExpense;
                Date expiryDate = new DateTime(now).plusDays(daysToExpiry).toDate();
                UserEvent userEvent = null;
                if (daysToExpiry < 1) {
                    creditTransactionService.expireCredits(user);
                    if (hasEverBoughtABundle) {
                        userEvent = new UserEvent(EventSource.WEB, user, user, EventType.CREDITS_EXPIRED);
                    } else {
                        userEvent = new UserEvent(EventSource.WEB, user, user, EventType.FREE_CREDITS_EXPIRED);
                    }
                } else if (daysToExpiry == FIRST_EXPIRATION_NOTICE_POINT
                        || daysToExpiry == LAST_EXPIRATION_NOTICE_POINT) {
                    if (hasEverBoughtABundle) {
                        userEvent = new UserEvent(EventSource.WEB, user, user, EventType.EXPIRATION_NOTICE);
                    } else {
                        userEvent = new UserEvent(EventSource.WEB, user, user, EventType.FREE_EXPIRATION_NOTICE);
                    }
                    if (daysToExpiry == FIRST_EXPIRATION_NOTICE_POINT) {
                        userEvent.putProperty(UserEvent.EXPIRY_NOTIFICATION_NO, 1);
                    } else if (daysToExpiry == LAST_EXPIRATION_NOTICE_POINT) {
                        userEvent.putProperty(UserEvent.EXPIRY_NOTIFICATION_NO, 2);
                    }
                }
                if (userEvent != null) {
                    userEvent.putProperty(UserEvent.CREDIT_BALANCE, creditBalance);
                    userEvent.putProperty(UserEvent.EXPIRY_PERIOD, expiryPeriod);
                    userEvent.putProperty(UserEvent.DAYS_TO_EXPIRY, daysToExpiry);
                    userEvent.putProperty(UserEvent.DORMANT_DAYS, daysSinceLastExpense);
                    userEvent.putProperty(UserEvent.LAST_EXPENSE_DATE, lastExpenseDate);
                    userEvent.putProperty(UserEvent.EXPIRY_DATE, expiryDate);
                    applicationEventPublisher.publishEvent(userEvent);
                }
            }
        }
    }

    /*
     * Calculate the number of days since the user spent any of his credits. If the user has never spent any credits,
     * instead calculate the number of days since he signed up.
     */
    private int daysSinceLastExpense(User user, Date expiryDate, Date today) {
        int daysSinceLastExpense = Days.daysBetween(
                new DateTime(today).toLocalDate(),
                new DateTime(expiryDate).toLocalDate()
        ).getDays();
        if (daysSinceLastExpense == 0) {
            daysSinceLastExpense = Days.daysBetween(
                    new DateTime(user.getDateCreated()).toLocalDate(),
                    new DateTime(today).toLocalDate()
            ).getDays();
        }
        return daysSinceLastExpense;
    }
}
