package ke.co.hoji.server.resolver;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.WebHook;
import ke.co.hoji.server.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.ArrayList;
import java.util.List;

public class WebHookHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {

    private final FormService formService;
    private final UserService userService;

    @Autowired
    public WebHookHandlerMethodArgumentResolver(FormService formService, UserService userService) {
        this.formService = formService;
        this.userService = userService;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().equals(WebHook.class);
    }

    @Override
    public Object resolveArgument(
            MethodParameter parameter, ModelAndViewContainer mavContainer,
            NativeWebRequest webRequest, WebDataBinderFactory binderFactory
    ) throws Exception {
        String possibleId = webRequest.getParameter("id");
        Integer id = StringUtils.isNotBlank(possibleId) ? Integer.valueOf(possibleId) : null;
        List<Form> forms = new ArrayList<>();
        String[] formIds = webRequest.getParameterValues("forms");
        if (formIds != null) {
            for (String formId : formIds) {
                forms.add(formService.getFormById(Integer.valueOf(formId)));
            }
        }
        List<EventType> eventTypes = new ArrayList<>();
        String[] events = webRequest.getParameterValues("eventTypes");
        if (events != null) {
            for (String event : events) {
                eventTypes.add(EventType.valueOf(event));
            }
        }
        String targetUrl = webRequest.getParameter("targetUrl");
        Integer userId = userService.getCurrentUser().getId();
        boolean verified = "true".equalsIgnoreCase(webRequest.getParameter("verified")) ? true : false;
        boolean active = "true".equalsIgnoreCase(webRequest.getParameter("active")) ? true : false;
        WebHook webHook = new WebHook(id, forms, eventTypes, targetUrl, userId, verified, active, null);
        if (parameter.hasParameterAnnotation(Validated.class)) {
            WebDataBinder dataBinder = binderFactory.createBinder(webRequest, webHook, "webHook");
            dataBinder.validate();
            String className = WebHook.class.getSimpleName();
            String beanName = Character.toLowerCase(className.charAt(0)) + className.substring(1);
            String attributeName = BindingResult.MODEL_KEY_PREFIX + beanName;
            mavContainer.getModel().addAttribute(attributeName, dataBinder.getBindingResult());
        }
        return webHook;
    }
}
