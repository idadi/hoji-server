package ke.co.hoji.server.resolver;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.server.controller.report.model.Filter;
import ke.co.hoji.server.controller.report.model.Filter.FilterBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static ke.co.hoji.core.Utils.parseLocalDate;

/**
 * Created by geoffreywasilwa on 12/04/2017.
 */
public class FilterHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {

    protected static final String FIELD_TYPE = "field";
    private static final String FORM_ID_PATH_VARIABLE = "formId";
    private static final String FIELD_ID_PATH_VARIABLE = "fieldId";
    private final ConfigService configService;

    @Autowired
    public FilterHandlerMethodArgumentResolver(ConfigService configService) {
        this.configService = configService;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().equals(Filter.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter,
                                  ModelAndViewContainer modelAndViewContainer,
                                  NativeWebRequest nativeWebRequest,
                                  WebDataBinderFactory webDataBinderFactory) throws Exception {
        String[] ids = nativeWebRequest.getParameterValues("selectedUsers[]");
        List<Integer> selectedUsers = new ArrayList<>();
        if (ids != null) {
            for (String id : ids) {
                selectedUsers.add(Integer.valueOf(id));
            }
        }
        String uri = ((ServletWebRequest) nativeWebRequest).getRequest().getRequestURI();
        Map<String, String> pathVariables =
            (Map<String, String>) nativeWebRequest.getNativeRequest(HttpServletRequest.class)
                .getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        FieldParent fieldParent;
        if (StringUtils.contains(uri, FIELD_TYPE)) {
            fieldParent = configService.getField(Integer.valueOf(pathVariables.get(FIELD_ID_PATH_VARIABLE)));
        } else {
            fieldParent = configService.getForm(Integer.valueOf(pathVariables.get(FORM_ID_PATH_VARIABLE)));
        }
        Integer filterFieldId = Utils.parseInteger(nativeWebRequest.getParameter("selectedField"));
        Integer filterFieldValue = Utils.parseInteger(nativeWebRequest.getParameter("selectedChoice"));
        String createdFrom = nativeWebRequest.getParameter("createdFrom");
        String createdTo = nativeWebRequest.getParameter("createdTo");
        String completed = nativeWebRequest.getParameter("completed");
        String completedAfter = nativeWebRequest.getParameter("completedAfter");
        String completedBefore = nativeWebRequest.getParameter("completedBefore");
        String uploaded = nativeWebRequest.getParameter("uploaded");
        String uploadedAfter = nativeWebRequest.getParameter("uploadedAfter");
        String uploadedBefore = nativeWebRequest.getParameter("uploadedBefore");
        String testMode = nativeWebRequest.getParameter("testMode");

        return new FilterBuilder(fieldParent)
            .selectedUsers(selectedUsers)
            .filterFieldId(filterFieldId)
            .filterFieldValue(filterFieldValue)
            .createdFrom(createdFrom != null ? parseLocalDate(createdFrom).toDate() : null)
            .createdTo(createdTo != null ? parseLocalDate(createdTo).toDate() : null)
            .completed(completed)
            .completedFrom(completedAfter != null ? parseLocalDate(completedAfter).toDate() : null)
            .completedTo(completedBefore != null ? parseLocalDate(completedBefore).toDate() : null)
            .uploaded(uploaded)
            .uploadedFrom(uploadedAfter != null ? parseLocalDate(uploadedAfter).toDate() : null)
            .uploadedTo(uploadedBefore != null ? parseLocalDate(uploadedBefore).toDate() : null)
            .testMode(Boolean.valueOf(testMode))
            .build();
    }
}
