package ke.co.hoji.server.templateresolver;

import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.Template;
import ke.co.hoji.server.service.TemplateService;
import org.thymeleaf.IEngineConfiguration;
import org.thymeleaf.templateresolver.StringTemplateResolver;
import org.thymeleaf.templateresource.ITemplateResource;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ThymeleafDatabaseResourceResolver extends StringTemplateResolver {

    private final static String PREFIX = "db:";
    private final TemplateService templateService;

    public ThymeleafDatabaseResourceResolver(TemplateService templateService) {
        this.templateService = templateService;
        Set<String> resolvablePatterns = new HashSet<>();
        resolvablePatterns.add(PREFIX + "*");
        setResolvablePatterns(resolvablePatterns);
    }

    @Override
    protected ITemplateResource computeTemplateResource(IEngineConfiguration configuration, String ownerTemplate, String template, Map<String, Object> templateResolutionAttributes) {

        try {
            template = template.substring(PREFIX.length());
            EventType eventType = Enum.valueOf(EventType.class, template);
            Template emailTemplate = templateService.getTemplateByEvent(eventType);
            if (emailTemplate == null) {
                return null;
            }
            String thymeleafTemplate = emailTemplate.getTemplate();
            if (thymeleafTemplate != null) {
                return super.computeTemplateResource(configuration, ownerTemplate, thymeleafTemplate, templateResolutionAttributes);
            }
        } catch (IllegalArgumentException e) {
            return null;
        }
        return null;
    }

}
