package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.service.EmailService;
import org.thymeleaf.TemplateEngine;

/**
 * Emails qualified officers to solicit app reviews.
 */
public class SolicitOfficerReviewEmailer extends SolicitReviewEmailer {

    public SolicitOfficerReviewEmailer(EmailService emailService, TemplateEngine templateEngine) {
        super(emailService, templateEngine);
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.SOLICIT_OFFICER_REVIEW.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }
}
