package ke.co.hoji.server.event.listener;

import ke.co.hoji.server.event.event.HojiApplicationEvent;
import ke.co.hoji.server.event.model.LoggableEvent;
import ke.co.hoji.server.event.processor.EventProcessorManager;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.LoggableEventService;
import ke.co.hoji.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * The {@link ApplicationListener} for all {@link HojiApplicationEvent}s.
 */
@Component
public class HojiApplicationListener implements ApplicationListener<HojiApplicationEvent> {

    /**
     * The {@link LoggableEventService} to use for logging events.
     */
    @Autowired
    private LoggableEventService loggableEventService;

    /**
     * The {@link EventProcessorManager} to use for processing events.
     */
    @Autowired
    private EventProcessorManager eventProcessorManager;

    /**
     * The {@link UserService} to use for managing user information.
     */
    @Autowired
    private UserService userService;

    /**
     * Logs a {@link HojiApplicationEvent} to the database, then delegates any further event processing to
     * {@link EventProcessorManager}.
     */
    @Override
    public void onApplicationEvent(HojiApplicationEvent hojiApplicationEvent) {
        logEvent(hojiApplicationEvent);
        eventProcessorManager.process(hojiApplicationEvent);
    }

    private void logEvent(HojiApplicationEvent hojiApplicationEvent) {
        User user = hojiApplicationEvent.getUser();
        if (user != null) {
            User tenant = userService.findById(user.getTenantUserId());
            loggableEventService.log(LoggableEvent.createFromHojiApplicationEvent(hojiApplicationEvent, user, tenant));
        }
    }
}
