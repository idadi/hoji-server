package ke.co.hoji.server.event.model;

import ke.co.hoji.server.event.event.HojiApplicationEvent;
import ke.co.hoji.server.model.User;

/**
 * The originator of a {@link HojiApplicationEvent}. An event may originate either from Hoji or from a {@link User}.
 */
public enum EventOriginator {

    /**
     * For events originated by the system.
     */
    HOJI,

    /**
     * For events originated by the user.
     */
    USER
}
