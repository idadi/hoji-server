package ke.co.hoji.server.event.listener;

import ke.co.hoji.core.Utils;
import ke.co.hoji.server.event.event.HojiApplicationEvent;
import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventOriginator;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.event.model.LoggableEvent;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.LoggableEventService;
import ke.co.hoji.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.LinkedHashMap;

/**
 * An {@link ApplicationListener} for detecting successful web-based sign in events.
 */
@Component
public class SignInListener implements ApplicationListener<InteractiveAuthenticationSuccessEvent> {

    private static final String NAME = UserEvent.class.getSimpleName();

    /**
     * The {@link LoggableEventService} to use for logging events.
     */
    @Autowired
    private LoggableEventService loggableEventService;

    /**
     * The {@link UserService} to use for managing user information.
     */
    @Autowired
    private UserService userService;


    @Override
    public void onApplicationEvent(InteractiveAuthenticationSuccessEvent interactiveAuthenticationSuccessEvent) {
        User user = (User) interactiveAuthenticationSuccessEvent.getAuthentication().getPrincipal();
        User tenant = userService.findById(user.getTenantUserId());
        LinkedHashMap<String, Object> descriptiveProperties = new LinkedHashMap<>();
        descriptiveProperties.put("Email", user.getEmail());
        descriptiveProperties.put("Tenant ID", user.getTenantUserId());
        descriptiveProperties.put("Roles", user.getRoles());
        String description = Utils.mapToString(descriptiveProperties);
        LoggableEvent loggableEvent = new LoggableEvent(
                NAME,
                new Date(interactiveAuthenticationSuccessEvent.getTimestamp()),
                user,
                tenant,
                EventType.SIGN_IN,
                user.getClass().getSimpleName(),
                user.getId().toString(),
                description,
                EventResult.SUCCESS,
                EventSource.WEB,
                EventOriginator.USER,
                null
        );
        loggableEventService.log(loggableEvent);
    }
}
