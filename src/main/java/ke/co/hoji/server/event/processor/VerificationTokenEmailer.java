package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.VerificationToken;
import ke.co.hoji.server.service.EmailService;
import ke.co.hoji.server.service.UserService;
import org.thymeleaf.TemplateEngine;

import java.util.Date;
import java.util.UUID;

/**
 * The convenience super class for all {@link Emailer}s need to send the user verification token.
 */
public abstract class VerificationTokenEmailer<E extends UserEvent> extends Emailer<E> {

    private final UserService userService;

    public VerificationTokenEmailer(EmailService emailService, TemplateEngine templateEngine, UserService userService) {
        super(emailService, templateEngine);
        this.userService = userService;
    }

    /**
     * Generates a verification token and stores it in the database along with its expiry period.
     *
     * @param type the {@link VerificationToken.Type} of verification token to generate.
     * @param user the @{@link User} for whom to generate a verification token.
     *
     * @return the newly generated verification token.
     */
    protected String verificationToken(int type, User user) {
        String token = UUID.randomUUID().toString();
        userService.createVerificationToken(new VerificationToken(token, type, user, calculateExpiryDate()));
        return token;
    }

    private long calculateExpiryDate() {
        long expiryDate = new Date().getTime();
        expiryDate += (VerificationToken.EXPIRATION_PERIOD * 1000);
        return expiryDate;
    }
}
