package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.SurveyEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

/**
 * Pushes out a notification to Firebase Cloud Messenger whenever a project is published.
 */
public class PublicationNotificationPusher implements EventProcessor<SurveyEvent> {

    private Logger logger = LoggerFactory.getLogger(PublicationNotificationPusher.class);

    /**
     * The authentication key for Firebase Cloud Messaging.
     */
    private final String fcmApiKey;

    /**
     * The URL for Hoji's Firebase Cloud Messaging project.
     */
    private final String fcmUrl;

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.PUBLISH.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }

    public PublicationNotificationPusher(String fcmApiKey, String fcmUrl) {
        this.fcmApiKey = fcmApiKey;
        this.fcmUrl = fcmUrl;
    }

    @Override
    public void process(SurveyEvent surveyEvent) {
        if (!surveyEvent.getSubject().isEnabled()) {
            return;
        }
        try {
            String json = "{ \"to\": \"/topics/project_updates\", \"data\": { " + "\"surveyId\": " + surveyEvent.getSubject().getId() + " } }";
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "key=" + fcmApiKey);
            headers.set("Content-Type", "application/json");
            HttpEntity<String> entity = new HttpEntity<>(json, headers);
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.exchange(fcmUrl, HttpMethod.POST, entity, String.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }
}
