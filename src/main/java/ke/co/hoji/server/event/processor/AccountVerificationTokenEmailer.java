package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.VerificationToken;
import ke.co.hoji.server.service.EmailService;
import ke.co.hoji.server.service.UserService;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Emails the user an email verification token.
 */
public class AccountVerificationTokenEmailer extends VerificationTokenEmailer<UserEvent> {

    public AccountVerificationTokenEmailer(
        EmailService emailService,
        TemplateEngine templateEngine,
        UserService userService
    ) {
        super(emailService, templateEngine, userService);
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        boolean verifiable = EventType.SIGN_UP.equals(eventType)
            || EventType.REQUEST_TOKEN.equals(eventType)
            || EventType.CHANGE_EMAIL.equals(eventType);
        return verifiable && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(UserEvent userEvent) {
        Context context = new Context();
        User user = userEvent.getUser();
        String applicationUrl = applicationUrl() + "/confirm?token="
            + verificationToken(VerificationToken.Type.EMAIL_VERIFICATION, user);
        context.setVariable("user", user);
        context.setVariable("applicationUrl", applicationUrl);
        String subject = "Please verify your email address";
        String body = templateEngine.process(templateName(userEvent.getType()), context);
        emailService.sendEmail(user, subject, body, userEvent);
    }
}
