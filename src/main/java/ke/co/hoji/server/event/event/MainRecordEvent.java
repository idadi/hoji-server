package ke.co.hoji.server.event.event;

import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;

/**
 * An event for which the {@link HojiApplicationEvent#subject} is a {@link MainRecord}.
 */
public class MainRecordEvent extends HojiApplicationEvent<MainRecord> {

    /**
     * The property key for the {@link Survey} under which the {@link MainRecord} here belongs.
     */
    public static final String SURVEY = "Survey";

    public MainRecordEvent(EventSource eventSource, User user, MainRecord subject, EventType type) {
        super(eventSource, user, subject, type);
    }

    @Override
    public String subjectId() {
        return subject.getUuid();
    }
}
