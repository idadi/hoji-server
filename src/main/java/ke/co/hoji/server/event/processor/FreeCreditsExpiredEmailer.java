package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.EmailService;
import org.thymeleaf.TemplateEngine;

/**
 * Emails the @{@link User} when their FREE credits expire.
 */
public class FreeCreditsExpiredEmailer extends CreditsExpiredEmailer {

    public FreeCreditsExpiredEmailer(EmailService emailService, TemplateEngine templateEngine) {
        super(emailService, templateEngine);
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.FREE_CREDITS_EXPIRED.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }
}
