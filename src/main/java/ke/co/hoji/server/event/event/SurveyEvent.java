package ke.co.hoji.server.event.event;

import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;

/**
 * An event for which the {@link HojiApplicationEvent#subject} is a {@link Survey}.
 */
public class SurveyEvent extends HojiApplicationEvent<Survey> {

    public SurveyEvent(EventSource eventSource, User user, Survey subject, EventType type) {
        super(eventSource, user, subject, type);
    }

    @Override
    public String subjectId() {
        return String.valueOf(subject.getId());
    }
}
