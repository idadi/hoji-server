package ke.co.hoji.server.event.processor;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.event.event.MainRecordEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.EmailService;
import org.apache.commons.lang3.StringUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Date;

/**
 * Emails the user after they have successfully submitted a sample {@link MainRecord}. This class is design to
 * support 2 versions of emails, depending on whether the user is using a pre-10.* version of the mobile app or later.
 */
public class MainRecordCreatedEmailer extends Emailer<MainRecordEvent> {

    public MainRecordCreatedEmailer(EmailService emailService, TemplateEngine templateEngine) {
        super(emailService, templateEngine);
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.MAIN_RECORD_CREATE.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(MainRecordEvent mainRecordEvent) {
        boolean newAge = false;
        String appVersion = mainRecordEvent.getSubject().getAppVersion();
        if (appVersion != null) {
            Integer mainVersion = Utils.parseInteger(appVersion.split("\\.")[0]);
            if (mainVersion != null && mainVersion >= 10) {
                newAge = true;
            }
        }
        Survey survey = (Survey) mainRecordEvent.getProperty(MainRecordEvent.SURVEY);
        if (survey != null && survey.isSample()) {
            MainRecord mainRecord = mainRecordEvent.getSubject();
            int formId = mainRecord.getFormId();
            Context context = new Context();
            User user = mainRecordEvent.getUser();
            String applicationUrl = applicationUrl();
            String outputUrl = "project/" + survey.getId() + "/form/" + formId + "/report/";
            String time = Utils.formatSimpleTime(new Date(mainRecord.getDateUploaded()));
            context.setVariable("user", user);
            context.setVariable("recordUrl", applicationUrl + "/project/" + survey.getId()
                + "/form/" + formId + "/record?recordUuid=" + mainRecord.getUuid());
            context.setVariable("dataTableUrl", applicationUrl + outputUrl + "table");
            context.setVariable("analysisUrl", applicationUrl + outputUrl + "analysis");
            context.setVariable("dashboardUrl", applicationUrl + outputUrl + "dashboard");
            context.setVariable("pivotTableUrl", applicationUrl + outputUrl + "pivot");
            context.setVariable("mapUrl", applicationUrl + outputUrl + "map");
            context.setVariable("monitorUrl", applicationUrl + outputUrl + "monitor");
            context.setVariable("recordTime", time);
            context.setVariable("recordCaption", StringUtils.isNotBlank(mainRecord.getCaption())
                ? mainRecord.getCaption() : "Your Record");
            context.setVariable("newAge", newAge);
            String subject;
            EmailService.SourceEmail sourceEmail;
            if (newAge) {
                subject = "Congratulations, " + user.getFirstName() + "! We have received your submission";
                sourceEmail = EmailService.SourceEmail.CUSTOMER_SUCCESS;
            } else {
                subject = "Congratulations! We have received your form submission at " + time;
                sourceEmail = EmailService.SourceEmail.NO_REPLY;
            }
            String body = templateEngine.process(templateName(mainRecordEvent.getType()), context);
            emailService.sendEmail(user, sourceEmail, subject, body, mainRecordEvent);
        }
    }
}
