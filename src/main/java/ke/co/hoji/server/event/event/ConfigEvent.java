package ke.co.hoji.server.event.event;

import ke.co.hoji.core.data.ConfigObject;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;

/**
 * An event for which the {@link HojiApplicationEvent#subject} is a {@link ConfigObject}.
 */
public class ConfigEvent extends HojiApplicationEvent<ConfigObject> {

    /**
     * The number of {@link Form}s under a {@link Survey}.
     */
    public static final String NO_OF_FORMS = "Forms";

    /**
     * The number of {@link Field}s under a {@link Form}.
     */
    public static final String NO_OF_FIELDS = "Fields";

    /**
     * The number of {@link Rule}s under a {@link Field}.
     */
    public static final String NO_OF_RULES = "Rules";

    /**
     * The type of {@link Rule} being listed.
     */
    public static final String RULE_TYPE = "Rule Type";

    public ConfigEvent(EventSource eventSource, User user, ConfigObject subject, EventType type) {
        super(eventSource, user, subject, type);
    }

    @Override
    public String subjectId() {
        return String.valueOf(subject.getId());
    }
}
