package ke.co.hoji.server.event.model;

import ke.co.hoji.server.event.event.HojiApplicationEvent;

/**
 * The source of a {@link HojiApplicationEvent}. An event may either be raised by the mobile app or by the web app.
 */
public enum EventSource {

    /**
     * For events caused from the mobile app.
     */
    MOBILE,

    /**
     * For events caused from the web app.
     */
    WEB
}
