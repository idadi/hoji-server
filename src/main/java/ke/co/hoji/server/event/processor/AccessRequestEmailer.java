package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.EmailService;
import ke.co.hoji.server.service.EmailService.SourceEmail;
import ke.co.hoji.server.service.UserService;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Emails a {@link User} (project owner) a request for access from another user (enumerator).
 */
public class AccessRequestEmailer extends Emailer<UserEvent> {

    private UserService userService;

    public AccessRequestEmailer(EmailService emailService, TemplateEngine templateEngine, UserService userService) {
        super(emailService, templateEngine);
        this.userService = userService;
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.REQUEST_ACCESS.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(UserEvent userEvent) {
        sendEmail(userEvent);
    }

    private void sendEmail(UserEvent userEvent) {
        Context context = new Context();
        User user = userEvent.getUser();
        User owner = userService.findById(user.getTenantUserId());
        String applicationUrl = applicationUrl();
        context.setVariable("user", user);
        context.setVariable("owner", owner);
        context.setVariable("applicationUrl", applicationUrl() + "/user/view");
        context.setVariable("userManagementUrl", applicationUrl + "user/users/");
        String subject = user.getFullName() + " is requesting access to your Hoji account";
        String body = templateEngine.process(templateName(userEvent.getType()), context);
        emailService.sendEmail(owner, SourceEmail.SUPPORT, subject, body, userEvent);
    }
}
