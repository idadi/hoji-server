package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.service.EmailService;
import org.thymeleaf.TemplateEngine;

/**
 * Emails qualified enumerators to solicit app reviews.
 */
public class SolicitEnumeratorReviewEmailer extends SolicitReviewEmailer {

    public SolicitEnumeratorReviewEmailer(EmailService emailService, TemplateEngine templateEngine) {
        super(emailService, templateEngine);
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.SOLICIT_ENUMERATOR_REVIEW.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }
}
