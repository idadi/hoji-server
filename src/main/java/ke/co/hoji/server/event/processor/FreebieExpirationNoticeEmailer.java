package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.EmailService;
import org.thymeleaf.TemplateEngine;

/**
 * Emails the @{@link User} the first notification when their FREE credits expiry is imminent.
 */
public class FreebieExpirationNoticeEmailer extends ExpirationNoticeEmailer {

    public FreebieExpirationNoticeEmailer(EmailService emailService, TemplateEngine templateEngine) {
        super(emailService, templateEngine);
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.FREE_EXPIRATION_NOTICE.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }
}
