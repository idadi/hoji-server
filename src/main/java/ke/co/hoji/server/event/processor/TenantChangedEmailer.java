package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.EmailService;
import ke.co.hoji.server.service.UserService;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Emails the user whenever they change their tenant account to someone else's.
 */
public class TenantChangedEmailer extends Emailer<UserEvent> {

    private final UserService userService;

    public TenantChangedEmailer(EmailService emailService, TemplateEngine templateEngine, UserService userService) {
        super(emailService, templateEngine);
        this.userService = userService;
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.CHANGE_TENANT.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(UserEvent userEvent) {
        User user = userEvent.getUser();
        User tenant = userService.findById(user.getTenantUserId());
        if (!user.getId().equals(tenant.getId())) {
            user.getRoles().isEmpty();
            String applicationUrl = applicationUrl() + "/user/view";
            Context context = new Context();
            context.setVariable("user", user);
            context.setVariable("tenant", tenant);
            context.setVariable("applicationUrl", applicationUrl);
            String subject = "You are now collaborating with " + tenant.getFullName();
            String body = templateEngine.process(templateName(userEvent.getType()), context);
            emailService.sendEmail(user, subject, body, userEvent);
        }
    }
}
