package ke.co.hoji.server.event.processor;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.server.event.event.MainRecordEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.ApiRecord;
import ke.co.hoji.server.model.WebHook;
import ke.co.hoji.server.service.UserService;
import ke.co.hoji.server.service.WebHookMessenger;
import ke.co.hoji.server.service.WebHookService;
import ke.co.hoji.server.utils.ServerUtils;

/**
 * Pushes a newly created or modified {@link MainRecord} to the registered {@link WebHook}s, if any.
 */
public class MainRecordWebHookPusher implements EventProcessor<MainRecordEvent> {

    private Logger logger = LoggerFactory.getLogger(MainRecordWebHookPusher.class);

    private final ApplicationContext applicationContext;
    private final WebHookService webHookService;
    private final FormService formService;
    private final FieldService fieldService;
    private final UserService userService;
    private final WebHookMessenger webHookMessenger;

    public MainRecordWebHookPusher(
            ApplicationContext applicationContext,
            WebHookService webHookService,
            FormService formService,
            FieldService fieldService,
            UserService userService,
            WebHookMessenger webHookMessenger) {
        this.applicationContext = applicationContext;
        this.webHookService = webHookService;
        this.formService = formService;
        this.fieldService = fieldService;
        this.userService = userService;
        this.webHookMessenger = webHookMessenger;
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        boolean createdOrModified = EventType.MAIN_RECORD_CREATE.equals(eventType)
                || EventType.MAIN_RECORD_MODIFY.equals(eventType);
        return createdOrModified && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(MainRecordEvent mainRecordEvent) {
        String eventDescription = "record.created";
        if (mainRecordEvent.getType().equals(EventType.MAIN_RECORD_MODIFY)) {
            eventDescription = "record.updated";
        }
        Form form = formService.getFormById(mainRecordEvent.getSubject().getFormId());
        ServerUtils.setChildren(form, fieldService);
        List<WebHook> webHooks = webHookService.findWebHooksFor(form);
        for (WebHook webHook : webHooks) {
            if (!webHook.isActive()) {
                continue;
            }
            ApiRecord apiRecord = ApiRecord.fromMainRecord(
                    form,
                    mainRecordEvent.getSubject(),
                    userService,
                    applicationContext.getBean(HojiContext.class)
            );
            try {
                WebHookMessenger.Status status = webHookMessenger.send(webHook, apiRecord, eventDescription);
                if (status == WebHookMessenger.Status.ERROR) {
                    webHookService.saveMissedMessage(webHook, mainRecordEvent.getSubject(), eventDescription);
                }
            } catch (JsonProcessingException e) {
                logger.error(e.getMessage(), e);
            }
        }
    }
}
