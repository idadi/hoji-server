package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.EmailService;
import ke.co.hoji.server.service.EmailService.SourceEmail;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Emails qualified @{@link User}s to solicit app reviews.
 */
public abstract class SolicitReviewEmailer extends Emailer<UserEvent> {

    public SolicitReviewEmailer(EmailService emailService, TemplateEngine templateEngine) {
        super(emailService, templateEngine);
    }

    @Override
    public abstract boolean supports(EventType eventType, EventResult eventResult);

    @Override
    public void process(UserEvent userEvent) {
        User user = userEvent.getUser();
        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("daysSinceSignUp", userEvent.getProperty(UserEvent.DAYS_SINCE_SIGN_UP));
        context.setVariable("tenantName", userEvent.getProperty(UserEvent.TENANT_NAME));
        String subject = "Make your voice count!";
        String body = templateEngine.process(templateName(userEvent.getType()), context);
        emailService.sendEmail(user, SourceEmail.CEO, subject, body, userEvent);
    }
}
