package ke.co.hoji.server.event.event;

import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.WebHook;

/**
 * An event for which the {@link HojiApplicationEvent#subject} is a {@link User}.
 */
public class UserEvent extends HojiApplicationEvent<User> {

    /**
     * The number of {@link Survey}s under a {@link User}.
     */
    public static final String NO_OF_SURVEYS = "Surveys";

    /**
     * The number of {@link WebHook}s under a {@link User}.
     */
    public static final String NO_OF_WEB_HOOKS = "Web-hooks";

    /**
     * The number of {@link User}s under this {@link User}s account.
     */
    public static final String NO_OF_USERS = "Users";

    /**
     * Amount of cash deposited by user.
     */
    public static final String CASH_AMOUNT = "Cash Amount";

    /**
     * Amount of credits bought by user.
     */
    public static final String CREDITS_AMOUNT = "Credits Amount";

    /**
     * The user's current credit balance.
     */
    public static final String CREDIT_BALANCE = "Credit Balance";

    /**
     * The number of days until the user's credits expire.
     */
    public static final String DAYS_TO_EXPIRY = "Days to Expiry";

    /**
     * The last date when this user spent a credit.
     */
    public static final String LAST_EXPENSE_DATE = "Last Expense Date";

    /**
     * The expiry date of this user's credits.
     */
    public static final String EXPIRY_DATE = "Expiry Date";

    /**
     * The expiry applicable credit expiry period.
     */
    public static final String EXPIRY_PERIOD = "Expiry Period";

    /**
     * The number of days the user has not spent any credits.
     */
    public static final String DORMANT_DAYS = "Dormant Days";

    /**
     * Number of days since this user signed up.
     */
    public static final String DAYS_SINCE_SIGN_UP = "Days Since Sign-up";

    /**
     * The name of the tenant under whose account this user is currently operating.
     */
    public static final String TENANT_NAME = "Tenant";

    /**
     * Whether this is the 1st, 2nd, e.t.c. notification of expiry.
     */
    public static final String EXPIRY_NOTIFICATION_NO = "Expiry Notification Number";

    public UserEvent(EventSource eventSource, User user, User subject, EventType type) {
        super(eventSource, user, subject, type);
    }

    @Override
    public String subjectId() {
        return String.valueOf(subject.getId());
    }
}
