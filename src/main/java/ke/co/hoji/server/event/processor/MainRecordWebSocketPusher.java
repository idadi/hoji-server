package ke.co.hoji.server.event.processor;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.server.event.event.MainRecordEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.service.UserService;
import ke.co.hoji.server.utils.ServerUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.messaging.simp.SimpMessagingTemplate;

/**
 * Pushes a newly created or modified {@link MainRecord} to the registered output web sockets, if any.
 */
public class MainRecordWebSocketPusher implements EventProcessor<MainRecordEvent> {

    private final SimpMessagingTemplate simpMessagingTemplate;
    private final HojiContext hojiContext;
    private final UserService userService;
    private final FormService formService;
    private final FieldService fieldService;

    public MainRecordWebSocketPusher(
            SimpMessagingTemplate simpMessagingTemplate, ApplicationContext applicationContext,
            UserService userService,
            FormService formService,
            FieldService fieldService) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.hojiContext = applicationContext.getBean(HojiContext.class);
        this.userService = userService;
        this.formService = formService;
        this.fieldService = fieldService;
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        boolean createdOrModified = EventType.MAIN_RECORD_CREATE.equals(eventType)
                || EventType.MAIN_RECORD_MODIFY.equals(eventType);
        return createdOrModified && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(MainRecordEvent mainRecordEvent) {
        MainRecord mainRecord = mainRecordEvent.getSubject();
        Integer formId = mainRecord.getFormId();
        Form form = formService.getFormById(formId);
        ServerUtils.setChildren(form, fieldService);
        sendMainRecordMessages(form, mainRecord);
        sendMatrixFieldMessages(form, mainRecord);
    }

    private void sendMainRecordMessages(Form form, MainRecord mainRecord) {
        Integer formId = form.getId();
        simpMessagingTemplate.convertAndSend("/topic/form/" + formId + "/report/pivot", "Update");
        simpMessagingTemplate.convertAndSend("/topic/form/" + formId + "/report/evolution", "Update");
        simpMessagingTemplate.convertAndSend("/topic/form/" + formId + "/report/gallery", "Update");
        simpMessagingTemplate.convertAndSend("/topic/form/" + formId + "/report/analysis", "Update");
        simpMessagingTemplate.convertAndSend("/topic/form/" + formId +"/report/monitor", "Update");
        simpMessagingTemplate.convertAndSend("/topic/form/" + formId + "/report/table", "Update");
        simpMessagingTemplate.convertAndSend("/topic/form/" + formId + "/report/map", "Update");
    }

    private void sendMatrixFieldMessages(Form form, MainRecord mainRecord) {
        form.getFields().stream().filter(Field::isMatrix).forEach(field -> {
            Integer fieldId = field.getId();
            simpMessagingTemplate.convertAndSend("/topic/field/" + fieldId + "/report/pivot", "Update");
            simpMessagingTemplate.convertAndSend("/topic/field/" + fieldId + "/report/evolution", "Update");
            simpMessagingTemplate.convertAndSend("/topic/field/" + fieldId + "/report/gallery", "Update");
            simpMessagingTemplate.convertAndSend("/topic/field/" + fieldId + "/report/analysis", "Update");
            simpMessagingTemplate.convertAndSend("/topic/field/" + fieldId +"/report/table", "Update");
        });
    }
}
