package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.EmailService;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Emails a user whenever his roles are modified.
 */
public class RolesModifiedEmailer extends Emailer<UserEvent> {

    public RolesModifiedEmailer(EmailService emailService, TemplateEngine templateEngine) {
        super(emailService, templateEngine);
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.MODIFY_ROLES.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(UserEvent userEvent) {
        User user = userEvent.getUser();
        User affectedUser = userEvent.getSubject();
        boolean affectedUserHasRoles = affectedUser.getRoles() != null && !affectedUser.getRoles().isEmpty();
        String applicationUrl = applicationUrl() + "/user/view";
        Context context = new Context();
        context.setVariable("user", affectedUser);
        context.setVariable("tenant", user);
        context.setVariable("applicationUrl", applicationUrl);
        String subject;
        if (!affectedUserHasRoles) {
            subject = "Your permissions have been revoked";
        } else {
            subject = "Your permissions have been changed";
        }
        String body = templateEngine.process(templateName(userEvent.getType()), context);
        emailService.sendEmail(affectedUser, subject, body, userEvent);
    }
}
