package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.EmailService;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Emails the @{@link User} the first notification when their non-FREE credits expiry is imminent.
 */
public class ExpirationNoticeEmailer extends Emailer<UserEvent> {

    public ExpirationNoticeEmailer(EmailService emailService, TemplateEngine templateEngine) {
        super(emailService, templateEngine);
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.EXPIRATION_NOTICE.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(UserEvent userEvent) {
        User user = userEvent.getUser();
        String applicationUrl = applicationUrl() + "/user/view";
        Integer creditBalance = (Integer) userEvent.getProperty(UserEvent.CREDIT_BALANCE);
        Integer daysToExpiry = (Integer) userEvent.getProperty(UserEvent.DAYS_TO_EXPIRY);
        Integer dormantDays = (Integer) userEvent.getProperty(UserEvent.DORMANT_DAYS);
        Integer expirationPeriod = (Integer) userEvent.getProperty(UserEvent.EXPIRY_PERIOD);
        Date date = (Date) userEvent.getProperty(UserEvent.EXPIRY_DATE);
        String expiryDate = new SimpleDateFormat("MMM d, yyyy").format(date);
        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("creditBalance", creditBalance);
        context.setVariable("daysToExpiry", daysToExpiry);
        context.setVariable("dormantDays", dormantDays);
        context.setVariable("expirationPeriod", expirationPeriod);
        context.setVariable("expiryDate", expiryDate);
        context.setVariable("expiryNotificationNo", userEvent.getProperty(UserEvent.EXPIRY_NOTIFICATION_NO));
        context.setVariable("applicationUrl", applicationUrl);
        String subject = "Your " + creditBalance + " credits are due for expiry in " + daysToExpiry + " days!";
        String body = templateEngine.process(templateName(userEvent.getType()), context);
        emailService.sendEmail(user, subject, body, userEvent);
    }
}
