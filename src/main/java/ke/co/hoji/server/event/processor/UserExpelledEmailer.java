package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.EmailService;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Emails the user whenever they are expeled from a tenant account.
 */
public class UserExpelledEmailer extends Emailer<UserEvent> {

    /**
     * The Hoji mobile app download link.
     */
    private static final String HOJI_DOWNLOAD_URL = "https://play.google.com/store/apps/details?id=ke.co.hoji.android&hl=en";

    /**
     * The ID of the Sample Form.
     */
    private static final int SAMPLE_FORM_ID = 278;

    public UserExpelledEmailer(EmailService emailService, TemplateEngine templateEngine) {
        super(emailService, templateEngine);
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.EXPEL_USER.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(UserEvent userEvent) {
        User affectedUser = userEvent.getSubject();
        User user = userEvent.getUser();
        Context context = new Context();
        context.setVariable("user", affectedUser);
        context.setVariable("tenant", user);
        String subject = "You are no longer collaborating with " + user.getFullName();
        String body = templateEngine.process(templateName(userEvent.getType()), context);
        emailService.sendEmail(affectedUser, subject, body, userEvent);
    }
}
