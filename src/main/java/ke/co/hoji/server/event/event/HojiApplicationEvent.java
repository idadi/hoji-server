package ke.co.hoji.server.event.event;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.EventSubject;
import ke.co.hoji.server.event.model.EventOriginator;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import org.springframework.context.ApplicationEvent;

import java.util.Date;
import java.util.LinkedHashMap;

/**
 * The super class for all our {@link ApplicationEvent}s.
 */

public abstract class HojiApplicationEvent<S extends EventSubject> extends ApplicationEvent {

    /**
     * The unique id of this event.
     */
    protected Integer id;

    /**
     * The current user at the time of the occurrence of this event.
     */
    protected final User user;

    /**
     * An {@link EventType} of this event.
     */
    protected final EventType type;

    /**
     * The {@link EventOriginator} of this event.
     */
    protected final EventOriginator eventOriginator;

    /**
     * The date of this event.
     */
    protected final Date date;

    /**
     * The subject of this event i.e. What object is this event about?
     */
    protected final S subject;

    /**
     * The {@link EventResult} of this event.
     */
    protected EventResult result;

    /**
     * If this event was not successful i.e. {@link EventResult#SUCCESS}, why was that?
     */
    protected String failureMessage;

    /**
     * A map of arbitrary properties constituting whatever additional information an event may need that is not
     * covered by the {@link EventSubject#descriptiveProperties()} of the subject.
     */
    private final LinkedHashMap<String, Object> additionalProperties = new LinkedHashMap<>();

    /**
     * Initializes a {@link HojiApplicationEvent}. The originator of this event is set to user and the date to now.
     *
     * @param eventSource the EventSource.
     * @param user        the User
     * @param subject     the subject of this event.
     * @param type        the type of this event.
     */
    public HojiApplicationEvent(EventSource eventSource, User user, S subject, EventType type) {
        this(eventSource, user, EventOriginator.USER, subject, type);
    }

    /**
     * Initializes a {@link HojiApplicationEvent}. The date of this event is set to now.
     *
     * @param eventSource     the EventSource.
     * @param user            the User
     * @param eventOriginator the EventOriginator.
     * @param subject         the subject of this event.
     * @param type            the type of this event.
     */
    public HojiApplicationEvent(EventSource eventSource, User user, EventOriginator eventOriginator, S subject, EventType type) {
        super(eventSource);
        this.user = user;
        this.eventOriginator = eventOriginator;
        this.subject = subject;
        this.type = type;
        this.result = EventResult.SUCCESS;
        this.date = new Date();
        if (!type.getSubjectClass().isInstance(subject)) {
            throw new IllegalArgumentException("The EventType specified is incompatible with the Subject.");
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EventType getType() {
        return type;
    }

    public void setResult(EventResult result) {
        this.result = result;
    }

    public EventResult getResult() {
        return result;
    }

    public EventOriginator getEventOriginator() {
        return eventOriginator;
    }

    public Date getDate() {
        return date;
    }

    public S getSubject() {
        return subject;
    }

    /**
     * @return the {@link EventSource} of this event.
     */
    public final EventSource getEventSource() {
        return (EventSource) getSource();
    }

    public User getUser() {
        return user;
    }

    /**
     * @return the name of this event.
     */
    public String name() {
        return this.getClass().getSimpleName();
    }

    /**
     * A detailed description of this event.
     * A detailed description of this event.
     *
     * @return the description of this event.
     */
    public final String description() {
        String description = Utils.mapToString(subject.descriptiveProperties());
        if (!additionalProperties.isEmpty()) {
            description += Utils.ENTRY_SEPARATOR;
            description += Utils.mapToString(additionalProperties);
        }
        return description;
    }

    /**
     * @return The name of the {@link #subject} of this event. By default, this method returns the simple class name of the
     * {@link #subject}.
     */
    public String subjectName() {
        return subject.getClass().getSimpleName();
    }

    /**
     * @return The id of the {@link #subject} of this event.
     */
    public abstract String subjectId();

    public String getFailureMessage() {
        return failureMessage;
    }

    public void setFailureMessage(String failureMessage) {
        this.failureMessage = failureMessage;
    }

    /**
     * Get an arbitrary property from this event.
     *
     * @param key, the key by which to get the property.
     * @return the value of the property associated with the key.
     */
    public Object getProperty(String key) {
        return additionalProperties.get(key);
    }

    /**
     * Set an arbitrary property for this event.
     *
     * @param key,   the key by which to identify the property.
     * @param value, the value of the property.
     * @return the previous value associated with key, or null if there was no mapping for key
     */
    public Object putProperty(String key, Object value) {
        return additionalProperties.put(key, value);
    }
}
