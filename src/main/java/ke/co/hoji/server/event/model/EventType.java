package ke.co.hoji.server.event.model;

import ke.co.hoji.core.data.ConfigObject;
import ke.co.hoji.core.data.Enablable;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.event.event.HojiApplicationEvent;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.WebHook;

/**
 * The various type of {@link HojiApplicationEvent}s that we track.
 */
public enum EventType {

    /**
     * A user signs up.
     */
    SIGN_UP(User.class, true, true),

    /**
     * Welcome a new user.
     */
    WELCOME(User.class, true, true),

    /**
     * A user signs in.
     */
    SIGN_IN(User.class),

    /**
     * A user signs out.
     */
    SIGN_OUT(User.class),

    /**
     * A user activates access to public projects.
     */
    ACTIVATE_PUBLIC_PROJECT_ACCESS(User.class, true, true),

    /**
     * A user requests to access another user's account.
     */
    REQUEST_ACCESS(User.class, true),

    /**
     * A user changes his email.
     */
    CHANGE_EMAIL(User.class, true),

    /**
     * A user changes his password.
     */
    CHANGE_PASSWORD(User.class),

    /**
     * A user verifies his account.
     */
    VERIFY(User.class),

    /**
     * A user gets suspended for not verifying his email on time.
     */
    SUSPEND(User.class, true),

    /**
     * A users says he forgot his password.
     */
    FORGOT_PASSWORD(User.class, true),

    /**
     * A users resets his password.
     */
    RESET_PASSWORD(User.class),

    /**
     * A user requests a new account verification token.
     */
    REQUEST_TOKEN(User.class, true),

    /**
     * A user changes to a different tenant account.
     */
    CHANGE_TENANT(User.class, true),

    /**
     * A user views his account page.
     */
    VIEW_USER(User.class),

    /**
     * A user updates his account.
     */
    MODIFY_USER(User.class),

    /**
     * A user is expelled from their current tenant account.
     */
    EXPEL_USER(User.class, true),

    /**
     * A user updates details of users under his account.
     */
    MODIFY_USERS(User.class),

    /**
     * A user modified the roles of an individual user under his account.
     */
    MODIFY_ROLES(User.class, true),

    /**
     * A user loads cash onto his account.
     */
    LOAD_CASH(User.class),

    /**
     * A user buys credits.
     */
    BUY_CREDITS(User.class),

    /**
     * Solicit a review from an officer user.
     */
    SOLICIT_OFFICER_REVIEW(User.class, true),

    /**
     * Solicit a review from an enumerator user.
     */
    SOLICIT_ENUMERATOR_REVIEW(User.class, true),

    /**
     * A user opens the home/projects/surveys page.
     */
    PROJECTS(User.class),

    /**
     * A user opens the web-hooks list page.
     */
    WEB_HOOKS(User.class),

    /**
     * A user's non-FREE credits are expired.
     */
    CREDITS_EXPIRED(User.class, true),

    /**
     * A user's FREE credits are expired.
     */
    FREE_CREDITS_EXPIRED(User.class, true),

    /**
     * Notification that a user's non-FREE credits are due for expiry.
     */
    EXPIRATION_NOTICE(User.class, true),

    /**
     * Notification that a user's FREE credits are due for expiry.
     */
    FREE_EXPIRATION_NOTICE(User.class, true),

    /**
     * An object is created.
     */
    CREATE(ConfigObject.class),

    /**
     * An object is modified.
     */
    MODIFY(ConfigObject.class),

    /**
     * Objects are re-ordered.
     */
    SORT(ConfigObject.class),

    /**
     * Objects are edited in bulk.
     */
    BULK_EDIT(ConfigObject.class),

    /**
     * An object is enabled.
     */
    ENABLE(Enablable.class),

    /**
     * An object is disabled.
     */
    DISABLE(Enablable.class),

    /**
     * An object is deleted.
     */
    DELETE(ConfigObject.class),

    /**
     * An object is viewed.
     */
    VIEW(ConfigObject.class),

    /**
     * Object are listed.
     */
    LIST(ConfigObject.class),

    /**
     * An survey is published.
     */
    PUBLISH(Survey.class),

    /**
     * A survey is cleared of all data.
     */
    CLEAR(Survey.class),

    /**
     * A {@link MainRecord} is marked as test data.
     */
    PRODUCTION_TO_TEST(MainRecord.class),

    /**
     * A {@link MainRecord} is marked as production data.
     */
    TEST_TO_PRODUCTION(MainRecord.class),

    /**
     * Survey is downloaded to mobile client.
     */
    DOWNLOAD(Survey.class),

    /**
     * Fields in Form are auto-numbered.
     */
    AUTO_NUMBER(Form.class),


    /**
     * Form major version incremented.
     */
    INCREMENT_MAJOR_VERSION(Form.class),


    /**
     * Form minor version incremented.
     */
    INCREMENT_MINOR_VERSION(Form.class),

    /**
     * Analysis output is ran.
     */
    ANALYSIS(FieldParent.class),

    /**
     * Pivot output is ran.
     */
    PIVOT(FieldParent.class),

    /**
     * Table output is ran.
     */
    TABLE(FieldParent.class),

    /**
     * CSV output is ran.
     */
    CSV(FieldParent.class),

    /**
     * Map output is ran.
     */
    MAP(Form.class),

    /**
     * Monitor output is ran.
     */
    MONITOR(Form.class),

    /**
     * Record output is ran i.e. individual record is opened.
     */
    MAIN_RECORD_VIEW(MainRecord.class),

    /**
     * Record is deleted.
     */
    MAIN_RECORD_DELETE(MainRecord.class),

    /**
     * Records for a {@link Form} requested via API.
     */
    API_READ(Form.class),

    /**
     * A record is imported.
     */
    IMPORT(MainRecord.class),

    /**
     * A web-hook is activated, usually after being created or edited.
     */
    WEB_HOOK_ACTIVATE(WebHook.class),

    /**
     * A web-hook client claims messages they missed.
     */
    MISSED_MESSAGES_CLAIMED(WebHook.class),

    /**
     * A web-hook client deletes messages they missed.
     */
    MISSED_MESSAGES_DELETED(WebHook.class),

    /**
     * A main record is created
     */
    MAIN_RECORD_CREATE(MainRecord.class, true, true),

    /**
     * A main record is modified
     */
    MAIN_RECORD_MODIFY(MainRecord.class);

    /**
     * The class of objects that can be subjects for this event type.
     */
    private final Class subjectClass;

    /**
     * True if this EventType may cause an email to be sent and false otherwise.
     */
    private final boolean emailable;

    /**
     * True if this EventType should only result in emailing a user only once in a life time and false otherwise
     */
    private final boolean onceInALifeTime;

    /**
     * Create an event type that supports the specified event subject and both emailable and onceInALifeTime set to
     * false.
     *
     * @param subjectClass the class of objects supported by this EventType
     */
    EventType(Class subjectClass) {
        this(subjectClass, false, false);
    }

    /**
     * Create an event type that supports the specified event subject and emailable and onceInALifeTime set to
     * false
     *
     * @param subjectClass the class of objects supported by this EventType
     * @param emailable    whether or not this EventType may cause an email to be sent
     */
    EventType(Class subjectClass, boolean emailable) {
        this.subjectClass = subjectClass;
        this.emailable = emailable;
        this.onceInALifeTime = false;
    }

    /**
     * Create an event type that supports the specified event subject and emailable attribute.
     *
     * @param subjectClass    the class of objects supported by this EventType
     * @param emailable       whether or not this EventType may cause an email to be sent
     * @param onceInALifeTime whether or not this EventType should only send an email once in a life time.
     */
    EventType(Class subjectClass, boolean emailable, boolean onceInALifeTime) {
        this.subjectClass = subjectClass;
        this.emailable = emailable;
        this.onceInALifeTime = onceInALifeTime;
    }

    public Class getSubjectClass() {
        return subjectClass;
    }

    public boolean isEmailable() {
        return emailable;
    }

    public boolean isOnceInALifeTime() {
        return onceInALifeTime;
    }
}
