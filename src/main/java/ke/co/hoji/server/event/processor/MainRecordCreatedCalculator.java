package ke.co.hoji.server.event.processor;

import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.server.calculation.Calculator;
import ke.co.hoji.server.event.event.MainRecordEvent;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.event.model.EventResult;

import java.util.stream.Stream;

/**
 * Executes and caches {@link MainRecord} calculations when a MainRecord is created or modified..
 */
public class MainRecordCreatedCalculator implements EventProcessor<MainRecordEvent> {

    private final ConfigService configService;
    private final Calculator calculator;

    public MainRecordCreatedCalculator(ConfigService configService, Calculator calculator) {
        this.configService = configService;
        this.calculator = calculator;
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        boolean createdOrModified = EventType.MAIN_RECORD_CREATE.equals(eventType)
                || EventType.MAIN_RECORD_MODIFY.equals(eventType);
        return createdOrModified && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(MainRecordEvent mainRecordEvent) {
        MainRecord mainRecord = mainRecordEvent.getSubject();
        Form form = configService.getForm(mainRecord.getFormId());
        form.getFields()
                .stream()
                .flatMap(field -> {
                    if (field.isMatrix()) {
                        return field.getChildren().stream();
                    }
                    return Stream.of(field);
                })
                .filter(field -> field.hasScriptFunction(Field.ScriptFunctions.CALCULATE))
                .forEach(field -> {
                    if (field.getParent() != null) {
                        mainRecord.getMatrixRecords().stream().forEach(matrixRecord -> calculator.calculate(mainRecord, matrixRecord.getUuid(), field, true));
                    } else {
                        calculator.calculate(mainRecord, mainRecord.getUuid(), field, true);
                    }
                });
    }
}
