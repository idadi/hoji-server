package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.VerificationToken;
import ke.co.hoji.server.service.EmailService;
import ke.co.hoji.server.service.UserService;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Emails the user a password reset token.
 */
public class PasswordResetTokenEmailer extends VerificationTokenEmailer<UserEvent> {

    public PasswordResetTokenEmailer(
        EmailService emailService,
        TemplateEngine templateEngine,
        UserService userService
    ) {
        super(emailService, templateEngine, userService);
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.FORGOT_PASSWORD.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(UserEvent userEvent) {
        User user = userEvent.getUser();
        String applicationUrl = applicationUrl() + "/resetPassword?token="
            + verificationToken(VerificationToken.Type.PASSWORD_RESET, user);
        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("applicationUrl", applicationUrl);
        String subject = "Password reset request";
        String body = templateEngine.process(templateName(userEvent.getType()), context);
        emailService.sendEmail(user, subject, body, userEvent);
    }
}
