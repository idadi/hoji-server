package ke.co.hoji.server.event.processor;

import ke.co.hoji.core.data.Constants.Server;
import ke.co.hoji.server.event.event.HojiApplicationEvent;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.service.EmailService;
import org.thymeleaf.TemplateEngine;

/**
 * A convenience super class for any {@link EventProcessor}s that need to send an email.
 */
public abstract class Emailer<E extends HojiApplicationEvent> implements EventProcessor<E> {

    /**
     * The {@link EmailService} via which to send emails.
     */
    protected final EmailService emailService;

    /**
     * The @{@link TemplateEngine} with which to prepare email body templates.
     */
    protected final TemplateEngine templateEngine;

    public Emailer(EmailService emailService, TemplateEngine templateEngine) {
        this.emailService = emailService;
        this.templateEngine = templateEngine;
    }

    /**
     * @return the URL of the web application. Used to prepare various hyperlinks for emailing.
     */
    protected String applicationUrl() {
        return emailService.getServerUrl();
    }

    /**
     * Gets the name of the email template for the given {@link EventType}.
     *
     * @param eventType the {@link EventType} for which to get the email template name.
     * @return the template name.
     */
    protected String templateName(EventType eventType) {
        return "db:" + eventType.toString();
    }
}
