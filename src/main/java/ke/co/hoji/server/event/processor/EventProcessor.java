package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.HojiApplicationEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;

/**
 * A mechanism for processing a {@link HojiApplicationEvent}. Each EventProcessor may support
 * {@link HojiApplicationEvent}s of one or more @{@link EventType}s.
 */
public interface EventProcessor<E extends HojiApplicationEvent> {

    /**
     * Checks whether this {@link EventProcessor} supports the given {@link EventType}.
     *
     * @param eventType    the {@link EventType} for which to check support.
     * @param eventResult  the result type of the event
     * @return true if this EventProcessor supports the given {@link EventType} and false otherwise.
     */
    boolean supports(EventType eventType, EventResult eventResult);

    /**
     * Processes a {@link HojiApplicationEvent}. Implementations are free to do whatever they want here.
     *
     * @param hojiApplicationEvent the {@link HojiApplicationEvent} to be processed.
     */
    void process(E hojiApplicationEvent);
}
