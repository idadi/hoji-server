package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.EmailService;
import ke.co.hoji.server.service.EmailService.SourceEmail;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Emails the {@link User} upon activating public project access.
 */
public class PublicProjectAccessActivatedEmailer extends Emailer<UserEvent> {

    /**
     * The ID of the Sample Survey.
     */
    private static final int SAMPLE_SURVEY_ID = 302;

    /**
     * The ID of the Sample Form.
     */
    private static final int SAMPLE_FORM_ID = 278;

    public PublicProjectAccessActivatedEmailer(EmailService emailService, TemplateEngine templateEngine) {
        super(emailService, templateEngine);
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.ACTIVATE_PUBLIC_PROJECT_ACCESS.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(UserEvent userEvent) {
        sendEmail(userEvent);
    }

    private void sendEmail(UserEvent userEvent) {
        Context context = new Context();
        User user = userEvent.getUser();
        String applicationUrl = applicationUrl();
        String outputUrl = "project/" + SAMPLE_SURVEY_ID + "/form/" + SAMPLE_FORM_ID + "/report/";
        context.setVariable("user", user);
        context.setVariable("analysisUrl", applicationUrl + outputUrl + "analysis");
        String subject = "Yay! Your sample project is ready";
        String body = templateEngine.process(templateName(userEvent.getType()), context);
        emailService.sendEmail(user, SourceEmail.CUSTOMER_SUCCESS, subject, body, userEvent);
    }
}
