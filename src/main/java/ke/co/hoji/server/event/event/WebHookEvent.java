package ke.co.hoji.server.event.event;

import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.ApiRecord;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.WebHook;

/**
 * An event for which the {@link HojiApplicationEvent#subject} is a {@link WebHook}.
 */
public class WebHookEvent extends HojiApplicationEvent<WebHook> {

    /**
     * The number of {@link ApiRecord}s for this Web-hook.
     */
    public static final String NO_OF_API_RECORDS = "Records";

    public WebHookEvent(EventSource eventSource, User user, WebHook subject, EventType eventType) {
        super(eventSource, user, subject, eventType);
    }

    @Override
    public String subjectId() {
        return String.valueOf(subject.getId());
    }
}
