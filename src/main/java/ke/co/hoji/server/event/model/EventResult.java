package ke.co.hoji.server.event.model;

import ke.co.hoji.server.event.event.HojiApplicationEvent;

/**
 * The possible results of an {@link HojiApplicationEvent}.
 */
public enum EventResult {

    /**
     * The event succeeded.
     */
    SUCCESS,

    /**
     * The event failed normally e.g. insufficient credits balance.
     */
    FAILURE,

    /**
     * The event failed because we ran into problems i.e. exceptional circumstances e.g. application error.
     */
    EXCEPTION;
}
