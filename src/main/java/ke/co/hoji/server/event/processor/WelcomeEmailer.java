package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.CreditBundle;
import ke.co.hoji.server.model.Transaction;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.BundlePurchaseService;
import ke.co.hoji.server.service.EmailService;
import ke.co.hoji.server.service.EmailService.SourceEmail;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Welcomes the @{@link User} upon sign-up. As currently implemented, this class will allocate the user some free
 * credits and then send them a welcome email.
 */
public class WelcomeEmailer extends Emailer<UserEvent> {

    /**
     * The number of free credits to allocate a user when welcoming them upon sign-up.
     */
    private static final Integer FREE_SIGN_UP_CREDITS = 50;

    /**
     * The expiry period of {@link #FREE_SIGN_UP_CREDITS} in days.
     */
    private static final Integer FREE_SIGN_UP_CREDITS_EXPIRY = 14;

    /**
     * The Hoji mobile app download link.
     */
    private static final String HOJI_DOWNLOAD_URL = "https://play.google.com/store/apps/details?id=ke.co.hoji.android&hl=en";

    /**
     * The ID of the Sample Survey.
     */
    private static final int SAMPLE_SURVEY_ID = 302;

    /**
     * The ID of the Sample Form.
     */
    private static final int SAMPLE_FORM_ID = 278;

    private final BundlePurchaseService bundlePurchaseService;

    public WelcomeEmailer(EmailService emailService, TemplateEngine templateEngine, BundlePurchaseService bundlePurchaseService) {
        super(emailService, templateEngine);
        this.bundlePurchaseService = bundlePurchaseService;
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.WELCOME.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(UserEvent userEvent) {
        CreditBundle creditBundle = new CreditBundle();
        creditBundle.setCredits(FREE_SIGN_UP_CREDITS);
        creditBundle.setExpiry(FREE_SIGN_UP_CREDITS_EXPIRY);
        Integer expiry = allocateWelcomeCreditBundle(userEvent, creditBundle);
        sendWelcomeEmail(userEvent, creditBundle.getCredits(), expiry);
    }

    private Integer allocateWelcomeCreditBundle(UserEvent userEvent, CreditBundle creditBundle) {
        return bundlePurchaseService.allocateFreeCreditBundle(userEvent.getUser(), creditBundle);
    }

    private void sendWelcomeEmail(UserEvent userEvent, Integer freeCredits, Integer expiry) {
        Context context = new Context();
        User user = userEvent.getUser();
        String applicationUrl = applicationUrl();
        String outputUrl = "project/" + SAMPLE_SURVEY_ID + "/form/" + SAMPLE_FORM_ID + "/report/";
        context.setVariable("user", user);
        context.setVariable("freeCredits", freeCredits);
        context.setVariable("expiry", expiry);
        context.setVariable("downloadUrl", HOJI_DOWNLOAD_URL);
        context.setVariable("viaMobile", userEvent.getEventSource().equals(EventSource.MOBILE));
        context.setVariable("dataTableUrl", applicationUrl + outputUrl + "table");
        context.setVariable("analysisUrl", applicationUrl + outputUrl + "analysis");
        context.setVariable("dashboardUrl", applicationUrl + outputUrl + "dashboard");
        context.setVariable("pivotTableUrl", applicationUrl + outputUrl + "pivot");
        context.setVariable("mapUrl", applicationUrl + outputUrl + "map");
        context.setVariable("monitorUrl", applicationUrl + outputUrl + "monitor");
        String subject = "Welcome to Hoji!";
        String body = templateEngine.process(templateName(userEvent.getType()), context);
        emailService.sendEmail(user, SourceEmail.CEO, subject, body, userEvent);
    }
}
