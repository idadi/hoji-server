package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.HojiApplicationEvent;

import java.util.ArrayList;
import java.util.List;

public class EventProcessorManager {

    private List<EventProcessor> eventProcessors = new ArrayList<>();

    public boolean registerEventProcessor(EventProcessor eventProcessor) {
        return eventProcessors.add(eventProcessor);
    }

    public void process(HojiApplicationEvent hojiApplicationEvent) {
        for (EventProcessor eventProcessor : eventProcessors) {
            if (eventProcessor.supports(hojiApplicationEvent.getType(), hojiApplicationEvent.getResult())) {
                eventProcessor.process(hojiApplicationEvent);
            }
        }
    }
}
