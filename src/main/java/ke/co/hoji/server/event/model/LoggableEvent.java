package ke.co.hoji.server.event.model;

import ke.co.hoji.core.data.HojiObject;
import ke.co.hoji.server.event.event.HojiApplicationEvent;
import ke.co.hoji.server.model.User;

import java.util.Date;

/**
 * Essentially a simplified wrapper for a {@link HojiApplicationEvent} for the purposes of logging events to the
 * database.
 */
public final class LoggableEvent implements HojiObject {

    /**
     * the unique id of the event.
     */
    private Long id;

    /**
     * The name of this event.
     */
    private final String name;

    /**
     * The date of the event.
     */
    private final Date date;

    /**
     * The {@link User} associated with this event.
     */
    private final User user;

    /**
     * The tenant {@link User} under whom {@link #user} was operating when this event occurred.
     */
    private final User tenant;

    /**
     * The @{@link EventType} of this event.
     */
    private final EventType type;

    /**
     * The name of the {@link HojiApplicationEvent#subject} associated with this event.
     */
    private final String subjectName;

    /**
     * The unique id of the {@link HojiApplicationEvent#subject} associated with this event.
     */
    private final String subjectId;

    /**
     * A detailed description of this event.
     */
    private final String description;

    /**
     * The {@link EventResult} of this event.
     */
    private final EventResult result;

    /**
     * The @{@link EventSource} of this event.
     */
    private final EventSource source;

    /**
     * The @{@link EventOriginator} of this event.
     */
    private final EventOriginator originator;

    /**
     * If this event was not successful, why was that?
     */
    private final String failureMessage;

    /**
     * Create a new LoggableEvent.
     * @param name, {@link #name}
     * @param date, {@link #date}
     * @param user, {@link #user}
     * @param tenant, {@link #tenant}
     * @param type, {@link #type}
     * @param subjectName, {@link #subjectName}
     * @param subjectId, {@link #subjectId}
     * @param description, {@link #description}
     * @param result, {@link #result}
     * @param source, {@link #source}
     * @param originator, {@link #originator}
     * @param failureMessage, {@link #failureMessage}
     */
    public LoggableEvent(
            String name,
            Date date,
            User user,
            User tenant,
            EventType type,
            String subjectName,
            String subjectId,
            String description,
            EventResult result,
            EventSource source,
            EventOriginator originator,
            String failureMessage
    ) {
        this(null, name, date, user, tenant, type, subjectName, subjectId, description, result, source, originator, failureMessage);
    }

    /**
     * Create a new LoggableEvent.
     * @param id, {@link #id}
     * @param name, {@link #name}
     * @param date, {@link #date}
     * @param user, {@link #user}
     * @param tenant, {@link #tenant}
     * @param type, {@link #type}
     * @param subjectName, {@link #subjectName}
     * @param subjectId, {@link #subjectId}
     * @param description, {@link #description}
     * @param result, {@link #result}
     * @param source, {@link #source}
     * @param originator, {@link #originator}
     * @param failureMessage, {@link #failureMessage}
     */
    public LoggableEvent(
            Long id,
            String name,
            Date date,
            User user,
            User tenant,
            EventType type,
            String subjectName,
            String subjectId,
            String description,
            EventResult result,
            EventSource source,
            EventOriginator originator,
            String failureMessage
    ) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.user = user;
        this.tenant = tenant;
        this.type = type;
        this.subjectName = subjectName;
        this.subjectId = subjectId;
        this.description = description;
        this.result = result;
        this.source = source;
        this.originator = originator;
        this.failureMessage = failureMessage;
    }

    /**
     * Create a new LoggableEvent from a {@link HojiApplicationEvent}.
     *
     * @param hojiApplicationEvent the HojiApplicationEvent.
     * @param user                 the {@link User} associated with this event.
     * @param tenant               the tenant {@link User} under whom {@link #user} was operating when this event occurred.
     * @return {@link LoggableEvent} based on details provided
     */
    public static final LoggableEvent createFromHojiApplicationEvent(HojiApplicationEvent hojiApplicationEvent, User user, User tenant) {
        LoggableEvent loggableEvent = new LoggableEvent(
                hojiApplicationEvent.name(),
                hojiApplicationEvent.getDate(),
                user,
                tenant,
                hojiApplicationEvent.getType(),
                hojiApplicationEvent.subjectName(),
                hojiApplicationEvent.subjectId(),
                hojiApplicationEvent.description(),
                hojiApplicationEvent.getResult(),
                hojiApplicationEvent.getEventSource(),
                hojiApplicationEvent.getEventOriginator(),
                hojiApplicationEvent.getFailureMessage()
        );
        return loggableEvent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public User getUser() {
        return user;
    }

    public User getTenant() {
        return tenant;
    }

    public EventType getType() {
        return type;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public EventResult getResult() {
        return result;
    }

    public EventSource getSource() {
        return source;
    }

    public EventOriginator getOriginator() {
        return originator;
    }

    @Override
    public Object getUniqueId() {
        return getId();
    }

    public String getFailureMessage() {
        return failureMessage;
    }
}
