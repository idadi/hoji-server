package ke.co.hoji.server.event.event;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.ApiRecord;
import ke.co.hoji.server.model.User;

/**
 * An event for which the {@link HojiApplicationEvent#subject} is a {@link Form}.
 */
public class FormEvent extends HojiApplicationEvent<Form> {

    /**
     * The number of {@link Field}s in this {@link Form}.
     */
    public static final String NO_OF_FIELDS = "Fields";

    /**
     * The caption of a record associated with this {@link Form}.
     */
    public static final String RECORD_CAPTION = "Record Caption";

    /**
     * The UUID of a record associated with this {@link Form}.
     */
    public static final String RECORD_UUID = "Record UUID";

    /**
     * The page number in an API request for {@link ApiRecord}s of a given {@link Form}.
     */
    public static final String PAGE = "Page";

    /**
     * The page size in an API request for {@link ApiRecord}s of a given {@link Form}.
     */
    public static final String PAGE_SIZE = "Page Size";

    /**
     * The number of {@link ApiRecord}s in an API request for a given {@link Form}.
     */
    public static final String RECORD_COUNT = "Records";

    /**
     * An event for which the {@link HojiApplicationEvent#subject} is a {@link Form}.
     * @param eventSource, {@link #source}
     * @param user, {@link #user}
     * @param subject, {@link #subject}
     * @param type, {@link #type}
     */
    public FormEvent(EventSource eventSource, User user, Form subject, EventType type) {
        super(eventSource, user, subject, type);
    }

    @Override
    public String subjectId() {
        return String.valueOf(subject.getId());
    }
}
