package ke.co.hoji.server.event.event;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;

/**
 * An event for which the {@link HojiApplicationEvent#subject} is a {@link Field}.
 */
public class FieldEvent extends HojiApplicationEvent<Field> {

    public static final String RECORD_CAPTION = "Record Caption";
    public static final String RECORD_UUID = "Record UUID";

    public FieldEvent(EventSource eventSource, User user, Field subject, EventType type) {
        super(eventSource, user, subject, type);
    }

    @Override
    public String subjectId() {
        return String.valueOf(subject.getId());
    }
}
