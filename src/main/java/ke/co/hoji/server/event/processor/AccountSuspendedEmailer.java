package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.VerificationToken;
import ke.co.hoji.server.service.EmailService;
import ke.co.hoji.server.service.UserService;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Emails the user when their account is suspended for not verifying their email on time.
 */
public class AccountSuspendedEmailer extends VerificationTokenEmailer<UserEvent> {

    public AccountSuspendedEmailer(EmailService emailService, TemplateEngine templateEngine, UserService userService) {
        super(emailService, templateEngine, userService);
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.SUSPEND.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(UserEvent userEvent) {
        User user = userEvent.getUser();
        user.getRoles().isEmpty();
        String applicationUrl = applicationUrl() + "/confirm?token="
            + verificationToken(VerificationToken.Type.EMAIL_VERIFICATION, user);
        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("applicationUrl", applicationUrl);
        String subject = "Action required: Please verify your email";
        String body = templateEngine.process(templateName(userEvent.getType()), context);
        emailService.sendEmail(user, subject, body, userEvent);
    }
}
