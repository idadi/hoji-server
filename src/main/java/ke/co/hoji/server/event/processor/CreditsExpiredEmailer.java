package ke.co.hoji.server.event.processor;

import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.EmailService;
import ke.co.hoji.server.service.UserService;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Emails the @{@link User} when their non-FREE credits expire.
 */
public class CreditsExpiredEmailer extends Emailer<UserEvent> {

    public CreditsExpiredEmailer(EmailService emailService, TemplateEngine templateEngine) {
        super(emailService, templateEngine);
    }

    @Override
    public boolean supports(EventType eventType, EventResult eventResult) {
        return EventType.CREDITS_EXPIRED.equals(eventType) && EventResult.SUCCESS.equals(eventResult);
    }

    @Override
    public void process(UserEvent userEvent) {
        User user = userEvent.getUser();
        String applicationUrl = applicationUrl() + "/user/view";
        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("creditBalance", userEvent.getProperty(UserEvent.CREDIT_BALANCE));
        context.setVariable("daysToExpiry", userEvent.getProperty(UserEvent.DAYS_TO_EXPIRY));
        context.setVariable("expiryDate", userEvent.getProperty(UserEvent.EXPIRY_DATE));
        context.setVariable("applicationUrl", applicationUrl);
        String subject = "Your credits have expired";
        String body = templateEngine.process(templateName(userEvent.getType()), context);
        emailService.sendEmail(user, subject, body, userEvent);
    }
}
