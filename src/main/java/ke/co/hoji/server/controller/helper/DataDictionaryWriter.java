package ke.co.hoji.server.controller.helper;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;

/**
 * Created by geoffreywasilwa on 10/07/2017.
 */
public class DataDictionaryWriter {
    final static String[] header =
            new String[] { "No.", "Field", "Choice Type", "Choice", "Code", "Scale", "Description" };
    private static final int START = 0;

    public static void write(FieldParent fieldParent, CsvPreference preference, File codeBook) throws Exception {
        ICsvListWriter listWriter = null;
        try {
            listWriter = new CsvListWriter(new FileWriter(codeBook), preference);

            final CellProcessor[] processors = getProcessors();

            // write the header
            listWriter.writeHeader(header);

            // write the codes
            for (Field field : fieldParent.getChildren()) {
                if (field.getType().isChoice()) {
                    int position = 0;
                    for (Choice choice : field.getChoiceGroup().getChoices()) {
                        List<String> values = new ArrayList<>();
                        if (START == position++) {
                            values.add(field.getName());
                            values.add(getDescription(field));
                            values.add(field.getType().getName());
                        } else {
                            values.add("");
                            values.add("");
                            values.add("");
                        }
                        values.add(choice.getName());
                        values.add(choice.getCode());
                        values.add(String.valueOf(choice.getScale()));
                        values.add(choice.getDescription());
                        listWriter.write(values, processors);
                    }
                }
            }
        } finally {
            if( listWriter != null ) {
                listWriter.close();
            }
        }
    }

    private static String getDescription(Field field) {
        return StringUtils.isNotBlank(field.getColumn()) ? field.getColumn() : field.getDescription();
    }

    private static CellProcessor[] getProcessors() {

        final CellProcessor[] processors = new CellProcessor[] {
                new Optional(), // number
                new Optional(), // field
                new Optional(), // choice type
                new NotNull(), // name
                new NotNull(), // code
                new NotNull(), // scale
                new Optional() // description
        };

        return processors;
    }
}
