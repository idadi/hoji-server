package ke.co.hoji.server.controller.helper;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import ke.co.hoji.server.controller.ChoiceGroupController;
import ke.co.hoji.server.controller.PropertyController;
import ke.co.hoji.server.exception.CsvFormatException;
import ke.co.hoji.server.exception.ChoiceGroupIntegrityViolationException;
import ke.co.hoji.server.exception.MissingChoiceParentException;
import ke.co.hoji.server.model.PostResult;

/**
 * Created by geoffreywasilwa on 27/04/2017.
 */
@ControllerAdvice(assignableTypes = { ChoiceGroupController.class, PropertyController.class })
public class ControllerExceptionHandler {

    @ResponseBody
    @ExceptionHandler(MissingChoiceParentException.class)
    public PostResult handleMissingParentException(Exception exception) {
        PostResult postResult = new PostResult();
        postResult.setSuccess(false);
        postResult.setValue(exception.getMessage());
        return postResult;
    }

    @ResponseBody
    @ExceptionHandler(CsvFormatException.class)
    public PostResult handleInvalidFormat(Exception exception) {
        PostResult postResult = new PostResult();
        postResult.setSuccess(false);
        postResult.setValue(exception.getMessage());
        return postResult;
    }

    @ResponseBody
    @ExceptionHandler(ChoiceGroupIntegrityViolationException.class)
    public PostResult handleDeleteChoiceGroupException(Exception exception) {
        PostResult postResult = new PostResult();
        postResult.setSuccess(false);
        postResult.setValue(exception.getMessage());
        return postResult;
    }

    @ResponseBody
    @ExceptionHandler(DuplicateKeyException.class)
    public PostResult handleDuplicateKeyExeption(Exception exception) {
        PostResult postResult = new PostResult();
        postResult.setSuccess(false);
        postResult.setValue(exception.getMessage());
        return postResult;
    }

}
