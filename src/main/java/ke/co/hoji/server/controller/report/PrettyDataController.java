package ke.co.hoji.server.controller.report;

import static ke.co.hoji.server.event.model.EventType.MAIN_RECORD_DELETE;
import static ke.co.hoji.server.event.model.EventType.MAIN_RECORD_VIEW;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.controller.report.model.Filter;
import ke.co.hoji.server.event.event.FieldEvent;
import ke.co.hoji.server.event.event.FormEvent;
import ke.co.hoji.server.event.event.MainRecordEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.GetResult;
import ke.co.hoji.server.model.PostError;
import ke.co.hoji.server.model.PostResult;
import ke.co.hoji.server.model.RecordOverview;
import ke.co.hoji.server.service.TenancyAccessService;

/**
 * Created by geoffreywasilwa on 03/11/2016.
 */
@Controller
public class PrettyDataController extends BaseDataController {

    @ResponseBody
    @GetMapping(value = "/api/project/{projectId}/form/{formId}/report/data")
    public GetResult getFormData(Filter filter) throws ParseException {
        Form form = (Form) filter.getFieldParent();
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.VIEW);

        GetResult getResult = new GetResult();
        getResult.setSuccess(true);
        getResult.setValue(dataService.getPrettyData(filter.toQueryFilter()));

        applicationEventPublisher.publishEvent(new FormEvent(EventSource.WEB, userService.getCurrentUser(), form, EventType.TABLE));
        return getResult;
    }

    @GetMapping(value = "/project/{projectId}/form/{formId}/record")
    public String getRecord
        (
            @PathVariable Integer formId,
            @RequestParam(value = "recordUuid") String recordUuid,
            ModelMap model
        ) {
        Form form = configService.getForm(formId);
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.VIEW);
        RecordOverview recordOverview = dataService.getRecordOverview(form, recordUuid);
        if (recordOverview == null) {
            throw new ResourceNotFoundException("No record with uuid: " + recordUuid + " found.");
        }
        model.addAttribute("form", form);
        model.addAttribute("configObject", form);
        model.addAttribute("caption", recordOverview.getCaption());
        model.addAttribute("recordResponses", recordOverview.getResponses());
        model.addAttribute("recordMetadata", recordOverview.getRecordMetadata());
        model.addAttribute("recordUuid", recordUuid);
        model.addAttribute("test", recordOverview.getRecordMetadata().isTest());
        applicationEventPublisher.publishEvent(createMainRecordEvent(recordOverview, MAIN_RECORD_VIEW));
        return "form/record-overview";
    }

    @ResponseBody
    @PostMapping(value = "/project/{projectId}/form/{formId}/record")
    public PostResult toggleRecordTestMode(
        @PathVariable Integer formId,
        @RequestParam("recordUuid") String recordUuid,
        @RequestParam("test") boolean test
    ) {
        Form form = configService.getForm(formId);
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.MODIFY);
        RecordOverview recordOverview = dataService.getRecordOverview(form, recordUuid);
        PostResult postResult = new PostResult();
        if (recordOverview == null) {
            failPost(postResult, null, "No record with uuid: " + recordUuid + " found.");
        } else {
            dataService.updateTestMode(recordUuid, test);
            postResult.setSuccess(true);
            postResult.setValue(Collections.singletonMap("test", test));

            EventType eventType;
            if (test) {
                eventType = EventType.PRODUCTION_TO_TEST;
            } else {
                eventType = EventType.TEST_TO_PRODUCTION;
            }
            applicationEventPublisher.publishEvent(createMainRecordEvent(recordOverview, eventType));
        }
        return postResult;
    }

    @ResponseBody
    @PostMapping(value = "/project/{projectId}/form/{formId}/record/delete")
    public PostResult deleteRecord(
        @PathVariable Integer formId,
        @RequestParam("recordUuid") String recordUuid,
        @RequestParam("name") String confirmationText
    ) {
        Form form = configService.getForm(formId);
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.MODIFY);
        return tryDeletingRecord(recordUuid, confirmationText, form);
    }

    private MainRecordEvent createMainRecordEvent(RecordOverview recordOverview, EventType eventType) {
        MainRecord recordForLogging = new MainRecord();
        recordForLogging.setUuid(recordOverview.getRecordMetadata().getUuid());
        recordForLogging.setCaption(recordOverview.getCaption());
        return new MainRecordEvent(EventSource.WEB, userService.getCurrentUser(), recordForLogging, eventType);
    }

    private void failPost(PostResult postResult, String parameter, String message) {
        postResult.setSuccess(false);
        List<PostError> postErrors = new ArrayList<>();
        PostError postError = new PostError();
        postError.setParameter(parameter);
        postError.setMessage(message);
        postErrors.add(postError);
        postResult.setPostErrors(postErrors);
    }

    @ResponseBody
    @GetMapping(value = "/api/project/{projectId}/form/{formId}/field/{fieldId}/report/data")
    public GetResult getFieldData(Filter filter) throws ParseException {
        Field field = (Field) filter.getFieldParent();
        tenancyAccessService.check(field.getForm().getSurvey(), TenancyAccessService.VIEW);

        GetResult getResult = new GetResult();
        getResult.setSuccess(true);
        getResult.setValue(dataService.getPrettyData(filter.toQueryFilter()));

        applicationEventPublisher.publishEvent(new FieldEvent(EventSource.WEB, userService.getCurrentUser(), field, EventType.TABLE));
        return getResult;
    }

    @GetMapping(value = "/project/{projectId}/form/{formId}/field/{fieldId}/record")
    public String getMatrixRecord
        (
            @PathVariable Integer fieldId,
            @RequestParam("recordUuid") String recordUuid,
            ModelMap model
        ) {
        Field field = configService.getField(fieldId);
        tenancyAccessService.check(field.getForm().getSurvey(), TenancyAccessService.VIEW);

        RecordOverview recordOverview = dataService.getRecordOverview(field, recordUuid);
        if (recordOverview == null) {
            throw new ResourceNotFoundException("No record with uuid: " + recordUuid + " found.");
        }
        model.addAttribute("field", field);
        model.addAttribute("caption", recordOverview.getCaption());
        model.addAttribute("recordResponses", recordOverview.getResponses());
        model.addAttribute("recordMetadata", recordOverview.getRecordMetadata());
        model.addAttribute("recordUuid", recordUuid);
        applicationEventPublisher.publishEvent(createMainRecordEvent(recordOverview, MAIN_RECORD_VIEW));
        return "field/record-overview";
    }

    @ResponseBody
    @PostMapping(value = "/project/{projectId}/form/{formId}/field/{fieldId}/record/delete")
    public PostResult deleteMatrixRecord(
        @PathVariable Integer fieldId,
        @RequestParam("recordUuid") String recordUuid,
        @RequestParam("name") String confirmationText
    ) {
        Field field = configService.getField(fieldId);
        tenancyAccessService.check(field.getForm().getSurvey(), TenancyAccessService.MODIFY);
        return tryDeletingRecord(recordUuid, confirmationText, field);
    }

    private PostResult tryDeletingRecord(String recordUuid, String confirmationText, FieldParent parent) {
        RecordOverview recordOverview = dataService.getRecordOverview(parent, recordUuid);
        PostResult postResult = new PostResult();
        if (recordOverview == null) {
            failPost(postResult, null, "No record with uuid: " + recordUuid + " found.");
        } else {
            String actualConfirmationText = StringUtils.isNotBlank(recordOverview.getCaption()) ?
                recordOverview.getCaption() : recordUuid.substring(0, 5);
            if (!actualConfirmationText.equalsIgnoreCase(confirmationText)) {
                String message = "'" + confirmationText + "' does not match the name of the record";
                failPost(postResult, "name", message);
                MainRecordEvent recordEvent = createMainRecordEvent(recordOverview, MAIN_RECORD_DELETE);
                recordEvent.setResult(EventResult.FAILURE);
                recordEvent.setFailureMessage(message);
                applicationEventPublisher.publishEvent(recordEvent);
            } else {
                dataService.deleteRecord(parent, recordUuid);
                postResult.setSuccess(true);
                applicationEventPublisher.publishEvent(createMainRecordEvent(recordOverview, MAIN_RECORD_DELETE));
            }
        }
        return postResult;
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public class ResourceNotFoundException extends RuntimeException {

        public ResourceNotFoundException(String message) {
            super(message);
        }
    }
}
