package ke.co.hoji.server.controller.report;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.controller.report.model.Filter;
import ke.co.hoji.server.event.event.ConfigEvent;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.Chart;
import ke.co.hoji.server.model.GetResult;
import ke.co.hoji.server.service.TenancyAccessService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.util.List;

/**
 * Created by geoffreywasilwa on 03/11/2016.
 */
@Controller
@RequestMapping("api/project/{projectId}/form/{formId}/")
public class MonitorController extends BaseDataController {

    @ResponseBody
    @GetMapping(value = "/report/monitor")
    public GetResult monitor(Filter filter) throws ParseException {

        Form form = (Form)filter.getFieldParent();
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.VIEW);

        List<Chart> charts = dataService.getMonitoringChartData(filter.toQueryFilter());

        GetResult getResult = new GetResult();
        getResult.setSuccess(true);
        getResult.setValue(charts);

        applicationEventPublisher.publishEvent(new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), form, EventType.MONITOR));
        return getResult;
    }
}
