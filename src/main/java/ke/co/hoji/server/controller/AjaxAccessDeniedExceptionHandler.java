package ke.co.hoji.server.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created by geoffreywasilwa on 30/01/2017.
 */
public class AjaxAccessDeniedExceptionHandler extends AccessDeniedHandlerImpl {

    @Override
    public void handle(
            HttpServletRequest request,
            HttpServletResponse response,
            AccessDeniedException accessDeniedException
    ) throws IOException, ServletException {
        if (!response.isCommitted()) {
            String contentType = request.getHeader("Content-Type");
            String accept = request.getHeader("Accept");
            if (StringUtils.isNotBlank(contentType) && StringUtils.startsWith(contentType, APPLICATION_JSON_VALUE)
                    || StringUtils.isNotBlank(accept) && StringUtils.contains(accept, APPLICATION_JSON_VALUE)) {
                response.setStatus(HttpStatus.FORBIDDEN.value());
                response.getWriter().append("{\"status\": \"" + HttpStatus.FORBIDDEN.value() + "\""
                        + ", \"message\": \"" + accessDeniedException.getMessage() + "\" }");
            } else {
                super.handle(request, response, accessDeniedException);
            }
        }
    }
}
