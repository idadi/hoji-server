package ke.co.hoji.server.controller;

import static ke.co.hoji.core.data.model.Rule.Type.BACKWARD_SKIP;
import static ke.co.hoji.core.data.model.Rule.Type.FORWARD_SKIP;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.server.controller.helper.ControllerHelper;
import ke.co.hoji.server.controller.model.RuleWrapper;
import ke.co.hoji.server.event.event.ConfigEvent;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.GetResult;
import ke.co.hoji.server.model.PostResult;
import ke.co.hoji.server.service.TenancyAccessService;
import ke.co.hoji.server.service.UserService;
import ke.co.hoji.server.validator.RuleValidator;

/**
 * Created by gitahi on 21/10/15.
 */
@Controller
@RequestMapping("/rule")
public class RuleController {

    @Autowired
    private FieldService fieldService;

    @Autowired
    private TenancyAccessService tenancyAccessService;

    @Autowired
    private ControllerHelper controllerHelper;

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @ResponseBody
    @GetMapping("{ownerId}/target")
    public GetResult getTargetOptions(@PathVariable int ownerId, @RequestParam int type) {
        Field owner = fieldService.getFieldById(ownerId);
        checkAccess(owner, TenancyAccessService.MODIFY);
        List<Field> allFields = new ArrayList<>();
        List<Field> targets = new ArrayList<>();
        if (owner.getParent() == null) {
            allFields.addAll(fieldService.getFields(owner.getForm()));
        } else {
            allFields.addAll(fieldService.getChildren(owner.getParent()));
        }
        if (type == FORWARD_SKIP) {
            targets.addAll(getFieldsAfter(owner, allFields));
        }
        if (type == BACKWARD_SKIP) {
            targets.addAll(getFieldsBefore(owner, allFields));
        }
        GetResult result = new GetResult();
        result.setSuccess(true);
        result.setValue(targets);
        return result;
    }

    @ResponseBody
    @GetMapping("{ownerId}/operator")
    public GetResult getOperatorOptions() {
        GetResult result = new GetResult();
        result.setSuccess(true);
        result.setValue(fieldService.getAllOperatorDescriptors());
        return result;
    }

    @RequestMapping(value = {"/{ownerId}/{type}/list"}, method = RequestMethod.GET)
    public String list(@PathVariable("ownerId") Integer ownerId, @PathVariable("type") Integer type, ModelMap model) {
        Field owner = fieldService.getFieldById(ownerId);
        checkAccess(owner, TenancyAccessService.VIEW);
        List<Rule> rules = fieldService.getRules(owner, type);
        List<RuleWrapper> ruleWrappers = new ArrayList<>();
        for (Rule rule : rules) {
            ruleWrappers.add(new RuleWrapper(rule));
        }
        model.addAttribute("rules", ruleWrappers);
        model.addAttribute("owner", owner);
        model.addAttribute("type", type);
        model.addAttribute("operatorDescriptors", fieldService.getAllOperatorDescriptors());
        ConfigEvent configEvent = new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), owner, EventType.LIST);
        configEvent.putProperty(ConfigEvent.NO_OF_RULES, ruleWrappers.size());
        configEvent.putProperty(ConfigEvent.RULE_TYPE, type);
        applicationEventPublisher.publishEvent(configEvent);
        return page("list");
    }

    @ResponseBody
    @RequestMapping(value = "/{fieldId}/save", method = RequestMethod.POST)
    public PostResult saveRule(@RequestBody Rule rule) {
        checkAccess(rule.getOwner(), TenancyAccessService.MODIFY);
        BindingResult result = new BindException(rule, "rule");
        if (rule.getOrdinal() == null) {
            if (rule.getType() == FORWARD_SKIP) {
                BigDecimal nextOrdinal = Utils
                        .nextOrdinal(new ArrayList<>(fieldService.getRules(rule.getOwner(), FORWARD_SKIP)));
                rule.setOrdinal(nextOrdinal);
            } else if (rule.getType() == BACKWARD_SKIP) {
                BigDecimal nextOrdinal = Utils
                        .nextOrdinal(new ArrayList<>(fieldService.getRules(rule.getOwner(), BACKWARD_SKIP)));
                rule.setOrdinal(nextOrdinal);
            }
        }
        new RuleValidator().validate(rule, result);

        PostResult postResult = new PostResult();
        if (result.hasErrors()) {
            postResult.setSuccess(false);
            postResult.setPostErrors(controllerHelper.getPostErrors(result.getFieldErrors()));
        } else {
            if (rule.getId() == null) {
                rule = fieldService.createRule(rule);
                applicationEventPublisher.publishEvent(new ConfigEvent(EventSource.WEB, userService.getCurrentUser(),
                        rule, EventType.CREATE));
            } else {
                rule = fieldService.modifyRule(rule);
                applicationEventPublisher.publishEvent(new ConfigEvent(EventSource.WEB, userService.getCurrentUser(),
                        rule, EventType.MODIFY));
            }
            postResult.setSuccess(true);
            postResult.setValue(new RuleWrapper(rule));
        }
        return postResult;
    }

    @ResponseBody
    @RequestMapping(value = "/{ownerId}/{ruleId}/delete", method = RequestMethod.POST)
    public PostResult delete(@PathVariable Integer ownerId, @PathVariable Integer ruleId) {
        Field owner = fieldService.getFieldById(ownerId);
        checkAccess(owner, TenancyAccessService.MODIFY);
        Rule rule = fieldService.getRuleById(owner, ruleId);
        fieldService.deleteRule(rule);
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        applicationEventPublisher.publishEvent(new ConfigEvent(EventSource.WEB, userService.getCurrentUser(),
                rule, EventType.DELETE));
        return postResult;
    }

    @ResponseBody
    @RequestMapping(value = "/editor/config", method = RequestMethod.GET)
    public GetResult getRuleEditor(@RequestParam Integer ownerId,
                                   @RequestParam Integer type,
                                   @RequestParam Integer ruleId) {
        Field owner = fieldService.getFieldById(ownerId);
        checkAccess(owner, TenancyAccessService.MODIFY);
        List<Field> fields;
        if (owner.getParent() == null) {
            fields = fieldService.getFields(owner.getForm());
        } else {
            fields = fieldService.getChildren(owner.getParent());
        }
        Map<String, Object> ruleEditorConfig = new HashMap<>();
        if (type == FORWARD_SKIP) {
            ruleEditorConfig.put("fields", getFieldsAfter(owner, fields));
            if (owner != null && owner.getChoiceGroup() != null) {
                ruleEditorConfig.put("choices", owner.getChoiceGroup().getChoices());
            }
        } else if (type == Rule.Type.BACKWARD_SKIP) {
            ruleEditorConfig.put("fields", getFieldsBefore(owner, fields));
            Rule rule = fieldService.getRuleById(owner, ruleId);
            if (rule != null && rule.getTarget() != null && rule.getTarget().getChoiceGroup() != null) {
                ruleEditorConfig.put("choices", rule.getTarget().getChoiceGroup().getChoices());
            }
        }
        GetResult getResult = new GetResult();
        getResult.setSuccess(true);
        getResult.setValue(ruleEditorConfig);
        return getResult;
    }

    @ResponseBody
    @RequestMapping(value = "/editor/choiceGroup", method = RequestMethod.GET)
    public GetResult getChoiceGroup(@RequestParam Integer targetId) {
        Field target = fieldService.getFieldById(targetId);
        checkAccess(target, TenancyAccessService.MODIFY);

        GetResult getResult = new GetResult();
        if (target.getChoiceGroup() != null) {
            getResult.setValue(target.getChoiceGroup().getChoices());
        }
        return getResult;
    }

    @ResponseBody
    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    public PostResult sort(@RequestParam Integer ownerId,
                           @RequestParam Integer type,
                           @RequestParam("sortedRuleIds[]") List<Integer> sortedRuleIds) {
        Field owner = fieldService.getFieldById(ownerId);
        List<Rule> rules = fieldService.getRules(owner, type);
        for (Rule rule : rules) {
            int ordinal = sortedRuleIds.indexOf(rule.getId());
            if (ordinal > -1) {
                rule.setOrdinal(new BigDecimal(ordinal + 1));
            }
        }
        fieldService.updateRuleOrdinal(rules);
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        ConfigEvent configEvent = new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), owner, EventType.SORT);
        configEvent.putProperty(ConfigEvent.NO_OF_RULES, rules.size());
        applicationEventPublisher.publishEvent(configEvent);
        return postResult;
    }

    private List<Field> getFieldsAfter(Field owner, List<Field> all) {
        List<Field> fields = new ArrayList<>();
        for (Field field : all) {
            if (!field.equals(owner) && field.compareTo(owner) > 0) {
                fields.add(field);
            }
        }
        return fields;
    }

    private List<Field> getFieldsBefore(Field owner, List<Field> all) {
        List<Field> fields = new ArrayList<>();
        for (Field field : all) {
            if (!field.equals(owner) && field.compareTo(owner) < 0) {
                fields.add(field);
            }
        }
        return fields;
    }

    private void checkAccess(Field field, int permissionType) {
        if (field != null) {
            checkAccess(field.getForm(), permissionType);
        }
    }

    private void checkAccess(Form form, int permissionType) {
        if (form != null) {
            tenancyAccessService.check(form.getSurvey(), permissionType);
        }
    }

    private String page(String name) {
        return "rule" + "/" + name;
    }
}
