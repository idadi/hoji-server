package ke.co.hoji.server.controller;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.core.service.model.SurveyService;
import ke.co.hoji.server.controller.helper.ControllerHelper;
import ke.co.hoji.server.event.event.ConfigEvent;
import ke.co.hoji.server.event.event.SurveyEvent;
import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.PostResult;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.PublicationService;
import ke.co.hoji.server.service.TenancyAccessService;
import ke.co.hoji.server.service.UserService;
import ke.co.hoji.server.validator.FieldPublicationValidator;
import ke.co.hoji.server.validator.FormPublicationValidator;
import ke.co.hoji.server.validator.PublicationValidatorManager;
import ke.co.hoji.server.validator.SurveyPublicationValidator;
import ke.co.hoji.server.validator.SurveyValidator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class SurveyController {

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private FormService formService;

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private PublicationService publicationService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private SurveyValidator surveyValidator;

    @Autowired
    private UserService userService;

    @Autowired
    private TenancyAccessService tenancyAccessService;

    @Autowired
    private ControllerHelper controllerHelper;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @InitBinder("survey")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(surveyValidator);
    }

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String list(ModelMap model) {
        User user = userService.getCurrentUser();
        User tenant = userService.findById(user.getTenantUserId());
        List<Survey> surveys = surveyService.getSurveysByTenantUserId(tenant.getId(), tenant.getPublicSurveyAccess());
        model.addAttribute("user", user);
        model.addAttribute("userId", tenant.getId());
        model.addAttribute("surveys", surveys);
        UserEvent userEvent = new UserEvent(EventSource.WEB, user, user, EventType.PROJECTS);
        userEvent.putProperty(UserEvent.NO_OF_SURVEYS, surveys.size());
        applicationEventPublisher.publishEvent(userEvent);
        return page("list");
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @RequestMapping(value = "/project/view/{id}/form", method = RequestMethod.GET)
    public String viewForms(@PathVariable Integer id, Model model) {
        Survey survey = surveyService.getSurveyById(id);
        tenancyAccessService.check(survey, TenancyAccessService.VIEW);
        List<Form> forms = formService.getFormsBySurvey(survey);
        model.addAttribute("survey", survey);
        model.addAttribute("forms", forms);
        model.addAttribute("userId", survey.getUserId());
        model.addAttribute("currentUserId", userService.getCurrentUser().getId());
        applicationEventPublisher
            .publishEvent(new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), survey, EventType.VIEW));
        return page("view-form");
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @RequestMapping(value = "/project/view/{id}/property", method = RequestMethod.GET)
    public String viewProperties(@PathVariable Integer id, Model model) {
        Survey survey = surveyService.getSurveyById(id);
        tenancyAccessService.check(survey, TenancyAccessService.VIEW);
        List<Property> properties = propertyService.getProperties(survey.getId());
        model.addAttribute("survey", survey);
        model.addAttribute("properties", properties);
        model.addAttribute("userId", survey.getUserId());
        model.addAttribute("currentUserId", userService.getCurrentUser().getId());
        applicationEventPublisher.publishEvent(new ConfigEvent(EventSource.WEB, userService.getCurrentUser(),
            survey, EventType.VIEW));
        return page("view-property");
    }

    @ResponseBody
    @RequestMapping(value = "/project/edit", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public PostResult save(@RequestBody Survey survey) {
        PostResult postResult = new PostResult();
        BindingResult bindingResult = new BindException(survey, "survey");
        surveyValidator.validate(survey, bindingResult);
        if (bindingResult.hasErrors()) {
            postResult.setPostErrors(controllerHelper.getPostErrors(bindingResult.getFieldErrors()));
        } else {
            Survey resourceToAccess = survey.getId() != null ? surveyService.getSurveyById(survey.getId()) : survey;
            tenancyAccessService.check(resourceToAccess, TenancyAccessService.MODIFY);
            if (survey.getCode() == null || StringUtils.isBlank(survey.getCode())) {
                survey.setCode(generateSurveyCode());
            }
            if (survey.getStatus() == null) {
                survey.setStatus(Survey.MODIFIED);
            }
            boolean isNew = false;
            if (survey.getId() == null) {
                survey.setEnabled(true);
                survey.setAccess(Survey.Access.PRIVATE);
                survey.setFree(false);
                isNew = true;
            }
            survey = surveyService.saveSurvey(survey);
            postResult.setSuccess(true);
            postResult.setValue(survey);
            if (isNew) {
                applicationEventPublisher.publishEvent(new ConfigEvent(EventSource.WEB, userService.getCurrentUser(),
                    survey, EventType.CREATE));
            } else {
                applicationEventPublisher.publishEvent(new ConfigEvent(EventSource.WEB, userService.getCurrentUser(),
                    survey, EventType.MODIFY));
            }
        }
        return postResult;
    }

    @ResponseBody
    @RequestMapping(value = "/project/delete", method = RequestMethod.POST)
    public PostResult delete(@RequestParam Integer id, @RequestParam String name) {
        PostResult postResult = new PostResult();
        Survey survey = configService.getSurvey(id, true);
        if (!name.equals(survey.getName())) {
            String message;
            if (StringUtils.isBlank(name)) {
                message = "Enter the name of the project";
            } else {
                message = "'" + name + "' does not match the name of the project";
            }
            postResult.setPostErrors(controllerHelper.getPostErrors("name", message));
            ConfigEvent configEvent = new ConfigEvent(EventSource.WEB, userService.getCurrentUser(),
                survey, EventType.DELETE);
            configEvent.setResult(EventResult.FAILURE);
            configEvent.setFailureMessage(message);
            applicationEventPublisher.publishEvent(configEvent);
        } else {
            tenancyAccessService.check(survey, TenancyAccessService.MODIFY);
            surveyService.deleteSurvey(survey.getId());
            publicationService.clearData(survey);
            postResult.setSuccess(true);
            applicationEventPublisher.publishEvent(new ConfigEvent(EventSource.WEB, userService.getCurrentUser(),
                survey, EventType.DELETE));
        }
        return postResult;
    }

    @ResponseBody
    @RequestMapping(value = "/project/enable", method = RequestMethod.GET)
    public boolean enable(@RequestParam Integer id, @RequestParam boolean enabled) {
        Survey survey = surveyService.getSurveyById(id);
        tenancyAccessService.check(survey, TenancyAccessService.MODIFY);
        surveyService.enableSurvey(survey, enabled);
        User user = userService.getCurrentUser();
        if (enabled) {
            applicationEventPublisher.publishEvent(new ConfigEvent(EventSource.WEB, user, survey, EventType.ENABLE));
        } else {
            applicationEventPublisher.publishEvent(new ConfigEvent(EventSource.WEB, user, survey, EventType.DISABLE));
        }
        return enabled;
    }

    @ResponseBody
    @RequestMapping(value = "/project/publish", method = RequestMethod.POST)
    public PostResult publish(@RequestParam Integer id) {
        PostResult postResult = new PostResult();
        Survey survey = configService.getSurvey(id, false);
        BindingResult bindingResult = new BindException(survey, "survey");
        PublicationValidatorManager publicationValidatorManager = new PublicationValidatorManager();
        publicationValidatorManager.addPublicationValidator(new SurveyPublicationValidator(survey));
        publicationValidatorManager.addPublicationValidator(new FormPublicationValidator(survey.getForms()));
        survey.getForms().stream()
            .forEach((form -> {
                publicationValidatorManager.addPublicationValidator(new FieldPublicationValidator(form.getFields()));
                form.getFields().stream()
                    .filter(field -> field.isMatrix())
                    .forEach(field -> publicationValidatorManager.addPublicationValidator(new FieldPublicationValidator(((FieldParent) field).getChildren())));
            }));
        publicationValidatorManager.run(bindingResult);
        User user = userService.getCurrentUser();
        if (bindingResult.hasErrors()) {
            postResult.setPostErrors(controllerHelper.getPostErrors(bindingResult.getFieldErrors()));
            SurveyEvent surveyEvent = new SurveyEvent(EventSource.WEB, user, survey, EventType.PUBLISH);
            surveyEvent.setResult(EventResult.FAILURE);
            surveyEvent.setFailureMessage(bindingResult.getFieldErrors().size() + " errors");
            applicationEventPublisher.publishEvent(surveyEvent);
        } else {
            tenancyAccessService.check(survey, TenancyAccessService.MODIFY);
            publicationService.publish(survey);
            postResult.setSuccess(true);
            applicationEventPublisher.publishEvent(new SurveyEvent(EventSource.WEB, user, survey, EventType.PUBLISH));
        }
        Map<String, Object> values = new HashMap<>();
        values.put("survey", survey);
        values.put("forms", survey.getForms());
        postResult.setValue(values);
        return postResult;
    }

    @ResponseBody
    @RequestMapping(value = "/project/clear", method = RequestMethod.POST)
    public PostResult clearData(@RequestParam Integer id, @RequestParam String name) {
        PostResult postResult = new PostResult();
        Survey survey = configService.getSurvey(id, false);
        User user = userService.getCurrentUser();
        if (!name.equals(survey.getName())) {
            String message;
            if (StringUtils.isBlank(name)) {
                message = "Enter the name of the project";
            } else {
                message = "'" + name + "' does not match the name of the project";
            }
            postResult.setPostErrors(controllerHelper.getPostErrors("name", message));
            SurveyEvent surveyEvent = new SurveyEvent(EventSource.WEB, user, survey, EventType.CLEAR);
            surveyEvent.setResult(EventResult.FAILURE);
            surveyEvent.setFailureMessage(message);
            applicationEventPublisher.publishEvent(surveyEvent);
        } else {
            tenancyAccessService.check(survey, TenancyAccessService.MODIFY);
            publicationService.clearData(survey);
            postResult.setSuccess(true);
            applicationEventPublisher.publishEvent(new SurveyEvent(EventSource.WEB, user, survey, EventType.CLEAR));
        }
        postResult.setValue(survey.getStatus());
        return postResult;
    }

    private String generateSurveyCode() {
        String code = Utils.ueid();
        if (surveyService.surveyExists(code)) {
            return generateSurveyCode();
        }
        return code;
    }

    private String page(String name) {
        return "survey" + "/" + name;
    }
}