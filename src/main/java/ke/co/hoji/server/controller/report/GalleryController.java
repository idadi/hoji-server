package ke.co.hoji.server.controller.report;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.controller.report.model.Filter;
import ke.co.hoji.server.event.event.FieldEvent;
import ke.co.hoji.server.event.event.FormEvent;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.GetResult;
import ke.co.hoji.server.service.TenancyAccessService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GalleryController extends BaseDataController {

    @ResponseBody
    @GetMapping(value = "/api/project/{projectId}/form/{formId}/report/gallery")
    public GetResult getImages(Filter filter) {
        Form form = (Form) filter.getFieldParent();
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.VIEW);

        GetResult getResult = new GetResult();
        getResult.setSuccess(true);
        getResult.setValue(dataService.getImages(filter.toQueryFilter()));

        applicationEventPublisher.publishEvent(new FormEvent(EventSource.WEB, userService.getCurrentUser(), form, EventType.TABLE));
        return getResult;
    }

    @ResponseBody
    @GetMapping(value = "/api/project/{projectId}/form/{formId}/field/{fieldId}/report/gallery")
    public GetResult getMatrixImages(Filter filter) {
        Field field = (Field) filter.getFieldParent();
        tenancyAccessService.check(field.getForm().getSurvey(), TenancyAccessService.VIEW);

        GetResult getResult = new GetResult();
        getResult.setSuccess(true);
        getResult.setValue(dataService.getImages(filter.toQueryFilter()));

        applicationEventPublisher.publishEvent(new FieldEvent(EventSource.WEB, userService.getCurrentUser(), field,
                EventType.TABLE));
        return getResult;
    }

}
