package ke.co.hoji.server.controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.service.model.ChoiceGroupService;
import ke.co.hoji.server.controller.helper.ChoiceCsvProcessor;
import ke.co.hoji.server.controller.helper.ControllerHelper;
import ke.co.hoji.server.dao.model.JdbcChoiceDao;
import ke.co.hoji.server.exception.CsvFormatException;
import ke.co.hoji.server.model.GetResult;
import ke.co.hoji.server.model.PostResult;
import ke.co.hoji.server.service.ChoiceGroupUploadService;
import ke.co.hoji.server.service.TenancyAccessService;
import ke.co.hoji.server.service.UserService;

@Controller
public class ChoiceGroupController {

    @Autowired
    private ChoiceGroupService choiceGroupService;

    @Autowired
    private JdbcChoiceDao choiceDao;

    @Autowired
    private UserService userService;

    @Autowired
    private TenancyAccessService tenancyAccessService;

    @Autowired
    private ControllerHelper controllerHelper;

    @Autowired
    private ChoiceCsvProcessor choiceCsvProcessor;

    @Autowired
    private ChoiceGroupUploadService choiceGroupUploadService;

    @GetMapping("/choice-group/**")
    public String page(ModelMap model) {
        return page("page");
    }

    @ResponseBody
    @GetMapping("/api/groups")
    public GetResult getChoiceGroups() {
        List<ChoiceGroupShort> choiceGroups = choiceGroupService
                .getChoiceGroupsByTenant(userService.getCurrentUser().getTenantUserId()).stream()
                .map(cg -> new ChoiceGroupShort(cg.getId(), cg.getName(), cg.getOrderingStrategy(),
                        cg.getExcludeLast()))
                .collect(Collectors.toList());
        GetResult result = new GetResult();
        result.setSuccess(true);
        result.setValue(choiceGroups);
        return result;
    }

    @ResponseBody
    @GetMapping("/api/groups/{id}")
    public GetResult getChoiceGroup(@PathVariable Integer id) {
        ChoiceGroup choiceGroup = choiceGroupService.getChoiceGroupById(id);
        tenancyAccessService.check(choiceGroup, TenancyAccessService.VIEW);
        GetResult result = new GetResult();
        result.setSuccess(true);
        result.setValue(new ChoiceGroupShort(choiceGroup.getId(), choiceGroup.getName(),
                choiceGroup.getOrderingStrategy(), choiceGroup.getExcludeLast()));
        return result;
    }

    @ResponseBody
    @DeleteMapping("/api/groups/{id}")
    public PostResult delete(@PathVariable("id") Integer id) {
        ChoiceGroup choiceGroup = choiceGroupService.getChoiceGroupById(id);
        tenancyAccessService.check(choiceGroup, TenancyAccessService.MODIFY);
        choiceGroupService.deleteChoiceGroup(id);
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        return postResult;
    }

    @ResponseBody
    @PostMapping({ "/api/groups", "/api/groups/{id}" })
    public PostResult save(@RequestBody @Validated ChoiceGroup choiceGroup, BindingResult result) {
        PostResult postResult = new PostResult();
        Integer userId = userService.getCurrentUser().getTenantUserId();
        choiceGroup.setUserId(userId);
        if (result.hasErrors()) {
            postResult.setSuccess(false);
            postResult.setPostErrors(controllerHelper.getPostErrors(result.getFieldErrors()));
        } else {
            choiceGroup = choiceGroupService.saveChoiceGroup(choiceGroup);
            postResult.setSuccess(true);
            postResult.setValue(new ChoiceGroupShort(choiceGroup.getId(), choiceGroup.getName(),
                    choiceGroup.getOrderingStrategy(), choiceGroup.getExcludeLast()));
        }
        return postResult;
    }

    @ResponseBody
    @PostMapping({ "/api/groups/{id}/choices" })
    public PostResult saveChoice(@PathVariable Integer id, @RequestBody @Validated Choice choice,
            BindingResult result) {
        PostResult postResult = new PostResult();
        Integer userId = userService.getCurrentUser().getTenantUserId();
        choice.setUserId(userId);
        if (result.hasErrors()) {
            postResult.setSuccess(false);
            postResult.setPostErrors(controllerHelper.getPostErrors(result.getFieldErrors()));
        } else {
            choiceDao.saveOrUpdate(id, choice);
            postResult.setSuccess(true);
        }
        return postResult;
    }

    @ResponseBody
    @PostMapping(value = "/api/bulk/groups")
    public PostResult upload(@RequestParam MultipartFile choicesCsv) {
        PostResult postResult = new PostResult();
        try {
            List<ChoiceGroup> choiceGroups = choiceCsvProcessor.process(choicesCsv.getInputStream());
            int userId = userService.getCurrentUser().getTenantUserId();
            List<ChoiceGroupShort> saved = choiceGroupUploadService.saveOrUpdate(userId, choiceGroups).stream().map(
                    cg -> new ChoiceGroupShort(cg.getId(), cg.getName(), cg.getOrderingStrategy(), cg.getExcludeLast()))
                    .collect(Collectors.toList());
            postResult.setSuccess(true);
            postResult.setValue(saved);
        } catch (IOException e) {
            throw new CsvFormatException("Could not read uploaded csv file.");
        }
        return postResult;
    }

    @ResponseBody
    @GetMapping(value = "/api/groups/{id}/choices")
    public GetResult getChoices(@PathVariable Integer id, Pageable page) {
        Page<Choice> choices = choiceDao.getByGroup(id, page);
        GetResult result = new GetResult();
        result.setSuccess(true);
        result.setValue(choices);
        return result;
    }

    @ResponseBody
    @GetMapping(value = "/api/groups/{id}/choices", params = { "query" })
    public GetResult getChoices(@PathVariable Integer id, @RequestParam String query, Pageable page) {
        Page<Choice> pagedChoices = choiceDao.searchInGroupByName(id, query, page);
        GetResult result = new GetResult();
        result.setSuccess(true);
        result.setValue(pagedChoices);
        return result;
    }

    @ResponseBody
    @GetMapping(value = "/api/choices", params = { "query" })
    public GetResult getChoices(@RequestParam String query) {
        Page<Choice> pagedChoices = choiceDao.searchByName(userService.getCurrentUser().getTenantUserId(), query, null);
        GetResult result = new GetResult();
        result.setSuccess(true);
        result.setValue(pagedChoices);
        return result;
    }

    @ResponseBody
    @DeleteMapping(value = "/api/groups/{groupId}/choices/{choiceId}")
    public PostResult removeChoice(@PathVariable int groupId, @PathVariable int choiceId) {
        choiceDao.removeFromGroup(groupId, choiceId);
        PostResult result = new PostResult();
        result.setSuccess(true);
        return result;
    }

    private String page(String name) {
        return "choices" + "/" + name;
    }

    private static class ChoiceGroupShort {
        private final int id;
        private final String name;
        private final int orderingStrategy;
        private final int excludeLast;

        public ChoiceGroupShort(int id, String name, int orderingStrategy, int excludeLast) {
            this.id = id;
            this.name = name;
            this.orderingStrategy = orderingStrategy;
            this.excludeLast = excludeLast;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getOrderingStrategy() {
            return orderingStrategy;
        }

        public int getExcludeLast() {
            return excludeLast;
        }

    }
}