package ke.co.hoji.server.controller.report;

import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.server.service.DataService;
import ke.co.hoji.server.service.TenancyAccessService;
import ke.co.hoji.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

/**
 * BaseDataController defines fields and methods used by all output controllers
 * <p>
 * Created by geoffreywasilwa on 03/11/2016.
 */
public class BaseDataController {

    @Autowired
    protected DataService dataService;

    @Autowired
    protected ConfigService configService;

    @Autowired
    protected TenancyAccessService tenancyAccessService;

    @Autowired
    protected UserService userService;

    @Autowired
    protected ApplicationEventPublisher applicationEventPublisher;
}
