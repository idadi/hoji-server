package ke.co.hoji.server.controller;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import ke.co.hoji.core.data.Constants;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.calculation.CalculationUtils;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Field.Calculated;
import ke.co.hoji.core.data.model.Field.FieldAction;
import ke.co.hoji.core.data.model.Field.InputFormat;
import ke.co.hoji.core.data.model.Field.Uniqueness;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.service.model.ChoiceGroupService;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.server.controller.helper.ControllerHelper;
import ke.co.hoji.server.controller.model.RuleWrapper;
import ke.co.hoji.server.event.event.ConfigEvent;
import ke.co.hoji.server.event.event.FormEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.exception.NotFoundException;
import ke.co.hoji.server.model.GetResult;
import ke.co.hoji.server.model.PostResult;
import ke.co.hoji.server.service.PublicationService;
import ke.co.hoji.server.service.TenancyAccessService;
import ke.co.hoji.server.service.UserService;
import ke.co.hoji.server.validator.ChildrenSortValidator;
import ke.co.hoji.server.validator.FieldValidator;

/**
 * Created by gitahi on 21/10/15.
 */
@Controller
@RequestMapping("/field")
public class FieldController {

    public static class BulkFieldEditor {

        private List<Integer> ids;
        private String tag;
        private int missingAction;
        private int parentId;

        public BulkFieldEditor() {
        }

        public void setIds(List<Integer> ids) {
            this.ids = new ArrayList<>(ids);
        }

        private boolean shouldUpdateTag() {
            return !tag.equals("-999");
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public void setMissingAction(int missingAction) {
            this.missingAction = missingAction;
        }

        private boolean shouldUpdateMissingAction() {
            return missingAction != -999;
        }

        private boolean shouldUpdateParent() {
            return parentId != -999;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public List<Field> updateFields(List<Field> fields) {
            List<Field> editedFields = new ArrayList<>();
            for (Field field : fields) {
                if (ids.contains(field.getId())) {
                    boolean edited = false;
                    if (shouldUpdateTag()) {
                        edited = true;
                        field.setTag(tag);
                    }
                    if (shouldUpdateParent()) {
                        edited = true;
                        Field parent = new Field(parentId);
                        field.setParent(parent);
                    }
                    if (shouldUpdateMissingAction()) {
                        edited = true;
                        field.setMissingAction(missingAction);
                    }
                    if (edited) {
                        editedFields.add(field);
                    }
                }
            }
            return editedFields;
        }
    }

    @Value("${application.autoGrowCollectionLimit}")
    private int autoGrowCollectionLimit;

    @Autowired
    private FieldService fieldService;

    @Autowired
    private FormService formService;

    @Autowired
    private ChoiceGroupService choiceGroupService;

    @Autowired
    private UserService userService;

    @Autowired
    private FieldValidator fieldValidator;

    @Autowired
    private ChildrenSortValidator childrenSortValidator;

    @Autowired
    private TenancyAccessService tenancyAccessService;

    @Autowired
    private PublicationService publicationService;

    @Autowired
    private ControllerHelper controllerHelper;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    List<String> autoCompleteList = null;

    @ResponseBody
    @GetMapping(value = "/input-format")
    public GetResult getInputFormatOptions() {
        GetResult result = new GetResult();
        result.setSuccess(true);
        result.setValue(InputFormat.values());
        return result;
    }

    @ResponseBody
    @GetMapping(value = "/field-action")
    public GetResult getFieldActionOptions() {
        GetResult result = new GetResult();
        result.setSuccess(true);
        result.setValue(FieldAction.values());
        return result;
    }

    @ResponseBody
    @GetMapping(value = "/parent")
    public GetResult getParentOptions(@RequestParam int formId, @RequestParam(required = false) Integer fieldId) {
        Form form = formService.getFormById(formId);
        Field target = fieldId != null ? fieldService.getFieldById(fieldId) : null;
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.MODIFY);
        List<Field> allFields = fieldService.getAllFields(form);
        List<Field> fieldsBefore = getFieldsBefore(target, allFields);
        List<Field> parents = new ArrayList<>();
        for (Field field : fieldsBefore) {
            if (field.getType().isParent()) {
                parents.add(field);
            }
        }
        GetResult result = new GetResult();
        result.setSuccess(true);
        result.setValue(parents);
        return result;
    }

    @ResponseBody
    @GetMapping(value = "/editor-options")
    public GetResult getEditorOptions(@RequestParam int formId, @RequestParam(required = false) Integer fieldId) {
        List<Map<String, Object>> types = fieldService.getAllFieldTypes()
                .stream()
                .map(ft -> {
                    Map<String, Object> entry = new HashMap<>();
                    entry.put("id", ft.getId());
                    entry.put("name", ft.getName());
                    return entry;
                })
                .collect(Collectors.toList());
        List<Map<String, Object>> choiceGroups = choiceGroupService
                .getChoiceGroupsByTenant(userService.getCurrentUser().getTenantUserId())
                .stream()
                .map(cg -> {
                    Map<String, Object> entry = new HashMap<>();
                    entry.put("id", cg.getId());
                    entry.put("name", cg.getName());
                    return entry;
                })
                .collect(Collectors.toList());
        GetResult response = new GetResult();
        Map<String, Object> options = new HashMap<>();
        Form form = formService.getFormById(formId);
        Field target = fieldId != null ? fieldService.getFieldById(fieldId) : null;
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.MODIFY);
        List<Field> allFields = fieldService.getAllFields(form);
        List<Field> fieldsBefore = getFieldsBefore(target, allFields);
        List<Map<String, Object>> parents = new ArrayList<>();
        List<Map<String, Object>> choiceFilters = new ArrayList<>();
        for (Field field : fieldsBefore) {
            if (field.getType().isParent()) {
                Map<String, Object> entry = new HashMap<>();
                entry.put("id", field.getId());
                entry.put("description", field.getDescription());
                parents.add(entry);
            }
            if (field.getType().isChoice()) {
                Map<String, Object> entry = new HashMap<>();
                entry.put("id", field.getId());
                entry.put("description", field.getDescription());
                choiceFilters.add(entry);
            }
        }
        List<Map<String, Object>> recordSources = formService.getFormsBySurvey(form.getSurvey())
                .stream()
                .filter(f -> f.getId() != formId)
                .map(f -> {
                    Map<String, Object> entry = new HashMap<>();
                    entry.put("id", f.getId());
                    entry.put("name", f.getName());
                    return entry;
                })
                .collect(Collectors.toList());
        List<Map<String, Object>> referenceFields = new ArrayList<>();
        for (Field field : allFields) {
            if (field.getType().getCode().equals(FieldType.Code.REFERENCE)) {
                Map<String, Object> entry = new HashMap<>();
                entry.put("id", field.getId());
                entry.put("description", field.getDescription());
                referenceFields.add(entry);
            }
        }
        options.put("types", types);
        options.put("choiceGroups", choiceGroups);
        options.put("uniqueSettings", Uniqueness.values());
        options.put("inputFormats", InputFormat.values());
        options.put("fieldActions", FieldAction.values());
        options.put("parents", parents);
        options.put("choiceFilters", choiceFilters);
        options.put("recordSources", recordSources);
        options.put("referenceFields", referenceFields);
        response.setSuccess(true);
        response.setValue(options);
        return response;
    }

    @ResponseBody
    @GetMapping(value = "/autocomplete")
    public GetResult getAutocompleteList() {
        if (autoCompleteList == null) {
            autoCompleteList = new ArrayList<>();
            autoCompleteList.add("Hoji");
            autoCompleteList.add("currentField");
            autoCompleteList.add("calculate");
            autoCompleteList.add("validate");
            Method[] apiMethods = CalculationUtils.class.getDeclaredMethods();
            for (Method method : apiMethods) {
                if (Modifier.isPublic(method.getModifiers())) {
                    autoCompleteList.add(method.getName());
                }
            }
            {
                java.lang.reflect.Field[] fields = CalculationUtils.TimePeriodUnits.class.getDeclaredFields();
                for (java.lang.reflect.Field field : fields) {
                    if (Modifier.isPublic(field.getModifiers())) {
                        autoCompleteList.add(field.getName());
                    }
                }
            }
            {
                java.lang.reflect.Field[] fields = CalculationUtils.DateTimeConstants.class.getDeclaredFields();
                for (java.lang.reflect.Field field : fields) {
                    if (Modifier.isPublic(field.getModifiers())) {
                        autoCompleteList.add(field.getName());
                    }
                }
            }
        }
        GetResult result = new GetResult();
        result.setSuccess(true);
        result.setValue(autoCompleteList);
        return result;
    }

    @RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
    public String view(@PathVariable Integer id, Model model) {
        Field field = fieldService.getFieldById(id);
        checkAccess(field, TenancyAccessService.VIEW);
        model.addAttribute("field", field);
        List<RuleWrapper> forwardSkips = fieldService.getRules(field, Rule.Type.FORWARD_SKIP).stream()
            .map(rule -> new RuleWrapper(rule)).collect(Collectors.toList());
        List<RuleWrapper> backwardSkips = fieldService.getRules(field, Rule.Type.BACKWARD_SKIP).stream()
            .map(rule -> new RuleWrapper(rule)).collect(Collectors.toList());
        model.addAttribute("rules", ListUtils.union(forwardSkips, backwardSkips));
        applicationEventPublisher
            .publishEvent(new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), field, EventType.VIEW));
        return page("view");
    }

    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public PostResult delete(@RequestParam Integer id, @RequestParam String name) {
        PostResult postResult = new PostResult();
        Field field = fieldService.getFieldById(id);
        if (!name.equals(field.getName())) {
            String message;
            if (StringUtils.isBlank(name)) {
                message = "Enter the Question No of the field";
            } else {
                message = "'" + name + "' does not match the Question No of the field";
            }
            postResult.setPostErrors(controllerHelper.getPostErrors("name", message));
            ConfigEvent configEvent = new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), field,
                EventType.DELETE);
            configEvent.setResult(EventResult.FAILURE);
            configEvent.setFailureMessage(message);
            applicationEventPublisher.publishEvent(configEvent);
        } else {
            checkAccess(field, TenancyAccessService.MODIFY);
            fieldService.deleteField(field.getId());
            publicationService.clearData(field);
            postResult.setSuccess(true);
            applicationEventPublisher.publishEvent(
                new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), field, EventType.DELETE));
        }
        return postResult;
    }

    @ResponseBody
    @RequestMapping(value = "/{formId}/save", method = RequestMethod.POST)
    public PostResult save(@RequestBody ke.co.hoji.core.data.dto.Field fieldDto,
                           @PathVariable("formId") Integer formId) {
        Field field = fieldService.getFieldFromDto(fieldDto);
        Form form = formService.getFormById(formId);
        field.setForm(form);
        checkAccess(field, TenancyAccessService.MODIFY);
        BindingResult result = new BeanPropertyBindingResult(field, "field");
        fieldValidator.validate(field, result);
        PostResult postResult = new PostResult();
        if (result.hasErrors()) {
            postResult.setPostErrors(controllerHelper.getPostErrors(result.getFieldErrors()));
        } else {
            Map<String, Object> results = new HashMap<>();
            field = saveField(field);
            results.put("field", field);
            postResult.setValue(results);
            postResult.setSuccess(true);
        }
        return postResult;
    }

    private Field saveField(Field field) {
        Field saved;
        boolean isNew = false;
        if (field.getId() == null) {
            field.setOrdinal(nextOrdinal(field.getForm()));
            if (field.getMissingAction() == null) {
                field.setMissingAction(Field.FieldAction.DO_NOT_ALLOW.getValue());
            }
            if (StringUtils.isBlank(field.getName())) {
                field.setName(nextQuestionNumber(field.getForm()));
            }
            if (StringUtils.isBlank(field.getTag())) {
                if (field.getType().getCode().equals(FieldType.Code.SINGLE_LINE_TEXT)
                    || field.getType().getCode().equals(FieldType.Code.ENCRYPTED_TEXT)) {
                    field.setTag(String.valueOf(Field.InputFormat.TITLE.getValue()));
                } else if (field.getType().getCode().equals(FieldType.Code.MULTIPLE_LINES_TEXT)) {
                    field.setTag(String.valueOf(Field.InputFormat.SENTENCE.getValue()));
                }
            }
            field.setEnabled(true);
            if (field.getType().isImage()) {
                field.setMaxValue(String.valueOf(Constants.ImageManagement.MAX_DIMENSION));
                field.setMinValue(String.valueOf(Constants.ImageManagement.DEFAULT_QUALITY));
            }
            if (field.getUniqueness() == null) {
                field.setUniqueness(Field.Uniqueness.NONE.getValue());
            }
            if (field.getCalculated() == null) {
                field.setCalculated(Calculated.NOT_CALCULATED);
            }
            saved = fieldService.createField(field);
            isNew = true;
        } else {
            saved = fieldService.modifyField(field);
        }
        if (isNew) {
            applicationEventPublisher.publishEvent(
                new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), field, EventType.CREATE));
        } else {
            applicationEventPublisher.publishEvent(
                new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), field, EventType.MODIFY));
        }
        return saved;
    }

    @ResponseBody
    @RequestMapping(value = "{formId}/bulk-save", method = RequestMethod.POST)
    public PostResult bulkSave(@PathVariable Integer formId, @RequestBody BulkFieldEditor bulkFieldEditor) {
        Form form = formService.getFormById(formId);
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.MODIFY);
        ;
        List<Field> allFields = fieldService.getFields(form);
        List<Field> savedFields = new ArrayList<>();
        if (allFields != null && !allFields.isEmpty()) {
            List<Field> editedFields = bulkFieldEditor.updateFields(allFields);
            fieldService.modifyFields(editedFields);
            List<Integer> editedFieldIds = editedFields.stream().map(field -> field.getId())
                    .collect(Collectors.toList());
            savedFields = fieldService.getAllFields(form).stream()
                    .filter(field -> editedFieldIds.contains(field.getId())).collect(Collectors.toList());
            ConfigEvent configEvent = new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), form,
                    EventType.BULK_EDIT);
            configEvent.putProperty(ConfigEvent.NO_OF_FIELDS, editedFields.size());
            applicationEventPublisher.publishEvent(configEvent);
        }
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        postResult.setValue(savedFields);
        return postResult;
    }

    @ResponseBody
    @PostMapping(value = "/{formId}/bulk-save", params = "attribute=captionable")
    public PostResult saveCaptionableFields(@PathVariable Integer formId,
                                            @RequestParam("fieldIds[]") List<Integer> fieldIds) {
        Form form = formService.getFormById(formId);
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.MODIFY);
        List<Field> fieldsToUpdate = fieldService.getAllFields(form).stream()
            .filter(field -> fieldIds.contains(field.getId())).map(field -> {
                field.setCaptioning(true);
                return field;
            }).collect(Collectors.toList());
        List<Field> modifiedFields = fieldService.modifyFields(fieldsToUpdate);
        ConfigEvent configEvent = new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), form,
            EventType.BULK_EDIT);
        configEvent.putProperty(ConfigEvent.NO_OF_FIELDS, modifiedFields.size());
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        postResult.setValue(modifiedFields);
        return postResult;
    }

    @ResponseBody
    @PostMapping(value = "/{formId}/bulk-save", params = "attribute=searchable")
    public PostResult saveSearchableFields(@PathVariable Integer formId,
                                           @RequestParam("fieldIds[]") List<Integer> fieldIds) {
        Form form = formService.getFormById(formId);
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.MODIFY);
        List<Field> fieldsToUpdate = fieldService.getAllFields(form).stream()
            .filter(field -> fieldIds.contains(field.getId())).map(field -> {
                field.setSearchable(true);
                return field;
            }).collect(Collectors.toList());
        List<Field> modifiedFields = fieldService.modifyFields(fieldsToUpdate);
        ConfigEvent configEvent = new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), form,
            EventType.BULK_EDIT);
        configEvent.putProperty(ConfigEvent.NO_OF_FIELDS, modifiedFields.size());
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        postResult.setValue(modifiedFields);
        return postResult;
    }

    @ResponseBody
    @PostMapping(value = "/{formId}/bulk-save", params = "attribute=responseInheriting")
    public PostResult saveResponseInheritingFields(@PathVariable Integer formId,
                                                   @RequestParam("fieldIds[]") List<Integer> fieldIds) {
        Form form = formService.getFormById(formId);
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.MODIFY);
        List<Field> fieldsToUpdate = fieldService.getAllFields(form).stream()
            .filter(field -> fieldIds.contains(field.getId())).map(field -> {
                field.setResponseInheriting(true);
                return field;
            }).collect(Collectors.toList());
        List<Field> modifiedFields = fieldService.modifyFields(fieldsToUpdate);
        ConfigEvent configEvent = new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), form,
            EventType.BULK_EDIT);
        configEvent.putProperty(ConfigEvent.NO_OF_FIELDS, modifiedFields.size());
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        postResult.setValue(modifiedFields);
        return postResult;
    }

    @ResponseBody
    @RequestMapping(value = "/{formId}/attachScript", method = RequestMethod.POST)
    public PostResult attachScript(
        @RequestBody ke.co.hoji.core.data.dto.Field fieldDto,
        @PathVariable("formId") Integer formId
    ) {
        Field field = fieldService.getFieldById(fieldDto.getId());
        field.setValueScript(fieldDto.getValueScript());
        field.setCalculated(fieldDto.getCalculated());
        Form form = formService.getFormById(formId);
        field.setForm(form);
        checkAccess(field, TenancyAccessService.MODIFY);
        PostResult postResult = new PostResult();
        Map<String, Object> results = new HashMap<>();
        field = saveField(field);
        results.put("field", field);
        postResult.setValue(results);
        postResult.setSuccess(true);
        return postResult;
    }

    @RequestMapping(value = "/auto-number", method = RequestMethod.GET)
    public String autoNumber(@RequestParam(value = "formId") Integer formId) {
        Form form = formService.getFormById(formId);
        checkAccess(form, TenancyAccessService.MODIFY);
        List<Field> fields = fieldService.getFields(form);
        formService.autoNumber(fields);
        FormEvent formEvent = new FormEvent(EventSource.WEB, userService.getCurrentUser(), form, EventType.AUTO_NUMBER);
        formEvent.putProperty(FormEvent.NO_OF_FIELDS, fields.size());
        applicationEventPublisher.publishEvent(formEvent);
        return redirect("form/view/" + form.getId() + "/field");
    }

    @ResponseBody
    @RequestMapping(value = "/enable", method = RequestMethod.GET)
    public boolean enable(@RequestParam Integer id, @RequestParam boolean enabled) {
        Field field = fieldService.getFieldById(id);
        checkAccess(field, TenancyAccessService.MODIFY);
        fieldService.enableField(field, enabled);
        if (enabled) {
            applicationEventPublisher.publishEvent(
                new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), field, EventType.ENABLE));
        } else {
            applicationEventPublisher.publishEvent(
                new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), field, EventType.DISABLE));
        }
        return enabled;
    }

    @ResponseBody
    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    public PostResult sort(@RequestParam Integer formId,
                           @RequestParam("sortedFieldIds[]") List<Integer> sortedFieldIds) {
        Form form = formService.getFormById(formId);
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.MODIFY);
        List<Field> fields = fieldService.getAllFields(form);
        form.setFields(fields);
        for (Field field : fields) {
            int ordinal = sortedFieldIds.indexOf(field.getId());
            if (ordinal > -1) {
                field.setOrdinal(new BigDecimal(ordinal + 1));
            }
        }
        Collections.sort(fields);
        BindingResult result = new BindException(form, "form");
        childrenSortValidator.validate(form, result);
        PostResult postResult = new PostResult();
        if (result.hasErrors()) {
            postResult.setSuccess(false);
            postResult.setPostErrors(controllerHelper.getPostErrors(result.getFieldErrors()));
        } else {
            fieldService.updateOrdinal(fields);
            postResult.setSuccess(true);
            ConfigEvent configEvent = new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), form,
                EventType.SORT);
            configEvent.putProperty(ConfigEvent.NO_OF_FIELDS, fields.size());
            applicationEventPublisher.publishEvent(configEvent);
        }
        return postResult;
    }

    private void checkAccess(Field field, int permissionType) {
        if (field != null) {
            checkAccess(field.getForm(), permissionType);
        } else {
            throw new NotFoundException();
        }
    }

    private void checkAccess(Form form, int permissionType) {
        if (form != null) {
            tenancyAccessService.check(form.getSurvey(), permissionType);
        }
    }

    private BigDecimal nextOrdinal(Form form) {
        List<Field> allFields = fieldService.getAllFields(form);
        return Utils.nextOrdinal(new ArrayList<>(allFields));
    }

    private String nextQuestionNumber(Form form) {
        String nextQuestionNumber = "";
        List<Field> allFields = fieldService.getFields(form);
        if (!allFields.isEmpty()) {
            Integer lastIntQuestionNo = null;
            int n = allFields.size() - 1;
            for (int i = n; i >= 0; i--) {
                Field field = allFields.get(i);
                lastIntQuestionNo = Utils.parseInteger(field.getName());
                if (lastIntQuestionNo != null) {
                    break;
                }
            }
            if (lastIntQuestionNo != null) {
                lastIntQuestionNo++;
                nextQuestionNumber = String.valueOf(lastIntQuestionNo);
            }
        } else {
            nextQuestionNumber = "1";
        }
        return nextQuestionNumber;
    }

    private List<Field> getFieldsBefore(Field target, List<Field> all) {
        if (target == null) {
            return all;
        }
        List<Field> fields = new ArrayList<>();
        for (Field field : all) {
            if (!field.equals(target) && field.compareTo(target) < 0) {
                fields.add(field);
            }
        }
        return fields;
    }

    private String page(String name) {
        return "field" + "/" + name;
    }

    private String redirect(String page) {
        return "redirect:/" + page;
    }
}
