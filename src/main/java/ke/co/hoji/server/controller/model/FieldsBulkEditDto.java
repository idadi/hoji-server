package ke.co.hoji.server.controller.model;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FieldsBulkEditDto {

    private List<Field> fields = new ArrayList<>();
    private Map<Integer, FieldConfigOptions> options = new HashMap<>();

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    public Map<Integer, FieldConfigOptions> getOptions() {
        return options;
    }

    public void setOptions(Map<Integer, FieldConfigOptions> options) {
        this.options = options;
    }

    public static FieldsBulkEditDto build(
            final List<Field> allFields,
            final List<Form> allForms,
            final FieldsEditorConfig editorConfig
    ) {
        FieldsBulkEditDto dto = new FieldsBulkEditDto();
        allFields.forEach(current -> {
            List<Field> fieldsBefore = getFieldsBefore(current, allFields);
            List<Field> choiceFilters = new ArrayList<>();
            if (current.getChoiceGroup() != null && editorConfig.isChoiceFilter()) {
                choiceFilters = getChoiceFilters(current.getChoiceGroup(), fieldsBefore);
            }
            List<Field> parents = new ArrayList<>();
            if (editorConfig.isParent() && !current.isMatrix()) {
                List<Field> allParents = allFields.stream().filter(Field::isMatrix).collect(Collectors.toList());
                parents = getFieldsBefore(current, allParents);
            }
            List<Field> referenceFields = new ArrayList<>();
            if (editorConfig.isReferenceField()) {
                referenceFields = allFields
                        .stream()
                        .filter(field -> field.getType().getCode().equals(FieldType.Code.REFERENCE))
                        .collect(Collectors.toList());
            }
            List<Form> recordSources = new ArrayList<>();
            if (editorConfig.isRecordSources() && current.getType().isRecord()) {
                recordSources = allForms;
            }
            dto.getFields().add(current);
            dto.getOptions().put(
                    current.getId(),
                    new FieldConfigOptions(choiceFilters, recordSources, referenceFields, parents)
            );
        });
        return dto;
    }

    private static List<Field> getFieldsBefore(Field owner, List<Field> all) {
        List<Field> fields = new ArrayList<>();
        for (Field field : all) {
            if (!field.equals(owner) && field.compareTo(owner) < 0) {
                fields.add(field);
            }
        }
        return fields;
    }

    private static List<Field> getPipeSources(List<Field> fields) {
        List<Field> pipeSources = new ArrayList<>();
        for (Field field : fields) {
            if (!field.getType().getCode().equals(FieldType.Code.LABEL)
                    && !field.getType().getCode().equals(FieldType.Code.MATRIX)) {
                pipeSources.add(field);
            }
        }
        return pipeSources;
    }

    private static List<Field> getChoiceFilters(ChoiceGroup current, List<Field> fieldsBefore) {
        List<Field> choiceFilters = new ArrayList<>();
        for (Choice choice : current.getChoices()) {
            Choice parent = choice.getParent();
            if (parent != null) {
                List<Field> viableChoiceFilters = getChoiceFiltersForChoice(choice, fieldsBefore);
                for (Field potentialChoiceFilter : viableChoiceFilters) {
                    if (!choiceFilters.contains(potentialChoiceFilter)) {
                        choiceFilters.add(potentialChoiceFilter);
                    }
                }
            }
        }
        return choiceFilters;
    }

    private static List<Field> getChoiceFiltersForChoice(Choice choice, List<Field> fieldsBefore) {
        List<Field> choiceFilters = new ArrayList<>();
        for (Field field : fieldsBefore) {
            if (field.getType().isChoice() && field.getChoiceGroup() != null) {
                for (Choice potentialParent : field.getChoiceGroup().getChoices()) {
                    if (choice.getParent().equals(potentialParent)) {
                        choiceFilters.add(field);
                        break;
                    }
                }
            }
        }
        return choiceFilters;
    }

    static class FieldConfigOptions {

        private final List<Field> choiceFilters;
        private final List<Form> recordSources;
        private final List<Field> referenceFields;
        private final List<Field> parents;

        FieldConfigOptions(
                List<Field> choiceFilters,
                List<Form> recordSources,
                List<Field> referenceFields,
                List<Field> parents
        ) {
            this.choiceFilters = choiceFilters;
            this.recordSources = recordSources;
            this.referenceFields = referenceFields;
            this.parents = parents;
        }

        public List<Field> getChoiceFilters() {
            return choiceFilters;
        }

        public List<Form> getRecordSources() {
            return recordSources;
        }

        public List<Field> getReferenceFields() {
            return referenceFields;
        }

        public List<Field> getParents() {
            return parents;
        }
    }
}
