package ke.co.hoji.server.controller.helper;

import ke.co.hoji.server.model.PostError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Provides general purpose methods for Controllers.
 * <p>
 * Created by gitahi on 1/12/17.
 */
@Component
public class ControllerHelper {

    private MessageSource messageSource;

    @Autowired
    public ControllerHelper(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * Convert a list of @{@link FieldError}s to @{@link PostError}s.
     *
     * @param fieldErrors the list of FieldErrors to convert.
     * @return a list of PostErrors.
     */
    public List<PostError> getPostErrors(List<FieldError> fieldErrors) {
        List<PostError> postErrors = new ArrayList<>();
        for (FieldError fieldError : fieldErrors) {
            PostError postError = new PostError();
            postError.setParameter(fieldError.getField());
            DefaultMessageSourceResolvable messageSourceResolvable =
                    new DefaultMessageSourceResolvable(fieldError.getCodes(), fieldError.getDefaultMessage());
            String message = messageSource.getMessage(messageSourceResolvable, Locale.ENGLISH);
            postError.setMessage(message);
            postErrors.add(postError);
        }
        return postErrors;
    }

    /**
     * Create a single @{@link PostError}, put it in a list and return the list. Suitable for creating a list of
     * PostErrors from a single error.
     *
     * @param parameter the name of the parameter associated with the error
     * @param message   the error message
     * @return the list of PostErrors
     */
    public List<PostError> getPostErrors(String parameter, String message) {
        List<PostError> postErrors = new ArrayList<>();
        PostError postError = new PostError();
        postError.setParameter(parameter);
        postError.setMessage(message);
        postErrors.add(postError);
        return postErrors;
    }

    /**
     * Return a redirect string for the give page.
     *
     * @param page the page
     * @return the redirect string
     */
    public String redirect(String page) {
        return "redirect:/" + page;
    }

}
