package ke.co.hoji.server.controller.model;

import static ke.co.hoji.core.data.model.Rule.Type.BACKWARD_SKIP;
import static ke.co.hoji.core.data.model.Rule.Type.FORWARD_SKIP;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.Constants.Command;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.OperatorDescriptor;
import ke.co.hoji.core.data.model.Rule;

/**
 * A wrapper for the {@link Rule} that helps translate rule values to choice labels where applicable
 */
public class RuleWrapper {
    Integer id;
    Integer type;
    Field owner;
    Field target;
    OperatorDescriptor operatorDescriptor;
    String displayValue;
    Object value;
    Integer combiner;
    Integer grouper;
    BigDecimal ordinal;

    public RuleWrapper(Rule rule) {
        id = rule.getId();
        type = rule.getType();
        owner = rule.getOwner();
        target = rule.getTarget();
        operatorDescriptor = rule.getOperatorDescriptor();
        combiner = rule.getCombiner();
        grouper = rule.getGrouper();
        ordinal = rule.getOrdinal();

        if (rule.getType() == FORWARD_SKIP && owner.getChoiceGroup() != null) {
            setReadableValue(owner.getChoiceGroup().getChoices(), rule.getValue());
        } else if (rule.getType() == BACKWARD_SKIP && target.getChoiceGroup() != null) {
            setReadableValue(target.getChoiceGroup().getChoices(), rule.getValue());
        } else {
            value = displayValue = rule.getValue();
        }
    }

    public Integer getId() {
        return id;
    }

    public Integer getType() {
        return type;
    }

    public Field getOwner() {
        return owner;
    }

    public Field getTarget() {
        return target;
    }

    public OperatorDescriptor getOperatorDescriptor() {
        return operatorDescriptor;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public Object getValue() {
        return value;
    }

    public Integer getCombiner() {
        return combiner;
    }

    public Integer getGrouper() {
        return grouper;
    }

    public BigDecimal getOrdinal() {
        return ordinal;
    }

    private void setReadableValue(List<Choice> choices, String rawValue) {
        if (Constants.Command.EMPTY.equalsIgnoreCase(rawValue) || isRuleValueEmpty(rawValue)) {
            displayValue = "Empty";
            value = Command.EMPTY;
            return;
        }
        List<Choice> selectedChoices = new ArrayList<>(getSelectedChoicesFromChoiceIds(choices, rawValue));
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < selectedChoices.size(); i++) {
            if (selectedChoices.get(i) != null) {
                builder.append(selectedChoices.get(i).getName());
                if (i < selectedChoices.size() - 1) {
                    builder.append(", ");
                }
            }
        }
        displayValue = builder.toString();
        value = selectedChoices;
    }

    private List<Choice> getSelectedChoicesFromChoiceIds(List<Choice> choices, String rawValue) {
        List<Choice> selectedChoices = new ArrayList<>();
        for (Choice choice : choices) {
            if (rawValue != null) {
                String[] selectedIds = rawValue.split("\\|");
                for (String selectedId : selectedIds) {
                    Integer choiceId = Utils.parseInteger(selectedId);
                    if (selectedId.contains("#")) {
                        String[] idAndState = selectedId.split("#");
                        if (idAndState[1].equals("1")) {
                            choiceId = Utils.parseInteger(idAndState[0]);
                        }
                    }
                    if (choice.getId().equals(choiceId)) {
                        selectedChoices.add(choice);
                    }
                }
            }
        }
        return selectedChoices;
    }

    private boolean isRuleValueEmpty(String rawValue) {
        return Utils.parseBin(rawValue) != null && Utils.parseBin(rawValue).equals(0L);
    }
}