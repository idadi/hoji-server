package ke.co.hoji.server.controller.helper;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.server.exception.CsvFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseBool;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ke.co.hoji.server.controller.helper.ChoiceCsvProcessor.ChoiceCsvHeaders.CHOICE_GROUP_HEADER;
import static ke.co.hoji.server.controller.helper.ChoiceCsvProcessor.ChoiceCsvHeaders.CODE_HEADER;
import static ke.co.hoji.server.controller.helper.ChoiceCsvProcessor.ChoiceCsvHeaders.DESCRIPTION_HEADER;
import static ke.co.hoji.server.controller.helper.ChoiceCsvProcessor.ChoiceCsvHeaders.LONER_HEADER;
import static ke.co.hoji.server.controller.helper.ChoiceCsvProcessor.ChoiceCsvHeaders.NAME_HEADER;
import static ke.co.hoji.server.controller.helper.ChoiceCsvProcessor.ChoiceCsvHeaders.PARENT_HEADER;
import static ke.co.hoji.server.controller.helper.ChoiceCsvProcessor.ChoiceCsvHeaders.SCALE_HEADER;
import static ke.co.hoji.server.controller.helper.ChoiceCsvProcessor.ChoiceCsvHeaders.asList;

/**
 * Builds choice group object graph from a CSV resource. Choice sharing names with parents
 * are treated as different objects. When duplicate choices are encountered, the first choice
 * wins.
 * <p>
 * Created by geoffreywasilwa on 21/04/2017.
 */
@Component
public class ChoiceCsvProcessor extends AbstractCsvProcessor<ChoiceGroup> {

    private Logger logger = LoggerFactory.getLogger(ChoiceCsvProcessor.class);

    @Override
    protected List<ChoiceGroup> build(List<Map<String, Object>> csvList) {
        return new CsvToChoiceGroup().build(csvList);
    }

    @Override
    protected void validateHeaders(String[] csvHeaders) {
        logger.debug("Validating headers: " + csvHeaders);
        List<String> headers = asList();
        if (!headers.removeAll(Arrays.asList(csvHeaders))) {
            String expected = Utils.string(asList());
            throw new CsvFormatException("The expected headers are: " + expected);
        }
        if (headers.size() > 0) {
            String missing = Utils.string(headers);
            throw new CsvFormatException("The uploaded csv is missing the following headers: " + missing);
        }
        int index = 0;
        for (String header : asList()) {
            if (!header.equals(csvHeaders[index++])) {
                throw new CsvFormatException("The headers should be in the order: " + Utils.string(asList()));
            }
        }
    }

    protected CellProcessor[] getProcessors() {

        final CellProcessor[] processors = new CellProcessor[] { new Optional(), // code
                new NotNull(), // name
                new Optional(), // description
                new Optional(), // parent
                new Optional(new ParseInt()), // scale
                new Optional(new ParseBool()), // loner
                new Optional() // group
        };

        return processors;
    }

    static class ChoiceCsvHeaders {
        static final String CODE_HEADER = "Code";
        static final String NAME_HEADER = "Name";
        static final String DESCRIPTION_HEADER = "Description";
        static final String PARENT_HEADER = "Parent";
        static final String SCALE_HEADER = "Scale";
        static final String LONER_HEADER = "Loner";
        static final String CHOICE_GROUP_HEADER = "Choice_Group";

        static List<String> asList() {
            List<String> headers = new ArrayList<>();
            headers.add(CODE_HEADER);
            headers.add(NAME_HEADER);
            headers.add(DESCRIPTION_HEADER);
            headers.add(PARENT_HEADER);
            headers.add(SCALE_HEADER);
            headers.add(LONER_HEADER);
            headers.add(CHOICE_GROUP_HEADER);
            return headers;
        }
    }

    private class CsvToChoiceGroup {
        List<ChoiceGroup> choiceGroups = new ArrayList<>();
        Map<String, ChoiceGroup> groupContainer = new HashMap<>();
        Map<String, Choice> parentContainer = new HashMap<>();

        public List<ChoiceGroup> build(List<Map<String, Object>> list) {
            for (Map<String, Object> line : list) {
                String groupName = Utils.removeSpaces(line.get(CHOICE_GROUP_HEADER).toString());
                buildGroup(groupName);
                buildOrUpdateChoice(line, groupName);
            }
            return choiceGroups;
        }

        private void buildGroup(String groupName) {
            if (!groupContainer.containsKey(groupName)) {
                ChoiceGroup choiceGroup = new ChoiceGroup();
                choiceGroup.setName(groupName);
                choiceGroup.setChoices(new ArrayList<>());
                groupContainer.put(groupName, choiceGroup);
                choiceGroups.add(choiceGroup);
            }
        }

        private void buildOrUpdateChoice(Map<String, Object> line, String groupName) {
            Object specifiedCode = line.get(CODE_HEADER);
            String code = specifiedCode != null ? Utils.removeSpaces(specifiedCode.toString()) : null;
            String name = Utils.removeSpaces(line.get(NAME_HEADER).toString());
            String description = line.get(DESCRIPTION_HEADER) != null
                    ? Utils.removeSpaces(line.get(DESCRIPTION_HEADER).toString())
                    : null;
            boolean loner = line.get(LONER_HEADER) != null ? Boolean.valueOf(Utils.removeSpaces(line.get(LONER_HEADER).toString()))
                    : false;
            int scale = line.get(SCALE_HEADER) != null ? Integer.valueOf(Utils.removeSpaces(line.get(SCALE_HEADER).toString()))
                    : 0;
            String parentName = line.get(PARENT_HEADER) != null ? Utils.removeSpaces(line.get("Parent").toString())
                    : null;

            Choice choice = new Choice(null, name, description, code, scale, loner);
            if (parentName != null) {
                String key = parentName.toLowerCase();
                Choice parent = parentContainer.get(key);
                if (parent == null) {
                    parent = new Choice();
                    parent.setName(parentName);
                    parentContainer.put(key, parent);
                }
                choice.setParent(parent);
            }
            ChoiceGroup choiceGroup = groupContainer.get(groupName);
            if (!choiceGroup.getChoices().contains(choice)) {
                choiceGroup.getChoices().add(choice);
            }
        }
    }
}
