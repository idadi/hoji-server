package ke.co.hoji.server.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.session.ChangeSessionIdAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ke.co.hoji.core.data.Constants;
import ke.co.hoji.core.data.PublicSurveyAccess;
import ke.co.hoji.server.controller.helper.ControllerHelper;
import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.GetResult;
import ke.co.hoji.server.model.PostError;
import ke.co.hoji.server.model.PostResult;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.VerificationToken;
import ke.co.hoji.server.service.CashTransactionService;
import ke.co.hoji.server.service.CreditTransactionService;
import ke.co.hoji.server.service.UserService;
import ke.co.hoji.server.validator.UserValidator;

@Controller
public class UserController {

    @Value("${application.autoGrowCollectionLimit}")
    private int autoGrowCollectionLimit;

    private final UserValidator userValidator;
    private final UserService userService;
    private final CashTransactionService cashTransactionService;
    private final CreditTransactionService creditTransactionService;
    private final AuthenticationManager authenticationManager;
    private final ControllerHelper controllerHelper;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final SessionAuthenticationStrategy sessionAuthenticationStrategy;

    public UserController(UserValidator userValidator, UserService userService,
            CashTransactionService cashTransactionService, CreditTransactionService creditTransactionService,
            AuthenticationManager authenticationManager, ControllerHelper controllerHelper,
            ApplicationEventPublisher applicationEventPublisher) {
        this.userValidator = userValidator;
        this.userService = userService;
        this.cashTransactionService = cashTransactionService;
        this.creditTransactionService = creditTransactionService;
        this.authenticationManager = authenticationManager;
        this.controllerHelper = controllerHelper;
        this.applicationEventPublisher = applicationEventPublisher;
        this.sessionAuthenticationStrategy = new ChangeSessionIdAuthenticationStrategy();
    }

    /**
     * The
     * {@link org.springframework.validation.DataBinder#DEFAULT_AUTO_GROW_COLLECTION_LIMIT}
     * is set to 256 items by default, but when managing users, it is possible to be
     * binding a lot more items.
     *
     * @param binder {@link WebDataBinder}
     */
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setAutoGrowCollectionLimit(autoGrowCollectionLimit);
    }

    @InitBinder("user")
    protected void initValidatorBinder(WebDataBinder binder) {
        binder.setValidator(userValidator);
    }

    @RequestMapping(value = "/signIn", method = RequestMethod.GET)
    public String signIn(HttpServletRequest request, HttpServletResponse response) {
        User user = userService.getCurrentUser();
        if (user != null) {
            return "redirect:/";
        }
        return "signIn";
    }

    @RequestMapping(value = "/signUp", method = RequestMethod.GET)
    public String signUp(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "signUp";
    }

    @PostMapping(value = "/signUp")
    public @ResponseBody PostResult signUp(HttpServletRequest request, HttpServletResponse response, @RequestBody User user) {
        PostResult postResult = new PostResult();
        String password = user.getPassword();
        user.setFullName(user.getFirstName());
        user.setConfirmPassword(user.getPassword());
        BindingResult result = new BeanPropertyBindingResult(user, "user");
        userValidator.validate(user, result);
        if (result.hasErrors()) {
            postResult.setSuccess(false);
            postResult.setPostErrors(controllerHelper.getPostErrors(result.getFieldErrors()));
            return postResult;
        } else {
            user.setFullName(user.getFirstName());
            userService.createUser(user);
            updateAuthenticationInfo(user.getUsername(), password, request, response);
            applicationEventPublisher.publishEvent(new UserEvent(EventSource.WEB, user, user, EventType.SIGN_UP));
            applicationEventPublisher.publishEvent(new UserEvent(EventSource.WEB, user, user, EventType.WELCOME));
            postResult.setSuccess(true);
        }
        return postResult;
    }

    /**
     * Creates a new authentication object and updates the associated http session.
     * 
     * @param username username to authenticate
     * @param password clear text password of the user
     * @param request Http request object
     * @param response Http response object
     */
    private void updateAuthenticationInfo(String username, String password, HttpServletRequest request, HttpServletResponse response) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        token.setDetails(new WebAuthenticationDetails(request));
        Authentication authentication = authenticationManager.authenticate(token);
        sessionAuthenticationStrategy.onAuthentication(authentication, request, response);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @GetMapping(value = "/verificationTokenSent")
    public String verified(Model model, @RequestParam("initial") boolean initial) {
        User user = userService.getCurrentUser();
        if (user.isVerified()) {
            return "redirect:/";
        }
        if (initial) {
            int daysOld = Days.daysBetween(new LocalDate(user.getDateCreated().getTime()), new LocalDate()).getDays();
            if (daysOld > 1) {
                return "redirect:/";
            }
            model.addAttribute("messageType", MessageType.INITIAL_VERIFICATION_TOKEN_SENT);
        } else {
            model.addAttribute("messageType", MessageType.SUBSEQUENT_VERIFICATION_TOKEN_SENT);
        }
        model.addAttribute("firstName", user.getFirstName());
        model.addAttribute("username", user.getUsername());
        return "message";
    }

    @GetMapping(value = "/emailVerified")
    public String welcome(Model model) {
        User user = userService.getCurrentUser();
        model.addAttribute("firstName", user.getFirstName());
        model.addAttribute("username", user.getUsername());
        model.addAttribute("messageType", MessageType.INITIAL_VERIFICATION_TOKEN_SENT);
        return "message";
    }

    @GetMapping(value = "/passwordTokenSent")
    public String passwordTokenSent(Model model, @RequestParam("email") String email) {
        model.addAttribute("email", email);
        model.addAttribute("messageType", MessageType.PASSWORD_RESET_TOKEN_SENT);
        return "message";
    }

    @GetMapping(value = "/passwordReset")
    public String passwordReset(Model model) {
        model.addAttribute("messageType", MessageType.PASSWORD_RESET_SUCCESSFULLY);
        return "message";
    }

    @RequestMapping(value = "/confirm", method = RequestMethod.GET)
    public String confirm(HttpServletRequest request, HttpServletResponse response, Model model, @RequestParam("token") String token,
            RedirectAttributes redirectAttributes) {
        User currentUser = userService.getCurrentUser();
        User userToVerify;
        VerificationToken verificationToken;
        if (currentUser != null) {
            if (currentUser.isVerified()) {
                model.addAttribute("firstName", currentUser.getFirstName());
                model.addAttribute("username", currentUser.getUsername());
                model.addAttribute("messageType", MessageType.SIGNED_IN_VERIFIED);
                return "message";
            } else {
                verificationToken = userService.getVerificationToken(token, VerificationToken.Type.EMAIL_VERIFICATION);
                if (verificationToken == null) {
                    model.addAttribute("messageType", MessageType.SIGNED_IN_INVALID_VERIFICATION_TOKEN);
                    return "message";
                } else {
                    userToVerify = verificationToken.getUser();
                    if (userToVerify.equals(currentUser)) {
                        if (verificationToken.isExpired()) {
                            model.addAttribute("messageType", MessageType.SIGNED_IN_EXPIRED_VERIFICATION_TOKEN);
                            return "message";
                        }
                    } else {
                        model.addAttribute("messageType", MessageType.SIGNED_IN_INVALID_VERIFICATION_TOKEN);
                        return "message";
                    }
                }
            }
        } else {
            verificationToken = userService.getVerificationToken(token, VerificationToken.Type.EMAIL_VERIFICATION);
            if (verificationToken == null) {
                model.addAttribute("messageType", MessageType.NOT_SIGNED_IN_INVALID_VERIFICATION_TOKEN);
                return "message";
            } else {
                if (verificationToken.isExpired()) {
                    model.addAttribute("messageType", MessageType.NOT_SIGNED_IN_EXPIRED_VERIFICATION_TOKEN);
                    return "message";
                } else {
                    userToVerify = verificationToken.getUser();
                }
            }
        }
        userToVerify.setVerified(true);
        userToVerify.setSuspended(false);
        userService.verifyUser(userToVerify);
        UserAuthenticationToken authenticationToken = new UserAuthenticationToken(userToVerify);
        authenticationToken.setDetails(new WebAuthenticationDetails(request));
        sessionAuthenticationStrategy.onAuthentication(authenticationToken, request, response);
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        userService.deleteVerificationTokens(VerificationToken.Type.EMAIL_VERIFICATION, userToVerify);
        applicationEventPublisher
                .publishEvent(new UserEvent(EventSource.WEB, userToVerify, userToVerify, EventType.VERIFY));
        model.addAttribute("messageType", MessageType.EMAIL_VERIFIED_SUCCESSFULLY);
        return "message";
    }

    @ResponseBody
    @RequestMapping(value = "/resendToken", method = RequestMethod.POST)
    public PostResult resendToken() {
        PostResult postResult = new PostResult();
        User user = userService.getCurrentUser();
        String message;
        if (user.isVerified()) {
            message = "The email <strong>" + user.getEmail() + "</strong> has already been verified";
        } else {
            userService.deleteVerificationTokens(VerificationToken.Type.EMAIL_VERIFICATION, user);
            applicationEventPublisher.publishEvent(new UserEvent(EventSource.WEB, user, user, EventType.REQUEST_TOKEN));
            message = "A verification link has been sent to <strong>" + user.getEmail() + "</strong>. Use it to "
                    + "verify your account within the next " + Constants.Server.VERIFICATION_EMAIL_EXPIRY + " hours.";
        }
        Map<String, Object> resultValues = new HashMap<>();
        resultValues.put("message", message);
        postResult.setSuccess(true);
        postResult.setValue(resultValues);
        user.setNagged(true);
        return postResult;
    }

    @RequestMapping(value = "/user/nag", method = RequestMethod.GET)
    public String nag(Model model) {
        User user = userService.getCurrentUser();
        if (user.isVerified()) {
            return "redirect:/";
        }
        model.addAttribute("user", user);
        return "user/nag";
    }

    @RequestMapping(value = "/user/verify", method = RequestMethod.GET)
    public String verify(@RequestParam("action") String action) {
        User user = userService.getCurrentUser();
        String page = "/";
        user.setNagged(true);
        return "redirect:" + page;
    }

    @ResponseBody
    @RequestMapping(value = "/user/edit", method = RequestMethod.POST)
    public PostResult edit(HttpServletRequest request, @RequestBody User user) {

        User current = userService.getCurrentUser();
        String password = current.getPassword();
        user.setPassword(password);
        user.setConfirmPassword(password);
        user.setRoles(current.getRoles());
        user.setEditMode(true);
        BindingResult result = new BeanPropertyBindingResult(user, "user");
        userValidator.validate(user, result);

        PostResult postResult = new PostResult();

        if (result.hasErrors()) {
            postResult.setSuccess(false);
            postResult.setPostErrors(controllerHelper.getPostErrors(result.getFieldErrors()));
        } else {

            userService.updateUser(user);
            user.setPassword(password);
            postResult.setSuccess(true);
            user = User.newInstance(user);
            User tenant;
            if (!user.getId().equals(user.getTenantUserId())) {
                tenant = User.newInstance(userService.findById(user.getTenantUserId()));
            } else {
                tenant = User.newInstance(user);
            }
            Map<String, User> userAndTenant = new HashMap<>();
            userAndTenant.put("user", user);
            userAndTenant.put("tenant", tenant);
            postResult.setValue(userAndTenant);
            applicationEventPublisher.publishEvent(new UserEvent(EventSource.WEB, user, user, EventType.MODIFY_USER));
        }
        return postResult;
    }

    @ResponseBody
    @RequestMapping(value = "/user/changeTenant", method = RequestMethod.POST)
    public PostResult changeTenant(HttpServletRequest request, HttpServletResponse response, @RequestBody TenantParameters tenantParameters) {
        User user = userService.getCurrentUser();
        String toJoin = tenantParameters.tenantCode;
        if (toJoin == null || toJoin.length() != 5) {
            return invalidTenantCode("Invalid host code!");
        }
        User beingJoined = userService.findByCode(toJoin);
        if (beingJoined == null) {
            String message = "Host not found";
            UserEvent userEvent = new UserEvent(EventSource.WEB, user, user, EventType.CHANGE_TENANT);
            userEvent.setResult(EventResult.FAILURE);
            userEvent.setFailureMessage(message);
            applicationEventPublisher.publishEvent(userEvent);
            return invalidTenantCode(message);
        }

        List<User.Role> roles;
        if (user.equals(beingJoined)) {
            roles = User.initialRoles();
        } else if (beingJoined.getId().equals(user.getTenantUserId())) {
            roles = user.getRoles();
        } else {
            roles = beingJoined.getDefaultRoles();
        }
        if (!user.isSuper()) {
            user.setRoles(roles);
        }
        user.setTenantUserId(beingJoined.getId());
        userService.updateUser(user);
        UserAuthenticationToken token = new UserAuthenticationToken(user);
        token.setDetails(new WebAuthenticationDetails(request));
        sessionAuthenticationStrategy.onAuthentication(token, request, response);
        SecurityContextHolder.getContext().setAuthentication(token);
        applicationEventPublisher.publishEvent(new UserEvent(EventSource.WEB, user, user, EventType.CHANGE_TENANT));

        Map<String, Object> resultValues = new HashMap<>();
        String message;
        if (user.equals(beingJoined)) {
            message = "You are now hosting personal projects. Others can collaborate with you using the code <strong>"
                    + user.getCode() + "</strong>. Click <a href=\"/\">here</a> to access your projects.";
        } else {
            message = "You are now collaborating with <strong>" + beingJoined.getFullName() + "</strong>. Click "
                    + "<a href=\"/\">here</a> to access their projects.";
        }
        resultValues.put("user", user);
        resultValues.put("tenant", beingJoined);
        resultValues.put("message", message);
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        postResult.setValue(resultValues);
        return postResult;
    }

    private PostResult invalidTenantCode(String message) {
        PostResult postResult = new PostResult();
        postResult.setSuccess(false);
        PostError postError = new PostError();
        postError.setParameter("tenantCode");
        postError.setMessage(message);
        postResult.setPostErrors(Collections.singletonList(postError));
        return postResult;
    }

    @RequestMapping(value = "/user/buy", method = RequestMethod.GET)
    public String buy(Model model) {
        model.addAttribute("user", userService.getCurrentUser());
        return "user/buy";
    }

    @ResponseBody
    @RequestMapping(value = "/user/changeEmail", method = RequestMethod.POST)
    public PostResult changeEmail(HttpServletRequest request, @RequestBody EmailParameters emailParameters) {
        User user = userService.getCurrentUser();
        PostResult postResult = new PostResult();
        String email = emailParameters.newEmail;
        String password = emailParameters.currentPassword;
        List<PostError> postErrors = validateUsernameAndPassword(user, email, password, "newEmail", "currentPassword");
        if (postErrors.size() > 0) {
            postResult.setSuccess(false);
            postResult.setPostErrors(postErrors);
        } else {
            User validate = new User();
            validate.setFirstName(user.getFirstName());
            validate.setId(user.getId());
            validate.setUsername(email);
            validate.setTenantUserId(user.getTenantUserId());
            validate.setPassword(password);
            validate.setConfirmPassword(password);
            BindingResult result = new BeanPropertyBindingResult(validate, "user");
            userValidator.validate(validate, result);

            if (result.hasErrors()) {
                postResult.setSuccess(false);
                postResult.setPostErrors(controllerHelper.getPostErrors(result.getFieldErrors()));
            } else {
                user.setUsername(email);
                userService.changeEmail(user);
                userService.deleteVerificationTokens(VerificationToken.Type.EMAIL_VERIFICATION, user);
                applicationEventPublisher
                        .publishEvent(new UserEvent(EventSource.WEB, user, user, EventType.CHANGE_EMAIL));
                user = userService.getCurrentUser();
                user.setVerified(false);
                user.setNagged(true);
                Map<String, Object> resultValues = new HashMap<>();
                String message = "Email changed! A verification link has been sent to <strong>" + email + "</strong>"
                        + ". Use it to verify your email within the next " + Constants.Server.VERIFICATION_EMAIL_EXPIRY
                        + " hours.";
                resultValues.put("message", message);
                resultValues.put("user", user);
                postResult.setSuccess(true);
                postResult.setValue(resultValues);
            }
        }
        return postResult;
    }

    @ResponseBody
    @RequestMapping(value = "/user/activateSampleProject", method = RequestMethod.POST)
    public PostResult setPublicProjectAccess(@RequestBody PublicSurveyAccessParameters publicSurveyAccessParameters) {
        User user = userService.getCurrentUser();
        int psa = publicSurveyAccessParameters.publicSurveyAccess;
        user.setPublicSurveyAccess(psa);
        userService.updateUser(user);
        applicationEventPublisher
                .publishEvent(new UserEvent(EventSource.MOBILE, user, user, EventType.ACTIVATE_PUBLIC_PROJECT_ACCESS));
        PostResult postResult = new PostResult();
        Map<String, Object> resultValues = new HashMap<>();
        String message;
        if (psa == PublicSurveyAccess.NONE) {
            message = "Sample project deactivated!";
        } else {
            message = "Sample project activated! Click <a href=\"/project/view/302/form\">here</a> to access it.";
        }
        resultValues.put("message", message);
        resultValues.put("user", user);
        postResult.setSuccess(true);
        postResult.setValue(resultValues);
        return postResult;
    }

    private List<PostError> validateUsernameAndPassword(User user, String email, String password,
            String emailParameterName, String passwordParameterName) {
        List<PostError> postErrors = new ArrayList<>();
        if (userService.userExists(email)) {
            postErrors.add(emailExistsError(emailParameterName));
        }
        if (!userService.verifyPassword(user, password)) {
            postErrors.add(passwordMatchError(passwordParameterName));
        }
        return postErrors;
    }

    private PostError emailExistsError(String emailParameterName) {
        PostError postError = new PostError();
        postError.setParameter(emailParameterName);
        postError.setMessage("Email already exists");
        return postError;
    }

    private PostError passwordMatchError(String passwordParameterName) {
        PostError postError = new PostError();
        postError.setParameter(passwordParameterName);
        postError.setMessage("Invalid password");
        return postError;
    }

    @ResponseBody
    @RequestMapping(value = "/user/changePassword", method = RequestMethod.POST)
    public PostResult changePassword(@RequestBody PasswordParameters passwordParameters) {
        User user = userService.getCurrentUser();
        user.setEditMode(true);
        PostResult postResult = new PostResult();
        String oldPassword = passwordParameters.oldPassword;
        String newPassword = passwordParameters.password;
        String confirmPassword = passwordParameters.confirmPassword;
        if (!userService.verifyPassword(user, oldPassword)) {
            postResult.setSuccess(false);
            postResult.setPostErrors(Arrays.asList(passwordMatchError("oldPassword")));
        } else {
            User validate = new User();
            validate.setFirstName(user.getFirstName());
            validate.setUsername(user.getUsername());
            validate.setTenantUserId(user.getTenantUserId());
            validate.setPassword(newPassword);
            validate.setConfirmPassword(confirmPassword);
            validate.setEditMode(true);
            BindingResult result = new BeanPropertyBindingResult(validate, "user");
            userValidator.validate(validate, result);

            if (result.hasErrors()) {
                postResult.setSuccess(false);
                postResult.setPostErrors(controllerHelper.getPostErrors(result.getFieldErrors()));
            } else {
                user.setPassword(newPassword);
                userService.changePassword(user);
                Map<String, Object> resultMap = new HashMap<>();
                resultMap.put("message", "Your password has been successfully changed!");
                postResult.setSuccess(true);
                postResult.setValue(resultMap);
                applicationEventPublisher
                        .publishEvent(new UserEvent(EventSource.WEB, user, user, EventType.CHANGE_PASSWORD));
            }
        }
        return postResult;
    }

    @RequestMapping(value = "/user/view", method = RequestMethod.GET)
    public String view(Model model) {
        User current = userService.getCurrentUser();
        User user = User.newInstance(current);
        User tenant = User.newInstance(userService.findById(user.getTenantUserId()));
        int noOfUsers = userService.findByTenantId(user.getId()).size();
        List<User> collaborators = userService.findByTenantId(user.getId());
        model.addAttribute("user", user);
        model.addAttribute("tenant", tenant);
        model.addAttribute("collaborators", collaborators);
        model.addAttribute("cashBalance", cashTransactionService.balance(user));
        model.addAttribute("creditBalance", creditTransactionService.balance(user));
        model.addAttribute("testRecordsQuota", creditTransactionService.testRecordsQuota(user));
        List<User.Role> roles = new ArrayList<>();
        roles.add(User.Role.DEVICE);
        roles.add(User.Role.IMPORTER);
        roles.add(User.Role.SMUGGLER);
        roles.add(User.Role.VIEWER);
        roles.add(User.Role.DOWNLOADER);
        model.addAttribute("defaultRoles", roles);
        model.addAttribute("roles", User.initialRoles());
        UserEvent userEvent = new UserEvent(EventSource.WEB, current, user, EventType.VIEW_USER);
        userEvent.putProperty(UserEvent.NO_OF_USERS, noOfUsers);
        applicationEventPublisher.publishEvent(userEvent);
        return "user/view";
    }

    @RequestMapping(value = "/password-recovery", method = RequestMethod.GET)
    public String sendPasswordResetToken() {
        User user = userService.getCurrentUser();
        if (user != null) {
            return "redirect:/";
        }
        return "passwordRecovery";
    }

    @PostMapping(value = "/password-recovery")
    public @ResponseBody PostResult sendPasswordResetToken(@RequestBody User user) {
        PostResult postResult = new PostResult();
        try {
            User existing = (User) userService.loadUserByUsername(user.getUsername());
            applicationEventPublisher
                    .publishEvent(new UserEvent(EventSource.WEB, existing, existing, EventType.FORGOT_PASSWORD));
            postResult.setSuccess(true);
            postResult.setValue(user.getUsername());
        } catch (UsernameNotFoundException ex) {
            postResult.setSuccess(false);
        }
        return postResult;
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
    public String resetPassword(Model model, @RequestParam("token") String token) {
        User user = userService.getCurrentUser();
        if (user != null) {
            return "redirect:/";
        }
        VerificationToken verificationToken = userService.getVerificationToken(token,
                VerificationToken.Type.PASSWORD_RESET);
        if (verificationToken == null) {
            model.addAttribute("messageType", MessageType.NOT_SIGNED_IN_INVALID_PASSWORD_TOKEN);
            return "message";
        }
        user = verificationToken.getUser();
        if (verificationToken.isExpired()) {
            model.addAttribute("messageType", MessageType.NOT_SIGNED_IN_EXPIRED_PASSWORD_TOKEN);
            return "message";
        }
        model.addAttribute("user", user);
        return "resetPassword";
    }

    @PostMapping(value = "/resetPassword")
    public @ResponseBody PostResult resetPassword(HttpServletRequest request, HttpServletResponse response,
            @RequestBody PasswordChangeParameters editor) {
        PostResult postResult = new PostResult();
        postResult.setSuccess(false);
        User user = userService.findById(editor.id);
        user.setPassword(editor.password);
        user.setConfirmPassword(editor.confirmPassword);
        user.setEditMode(true);
        User validate = new User();
        validate.setFirstName(user.getFirstName());
        validate.setUsername(user.getUsername());
        validate.setTenantUserId(user.getTenantUserId());
        validate.setPassword(user.getPassword());
        validate.setConfirmPassword(user.getConfirmPassword());
        validate.setEditMode(user.isEditMode());
        BindingResult result = new BeanPropertyBindingResult(validate, "user");
        userValidator.validate(validate, result);
        if (result.hasErrors()) {
            postResult.setPostErrors(controllerHelper.getPostErrors(result.getFieldErrors()));
        } else {
            userService.changePassword(user);
            updateAuthenticationInfo(user.getUsername(), editor.password, request, response);
            userService.deleteVerificationTokens(VerificationToken.Type.PASSWORD_RESET, user);
            applicationEventPublisher
                    .publishEvent(new UserEvent(EventSource.WEB, user, user, EventType.RESET_PASSWORD));
            postResult.setSuccess(true);
        }
        return postResult;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        User user = userService.getCurrentUser();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        if (user != null) {
            applicationEventPublisher.publishEvent(new UserEvent(EventSource.WEB, user, user, EventType.SIGN_OUT));
        }
        return "redirect:/";
    }

    @ResponseBody
    @PostMapping(value = "/user/expel")
    public PostResult expel(@RequestParam("userIds[]") List<Integer> userIds) {
        User current = userService.getCurrentUser();
        List<User> all = userService.findByTenantId(current.getId());
        List<User> expelled = new ArrayList<>();
        if (all != null && !all.isEmpty()) {
            List<Integer> ids = userIds;
            for (User user : all) {
                if (ids.contains(user.getId())) {
                    userService.expelUser(user);
                    applicationEventPublisher
                            .publishEvent(new UserEvent(EventSource.WEB, current, user, EventType.EXPEL_USER));
                    expelled.add(user);
                }
            }
        }
        all.removeAll(expelled);
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        Map<String, Object> resultValue = new HashMap<>();
        resultValue.put("collaborators", all);
        resultValue.put("expelled", expelled.size());
        postResult.setValue(resultValue);
        return postResult;
    }

    @ResponseBody
    @RequestMapping(value = "/user/assignRoles", method = RequestMethod.POST)
    public PostResult assignRoles(@RequestBody RoleAssignmentParameters editor) {
        User current = userService.getCurrentUser();
        List<User> all = userService.findByTenantId(current.getId());
        List<User> edited = new ArrayList<>();
        if (all != null && !all.isEmpty()) {
            List<Integer> ids = editor.getIds();
            for (User user : all) {
                if (ids.contains(user.getId())) {
                    User toSave = userService.findByUsername(user.getUsername());
                    if (toSave.isSuper()) {
                        continue;
                    }
                    List<User.Role> oldRoles = toSave.getRoles();
                    List<User.Role> newRoles = editor.getRoles() != null ? editor.getRoles() : new ArrayList<>();
                    Collections.sort(oldRoles);
                    Collections.sort(newRoles);
                    if (!newRoles.equals(oldRoles)) {
                        applicationEventPublisher
                                .publishEvent(new UserEvent(EventSource.WEB, current, toSave, EventType.MODIFY_ROLES));
                    }
                    toSave.setRoles(newRoles);
                    userService.updateUser(toSave);
                    edited.add(toSave);
                }
            }
            UserEvent userEvent = new UserEvent(EventSource.WEB, current, current, EventType.MODIFY_USERS);
            userEvent.putProperty(UserEvent.NO_OF_USERS, edited.size());
            applicationEventPublisher.publishEvent(userEvent);
        }
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        postResult.setValue(edited);
        return postResult;
    }

    @RequestMapping(value = "/loggedIn", method = RequestMethod.GET)
    public @ResponseBody Boolean isLoggedIn() {
        return SecurityContextHolder.getContext().getAuthentication() != null
                && SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
                // when Anonymous Authentication is enabled
                !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken);
    }

    @ResponseBody
    @GetMapping(value = "/user/cash/balance")
    public GetResult queryCashBalance() {
        User user = userService.getCurrentUser();
        BigDecimal balance = cashTransactionService.balance(user);
        GetResult getResult = new GetResult();
        getResult.setSuccess(true);
        getResult.setValue(balance);
        return getResult;
    }

    private static final class MessageType {

        private static final String INITIAL_VERIFICATION_TOKEN_SENT = "INITIAL_VERIFICATION_TOKEN_SENT";
        private static final String SUBSEQUENT_VERIFICATION_TOKEN_SENT = "SUBSEQUENT_VERIFICATION_TOKEN_SENT";
        private static final String EMAIL_VERIFIED_SUCCESSFULLY = "EMAIL_VERIFIED_SUCCESSFULLY";
        private static final String SIGNED_IN_VERIFIED = "SIGNED_IN_VERIFIED";
        private static final String SIGNED_IN_INVALID_VERIFICATION_TOKEN = "SIGNED_IN_INVALID_VERIFICATION_TOKEN";
        private static final String SIGNED_IN_EXPIRED_VERIFICATION_TOKEN = "SIGNED_IN_EXPIRED_VERIFICATION_TOKEN";
        private static final String NOT_SIGNED_IN_INVALID_VERIFICATION_TOKEN = "NOT_SIGNED_IN_INVALID_VERIFICATION_TOKEN";
        private static final String NOT_SIGNED_IN_EXPIRED_VERIFICATION_TOKEN = "NOT_SIGNED_IN_EXPIRED_VERIFICATION_TOKEN";
        private static final String PASSWORD_RESET_TOKEN_SENT = "PASSWORD_RESET_TOKEN_SENT";
        private static final String PASSWORD_RESET_SUCCESSFULLY = "PASSWORD_RESET_SUCCESSFULLY";
        private static final String NOT_SIGNED_IN_INVALID_PASSWORD_TOKEN = "NOT_SIGNED_IN_INVALID_PASSWORD_TOKEN";
        private static final String NOT_SIGNED_IN_EXPIRED_PASSWORD_TOKEN = "NOT_SIGNED_IN_EXPIRED_PASSWORD_TOKEN";
    }

    private final static class PasswordParameters {

        private String oldPassword;
        private String password;
        private String confirmPassword;

        public String getOldPassword() {
            return oldPassword;
        }

        public void setOldPassword(String oldPassword) {
            this.oldPassword = oldPassword;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getConfirmPassword() {
            return confirmPassword;
        }

        public void setConfirmPassword(String confirmPassword) {
            this.confirmPassword = confirmPassword;
        }
    }

    private final static class EmailParameters {

        private String newEmail;
        private String currentPassword;

        public String getNewEmail() {
            return newEmail;
        }

        public void setNewEmail(String newEmail) {
            this.newEmail = newEmail;
        }

        public String getCurrentPassword() {
            return currentPassword;
        }

        public void setCurrentPassword(String currentPassword) {
            this.currentPassword = currentPassword;
        }
    }

    private final static class TenantParameters {

        private String tenantCode;

        public String getTenantCode() {
            return tenantCode;
        }

        public void setTenantCode(String tenantCode) {
            this.tenantCode = tenantCode;
        }
    }

    private final static class PublicSurveyAccessParameters {

        private int publicSurveyAccess;

        public int getPublicSurveyAccess() {
            return publicSurveyAccess;
        }

        public void setPublicSurveyAccess(int publicSurveyAccess) {
            this.publicSurveyAccess = publicSurveyAccess;
        }
    }

    public final static class RoleAssignmentParameters {

        private List<Integer> ids;
        private List<User.Role> roles;

        public List<Integer> getIds() {
            return ids;
        }

        public void setIds(List<Integer> ids) {
            this.ids = ids;
        }

        public List<User.Role> getRoles() {
            return roles;
        }

        public void setRoles(List<User.Role> roles) {
            this.roles = roles;
        }
    }

    private static class PasswordChangeParameters {

        private int id;
        private String password;
        private String confirmPassword;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getConfirmPassword() {
            return confirmPassword;
        }

        public void setConfirmPassword(String confirmPassword) {
            this.confirmPassword = confirmPassword;
        }
    }

    private class UserAuthenticationToken extends AbstractAuthenticationToken {

        private final User principal;

        public UserAuthenticationToken(User principal) {
            super(principal.getAuthorities());
            super.setAuthenticated(true);
            this.principal = principal;
        }

        @Override
        public Object getCredentials() {
            return null;
        }

        @Override
        public User getPrincipal() {
            return principal;
        }

    }
}