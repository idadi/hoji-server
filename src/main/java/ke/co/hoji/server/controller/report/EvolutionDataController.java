package ke.co.hoji.server.controller.report;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.controller.report.model.Filter;
import ke.co.hoji.server.event.event.FieldEvent;
import ke.co.hoji.server.event.event.FormEvent;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.GetResult;
import ke.co.hoji.server.service.TenancyAccessService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("api/project/{projectId}/form/{formId}/")
public class EvolutionDataController extends BaseDataController {

    @ResponseBody
    @GetMapping(value = "report/evolution")
    public GetResult getFormData(Filter filter) {
        GetResult getResult = new GetResult();
        Form form = (Form) filter.getFieldParent();
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.VIEW);
        getResult.setSuccess(true);
        getResult.setValue(dataService.getEvolutionData(filter.toQueryFilter()));

        applicationEventPublisher.publishEvent(new FormEvent(EventSource.WEB, userService.getCurrentUser(), form, EventType.PIVOT));
        return getResult;
    }

    @ResponseBody
    @GetMapping(value = "/field/{fieldId}/report/evolution")
    public GetResult getFieldData(Filter filter) {
        GetResult getResult = new GetResult();
        Field field = (Field) filter.getFieldParent();
        tenancyAccessService.check(field.getForm().getSurvey(), TenancyAccessService.VIEW);
        getResult.setSuccess(true);
        getResult.setValue(dataService.getEvolutionData(filter.toQueryFilter()));

        applicationEventPublisher.publishEvent(new FieldEvent(EventSource.WEB, userService.getCurrentUser(), field, EventType.PIVOT));
        return getResult;
    }

}
