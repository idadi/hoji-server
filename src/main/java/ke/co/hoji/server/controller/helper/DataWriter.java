package ke.co.hoji.server.controller.helper;

import org.supercsv.io.CsvListWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

/**
 * Created by geoffreywasilwa on 10/07/2017.
 */
public class DataWriter {
    public static void write(List<List<String>> data, CsvPreference preference, File dataFile) {
        try (CsvListWriter writer = new CsvListWriter(new FileWriter(dataFile), preference)) {
            writer.writeHeader(data.get(0).toArray(new String[0]));
            for (int i = 1; i < data.size(); i++) {
                writer.write(data.get(i));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
