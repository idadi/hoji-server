package ke.co.hoji.server.controller;

import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.server.controller.helper.PropertyCsvProcessor;
import ke.co.hoji.server.exception.CsvFormatException;
import ke.co.hoji.server.model.PostResult;
import ke.co.hoji.server.service.TenancyAccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class PropertyController {

    @Autowired
    private PropertyService propertyService;

    @Autowired
    private TenancyAccessService tenancyAccessService;

    @Autowired
    private PropertyCsvProcessor propertyCsvProcessor;

    @GetMapping(value = {"property/{surveyId}/list"})
    public String list(@PathVariable("surveyId") Survey survey, ModelMap model) {
        List<Property> properties = propertyService.getProperties(survey.getId());
        model.addAttribute("survey", survey);
        model.addAttribute("properties", properties);
        return page("list");
    }

    @ResponseBody
    @PostMapping(value = { "project/{surveyId}/property", "project/{surveyId}/property/{key}/" }, consumes =
            MediaType.APPLICATION_JSON_VALUE)
    public PostResult save(@PathVariable("surveyId") Survey survey,
                           @PathVariable(value = "key", required = false) String key, @RequestBody Property property) {
        PostResult postResult = new PostResult();
        tenancyAccessService.check(survey, TenancyAccessService.MODIFY);
        if (key != null) {
            propertyService.deleteProperty(key, survey.getId());
        }
        Property propertyWithSurvey = new Property(property.getKey(), property.getValue(), survey);
        propertyService.setProperty(propertyWithSurvey);
        postResult.setValue(propertyWithSurvey);
        postResult.setSuccess(true);
        return postResult;
    }

    @ResponseBody
    @PostMapping( value = "project/{surveyId}/property", params = "action=upload")
    public PostResult upload(@PathVariable("surveyId") Survey survey,
                                    @RequestParam("propertiesCsv") MultipartFile propertiesCsv) {
        PostResult postResult = new PostResult();
        try {
            List<Property> properties = propertyCsvProcessor.process(propertiesCsv.getInputStream());
            List<Property> propertiesWithSurvey = properties.stream()
                    .map(property -> new Property(property.getKey(), property.getValue(), survey))
                    .collect(Collectors.toList());
            propertyService.setProperties(propertiesWithSurvey);
            postResult.setSuccess(true);
            postResult.setValue(propertiesWithSurvey);
        } catch (IOException e) {
            throw new CsvFormatException("Could not read uploaded csv file.");
        }
        return postResult;
    }

    @DeleteMapping(value = "project/{surveyId}/property/{key}/")
    public @ResponseBody PostResult delete(@PathVariable("surveyId") Survey survey, @PathVariable("key") String key) {
        tenancyAccessService.check(survey, TenancyAccessService.MODIFY);
        propertyService.deleteProperty(key, survey.getId());
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        return postResult;
    }

    private String page(String name) {
        return "property" + "/" + name;
    }
}