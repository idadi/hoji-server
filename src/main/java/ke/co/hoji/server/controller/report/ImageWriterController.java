package ke.co.hoji.server.controller.report;

import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.server.repository.MainRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.stream.Stream;

@Controller
public class ImageWriterController {

    @Autowired
    private ConfigService configService;

    @Autowired
    private MainRecordRepository mainRecordRepository;

    @GetMapping("/migrate")
    public String migrate() {
        configService.getSurveys()
                .stream()
                .flatMap(survey -> survey.getForms().stream())
                .filter(form -> form.getFields().stream().flatMap(field -> {
                    if (field.isMatrix()) {
                        return field.getChildren().stream();
                    }
                    return Stream.of(field);
                }).anyMatch(field -> field.getType().isImage()))
                .forEach(form -> mainRecordRepository.migrateImages(form.getId()));
        return "survey/list";
    }
}
