package ke.co.hoji.server.controller.model;

/**
 * <p>
 * Indicates which attributes of a {@link ke.co.hoji.core.data.model.Field} are to be bulk edited.
 * </p>
 * Created by geoffreywasilwa on 09/03/2017.
 */
public class FieldsEditorConfig {

    private boolean name;
    private boolean description;
    private boolean instructions;
    private boolean missingValue;
    private boolean column;
    private boolean captioning;
    private boolean inputFormat;
    private boolean defaultValue;
    private boolean minValue;
    private boolean maxValue;
    private boolean regexValue;
    private boolean uniqueness;
    private boolean searchable;
    private boolean filterable;
    private boolean outputType;
    private boolean pipeSource;
    private boolean choiceFilter;
    private boolean recordSources;
    private boolean referenceField;
    private boolean parent;
    private boolean calculated;
    private boolean valueScript;
    private boolean enableIfNull;
    private boolean validationScript;
    private boolean responseInheriting;

    public boolean isName() {
        if (noFieldAttributeConfigured()) {
            return true;
        }
        return name;
    }

    public void setName(boolean name) {
        this.name = name;
    }

    public boolean isDescription() {
        if (noFieldAttributeConfigured()) {
            return true;
        }
        return description;
    }

    public void setDescription(boolean description) {
        this.description = description;
    }

    public boolean isInstructions() {
        return instructions;
    }

    public void setInstructions(boolean instructions) {
        this.instructions = instructions;
    }

    public boolean isMissingValue() {
        return missingValue;
    }

    public void setMissingValue(boolean missingValue) {
        this.missingValue = missingValue;
    }

    public boolean isColumn() {
        return column;
    }

    public void setColumn(boolean column) {
        this.column = column;
    }

    public boolean isCaptioning() {
        return captioning;
    }

    public void setCaptioning(boolean captioning) {
        this.captioning = captioning;
    }

    public boolean isInputFormat() {
        return inputFormat;
    }

    public void setInputFormat(boolean inputFormat) {
        this.inputFormat = inputFormat;
    }

    public boolean isDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(boolean defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isMinValue() {
        return minValue;
    }

    public void setMinValue(boolean minValue) {
        this.minValue = minValue;
    }

    public boolean isMaxValue() {
        return maxValue;
    }

    public void setMaxValue(boolean maxValue) {
        this.maxValue = maxValue;
    }

    public boolean isRegexValue() {
        return regexValue;
    }

    public void setRegexValue(boolean regexValue) {
        this.regexValue = regexValue;
    }

    public boolean isUniqueness() {
        return uniqueness;
    }

    public void setUniqueness(boolean uniqueness) {
        this.uniqueness = uniqueness;
    }

    public boolean isSearchable() {
        return searchable;
    }

    public void setSearchable(boolean searchable) {
        this.searchable = searchable;
    }

    public boolean isFilterable() {
        return filterable;
    }

    public void setFilterable(boolean filterable) {
        this.filterable = filterable;
    }

    public boolean isOutputType() {
        return outputType;
    }

    public void setOutputType(boolean outputType) {
        this.outputType = outputType;
    }

    public boolean isPipeSource() {
        return pipeSource;
    }

    public void setPipeSource(boolean pipeSource) {
        this.pipeSource = pipeSource;
    }

    public boolean isChoiceFilter() {
        return choiceFilter;
    }

    public void setChoiceFilter(boolean choiceFilter) {
        this.choiceFilter = choiceFilter;
    }

    public boolean isRecordSources() {
        return recordSources;
    }

    public void setRecordSources(boolean recordSources) {
        this.recordSources = recordSources;
    }

    public boolean isReferenceField() {
        return referenceField;
    }

    public void setReferenceField(boolean referenceField) {
        this.referenceField = referenceField;
    }

    public boolean isParent() {
        return parent;
    }

    public void setParent(boolean parent) {
        this.parent = parent;
    }

    public boolean isCalculated() {
        return calculated;
    }

    public void setCalculated(boolean calculated) {
        this.calculated = calculated;
    }

    public boolean isValueScript() {
        return valueScript;
    }

    public void setValueScript(boolean valueScript) {
        this.valueScript = valueScript;
    }

    public boolean isEnableIfNull() {
        return enableIfNull;
    }

    public void setEnableIfNull(boolean enableIfNull) {
        this.enableIfNull = enableIfNull;
    }

    public boolean isValidationScript() {
        return validationScript;
    }

    public void setValidationScript(boolean validationScript) {
        this.validationScript = validationScript;
    }

    public boolean isResponseInheriting() {
        return responseInheriting;
    }

    public void setResponseInheriting(boolean responseInheriting) {
        this.responseInheriting = responseInheriting;
    }

    private boolean noFieldAttributeConfigured() {
        return !name && !description && !instructions && !missingValue && !column
            && !captioning && !inputFormat && !defaultValue && !missingValue && !minValue
            && !maxValue && !regexValue && !uniqueness && !searchable && !filterable
            && !outputType && !pipeSource && !choiceFilter && !recordSources && !referenceField
            && !parent && !calculated && !valueScript && !validationScript && !responseInheriting;
    }
}
