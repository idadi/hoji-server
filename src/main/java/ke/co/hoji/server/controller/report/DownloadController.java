package ke.co.hoji.server.controller.report;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.controller.helper.DataDictionaryWriter;
import ke.co.hoji.server.controller.helper.DataWriter;
import ke.co.hoji.server.controller.report.model.Filter;
import ke.co.hoji.server.event.event.FieldEvent;
import ke.co.hoji.server.event.event.FormEvent;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.Constants;
import ke.co.hoji.server.service.TenancyAccessService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by geoffreywasilwa on 03/11/2016.
 */
@Controller
@RequestMapping("api/project/{projectId}/form/{formId}/")
public class DownloadController extends BaseDataController {

    private static final int BUFFER_SIZE = 1024;
    private static final String TMP_DIR = System.getProperty("java.io.tmpdir");

    @GetMapping(value = "/report/download")
    public void downloadForm(
            @RequestParam(value = "cleanDownloadHeaders", required = false) boolean cleanDownloadHeaders, Filter filter,
            HttpServletResponse response) throws ParseException {
        Form form = (Form) filter.getFieldParent();
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.VIEW);

        applicationEventPublisher
                .publishEvent(new FormEvent(EventSource.WEB, userService.getCurrentUser(), form, EventType.CSV));
        download(filter, cleanDownloadHeaders, response);
    }

    @GetMapping(value = "/field/{fieldId}/report/download")
    public void downloadField(
            @RequestParam(value = "cleanDownloadHeaders", required = false) boolean cleanDownloadHeaders, Filter filter,
            HttpServletResponse response) throws ParseException {
        Field field = (Field) filter.getFieldParent();
        tenancyAccessService.check(field.getForm().getSurvey(), TenancyAccessService.VIEW);

        applicationEventPublisher
                .publishEvent(new FieldEvent(EventSource.WEB, userService.getCurrentUser(), field, EventType.CSV));
        download(filter, cleanDownloadHeaders, response);
    }

    private void download(Filter filter, boolean cleanHeaders, HttpServletResponse response) {
        List<List<String>> data =
                dataService.getRawData(filter.toQueryFilter(), cleanHeaders);
        if (data != null) {
            CsvPreference preference = CsvPreference.STANDARD_PREFERENCE;
            try {
                String parentDir = String.valueOf(System.nanoTime());
                Files.createDirectory(Paths.get(TMP_DIR, parentDir));
                String name = filter.getFieldParent().getName();
                Path dictionaryPath = Paths.get(TMP_DIR, parentDir, name + " - Dictionary.csv");
                Path dataPath = Paths.get(TMP_DIR, parentDir, name + " - Data.csv");
                File codeBook = Files.createFile(dictionaryPath).toFile();
                DataDictionaryWriter.write(filter.getFieldParent(), preference, codeBook);
                File dataFile = Files.createFile(dataPath).toFile();
                DataWriter.write(data, preference, dataFile);
                File zipFile = zipFiles(dataFile);
                writeToResponse(filter, response, zipFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private File zipFiles(File... filesToZip) {
        byte[] buffer = new byte[BUFFER_SIZE];
        File zipFile = null;
        try {
            zipFile = File.createTempFile("zip", ".zip");
            try (
                    FileOutputStream fos = new FileOutputStream(zipFile);
                    ZipOutputStream zos = new ZipOutputStream(fos);) {
                for (File file : filesToZip) {
                    ZipEntry entry = new ZipEntry(file.getName());
                    zos.putNextEntry(entry);
                    FileInputStream in = new FileInputStream(file);
                    int len;
                    while ((len = in.read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                    in.close();
                    zos.closeEntry();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return zipFile;
    }

    private void writeToResponse(Filter filter, HttpServletResponse response, File zipFile) throws IOException {
        try (
                FileInputStream inputStream = new FileInputStream(zipFile);
                OutputStream outStream = response.getOutputStream();
        ) {
            response.setContentType("application/octet-stream");
            response.setContentLength((int) zipFile.length());
            // set headers for the response
            String headerKey = "Content-Disposition";
            String fileName = filter.getFieldParent().getName() + ".zip";
            String headerValue = String.format("attachment; filename=\"%s\"", fileName);
            response.setHeader(headerKey, headerValue);

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;

            // write bytes read from the input stream into the output stream
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
        }
    }
}
