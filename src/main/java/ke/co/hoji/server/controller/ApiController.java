package ke.co.hoji.server.controller;

import static ke.co.hoji.core.data.Constants.Device.CODE_TO_ID_MILESTONE;
import static ke.co.hoji.core.data.Constants.Device.UNIQUE_NAME_MILESTONE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.PublicSurveyAccess;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.Survey;
import ke.co.hoji.core.data.http.AuthenticationRequest;
import ke.co.hoji.core.data.http.AuthenticationResponse;
import ke.co.hoji.core.data.http.ChangeTenantRequest;
import ke.co.hoji.core.data.http.ChangeTenantResponse;
import ke.co.hoji.core.data.http.FieldsRequest;
import ke.co.hoji.core.data.http.FieldsResponse;
import ke.co.hoji.core.data.http.FormsRequest;
import ke.co.hoji.core.data.http.FormsResponse;
import ke.co.hoji.core.data.http.ImportRequest;
import ke.co.hoji.core.data.http.ImportResponse;
import ke.co.hoji.core.data.http.LanguagesRequest;
import ke.co.hoji.core.data.http.LanguagesResponse;
import ke.co.hoji.core.data.http.PropertiesRequest;
import ke.co.hoji.core.data.http.PropertiesResponse;
import ke.co.hoji.core.data.http.ReferencesRequest;
import ke.co.hoji.core.data.http.ReferencesResponse;
import ke.co.hoji.core.data.http.SearchRequest;
import ke.co.hoji.core.data.http.SearchResponse;
import ke.co.hoji.core.data.http.SignUpRequest;
import ke.co.hoji.core.data.http.SignUpResponse;
import ke.co.hoji.core.data.http.SurveyRequest;
import ke.co.hoji.core.data.http.SurveyResponse;
import ke.co.hoji.core.data.http.SurveysRequest;
import ke.co.hoji.core.data.http.SurveysResponse;
import ke.co.hoji.core.data.http.TranslationsRequest;
import ke.co.hoji.core.data.http.TranslationsResponse;
import ke.co.hoji.core.data.http.UploadRequest;
import ke.co.hoji.core.data.http.UploadResponse;
import ke.co.hoji.core.data.http.UploadResult;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.core.service.model.SurveyService;
import ke.co.hoji.server.event.event.MainRecordEvent;
import ke.co.hoji.server.event.event.SurveyEvent;
import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.DataService;
import ke.co.hoji.server.service.DownloadService;
import ke.co.hoji.server.service.ImportService;
import ke.co.hoji.server.service.TenancyAccessService;
import ke.co.hoji.server.service.UploadService;
import ke.co.hoji.server.service.UserService;

/**
 * Created by gitahi on 05/07/15.
 */
@RestController
@RequestMapping("/api")
public class ApiController {

    private static final Logger logger = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private FormService formService;

    @Autowired
    private UserService userService;

    @Autowired
    private DownloadService downloadService;

    @Autowired
    private UploadService uploadService;

    @Autowired
    private ImportService importService;

    @Autowired
    protected TenancyAccessService tenancyAccessService;

    @Autowired
    ApplicationEventPublisher eventPublisher;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    private ObjectMapper objectMapper;

    @RequestMapping(value = "/user/signUp", method = RequestMethod.POST)
    public SignUpResponse signUp(@RequestBody SignUpRequest signUpRequest) {
        SignUpResponse response = new SignUpResponse(SignUpResponse.StatusCode.FAILED);
        User user = new User();
        user.setFullName(signUpRequest.getName());
        user.setUsername(signUpRequest.getUsername());
        user.setPassword(signUpRequest.getPassword());
        if (signUpRequest.getAppVersionCode() != null
            && signUpRequest.getAppVersionCode() >= 188) {
            user.setPublicSurveyAccess(PublicSurveyAccess.NONE);
        } else {
            user.setPublicSurveyAccess(PublicSurveyAccess.VERIFIED);
        }
        if (userService.userExists(user.getUsername())) {
            response.setStatusCode(SignUpResponse.StatusCode.EXISTS);
        } else {
            if (signUp(user)) {
                response.setStatusCode(SignUpResponse.StatusCode.SUCCEEDED);
                response.setUserId(user.getId());
                response.setUserCode(user.getCode());
            }
        }
        return response;
    }

    private boolean signUp(User user) {
        userService.createUser(user);
        applicationEventPublisher.publishEvent(new UserEvent(EventSource.MOBILE, user, user, EventType.SIGN_UP));
        applicationEventPublisher.publishEvent(new UserEvent(EventSource.MOBILE, user, user, EventType.WELCOME));
        return true;
    }

    @RequestMapping(value = "/user/authenticate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public AuthenticationResponse authenticate(@RequestBody AuthenticationRequest request) {
        Authentication token = new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
        Authentication authenticatedUser;
        AuthenticationResponse response = new AuthenticationResponse();
        try {
            authenticatedUser = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
            User user = userService.getCurrentUser();
            User tenant = userService.findById(user.getTenantUserId());
            response.setAuthenticated(true);
            response.setAuthorized(true);
            applicationEventPublisher.publishEvent(new UserEvent(EventSource.MOBILE, user, user, EventType.SIGN_IN));
            response.setUserId(user.getId());
            response.setName(user.getFullName());
            response.setUserCode(user.getCode());
            response.setTenantCode(tenant.getCode());
            response.setTenantName(tenant.getFullName());
            Integer surveyId = request.getSurveyId();
            if (surveyId != null) {
                SurveyResponse surveyResponse = downloadService.downloadSurvey(request.getSurveyId());
                Survey survey = surveyResponse.getSurvey();
                if (survey != null) {
                    tenancyAccessService.check(surveyResponse.getSurvey(), TenancyAccessService.VIEW);
                }
            }
        } catch (AuthenticationException ex) {
            response.setAuthenticated(false);
        } catch (AccessDeniedException ex) {
            response.setAuthorized(false);
        }
        return response;
    }

    @RequestMapping(value = {"/download/surveys", "/download/projects"}, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public SurveysResponse downloadSurveys(@RequestBody SurveysRequest request) {
        User user = userService.getCurrentUser();
        Integer psa = request.getPublicSurveyAccess();
        if (psa != null && (psa == PublicSurveyAccess.VERIFIED || psa == PublicSurveyAccess.ALL)) {
            user.setPublicSurveyAccess(request.getPublicSurveyAccess());
            userService.updateUser(user);
            applicationEventPublisher.publishEvent(new UserEvent(EventSource.MOBILE, user, user, EventType.ACTIVATE_PUBLIC_PROJECT_ACCESS));
        }
        User tenant = userService.findById(user.getTenantUserId());
        SurveysResponse response = downloadService.getSurveys(user);
        response.setTenantCode(tenant.getCode());
        response.setTenantName(tenant.getFullName());
        UserEvent userEvent = new UserEvent(EventSource.MOBILE, user, user, EventType.PROJECTS);
        userEvent.putProperty(UserEvent.NO_OF_SURVEYS, response.getSurveyResponses().size());
        applicationEventPublisher.publishEvent(userEvent);
        return response;
    }

    @RequestMapping(value = {"/download/survey", "/download/project"}, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public SurveyResponse downloadSurvey(@RequestBody SurveyRequest request) {
        SurveyResponse surveyResponse =
            downloadService.downloadSurvey(request.getSurveyId(), request.getAppVersionCode());
        Survey survey = surveyResponse.getSurvey();
        if (survey != null) {
            tenancyAccessService.check(surveyResponse.getSurvey(), TenancyAccessService.VIEW);
            applicationEventPublisher.publishEvent(new SurveyEvent(EventSource.MOBILE, userService.getCurrentUser(),
                surveyService.getSurveyById(survey.getId()), EventType.DOWNLOAD));
        }
        return surveyResponse;
    }

    @RequestMapping(value = "/download/forms", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public FormsResponse downloadForms(@RequestBody FormsRequest request) {
        return downloadService.downloadForms(request.getSurveyId());
    }

    @RequestMapping(value = "/download/fields", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public FieldsResponse downloadFields(@RequestBody FieldsRequest request) {
        return downloadService.downloadFields(request.getFormId());
    }

    @RequestMapping(value = "/download/references", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ReferencesResponse downloadReferences(@RequestBody ReferencesRequest request) {
        return downloadService.downloadReferences(request.getSurveyId());
    }

    @RequestMapping(value = "/download/properties", method = RequestMethod.POST, consumes =
        MediaType.APPLICATION_JSON_VALUE)
    public PropertiesResponse downloadProperties(@RequestBody PropertiesRequest request) {
        return downloadService.downloadProperties(request.getSurveyId());
    }

    @RequestMapping(value = "/download/languages", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public LanguagesResponse downloadLanguages(@RequestBody LanguagesRequest request) {
        return downloadService.downloadLanguages(request.getSurveyId());
    }

    @RequestMapping(value = "/download/translations", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public TranslationsResponse downloadTranslations(@RequestBody TranslationsRequest request) {
        return downloadService.downloadTranslations(request.getLanguages());
    }

    @RequestMapping(value = "/upload/data", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public UploadResponse upload(@RequestBody UploadRequest request) {
        try {
            logger.debug("Processing upload of {}", objectMapper.writeValueAsString(request));
        } catch (JsonProcessingException e) {
            logger.error(e.getMessage(), e);
        }
        UploadResponse response = new UploadResponse();
        Map<String, UploadResult> results = new HashMap<>();
        Integer appVersionCode = request.getAppVersionCode();
        if (appVersionCode == null || appVersionCode <= CODE_TO_ID_MILESTONE) {
            logger.error("Unable to process upload because sending app version ({}) is too old.", appVersionCode);
            response.setMessage("App version too old");
        } else {
            ke.co.hoji.core.data.model.Survey survey = surveyService.getSurveyById(request.getSurveyId());
            if (survey == null) {
                logger.error("Unable to process upload because project [id= {}] no longer exists.", request.getSurveyId());
                response.setMessage("Project no longer exists");
            } else {
                tenancyAccessService.check(survey, TenancyAccessService.VIEW);
                Set<String> disabledForms = new HashSet<>();
                List<String> failedFields = new ArrayList<>();
                if (!survey.isEnabled()) {
                    logger.error("Unable to process upload because project [id= {}] is disabled.", survey.getId());
                    response.setMessage("Project disabled");
                } else if (survey.getStatus() != ke.co.hoji.core.data.model.Survey.PUBLISHED) {
                    logger.error("Unable to process upload because project [id= {}] has not been published.", survey.getId());
                    response.setMessage("Project not published");
                } else {
                    // Temporary cache for efficiency
                    Map<Integer, Form> existingForms = new HashMap<>();
                    for (MainRecord mainRecord : request.getMainRecords()) {
                        Form form = existingForms.get(mainRecord.getFormId());
                        if (form == null) {
                            form = formService.getFormById(mainRecord.getFormId());
                            if (form != null) {
                                existingForms.put(form.getId(), form);
                            }
                        }
                        if (form == null) {
                            logger.error("Unable to process upload because form [id= {}] no longer exists.", mainRecord.getFormId());
                            response.setMessage("Some form(s) no longer exist");
                        } else if (!form.isEnabled()) {
                            logger.error("Unable to process upload because form [id= {}] is disabled.", form.getId());
                            disabledForms.add(form.getName());
                            response.setMessage("Form" + (disabledForms.size() == 1 ? "" : "s") + " '"
                                + Utils.string(disabledForms) + "' " + (disabledForms.size() == 1 ?
                                " is " : " are ") + " disabled");
                        } else {
                            UploadResult uploadResult;
                            try {
                                if (request.getAppVersionCode() < UNIQUE_NAME_MILESTONE) {
                                    logger.error(
                                        "Unable to process upload because sending app version ({}) is too old.",
                                        appVersionCode
                                    );
                                    response.setMessage("Update app");
                                }
                                uploadResult = uploadService.uploadRecord(form, mainRecord, request.getAppVersionCode());
                                if (uploadResult.getCode() == UploadResult.SUCCESS) {
                                    results.put(mainRecord.getUuid(), uploadResult);
                                } else if (uploadResult.getCode() == UploadResult.INSUFFICIENT_CREDITS) {
                                    results.put(mainRecord.getUuid(), null);
                                    response.setMessage("Insufficient credits");
                                }
                            } catch (RuntimeException ex) {
                                // Any RuntimeException will have caused a transaction rollback so treat it as a failed upload.
                                results.put(mainRecord.getUuid(), null);
                                if (mainRecord.getCaption() != null && !mainRecord.getCaption().equals("")) {
                                    failedFields.add(mainRecord.getCaption());
                                } else {
                                    failedFields.add(mainRecord.getUuid());
                                }
                                // In case there are already other errors just view those instead.
                                if (response.getMessage() == null) {
                                    response.setMessage(ex.getClass().getSimpleName() + ": Could not upload "
                                        + Utils.string(failedFields));
                                }
                                logger.error(ex.getMessage(), ex);
                            }
                        }
                    }
                }
            }
        }
        response.setResults(results);
        return response;
    }

    @RequestMapping(value = "/import/search", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public SearchResponse search(@RequestBody SearchRequest searchRequest) {
        String wildCard = "*";
        User user = userService.getCurrentUser();
        if (user.isImporter() || user.isSmuggler()) {
            if (searchRequest.getSearchTerms().equals(wildCard)) {
                searchRequest.setSearchTerms("-");
            }
            if (!user.isSmuggler()) {
                searchRequest.setUserId(user.getId());
            }
            return importService.search(searchRequest);
        } else {
            throw new AccessDeniedException("Access denied");
        }
    }

    @RequestMapping(value = "/import/record", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ImportResponse downloadRecord(@RequestBody ImportRequest importRequest) {
        User user = userService.getCurrentUser();
        if (!user.isImporter() && !user.isSmuggler()) {
            throw new AccessDeniedException("Access denied");
        }
        ImportResponse importResponse = importService.getRecord(importRequest);
        if (!importResponse.getMainRecordMap().isEmpty()) {
            Map.Entry<String, List<MainRecord>> entry = importResponse.getMainRecordMap().entrySet().iterator().next();
            MainRecord mainRecord = entry.getValue().get(0);
            MainRecordEvent mainRecordEvent = new MainRecordEvent(
                EventSource.MOBILE,
                userService.getCurrentUser(),
                mainRecord,
                EventType.IMPORT
            );
            applicationEventPublisher.publishEvent(mainRecordEvent);
        }
        return importResponse;
    }

    @RequestMapping(value = "/user/changeTenant", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ChangeTenantResponse changeTenant(@RequestBody ChangeTenantRequest changeRequest) {
        User user = userService.findById(changeRequest.getUserId());
        String toJoin = changeRequest.getTenantCode();
        if (toJoin == null || "".equals(toJoin)) {
            return ChangeTenantResponse.tenantNotFound();
        }
        User beingJoined = userService.findByCode(toJoin);
        if (beingJoined == null) {
            UserEvent userEvent = new UserEvent(EventSource.MOBILE, user, user, EventType.CHANGE_TENANT);
            userEvent.setResult(EventResult.FAILURE);
            userEvent.setFailureMessage("Tenant not found");
            applicationEventPublisher.publishEvent(userEvent);
            return ChangeTenantResponse.tenantNotFound();
        }
        List<User.Role> roles;
        if (user.equals(beingJoined)) {
            roles = User.initialRoles();
        } else {
            roles = beingJoined.getDefaultRoles();
        }
        if (!user.isSuper()) {
            user.setRoles(roles);
        }
        user.setTenantUserId(beingJoined.getId());
        userService.updateUser(user);
        applicationEventPublisher.publishEvent(new UserEvent(EventSource.MOBILE, user, user, EventType.CHANGE_TENANT));
        return new ChangeTenantResponse(ChangeTenantResponse.Code.TENANT_FOUND, toJoin, beingJoined.getFullName());
    }

    @RequestMapping(value = "/user/requestAccess", method = RequestMethod.POST)
    public Boolean requestAccess() {
        User user = userService.getCurrentUser();
        applicationEventPublisher.publishEvent(new UserEvent(EventSource.MOBILE, user, user, EventType.REQUEST_ACCESS));
        return Boolean.TRUE;
    }
}
