package ke.co.hoji.server.controller.report;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.TenancyAccessService;

/**
 * Created by geoffreywasilwa on 07/11/2016.
 */
@Controller
@RequestMapping("/project/{projectId}/form/{formId}/")
public class ReportController extends BaseDataController {

    @GetMapping(value = "/report/**")
    public String getFormFilter(
            @PathVariable Integer formId,
            ModelMap model
    ) {
        Form form = configService.getForm(formId);
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.VIEW);
        List<Field> choiceFields = form.getFields().stream()
                .filter(f -> f.getType().isChoice() && !f.getType().isMultiChoice())
                .collect(Collectors.toList());
        model.addAttribute("form", form);
        model.addAttribute("users", getUsers(form));
        model.addAttribute("fields", choiceFields);
        if (userService.getCurrentUser() == null) {
            model.addAttribute("anonymousUser", true);
        } else {
            model.addAttribute("fullName", userService.getCurrentUser().getFullName());
            model.addAttribute("canfly", userService.getCurrentUser().getRoles().contains(User.Role.SUPER));
        }
        return "form/report";
    }

    @GetMapping(value = "/field/{fieldId}/report/**")
    public String getFieldFilter(
            @PathVariable Integer fieldId,
            ModelMap model
    ) {
        Field field = configService.getField(fieldId);
        tenancyAccessService.check(field.getForm().getSurvey(), TenancyAccessService.VIEW);
        List<Field> parentFilterFields = field.getForm().getChildren()
                .stream()
                .filter(f -> f.isSearchable() && (f.getType().isChoice() || !f.getType().isMultiChoice()))
                .collect(Collectors.toList());
        List<Field> childrenFilterFields = field.getChildren().stream()
                .filter(f -> f.getType().isChoice() && !f.getType().isMultiChoice())
                .collect(Collectors.toList());
        List<Field> filterFields = new ArrayList<>();
        filterFields.addAll(parentFilterFields);
        filterFields.addAll(childrenFilterFields);
        model.addAttribute("field", field);
        model.addAttribute("users", getUsers(field.getForm()));
        model.addAttribute("fields", filterFields);
        if (userService.getCurrentUser() == null) {
            model.addAttribute("anonymousUser", true);
        } else {
            model.addAttribute("fullName", userService.getCurrentUser().getFullName());
            model.addAttribute("canfly", userService.getCurrentUser().getRoles().contains(User.Role.SUPER));
        }
        return "/field/report";
    }

    private List<UserShort> getUsers(Form form) {
        User currentUser = userService.getCurrentUser();
        if (currentUser == null) {
            return Collections.emptyList();
        }
        User tenant = userService.findById(currentUser.getTenantUserId());
        List<User> usersUnderTenant = userService.findByTenantId(tenant.getId());
        List<UserShort> users = new ArrayList<>();
        users.add(new UserShort(tenant));
        for (User userUnderTenant : usersUnderTenant) {
            users.add(new UserShort(userUnderTenant));
        }
        List<User> usersWithRecords = dataService.getUsersWithRecords(form);
        for (User userWithRecords : usersWithRecords) {
            UserShort userShort = new UserShort(userWithRecords);
            if (!users.contains(userShort)) {
                users.add(userShort);
            }
        }
        Collections.sort(users);
        return users;
    }

    public class UserShort implements Comparable {

        private Integer id;
        private String username;
        private String fullName;

        public UserShort(User user) {
            this.id = user.getId();
            this.username = user.getUsername();
            this.fullName = user.getFullName();
        }

        public Integer getId() {
            return id;
        }

        public String getUsername() {
            return username;
        }

        public String getFullName() {
            return fullName;
        }

        @Override
        public int compareTo(Object that) {
            return this.fullName.compareToIgnoreCase(((UserShort) that).fullName);
        }

        @Override
        public boolean equals(Object other) {
            if (this == other) {
                return true;
            }
            if (other == null) {
                return false;
            }
            if (getClass() != other.getClass()) {
                return false;
            }
            UserShort that = (UserShort) other;
            if (this.id == null && that.id == null) {
                return true;
            }
            if (this.id != null && that.id != null) {
                return this.id.equals(that.id);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return id != null ? id.hashCode() : 0;
        }
    }
}
