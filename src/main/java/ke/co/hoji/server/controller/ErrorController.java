package ke.co.hoji.server.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.UserService;

/**
 * Created by gitahi on 25/10/15.
 */
@Controller
public class ErrorController {

    private final Logger logger = LoggerFactory.getLogger(ErrorController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/error")
    public String error(@RequestParam(required = false) Integer statusCode, HttpServletRequest request,
            HttpServletResponse response, Model model) {
        User user = userService.getCurrentUser();
        if (user != null) {
            Integer tenantId = user.getTenantUserId();
            if (tenantId == null) {
                tenantId = user.getId();
            }
            User tenant = userService.findById(tenantId);
            model.addAttribute("userFullName", user.getFullName());
            model.addAttribute("tenantFullName", tenant.getFullName());
        }
        if (statusCode == null) {
            statusCode = response.getStatus();
        }
        String message = "Unknown error";
        try {
            message = HttpStatus.valueOf(statusCode).getReasonPhrase();
        } catch (IllegalArgumentException e) {
            logger.error(e.getMessage(), e);
        }
        model.addAttribute("statusCode", statusCode);
        model.addAttribute("message", message);
        return "error";
    }
}
