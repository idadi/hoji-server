package ke.co.hoji.server.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.FieldService;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.core.service.model.SurveyService;
import ke.co.hoji.server.controller.helper.ControllerHelper;
import ke.co.hoji.server.event.event.ConfigEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.exception.NotFoundException;
import ke.co.hoji.server.model.PostResult;
import ke.co.hoji.server.repository.MainRecordRepository;
import ke.co.hoji.server.service.PublicationService;
import ke.co.hoji.server.service.TenancyAccessService;
import ke.co.hoji.server.service.UserService;
import ke.co.hoji.server.validator.FormValidator;

/**
 * Created by gitahi on 21/10/15.
 */
@Controller
@RequestMapping("/form")
public class FormController {

    public static class BulkFormEditor {

        private List<Integer> ids;
        private Integer gpsCapture;

        public BulkFormEditor() {
        }

        public void setIds(List<Integer> ids) {
            this.ids = new ArrayList<>(ids);
        }

        public boolean shouldUpdateGpsCapture() {
            return !gpsCapture.equals(-999);
        }

        public List<Form> updateForms(List<Form> forms) {
            List<Form> editedForms = new ArrayList<>();
            for (Form form : forms) {
                if (ids.contains(form.getId())) {
                    if (shouldUpdateGpsCapture()) {
                        form.setGpsCapture(gpsCapture);
                    }
                    editedForms.add(form);
                }
            }
            return editedForms;
        }
    }

    @Autowired
    private FieldService fieldService;

    @Autowired
    private FormService formService;

    @Autowired
    private SurveyService surveyService;

    @Autowired
    private PublicationService publicationService;

    @Autowired
    private FormValidator formValidator;

    @Autowired
    private UserService userService;

    @Autowired
    private TenancyAccessService tenancyAccessService;

    @Autowired
    private ControllerHelper controllerHelper;

    @Autowired
    private MainRecordRepository mainRecordRepository;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @InitBinder("form")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(formValidator);
    }

    @RequestMapping(value = { "/{surveyId}/list" }, method = RequestMethod.GET)
    public String list(@PathVariable("surveyId") Integer surveyId, ModelMap model) {
        Survey survey = surveyService.getSurveyById(surveyId);
        List<Form> forms = formService.getFormsBySurvey(survey);
        Collections.sort(forms);
        tenancyAccessService.check(survey, TenancyAccessService.VIEW);
        model.addAttribute("forms", forms);
        model.addAttribute("survey", survey);
        ConfigEvent configEvent = new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), survey,
                EventType.LIST);
        configEvent.putProperty(ConfigEvent.NO_OF_FORMS, forms.size());
        applicationEventPublisher.publishEvent(configEvent);
        return page("list");
    }

    @RequestMapping(value = "/view/{id}/field", method = RequestMethod.GET)
    public String viewFields(@PathVariable Integer id, Model model) {
        Form form = formService.getFormById(id);
        checkAccess(form, TenancyAccessService.VIEW);
        form.setCredits(formService.computeCredits(form));
        model.addAttribute("form", form);
        model.addAttribute("fields", fieldService.getAllFields(form));
        model.addAttribute("noOfRecords", mainRecordRepository.getCountOfRecords(form));
        applicationEventPublisher
                .publishEvent(new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), form, EventType.VIEW));
        return page("view-field");
    }

    @RequestMapping(value = "/view/{id}/visualization", method = RequestMethod.GET)
    public String viewVisualizations(@PathVariable Integer id, Model model) {
        Form form = formService.getFormById(id);
        checkAccess(form, TenancyAccessService.VIEW);
        form.setCredits(formService.computeCredits(form));
        model.addAttribute("form", form);
        model.addAttribute("noOfRecords", mainRecordRepository.getCountOfRecords(form));
        applicationEventPublisher
                .publishEvent(new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), form, EventType.VIEW));
        return page("view-visualization");
    }

    @ResponseBody
    @RequestMapping(value = "/{surveyId}/save", method = RequestMethod.POST)
    public PostResult save(@PathVariable("surveyId") Integer surveyId, @RequestBody Form form, BindingResult result) {
        Survey survey = surveyService.getSurveyById(surveyId);
        form.setSurvey(survey);
        if (form.getOrdinal() == null) {
            form.setOrdinal(Utils.nextOrdinal(new ArrayList<>(formService.getFormsBySurvey(survey))));
        }
        formValidator.validate(form, result);
        PostResult postResult = new PostResult();
        if (result.hasErrors()) {
            postResult.setPostErrors(controllerHelper.getPostErrors(result.getFieldErrors()));
        } else {
            checkAccess(form, TenancyAccessService.MODIFY);
            boolean isNew = false;
            if (form.getId() == null) {
                form.setMajorVersion(0);
                form.setMinorVersion(0);
                isNew = true;
            }
            form.setCredits(formService.computeCredits(form));
            form.setEnabled(true);
            formService.saveForm(form);
            postResult.setSuccess(true);
            postResult.setValue(form);
            if (isNew) {
                applicationEventPublisher.publishEvent(
                        new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), form, EventType.CREATE));
            } else {
                applicationEventPublisher.publishEvent(
                        new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), form, EventType.MODIFY));
            }
        }
        return postResult;
    }

    @ResponseBody
    @RequestMapping(value = "{surveyId}/bulk-save", method = RequestMethod.POST)
    public PostResult bulkSave(@PathVariable Integer surveyId, @RequestBody BulkFormEditor bulkFormEditor) {
        Survey survey = surveyService.getSurveyById(surveyId);
        List<Form> allForms = formService.getFormsBySurvey(survey);
        List<Form> editedForms = new ArrayList<>();
        if (allForms != null && !allForms.isEmpty()) {
            tenancyAccessService.check(survey, TenancyAccessService.MODIFY);
            editedForms = bulkFormEditor.updateForms(allForms);
            formService.modifyForms(editedForms);
            ConfigEvent configEvent = new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), survey,
                    EventType.BULK_EDIT);
            configEvent.putProperty(ConfigEvent.NO_OF_FORMS, editedForms.size());
            applicationEventPublisher.publishEvent(configEvent);
        }
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        postResult.setValue(editedForms);
        return postResult;
    }

    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public PostResult delete(@RequestParam Integer id, @RequestParam String name) {
        PostResult postResult = new PostResult();
        Form form = formService.getFormById(id);
        if (!name.equals(form.getName())) {
            String message;
            if (StringUtils.isBlank(name)) {
                message = "Enter the name of the form";
            } else {
                message = "'" + name + "' does not match the name of the form";
            }
            postResult.setPostErrors(controllerHelper.getPostErrors("name", message));
            ConfigEvent configEvent = new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), form,
                    EventType.DELETE);
            configEvent.setResult(EventResult.FAILURE);
            configEvent.setFailureMessage(message);
            applicationEventPublisher.publishEvent(configEvent);
        } else {
            checkAccess(form, TenancyAccessService.MODIFY);
            formService.deleteForm(form.getId());
            publicationService.clearData(form);
            postResult.setSuccess(true);
            applicationEventPublisher.publishEvent(
                    new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), form, EventType.DELETE));
        }
        return postResult;
    }

    @ResponseBody
    @RequestMapping(value = "/enable", method = RequestMethod.GET)
    public boolean enable(@RequestParam Integer id, @RequestParam boolean enabled) {
        Form form = formService.getFormById(id);
        checkAccess(form, TenancyAccessService.MODIFY);
        formService.enableForm(form, enabled);
        if (enabled) {
            applicationEventPublisher.publishEvent(
                    new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), form, EventType.ENABLE));
        } else {
            applicationEventPublisher.publishEvent(
                    new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), form, EventType.DISABLE));
        }
        return enabled;
    }

    @ResponseBody
    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    public PostResult sort(@RequestParam Integer surveyId,
            @RequestParam("sortedFormIds[]") List<Integer> sortedFieldIds) {
        Survey survey = surveyService.getSurveyById(surveyId);
        tenancyAccessService.check(survey, TenancyAccessService.MODIFY);
        List<Form> forms = formService.getFormsBySurvey(survey);
        for (Form form : forms) {
            int ordinal = sortedFieldIds.indexOf(form.getId());
            if (ordinal > -1) {
                form.setOrdinal(new BigDecimal(ordinal + 1));
            }
        }
        formService.updateOrdinal(forms);
        ConfigEvent configEvent = new ConfigEvent(EventSource.WEB, userService.getCurrentUser(), survey,
                EventType.SORT);
        configEvent.putProperty(ConfigEvent.NO_OF_FORMS, forms.size());
        applicationEventPublisher.publishEvent(configEvent);
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        return postResult;
    }

    private void checkAccess(Form form, int permissionType) {
        if (form != null) {
            tenancyAccessService.check(form.getSurvey(), permissionType);
        } else {
            throw new NotFoundException();
        }
    }

    private String page(String name) {
        return "form" + "/" + name;
    }
}
