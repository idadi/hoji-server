package ke.co.hoji.server.controller.report;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.core.service.model.SurveyService;
import ke.co.hoji.server.model.DashboardComponent;
import ke.co.hoji.server.model.GetResult;
import ke.co.hoji.server.model.PostResult;
import ke.co.hoji.server.service.DashboardComponentService;
import ke.co.hoji.server.service.TenancyAccessService;

@Controller
public class DashboardController {

    private final DashboardComponentService dashboardComponentService;
    private final SurveyService surveyService;
    private final FormService formService;
    private final TenancyAccessService tenancyAccessService;

    @Autowired
    public DashboardController(
            DashboardComponentService dashboardComponentService, SurveyService surveyService,
            FormService formService, TenancyAccessService tenancyAccessService) {
        this.dashboardComponentService = dashboardComponentService;
        this.surveyService = surveyService;
        this.formService = formService;
        this.tenancyAccessService = tenancyAccessService;
    }

    @ResponseBody
    @GetMapping(value = "api/project/{projectId}/form/{formId}/dashboard/component")
    public GetResult getDashboardComponentsForListPage(@PathVariable Integer formId, Model model) {
        Form form = formService.getFormById(formId);
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.VIEW);
        List<DashboardComponent> components = dashboardComponentService.findDashboardComponentsFor(formId);
        Collections.sort(components);
        GetResult result = new GetResult();
        result.setSuccess(true);
        result.setValue(components);
        return result;
    }

    @ResponseBody
    @PostMapping(
            value = "api/project/{projectId}/form/{formId}/dashboard/component",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public PostResult saveDashboardComponent(
            @RequestBody DashboardComponent dashboardComponent, @PathVariable Integer projectId
    ) {
        return save(dashboardComponent, projectId);
    }

    @ResponseBody
    @PostMapping(
            value = "api/project/{projectId}/form/{formId}/dashboard/component/{componentId}",
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public PostResult updateDashboardComponent(
            @RequestBody DashboardComponent dashboardComponent, @PathVariable Integer projectId
    ) {
        return save(dashboardComponent, projectId);
    }

    private PostResult save(DashboardComponent component, Integer projectId) {
        Survey survey = surveyService.getSurveyById(projectId);
        tenancyAccessService.check(survey, TenancyAccessService.MODIFY);
        if (component.getOrdinal() == null) {
            List<DashboardComponent> allComponents =
                    dashboardComponentService.findDashboardComponentsFor(component.getForm().getId());
            BigDecimal nextOrdinal = Utils.nextOrdinal(new ArrayList<>(allComponents));
            component.setOrdinal(nextOrdinal);
        }
        component = dashboardComponentService.saveDashboardComponent(component);
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        postResult.setValue(component);
        return postResult;
    }

    @ResponseBody
    @PostMapping("api/project/{projectId}/form/{formId}/dashboard/component/sort")
    public PostResult sortDashboardComponents(
            @PathVariable Integer formId,
            @RequestParam("sortedComponentIds[]") List<Integer> sortedComponentIds
    ) {

        List<DashboardComponent> allComponents =  dashboardComponentService.findDashboardComponentsFor(formId);
        for (DashboardComponent component : allComponents) {
            int ordinal = sortedComponentIds.indexOf(component.getId());
            if (ordinal > -1) {
                component.setOrdinal(new BigDecimal(ordinal + 1));
            }
        }
        dashboardComponentService.updateAllDashboardComponents(allComponents);
        PostResult result = new PostResult();
        result.setSuccess(true);
        return result;
    }

    @ResponseBody
    @PostMapping(value = "api/project/{projectId}/form/{formId}/dashboard/component/{componentId}/duplicate")
    public PostResult duplicateDashboardComponent(@PathVariable Integer projectId, @PathVariable Integer componentId) {
        Survey survey = surveyService.getSurveyById(projectId);
        tenancyAccessService.check(survey, TenancyAccessService.MODIFY);
        DashboardComponent dashboardComponent = dashboardComponentService.duplicateDashboardComponent(componentId);
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        postResult.setValue(dashboardComponent);
        return postResult;
    }

    @ResponseBody
    @GetMapping(
            value = "api/project/{projectId}/form/{formId}/report/dashboard/component",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public GetResult getDashboardComponents(@PathVariable Integer formId) {
        List<DashboardComponent> components = dashboardComponentService.findDashboardComponentsFor(formId);
        Collections.sort(components);
        GetResult result = new GetResult();
        result.setSuccess(true);
        result.setValue(components);
        return result;
    }

    @ResponseBody
    @GetMapping(
            value = "api/project/{projectId}/form/{formId}/report/dashboard/component/{componentId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public GetResult getDashboardComponent(@PathVariable Integer componentId) {
        GetResult result = new GetResult();
        result.setSuccess(true);
        result.setValue(dashboardComponentService.findDashboardComponentById(componentId));
        return result;
    }

    @ResponseBody
    @DeleteMapping(
            value = "api/project/{projectId}/form/{formId}/dashboard/component/{componentId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public PostResult deleteDashboardComponent(@PathVariable Integer componentId) {
        dashboardComponentService.deleteDashboardComponent(componentId);
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        return postResult;
    }

}
