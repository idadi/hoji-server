package ke.co.hoji.server.controller.report.model;

import ke.co.hoji.core.data.Period;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter.MainRecordPropertiesFilterBuilder;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * A container for all the filter parameters specified by a user for any given data output.
 * </p>
 * Created by geoffreywasilwa on 05/06/2017.
 */
public class Filter {
    private final FieldParent fieldParent;
    private final boolean testMode;
    private final Date createdFrom;
    private final Date createdTo;
    private final String completed;
    private final Date completedFrom;
    private final Date completedTo;
    private final String uploaded;
    private final Date uploadedFrom;
    private final Date uploadedTo;
    private final List<Integer> selectedUsers;
    private final Integer filterFieldId;
    private final Integer filterFieldValue;

    public Filter(FieldParent fieldParent, boolean testMode, Date createdFrom, Date createdTo, String completed,
                  Date completedFrom, Date completedTo, String uploaded, Date uploadedFrom, Date uploadedTo,
                  List<Integer> selectedUsers, Integer filterFieldId, Integer filterFieldValue) {
        this.fieldParent = fieldParent;
        this.testMode = testMode;
        this.createdFrom = createdFrom;
        this.createdTo = createdTo;
        this.completed = completed;
        this.completedFrom = completedFrom;
        this.completedTo = completedTo;
        this.uploaded = uploaded;
        this.uploadedFrom = uploadedFrom;
        this.uploadedTo = uploadedTo;
        this.selectedUsers = selectedUsers;
        this.filterFieldId = filterFieldId;
        this.filterFieldValue = filterFieldValue;
    }

    public FieldParent getFieldParent() {
        return fieldParent;
    }

    public boolean isTestMode() {
        return testMode;
    }

    public Date getCreatedFrom() {
        return createdFrom;
    }

    public Date getCreatedTo() {
        return createdTo;
    }

    public String getCompleted() {
        return completed;
    }

    public Date getCompletedFrom() {
        return completedFrom;
    }

    public Date getCompletedTo() {
        return completedTo;
    }

    public String getUploaded() {
        return uploaded;
    }

    public Date getUploadedFrom() {
        return uploadedFrom;
    }

    public Date getUploadedTo() {
        return uploadedTo;
    }

    public List<Integer> getSelectedUsers() {
        return selectedUsers;
    }

    public Integer getFilterFieldId() {
        return filterFieldId;
    }

    public Integer getFilterFieldValue() {
        return filterFieldValue;
    }

    public MainRecordPropertiesFilter toQueryFilter() {
        return new MainRecordPropertiesFilterBuilder(fieldParent)
                .testMode(testMode)
                .createdFrom(createdFrom)
                .createdTo(createdTo)
                .completedFrom(completedFrom)
                .completedTo(completedTo)
                .uploadedFrom(uploadedFrom)
                .uploadedTo(uploadedTo)
                .userIds(selectedUsers)
                .filterFieldId(filterFieldId)
                .filterFieldValue(filterFieldValue)
                .build();
    }

    public static class FilterBuilder {
        private final FieldParent fieldParent;
        private boolean testMode;
        private Date createdFrom;
        private Date createdTo;
        private String completed;
        private Date completedFrom;
        private Date completedTo;
        private String uploaded;
        private Date uploadedFrom;
        private Date uploadedTo;
        private List<Integer> userIds;
        private Integer filterFieldId;
        private Integer filterFieldValue;

        public FilterBuilder(FieldParent fieldParent) {
            this.fieldParent = fieldParent;
        }

        public FilterBuilder testMode(boolean testMode) {
            this.testMode = testMode;
            return this;
        }

        public FilterBuilder createdFrom(Date createdFrom) {
            if (createdFrom == null) {
                return this;
            }
            this.createdFrom = createdFrom;
            return this;
        }

        public FilterBuilder createdTo(Date createdTo) {
            if (createdTo == null) {
                return this;
            }
            DateTime to =
                    new DateTime(createdTo.getTime()).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
            this.createdTo = to.toDate();
            return this;
        }

        public FilterBuilder completed(String completed) {
            if (completed == null) {
                return this;
            }
            this.completed = completed;
            Period period = new Period(completed);
            if (period.getFrom() != null) {
                this.completedFrom = period.getFrom();
                this.completedTo = period.getTo();
            }
            return this;
        }

        public FilterBuilder completedFrom(Date completedFrom) {
            if (completedFrom == null) {
                return this;
            }
            this.completedFrom = completedFrom;
            return this;
        }

        public FilterBuilder completedTo(Date completedTo) {
            if (completedTo == null) {
                return this;
            }
            DateTime to =
                    new DateTime(completedTo.getTime()).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
            this.completedTo = to.toDate();
            return this;
        }

        public FilterBuilder uploaded(String uploaded) {
            if (uploaded == null) {
                return this;
            }
            this.uploaded = uploaded;
            Period period = new Period(uploaded);
            if (period.getFrom() != null) {
                this.uploadedFrom = period.getFrom();
                this.uploadedTo = period.getTo();
            }
            return this;
        }

        public FilterBuilder uploadedFrom(Date uploadedFrom) {
            if (uploadedFrom == null) {
                return this;
            }
            this.uploadedFrom = uploadedFrom;
            return this;
        }

        public FilterBuilder uploadedTo(Date uploadedTo) {
            if (uploadedTo == null) {
                return this;
            }
            DateTime to =
                    new DateTime(uploadedTo.getTime()).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
            this.uploadedTo = to.toDate();
            return this;
        }

        public FilterBuilder selectedUsers(List<Integer> userIds) {
            if (userIds == null) {
                return this;
            }
            this.userIds = new ArrayList<>(userIds);
            return this;
        }

        public FilterBuilder filterFieldId(Integer filterFieldId) {
            if (filterFieldId == null) {
                return this;
            }
            this.filterFieldId = filterFieldId;
            return this;
        }

        public FilterBuilder filterFieldValue(Integer filterFieldValue) {
            if (filterFieldValue == null) {
                return this;
            }
            this.filterFieldValue = filterFieldValue;
            return this;
        }

        public Filter build() {
            return new Filter(fieldParent, testMode, createdFrom, createdTo, completed, completedFrom, completedTo,
                    uploaded, uploadedFrom, uploadedTo, userIds, filterFieldId,filterFieldValue);
        }
    }
}
