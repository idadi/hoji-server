package ke.co.hoji.server.controller.helper;

import ke.co.hoji.server.exception.CsvFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.exception.SuperCsvException;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.prefs.CsvPreference;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by geoffreywasilwa on 27/04/2017.
 */
public abstract class AbstractCsvProcessor<T> {

    private Logger logger = LoggerFactory.getLogger(ChoiceCsvProcessor.class);

    protected abstract void validateHeaders(String[] csvHeaders);

    protected abstract CellProcessor[] getProcessors();

    protected abstract List<T> build(List<Map<String, Object>> csvList);

    public List<T> process(InputStream inputStream) {
        Assert.notNull(inputStream, "inputStream cannot be null");

        List<Map<String, Object>> csvList = readCsv(inputStream);
        if (csvList.size() == 0) {
            throw new CsvFormatException("The uploaded csv only contains headers, please provide values and re-upload");
        }
        return build(csvList);
    }

    private List<Map<String, Object>> readCsv(InputStream inputStream) {
        ICsvMapReader mapReader = null;
        try {
            mapReader = new CsvMapReader(new InputStreamReader(inputStream), CsvPreference.STANDARD_PREFERENCE);

            final String[] headers = mapReader.getHeader(true);
            if (headers == null) {
                throw new CsvFormatException("The uploaded csv is blank");
            }
            validateHeaders(headers);
            final CellProcessor[] processors = getProcessors();

            List<Map<String, Object>> lines = new ArrayList<>();
            Map<String, Object> line;
            while((line = mapReader.read(headers, processors)) != null) {
                lines.add(line);
            }
            return lines;
        } catch (IOException ioe) {
            logger.error(ioe.getMessage());
            throw new RuntimeException();
        } catch (SuperCsvException sce) {
            logger.error(sce.getMessage());
            int row = sce.getCsvContext().getRowNumber();
            int col = sce.getCsvContext().getColumnNumber();
            throw new CsvFormatException("Invalid value in row: " + row + ", column: " + col);
        } finally {
            if (mapReader != null) {
                try {
                    mapReader.close();
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
            }
        }
    }

}
