package ke.co.hoji.server.controller;

import ke.co.hoji.core.Utils;
import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.CreditBundle;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.BundlePurchaseService;
import ke.co.hoji.server.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by gitahi on 05/07/15.
 */
@RestController
@RequestMapping("/api/lipisha")
public class LipishaController {

    private static final Logger logger = LoggerFactory.getLogger(LipishaController.class);

    private static final String API_KEY = "29850414bc4c654c946743be7dd7683a";
    private static final String API_SIGNATURE = "RzbPuPFH2k4U2VusHTobEiPla6nayE2fYKeceCrxD/jkFNMsMtQOk2g4XI2TXAwtXanIUK7kLwuM+zcpWq55M7BEHuQoLw7/KeFOB3Fkg/pxppf1KmfbmHfjAI7iZTjC3VExm1YNNSMl4WPVB/oEnIov67B21qH79N89XpyPfi0=";
    private static final String TYPE_INITIATE = "Initiate";
    private static final String TYPE_ACKNOWLEDGE = "Acknowledge";
    private static final String TYPE_RECEIPT = "Receipt";
    private static final String TYPE_PAYMENT = "Payment";
    private static final String TYPE_SETTLEMENT = "Settlement";

    private static final String STATUS_SUCCESS = "001";
    private static final String STATUS_ACKNOWLEDGED = "002";
    private static final String STATUS_INITIATE_FAILURE = "002";
    private static final String STATUS_INVALID_TRANSACTION = "003";
    private static final String STATUS_ERROR_RECEIPT = "004";

    private static final String V_API_KEY = "api_key";
    private static final String V_API_SIGNATURE = "api_signature";
    private static final String V_API_TYPE = "api_type";
    private static final String V_API_VERSION = "api_version";
    private static final String V_TRANSACTION = "transaction";
    private static final String V_TRANSACTION_REFERENCE = "transaction_reference";
    private static final String V_TRANSACTION_CODE = "transaction_code";
    private static final String V_TRANSACTION_DATE = "transaction_date";
    private static final String V_TRANSACTION_AMOUNT = "transaction_amount";
    private static final String V_TRANSACTION_TYPE = "transaction_type";
    private static final String V_TRANSACTION_METHOD = "transaction_method";
    private static final String V_TRANSACTION_NAME = "transaction_name";
    private static final String V_TRANSACTION_MOBILE = "transaction_mobile";
    private static final String V_TRANSACTION_PAYBILL = "transaction_paybill";
    private static final String V_TRANSACTION_ACCOUNT = "transaction_account";
    private static final String V_TRANSACTION_ACCOUNT_NAME = "transaction_account_name";
    private static final String V_TRANSACTION_ACCOUNT_NUMBER = "transaction_account_number";
    private static final String V_TRANSACTION_MERCHANT_REFERENCE = "transaction_merchant_reference";
    private static final String V_TRANSACTION_STATUS = "transaction_status";
    private static final String V_TRANSACTION_STATUS_CODE = "transaction_status_code";
    private static final String V_TRANSACTION_STATUS_DESCRIPTION = "transaction_status_description";

    private static final String PROCESSED = "Processed";
    private static final String ERROR = "Error";
    private static final String ERROR_MESSAGE = "Error while processing";

    @Autowired
    private BundlePurchaseService bundlePurchaseService;

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @RequestMapping(value = "/ipn", method = RequestMethod.POST)
    public Map<String, Object> ipn
            (
                    @RequestParam(V_API_KEY) String apiKey,
                    @RequestParam(V_API_SIGNATURE) String apiSignature,
                    @RequestParam(V_API_TYPE) String apiType,
                    @RequestParam(V_TRANSACTION_MERCHANT_REFERENCE) String accountName,
                    @RequestParam(V_TRANSACTION_METHOD) String paymentMethod,
                    @RequestParam(V_TRANSACTION_CODE) String paymentReference,
                    @RequestParam(V_TRANSACTION_AMOUNT) BigDecimal amount,
                    @RequestParam(V_TRANSACTION_DATE) String dateString,
                    @RequestParam(value = V_TRANSACTION_STATUS_CODE, required = false) String statusCode,
                    @RequestParam(V_API_VERSION) String apiVersion
            ) {

        logger.debug("ipn method called");

        Map<String, Object> response = new LinkedHashMap<>();
        boolean success = false;
        boolean sendReceipt = true;
        if (apiType != null) {
            if (API_KEY.equals(apiKey) && API_SIGNATURE.equals(apiSignature)) {
                if (apiType.equals(TYPE_INITIATE)) {
                    logger.debug("Account Name: {}", accountName);
                    logger.debug("Description: {}", paymentMethod);
                    logger.debug("Reference No: {}", paymentReference);
                    logger.debug("Amount: {}", amount);
                    Integer userId = Utils.parseInteger(accountName);
                    if (userId != null) {
                        User user = userService.findById(userId);
                        if (user != null) {
                            Date date;
                            try {
                                date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString);
                            } catch (Exception ex) {
                                date = new Date();//TODO: Might be better to error here instead.
                            }
                            if (!bundlePurchaseService.depositProcessed(user, date, paymentReference)) {
                                bundlePurchaseService.purchaseBundle(user, new CreditBundle(), date, paymentMethod, paymentReference, amount);
                                UserEvent userEvent = new UserEvent(EventSource.WEB, user, user, EventType.LOAD_CASH);
                                userEvent.putProperty(UserEvent.CASH_AMOUNT, amount);
                                applicationEventPublisher.publishEvent(userEvent);
                            }
                        }
                    }
                } else {
                    sendReceipt = false;
                }
                success = true;
            }
        }
        prepareReceipt(response, sendReceipt, success, apiVersion, paymentReference, accountName);
        return response;
    }

    private void prepareReceipt
            (
                    Map<String, Object> response,
                    boolean sendReceipt,
                    boolean success,
                    String apiVersion,
                    String referenceNo,
                    String accountName
            ) {
        String transactionStatusCode;
        String transactionStatus;
        String transactionStatusDescription;
        if (success) {
            transactionStatusCode = STATUS_SUCCESS;
            transactionStatus = PROCESSED;
            transactionStatusDescription = PROCESSED;
        } else {
            transactionStatusCode = STATUS_INITIATE_FAILURE;
            transactionStatus = ERROR;
            transactionStatusDescription = ERROR_MESSAGE;
        }

        if (sendReceipt) {
            response.put(V_API_KEY, API_KEY);
            response.put(V_API_SIGNATURE, API_SIGNATURE);
            response.put(V_API_VERSION, apiVersion);
            response.put(V_API_TYPE, TYPE_RECEIPT);
            response.put(V_TRANSACTION, referenceNo);
            response.put(V_TRANSACTION_REFERENCE, accountName);
            response.put(V_TRANSACTION_STATUS_CODE, transactionStatusCode);
            response.put(V_TRANSACTION_STATUS, transactionStatus);
            response.put(V_TRANSACTION_STATUS_DESCRIPTION, transactionStatusDescription);
        }
    }
}
