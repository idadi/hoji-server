package ke.co.hoji.server.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.GetResult;
import ke.co.hoji.server.model.PostResult;
import ke.co.hoji.server.model.Template;
import ke.co.hoji.server.service.TemplateService;

@Controller
@RequestMapping("admin")
public class AdminController {

    @Autowired
    private TemplateService templateService;

    @ResponseBody
    @GetMapping("template/event-type")
    public GetResult getUnAssociatedEvents() {
        GetResult result = new GetResult();
        Set<EventType> associatedEvents = templateService.getAssociatedEvents();
        List<EventType> unAssociatedEvents = new ArrayList<>();
        for (EventType eventType : EventType.values()) {
            if (eventType.isEmailable() && !associatedEvents.contains(eventType)) {
                unAssociatedEvents.add(eventType);
            }
        }
        result.setValue(unAssociatedEvents);
        result.setSuccess(true);
        return result;
    }

    @GetMapping("template/list")
    public String templates(Model model) {
        List<Template> templates = templateService.getAllTemplates();
        model.addAttribute("emailTemplates", templates);
        return "admin/list";
    }

    @ResponseBody
    @PostMapping("template/edit")
    public PostResult editEmailTemplate(@RequestBody Template template) {
        PostResult result = new PostResult();
        result.setSuccess(true);
        Template saved = templateService.saveTemplate(template);
        result.setValue(saved);
        return result;
    }
}
