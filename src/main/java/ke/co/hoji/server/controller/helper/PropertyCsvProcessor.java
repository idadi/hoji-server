package ke.co.hoji.server.controller.helper;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.model.Property;
import ke.co.hoji.server.exception.CsvFormatException;
import org.springframework.stereotype.Component;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class PropertyCsvProcessor extends AbstractCsvProcessor<Property> {

    @Override
    protected void validateHeaders(String[] csvHeaders) {
        List<String> actualHeaders = Arrays.asList(csvHeaders);
        List<String> expectedHeaders = Arrays.asList("key", "value");
        if (!expectedHeaders.containsAll(actualHeaders)) {
            String expected = Utils.string(expectedHeaders);
            throw new CsvFormatException("The expected headers are: " + expected);
        }
    }

    @Override
    protected CellProcessor[] getProcessors() {

        return new CellProcessor[] {
                new NotNull(), // key
                new NotNull()  // value
        };
    }

    protected List<Property> build(List<Map<String, Object>> csvList) {
        return csvList.stream()
                .map(map -> {
                    String key = map.get("key").toString();
                    String value = map.get("value").toString();
                    return new Property(key, value);
                })
                .collect(Collectors.toList());
    }
}
