package ke.co.hoji.server.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ke.co.hoji.core.data.PublicSurveyAccess;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.core.service.model.SurveyService;
import ke.co.hoji.server.controller.helper.ControllerHelper;
import ke.co.hoji.server.event.event.ConfigEvent;
import ke.co.hoji.server.event.event.UserEvent;
import ke.co.hoji.server.event.event.WebHookEvent;
import ke.co.hoji.server.event.model.EventResult;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.PostResult;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.model.WebHook;
import ke.co.hoji.server.service.TenancyAccessService;
import ke.co.hoji.server.service.UserService;
import ke.co.hoji.server.service.WebHookMessenger;
import ke.co.hoji.server.service.WebHookService;
import ke.co.hoji.server.validator.WebHookValidator;

@Controller
@RequestMapping
public class WebHookController {

    private final UserService userService;
    private final WebHookService webHookService;
    private final WebHookMessenger webHookMessenger;
    private final SurveyService surveyService;
    private final FormService formService;
    private final TenancyAccessService tenancyAccessService;
    private final WebHookValidator webHookValidator;
    private final ControllerHelper controllerHelper;
    private final ApplicationEventPublisher eventPublisher;

    @Value("${crypto.algorithm}")
    private String cryptoAlgorithm;

    @Autowired
    public WebHookController(
            UserService userService, WebHookService webHookService,
            WebHookMessenger webHookMessenger, SurveyService surveyService,
            FormService formService, TenancyAccessService tenancyAccessService,
            WebHookValidator webHookValidator, ControllerHelper controllerHelper,
            ApplicationEventPublisher eventPublisher
    ) {
        this.userService = userService;
        this.webHookService = webHookService;
        this.webHookMessenger = webHookMessenger;
        this.surveyService = surveyService;
        this.formService = formService;
        this.tenancyAccessService = tenancyAccessService;
        this.webHookValidator = webHookValidator;
        this.controllerHelper = controllerHelper;
        this.eventPublisher = eventPublisher;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(webHookValidator);
    }

    @GetMapping("/hook/list")
    public String list(Model model) {
        User user = userService.getCurrentUser();
        User tenant = userService.findById(user.getTenantUserId());
        List<WebHook> webHooks = webHookService.findWebHooksFor(tenant);
        model.addAttribute("userId", user.getTenantUserId());
        model.addAttribute("webhooks", webHooks);
        model.addAttribute("forms", getAccessibleFormsForUser());
        model.addAttribute("eventTypes", Arrays.asList(EventType.CREATE, EventType.MODIFY));
        UserEvent userEvent = new UserEvent(EventSource.WEB, user, user, EventType.WEB_HOOKS);
        userEvent.putProperty(UserEvent.NO_OF_WEB_HOOKS, webHooks.size());
        eventPublisher.publishEvent(userEvent);
        return "hook/list";
    }

    @ResponseBody
    @PostMapping("/hook/edit")
    public PostResult save(@RequestBody @Validated WebHook webHook, BindingResult bindingResult, Model model) {
        PostResult result = new PostResult();
        if (bindingResult.hasErrors()) {
            result.setSuccess(false);
            result.setPostErrors(controllerHelper.getPostErrors(bindingResult.getFieldErrors()));
            return result;
        }
        User user = userService.getCurrentUser();
        boolean isNew = webHook.getId() == null;
        webHook = webHookService.saveWebHook(webHook);
        if (isNew) {
            eventPublisher.publishEvent(new ConfigEvent(EventSource.WEB, user, webHook, EventType.CREATE));
        } else {
            eventPublisher.publishEvent(new ConfigEvent(EventSource.WEB, user, webHook, EventType.MODIFY));
        }
        if (!webHook.isActive()) {
            verify(webHook);
        }
        result.setSuccess(true);
        result.setValue(webHook);
        return result;
    }

    @ResponseBody
    @PostMapping("/hook/delete")
    public PostResult delete(@RequestParam Integer id) {
        WebHook webHook = webHookService.findWebHook(id);
        tenancyAccessService.check(webHook, TenancyAccessService.MODIFY);
        webHookService.deleteWebHook(id);
        PostResult postResult = new PostResult();
        postResult.setSuccess(true);
        eventPublisher.publishEvent(new ConfigEvent(EventSource.WEB, userService.getCurrentUser(),
                webHook, EventType.DELETE));
        return postResult;
    }

    private List<Form> getAccessibleFormsForUser() {
        List<Survey> surveys = surveyService
                .getSurveysByTenantUserId(userService.getCurrentUser().getTenantUserId(), PublicSurveyAccess.ALL);
        List<Form> forms = new ArrayList<>();
        for (Survey survey : surveys) {
            try {
                tenancyAccessService.check(survey, TenancyAccessService.VIEW);
                forms.addAll(formService.getFormsBySurvey(survey));
            } catch (AccessDeniedException e) {
                //user has no access to surveys in this form so continue
            }
        }
        return forms;
    }

    private void verify(WebHook webHook) {
        String secret = webHookMessenger.verify(webHook.getTargetUrl());
        if (StringUtils.isNotBlank(secret)) {
            webHook = new WebHook(webHook.getId(), webHook.getForms(), webHook.getEventTypes(),
                    webHook.getTargetUrl(), webHook.getUserId(), true, true, secret);
            webHookService.saveWebHook(webHook);
            WebHookEvent event = new WebHookEvent(
                    EventSource.WEB,
                    userService.getCurrentUser(),
                    webHook,
                    EventType.WEB_HOOK_ACTIVATE
            );
            eventPublisher.publishEvent(event);
        } else {
            WebHookEvent event = new WebHookEvent(
                    EventSource.WEB,
                    userService.getCurrentUser(),
                    webHook,
                    EventType.WEB_HOOK_ACTIVATE
            );
            event.setResult(EventResult.FAILURE);
            event.setFailureMessage("Verification of "+ webHook.getTargetUrl() +" web hook failed.");
            eventPublisher.publishEvent(event);
        }
    }
}
