package ke.co.hoji.server.controller.report;

import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.controller.report.model.Filter;
import ke.co.hoji.server.event.event.FormEvent;
import ke.co.hoji.server.event.model.EventSource;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.GetResult;
import ke.co.hoji.server.model.GpsPoint;
import ke.co.hoji.server.service.TenancyAccessService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by geoffreywasilwa on 03/11/2016.
 */
@Controller
@RequestMapping("api/project/{projectId}/form/{formId}/")
public class MapController extends BaseDataController {

    @ResponseBody
    @RequestMapping(value = "/report/map", method = RequestMethod.GET)
    public GetResult map(Filter filter) throws ParseException {
        Form form = (Form) filter.getFieldParent();
        tenancyAccessService.check(form.getSurvey(), TenancyAccessService.VIEW);

        List<GpsPoint> gpsPoints = dataService.getGpsPoints(filter.toQueryFilter());
        if (gpsPoints == null) {
            gpsPoints = new ArrayList<>();
        }
        replaceEncryptedCaption(form, gpsPoints);
        Map<String, List<GpsPoint>> results = new HashMap<>();
        results.put("gpsPoints", gpsPoints);
        GetResult getResult = new GetResult();
        getResult.setSuccess(true);
        getResult.setValue(results);

        applicationEventPublisher.publishEvent(new FormEvent(EventSource.WEB, userService.getCurrentUser(), form, EventType.MAP));
        return getResult;
    }

    /*
     * Replace all encrypted captions with a placeholder word to avoid leaking encrypted data.
     */
    private void replaceEncryptedCaption(Form form, List<GpsPoint> gpsPoints) {
        boolean hasEncryptedCaption = false;
        for (Field field : form.getFields()) {
            if (field.isCaptioning() && field.getType().isEncrypted()) {
                hasEncryptedCaption = true;
            }
        }
        if (hasEncryptedCaption) {
            for (GpsPoint gpsPoint : gpsPoints) {
                gpsPoint.setCaption("Encrypted");
            }
        }
        return;
    }
}
