package ke.co.hoji.server.json.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import ke.co.hoji.core.data.Choice;

import java.io.IOException;

public class ChoiceSerializer extends StdSerializer<Choice> {

    public ChoiceSerializer() {
        super(Choice.class);
    }

    @Override
    public void serialize(Choice value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeNumberField("id", value.getId());
        gen.writeStringField("name", value.getName());
        gen.writeNumberField("ordinal", value.getOrdinal());
        gen.writeStringField("code", value.getCode());
        gen.writeStringField("description", value.getDescription());
        gen.writeNumberField("scale", value.getScale());
        gen.writeBooleanField("loner", value.isLoner());
        if (value.getParent() != null) {
            gen.writeObjectFieldStart("parent");
            gen.writeNumberField("id", value.getParent().getId());
            gen.writeStringField("name", value.getParent().getName());
            gen.writeNumberField("ordinal", value.getParent().getOrdinal());
            gen.writeStringField("description", value.getParent().getDescription());
            gen.writeNumberField("scale", value.getParent().getScale());
            gen.writeBooleanField("loner", value.getParent().isLoner());
            gen.writeEndObject();
        }
        gen.writeEndObject();
    }
}
