package ke.co.hoji.server.json.deserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.service.model.FormService;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.WebHook;


public class WebHookDeserializer extends StdDeserializer<WebHook> {

    private static final long serialVersionUID = -6528482148853550395L;
    private final FormService formService;

    public WebHookDeserializer(FormService formService) {
        super(WebHook.class);
        this.formService = formService;
    }

    @Override
    public WebHook deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        Integer webHookId = null;
        List<Form> forms = new ArrayList<>();
        List<EventType> eventTypes = new ArrayList<>();
        String targetUrl = null;
        Integer ownerId = null;
        boolean verified = false;
        boolean active = false;
        JsonToken currentToken = p.getCurrentToken();
        if (currentToken == JsonToken.START_OBJECT) {
            while ((currentToken = p.nextValue()) != JsonToken.END_OBJECT) {
                switch (p.getCurrentName()) {
                    case "id":
                        int id = _parseIntPrimitive(p, ctxt);
                        if (id != 0) {
                            webHookId = id;
                        }
                        break;
                    case "forms":
                        if (currentToken == JsonToken.START_ARRAY) {
                            while ((currentToken = p.nextValue()) != JsonToken.END_ARRAY) {
                                int formId = _parseIntPrimitive(p, ctxt);
                                if (formId != 0) {
                                    forms.add(formService.getFormById(formId));
                                }
                            }
                        }
                        break;
                    case "eventTypes":
                        if (currentToken == JsonToken.START_ARRAY) {
                            while ((currentToken = p.nextValue()) != JsonToken.END_ARRAY) {
                                eventTypes.add(EventType.valueOf(p.getText()));
                            }
                        }
                        break;
                    case "targetUrl":
                        targetUrl = p.getText();
                        break;
                    case "userId":
                        int userId = _parseIntPrimitive(p, ctxt);
                        if (userId != 0) {
                            ownerId = userId;
                        }
                        break;
                    case "verified":
                        verified = _parseBooleanPrimitive(p, ctxt);
                    case "active":
                        active = _parseBooleanPrimitive(p, ctxt);
                }
            }
        }
        return new WebHook(webHookId, forms, eventTypes, targetUrl, ownerId, verified, active, null);
    }

}
