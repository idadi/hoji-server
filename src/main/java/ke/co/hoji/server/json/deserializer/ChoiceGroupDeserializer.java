package ke.co.hoji.server.json.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.ChoiceGroup;
import ke.co.hoji.core.service.model.ChoiceGroupService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by geoffreywasilwa on 14/01/2017.
 */
public class ChoiceGroupDeserializer extends StdDeserializer<ChoiceGroup> {

    private final ChoiceGroupService choiceGroupService;

    public ChoiceGroupDeserializer(ChoiceGroupService choiceGroupService) {
        super(ChoiceGroup.class);
        this.choiceGroupService = choiceGroupService;
    }

    @Override
    public ChoiceGroup deserialize(
            JsonParser jsonParser,
            DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        return buildChoiceGroupFromJson(jsonParser, deserializationContext);
    }


    private ChoiceGroup buildChoiceGroupFromJson(
            JsonParser jsonParser,
            DeserializationContext deserializationContext) throws IOException {
        ChoiceGroup choiceGroup = new ChoiceGroup();
        JsonToken currentToken = jsonParser.getCurrentToken();
        if (currentToken == JsonToken.START_OBJECT) {
            while ((currentToken = jsonParser.nextValue()) != JsonToken.END_OBJECT) {
                if (currentToken == JsonToken.START_ARRAY) {
                    buildChoiceGroupChoicesFromJson(jsonParser, deserializationContext, choiceGroup);
                } else {
                    setChoiceGroupProperties(choiceGroup, jsonParser, deserializationContext);
                }
            }
        } else {
            Integer choiceGroupId = _parseIntPrimitive(jsonParser, deserializationContext);
            if (choiceGroupId == 0) {
                choiceGroup = null;
            } else {
                choiceGroup = choiceGroupService.getChoiceGroupById(choiceGroupId);
            }
        }
        return choiceGroup;
    }

    private void buildChoiceGroupChoicesFromJson(JsonParser jsonParser, DeserializationContext deserializationContext, ChoiceGroup choiceGroup) throws IOException {
        List<Choice> choices = new ArrayList<>();
        while ((jsonParser.nextToken()) != JsonToken.END_ARRAY) {
            Choice choice = getChoice(jsonParser, deserializationContext);
            if (choice != null) {
                choices.add(choice);
            }
        }
        choiceGroup.setChoices(choices);
    }

    private void setChoiceGroupProperties(ChoiceGroup choiceGroup, JsonParser jsonParser,
                                          DeserializationContext deserializationContext) throws IOException {
        switch (jsonParser.getCurrentName()) {
            case "id":
                int choiceGroupId = _parseIntPrimitive(jsonParser, deserializationContext);
                if (choiceGroupId != 0) {
                    choiceGroup.setId(choiceGroupId);
                }
                break;
            case "name":
                choiceGroup.setName(jsonParser.getText());
                break;
            case "orderingStrategy":
                choiceGroup.setOrderingStrategy(_parseIntPrimitive(jsonParser, deserializationContext));
                break;
            case "excludeLast":
                choiceGroup.setExcludeLast(_parseIntPrimitive(jsonParser, deserializationContext));
                break;
            case "userId":
                choiceGroup.setUserId(_parseIntPrimitive(jsonParser, deserializationContext));
                break;
            default:
                break;
        }
    }

    private Choice getChoice(JsonParser jsonParser,
                              DeserializationContext deserializationContext) throws IOException {
        JavaType choiceType = deserializationContext.constructType(Choice.class);
        JsonDeserializer<Object> choiceDeserializer = this.findDeserializer(deserializationContext, choiceType, null);
        return (Choice)choiceDeserializer.deserialize(jsonParser, deserializationContext);
    }
}
