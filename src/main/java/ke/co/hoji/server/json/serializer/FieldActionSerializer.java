package ke.co.hoji.server.json.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ke.co.hoji.core.data.model.Field.FieldAction;

public class FieldActionSerializer extends StdSerializer<FieldAction> {

    private static final long serialVersionUID = -4020179029934072561L;

    public FieldActionSerializer() {
        super(FieldAction.class);
    }

    @Override
    public void serialize(FieldAction action, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeStringField("label", action.getLabel());
        gen.writeNumberField("value", action.getValue());
        gen.writeEndObject();
    }

}
