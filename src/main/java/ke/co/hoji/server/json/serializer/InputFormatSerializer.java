package ke.co.hoji.server.json.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ke.co.hoji.core.data.model.Field.InputFormat;


public class InputFormatSerializer extends StdSerializer<InputFormat> {

    private static final long serialVersionUID = 783411641803398210L;

    public InputFormatSerializer() {
        super(InputFormat.class);
    }

    @Override
    public void serialize(InputFormat inputFormat, JsonGenerator generator, SerializerProvider provider)
            throws IOException {
        generator.writeStartObject();
        generator.writeStringField("label", inputFormat.getLabel());
        generator.writeNumberField("value", inputFormat.getValue());
        generator.writeEndObject();
    }

}
