package ke.co.hoji.server.json.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;

import java.io.IOException;

/**
 * Created by geoffreywasilwa on 21/02/2017.
 */
public class FormSerializer extends StdSerializer<Form> {

    public FormSerializer() {
        super(Form.class);
    }

    @Override
    public void serialize(Form form, JsonGenerator generator, SerializerProvider provider) throws IOException {
        generator.writeStartObject();
        generator.writeNumberField("id", form.getId());
        generator.writeStringField("name", form.getName());
        generator.writeNumberField("gpsCapture", form.getGpsCapture());
        generator.writeNumberField("ordinal", form.getOrdinal());
        generator.writeStringField("fullVersion", form.getFullVersion());
        generator.writeNumberField("minorVersion", form.getMinorVersion());
        generator.writeNumberField("majorVersion", form.getMajorVersion());
        generator.writeBooleanField("enabled", form.isEnabled());
        generator.writeNumberField("credits", form.getCredits());
        generator.writeArrayFieldStart("outputStrategy");
        for (String output : form.getOutputStrategy()) {
            generator.writeString(output);
        }
        generator.writeEndArray();
        generator.writeBooleanField("transposable", form.isTransposable());
        generator.writeStringField("transpositionCategories", form.getTranspositionCategories());
        generator.writeStringField("transpositionValueLabel", form.getTranspositionValueLabel());
        generator.writeFieldName("survey");
        JsonSerializer<Object> surveySerializer = provider.findValueSerializer(Survey.class);
        surveySerializer.serialize(form.getSurvey(), generator, provider);
        generator.writeEndObject();
    }
}
