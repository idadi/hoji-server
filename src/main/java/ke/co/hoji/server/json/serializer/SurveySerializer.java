package ke.co.hoji.server.json.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.UserService;

import java.io.IOException;

/**
 * Created by geoffreywasilwa on 21/02/2017.
 */
public class SurveySerializer extends StdSerializer<Survey> {

    private final UserService userService;

    public SurveySerializer(UserService userService) {
        super(Survey.class);
        this.userService = userService;
    }

    @Override
    public void serialize(Survey survey, JsonGenerator generator, SerializerProvider serializerProvider) throws IOException {
        if (survey == null) {
            generator.writeNull();
            return;
        }
        generator.writeStartObject();
        generator.writeNumberField("id", survey.getId());
        generator.writeStringField("name", survey.getName());
        generator.writeNumberField("status", survey.getStatus());
        generator.writeBooleanField("enabled", survey.isEnabled());
        generator.writeNumberField("access", survey.getAccess());
        generator.writeBooleanField("free", survey.isFree());
        generator.writeStringField("code", survey.getCode());
        User user = userService.findById(survey.getUserId());
        generator.writeObjectFieldStart("user");
        generator.writeNumberField("id", user.getId());
        generator.writeStringField("fullName", user.getFullName());
        generator.writeBooleanField("known", user.isSuspended());
        generator.writeEndObject();
        generator.writeEndObject();
    }
}
