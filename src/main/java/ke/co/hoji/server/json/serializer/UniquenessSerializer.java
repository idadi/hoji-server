package ke.co.hoji.server.json.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ke.co.hoji.core.data.model.Field.Uniqueness;


public class UniquenessSerializer extends StdSerializer<Uniqueness> {

    private static final long serialVersionUID = 1L;

    public UniquenessSerializer() {
        super(Uniqueness.class);
    }

    @Override
    public void serialize(Uniqueness uniqueness, JsonGenerator generator, SerializerProvider provider)
            throws IOException {
        generator.writeStartObject();
        generator.writeStringField("label", uniqueness.getLabel());
        generator.writeNumberField("value", uniqueness.getValue());
        generator.writeEndObject();
    }

}
