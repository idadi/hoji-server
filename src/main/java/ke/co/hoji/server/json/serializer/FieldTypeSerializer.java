package ke.co.hoji.server.json.serializer;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import ke.co.hoji.core.data.model.FieldType;
import org.springframework.context.MessageSource;

import java.io.IOException;
import java.util.Locale;

/**
 * Created by geoffreywasilwa on 13/03/2017.
 */
public class FieldTypeSerializer extends StdSerializer<FieldType> {

    private MessageSource messageSource;

    public FieldTypeSerializer(MessageSource messageSource) {
        super(FieldType.class);
        this.messageSource = messageSource;
    }

    @Override
    public void serialize(
            FieldType fieldType,
            JsonGenerator generator,
            SerializerProvider provider
    ) throws IOException {
        generator.writeStartObject();
        generator.writeNumberField("id", fieldType.getId());
        generator.writeNumberField("uniqueId", (Integer) fieldType.getUniqueId());
        generator.writeStringField("name", fieldType.getName());
        generator.writeStringField("code", fieldType.getCode());
        generator.writeNumberField("dataType", fieldType.getDataType());
        generator.writeNumberField("group", fieldType.getGroup());
        String groupName = messageSource.getMessage(FieldType.Group.getGroupCode(fieldType.getGroup()), null, Locale.ENGLISH);
        generator.writeStringField("groupName", groupName);
        generator.writeBooleanField("label", fieldType.isLabel());
        generator.writeBooleanField("choice", fieldType.isChoice());
        generator.writeBooleanField("multiChoice", fieldType.isMultiChoice());
        generator.writeBooleanField("date", fieldType.isDate());
        generator.writeBooleanField("image", fieldType.isImage());
        generator.writeBooleanField("null", fieldType.isNull());
        generator.writeBooleanField("parent", fieldType.isParent());
        generator.writeBooleanField("record", fieldType.isRecord());
        generator.writeBooleanField("reference", fieldType.isReference());
        generator.writeBooleanField("time", fieldType.isTime());
        generator.writeStringField("responseClass", fieldType.getResponseClass());
        generator.writeStringField("widgetClass", fieldType.getWidgetClass());
        generator.writeNumberField("ordinal", fieldType.getOrdinal());
        generator.writeEndObject();
    }
}
