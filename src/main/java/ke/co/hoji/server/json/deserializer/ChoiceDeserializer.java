package ke.co.hoji.server.json.deserializer;

import java.io.IOException;
import java.math.BigDecimal;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import org.apache.commons.lang3.StringUtils;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.Choice;
import ke.co.hoji.server.dao.model.JdbcChoiceDao;

/**
 * Created by geoffreywasilwa on 14/01/2017.
 */
public class ChoiceDeserializer extends StdDeserializer<Choice> {

    private static final long serialVersionUID = -312648067962971578L;

    private JdbcChoiceDao choiceDao;

    public ChoiceDeserializer(JdbcChoiceDao choiceDao) {
        super(Choice.class);
        this.choiceDao = choiceDao;
    }

    @Override
    public Choice deserialize(
            JsonParser jsonParser,
            DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Choice choice = buildChoiceFromJson(jsonParser, deserializationContext);
        if (choice != null && choice.getId() != null) {
            Choice fromStore = choiceDao.getById(choice.getId());
            if (choice.getName() != null) {
                fromStore.setName(choice.getName());
            }
            if (choice.getDescription() != null) {
                fromStore.setDescription(choice.getDescription());
            }
            if (choice.getOrdinal() != null) {
                fromStore.setOrdinal(choice.getOrdinal());
            }
            if (choice.getCode() != null) {
                fromStore.setCode(choice.getCode());
            }
            if (choice.getScale() != fromStore.getScale()) {
                fromStore.setScale(choice.getScale());
            }
            if (choice.isLoner() != fromStore.isLoner()) {
                fromStore.setLoner(choice.isLoner());
            }
            if (choice.getParent() !=  null) {
                fromStore.setParent(choice.getParent());
            }
            if (choice.getUserId() != null) {
                fromStore.setUserId(choice.getUserId());
            }
            choice = fromStore;
        }
        return choice;
    }

    private Boolean parseBoolean(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonToken token = jp.getCurrentToken();
        if (token == JsonToken.VALUE_STRING && "on".equalsIgnoreCase(jp.getText().trim())) {
            return Boolean.TRUE;
        } else if (token == JsonToken.VALUE_STRING && "off".equalsIgnoreCase(jp.getText().trim())) {
            return Boolean.FALSE;
        } else {
            return _parseBooleanPrimitive(jp, ctxt);
        }
    }

    private Choice buildChoiceFromJson(
            JsonParser jsonParser,
            DeserializationContext deserializationContext) throws IOException {
        if (jsonParser.hasToken(JsonToken.VALUE_NULL)) {
            return null;
        }
        JsonToken currentToken = jsonParser.getCurrentToken();
        if (currentToken == JsonToken.START_OBJECT) {
            Choice choice = new Choice();
            while (jsonParser.nextValue() != JsonToken.END_OBJECT) {
                if ("translatableComponents".equals(jsonParser.getCurrentName())) {
                    while(jsonParser.nextValue() != JsonToken.END_ARRAY) {
                        continue;
                    }
                }
                setChoiceProperties(choice, jsonParser, deserializationContext);
            }
            return choice;
        } else {
            String value = _parseString(jsonParser, deserializationContext);
            if (StringUtils.isBlank(value)) {
                return null;
            }
            Choice choice = new Choice();
            choice.setName(Utils.removeSpaces(value));
            return choice;
        }
    }

    private void setChoiceProperties(Choice choice, JsonParser jsonParser,
                                     DeserializationContext deserializationContext) throws IOException {
        switch (jsonParser.getCurrentName()) {
            case "id":
                int choiceId = _parseIntPrimitive(jsonParser, deserializationContext);
                if (choiceId != 0) {
                    choice.setId(choiceId);
                }
                break;
            case "name":
                choice.setName(Utils.removeSpaces(jsonParser.getText()));
                break;
            case "description":
                if (jsonParser.hasToken(JsonToken.VALUE_STRING)) {
                    choice.setDescription(Utils.removeSpaces(jsonParser.getText()));
                }
                break;
            case "ordinal":
                if (jsonParser.hasToken(JsonToken.VALUE_NUMBER_INT)) {
                    int ordinal = _parseIntPrimitive(jsonParser, deserializationContext);
                    choice.setOrdinal(new BigDecimal(ordinal));
                }
                break;
            case "code":
                if (jsonParser.hasToken(JsonToken.VALUE_STRING)) {
                    choice.setCode(Utils.removeSpaces(jsonParser.getText()));
                }
                break;
            case "scale":
                choice.setScale(_parseIntPrimitive(jsonParser, deserializationContext));
                break;
            case "loner":
                Boolean isLoner = parseBoolean(jsonParser, deserializationContext);
                if (isLoner != null) {
                    choice.setLoner(isLoner);
                }
                break;
            case "parent":
                Choice parent = this.deserialize(jsonParser, deserializationContext);
                if (parent != null) {
                    choice.setParent(parent);
                }
                break;
            case "userId":
                int userId = _parseIntPrimitive(jsonParser, deserializationContext);
                if (userId != 0) {
                    choice.setUserId(userId);
                }
                break;
            default:
                break;
        }
    }
}
