package ke.co.hoji.server.json.serializer;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import ke.co.hoji.server.controller.report.model.Filter;

import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * Created by geoffreywasilwa on 12/04/2017.
 */
public class FilterSerializer extends StdSerializer<Filter> {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public FilterSerializer() {
        super(Filter.class);
    }

    @Override
    public void serialize(Filter filter, JsonGenerator generator, SerializerProvider provider) throws IOException, JsonGenerationException {
        generator.writeStartObject();
        generator.writeArrayFieldStart("selectedUsers");
        for (Integer id : filter.getSelectedUsers()) {
            generator.writeNumber(id);
        }
        generator.writeEndArray();
        generator.writeStringField(
                "createdFrom",
                filter.getCreatedFrom() != null ? dateFormat.format(filter.getCreatedFrom()) : null);
        generator.writeStringField(
                "createdTo",
                filter.getCreatedTo() != null ? dateFormat.format(filter.getCreatedTo()) : null);
        generator.writeStringField("completed", filter.getCompleted());
        generator.writeStringField(
                "completedAfter",
                filter.getCompletedFrom() !=null ? dateFormat.format(filter.getCompletedFrom()) : null);
        generator.writeStringField(
                "completedBefore",
                filter.getCompletedTo() != null ? dateFormat.format(filter.getCompletedTo()) : null);
        generator.writeStringField("uploaded", filter.getUploaded());
        generator.writeStringField(
                "uploadedAfter",
                filter.getUploadedFrom() != null ? dateFormat.format(filter.getUploadedFrom()) : null);
        generator.writeStringField(
                "uploadedBefore",
                filter.getUploadedTo() != null ? dateFormat.format(filter.getUploadedTo()) : null);
        generator.writeBooleanField("testMode", filter.isTestMode());
        generator.writeEndObject();
    }
}
