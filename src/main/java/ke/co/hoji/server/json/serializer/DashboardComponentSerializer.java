package ke.co.hoji.server.json.serializer;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import ke.co.hoji.server.model.DashboardComponent;

import java.io.IOException;

public class DashboardComponentSerializer extends StdSerializer<DashboardComponent> {

    public DashboardComponentSerializer() {
        super(DashboardComponent.class);
    }

    @Override
    public void serialize(
            DashboardComponent dashboardComponent, JsonGenerator generator, SerializerProvider serializerProvider
    ) throws IOException, JsonGenerationException {
        generator.writeStartObject();
        generator.writeNumberField("id", dashboardComponent.getId());
        generator.writeStringField("name", dashboardComponent.getName());
        generator.writeNumberField("ordinal", dashboardComponent.getOrdinal());
        generator.writeObjectField("configuration", dashboardComponent.getConfiguration());
        generator.writeEndObject();
    }
}
