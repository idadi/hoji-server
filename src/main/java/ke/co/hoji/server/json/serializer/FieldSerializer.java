package ke.co.hoji.server.json.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.Form;

/**
 * Created by geoffreywasilwa on 21/02/2017.
 */
public class FieldSerializer extends StdSerializer<Field> {

    public FieldSerializer() {
        super(Field.class);
    }

    @Override
    public void serialize(Field field, JsonGenerator generator, SerializerProvider provider) throws IOException {
        generator.writeStartObject();
        generator.writeNumberField("id", field.getId());
        generator.writeStringField("name", field.getName());
        generator.writeObjectFieldStart("type");
        generator.writeNumberField("id", field.getType().getId());
        generator.writeStringField("name", field.getType().getName());
        generator.writeStringField("code", field.getType().getCode());
        generator.writeEndObject();
        generator.writeStringField("description", field.getDescription());
        generator.writeStringField("instructions", field.getInstructions());
        generator.writeNumberField("ordinal", field.getOrdinal());
        generator.writeStringField("column", field.getColumn());
        generator.writeStringField("missingValue", field.getMissingValue());
        generator.writeNumberField("missingAction", field.getMissingAction());
        generator.writeStringField("tag", field.getTag());
        generator.writeNumberField("calculated", field.getCalculated());
        generator.writeStringField("valueScript", field.getValueScript());
        generator.writeStringField("tagLabel", field.getTagLabel());
        if (field.getChoiceGroup() != null) {
            generator.writeObjectFieldStart("choiceGroup");
            generator.writeNumberField("id", field.getChoiceGroup().getId());
            generator.writeStringField("name", field.getChoiceGroup().getName());
            generator.writeArrayFieldStart("choices");
            for (Choice choice : field.getChoiceGroup().getChoices()) {
                generator.writeStartObject();
                generator.writeNumberField("id", choice.getId());
                generator.writeStringField("name", choice.getName());
                generator.writeEndObject();
            }
            generator.writeEndArray();
            generator.writeEndObject();
        }
        if (field.getChoiceFilterField() != null) {
            generator.writeObjectFieldStart("choiceFilterField");
            generator.writeNumberField("id", field.getChoiceFilterField().getId());
            generator.writeStringField("name", field.getChoiceFilterField().getName());
            generator.writeStringField("description", field.getChoiceFilterField().getDescription());
            generator.writeEndObject();
        }
        generator.writeStringField("defaultValue", field.getDefaultValue());
        if (field.getParent() != null) {
            generator.writeObjectFieldStart("parent");
            generator.writeNumberField("id", field.getParent().getId());
            generator.writeStringField("name", field.getParent().getName());
            generator.writeStringField("description", field.getParent().getDescription());
            generator.writeEndObject();
        }
        generator.writeBooleanField("captioning", field.isCaptioning());
        generator.writeBooleanField("searchable", field.isSearchable());
        generator.writeBooleanField("filterable", field.isFilterable());
        if (field.getOutputType() != null) {
            generator.writeNumberField("outputType", field.getOutputType());
        }
        generator.writeStringField("minValue", field.getMinValue());
        generator.writeStringField("maxValue", field.getMaxValue());
        generator.writeStringField("regexValue", field.getRegexValue());
        generator.writeNumberField("uniqueness", field.getUniqueness());
        if (field.getRecordForms() != null) {
            generator.writeArrayFieldStart("recordForms");
            for (Form recordForm : field.getRecordForms()) {
                generator.writeStartObject();
                generator.writeNumberField("id", recordForm.getId());
                generator.writeStringField("name", recordForm.getName());
                generator.writeEndObject();
            }
            generator.writeEndArray();
        }
        generator.writeBooleanField("matrix", field.getType().isParent());
        generator.writeBooleanField("record", field.getType().isRecord());
        generator.writeBooleanField("enabled", field.isEnabled());
        generator.writeBooleanField("responseInheriting", field.isResponseInheriting());
        generator.writeFieldName("form");
        JsonSerializer<Object> formSerializer = provider.findValueSerializer(Form.class);
        formSerializer.serialize(field.getForm(), generator, provider);
        generator.writeEndObject();
    }
}
