package ke.co.hoji.server.json.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.Template;

public class TemplateSerializer extends StdSerializer<Template> {

    private static final long serialVersionUID = -3375651456006111069L;

    public TemplateSerializer() {
        super(Template.class);
    }

    @Override
    public void serialize(Template template, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeNumberField("id", template.getId());
        gen.writeStringField("template", template.getTemplate());
        gen.writeFieldName("eventType");
        JsonSerializer<Object> eventTypeSerializer = provider.findValueSerializer(EventType.class);
        eventTypeSerializer.serialize(template.getEventType(), gen, provider);
        gen.writeEndObject();
    }

}
