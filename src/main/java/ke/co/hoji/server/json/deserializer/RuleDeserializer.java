package ke.co.hoji.server.json.deserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import ke.co.hoji.core.data.Choice;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.OperatorDescriptor;
import ke.co.hoji.core.data.model.Rule;
import ke.co.hoji.core.service.model.FieldService;

/**
 * Created by geoffreywasilwa on 03/03/2017.
 */
public class RuleDeserializer extends StdDeserializer<Rule> {

    private static final long serialVersionUID = -8239738957367450541L;

    private FieldService fieldService;

    public RuleDeserializer(FieldService fieldService) {
        super(Rule.class);
        this.fieldService = fieldService;
    }

    @Override
    public Rule deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        Rule rule = buildChoiceFromJson(jsonParser, deserializationContext);
        List<Choice> choices = new ArrayList<>();
        if (isContainsOperator(rule)) {
            if (rule.getType() == Rule.Type.FORWARD_SKIP
                    && (rule.getOwner().getType().isMultiChoice() || rule.getOwner().getType().isRanking())
                    ) {
                choices = rule.getOwner().getChoiceGroup().getChoices();
            } else if (rule.getType() == Rule.Type.BACKWARD_SKIP
                    && (rule.getTarget().getType().isMultiChoice() || rule.getTarget().getType().isRanking())
                    ) {
                choices = rule.getTarget().getChoiceGroup().getChoices();
            }
        }
        if (choices.size() > 0) {
            List<String> selectedChoices = Arrays.asList(rule.getValue().split("\\|"));
            String value = choices
                    .stream()
                    .map(choice -> {
                        if (selectedChoices.contains(choice.getId().toString())) {
                            return choice.getId() + "#1";
                        }
                        return choice.getId() + "#0";
                    })
                    .collect(Collectors.joining("|"));
            rule.setValue(value);
        }
        return rule;
    }

    private boolean isContainsOperator(Rule rule) {
        return rule.getOperatorDescriptor() != null && rule.getOperatorDescriptor().getId().equals(7);
    }

    private Rule buildChoiceFromJson(
            JsonParser jsonParser,
            DeserializationContext deserializationContext) throws IOException {
        Rule rule = new Rule();
        JsonToken currentToken = jsonParser.getCurrentToken();
        if (currentToken == JsonToken.START_OBJECT) {
            while (jsonParser.nextValue() != JsonToken.END_OBJECT) {
                setRuleProperties(rule, jsonParser, deserializationContext);
            }
        }
        return rule;
    }

    private void setRuleProperties(Rule rule, JsonParser jsonParser,
                                   DeserializationContext deserializationContext) throws IOException {
        switch (jsonParser.getCurrentName()) {
            case "id":
                int ruleId = _parseIntPrimitive(jsonParser, deserializationContext);
                if (ruleId != 0) {
                    rule.setId(ruleId);
                }
                break;
            case "combiner":
                rule.setCombiner(_parseIntPrimitive(jsonParser, deserializationContext));
                break;
            case "grouper":
                rule.setGrouper(_parseBytePrimitive(jsonParser, deserializationContext));
                break;
            case "owner":
                int ownerId = _parseIntPrimitive(jsonParser, deserializationContext);
                if (ownerId != 0) {
                    Field owner = fieldService.getFieldById(ownerId);
                    rule.setOwner(owner);
                }
                break;
            case "target":
                int targetId = _parseIntPrimitive(jsonParser, deserializationContext);
                if (targetId != 0) {
                    Field target = fieldService.getFieldById(targetId);
                    rule.setTarget(target);
                }
                break;
            case "operatorDescriptor":
                int operatorId = _parseIntPrimitive(jsonParser, deserializationContext);
                if (operatorId != 0) {
                    OperatorDescriptor operatorDescriptor = fieldService.getOperatorDescriptorById(operatorId);
                    rule.setOperatorDescriptor(operatorDescriptor);
                }
                break;
            case "value":
                rule.setValue(String.join("|", jsonParser.getText().split(",")));
                break;
            case "type":
                rule.setType(_parseIntPrimitive(jsonParser, deserializationContext));
                break;
            case "ordinal":
                Double ordinal = _parseDoublePrimitive(jsonParser, deserializationContext);
                rule.setOrdinal(new BigDecimal(ordinal));
                break;
            default:
                break;
        }
    }
}
