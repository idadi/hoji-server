package ke.co.hoji.server.json.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import ke.co.hoji.core.data.model.Property;

import java.io.IOException;

public class PropertySerializer extends StdSerializer<Property> {

    public PropertySerializer() {
        super(Property.class);
    }

    @Override
    public void serialize(Property property, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeStringField("key", property.getKey());
        gen.writeStringField("value", property.getValue());
        gen.writeEndObject();
    }
}
