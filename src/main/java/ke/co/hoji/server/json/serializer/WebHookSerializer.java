package ke.co.hoji.server.json.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.event.model.EventType;
import ke.co.hoji.server.model.WebHook;

public class WebHookSerializer extends StdSerializer<WebHook> {

    public WebHookSerializer() {
        super(WebHook.class);
    }

    @Override
    public void serialize(
            WebHook webHook, JsonGenerator generator, SerializerProvider serializerProvider
    ) throws IOException, JsonGenerationException {
        generator.writeStartObject();
        generator.writeNumberField("id", webHook.getId());
        generator.writeArrayFieldStart("forms");
        for (Form form : webHook.getForms()) {
            generator.writeStartObject();
            generator.writeNumberField("id", form.getId());
            generator.writeStringField("name", form.getName());
            generator.writeEndObject();
        }
        generator.writeEndArray();
        generator.writeArrayFieldStart("eventTypes");
        for (EventType eventType : webHook.getEventTypes()) {
            JsonSerializer<Object> eventTypeSerializer = serializerProvider.findValueSerializer(EventType.class);
            eventTypeSerializer.serialize(eventType, generator, serializerProvider);
        }
        generator.writeEndArray();
        generator.writeStringField("targetUrl", webHook.getTargetUrl());
        generator.writeBooleanField("verified", webHook.isVerified());
        generator.writeBooleanField("active", webHook.isActive());
        generator.writeNumberField("userId", webHook.getUserId());
        generator.writeEndObject();
    }
}
