package ke.co.hoji.server.json.serializer;

import java.io.IOException;

import org.springframework.context.MessageSource;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ke.co.hoji.server.event.model.EventType;


public class EventTypeSerializer extends StdSerializer<EventType> {

    private static final long serialVersionUID = -2102198550000903639L;

    private final MessageSource messageSource;

    public EventTypeSerializer(MessageSource messageSource) {
        super(EventType.class);
        this.messageSource = messageSource;
    }

    @Override
    public void serialize(EventType eventType, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeStringField("label", messageSource.getMessage(eventType.name(), null, provider.getLocale()));
        gen.writeStringField("value", eventType.name());
        gen.writeEndObject();
    }

}
