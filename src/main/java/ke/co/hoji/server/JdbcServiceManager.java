package ke.co.hoji.server;

import ke.co.hoji.core.AbstractServiceManager;
import ke.co.hoji.core.ModelServiceManager;
import ke.co.hoji.core.service.model.ConfigService;
import ke.co.hoji.core.service.model.LanguageService;
import ke.co.hoji.core.service.model.PropertyService;
import ke.co.hoji.core.service.model.RecordService;
import ke.co.hoji.core.service.model.ReferenceService;
import ke.co.hoji.core.service.model.TranslationService;
import ke.co.hoji.server.service.RecordServiceImpl;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Created by gitahi on 15/04/15.
 */
@Component("serviceManager")
public class JdbcServiceManager extends AbstractServiceManager implements ModelServiceManager, ApplicationContextAware {

    private ApplicationContext context;

    public JdbcServiceManager() {
    }

    public ConfigService getConfigService() {
        return context.getBean(ConfigService.class);
    }

    public RecordService getRecordService() {
        return context.getBean(RecordServiceImpl.class);
    }

    public ReferenceService getReferenceService() {
        return context.getBean(ReferenceService.class);
    }

    public PropertyService getPropertyService() {
        return context.getBean(PropertyService.class);
    }

    @Override
    public void clearCache() {
        throw new UnsupportedOperationException("Not supported.");
    }

    @Override
    public LanguageService getLanguageService() {
        return context.getBean(LanguageService.class);
    }

    @Override
    public TranslationService getTranslationService() {
        return context.getBean(TranslationService.class);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}
