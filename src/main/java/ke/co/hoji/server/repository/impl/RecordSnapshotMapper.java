package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.dto.MatrixRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.server.converter.DtoToModelLiveFieldConverter;
import ke.co.hoji.server.model.DataRow;
import ke.co.hoji.server.model.DataTable;
import ke.co.hoji.server.service.UserService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ke.co.hoji.core.data.dto.DataElement.Type.DATE;
import static ke.co.hoji.core.data.dto.DataElement.Type.NUMBER;
import static ke.co.hoji.core.data.dto.DataElement.Type.STRING;
import static ke.co.hoji.server.model.Metadata.Label.CREATED_BY;
import static ke.co.hoji.server.model.Metadata.Label.DATE_COMPLETED;
import static ke.co.hoji.server.model.Metadata.Label.DATE_CREATED;
import static ke.co.hoji.server.model.Metadata.Label.DATE_UPDATED;
import static ke.co.hoji.server.model.Metadata.Label.DATE_UPLOADED;
import static ke.co.hoji.server.model.Metadata.Label.LOCATION;
import static ke.co.hoji.server.model.Metadata.Label.MAIN_RECORD_UUID;
import static ke.co.hoji.server.model.Metadata.Label.RECORD_UUID;
import static ke.co.hoji.server.model.Metadata.Label.RECORD_VERSION;

public class RecordSnapshotMapper {

    private static final String CAPTION = "Caption";
    private static final String RECORD_MODIFIED = "Record Modified";
    private static final String DATE_ANSWERED = "Date Answered";
    private static final String DATE_RE_ANSWERED = "Date Re-answered";
    private static final String FIELD = "Field";
    private static final String VALUE = "Value";
    private static final String VALUE_MODIFIED = "Value Modified";
    private static final String ISO_DATE_TIME_FORMAT = "YYYY-MM-DD HH:mm:ss";
    private static final String DATETIME = "datetime";

    private final UserService userService;
    private final HojiContext hojiContext;

    public RecordSnapshotMapper(UserService userService, HojiContext hojiContext) {
        this.userService = userService;
        this.hojiContext = hojiContext;
    }

    public DataTable mapDataRows(Stream<MainRecord> mainRecords, FieldParent fieldParent) {
        List<DataRow> dataRows = new ArrayList<>();
        if (fieldParent instanceof Form) {
            dataRows.addAll(getData(mainRecords, fieldParent));
        } else {
            dataRows.addAll(getData(mainRecords, fieldParent));
        }
        return new DataTable(dataRows, CREATED_BY);
    }

    private List<DataRow> getData(
            Stream<MainRecord> mainRecords,
            FieldParent fieldParent
    ) {
        if (fieldParent instanceof Form) {
            return getFormData(mainRecords, fieldParent);
        } else {
            return getFieldData(mainRecords, fieldParent);
        }
    }

    private List<DataRow> getFormData(Stream<MainRecord> mainRecords, FieldParent fieldParent) {
        return mainRecords
                .map(mainRecord -> {
                    List<DataElement> metadata = getMainRecordMetadata(mainRecord);
                    List<DataRow> dataRows = fieldParent.getChildren().stream()
                            .filter(field -> field != null && field.isEnabled() && !field.getType().isLabel() && !field.isMatrix())
                            .map(field -> {
                                List<DataElement> columns = new ArrayList<>();
                                columns.addAll(new ArrayList<>(metadata));
                                columns.addAll(mapFieldResponse(field, mainRecord.getLiveFields()));
                                return new DataRow(columns);
                            })
                            .collect(Collectors.toList());
                    return dataRows;
                })
                .flatMap(dataRows -> dataRows.stream())
                .collect(Collectors.toList());
    }

    private List<DataRow> getFieldData(Stream<MainRecord> mainRecords, FieldParent fieldParent) {
        List<MatrixRecord> matrixRecords = new ArrayList<>();
        mainRecords.forEach(mainRecord -> {
            matrixRecords.addAll(mainRecord.getMatrixRecords());
        });
        return matrixRecords.stream().map(matrixRecord -> {
            List<DataRow> dataRows = fieldParent.getChildren().stream()
                    .filter(field -> field == null || !field.isEnabled() || field.getType().isLabel() || field.isMatrix())
                    .map(field -> {
                        List<DataElement> columns = new ArrayList<>();
                        columns.add(
                                new DataElement(
                                        MAIN_RECORD_UUID,
                                        matrixRecord.getMainRecordUuid(),
                                        STRING,
                                        ""
                                )
                        );
                        columns.add(
                                new DataElement(
                                        RECORD_UUID,
                                        matrixRecord.getUuid(),
                                        STRING, ""
                                )
                        );
                        columns.addAll(mapFieldResponse(field, matrixRecord.getLiveFields()));
                        return new DataRow(columns);
                    })
                    .collect(Collectors.toList());
            return dataRows;
        }).flatMap(dataRows -> dataRows.stream()).collect(Collectors.toList());
    }

    private List<DataElement> mapFieldResponse(Field field, List<LiveField> liveFields) {
        List<DataElement> rowData = new ArrayList<>();
        Optional<LiveField> liveField =
                liveFields.stream().filter(lf -> field.getId().equals(lf.getFieldId())).findFirst();
        if (liveField.isPresent()) {
            Response response =
                    new DtoToModelLiveFieldConverter(hojiContext, field).convert(liveField.get()).getResponse();
            rowData.add(new DataElement(
                    FIELD,
                    Utils.removeHtmlTags(field.getHeader()),
                    STRING, ""));
            rowData.add(new DataElement(VALUE, response.displayString(true), STRING, ""));
            rowData.add(new DataElement(
                    DATE_ANSWERED,
                    Utils.formatIsoDateTime(new Date(liveField.get().getDateCreated())),
                    DATETIME,
                    ISO_DATE_TIME_FORMAT));
            rowData.add(new DataElement(
                    DATE_RE_ANSWERED,
                    Utils.formatIsoDateTime(new Date(liveField.get().getDateUpdated())),
                    DATETIME,
                    ISO_DATE_TIME_FORMAT));
            rowData.add(new DataElement(
                    VALUE_MODIFIED,
                    !liveField.get().getDateCreated().equals(liveField.get().getDateUpdated()) ? "Yes" : "No",
                    STRING, ""));
        } else {
            Response response =
                    new DtoToModelLiveFieldConverter(hojiContext, field).convert(new LiveField()).getResponse();
            rowData.add(new DataElement(FIELD, field.getHeader(), STRING, ""));
            rowData.add(new DataElement(VALUE, response.displayString(true), STRING, ""));
            rowData.add(new DataElement(
                    DATE_ANSWERED,
                    "Missing",
                    DATE,
                    ""));
            rowData.add(new DataElement(
                    DATE_RE_ANSWERED,
                    "Missing",
                    DATE,
                    ""));
            rowData.add(new DataElement(
                    VALUE_MODIFIED,
                    "No",
                    STRING, ""));
        }
        return rowData;
    }

    private List<DataElement> getMainRecordMetadata(MainRecord mainRecord) {
        List<DataElement> metadata = new ArrayList<>();
        metadata.add(new DataElement(RECORD_UUID, mainRecord.getUuid(), STRING, ""));
        metadata.add(new DataElement(CAPTION, mainRecord.getCaption(), STRING, ""));
        metadata.add(new DataElement(
                        CREATED_BY,
                        userService.findById(mainRecord.getUserId()).getFullName(),
                        STRING,
                        ""));
        metadata.add(new DataElement(
                DATE_CREATED,
                Utils.formatIsoDateTime(new Date(mainRecord.getDateCreated())),
                DATETIME, ISO_DATE_TIME_FORMAT));
        metadata.add(new DataElement(
                DATE_COMPLETED,
                Utils.formatIsoDateTime(new Date(mainRecord.getDateCompleted())),
                DATETIME, ISO_DATE_TIME_FORMAT));
        metadata.add(new DataElement(
                DATE_UPDATED,
                Utils.formatIsoDateTime(new Date(mainRecord.getDateUpdated())),
                DATETIME, ISO_DATE_TIME_FORMAT));
        metadata.add(new DataElement(
                DATE_UPLOADED,
                Utils.formatIsoDateTime(new Date(mainRecord.getDateUploaded())),
                DATETIME, ISO_DATE_TIME_FORMAT));
        metadata.add(new DataElement(
                RECORD_VERSION,
                mainRecord.getVersion().toString(),
                NUMBER, ""));
        metadata.add(new DataElement(
                RECORD_MODIFIED,
                mainRecord.getVersion() > 1 ? "Yes" : "No",
                STRING, ""));
        metadata.add(new DataElement(
                LOCATION,
                mainRecord.getStartAddress(),
                STRING, ""));
        return metadata;
    }
}
