package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.data.dto.LiteRecord;
import ke.co.hoji.server.repository.FilterCriteria;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;

import java.util.List;

/**
 * Created by geoffreywasilwa on 05/06/2017.
 */
class LiteRecordMapper {

    private static LiteRecordMapper liteRecordMapper;

    static LiteRecordMapper getInstance(MongoOperations mongoOps) {
        if (liteRecordMapper == null) {
            liteRecordMapper = new LiteRecordMapper(mongoOps);
        }
        return liteRecordMapper;
    }

    private final MongoOperations mongoOps;

    private LiteRecordMapper(MongoOperations mongoOps) {
        this.mongoOps = mongoOps;
    }

    List<LiteRecord> findLiteRecords(FilterCriteria filterCriteria) {
        Aggregation aggregation = LiteRecordSearchAggregator.getAggregation(filterCriteria);
        return mongoOps.aggregate(aggregation, "mainRecord", LiteRecord.class).getMappedResults();
    }

}
