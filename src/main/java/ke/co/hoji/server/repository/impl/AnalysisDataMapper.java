package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.FieldType;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.LiveField;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.server.calculation.CalculationResult;
import ke.co.hoji.server.calculation.Calculator;
import ke.co.hoji.server.converter.DtoToModelLiveFieldConverter;
import ke.co.hoji.server.model.Chart;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class AnalysisDataMapper {

    private final HojiContext hojiContext;
    private final Calculator calculator;

    AnalysisDataMapper(HojiContext hojiContext, Calculator calculator) {
        this.hojiContext = hojiContext;
        this.calculator = calculator;
    }

    List<Chart> mapChart(Stream<MainRecord> mainRecords, MainRecordPropertiesFilter filter) {
        FieldParent fieldParent = filter.getFieldParent();
        List<LiveField> allLiveFields;
        List<Field> fields = new ArrayList<>();
        if (fieldParent instanceof Form) {
            fields.addAll(fieldParent.getChildren());
            allLiveFields = mainRecords
                    .flatMap(mainRecord -> {
                        if (filter.passesFilterFieldCriteria(mainRecord.getLiveFields())) {
                            return fieldParent.getChildren()
                                    .stream()
                                    .filter(field -> isFieldAnalyzable(field))
                                    .map(field -> {
                                        Optional<ke.co.hoji.core.data.dto.LiveField> found = mainRecord.getLiveFields()
                                                .stream()
                                                .filter(lf -> field.getId().equals(lf.getFieldId()))
                                                .findFirst();
                                        DtoToModelLiveFieldConverter liveFieldConverter =
                                                new DtoToModelLiveFieldConverter(hojiContext, field, mainRecord);
                                        if (field.hasScriptFunction(Field.ScriptFunctions.CALCULATE)) {
                                            LiveField converted = liveFieldConverter.convert(found.orElse(new ke.co.hoji.core.data.dto.LiveField()));
                                            field.setResponse(converted.getResponse());
                                            CalculationResult result = calculator.calculate(mainRecord, mainRecord.getUuid(), field);
                                            if (result.isEvaluated() && result.getValue() != null) {
                                                converted.setResponse(
                                                        Response.create(hojiContext, converted, result.getValue(), true)
                                                );
                                                return converted;
                                            }
                                        }
                                        return found.map(liveFieldConverter::convert).orElse(null);
                                    })
                                    .filter(Objects::nonNull);
                        }
                        return Stream.of();
                    })
                    .collect(Collectors.toList());
        } else {
            fields.addAll(
                    ((Field) fieldParent).getForm().getFields()
                            .stream()
                            .filter(field -> {
                                if (fieldParent.equals(field)) {
                                    return true;
                                }
                                return field.isSearchable();
                            })
                            .flatMap(field -> {
                                if (fieldParent.equals(field)) {
                                    return fieldParent.getChildren().stream();
                                }
                                return Stream.of(field);
                            })
                            .collect(Collectors.toList())
            );
            allLiveFields = mainRecords
                    .flatMap(mainRecord -> {
                        return fields.stream()
                                .filter(field -> isFieldAnalyzable(field))
                                .flatMap(field -> mainRecord.getMatrixRecords()
                                        .stream()
                                        .map(matrixRecord -> {
                                            List<ke.co.hoji.core.data.dto.LiveField> liveFieldsToConvert = new ArrayList<>();
                                            liveFieldsToConvert.addAll(mainRecord.getLiveFields());
                                            liveFieldsToConvert.addAll(matrixRecord.getLiveFields());
                                            if (filter.passesFilterFieldCriteria(liveFieldsToConvert)) {
                                                Optional<ke.co.hoji.core.data.dto.LiveField> found = liveFieldsToConvert
                                                        .stream()
                                                        .filter(lf -> field.getId().equals(lf.getFieldId()))
                                                        .findFirst();
                                                DtoToModelLiveFieldConverter liveFieldConverter =
                                                        new DtoToModelLiveFieldConverter(hojiContext, field, mainRecord);
                                                if (field.hasScriptFunction(Field.ScriptFunctions.CALCULATE)) {
                                                    LiveField converted =
                                                            liveFieldConverter.convert(found.orElse(new ke.co.hoji.core.data.dto.LiveField()));
                                                    field.setResponse(converted.getResponse());
                                                    CalculationResult result = calculator.calculate(mainRecord, mainRecord.getUuid(), field);
                                                    if (result.isEvaluated() && result.getValue() != null) {
                                                        converted.setResponse(
                                                                Response.create(hojiContext, converted, result.getValue(), true)
                                                        );
                                                        return converted;
                                                    }
                                                }
                                                return found.map(liveFieldConverter::convert).orElse(null);
                                            }
                                            return null;
                                        })
                                )
                                .filter(Objects::nonNull);
                    })
                    .collect(Collectors.toList());
        }
        return fields.stream()
                .filter(this::isFieldAnalyzable)
                .map(field -> getChartForField(field, allLiveFields))
                .collect(Collectors.toList());
    }

    private boolean isFieldAnalyzable(Field field) {
        return field.isEnabled()
                && (field.getType().isChoice()
                || field.getType().getCode().equals(FieldType.Code.MULTIPLE_LINES_TEXT)
                || (field.getType().isNumber()
                && (field.getOutputType() == null
                || field.getOutputType().equals(0)
                || field.getOutputType().equals(Field.OutputType.NUMBER))));
    }

    private Chart getChartForField(Field field, List<LiveField> allLiveFields) {
        List<LiveField> filtered = allLiveFields.stream()
                .filter(liveField -> field.equals(liveField.getField()))
                .collect(Collectors.toList());
        return new Chart(field, filtered);
    }
}
