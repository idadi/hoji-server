package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.model.ImageData;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class ImageDataMapper {

    private final String imageLocation;

    ImageDataMapper(String imageLocation) {
        this.imageLocation = imageLocation;
    }

    List<ImageData> mapDataTable(Stream<MainRecord> mainRecords, FieldParent fieldParent) {
        return getData(mainRecords, fieldParent).collect(Collectors.toList());
    }

    private Stream<ImageData> getData(
            Stream<MainRecord> mainRecords,
            FieldParent fieldParent
    ) {
        if (fieldParent instanceof Form) {
            return getFormData(mainRecords, fieldParent);
        } else if (fieldParent instanceof Field) {
            return getFieldData(mainRecords, fieldParent);
        }
        return Stream.of();
    }

    private Stream<ImageData> getFormData(
            Stream<MainRecord> mainRecords,
            FieldParent fieldParent
    ) {
        return mainRecords
                .flatMap(mainRecord ->
                        getImageDataElements(mainRecord.getLiveFields(), fieldParent, mainRecord.getCaption())
                );
    }

    private Stream<ImageData> getFieldData(
            Stream<MainRecord> mainRecords,
            FieldParent fieldParent
    ) {
        return mainRecords
                .flatMap(mainRecord ->
                        mainRecord.getMatrixRecords()
                                .stream()
                                .flatMap(matrixRecord ->
                                        getImageDataElements(matrixRecord.getLiveFields(), fieldParent, matrixRecord.getCaption())
                                )
                );
    }

    private Stream<ImageData> getImageDataElements(
            List<LiveField> liveFields,
            FieldParent fieldParent,
            String caption
    ) {
        return liveFields.stream()
                .map(liveField -> {
                    Optional<Field> match = fieldParent.getChildren()
                            .stream()
                            .filter(field -> field.getId().equals(liveField.getFieldId()))
                            .findFirst();
                    if (match.isPresent()) {
                        String label = match.get().getName() + ". " + match.get().getResolvedHeader();
                        String imagePath =
                                Objects.nonNull(liveField.getValue()) && StringUtils.isNotBlank(liveField.getValue().toString()) ?
                                liveField.getValue().toString().replace(imageLocation, "/images/") : "";
                        return new ImageData(label, imagePath, caption);
                    }
                    return null;
                })
                .filter(Objects::nonNull);
    }

}
