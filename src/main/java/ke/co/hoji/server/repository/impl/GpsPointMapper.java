package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.server.model.GpsPoint;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter;
import ke.co.hoji.server.service.UserService;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>
 * Creates {@link GpsPoint} from {@link MainRecord}'s metadata
 * </p>
 * Created by geoffreywasilwa on 25/05/2017.
 */
public class GpsPointMapper {

    private final UserService userService;

    public GpsPointMapper(UserService userService) {
        this.userService = userService;
    }

    public List<GpsPoint> findGpsPoints(Stream<MainRecord> mainRecords, MainRecordPropertiesFilter filter) {
        return mainRecords
                .filter(mainRecord ->
                    !mainRecord.getStartLongitude().equals(0D) && !mainRecord.getStartLatitude().equals(0D))
                .filter(mainRecord -> filter.passesFilterFieldCriteria(mainRecord.getLiveFields()))
                .map(mainRecord -> {
                    String address = StringUtils.isNotBlank(mainRecord.getStartAddress())
                            ? mainRecord.getStartAddress() : "Unknown Address";
                    GpsPoint gpsPoint = new GpsPoint
                            (
                                mainRecord.getUuid(),
                                mainRecord.getStartLatitude(),
                                mainRecord.getStartLongitude(),
                                address,
                                mainRecord.getStartAccuracy(),
                                mainRecord.getStartAge().intValue(),
                                userService.findById(mainRecord.getUserId()).getFullName(),
                                mainRecord.getDeviceId(),
                                mainRecord.getVersion(),
                                mainRecord.getCaption(),
                                Utils.formatDateTime(new Date(mainRecord.getDateCreated())),
                                Utils.formatDateTime(new Date(mainRecord.getDateCompleted())),
                                Utils.formatDateTime(new Date(mainRecord.getDateUpdated())),
                                Utils.formatDateTime(new Date(mainRecord.getDateUploaded()))
                            );
                    return gpsPoint;
                })
                .collect(Collectors.toList());
    }

}
