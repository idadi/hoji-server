package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.codec.Base64;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

class ImageWriter {

    private final String imageLocation;
    protected final Logger logger = LoggerFactory.getLogger(ImageWriter.class);

    /**
     * The typical prefix for a PNG encoded image in Base64.
     * See: https://digital-forensics.sans.org/blog/2011/01/09/digital-forensics-finding-encoded-evidence
     */
    private static final String PNG_PREFIX = "iVBOR";

    /**
     * The typical prefix for a JPEG encoded image in Base64.
     * See: https://digital-forensics.sans.org/blog/2011/01/09/digital-forensics-finding-encoded-evidence
     */
    private static final String JPEG_PREFIX = "/9j/4AA";

    ImageWriter(String imageLocation) {
        this.imageLocation = imageLocation;
    }

    void writeImages(MainRecord mainRecord) {
        mainRecord.getLiveFields()
            .forEach(liveField -> writeToFile(mainRecord, liveField));
        if (mainRecord.getMatrixRecords() != null) {
            mainRecord.getMatrixRecords()
                .forEach(matrixRecord ->
                    matrixRecord.getLiveFields().forEach(liveField -> writeToFile(mainRecord, liveField)));
        }
    }

    private void writeToFile(MainRecord mainRecord, LiveField liveField) {
        if (liveField.getValue() != null) {
            String value = liveField.getValue().toString();
            if (value.length() % 4 == 0 && Base64.isBase64(value.getBytes())) {
                String extension = ".png";
                boolean writeToFile = false;
                if (value.startsWith(PNG_PREFIX)) {
                    extension = ".png";
                    writeToFile = true;
                } else if (value.startsWith(JPEG_PREFIX)) {
                    extension = ".jpg";
                    writeToFile = true;
                }
                if (writeToFile) {
                    File imageFile = createImageFile(mainRecord, liveField, extension);
                    writeImageToFile(value, imageFile);
                    liveField.setValue(imageFile.getPath());
                }
            }
        }
    }

    private void writeImageToFile(String value, File imageFile) {
        try (FileOutputStream fos = new FileOutputStream(imageFile)) {
            fos.write(Base64.decode(value.getBytes()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File createImageFile(MainRecord mainRecord, LiveField liveField, String extension) {
        String imagePath = imageLocation + mainRecord.getFormId() + File.separator +
            liveField.getFieldId() + File.separator + liveField.getUuid() + extension;
        File imageFile = new File(imagePath);
        try {
            imageFile.getParentFile().mkdirs();
            imageFile.createNewFile();
        } catch (IOException e) {
            logger.error("Unable to create image folder: " + imagePath, e);
        }
        return imageFile;
    }
}
