package ke.co.hoji.server.repository;

import ke.co.hoji.core.data.dto.LiteRecord;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.model.ApiRecord;
import ke.co.hoji.server.model.Chart;
import ke.co.hoji.server.model.DataTable;
import ke.co.hoji.server.model.GpsPoint;
import ke.co.hoji.server.model.ImageData;
import ke.co.hoji.server.model.RecordOverview;
import ke.co.hoji.server.model.User;

import java.util.List;
import java.util.Optional;

public interface MainRecordRepositoryCustom {

    MainRecord save(MainRecord mainRecord);

    List<MainRecord> saveAll(List<MainRecord> mainRecords);

    /**
     * @param uuid the uuid of the MainRecord to fetch.
     * @return main record with specified uuid.
     */
    MainRecord findByUuid(String uuid);

    /**
     * <b>NOTE:</b> The returned main record only contains one matrix record that matches specified id.
     * @param uuid the uuid of the matrix record to filter by.
     * @param formId id of the form that my contain said matrix record.
     * @return main record with one matrix record that matches specified uuid.
     */
    MainRecord findByMatrixUuid(String uuid, Integer formId);

    List<MainRecord> findRecordsWithoutAddress(long fetchAfter, int batchSize);

    DataTable findDataTable(MainRecordPropertiesFilter mainRecordPropertiesFilter, String renderingType);

    DataTable findRecordSnapshot(MainRecordPropertiesFilter mainRecordPropertiesFilter);

    List<ImageData> findImageData(MainRecordPropertiesFilter mainRecordPropertiesFilter);

    List<GpsPoint> findGpsPoints(MainRecordPropertiesFilter mainRecordPropertiesFilter);

    List<Chart> findMonitorChartData(MainRecordPropertiesFilter mainRecordPropertiesFilter);

    List<Chart> findChartingData(MainRecordPropertiesFilter mainRecordPropertiesFilter);

    List<LiteRecord> findLiteRecords(FilterCriteria filterCriteria);

    List<MainRecord> findMainRecords(String uuid, List<Form> dependentForms);

    List<MainRecord> findMainRecords(FilterCriteria filterCriteria, List<Form> dependentForms);

    Optional<ApiRecord> findApiRecord(String uuid, Form form);

    List<User> findUsersWithRecords(Form form);

    RecordOverview findRecordOverview(FieldParent fieldParent, String uuid);

    long getCountOfRecords(FieldParent fieldParent);

    void deleteRecord(String uuid, FieldParent fieldParent);

    void clearData(Survey survey);

    void clearData(Form form);

    void clearData(Field field);

    void updateTestMode(String uuid, boolean isTest);

    void migrateImages(Integer formId);
}
