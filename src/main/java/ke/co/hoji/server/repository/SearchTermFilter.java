package ke.co.hoji.server.repository;

import ke.co.hoji.core.data.model.Form;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.Date;

import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 * <p>
 * A container for filter parameters used for fetching {@link ke.co.hoji.core.data.dto.MainRecord} for import
 * </p>
 * Created by geoffreywasilwa on 05/06/2017.
 */
public class SearchTermFilter implements FilterCriteria {
    public static final int EXACT_MATCH = 1;
    public static final int PHRASE_MATCH = 2;
    public static final int BROAD_MATCH = 3;
    protected static final String CASE_INSENSITIVE_MATCH = "i";
    protected static final String SEARCH_PHRASE_DELIMITER = "\\|";
    private final Form form;
    private final Date from;
    private final Date to;
    private final int matchSensitivity;
    private final String[] searchTerms;
    private final Integer userId;

    SearchTermFilter(Form form, Date from, Date to, int matchSensitivity, String[] searchTerms, Integer userId) {
        this.form = form;
        this.from = from;
        this.to = to;
        this.matchSensitivity = matchSensitivity;
        this.searchTerms = searchTerms;
        this.userId = userId;
    }

    @Override
    public Criteria getCriteria() {
        Criteria criteria = where("formId").is(form.getId());
        if (userId != null) {
            criteria.and("userId").is(userId);
        }
        if (from != null && to != null) {
            criteria.and("dateCreated").gte(from.getTime()).lte(to.getTime());
        } else if (from != null) {
            criteria.and("dateCreated").gte(from.getTime());
        } else if (to != null) {
            criteria.and("dateCreated").lte(to.getTime());
        }
        if (searchTerms != null) {
            String expression = "";
            expression = delimitExpression(expression);
            for (String term : searchTerms) {
                expression = combineTerms(expression);
                expression += term;
            }
            expression = delimitExpression(expression);
            criteria.and("searchTerms").regex(expression, CASE_INSENSITIVE_MATCH);
        }
        return criteria;
    }

    private String combineTerms(String expression) {
        String combiner;
        if (matchSensitivity == EXACT_MATCH) {
            combiner = "\\|(.*?\\|)?";
        } else if (matchSensitivity == PHRASE_MATCH) {
            combiner = "\\||\\|";
        } else {
            combiner = "|";
        }
        if (StringUtils.isNotEmpty(expression) && !expression.equals(SEARCH_PHRASE_DELIMITER)) {
            expression += combiner;
        }
        return expression;
    }

    private String delimitExpression(String expression) {
        if (matchSensitivity != BROAD_MATCH) {
            expression += SEARCH_PHRASE_DELIMITER;
        }
        return expression;
    }

    public static class ImportFilterBuilder {
        private final Form form;
        private Date from;
        private Date to;
        private int matchSensitivity;
        private String[] searchTerms;
        private Integer userId;

        public ImportFilterBuilder(Form form) {
            this.form = form;
        }

        public ImportFilterBuilder from(Date from) {
            this.from = from;
            return this;
        }

        public ImportFilterBuilder to(Date to) {
            this.to = to;
            return this;
        }

        public ImportFilterBuilder matchSensitivity(int matchSensitivity) {
            this.matchSensitivity = matchSensitivity;
            return this;
        }

        public ImportFilterBuilder searchTerms(String[] searchTerms) {
            this.searchTerms = searchTerms;
            return this;
        }

        public ImportFilterBuilder userId(Integer userId) {
            this.userId = userId;
            return this;
        }

        public SearchTermFilter build() {
            return new SearchTermFilter(
                    form, from, to, matchSensitivity, searchTerms, userId
            );
        }
    }
}
