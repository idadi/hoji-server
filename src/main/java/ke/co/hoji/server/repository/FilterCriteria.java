package ke.co.hoji.server.repository;

import org.springframework.data.mongodb.core.query.Criteria;

/**
 * Created by geoffreywasilwa on 05/06/2017.
 */
public interface FilterCriteria {
    Criteria getCriteria();
}
