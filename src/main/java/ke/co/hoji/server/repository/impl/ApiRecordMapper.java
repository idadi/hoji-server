package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.server.model.ApiRecord;
import ke.co.hoji.server.service.UserService;

import java.util.List;
import java.util.Optional;

class ApiRecordMapper {

    private final UserService userService;
    private final HojiContext hojiContext;

    ApiRecordMapper(UserService userService, HojiContext hojiContext) {
        this.userService = userService;
        this.hojiContext = hojiContext;
    }

    Optional<ApiRecord> getApiRecord(List<MainRecord> mainRecords, Form form) {
        if (mainRecords.size() > 0) {
            return Optional.of(ApiRecord.fromMainRecord(form, mainRecords.get(0), userService, hojiContext));
        }
        return Optional.empty();
    }
}
