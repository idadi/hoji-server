package ke.co.hoji.server.repository.impl;

import com.mongodb.client.result.UpdateResult;
import ke.co.hoji.server.calculation.CalculationResult;
import ke.co.hoji.server.repository.CalculationResultRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Repository
public class CalculationResultRepositoryImpl implements CalculationResultRepositoryCustom {

    private final MongoOperations mongoOps;

    @Autowired
    public CalculationResultRepositoryImpl(MongoOperations mongoOps) {
        this.mongoOps = mongoOps;
    }

    @Override
    public CalculationResult save(CalculationResult calculationResult) {
        Query query =
                new Query(where("objectId").is(calculationResult.getObjectId()).and("fieldId").is(calculationResult.getFieldId()));
        Update update = new Update()
                .set("objectId", calculationResult.getObjectId())
                .set("recordUuid", calculationResult.getRecordUuid())
                .set("formId", calculationResult.getFormId())
                .set("fieldId", calculationResult.getFieldId())
                .set("value", calculationResult.getValue());
        UpdateResult updateResult = mongoOps.upsert(query, update, CalculationResult.class);
        if (updateResult.getMatchedCount() == 0) {
            return new CalculationResult(
                    updateResult.getUpsertedId().toString(),
                    calculationResult.getRecordUuid(),
                    calculationResult.getFormId(),
                    calculationResult.getFieldId(),
                    calculationResult.isEvaluated()
            );
        }
        return calculationResult;
    }
}
