package ke.co.hoji.server.repository.impl;

import com.mongodb.BasicDBObject;
import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.ConfigObject;
import ke.co.hoji.core.data.dto.LiteRecord;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Survey;
import ke.co.hoji.server.model.ApiRecord;
import ke.co.hoji.server.model.Chart;
import ke.co.hoji.server.model.DataTable;
import ke.co.hoji.server.model.GpsPoint;
import ke.co.hoji.server.model.ImageData;
import ke.co.hoji.server.model.RecordOverview;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.repository.FilterCriteria;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter;
import ke.co.hoji.server.repository.MainRecordRepositoryCustom;
import ke.co.hoji.server.service.UserService;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.ComparisonOperators;
import org.springframework.data.mongodb.core.aggregation.ConditionalOperators;
import org.springframework.data.mongodb.core.aggregation.DataTypeOperators;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.StringOperators;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.StreamUtils;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregationOptions;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

/**
 * Created by geoffreywasilwa on 16/05/2017.
 */
@Repository
public class MainRecordRepositoryImpl implements MainRecordRepositoryCustom {

    private final MongoOperations mongoOps;
    private final UserService userService;
    private final TabularDataMapper tabularDataMapper;
    private final AnalysisDataMapper analysisDataMapper;
    private final RecordOverviewMapper recordOverviewMapper;
    private final HojiContext hojiContext;
    @Value("${field.value.maxLength}")
    private int fieldValueMaxLength;
    @Value("${image.location}")
    private String imageLocation;

    @Autowired
    public MainRecordRepositoryImpl(
            MongoOperations mongoOps,
            UserService userService,
            TabularDataMapper tabularDataMapper,
            AnalysisDataMapper analysisDataMapper,
            RecordOverviewMapper recordOverviewMapper,
            HojiContext hojiContext
    ) {
        this.mongoOps = mongoOps;
        this.userService = userService;
        this.tabularDataMapper = tabularDataMapper;
        this.analysisDataMapper = analysisDataMapper;
        this.recordOverviewMapper = recordOverviewMapper;
        this.hojiContext = hojiContext;
    }

    @Override
    public MainRecord save(MainRecord mainRecord) {
        new ImageWriter(imageLocation).writeImages(mainRecord);
        mongoOps.save(mainRecord);
        return mainRecord;
    }

    @Override
    public List<MainRecord> saveAll(List<MainRecord> mainRecords) {
        BulkOperations bulkOperations = mongoOps.bulkOps(BulkOperations.BulkMode.UNORDERED, MainRecord.class, "mainRecord");
        for (MainRecord mainRecord : mainRecords) {
            new ImageWriter(imageLocation).writeImages(mainRecord);
            if (mainRecord.getId() == null) {
                bulkOperations.insert(mainRecord);
            } else {
                MongoConverter converter = mongoOps.getConverter();
                Document document = new Document();
                converter.write(mainRecord, document);
                Update update = Update.fromDocument(new Document("$set", document));
                bulkOperations.updateOne(query(where("id").is(mainRecord.getId())), update);
            }
        }
        bulkOperations.execute();
        return mainRecords;
    }

    @Override
    public MainRecord findByUuid(String uuid) {
        return mongoOps.findOne(query(where("uuid").is(uuid)), MainRecord.class);
    }

    @Override
    public MainRecord findByMatrixUuid(String uuid, Integer formIds) {
    	Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(where("formId").is(formIds)),
                Aggregation.unwind("matrixRecords"),
                Aggregation.match(where("matrixRecords.uuid").is(uuid)),
                commonGroupOperation(Fields.UNDERSCORE_ID)
                    .push("matrixRecords").as("matrixRecords")
                    .first("liveFields").as("liveFields")
            );
            return mongoOps.aggregate(aggregation, "mainRecord", MainRecord.class).getUniqueMappedResult();
    }

    @Override
    public List<MainRecord> findRecordsWithoutAddress(long fetchAfter, int batchSize) {
        Criteria criteria = where("startLatitude").ne(0)
            .and("startLongitude").ne(0)
            .orOperator(where("startAddress").is(null), where("startAddress").is(""))
            .and("dateCreated").gt(fetchAfter);
        Query query = query(criteria).limit(batchSize);
        return mongoOps.find(query, MainRecord.class);
    }

    @Override
    public DataTable findDataTable(
        MainRecordPropertiesFilter mainRecordPropertiesFilter, String renderingType) {
        Stream<MainRecord> mainRecords = getMainRecords(mainRecordPropertiesFilter);
        Long totalRecords = getCountOfRecords(mainRecordPropertiesFilter.getFieldParent());
        return tabularDataMapper.mapDataTable(mainRecords, totalRecords, mainRecordPropertiesFilter, renderingType);
    }

    @Override
    public DataTable findRecordSnapshot(
        MainRecordPropertiesFilter mainRecordPropertiesFilter) {
        Stream<MainRecord> mainRecords = getMainRecords(mainRecordPropertiesFilter);
        return new RecordSnapshotMapper(userService, hojiContext)
            .mapDataRows(mainRecords, mainRecordPropertiesFilter.getFieldParent());
    }

    @Override
    public List<ImageData> findImageData(MainRecordPropertiesFilter mainRecordPropertiesFilter) {
        Stream<MainRecord> mainRecords;
        List<Integer> imageFieldIds =
                mainRecordPropertiesFilter.getFieldParent().getChildren()
                        .stream()
                        .filter(field -> field.getType().isImage())
                        .map(Field::getId)
                        .collect(Collectors.toList());
        if (mainRecordPropertiesFilter.getFieldParent() instanceof Form) {
            Aggregation aggregation = Aggregation.newAggregation(
                    Aggregation.match(mainRecordPropertiesFilter.getCriteria()),
                    Aggregation.unwind("liveFields"),
                    Aggregation.match(where("liveFields.fieldId").in(imageFieldIds)),
                    commonProjectionOperation()
                            .and("liveFields.fieldId").as("liveFields.fieldId")
                            .and("liveFields.recordUuid").as("liveFields.recordUuid")
                            .and("liveFields.dateCreated").as("liveFields.dateCreated")
                            .and("liveFields.dateUpdated").as("liveFields.dateUpdated")
                            .and("liveFields.uuid").as("liveFields.uuid")
                            .and("liveFields.value").as("liveFields.value"),
                    commonGroupOperation(Fields.UNDERSCORE_ID)
                            .first("matrixRecords").as("matrixRecords")
                            .push(new BasicDBObject("fieldId", "$liveFields.fieldId")
                                    .append("recordUuid", "$liveFields.recordUuid")
                                    .append("dateCreated", "$liveFields.dateCreated")
                                    .append("dateUpdated", "$liveFields.dateUpdated")
                                    .append("value", "$liveFields.value")
                                    .append("uuid", "$liveFields.uuid")
                            ).as("liveFields"),
                    sort(Sort.Direction.DESC, Fields.UNDERSCORE_ID),
                    limit(100)
            ).withOptions(newAggregationOptions().allowDiskUse(true).build());
            mainRecords = StreamUtils.createStreamFromIterator(
                    mongoOps.aggregateStream(aggregation, "mainRecord", MainRecord.class)
            );
        } else {
            Aggregation aggregation = Aggregation.newAggregation(
                    Aggregation.unwind("matrixRecords"),
                    Aggregation.match(mainRecordPropertiesFilter.getCriteria()),
                    Aggregation.unwind("matrixRecords.liveFields"),
                    Aggregation.match(where("matrixRecords.liveFields.fieldId").in(imageFieldIds)),
                    commonProjectionOperation(),
                    commonGroupOperation("matrixRecords.liveFields.uuid")
                            .first("matrixRecords.fieldId").as("matrixRecords_fieldId")
                            .first("matrixRecords.mainRecordUuid").as("matrixRecords_mainRecordUuid")
                            .first("matrixRecords.caption").as("matrixRecords_caption")
                            .first("matrixRecords.main").as("matrixRecords_main")
                            .first("matrixRecords.test").as("matrixRecords_test")
                            .first("matrixRecords.dateCreated").as("matrixRecords_dateCreated")
                            .first("matrixRecords.dateUpdated").as("matrixRecords_dateUpdated")
                            .first("matrixRecords.uuid").as("matrixRecords_uuid")
                            .push(new BasicDBObject("fieldId", "$matrixRecords.liveFields.fieldId")
                                    .append("recordUuid", "$matrixRecords.liveFields.recordUuid")
                                    .append("dateCreated", "$matrixRecords.liveFields.dateCreated")
                                    .append("dateUpdated", "$matrixRecords.liveFields.dateUpdated")
                                    .append("value", "$matrixRecords.liveFields.value")
                                    .append("uuid", "$matrixRecords.liveFields.uuid")
                            ).as("matrixRecords_liveFields"),
                    commonGroupOperation("uuid")
                            .push(new BasicDBObject("fieldId", "$matrixRecords_fieldId")
                                    .append("mainRecordUuid", "$matrixRecords_mainRecordUuid")
                                    .append("liveFields", "$matrixRecords_liveFields")
                                    .append("caption", "$matrixRecords_caption")
                                    .append("main", "$matrixRecords_main")
                                    .append("test", "$matrixRecords_test")
                                    .append("dateCreated", "$matrixRecords_dateCreated")
                                    .append("dateUpdated", "$matrixRecords_dateUpdated")
                                    .append("uuid", "$matrixRecords_uuid")
                            ).as("matrixRecords"),
                    sort(Sort.Direction.DESC, "dateCreated", "matrixRecords.dateCreated"),
                    limit(100)
            ).withOptions(newAggregationOptions().allowDiskUse(true).build());
            mainRecords = StreamUtils.createStreamFromIterator(
                    mongoOps.aggregateStream(aggregation, "mainRecord", MainRecord.class)
            );
        }
        return new ImageDataMapper(imageLocation).mapDataTable(mainRecords, mainRecordPropertiesFilter.getFieldParent());
    }

    private Stream<MainRecord> getMainRecords(MainRecordPropertiesFilter filter) {
        FieldParent fieldParent = filter.getFieldParent();
        if (fieldParent instanceof Form) {
            Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(filter.getCriteria()),
                Aggregation.unwind("liveFields"),
                commonProjectionOperation()
                    .and("liveFields.fieldId").as("liveFields.fieldId")
                    .and("liveFields.recordUuid").as("liveFields.recordUuid")
                    .and("liveFields.dateCreated").as("liveFields.dateCreated")
                    .and("liveFields.dateUpdated").as("liveFields.dateUpdated")
                    .and("liveFields.uuid").as("liveFields.uuid")
                    .and(
                        ConditionalOperators.when(
                            ComparisonOperators.Eq.valueOf(
                                DataTypeOperators.typeOf("liveFields.value")
                            ).equalToValue("string")
                        ).thenValueOf(
                            StringOperators.Substr.valueOf("liveFields.value")
                                .substring(0, fieldValueMaxLength)
                        ).otherwiseValueOf("liveFields.value")
                    ).as("value"),
                commonGroupOperation(Fields.UNDERSCORE_ID)
                    .first("matrixRecords").as("matrixRecords")
                    .push(new BasicDBObject("fieldId", "$liveFields.fieldId")
                        .append("recordUuid", "$liveFields.recordUuid")
                        .append("dateCreated", "$liveFields.dateCreated")
                        .append("dateUpdated", "$liveFields.dateUpdated")
                        .append("value", "$value")
                        .append("uuid", "$liveFields.uuid")
                    ).as("liveFields"),
                sort(Sort.Direction.DESC, Fields.UNDERSCORE_ID)
            ).withOptions(newAggregationOptions().allowDiskUse(true).build());
            return StreamUtils.createStreamFromIterator(
                mongoOps.aggregateStream(aggregation, "mainRecord", MainRecord.class)
            );
        } else {
            Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.unwind("matrixRecords"),
                Aggregation.match(filter.getCriteria()),
                commonProjectionOperation()
                        .and("liveFields").as("liveFields"),
                commonGroupOperation(Fields.UNDERSCORE_ID)
                        .first("liveFields").as("liveFields")
                        .addToSet("matrixRecords").as("matrixRecords"),
                sort(Sort.Direction.DESC, "dateCreated", "matrixRecords.dateCreated")
            ).withOptions(newAggregationOptions().allowDiskUse(true).build());
            ;
            return StreamUtils.createStreamFromIterator(
                mongoOps.aggregateStream(aggregation, "mainRecord", MainRecord.class)
            );
        }
    }

    private ProjectionOperation commonProjectionOperation() {
        return Aggregation.project(
            Fields.UNDERSCORE_ID, "uuid", "formId", "stage", "referenceId", "baseRecordUuid",
            "dateCompleted", "dateUploaded", "version", "userId", "deviceId", "startLatitude",
            "startLongitude", "startAccuracy", "startAge", "startAddress", "endLatitude",
            "endLongitude", "endAccuracy", "endAge", "endAddress", "formVersion", "appVersion",
            "matrixRecords", "caption", "searchTerms", "main", "test", "dateCreated", "dateUpdated"
        );
    }

    private GroupOperation commonGroupOperation(String groupBy) {
        return Aggregation.group(groupBy)
            .first("uuid").as("uuid")
            .first("formId").as("formId")
            .first("stage").as("stage")
            .first("referenceId").as("referenceId")
            .first("baseRecordUuid").as("baseRecordUuid")
            .first("dateCompleted").as("dateCompleted")
            .first("dateUploaded").as("dateUploaded")
            .first("version").as("version")
            .first("userId").as("userId")
            .first("deviceId").as("deviceId")
            .first("startLatitude").as("startLatitude")
            .first("startLongitude").as("startLongitude")
            .first("startAccuracy").as("startAccuracy")
            .first("startAge").as("startAge")
            .first("startAddress").as("startAddress")
            .first("endLatitude").as("endLatitude")
            .first("endLongitude").as("endLongitude")
            .first("endAccuracy").as("endAccuracy")
            .first("endAge").as("endAge")
            .first("endAddress").as("endAddress")
            .first("formVersion").as("formVersion")
            .first("appVersion").as("appVersion")
            .first("caption").as("caption")
            .first("searchTerms").as("searchTerms")
            .first("main").as("main")
            .first("test").as("test")
            .first("dateCreated").as("dateCreated")
            .first("dateUpdated").as("dateUpdated");
    }

    @Override
    public List<GpsPoint> findGpsPoints(MainRecordPropertiesFilter mainRecordPropertiesFilter) {
        Stream<MainRecord> mainRecords = getMainRecords(mainRecordPropertiesFilter);
        return new GpsPointMapper(userService).findGpsPoints(mainRecords, mainRecordPropertiesFilter);
    }

    @Override
    public List<Chart> findMonitorChartData(MainRecordPropertiesFilter mainRecordPropertiesFilter) {
        Stream<MainRecord> mainRecords = getMainRecords(mainRecordPropertiesFilter);
        return new MonitorDataMapper(userService).mapChart(mainRecords, mainRecordPropertiesFilter);
    }

    @Override
    public List<Chart> findChartingData(MainRecordPropertiesFilter mainRecordPropertiesFilter) {
        Stream<MainRecord> mainRecords =
            getMainRecords(mainRecordPropertiesFilter);
        return analysisDataMapper.mapChart(mainRecords, mainRecordPropertiesFilter);
    }

    @Override
    public List<LiteRecord> findLiteRecords(FilterCriteria filterCriteria) {
        return LiteRecordMapper.getInstance(mongoOps).findLiteRecords(filterCriteria);
    }

    @Override
    public List<MainRecord> findMainRecords(String uuid, List<Form> dependentForms) {
        MainRecord mainRecord = findMainRecord(uuid);
        List<MainRecord> mainRecordsWithRelations = new ArrayList<>();
        mainRecordsWithRelations.add(mainRecord);
        if (dependentForms.size() > 0) {
            mainRecordsWithRelations.addAll(findRelatedRecords(mainRecord, dependentForms));
        }
        return mainRecordsWithRelations;
    }

    private MainRecord findMainRecord(String uuid) {
        Criteria criteria = where("uuid").is(uuid);
        return mongoOps.findOne(query(criteria), MainRecord.class);
    }

    @Override
    public List<MainRecord> findMainRecords(FilterCriteria filterCriteria, List<Form> dependentForms) {
        Criteria criteria = filterCriteria.getCriteria();
        List<MainRecord> mainRecords = mongoOps.find(query(criteria), MainRecord.class);
        List<MainRecord> mainRecordsWithRelations = new ArrayList<>();
        for (MainRecord mainRecord : mainRecords) {
            mainRecordsWithRelations.add(mainRecord);
            if (dependentForms.size() > 0) {
                mainRecordsWithRelations.addAll(findRelatedRecords(mainRecord, dependentForms));
            }
        }
        return mainRecordsWithRelations;
    }

    private List<MainRecord> findRelatedRecords(MainRecord mainRecord, List<Form> dependentForms) {
        Criteria criteria =
            where("formId").in(dependentForms.stream().map(ConfigObject::getId).collect(Collectors.toSet()))
                .and("liveFields").elemMatch(where("value").is(mainRecord.getUuid()));
        return mongoOps.find(query(criteria), MainRecord.class);
    }

    @Override
    public Optional<ApiRecord> findApiRecord(String uuid, Form form) {
        Query query = Query.query(Criteria.where("uuid").is(uuid));
        List<MainRecord> mainRecords = mongoOps.find(query, MainRecord.class);
        return new ApiRecordMapper(userService, hojiContext)
            .getApiRecord(mainRecords, form);
    }

    @Override
    public List<User> findUsersWithRecords(Form form) {
        Aggregation aggregation = Aggregation.newAggregation(
            match(where("formId").is(form.getId())),
            project("userId")
        );
        AggregationResults<UserId> aggregationResults = mongoOps
            .aggregate(aggregation, "mainRecord", UserId.class);
        List<UserId> userIds = aggregationResults.getMappedResults();
        List<User> users =
                userService.findByIds(userIds.stream().map(userId -> userId.userId).collect(Collectors.toSet()));
        return users;
    }

    @Override
    public RecordOverview findRecordOverview(FieldParent fieldParent, String uuid) {
        MainRecord mainRecord;
        if (fieldParent instanceof Form) {
            Criteria criteria = where("uuid").is(uuid);
            mainRecord = mongoOps.findOne(query(criteria), MainRecord.class);
        } else {
            Criteria criteria = where("matrixRecords.uuid").is(uuid);
            Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.unwind("matrixRecords"),
                Aggregation.match(criteria),
                Aggregation.group("matrixRecords.uuid")
                    .first("uuid").as("uuid")
                    .first("startAddress").as("startAddress")
                    .first("startLatitude").as("startLatitude")
                    .first("startLongitude").as("startLongitude")
                    .first("userId").as("userId")
                    .first("dateCreated").as("dateCreated")
                    .first("dateUpdated").as("dateUpdated")
                    .first("dateCompleted").as("dateCompleted")
                    .first("dateUploaded").as("dateUploaded")
                    .push("matrixRecords").as("matrixRecords")
            );
            AggregationResults<MainRecord> aggregationResults = mongoOps.aggregate(aggregation, "mainRecord", MainRecord.class);
            mainRecord = aggregationResults.getUniqueMappedResult();
        }
        return recordOverviewMapper.mapRecordOverview(mainRecord, fieldParent);
    }

    @Override
    public long getCountOfRecords(FieldParent fieldParent) {
        Integer formId;
        if (fieldParent instanceof Form) {
            formId = fieldParent.getId();
        } else {
            formId = ((Field)fieldParent).getForm().getId();
        }
        return mongoOps.count(query(where("formId").is(formId).and("test").is(false)), "mainRecord");
    }

    @Override
    public void deleteRecord(String uuid, FieldParent fieldParent) {
        MainRecord mainRecord = mongoOps.findOne(query(where("uuid").is(uuid)), MainRecord.class);
        StringBuffer imageParentLocation = new StringBuffer(imageLocation);
        if (fieldParent instanceof Form) {
            mongoOps.remove(query(where("uuid").is(uuid)), MainRecord.class, "mainRecord");
            imageParentLocation.append(((Form)fieldParent).getId()).append(File.separator);
        } else {
            Criteria criteria = where("matrixRecords").elemMatch(where("uuid").is(uuid));
            Update update = new Update().pull("matrixRecords", new BasicDBObject("uuid", uuid));
            mongoOps.updateMulti(query(criteria), update, MainRecord.class);
            imageParentLocation.append(((Field)fieldParent).getForm().getId()).append(File.separator);
        }
        if (mainRecord != null) {
            List<Integer> imageFieldIds = fieldParent.getChildren()
                    .stream()
                    .filter(field -> field.getType().isImage())
                    .map(Field::getId)
                    .collect(Collectors.toList());
            mainRecord.getLiveFields()
                    .stream()
                    .filter(liveField -> imageFieldIds.contains(liveField.getFieldId()))
                    .forEach(liveField -> getImageFile(imageParentLocation, liveField).delete());
            mainRecord.getMatrixRecords()
                    .stream()
                    .flatMap(matrixRecord -> matrixRecord.getLiveFields().stream())
                    .filter(liveField -> imageFieldIds.contains(liveField.getFieldId()))
                    .forEach(liveField -> getImageFile(imageParentLocation, liveField).delete());
        }
    }

    @Override
    public void clearData(Survey survey) {
        List<Integer> formIds = findFormIdsInSurvey(survey);
        mongoOps.remove(query(where("formId").in(formIds)), "mainRecord");
        formIds.forEach(formId -> deleteFiles(new File(imageLocation + formId)));
    }

    @Override
    public void clearData(Form form) {
        mongoOps.remove(query(where("formId").is(form.getId())), "mainRecord");
        deleteFiles(new File(imageLocation + form.getId()));
    }

    @Override
    public void clearData(Field field) {
        if (field.getParent() != null) {
            Map<String, Object> updates = new HashMap<>();
            updates.put("q", new Document("matrixRecords.fieldId", field.getParent().getId()));
            updates.put("u", new Document("$pull",
                    new Document("matrixRecords.$[mIndex].liveFields", new Document("fieldId", field.getId()))));
            updates.put("multi", true);
            updates.put("arrayFilters",
                    Collections.singletonList(new Document("mIndex.fieldId", field.getParent().getId())));
            Document updateCommand = new Document()
                    .append("update", "mainRecord")
                    .append("updates", Collections.singletonList(updates));
            mongoOps.executeCommand(updateCommand);
        } else {
            Criteria criteria = Criteria.where("liveFields.fieldId").is(field.getId());
            Update update = new Update().pull("liveFields", new BasicDBObject("fieldId", field.getId()));
            mongoOps.updateMulti(query(criteria), update, MainRecord.class);
        }
        deleteFiles(new File(imageLocation + field.getForm().getId() + File.separator + field.getId()));
    }

    @Override
    public void updateTestMode(String uuid, boolean isTest) {
        mongoOps.updateFirst(
            query(where("uuid").is(uuid)),
            new Update().set("test", isTest),
            MainRecord.class
        );
    }

    @Override
    public void migrateImages(Integer formId) {
        mongoOps.stream(query(where("formId").is(formId)), MainRecord.class)
                .forEachRemaining(this::save);
    }

    private List<Integer> findFormIdsInSurvey(Survey survey) {
        List<Integer> formIds = new ArrayList<>();
        for (Form form : survey.getForms()) {
            formIds.add(form.getId());
        }
        return formIds;
    }

    private static class UserId {

        final Integer userId;

        public UserId(Integer userId) {
            this.userId = userId;
        }
    }

    private void deleteFiles(File dir) {
        if (dir.exists()) {
            File[] filesInDir = dir.listFiles();
            if (filesInDir != null) {
                for (File file : dir.listFiles()) {
                    if (file.isDirectory()) {
                        deleteFiles(file);
                    } else {
                        file.delete();
                    }
                }
            }
            dir.delete();
        }
    }

    private File getImageFile(StringBuffer imagePath, LiveField liveField) {
        return new File(
                imagePath
                        .append(liveField.getFieldId())
                        .append(File.separator)
                        .append(liveField.getUuid())
                        .append(".png")
                        .toString()
        );
    }
}
