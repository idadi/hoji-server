package ke.co.hoji.server.repository;

import ke.co.hoji.server.calculation.CalculationResult;

public interface CalculationResultRepositoryCustom {

    CalculationResult save(CalculationResult calculationResult);

}
