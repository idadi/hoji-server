package ke.co.hoji.server.repository.impl;

import ke.co.hoji.server.repository.FilterCriteria;
import org.springframework.data.mongodb.core.aggregation.Aggregation;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

/**
 * Created by geoffreywasilwa on 05/06/2017.
 */
class LiteRecordSearchAggregator {
    static Aggregation getAggregation(FilterCriteria filterCriteria) {
        return Aggregation.newAggregation(
                match(filterCriteria.getCriteria()),
                project("uuid", "caption", "dateCreated", "dateUpdated", "searchTerms")
        );
    }
}
