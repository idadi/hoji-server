package ke.co.hoji.server.repository;

import ke.co.hoji.server.calculation.CalculationResult;
import org.springframework.data.repository.Repository;

public interface CalculationResultRepository extends CalculationResultRepositoryCustom, Repository<CalculationResult, String> {

    CalculationResult findByObjectIdAndRecordUuidAndFieldId(String objectId, String recordUuid, Integer fieldId);

    void deleteByRecordUuidAndFormIdAndFieldId(String recordUuid, Integer formId, Integer fieldId);

}
