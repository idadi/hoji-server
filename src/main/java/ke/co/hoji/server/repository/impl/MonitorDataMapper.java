package ke.co.hoji.server.repository.impl;

import static java.util.stream.Collectors.groupingBy;
import static ke.co.hoji.server.utils.ServerUtils.getAggregatedDataPointsSampleSize;
import static ke.co.hoji.server.utils.ServerUtils.getDataPointsSampleSize;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.server.model.Chart;
import ke.co.hoji.server.model.DataPoint;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter;
import ke.co.hoji.server.service.UserService;

/**
 * Created by geoffreywasilwa on 21/05/2017.
 */
final class MonitorDataMapper {

    private static final String RECORDS_BY_ENUMERATOR_LABEL = "1. Number of records by enumerator";
    private static final String RECORD_SPEED_LABEL = "2. Speed of completion per record (Fields per Minute)";
    private static final String RECORD_COMPLETION_TIME_LABEL = "3. Time to complete per record (Minutes)";
    private static final String RECORD_UPLOAD_TIME_LABEL = "4. Time to upload per record (Minutes)";
    private static final String RECORDS_BY_UPLOAD_VERSION_LABEL = "5. Number of records by upload version";
    private static final String RECORDS_BY_FORM_VERSION_LABEL = "6. Number of records by form version";
    private static final String RECORDS_BY_APP_VERSION_LABEL = "7. Number of records by app version";
    private static final String RECORDS_BY_GEO_TAGGING_LABEL = "8. Number of geo-tagged records";
    private static final String RECORD_GPS_ACCURACY_LABEL = "9. GPS accuracy per record (Meters)";
    private static final String RECORD_DISTANCE_LABEL = "10. Distance between record start are record end (Meters)";
    private static final int MILLS_IN_MINUTE = 60000;

    private final UserService userService;

    MonitorDataMapper(UserService userService) {
        this.userService = userService;
    }

    List<Chart> mapChart(Stream<MainRecord> mainRecordStream, MainRecordPropertiesFilter filter) {
        List<MainRecord> mainRecords = mainRecordStream
            .filter(mainRecord -> filter.passesFilterFieldCriteria(mainRecord.getLiveFields()))
            .collect(Collectors.toList());
        final Predicate<MainRecord> geoTagged = mainRecord ->
            !mainRecord.getStartLatitude().equals(0D) && !mainRecord.getStartLongitude().equals(0D);
        List<Chart> charts = new ArrayList<>();
        {
            Map<Integer, Long> recordsByEnumerator = mainRecords
                .stream()
                .collect(groupingBy(MainRecord::getUserId, Collectors.counting()));
            List<DataPoint> dataPointsWithNames = new ArrayList<>();
            for (Integer userId : recordsByEnumerator.keySet()) {
                String fullName = userService.findById(userId).getFullName();
                dataPointsWithNames.add(new DataPoint(fullName, recordsByEnumerator.get(userId)));
            }
            Chart chart = buildChart(1, new BigDecimal(1), Chart.Type.CATEGORICAL, RECORDS_BY_ENUMERATOR_LABEL, dataPointsWithNames, true);
            charts.add(chart);
        }
        {
            List<DataPoint> dataPoints = mainRecords
                .stream()
                .filter(mainRecord -> mainRecord.getLiveFields() != null && filter.passesFilterFieldCriteria(mainRecord.getLiveFields()))
                .map(mainRecord -> {
                    Integer mLiveFields = mainRecord.getLiveFields().size();
                    Long mxLiveFields;
                    if (mainRecord.getMatrixRecords() != null) {
                        mxLiveFields = mainRecord.getMatrixRecords()
                            .stream()
                            .mapToLong(matrixRecord -> matrixRecord.getLiveFields().size())
                            .sum();
                    } else {
                        mxLiveFields = 0L;
                    }
                    Long fieldCount = mLiveFields + mxLiveFields;
                    Double completionTime =
                        (Double.valueOf(mainRecord.getDateCompleted()) - mainRecord.getDateCreated()) / MILLS_IN_MINUTE;
                    Double averageSpeed = Double.valueOf(fieldCount) / completionTime;
                    return new DataPoint(getRecordCaption(mainRecord), Utils.roundDouble(averageSpeed));
                })
                .collect(Collectors.toList());
            Chart chart = buildChart(2, new BigDecimal(2), Chart.Type.NUMERIC_DECIMAL, RECORD_SPEED_LABEL, "Fields per Minute", dataPoints, false);
            charts.add(chart);
        }
        {
            List<DataPoint> dataPoints = mainRecords
                .stream()
                .map(mainRecord -> {
                    Double completed = mainRecord.getDateCompleted() != null
                        ? Double.valueOf(mainRecord.getDateCompleted())
                        : 0D;
                    Double created = mainRecord.getDateCreated() != null
                        ? Double.valueOf(mainRecord.getDateCreated())
                        : 0D;
                    Double duration = Utils.roundDouble((completed - created) / MILLS_IN_MINUTE);
                    return new DataPoint(getRecordCaption(mainRecord), duration);
                })
                .collect(Collectors.toList());
            Chart chart = buildChart(3, new BigDecimal(3), Chart.Type.NUMERIC_DECIMAL, RECORD_COMPLETION_TIME_LABEL, "Minutes", dataPoints, false);
            charts.add(chart);
        }
        {
            List<DataPoint> dataPoints = mainRecords
                .stream()
                .map(mainRecord -> {
                    Double uploaded = mainRecord.getDateUploaded() != null
                        ? Double.valueOf(mainRecord.getDateUploaded())
                        : 0D;
                    Double updated = mainRecord.getDateUpdated() != null
                        ? Double.valueOf(mainRecord.getDateUpdated())
                        : 0D;
                    Double duration = Utils.roundDouble((uploaded - updated) / MILLS_IN_MINUTE);
                    return new DataPoint(getRecordCaption(mainRecord), duration);
                })
                .collect(Collectors.toList());
            Chart chart = buildChart(4, new BigDecimal(4), Chart.Type.NUMERIC_DECIMAL, RECORD_UPLOAD_TIME_LABEL, "Minutes", dataPoints, false);
            charts.add(chart);
        }
        {
            Map<Integer, Long> recordsByUploadVersion = mainRecords
                .stream()
                .filter(mainRecord -> mainRecord.getVersion() != null)
                .collect(groupingBy(MainRecord::getVersion, Collectors.counting()));
            List<DataPoint> dataPoints = new ArrayList<>();
            for (Integer version : recordsByUploadVersion.keySet()) {
                dataPoints.add(new DataPoint(String.valueOf(version), recordsByUploadVersion.get(version)));
            }
            Chart chart = buildChart(5, new BigDecimal(5), Chart.Type.CATEGORICAL, RECORDS_BY_UPLOAD_VERSION_LABEL, dataPoints, true);
            charts.add(chart);
        }
        {
            Map<String, Long> recordsByFormVersion = mainRecords
                .stream()
                .filter(mainRecord -> mainRecord.getFormVersion() != null)
                .collect(groupingBy(MainRecord::getFormVersion, Collectors.counting()));
            List<DataPoint> dataPoints = new ArrayList<>();
            for (String formVersion : recordsByFormVersion.keySet()) {
                dataPoints.add(new DataPoint(formVersion, recordsByFormVersion.get(formVersion)));
            }
            Chart chart = buildChart(6, new BigDecimal(6), Chart.Type.CATEGORICAL, RECORDS_BY_FORM_VERSION_LABEL, dataPoints, true);
            charts.add(chart);
        }
        {
            Map<String, Long> recordsByAppVersion = mainRecords
                .stream()
                .filter(mainRecord -> mainRecord.getAppVersion() != null)
                .collect(groupingBy(MainRecord::getAppVersion, Collectors.counting()));
            List<DataPoint> dataPoints = new ArrayList<>();
            for (String appVersion : recordsByAppVersion.keySet()) {
                dataPoints.add(new DataPoint(appVersion, recordsByAppVersion.get(appVersion)));
            }
            Chart chart = buildChart(7, new BigDecimal(7), Chart.Type.CATEGORICAL, RECORDS_BY_APP_VERSION_LABEL,
                    dataPoints, true);
            charts.add(chart);
        }
        {
            Map<String, Long> recordsByGeoTagging = mainRecords
                .stream()
                .map(mainRecord -> geoTagged.test(mainRecord) ? "Yes" : "No")
                .collect(groupingBy(tagged -> tagged, Collectors.counting()));
            List<DataPoint> dataPoints = new ArrayList<>();
            for (String taggedState : recordsByGeoTagging.keySet()) {
                dataPoints.add(new DataPoint(taggedState, recordsByGeoTagging.get(taggedState)));
            }
            Chart chart = buildChart(8, new BigDecimal(8), Chart.Type.BINARY, RECORDS_BY_GEO_TAGGING_LABEL, dataPoints,
                    true);
            charts.add(chart);
        }
        {
            List<DataPoint> dataPoints = mainRecords
                .stream()
                .filter(geoTagged)
                .map(mainRecord -> new DataPoint(getRecordCaption(mainRecord), mainRecord.getStartAccuracy()))
                .collect(Collectors.toList());
            Chart chart = buildChart(9, new BigDecimal(9), Chart.Type.NUMERIC_DECIMAL, RECORD_GPS_ACCURACY_LABEL,
                    "Meters", dataPoints, false);
            charts.add(chart);
        }
        {
            List<DataPoint> dataPoints = mainRecords
                .stream()
                .filter(mainRecord ->
                    !mainRecord.getStartLatitude().equals(0D)
                        && !mainRecord.getEndLatitude().equals(0D)
                        && !mainRecord.getStartLongitude().equals(0D)
                        && !mainRecord.getEndLongitude().equals(0D)
                )
                .map(mainRecord -> {
                    Double distance = Utils.distance(
                        mainRecord.getStartLatitude(),
                        mainRecord.getEndLatitude(),
                        mainRecord.getStartLongitude(),
                        mainRecord.getEndLongitude()
                    );
                    return new DataPoint(getRecordCaption(mainRecord), Utils.roundDouble(distance));
                })
                .collect(Collectors.toList());
            Chart chart = buildChart(10, new BigDecimal(10), Chart.Type.NUMERIC_DECIMAL, RECORD_DISTANCE_LABEL,
                    "Meters", dataPoints, false);
            charts.add(chart);
        }
        return charts;
    }

    private Chart buildChart(int key, BigDecimal ordinal, int type, String label, List<DataPoint> dataPoints, boolean add) {
        return buildChart(key, ordinal, type, label, null, dataPoints, add);
    }

    private Chart buildChart(int key, BigDecimal ordinal, int type, String label, String numberLabel, List<DataPoint> dataPoints, boolean add) {
        Chart chart = new Chart(key, ordinal, type, label);
        chart.setData(dataPoints);
        chart.setNumberLabel(numberLabel);
        int sampleSize = 0;
        if (add) {
            sampleSize = getAggregatedDataPointsSampleSize(dataPoints);
        } else {
            sampleSize = getDataPointsSampleSize(dataPoints);
        }
        chart.setSampleSize(sampleSize);
        return chart;
    }

    private String getRecordCaption(MainRecord mainRecord) {
        String caption = "";
        if (!StringUtils.isBlank(mainRecord.getCaption())) {
            caption = mainRecord.getCaption();
        } else {
            if (!StringUtils.isBlank(mainRecord.getUuid())) {
                caption = mainRecord.getUuid().split("-")[0];
            }
        }
        return caption;
    }
}
