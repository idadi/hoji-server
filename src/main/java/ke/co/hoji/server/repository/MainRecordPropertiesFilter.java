package ke.co.hoji.server.repository;

import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 * <p>
 * Container that allow clients to specify criteria based on {@link MainRecord} properties
 * </p>
 * Created by geoffreywasilwa on 05/06/2017.
 */
public class MainRecordPropertiesFilter implements FilterCriteria {
    private final FieldParent fieldParent;
    private final boolean testMode;
    private final Date createdFrom;
    private final Date createdTo;
    private final Date completedFrom;
    private final Date completedTo;
    private final Date uploadedFrom;
    private final Date uploadedTo;
    private final List<Integer> userIds;
    private final List<String> uuids;
    private final Integer filterFieldId;
    private final Integer filterFieldValue;

    MainRecordPropertiesFilter(
            FieldParent fieldParent, boolean testMode, Date createdFrom, Date createdTo, Date completedFrom, Date completedTo,
            Date uploadedFrom, Date uploadedTo, List<Integer> userIds, List<String> uuids, Integer filterFieldId,
            Integer filterFieldValue) {
        this.fieldParent = fieldParent;
        this.testMode = testMode;
        this.createdFrom = createdFrom;
        this.createdTo = createdTo;
        this.completedFrom = completedFrom;
        this.completedTo = completedTo;
        this.uploadedFrom = uploadedFrom;
        this.uploadedTo = uploadedTo;
        this.userIds = userIds != null ? userIds : new ArrayList<>();
        this.uuids = uuids;
        this.filterFieldId = filterFieldId;
        this.filterFieldValue = filterFieldValue;
    }

    public FieldParent getFieldParent() {
        return fieldParent;
    }

    @Override
    public Criteria getCriteria() {
        Criteria criteria;
        if (fieldParent instanceof Form) {
            criteria = where("formId").is(fieldParent.getId());
            criteria.and("test").is(testMode);
        } else {
            criteria = where("matrixRecords.fieldId").is(fieldParent.getId());
            criteria.and("test").is(testMode);
        }
        if (userIds.size() > 0) {
            criteria.and("userId").in(userIds);
        }
        if (uuids.size() > 0) {
            criteria.and("uuid").in(uuids);
        }
        if (completedFrom != null && completedTo != null) {
            criteria.and("dateCompleted").gte(completedFrom.getTime()).lte(completedTo.getTime());
        } else if (completedFrom != null) {
            criteria.and("dateCompleted").gte(completedFrom.getTime());
        } else if (completedTo != null) {
            criteria.and("dateCompleted").lte(completedTo.getTime());
        }

        if (uploadedFrom != null && uploadedTo != null) {
            criteria.and("dateUploaded").gte(uploadedFrom.getTime()).lte(uploadedTo.getTime());
        } else if (uploadedFrom != null) {
            criteria.and("dateUploaded").gte(uploadedFrom.getTime());
        } else if (uploadedTo != null) {
            criteria.and("dateUploaded").lte(uploadedTo.getTime());
        }

        if (createdFrom != null && createdTo != null) {
            criteria.and("dateCreated").gt(createdFrom.getTime()).lt(createdTo.getTime());
        } else if (createdFrom != null) {
            criteria.and("dateCreated").gt(createdFrom.getTime());
        } else if (createdTo != null) {
            criteria.and("dateCreated").lt(createdTo.getTime());
        }

        return criteria;
    }

    /**
     * Checks where a collection of {@link LiveField}s passes supplied filter field criteria
     *
     * @param liveFields, a collection of {@link LiveField}s from the same record
     * @return boolean, indicating whether the liveFields passes filter field criteria
     */
    public boolean passesFilterFieldCriteria(List<LiveField> liveFields) {
        if (filterFieldId != null && filterFieldValue != null) {
            Optional<LiveField> match = liveFields.stream()
                    .filter(liveField -> {
                        Integer value = liveField.getValue() != null ?
                                Utils.parseInteger(liveField.getValue().toString()) : null;
                        return filterFieldId.equals(liveField.getFieldId()) && filterFieldValue.equals(value);
                    })
                    .findAny();
            return match.isPresent();
        }
        return true;
    }

    public static class MainRecordPropertiesFilterBuilder {
        private final FieldParent fieldParent;
        private boolean testMode;
        private Date createdFrom;
        private Date createdTo;
        private Date completedFrom;
        private Date completedTo;
        private Date uploadedFrom;
        private Date uploadedTo;
        private List<Integer> userIds;
        private List<String> uuids = new ArrayList<>();
        private Integer filterFieldId;
        private Integer filterFieldValue;

        public MainRecordPropertiesFilterBuilder(FieldParent fieldParent) {
            this.fieldParent = fieldParent;
        }

        public MainRecordPropertiesFilterBuilder testMode(boolean testMode) {
            this.testMode = testMode;
            return this;
        }

        public MainRecordPropertiesFilterBuilder createdFrom(Date createdFrom) {
            if (createdFrom == null) {
                return this;
            }
            this.createdFrom = createdFrom;
            return this;
        }

        public MainRecordPropertiesFilterBuilder createdTo(Date createdTo) {
            if (createdTo == null) {
                return this;
            }
            this.createdTo = createdTo;
            return this;
        }

        public MainRecordPropertiesFilterBuilder completedFrom(Date completedFrom) {
            if (completedFrom == null) {
                return this;
            }
            this.completedFrom = completedFrom;
            return this;
        }

        public MainRecordPropertiesFilterBuilder completedTo(Date completedTo) {
            if (completedTo == null) {
                return this;
            }
            this.completedTo = completedTo;
            return this;
        }

        public MainRecordPropertiesFilterBuilder uploadedFrom(Date uploadedFrom) {
            if (uploadedFrom == null) {
                return this;
            }
            this.uploadedFrom = uploadedFrom;
            return this;
        }

        public MainRecordPropertiesFilterBuilder uploadedTo(Date uploadedTo) {
            if (uploadedTo == null) {
                return this;
            }
            this.uploadedTo = uploadedTo;
            return this;
        }

        public MainRecordPropertiesFilterBuilder userIds(List<Integer> userIds) {
            if (userIds == null) {
                return this;
            }
            this.userIds = new ArrayList<>(userIds);
            return this;
        }

        public MainRecordPropertiesFilterBuilder recordUuid(String... uuids) {
            if (uuids.length == 0) {
                return this;
            }
            for (String uuid : uuids) {
                this.uuids.add(uuid);
            }
            return this;
        }

        public MainRecordPropertiesFilterBuilder filterFieldId(Integer filterFieldId) {
            if (filterFieldId == null) {
                return this;
            }
            this.filterFieldId = filterFieldId;
            return this;
        }

        public MainRecordPropertiesFilterBuilder filterFieldValue(Integer filterFieldValue) {
            if (filterFieldValue == null) {
                return this;
            }
            this.filterFieldValue = filterFieldValue;
            return this;
        }

        public MainRecordPropertiesFilter build() {
            return new MainRecordPropertiesFilter(fieldParent, testMode, createdFrom, createdTo, completedFrom, completedTo,
                    uploadedFrom, uploadedTo, userIds, uuids, filterFieldId, filterFieldValue);
        }
    }
}
