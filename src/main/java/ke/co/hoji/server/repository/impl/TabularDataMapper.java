package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.Utils;
import ke.co.hoji.core.data.dto.DataElement;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.server.calculation.CalculationResult;
import ke.co.hoji.server.calculation.Calculator;
import ke.co.hoji.server.converter.DtoToModelLiveFieldConverter;
import ke.co.hoji.server.model.DataRow;
import ke.co.hoji.server.model.DataTable;
import ke.co.hoji.server.model.Metadata;
import ke.co.hoji.server.repository.MainRecordPropertiesFilter;
import ke.co.hoji.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ke.co.hoji.core.data.dto.DataElement.Type.STRING;
import static ke.co.hoji.server.model.Metadata.Label.CREATED_BY;
import static ke.co.hoji.server.model.Metadata.Label.MAIN_RECORD_ID;
import static ke.co.hoji.server.model.Metadata.Label.MAIN_RECORD_UUID;
import static ke.co.hoji.server.model.Metadata.Label.RECORD_ID;
import static ke.co.hoji.server.model.Metadata.Label.RECORD_UUID;

/**
 * <p>
 * Creates a {@link DataTable} from a collection of {@link MainRecord}s
 * </p>
 * Created by geoffreywasilwa on 21/05/2017.
 */
@Component
public class TabularDataMapper {

    private final UserService userService;
    private final HojiContext hojiContext;
    private final Calculator calculator;

    @Autowired
    public TabularDataMapper(UserService userService, HojiContext hojiContext, Calculator calculator) {
        this.userService = userService;
        this.hojiContext = hojiContext;
        this.calculator = calculator;
    }

    public DataTable mapDataTable(Stream<MainRecord> mainRecords, long totalRecords, MainRecordPropertiesFilter filter,
                                  String renderingType) {
        DataTable dataTable = readData(mainRecords, totalRecords, filter, renderingType);
        if (filter.getFieldParent() instanceof Form && Form.OutputStrategy.PIVOT.equals(renderingType)) {
            Form form = (Form) filter.getFieldParent();
            if (form.isTransposable()) {
                return dataTable.requestTranspose(form);
            }
        }
        return dataTable;
    }

    private DataTable readData(
        Stream<MainRecord> mainRecords,
        Long totalRecords,
        MainRecordPropertiesFilter filter,
        String renderingType
    ) {
        List<DataRow> dataRows = new ArrayList<>();
        Integer surveyId;
        if (filter.getFieldParent() instanceof Form) {
            surveyId = ((Form) filter.getFieldParent()).getSurvey().getId();
        } else {
            surveyId = ((Field) filter.getFieldParent()).getForm().getSurvey().getId();
        }
        dataRows.addAll(getData(mainRecords, totalRecords, filter, renderingType));
        return new DataTable(dataRows, getDefaultRow(filter.getFieldParent()));
    }

    private String getDefaultRow(FieldParent parent) {
        Field match = null;
        for (Field field : parent.getChildren()) {
            if (field.getType().isChoice()) {
                match = field;
                break;
            }
        }
        String defaultRow;
        if (match != null) {
            defaultRow = match.getHeader();
            if (match.getType().isMultiChoice()) {
                defaultRow += " (" + match.getChoiceGroup().getChoices().get(0).getName() + ")";
            }
        } else {
            defaultRow = CREATED_BY;
        }
        return defaultRow;
    }

    private List<DataRow> getData(
        Stream<MainRecord> mainRecords,
        long totalRecords,
        MainRecordPropertiesFilter filter,
        String renderingType
    ) {
        List<DataRow> records = new ArrayList<>();
        if (filter.getFieldParent() instanceof Form) {
            records = getFormData(mainRecords, totalRecords, filter, renderingType);
        } else if (filter.getFieldParent() instanceof Field) {
            records = getFieldData(mainRecords, totalRecords, filter, renderingType);
        }
        return records;
    }

    private List<DataRow> getFormData(
        Stream<MainRecord> mainRecords,
        long totalRecords,
        MainRecordPropertiesFilter filter,
        String renderingType
    ) {
        AtomicLong recordId = new AtomicLong(totalRecords);
        return mainRecords
                .map(mainRecord -> {
                    FieldParent fieldParent = filter.getFieldParent();
                    if (filter.passesFilterFieldCriteria(mainRecord.getLiveFields())) {
                        List<ke.co.hoji.core.data.model.LiveField> liveFields =
                                dtoToModel(fieldParent.getChildren().stream(), mainRecord, mainRecord.getUuid(),
                                        mainRecord.getLiveFields());
                        List<DataElement> actualData = findActualData(liveFields, renderingType);
                        Metadata metadata = new Metadata(mainRecord, userService);
                        List<DataElement> record = new ArrayList<>(metadata.getIdentifierMetadata());
                        record.add(
                                new DataElement(
                                        RECORD_ID,
                                        String.valueOf(recordId.getAndDecrement()),
                                        DataElement.Type.NUMBER,
                                        ""
                                )
                        );
                        record.addAll(actualData);
                        record.addAll(metadata.getMetadata());
                        return new DataRow(record);
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private List<DataRow> getFieldData(
        Stream<MainRecord> mainRecords,
        long totalRecords,
        MainRecordPropertiesFilter filter,
        String renderingType
    ) {
        FieldParent fieldParent = filter.getFieldParent();
        AtomicLong recordCount = new AtomicLong(totalRecords);
        AtomicLong matrixRecordId = new AtomicLong(1);
        return mainRecords
                .flatMap(mainRecord -> {
                    Long mainRecordId = recordCount.getAndDecrement();
                    return mainRecord.getMatrixRecords().stream().map(matrixRecord -> {
                        List<LiveField> liveFieldsToConvert = new ArrayList<>();
                        liveFieldsToConvert.addAll(mainRecord.getLiveFields());
                        liveFieldsToConvert.addAll(matrixRecord.getLiveFields());
                        Stream<Field> allFields = ((Field)fieldParent).getForm().getFields()
                                .stream()
                                .filter(field -> {
                                    if (fieldParent.equals(field)) {
                                        return true;
                                    }
                                    return field.isSearchable();
                                })
                                .flatMap(field -> {
                                    if (fieldParent.equals(field)) {
                                        return fieldParent.getChildren().stream();
                                    }
                                    return Stream.of(field);
                                });
                        if (filter.passesFilterFieldCriteria(liveFieldsToConvert)) {
                            List<ke.co.hoji.core.data.model.LiveField> liveFields =
                                    dtoToModel(allFields, mainRecord, matrixRecord.getUuid(), liveFieldsToConvert);
                            List<DataElement> actualData = findActualData(liveFields, renderingType);
                            List<DataElement> record = new ArrayList<>();
                            record.add(
                                    new DataElement(
                                            MAIN_RECORD_UUID,
                                            matrixRecord.getMainRecordUuid(),
                                            STRING,
                                            ""
                                    )
                            );
                            record.add(
                                    new DataElement(
                                            RECORD_UUID,
                                            matrixRecord.getUuid(),
                                            STRING, ""
                                    )
                            );
                            record.add(
                                    new DataElement(
                                            RECORD_ID,
                                            String.valueOf(matrixRecordId.getAndIncrement()),
                                            DataElement.Type.STRING,
                                            ""
                                    )
                            );
                            record.add(
                                    new DataElement(
                                            MAIN_RECORD_ID,
                                            String.valueOf(mainRecordId),
                                            DataElement.Type.STRING,
                                            ""
                                    )
                            );
                            record.addAll(actualData);
                            return new DataRow(record);
                        }
                        return null;
                    });
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private List<ke.co.hoji.core.data.model.LiveField> dtoToModel(
            Stream<Field> fields,
            MainRecord mainRecord,
            String recordUuid,
            List<LiveField> liveFields
    ) {
        return fields
                .filter(this::isAccessible)
                .map(field -> {
                    LiveField found = liveFields
                            .stream()
                            .filter(liveField -> field.getId() == liveField.getFieldId())
                            .findFirst()
                            .orElse(new LiveField());
                    ke.co.hoji.core.data.model.LiveField converted
                            = new DtoToModelLiveFieldConverter(hojiContext, field).convert(found);
                    if (field.hasScriptFunction(Field.ScriptFunctions.CALCULATE)) {
                        field.setResponse(converted.getResponse());
                        CalculationResult result = calculator.calculate(mainRecord, recordUuid, field);
                        Response response = converted.getResponse();
                        if (result.isEvaluated() && result.getValue() != null) {
                            response = Response.create(hojiContext, converted, result.getValue(), true);
                        }
                        converted.setResponse(response);
                    }
                    return converted;
                })
                .collect(Collectors.toList());
    }

    private List<DataElement> findActualData(
        List<ke.co.hoji.core.data.model.LiveField> liveFields,
        String renderingType
    ) {
        return liveFields.stream().filter(liveField -> isAccessible(liveField.getField()))
            .map(liveField -> (List<DataElement>) liveField.getResponse().getValue(renderingType))
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

    private boolean isAccessible(Field field) {
        return field != null && field.isEnabled() && !field.getType().isLabel() && !field.isMatrix();
    }

}
