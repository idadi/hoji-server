package ke.co.hoji.server.repository.impl;

import ke.co.hoji.core.HojiContext;
import ke.co.hoji.core.data.dto.LiveField;
import ke.co.hoji.core.data.dto.MainRecord;
import ke.co.hoji.core.data.model.Field;
import ke.co.hoji.core.data.model.FieldParent;
import ke.co.hoji.core.data.model.Form;
import ke.co.hoji.core.data.model.Record;
import ke.co.hoji.core.response.Response;
import ke.co.hoji.server.calculation.CalculationResult;
import ke.co.hoji.server.calculation.Calculator;
import ke.co.hoji.server.converter.DtoToModelLiveFieldConverter;
import ke.co.hoji.server.model.RecordMetadata;
import ke.co.hoji.server.model.RecordOverview;
import ke.co.hoji.server.model.User;
import ke.co.hoji.server.service.UserService;
import ke.co.hoji.server.utils.ServerUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Maps {@link MainRecord} to {@link RecordOverview}</p>
 * Created by geoffreywasilwa on 25/05/2017.
 */
@Component
public
class RecordOverviewMapper {

    private final UserService userService;
    private final HojiContext hojiContext;
    private final Calculator calculator;

    RecordOverviewMapper(UserService userService, HojiContext hojiContext, Calculator calculator) {
        this.userService = userService;
        this.hojiContext = hojiContext;
        this.calculator = calculator;
    }

    RecordOverview mapRecordOverview(MainRecord mainRecord, FieldParent fieldParent) {
    	RecordMetadata recordMetadata = findRecordMetadata(mainRecord, fieldParent);
    	List<ke.co.hoji.core.data.model.LiveField> liveFields = getLiveFields(fieldParent, mainRecord);
    	String caption = getCaption(mainRecord, liveFields, fieldParent);
    	Record record = new Record(fieldParent, mainRecord.isMain(), mainRecord.isTest(), caption);
    	record.setLiveFields(liveFields);
        return new RecordOverview(record, recordMetadata);
    }

    /*
     * Return the record caption, replacing any non-encrypted caption text created from an encrypted field with
     * the encrypted value itself, shortened to 5 characters for brevity. This avoids leaking encrypted data.
     */
    private String getCaption(MainRecord mainRecord, List<ke.co.hoji.core.data.model.LiveField> liveFields, FieldParent fieldParent) {
        String caption = mainRecord.getCaption();
        for (ke.co.hoji.core.data.model.LiveField liveField : liveFields) {
            Field field = liveField.getField();
            if (field.isCaptioning() && field.getType().isEncrypted()) {
                caption = liveField.getResponse().displayString();
                if (!StringUtils.isBlank(caption) && caption.length() > 5) {
                    caption = caption.substring(0, 5) + "...";
                }
            }
        }
        if (StringUtils.isBlank(caption)) {
            caption = fieldParent.getName();
        }
        return caption;
    }

    private RecordMetadata findRecordMetadata(MainRecord mainRecord, FieldParent fieldParent) {
        RecordMetadata recordMetadata = new RecordMetadata();
        if (fieldParent instanceof Form) {
            recordMetadata.setUuid(mainRecord.getUuid());
        } else {
            recordMetadata.setUuid(mainRecord.getMatrixRecords().get(0).getUuid());
        }
        recordMetadata.setTest(mainRecord.isTest());
        User user = userService.findById(mainRecord.getUserId());
        recordMetadata.setCreatedBy(user.getFullName());
        recordMetadata.setDateCreated(mainRecord.getDateCreated());
        recordMetadata.setAddress(mainRecord.getStartAddress());
        recordMetadata.setLatitude(mainRecord.getStartLatitude());
        recordMetadata.setLongitude(mainRecord.getStartLongitude());
        return recordMetadata;
    }

    private List<ke.co.hoji.core.data.model.LiveField> getLiveFields(FieldParent fieldParent, MainRecord mainRecord) {
        List<LiveField> liveFields = findLiveFieldsForFieldParent(fieldParent, mainRecord);
        List<ke.co.hoji.core.data.model.LiveField> converted = new ArrayList<>();
        for (Field field : fieldParent.getChildren()) {
            LiveField match = ServerUtils.findOne(field, liveFields, new ServerUtils.LiveFieldSearchPredicate());
            if (match == null) {
                match = new LiveField();
            }
            ke.co.hoji.core.data.model.LiveField liveField = convertLiveField(field, match, mainRecord);
            if (field.isEnabled()) {
                converted.add(liveField);
            }
        }
        return converted;
    }

    private ke.co.hoji.core.data.model.LiveField convertLiveField(Field field, LiveField match, MainRecord mainRecord) {
        ke.co.hoji.core.data.model.LiveField converted =
            new DtoToModelLiveFieldConverter(hojiContext, field, mainRecord).convert(match);
        if (field.hasScriptFunction(Field.ScriptFunctions.CALCULATE)) {
            field.setResponse(converted.getResponse());
            CalculationResult result = calculator.calculate(mainRecord, mainRecord.getUuid(), field);
            Response response = converted.getResponse();
            if (result.isEvaluated() && result.getValue() != null) {
                response = Response.create(hojiContext, converted, result.getValue(), true);
            }
            converted.setResponse(response);
        }
        return converted;
    }

    private List<LiveField> findLiveFieldsForFieldParent(FieldParent fieldParent, MainRecord mainRecord) {
        List<LiveField> liveFields = new ArrayList<>();
        if (fieldParent instanceof Form) {
            liveFields.addAll(mainRecord.getLiveFields());
        } else {
            liveFields.addAll(mainRecord.getMatrixRecords().get(0).getLiveFields());
        }
        return liveFields;
    }

}
