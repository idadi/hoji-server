package ke.co.hoji.server.repository;

import ke.co.hoji.core.data.dto.MainRecord;
import org.springframework.data.repository.Repository;

/**
 * Created by gitahi on 5/14/17.
 */
public interface MainRecordRepository extends Repository<MainRecord, String>, MainRecordRepositoryCustom {
}
