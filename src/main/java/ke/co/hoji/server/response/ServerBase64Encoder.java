package ke.co.hoji.server.response;

import ke.co.hoji.core.response.helper.Base64Encoder;

import java.util.Base64;

public class ServerBase64Encoder implements Base64Encoder {

    @Override
    public String encodeToBase64String(byte[] byteArray) {
        return Base64.getEncoder().encodeToString(byteArray);
    }

    @Override
    public byte[] decodeToByteArray(String base64String) {
        return Base64.getDecoder().decode(base64String);
    }
}
