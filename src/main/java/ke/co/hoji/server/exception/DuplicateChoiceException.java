package ke.co.hoji.server.exception;


public class DuplicateChoiceException extends RuntimeException {

    private static final long serialVersionUID = 7622486335447106189L;

    public DuplicateChoiceException(String message) {
        super(message);
    }

    public DuplicateChoiceException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
