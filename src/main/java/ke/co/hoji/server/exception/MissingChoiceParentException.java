package ke.co.hoji.server.exception;

import ke.co.hoji.core.data.Choice;

import java.util.List;

/**
 * Created by geoffreywasilwa on 21/04/2017.
 */
public class MissingChoiceParentException extends RuntimeException {
    private Choice choice;
    private String parentName;

    public MissingChoiceParentException(Choice choice, String parentName, String message) {
        super(message);
        this.choice = choice;
        this.parentName = parentName;
    }

    public Choice getChoice() {
        return choice;
    }

    public String getParentName() {
        return parentName;
    }
}
