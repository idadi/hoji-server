package ke.co.hoji.server.exception;

/**
 * Created by geoffreywasilwa on 25/04/2017.
 */
public class CsvFormatException extends RuntimeException {

    public CsvFormatException(String message) {
        super(message);
    }

    public CsvFormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
