package ke.co.hoji.server.exception;


public class ChoiceGroupIntegrityViolationException extends RuntimeException {

    private static final long serialVersionUID = 7622486335447106189L;

    public ChoiceGroupIntegrityViolationException(String message) {
        super(message);
    }

    public ChoiceGroupIntegrityViolationException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
