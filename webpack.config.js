const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
    entry: {
        choices: path.resolve(__dirname, 'app', 'choices.page.js'),
        dashboard: path.resolve(__dirname, 'app', 'dashboard.page.js'),
        report: path.resolve(__dirname, 'app', 'report.page.js'),
    },
    output: {
        path: path.resolve(__dirname, 'src', 'main', 'webapp', 'resources', 'js'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.jsx$|\.js$/,
                exclude: /(disposables)/,
                use: {
                    loader: 'babel-loader',
                    options: { 
                        presets: ['@babel/react', '@babel/env'],
                        plugins: [
                            ["@babel/plugin-proposal-class-properties", { "loose": true }]
                        ] 
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: '../css'
                        }
                    },
                    'css-loader',
                    'postcss-loader'
                ]
            },
            {
                test: /\.(jpg|png|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: "images/[hash].[ext]"
                        }
                    }
                ]
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [

                    {
                        loader: 'url-loader',
                        options: {
                            limit: 80000,
                            mimetype: "application/font-woff"
                        }
                    }
                ]
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader"
            }
        ]
    },
    optimization: {
        minimizer: [new TerserPlugin(), new OptimizeCssAssetsPlugin({cssProcessor: require("cssnano")})],
        splitChunks: {
            cacheGroups: {
                vendor: {
                    chunks: "initial",
                    test: path.resolve(__dirname, "node_modules"),
                    name: "vendor",
                    enforce: true,
                },
            }
        }
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '../css/[name].css'
        }),
    ],
    resolve: {
        extensions: [ '.js' ]
    }
};
