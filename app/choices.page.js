import React, { Component } from "react";
import PropTypes from "prop-types";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { CONTEXT_PATH } from "./constants";
import ChoicesListContainer from "./container/ChoicesListContainer.jsx";
import ChoicesDetailContainer from "./container/ChoicesDetailContainer.jsx";

import './lib/hoji.css';

class ChoiceGroupPage extends Component {

    render() {
        return (
            <Router>
                <Route exact path={`${contextPath}/choice-group`} render={props =>
                    <ChoicesListContainer contextPath={this.props.contextPath} {...props} />
                } />
                <Route exact path={`${contextPath}/choice-group/:id`} render={props =>
                    <ChoicesDetailContainer {...props} />
                } />
            </Router>
        );
    }
}

ChoiceGroupPage.propTypes = {
    contextPath: PropTypes.string.isRequired,
}

const contextPath = CONTEXT_PATH;

ReactDOM.render(
    <ChoiceGroupPage contextPath={contextPath} />,
    document.getElementById("choice-group-container")
);