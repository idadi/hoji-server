import React, {Component} from "react";
import PropTypes from "prop-types";
import {List} from "immutable";
import {Button, Modal} from "semantic-ui-react";
import {fetch} from "../network.utils";
import {API_BASE_URL} from "../constants";

class ChoiceRemoverModal extends Component {

    constructor(props) {
        super(props);
        this.handleRemove = this.handleRemove.bind(this);
    }

    handleRemove() {
        const {choice, choiceGroup} = this.props;
        fetch({
            url: `${API_BASE_URL}/groups/${choiceGroup.id}/choices/${choice.id}`,
            method: "delete",
            headers: {"Content-Type": "application/json"},
            onSuccess: () => this.props.onRemove(),
        });
    }

    render() {
        const {choice, choiceGroup, closeModal, isOpen} = this.props;
        return (
            <Modal size="tiny" closeOnDimmerClick={false} closeOnEscape={true} onClose={closeModal} open={isOpen}>
                <Modal.Header className="orange">Remove choice</Modal.Header>
                <Modal.Content>
                    <p>
                        <span>Are you sure you want to remove the choice</span>{" "}
                        <strong>{choice && choice.name}</strong> from{" "}
                        <strong>{choiceGroup && choiceGroup.name}</strong> group?
                    </p>
                </Modal.Content>
                <Modal.Actions>
                    <Button className="delete-submit" color="red" onClick={this.handleRemove}>Delete choice</Button>
                    <Button basic onClick={closeModal}>Cancel</Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

ChoiceRemoverModal.propTypes = {
    choice: PropTypes.object.isRequired,
    choiceGroup: PropTypes.object.isRequired,
    closeModal: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onRemove: PropTypes.func.isRequired,
};

export default ChoiceRemoverModal;