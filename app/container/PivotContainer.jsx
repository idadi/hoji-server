import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import _function from 'lodash/function';
import _object from 'lodash/object';
import { List } from 'immutable';
import striptags from 'striptags';
import { Button, Container, Form, Grid, Header, Message, Modal, Segment, Table, Transition } from 'semantic-ui-react';
import { fetch, setupSocket, closeSocket } from '../network.utils';
import { getFilters, saveAutoloadSetting, getAutoloadSetting } from '../store.util';

import DownloadModal from './DownloadModal.jsx';
import FilterFormContainer from './FilterFormContainer.jsx';

import FlexContainer, { pivotJsonProcessor } from '../components/Flexmonster.jsx';
import Notification from '../components/NewDataNotification.jsx';
import ReportActions from '../components/ReportActions.jsx';
import ReportSelector from '../components/ReportSelector.jsx';
import SecondaryNav from '../components/SecondaryNav.jsx';

class PivotContainer extends Component {

    constructor(props) {
        super(props);
        const autoload = getAutoloadSetting();
        this.state = {
            autoload,
            componentName: '',
            errors: {},
            flexData: List(),
            isLoading: true,
            isLoadingJsonFile: false,
            isMessageVisible: false,
            isDownloadOpen: false,
            isFilterOpen: false,
            isSaveOpen: false,
            showFilter: false,
            showNotification: false,
            transposed: false,
        };

        this.handleAutoloadChange = this.handleAutoloadChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleDismiss = this.handleDismiss.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.handleFlexmonsterLoaded = this.handleFlexmonsterLoaded.bind(this);
        this.handleJsonDataLoading = this.handleJsonDataLoading.bind(this);
        this.handleJsonDataLoaded = this.handleJsonDataLoaded.bind(this);
        this.handleReload = this.handleReload.bind(this);
        this.handleSaveClick = this.handleSaveClick.bind(this);
        this.customizeToolbar = this.customizeToolbar.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.initSocket = this.initSocket.bind(this);
        this.saveDashboardComponent = this.saveDashboardComponent.bind(this);
    }

    componentDidMount() {
        this.fetchData();
        this.initSocket();
    }

    componentWillUnmount() {
        closeSocket(this.stompClient);
    }

    handleAutoloadChange(autoload) {
        saveAutoloadSetting(autoload)
        this.setState({ autoload });
    }

    handleChange(event, data) {
        this.setState({ [data.name]: data.value });
    }

    handleDismiss() {
        this.setState({ isMessageVisible: false, message: '' });
    }

    handleFilter(updated) {
        this.setState({ isFilterOpen: false });
        if (updated) {
            this.fetchData();
        }
    }

    handleFlexmonsterLoaded(flexmonster) {
        this.flexmonster = flexmonster;
    }

    handleJsonDataLoading() {
        this.setState({ isLoadingJsonFile: true });
    }

    handleJsonDataLoaded() {
        if (this.state.isLoadingJsonFile) {
            this.fetchData();
        }
    }

    handleReload() {
        this.fetchData();
        this.setState({ showNotification: false });
    }

    handleSaveClick() {
        this.setState({ isSaveOpen: true });
    }

    fetchData() {
        this.setState({ isLoading: true });
        const filters = getFilters();
        filters.id = this.props.resource.id;
        const url = `${this.props.apiBaseUrl}/pivot?${toQueryString(filters)}`;
        const jsonProcessor = _function.partialRight(
            pivotJsonProcessor,
            (flexData, defaultColumn, transposed) => {
                this.setState({ flexData, defaultColumn, transposed, isLoading: false, isLoadingJsonFile: false });
            });
        fetch({ url, onSuccess: jsonProcessor });
    }

    initSocket() {
        this.stompClient = setupSocket(`/topic/${this.props.resourceType}/${this.props.resource.id}/report/pivot`, () => {
            if (this.state.autoload) {
                this.fetchData();
            } else {
                this.setState({ showNotification: true });
            }
        });
    }

    customizeToolbar(toolbar) {
        const tabs = toolbar.getTabs().filter(tab => !['fm-tab-connect', 'fm-tab-open'].includes(tab.id));
        if (this.props.resourceType === 'form') {
            toolbar.getTabs = () => {
                tabs.find(t => t.id === 'fm-tab-save').handler = this.handleSaveClick;
                return tabs;
            }
        }
    }

    saveDashboardComponent() {
        const { contextPath, resource } = this.props;
        const postUrl = `${contextPath}/api/project/${resource.survey.id}/form/${resource.id}/dashboard/component`;
        const componentName = this.state.componentName;
        if (!componentName) {
            this.setState({ errors: { componentName: 'Required' } });
        } else {
            const reportConfig = _object.omit(this.flexmonster.getReport(), 'dataSource.data');
            _object.merge(reportConfig, {
                options: {
                    chart: { showFilter: this.state.showFilter }
                }
            });
            const data = JSON.stringify(Object.assign(
                {
                    name: componentName,
                    configuration: reportConfig,
                    form: resource.id
                })
            );
            const onSuccess = () => {
                const message = (
                    <span>
                        <strong>{componentName}</strong>
                        {' has been saved to the dashboard. Click '}
                        <Link to={`./dashboard`}>here</Link>
                        {' to open the dashboard.'}
                    </span>
                );
                this.setState({ componentName: '', showFilter: false, isMessageVisible: true, isSaveOpen: false, message });
            };
            fetch({ method: 'post', url: postUrl, data, onSuccess, headers: { 'Content-Type': 'application/json' } });
        }
    }

    render() {
        const { apiBaseUrl, breadcrumbSections, resource, resourceType, users } = this.props;
        const { autoload, componentName, defaultColumn, errors, flexData, isLoading, isLoadingJsonFile, isMessageVisible, isDownloadOpen, isFilterOpen, isSaveOpen, message, showFilter, showNotification, } = this.state;
        const placeholder = (
            <Grid.Row>
                <Grid.Column>
                    <Segment placeholder>
                        <Header icon>
                            {'No data has been submited for this form.'}
                        </Header>
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        );
        const content = (
            <Grid.Row>
                <Grid.Column>
                    <Segment attached>
                        <FlexContainer
                            flexData={flexData}
                            defaultColumn={defaultColumn}
                            isLoading={isLoading}
                            isLoadingJsonFile={isLoadingJsonFile}
                            onFlexmonsterLoad={this.handleFlexmonsterLoaded}
                            onJsonDataLoading={this.handleJsonDataLoading}
                            onJsonDataLoaded={this.handleJsonDataLoaded}
                            toolbarCustomizer={this.customizeToolbar}
                        />
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        );
        const header = resourceType === "form" ? resource.name : striptags(resource.description);
        const messageStyle = { position: 'fixed', width: '60%', bottom: '1rem', left: 0, right: 0, marginLeft: 'auto', marginRight: 'auto', zIndex: 2 };
        return (
            <React.Fragment>
                <SecondaryNav sections={[...breadcrumbSections, { key: "pivot", content: "Pivot table", active: true },]}/>
                <Container style={{ marginTop: '3rem' }}>
                    <Transition visible={isMessageVisible} animation='fly up'>
                        <Message style={messageStyle} color="green" onDismiss={this.handleDismiss} content={message} />
                    </Transition>
                    <Grid style={{ marginTop: '1rem' }}>
                        <Grid.Row>
                            <Grid.Column width={10}>
                                <Table basic="very" fixed singleLine>
                                    <Table.Body>
                                        <Table.Row>
                                            <Table.Cell className="compact">
                                                <Header as="h2">{header}</Header>
                                            </Table.Cell>
                                        </Table.Row>
                                    </Table.Body>
                                </Table>
                            </Grid.Column>
                            <Grid.Column width={6}>
                                <ReportActions autoload={autoload}
                                        onAutoloadChange={() => this.handleAutoloadChange(!autoload)}
                                        onDownload={() => this.setState({ isDownloadOpen: true })}
                                        onFilter={() => this.setState({ isFilterOpen: true })} />
                                <ReportSelector active="pivot" reportTypes={reportTypes} />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <Grid>
                        {!(isLoading || isLoadingJsonFile) && flexData.isEmpty() ? placeholder : content}
                    </Grid>
                    <Modal closeOnDimmerClick={false} closeOnEscape={true} open={isSaveOpen} onClose={() => this.setState({ isSaveOpen: false })} size="tiny">
                        <Modal.Header className="ui orange">Save to dashboard</Modal.Header>
                        <Modal.Content>
                            <Form>
                                <Form.Input label='Name' name='componentName' value={componentName} error={errors.componentName && { content: errors.componentName, pointing: 'below', }} onChange={this.handleChange} />
                                <Form.Checkbox label='Show filter' name='showFilter' defaultChecked={showFilter} onChange={this.handleChange} />
                            </Form>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button color="orange" onClick={this.saveDashboardComponent}>Save to dashboard</Button>
                            <Button basic onClick={() => this.setState({ isSaveOpen: false })}>Cancel</Button>
                        </Modal.Actions>
                    </Modal>
                    <DownloadModal apiBaseUrl={apiBaseUrl} isOpen={isDownloadOpen} onClose={() => this.setState({ isDownloadOpen: false })}/>
                    <FilterFormContainer isOpen={isFilterOpen} onCloseModal={this.handleFilter} fields={fields} users={users} />
                    <Notification show={showNotification}
                        onYesClicked={this.handleReload}
                        onNoClicked={() => this.setState({ showNotification: false })} />
                </Container>
            </React.Fragment>
        )
    }
}

PivotContainer.propTypes = {
    apiBaseUrl: PropTypes.string.isRequired,
    breadcrumbSections: PropTypes.array.isRequired,
    contextPath: PropTypes.string.isRequired,
    fields: PropTypes.array.isRequired,
    reportTypes: PropTypes.arrayOf(PropTypes.shape({ text: PropTypes.string.isRequired, path: PropTypes.string.isRequired })).isRequired,
    resource: PropTypes.object.isRequired,
    resourceType: PropTypes.string.isRequired,
    users: PropTypes.array.isRequired,
};

export default PivotContainer;