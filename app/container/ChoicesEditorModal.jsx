import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button, Dropdown, Form, Label, Modal, Input } from "semantic-ui-react";
import { fetch } from "../network.utils";
import _lang from "lodash/lang";
import { API_BASE_URL } from "../constants";

class ChoicesEditorModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            choices: [],
            choiceGroup: this.props.choiceGroup,
            choiceMap: {},
            errors: {},
            selectedChoices: [],
        };
    }
    
    componentDidUpdate(prevProps) {
        if (prevProps.choiceGroup !== this.props.choiceGroup) {
            const choiceGroup = Object.assign({ excludeLast: 0 }, {...this.props.choiceGroup});
            this.setState({ choiceGroup });
        }
    }

    handleChange = (e, data) => {
        if (data.name === "choices") {
            const choiceGroup = Object.assign({}, this.state.choiceGroup, { choices: [] });
            const selected = [];
            data.value.forEach(value => {
                if (this.state.choiceMap[value]) {
                    choiceGroup.choices.push({ id: value });
                    selected.push({id: value, name: this.state.choiceMap[value]});
                } else {
                    choiceGroup.choices.push({ name: value });
                    // so that the new choice get's displayed as selected option in control
                    selected.push({ id: value, name: value });
                }
            });
            const selectedChoices = [...this.state.selectedChoices, ...selected];
            this.setState({ choiceGroup, selectedChoices })
        } else {
            const choiceGroup = Object.assign({}, this.state.choiceGroup);
            choiceGroup[data.name] = data.value;
            this.setState({ choiceGroup });
        }
    }

    handleClose = () => {
        this.resetState();
        this.props.closeModal();
    }

    handleEdit = () => {
        const choiceGroup = Object.assign({}, this.state.choiceGroup);
        const errors = {};
        if (!choiceGroup.name) {
            errors.name = "required";
        }
        if (!_lang.isEmpty(errors)) {
            this.setState({ errors });
            return;
        }
        fetch({
            url: `/api/groups/${choiceGroup.id}`,
            data: JSON.stringify(choiceGroup),
            method: "post",
            headers: { "Content-Type": "application/json" },
            onSuccess: () => this.props.onEdit(),
        });
        this.resetState();
    }

    searchChoices = (e, { searchQuery }) => {
        const onSuccess = pagedChoices => {
            const choiceMap = Object.assign({}, this.state.choiceMap);
            pagedChoices.content.forEach(choice => choiceMap[choice.id] = choice.name);
            this.setState({ choices: pagedChoices.content, choiceMap });
        }
        fetch({ url: `${API_BASE_URL}/choices?query=${searchQuery}`, onSuccess });
    }

    resetState = () => {
        this.setState({
            choices: [],
            choiceGroup: {},
            choiceMap: {},
            errors: {},
            selectedChoices: [],
        });
    }

    render() {
        const { isOpen } = this.props;
        const { choiceGroup, choices, errors, selectedChoices } = this.state;
        const hasErrors = Object.entries(errors).length > 0;
        const orderingOptions = [
            { key: 0, value: 0, text: "Order as entered" },
            { key: 1, value: 1, text: "Order randomly" },
            { key: 2, value: 2, text: "Order alphabetically" },
        ];
        const choiceOptions = [...choices, ...selectedChoices].map(choice => (
            { key: choice.id, value: choice.id, text: choice.name }
        ));
        const newChoiceLabel = <Label color='orange' horizontal>{'New'}</Label>;
        return (
            <Modal closeOnDimmerClick={false} closeOnEscape={true} open={isOpen} onClose={this.handleClose}>
                <Modal.Header className="ui orange">{`${choiceGroup.id ? 'Edit' : 'Add'} choices`}</Modal.Header>
                <Modal.Content>
                    <div className="ui visible positive message">
                        <strong>TIP: </strong>
                        Choices provide options that can be be selected as responses to fields.
                        <a href="https://www.hoji.co.ke/support/#reamaze#0#/kb/choices/what-is-a-choice"
                           target="_blank">
                            {' More on choices.'}
                        </a>
                    </div>
                    <Form onSubmit={this.handleEdit}>
                        <div className="field">
                            <label>Choices</label>
                            <Dropdown fluid name="choices" allowAdditions additionLabel={newChoiceLabel} search selection multiple options={choiceOptions} onChange={this.handleChange} onSearchChange={this.searchChoices}  />
                            <span className="optional label">
                                {`Enter all choices you would like listed together e.g. "Male" then "Female". Hit Enter at the end of each one.`}
                                <a href="https://www.hoji.co.ke/support/#reamaze#0#/kb/choices/what-is-a-choice"
                                    target="_blank">
                                        {' More on choices.'}
                                </a>
                            </span>
                        </div>
                        <div className="field">
                            <label>Choice order</label>
                            <Dropdown fluid name="orderingStrategy" selection options={orderingOptions} defaultValue={choiceGroup.orderingStrategy || 0} onChange={this.handleChange} />
                            <span className="optional label">
                                {`Choose how you would like your choices to be ordered when presented to the user.`}
                                <a href="https://www.hoji.co.ke/support/#reamaze#0#/kb/choices/how-to-configure-choice-ordering"
                                   target="_blank">
                                        {' More on choice ordering.'}
                                </a>
                            </span>
                        </div>
                        <div className="field">
                            <label>Anchor the last n choices</label>
                            <Input name="excludeLast" value={choiceGroup.excludeLast || ""} type="number" onChange={this.handleChange} minimum={0} />
                            <span className="optional label">
                                {`Specify the number of choices to anchor at the bottom of the list regardless of the selected order.`}
                                <a href="https://www.hoji.co.ke/support/#reamaze#0#/kb/choices/how-to-anchor-choices"
                                   target="_blank">
                                        {' More on choice anchoring.'}
                                </a>
                            </span>
                        </div>
                        <div className={`${hasErrors?'error':''} field`}>
                            <label>List name</label>
                            {hasErrors && <Label prompt pointing="below">{errors.name}</Label>}
                            <Input fluid name="name" value={choiceGroup.name || ""} onChange={this.handleChange} />
                            <span className="optional label">
                                {`Give your list of choices a common name e.g. if it contains "Male" and "Female" you can call it "Sex".`}
                                <a href="https://www.hoji.co.ke/support/#reamaze#0#/kb/choices/what-is-a-choice-list"
                                   target="_blank">
                                        {' More on choice lists.'}
                                </a>
                            </span>
                        </div>
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button className="edit-submit" color="orange" onClick={this.handleEdit}>{`${choiceGroup.id ? 'Update' : 'Add'} choices`}</Button>
                    <Button basic onClick={this.handleClose}>Cancel</Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

ChoicesEditorModal.propTypes = {
    choiceGroup: PropTypes.object,
    closeModal: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onEdit: PropTypes.func.isRequired,
};

export default ChoicesEditorModal;