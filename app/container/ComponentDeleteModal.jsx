import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Modal } from 'semantic-ui-react';
import _function from 'lodash/function';
import { fetch } from "../network.utils";

class ComponentDeleteModal extends Component {
    constructor(props) {
        super(props);

        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete() {
        const { component, form, onDelete } = this.props;
        const onSuccess = () => {
            onDelete(component.id);
        };
        fetch({
            url: `${contextPath}/api/project/${form.survey.id}/form/${form.id}/dashboard/component/${component.id}`,
            onSuccess,
            method: "delete"
        });
    }

    render() {
        const { component, isOpen, onClose } = this.props;
        return (
            <Modal closeOnDimmerClick={false} closeOnEscape onClose={onClose} open={isOpen}>
                <Modal.Header className="orange">Delete visualization</Modal.Header>
                <Modal.Content>
                    <p>
                        {'Are you sure you want to delete the visualization: '}
                        <strong>{component && component.name}</strong>
                    </p>
                </Modal.Content>
                <Modal.Actions>
                    <Button color="red" onClick={this.handleDelete}>Delete visualization</Button>
                    <Button basic onClick={onClose}>Cancel</Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

ComponentDeleteModal.propTypes = {
    component: PropTypes.object,
    contextPath: PropTypes.string.isRequired,
    form: PropTypes.object.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onDelete: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
};

export default ComponentDeleteModal;