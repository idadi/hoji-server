import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import ImageGallery from 'react-image-gallery';
import striptags from 'striptags';
import { Container, Grid, Header, Loader, Segment, Table } from "semantic-ui-react";
import { fetch, setupSocket, closeSocket } from "../network.utils";
import { getAutoloadSetting, getFilters, saveAutoloadSetting } from '../store.util';

import DownloadModal from './DownloadModal.jsx';
import FilterFormContainer from './FilterFormContainer.jsx';

import Notification from '../components/NewDataNotification.jsx';
import ReportActions from '../components/ReportActions.jsx';
import ReportSelector from '../components/ReportSelector.jsx';
import SecondaryNav from '../components/SecondaryNav.jsx';

class GalleryContainer extends Component {

    constructor(props) {
        super(props);
        const autoload = getAutoloadSetting();
        this.state = {
            autoload,
            images: List(),
            isLoading: false,
            isDownloadOpen: false,
            isFilterOpen: false,
            showNotification: false,
        };

        this.handleAutoloadChange = this.handleAutoloadChange.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.handleReload = this.handleReload.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.initSocket = this.initSocket.bind(this);
    }

    componentDidMount() {
        this.fetchData();
        this.initSocket();
    }

    componentWillUnmount() {
        closeSocket(this.stompClient);
    }

    handleAutoloadChange(autoload) {
        saveAutoloadSetting(autoload)
        this.setState({ autoload });
    }

    handleFilter(updated) {
        this.setState({ isFilterOpen: false });
        if (updated) {
            this.fetchData();
        }
    }

    handleReload() {
        this.fetchData();
        this.setState({ showNotification: false });
    }

    fetchData() {
        this.setState({ isLoading: true });
        const filters = getFilters();
        filters.id = this.props.resource.id;
        const url = `${this.props.apiBaseUrl}/gallery?${toQueryString(filters)}`;
        const jsonProcessor = response => {
            const images = List(response);
            this.setState({ images, isLoading: false });
        }
        fetch({ url, onSuccess: jsonProcessor });
    }

    initSocket() {
        this.stompClient = setupSocket(`/topic/${this.props.resourceType}/${this.props.resource.id}/report/gallery`, () => {
            if (this.state.autoload) {
                this.fetchData();
            } else {
                this.setState({ showNotification: true });
            }
        });
    }

    render() {
        const { apiBaseUrl, breadcrumbSections, resource, resourceType, users } = this.props;
        const { autoload, images, isLoading, isDownloadOpen, isFilterOpen, showNotification } = this.state;
        const placeholder = (
            <Grid.Row>
                <Grid.Column>
                    <Segment placeholder>
                        <Header icon>
                            {'No images captured for this form.'}
                        </Header>
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        );
        const template = {};
        images.forEach(imageData => {
            const label = imageData.label.trim().replace(/[()'"]/g, "");
            if (template.hasOwnProperty(label)) {
                template[label].push(imageData);
            } else {
                template[label] = [imageData];
            }
        });
        const gallery = (
            <Grid.Row>
                {
                    Object.keys(template).map((label, index) => {
                        const photos = template[label].map(imageData => {
                            return (
                                {
                                    original: `${imageData.src}`,
                                    originalAlt: `${imageData.caption || ''}`,
                                    description: `${imageData.caption || ''}`,
                                }
                            );
                        });
                        return (
                            <Grid.Column key={`${index}`}>
                                <Header attached="top" as="h5">{label}</Header>
                                <Segment attached>
                                    <ImageGallery
                                        additionalClass={'ui image'}
                                        items={photos}
                                        showThumbnails={false}
                                        showPlayButton={false}
                                        defaultImage="/resources/img/image-404.png"
                                        lazyLoad
                                    />
                                </Segment>
                            </Grid.Column>
                        );
                    })
                }
            </Grid.Row>
        );
        const header = resourceType === "form" ? resource.name : striptags(resource.description);
        return (
            <React.Fragment>
                <SecondaryNav sections={[...breadcrumbSections, { key: "gallery", content: "Image gallery", active: true },]}/>
                <Container style={{ marginTop: "3rem" }}>
                    <Grid style={{ marginTop: '1rem' }}>
                        <Grid.Row>
                            <Grid.Column width={10}>
                                <Table basic="very" fixed singleLine>
                                    <Table.Body>
                                        <Table.Row>
                                            <Table.Cell className="compact">
                                                <Header as="h2">{header}</Header>
                                            </Table.Cell>
                                        </Table.Row>
                                    </Table.Body>
                                </Table>
                            </Grid.Column>
                            <Grid.Column width={6}>
                                <ReportActions autoload={autoload}
                                        onAutoloadChange={() => this.handleAutoloadChange(!autoload)}
                                        onDownload={() => this.setState({ isDownloadOpen: true })}
                                        onFilter={() => this.setState({ isFilterOpen: true })} />
                                <ReportSelector active="gallery" reportTypes={reportTypes} />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <Grid>
                        <Grid.Column>
                            <Loader active={isLoading} />
                            {!isLoading && images.isEmpty() ? placeholder : gallery}
                        </Grid.Column>
                    </Grid>
                    <DownloadModal apiBaseUrl={apiBaseUrl} isOpen={isDownloadOpen} onClose={() => this.setState({ isDownloadOpen: false })}/>
                    <FilterFormContainer isOpen={isFilterOpen} onCloseModal={this.handleFilter} fields={fields} users={users} />
                    <Notification show={showNotification}
                        onYesClicked={this.handleReload}
                        onNoClicked={() => this.setState({ showNotification: false })} />
                </Container>
            </React.Fragment>
        );
    }
}

GalleryContainer.propTypes = {
    apiBaseUrl: PropTypes.string.isRequired,
    breadcrumbSections: PropTypes.array.isRequired,
    fields: PropTypes.array.isRequired,
    reportTypes: PropTypes.arrayOf(PropTypes.shape({ text: PropTypes.string.isRequired, path: PropTypes.string.isRequired })).isRequired,
    resource: PropTypes.object.isRequired,
    resourceType: PropTypes.string.isRequired,
    users: PropTypes.array.isRequired,
};

export default GalleryContainer;