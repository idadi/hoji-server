import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Modal } from 'semantic-ui-react';
import { getFilters } from '../store.util';

class DownloadModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            downloadProperties: { }
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleDownload = this.handleDownload.bind(this);
    }

    handleChange(event, data) {
        const { downloadProperties } = this.state;
        downloadProperties[data.name] = data.value;
        this.setState({ downloadProperties });
    }

    handleDownload() {
        const { apiBaseUrl, onClose } = this.props;
        onClose();
        var downloadWindow = window.open(`${apiBaseUrl}/download?${toQueryString(this.state.downloadProperties)}&${toQueryString(getFilters())}`, '_blank');
        downloadWindow.focus();
    }

    render() {
        const { isOpen, onClose } = this.props;
        return (
            <Modal closeOnDimmerClick={false} open={isOpen} onClose={onClose} size="tiny">
                <Modal.Header className="ui orange">Download data</Modal.Header>
                <Modal.Content>
                    <Form>
                        <Form.Checkbox name="cleanDownloadHeaders" label="Clean headers" value="true" onChange={this.handleChange} />
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button color="orange" onClick={this.handleDownload}>Download data</Button>
                    <Button basic onClick={onClose}>Cancel</Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

DownloadModal.propTypes = {
    apiBaseUrl: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
};

export default DownloadModal;