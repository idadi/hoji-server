import React, { Component } from 'react';
import PropTypes from 'prop-types';
import HTML5Backend from 'react-dnd-html5-backend';
import { DndProvider } from 'react-dnd';
import { Button, Container, Grid, Header, Icon, Loader, Modal, Segment, Table } from 'semantic-ui-react';
import { List } from 'immutable';
import _function from 'lodash/function';
import { fetch } from "../network.utils";
import { pivotJsonProcessor } from "../components/Flexmonster.jsx";
import DraggableRow from '../components/DraggableRow.jsx'
import ComponentEditorModal from './ComponentEditorModal.jsx';

class DashboardComponentsContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            components: List(),
            defaultColumn: '',
            errors: {},
            flexData: List(),
            isDeleteModalOpen: false,
            isEditModalOpen: false,
            isLoadingComponents: false,
            isLoadingData: false,
            selectedComponent: null,
        };

        this.fetchData = this.fetchData.bind(this);
        this.handleCloseDeleteModal = this.handleCloseDeleteModal.bind(this);
        this.handleCloseEditModal = this.handleCloseEditModal.bind(this);
        this.handleDuplicate = this.handleDuplicate.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleOpenDeleteModal = this.handleOpenDeleteModal.bind(this);
        this.handleOpenEditModal = this.handleOpenEditModal.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.sortComponent = this.sortComponent.bind(this);
        this.sortingDone = this.sortingDone.bind(this);
    }

    componentDidMount() {
        this.fetchData();
    }

    handleDuplicate(componentId) {
        const onSuccess = component => {
            this.handleUpdate(component);
        }
        fetch({
            url: `${this.props.apiBaseUrl}/form/${form.id}/dashboard/component/${componentId}/duplicate`,
            onSuccess,
            method: "post"
        });
    }

    handleDelete() {
        const { selectedComponent } = this.state;
        const onSuccess = () => {
            const index = this.state.components.findIndex(c => c.id === selectedComponent.id);
            if (index > -1) {
                const updatedComponents = this.state.components.delete(index);
                this.setState({ components: updatedComponents, isDeleteModalOpen: false, selectedComponent: null });
            }
        };
        fetch({
            url: `${this.props.apiBaseUrl}/form/${form.id}/dashboard/component/${this.state.selectedComponent.id}`,
            onSuccess,
            method: "delete"
        });
    }

    handleOpenDeleteModal(componentId) {
        const selectedComponent = this.state.components.find(c => c.id === componentId);
        this.setState({ selectedComponent, isDeleteModalOpen: true });
    }

    handleOpenEditModal(componentId) {
        const selectedComponent = this.state.components.find(c => c.id === componentId);
        this.setState({ selectedComponent, isEditModalOpen: true });
    }

    handleUpdate(component) {
        const index = this.state.components.findIndex(c => c.id === component.id);
        let updatedComponents;
        if (index > -1) {
            updatedComponents = this.state.components.update(index, () => component);
        } else {
            updatedComponents = this.state.components.push(component);
        }
        this.setState({ components: updatedComponents, isEditModalOpen: false, selectedComponent: null });
    }

    handleCloseDeleteModal() {
        this.setState({ selectedComponent: null, isDeleteModalOpen: false });
    }

    handleCloseEditModal() {
        this.setState({ selectedComponent: null, isEditModalOpen: false });
    }

    sortComponent(dragIndex, hoverIndex) {
        this.beforeSort = List(this.state.components);
        const { components } = this.state;
        const dragComponent = components.get(dragIndex);
        const sortedComponents = components.delete(dragIndex).insert(hoverIndex, dragComponent);

        this.setState({
            components: sortedComponents,
        });
    }

    sortingDone() {
        const params = toQueryString({ sortedComponentIds: this.state.components.toJS().map(c => c.id) });
        const onError = () => this.setState({ components: this.beforeSort });
        fetch({ url: `${this.props.apiBaseUrl}/form/${this.props.form.id}/dashboard/component/sort?${params}`, method: "post", onError })
    }

    fetchData() {
        const { apiBaseUrl, form, } = this.props;
        this.setState({ isLoadingComponents: true, isLoadingData: true, });
        const componentsUrl = `${apiBaseUrl}/form/${form.id}/dashboard/component`
        const dataUrl = `${apiBaseUrl}/form/${form.id}/report/pivot?testMode=${form.survey.testMode}`;
        const onComponentsFetched = components => this.setState({ components: List(components), isLoadingComponents: false })
        const jsonProcessor = _function.partialRight(pivotJsonProcessor,
            (flexData, defaultColumn) => this.setState({ flexData, defaultColumn, isLoadingData: false }));
        fetch({ url: componentsUrl, onSuccess: onComponentsFetched });
        fetch({ url: dataUrl, onSuccess: jsonProcessor });
    }

    render() {
        const { components, defaultColumn, flexData, isDeleteModalOpen, isEditModalOpen, isLoadingComponents, isLoadingData, selectedComponent } = this.state;
        const { apiBaseUrl, form } = this.props;
        const placeholder = (
            <Segment placeholder>
                <Header icon>
                    No visualizations defined for this form yet
                </Header>
                <Button color="orange" onClick={this.handleOpenEditModal}>Add visualization</Button>
            </Segment>
        );
        const header = (
            <Table.Row>
                <Table.HeaderCell className="sort-header" />
                <Table.HeaderCell>Name</Table.HeaderCell>
                <Table.HeaderCell>Actions</Table.HeaderCell>
            </Table.Row>
        );
        const body = components.map((component, index) => {
            const rowContent = (
                <React.Fragment>
                    <Table.Cell>{component.name}</Table.Cell>
                    <Table.Cell collapsing singleLine>
                        <Icon name="edit" color="black" link onClick={() => this.handleOpenEditModal(component.id)} />
                        <Icon name="copy" color="black" link onClick={() => this.handleDuplicate(component.id)} />
                        <Icon name="trash" color="black" link onClick={() => this.handleOpenDeleteModal(component.id)} />
                    </Table.Cell>
                </React.Fragment>
            )
            return (
                <DraggableRow key={component.id}
                    item={component}
                    content={rowContent}
                    index={index}
                    sortItem={this.sortComponent}
                    sortingDone={this.sortingDone} />
            );
        });
        const content = (
            <React.Fragment>
                <Grid style={{ marginTop: '1rem' }}>
                    <Grid.Column width="sixteen">
                        <Header as="h2" className="title">Visualizations</Header>
                        <Button color="orange" floated="right" onClick={this.handleOpenEditModal}>Add visualization</Button>
                    </Grid.Column>
                </Grid>
                <Table striped>
                    <Table.Header>{header}</Table.Header>
                    <Table.Body>{body}</Table.Body>
                </Table>
            </React.Fragment>
        );
        return (
            <DndProvider backend={HTML5Backend}>
                <Container>
                    <Loader active={isLoadingComponents} />
                    {components.isEmpty() && !isLoadingComponents ? placeholder : content}
                    <Modal closeOnDimmerClick={false} open={isDeleteModalOpen}>
                        <Modal.Header className="orange">Delete visualization</Modal.Header>
                        <Modal.Content>
                            <p>
                                {'Are you sure you want to delete the visualization: '}
                                <strong>{selectedComponent && selectedComponent.name}</strong>
                            </p>
                        </Modal.Content>
                        <Modal.Actions>
                            <Button color="red" onClick={this.handleDelete}>Delete visualization</Button>
                            <Button basic onClick={this.handleCloseDeleteModal}>Cancel</Button>
                        </Modal.Actions>
                    </Modal>
                    <ComponentEditorModal isOpen={isEditModalOpen} apiBaseUrl={apiBaseUrl} component={selectedComponent} defaultColumn={defaultColumn} flexData={flexData} formId={form.id} isLoading={isLoadingData} onSave={this.handleUpdate} onCloseModal={this.handleCloseEditModal} />
                </Container>
            </DndProvider>
        );
    }
}

DashboardComponentsContainer.propTypes = {
    apiBaseUrl: PropTypes.string.isRequired,
    form: PropTypes.object.isRequired,
};

export default DashboardComponentsContainer;