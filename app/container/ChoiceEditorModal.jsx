import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, Form, Message, Modal} from "semantic-ui-react";
import {fetch} from "../network.utils";
import _lang from "lodash/lang";
import { API_BASE_URL } from "../constants";

class ChoiceEditorModal extends Component {
    state = {
        anchor: {},
        anchors: [],
        choice: {},
        errors: {},
        firstAndLastAnchors: [{ key: 'first', ordinal: 1, name: 'First'}, { key: 'last', ordinal: this.props.lastOrdinal, name: 'Last' }],
        parent: {},
        parents: [],
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState.choice.id !== this.props.choice.id) {
            const choice = Object.assign({scale: 0}, this.props.choice);
            const parent = choice.parent;
            choice.parent = parent ? parent.id : null;
            this.setState({ choice, parent });
        }
        if (prevProps.lastOrdinal !== this.props.lastOrdinal) {
            this.setState({ firstAndLastAnchors: [{ key: 'first', ordinal: 1, name: 'First'}, { key: 'last', ordinal: this.props.lastOrdinal, name: 'Last' }] })
        }
    }

    handleChange = (e, data) => {
        const choice = this.state.choice;
        let value = data.value;
        if (data.type === "checkbox") {
            value = data.checked;
        }
        choice[data.name] = value;
        this.setState({choice});
    }

    handleClose = () => {
        this.resetState();
        this.props.closeModal();
    }

    handleSubmit = () => {
        const choice = this.state.choice;
        const errors = {};
        if (!choice.name) {
            errors.name = "name is required";
        }
        if (choice.scale !== 0 && !choice.scale) {
            errors.scale = "scale is required";
        }
        if (!_lang.isEmpty(errors)) {
            this.setState({errors});
            return;
        }
        if (choice.parent) {
            choice.parent = { id: choice.parent }
        }
        fetch({
            url: `/api/groups/${this.props.groupId}/choices`,
            data: JSON.stringify(choice),
            method: "post",
            headers: { "Content-Type": "application/json" },
            onSuccess: () => {
                this.props.onEdit();
                this.resetState();
            },
            onError: message => {
                this.setState({ errors: { global: message } });
            }
        });
    }

    searchAnchors = (e, { searchQuery }) => {
        const onSuccess = pagedChoices => {
            const anchors = pagedChoices.content.map(anchor => ({ ordinal: anchor.ordinal + 1, name: `After ${anchor.name}` }));
            this.setState({ anchors });
        };
        fetch({ url: `${API_BASE_URL}/groups/${this.props.groupId}/choices?query=${searchQuery}`, onSuccess });
    }

    searchParents = (e, { searchQuery }) => {
        const onSuccess = pagedChoices => this.setState({ parents: pagedChoices.content });
        fetch({ url: `${API_BASE_URL}/choices?query=${searchQuery}`, onSuccess });
    }

    resetState = () => {
        this.setState({
            anchor: {},
            anchors: [],
            choice: {},
            errors: {},
            parent: {},
            parents: [],
        });
    }

    render() {
        const { closeModal, anchor, isOpen, showPosition } = this.props;
        const { anchors, choice, errors, firstAndLastAnchors, parent, parents } = this.state;
        let anchorOptions;
        if (showPosition) {
            anchorOptions = [{key: anchor.ordinal, ordinal: anchor.ordinal && choice.ordinal,  name: `After ${anchor.name}`}, ...anchors, ...firstAndLastAnchors].filter(choice => choice && choice.ordinal).map(choice => (
                {key: choice.key || choice.ordinal, value: choice.ordinal, text: choice.name}
            ));
        }
        const parentOptions = [parent, ...parents].filter(choice => choice && choice.id).map(choice => (
            {key: choice.id, value: choice.id, text: choice.name}
        ));
        return (
            <Modal closeOnDimmerClick={false} onClose={closeModal} open={isOpen}>
                <Modal.Header className="ui orange">{choice.id ? "Edit choice" : "Add choice"}</Modal.Header>
                <Modal.Content>
                    <Form error={!!errors.global} onSubmit={this.handleSubmit}>
                        <Form.Input fluid name="name" label="Name" value={choice.name || ""} onChange={this.handleChange} error={errors.name && { content: errors.name, pointing: 'below' }} />
                        <Form.Input fluid name="code" label="Code" value={choice.code || ""} onChange={this.handleChange} error={errors.code && { content: errors.code, pointing: 'below'}} />
                        <Form.Input fluid name="description" label="Description" value={choice.description || ""} onChange={this.handleChange} />
                        <Form.Input fluid name="scale" label="Scale" value={choice.scale || "0"} onChange={this.handleChange} type="number" />
                        <Form.Dropdown clearable fluid name="parent" label="Parent" placeholder="Search for parent choice" search selection options={parentOptions} value={choice.parent} onChange={this.handleChange} onSearchChange={this.searchParents} />
                        {showPosition &&
                            <Form.Dropdown fluid name="ordinal" label="Position" placeholder="Select choice that should appear before this one" search selection options={anchorOptions} value={choice.ordinal} onChange={this.handleChange} onSearchChange={this.searchAnchors} />}
                        <Form.Checkbox name="loner" label="Loner" value="1" checked={choice.loner} onChange={this.handleChange} />
                        <Message error content={errors.global} />
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button className="edit-submit" color="orange" onClick={this.handleSubmit}>{`${choice.id?'Update':'Add'} choice`}</Button>
                    <Button basic onClick={this.handleClose}>Cancel</Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

ChoiceEditorModal.propTypes = {
    anchor: PropTypes.object,
    choice: PropTypes.object,
    closeModal: PropTypes.func.isRequired,
    groupId: PropTypes.number.isRequired,
    lastOrdinal: PropTypes.number.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onEdit: PropTypes.func.isRequired,
    showPosition: PropTypes.bool,
};

export default ChoiceEditorModal;