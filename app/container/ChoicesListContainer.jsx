import React, { Component } from "react";
import PropTypes from "prop-types";
import { List } from "immutable";
import { Button, Container, Dimmer, Grid, Header, Icon, Loader, Message, Segment, Table, Transition } from "semantic-ui-react";
import InfiniteScroll from "react-infinite-scroller";
import ChoicesEditorModal from "./ChoicesEditorModal.jsx";
import ChoiceGroupDeleteModal from "./ChoiceGroupDeleteModal.jsx";
import { fetch } from "../network.utils";

export const getOrderingStrategy = code => {
    switch (`${code}`) {
        case "0":
            return "Order as entered";
        case "1":
            return "Order randomly";
        default:
            return "Order alphabetically";
    }
}

const PAGE_SIZE = 100;

class ChoicesListContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 1,
            pages: 0,
            currentChoiceGroup: null,
            choiceGroups: List(),
            deleteOpen: false,
            editOpen: false,
            uploadError: null,
            isLoading: true,
        };
        this.hasMoreChoiceGroups = this.hasMoreChoiceGroups.bind(this);
        this.loadMoreChoiceGroups = this.loadMoreChoiceGroups.bind(this);
        this.handleOpenAddModal = this.handleOpenAddModal.bind(this);
        this.handleOpenDeleteModal = this.handleOpenDeleteModal.bind(this);
        this.handleOpenEditModal = this.handleOpenEditModal.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleUploadChoices = this.handleUploadChoices.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.saveChanges = this.saveChanges.bind(this);
    }

    componentDidMount() {
        const onSuccess = choiceGroups => this.setState({
            currentPage: 1,
            pages: Math.ceil(choiceGroups.length / PAGE_SIZE),
            choiceGroups: List(choiceGroups).sortBy(cg => cg.name),
            isLoading: false,
        });
        fetch({
            url: "/api/groups",
            onSuccess,
        });
    }

    hasMoreChoiceGroups() {
        const { currentPage, pages } = this.state;
        return currentPage < pages;
    }

    loadMoreChoiceGroups() {
        const nextPage = this.state.currentPage + 1;
        this.setState({ currentPage: nextPage });
    }

    handleOpenAddModal() {
        this.setState({ editOpen: true });
    }

    handleOpenDeleteModal(currentChoiceGroup) {
        this.setState({ deleteOpen: true, currentChoiceGroup });
    }

    handleOpenEditModal(currentChoiceGroup) {
        this.setState({ currentChoiceGroup, editOpen: true });
    }

    handleDelete() {
        const { choiceGroups, currentChoiceGroup } = this.state;
        const index = choiceGroups.findIndex(cg => cg.id === currentChoiceGroup.id);
        if (index > -1) {
            const updatedGroups = choiceGroups.delete(index);
            this.setState({ choiceGroups: updatedGroups, deleteOpen: false, currentChoiceGroup: null });
        }
    }

    handleEdit() {
        this.setState({ isLoading: true });
        const onSuccess = choiceGroups => this.setState({
            currentPage: 1,
            pages: Math.ceil(choiceGroups.length / PAGE_SIZE),
            choiceGroups: List(choiceGroups).sortBy(cg => cg.name),
            editOpen: false,
            isLoading: false,
        });
        fetch({
            url: "/api/groups",
            onSuccess,
        });
    }

    handleUploadChoices(event) {
        this.setState({ isLoading: true });
        const onSuccess = choiceGroups => {
            let updatedGroups = List(this.state.choiceGroups);
            choiceGroups.forEach(choiceGroup => {
                const pos = updatedGroups.findIndex(cg => cg.id === choiceGroup.id);
                if (pos > -1) {
                    updatedGroups = updatedGroups.update(pos, () => choiceGroup)
                } else {
                    updatedGroups = updatedGroups.push(choiceGroup).sortBy(cg => cg.name);
                }
            });
            this.setState({ choiceGroups: updatedGroups, isLoading: false, uploadError: null });
        }
        const onError = uploadError => this.setState({ isLoading: false, uploadError });
        const formData = new FormData();
        formData.append("choicesCsv", event.target.files[0]);
        fetch({
            url: "/api/bulk/groups",
            method: "post",
            data: formData,
            onSuccess,
            onError
        });
    }

    closeModal() {
        this.setState({ editOpen: false, deleteOpen: false });
    }

    saveChanges(choiceGroup, onSuccess) {
        const url = `/api/groups${choiceGroup.id ? '/' + choiceGroup.id : '/'}`;
        fetch({
            url,
            data: JSON.stringify(choiceGroup),
            method: "post",
            headers: { "Content-Type": "application/json" },
            onSuccess,
        });
    }

    render() {
        const { editOpen, choiceGroups, currentChoiceGroup, currentPage, deleteOpen, isLoading, uploadError } = this.state;
        const { contextPath } = this.props;
        const loader = (
            <Table.Row key={'loader'}>
                <Dimmer.Dimmable as={Table.Cell} colSpan={3}>
                    <Dimmer active><Loader /></Dimmer>
                </Dimmer.Dimmable>
            </Table.Row>
        );
        const UploadButton = props => (
            <React.Fragment>
                <label htmlFor="upload-choices" className={`ui orange ${props.className} upload basic button`}>Upload choices</label>
                <input type="file" accept=".csv,.xls,.xlsx" id="upload-choices" onChange={this.handleUploadChoices} />
            </React.Fragment>
        );
        const placeholder = isLoading
            ? (<Segment basic style={{ padding: 0 }}>Loading...</Segment>)
            : (
                <Segment placeholder>
                    <Header icon>
                        No choices have been added under to your account.
                    </Header>
                    <Segment.Inline>
                        <Button className="add" color="orange" onClick={this.handleOpenAddModal}>Add choices</Button>
                        <UploadButton />
                    </Segment.Inline>
                </Segment>
            )
        const rows = (
            <Transition.Group animation='glow'>
                {choiceGroups.slice(0, currentPage * PAGE_SIZE).map(group => {
                    return (
                        <Table.Row key={group.id}>
                            <Table.Cell>
                                <Grid columns="equal">
                                    <Grid.Row stretched>
                                        <Grid.Column>
                                            <div>
                                                <a href={`${contextPath}/choice-group/${group.id}`}>{group.name}</a>
                                            </div>
                                            <div className="resource detail">
                                                <span>
                                                    <strong>Ordering strategy: </strong>
                                                    {getOrderingStrategy(group.orderingStrategy)}
                                                </span>
                                                {"; "}
                                                <span>
                                                    <strong>Anchor last: </strong>
                                                    {group.excludeLast}
                                                </span>
                                            </div>
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Table.Cell>
                            <Table.Cell collapsing>
                                <Icon name="edit" className="edit-group" color="black" link onClick={() => this.handleOpenEditModal(group)} />
                                <Icon name="trash" className="delete-group" color="black" link onClick={() => this.handleOpenDeleteModal(group)} />
                            </Table.Cell>
                        </Table.Row>
                    )
                })}
            </Transition.Group>
        );
        const content = (
            <Table selectable>
                <InfiniteScroll
                    element={'tbody'}
                    loadMore={this.loadMoreChoiceGroups}
                    hasMore={this.hasMoreChoiceGroups()}
                    loader={loader}>
                    {rows}
                </InfiniteScroll>
            </Table>
        )
        return (
            <Container>
                <Dimmer active={isLoading}><Loader /></Dimmer>
                <Grid style={{ marginTop: '1rem' }}>
                    <Grid.Column width="sixteen">
                        <Header as="h2" className="title">Choices</Header>
                        <UploadButton className="right floated" />
                        <Button className="add" color="orange" floated="right" onClick={this.handleOpenAddModal}>Add choices</Button>
                    </Grid.Column>
                </Grid>
                {uploadError ? <Message error content={uploadError} /> : null}
                {choiceGroups.isEmpty() ? placeholder : content}
                <ChoicesEditorModal choiceGroup={currentChoiceGroup || {}} closeModal={this.closeModal} isOpen={editOpen} onEdit={this.handleEdit} />
                <ChoiceGroupDeleteModal choiceGroup={currentChoiceGroup || {}} closeModal={this.closeModal} isOpen={deleteOpen} onDelete={this.handleDelete} />
            </Container>
        );
    }
}

ChoicesListContainer.propTypes = {
    contextPath: PropTypes.string.isRequired,
}

export default ChoicesListContainer;