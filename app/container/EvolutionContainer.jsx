import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _function from 'lodash/function';
import { List } from 'immutable';
import striptags from 'striptags';
import { Breadcrumb, Button, Checkbox, Container, Grid, Header, Popup, Table } from "semantic-ui-react";
import { fetch, setupSocket, closeSocket } from "../network.utils";
import FlexContainer, { pivotJsonProcessor } from "../components/Flexmonster.jsx";
import { getAutoloadSetting, getFilters, saveAutoloadSetting } from "../store.util";
import FilterFormContainer from './FilterFormContainer.jsx';
import Notification from '../components/NewDataNotification.jsx';

class EvolutionContainer extends Component {

    constructor(props) {
        super(props);
        const autoload = getAutoloadSetting();
        this.state = {
            autoload,
            flexData: List(),
            isLoading: false,
            isLoadingJsonFile: false,
            isOpen: false,
            showNotification: false,
            transposed: false,
        };

        this.handleAutoloadChange = this.handleAutoloadChange.bind(this);
        this.handleJsonDataLoading = this.handleJsonDataLoading.bind(this);
        this.handleJsonDataLoaded = this.handleJsonDataLoaded.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.handleReload = this.handleReload.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.initSocket = this.initSocket.bind(this);
    }

    componentDidMount() {
        this.fetchData();
        this.initSocket();
    }

    componentWillUnmount() {
        closeSocket(this.stompClient);
    }

    handleAutoloadChange(event, data) {
        saveAutoloadSetting(data.checked);
        this.setState({
            autoload: data.checked
        });
    }

    handleJsonDataLoading() {
        this.setState({ isLoadingJsonFile: true });
    }

    handleJsonDataLoaded() {
        if (this.state.isLoadingJsonFile) {
            this.fetchData();
        }
    }

    handleFilter(updated) {
        this.setState({ isOpen: false });
        if (updated) {
            this.fetchData();
        }
    }

    handleReload() {
        this.fetchData();
        this.setState({ showNotification: false });
    }

    fetchData() {
        this.setState({ isLoading: true });
        const filters = getFilters();
        filters.id = this.props.resource.id;
        const url = `${this.props.apiBaseUrl}/evolution?${toQueryString(filters)}`;
        const jsonProcessor = _function.partialRight(
            pivotJsonProcessor,
            (flexData, defaultColumn, transposed) => {
                this.setState({ flexData, defaultColumn, transposed, isLoading: true, isLoadingJsonFile: false });
            });
        fetch({ url, onSuccess: jsonProcessor });
    }

    initSocket() {
        this.stompClient = setupSocket(`/topic/${this.props.resourceType}/${this.props.resource.id}/report/evolution`, () => {
            if (this.state.autoload) {
                this.fetchData();
            } else {
                this.setState({ showNotification: true });
            }
        });
    }

    render() {
        const { breadcrumbSections, resource, resourceType, users } = this.props;
        const { autoload, defaultColumn, flexData, isLoading, isLoadingJsonFile, isOpen, showNotification, } = this.state;
        const sections = [...breadcrumbSections, { key: "evolution", content: "Evolution", active: true },];
        const header = resourceType === "form" ? resource.name : striptags(resource.description);
        return (
            <Container style={{ marginTop: "4.3rem" }}>
                <Breadcrumb icon='right angle' sections={sections} />
                <Grid style={{ marginTop: '1rem' }}>
                    <Grid.Row>
                        <Grid.Column width={10}>
                            <Table basic="very" fixed singleLine>
                                <Table.Body>
                                    <Table.Row>
                                        <Table.Cell className="compact">
                                            <Header as="h2">{header}</Header>
                                        </Table.Cell>
                                    </Table.Row>
                                </Table.Body>
                            </Table>
                        </Grid.Column>
                        <Grid.Column width={6}>
                            <Popup hoverable position="bottom right" trigger={<Button basic color="orange" floated="right">Extras</Button>}>
                                <Checkbox label="Auto-refresh" checked={autoload} onClick={this.handleAutoloadChange} />
                            </Popup>
                            <Button floated="right" color="orange" onClick={() => this.setState({ isOpen: true })} >Filter</Button>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <FlexContainer
                    flexData={flexData}
                    defaultColumn={defaultColumn}
                    isLoading={isLoading}
                    isLoadingJsonFile={isLoadingJsonFile}
                    onJsonDataLoading={this.handleJsonDataLoading}
                    onJsonDataLoaded={this.handleJsonDataLoaded}
                />
                <Notification show={showNotification}
                    onYesClicked={this.handleReload}
                    onNoClicked={() => this.setState({ showNotification: false })} />
                <FilterFormContainer isOpen={isOpen} onCloseModal={this.handleFilter} fields={fields} users={users} />
            </Container>
        )
    }
}

EvolutionContainer.propTypes = {
    apiBaseUrl: PropTypes.string.isRequired,
    breadcrumbSections: PropTypes.array.isRequired,
    fields: PropTypes.array.isRequired,
    resource: PropTypes.object.isRequired,
    resourceType: PropTypes.string.isRequired,
    users: PropTypes.array.isRequired,
};

export default EvolutionContainer;