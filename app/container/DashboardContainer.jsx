import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _function from 'lodash/function';
import { List } from 'immutable';
import striptags from 'striptags';
import { Button, Container, Form, Grid, Header, Loader, Segment, Table } from 'semantic-ui-react';
import { fetch, setupSocket, closeSocket } from '../network.utils';
import { getAutoloadSetting, saveAutoloadSetting, getFilters, getViewModeSetting, saveViewModeSetting } from '../store.util';
import { pivotJsonProcessor } from '../components/Flexmonster.jsx';
import { VIEW_MODES } from '../constants';

import ComponentDeleteModal from './ComponentDeleteModal.jsx';
import ComponentEditorModal from './ComponentEditorModal.jsx';
import DownloadModal from './DownloadModal.jsx';
import FilterFormContainer from './FilterFormContainer.jsx';

import Dashboard from '../components/Dashboard.jsx';
import Notification from '../components/NewDataNotification.jsx';
import ReportActions from '../components/ReportActions.jsx';
import ReportSelector from '../components/ReportSelector.jsx';
import SecondaryNav from '../components/SecondaryNav.jsx';

class DashboardContainer extends Component {
    constructor(props) {
        super(props);
        const autoload = getAutoloadSetting();
        const viewMode = getViewModeSetting();
        this.state = {
            autoload,
            dashboardComponents: List(),
            isDeleteOpen: false,
            isDownloadOpen: false,
            isEditOpen: false,
            isFilterOpen: false,
            isLoading: true,
            isLoadingComponents: true,
            flexData: List(),
            showNotification: false,
            selectedComponent: null,
            viewMode,
        };

        this.handleAutoloadChange = this.handleAutoloadChange.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleDuplicate = this.handleDuplicate.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.handleOpenDeleteModal = this.handleOpenDeleteModal.bind(this);
        this.handleOpenEditModal = this.handleOpenEditModal.bind(this);
        this.handleReload = this.handleReload.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleViewModeChange = this.handleViewModeChange.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.initSocket = this.initSocket.bind(this);
        this.sortComponent = this.sortComponent.bind(this);
        this.sortingDone = this.sortingDone.bind(this);
    }

    componentDidMount() {
        this.fetchData();
        this.initSocket();
    }

    componentWillUnmount() {
        closeSocket(this.stompClient);
    }

    handleAutoloadChange(autoload) {
        saveAutoloadSetting(autoload)
        this.setState({ autoload });
    }

    handleCloseModal() {
        this.setState({ isDeleteOpen: false, isEditOpen: false });
    }

    handleDelete(componentId) {
        const index = this.state.dashboardComponents.findIndex(c => c.id === componentId);
        if (index > -1) {
            const updatedComponents = this.state.dashboardComponents.delete(index);
            this.setState({ dashboardComponents: updatedComponents, isDeleteOpen: false, selectedComponent: null });
        }
    }

    handleDuplicate(componentId) {
        const { contextPath, resource } = this.props;
        const onSuccess = duplicatedComponent => {
            const insertIndex = this.state.dashboardComponents.findIndex(c => c.id === componentId) + 1;
            const updatedComponents = this.state.dashboardComponents.insert(insertIndex, duplicatedComponent);
            this.setState({ dashboardComponents: updatedComponents, isEditOpen: false, selectedComponent: null });
        }
        fetch({
            url: `${contextPath}/api/project/${resource.survey.id}/form/${resource.id}/dashboard/component/${componentId}/duplicate`,
            onSuccess,
            method: "post"
        });
    }

    handleFilter(updated) {
        this.setState({ isFilterOpen: false });
        if (updated) {
            this.fetchData();
        }
    }

    handleOpenDeleteModal(componentId) {
        const selectedComponent = this.state.dashboardComponents.find(c => c.id === componentId);
        this.setState({ selectedComponent, isDeleteOpen: true });
    }

    handleOpenEditModal(componentId) {
        let selectedComponent = null;
        if (componentId) {
            selectedComponent = this.state.dashboardComponents.find(c => c.id === componentId);
        }
        this.setState({ selectedComponent, isEditOpen: true });
    }

    handleReload() {
        this.fetchData();
        this.setState({ showNotification: false });
    }

    handleEdit(component) {
        const index = this.state.dashboardComponents.findIndex(c => c.id === component.id);
        let updatedComponents;
        if (index > -1) {
            updatedComponents = this.state.dashboardComponents.update(index, () => component);
        } else {
            updatedComponents = this.state.dashboardComponents.push(component);
        }
        this.setState({ dashboardComponents: updatedComponents, isEditOpen: false, selectedComponent: null });
    }

    handleViewModeChange() {
        const { viewMode } = this.state;
        const updatedViewMode = viewMode === VIEW_MODES.LIST ? VIEW_MODES.GRID : VIEW_MODES.LIST;
        saveViewModeSetting(updatedViewMode);
        this.setState({
            viewMode: updatedViewMode,
        });
    }

    fetchData() {
        this.setState({ isLoadingComponents: true, isLoading: true });
        const componentUrl = `${this.props.apiBaseUrl}/dashboard/component`;
        const dcJsonProcessor = _function.partialRight(dashboardComponentJsonProcessor, (dashboardComponents) => {
            const filters = getFilters();
            filters.id = this.props.resource.id;
            const dataUrl = `${this.props.apiBaseUrl}/pivot?${toQueryString(filters)}`;
            const pJsonProcessor = _function.partialRight(
                pivotJsonProcessor,
                (flexData, defaultColumn) => {
                    this.setState({ dashboardComponents: List(dashboardComponents), flexData, defaultColumn, isLoading: false, isLoadingComponents: false  });
                });
            fetch({ url: dataUrl, onSuccess: pJsonProcessor });
        });
        fetch({ url: componentUrl, onSuccess: dcJsonProcessor });
    }

    initSocket() {
        this.stompClient = setupSocket(`/topic/${this.props.resourceType}/${this.props.resource.id}/report/pivot`,
            () => {
                if (this.state.autoload) {
                    this.fetchData();
                } else {
                    this.setState({ showNotification: true });
                }
            });
    }

    sortComponent(dragIndex, hoverIndex) {
        this.beforeSort = List(this.state.dashboardComponents);
        const { dashboardComponents } = this.state;
        const dragComponent = dashboardComponents.get(dragIndex);
        const sortedComponents = dashboardComponents.delete(dragIndex).insert(hoverIndex, dragComponent);

        this.setState({
            dashboardComponents: sortedComponents,
        });
    }

    sortingDone() {
        const params = toQueryString({ sortedComponentIds: this.state.dashboardComponents.toJS().map(c => c.id) });
        const onError = () => this.setState({ dashboardComponents: this.beforeSort });
        fetch({ url: `${contextPath}/api/project/${resource.survey.id}/form/${this.props.resource.id}/dashboard/component/sort?${params}`, method: "post", onError })
    }

    render() {
        const { apiBaseUrl, breadcrumbSections, contextPath, resource, resourceType, users, } = this.props;
        const { autoload, dashboardComponents, defaultColumn, flexData, isDeleteOpen, isDownloadOpen, isEditOpen, isFilterOpen, isLoading, isLoadingComponents, selectedComponent, showNotification, viewMode, } = this.state;
        const content =(
            <Dashboard
                components={dashboardComponents}
                defaultColumn={defaultColumn}
                flexData={flexData}
                sortComponent={this.sortComponent}
                sortingDone={this.sortingDone}
                onDuplicate={this.handleDuplicate}
                onOpenDeleteModal={this.handleOpenDeleteModal}
                onOpenEditModal={this.handleOpenEditModal}
                viewMode={viewMode}
            />
        );
        let emptyMessage = <Header icon>{'No data has been submited for this form'}</Header>;
        if (dashboardComponents.isEmpty()) {
            emptyMessage = (
                <React.Fragment>
                    <Header icon>
                        {'No visualizations have been configured for this form.'}
                    </Header>
                    <Button onClick={this.handleOpenEditModal} color="orange">Add visualization</Button>
                </React.Fragment>
            );
        }
        const isEmpty = (!isLoadingComponents && dashboardComponents.isEmpty()) || (!isLoading && flexData.isEmpty());
        const placeholder = (
            <Grid.Row>
                <Grid.Column>
                    <Segment placeholder>
                        {emptyMessage}
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        );
        const header = resourceType === "form" ? resource.name : striptags(resource.description);
        return (
            <React.Fragment>
                <SecondaryNav sections={[...breadcrumbSections, { key: "dashboard", content: "Custom dashboard", active: true },]}/>
                <Container style={{ marginTop: '3rem' }}>
                    <Grid style={{ marginTop: '1rem' }}>
                        <Grid.Row>
                            <Grid.Column width={10}>
                                <Table basic="very" fixed singleLine>
                                    <Table.Body>
                                        <Table.Row>
                                            <Table.Cell className="compact">
                                                <Header as="h2">{header}</Header>
                                            </Table.Cell>
                                        </Table.Row>
                                    </Table.Body>
                                </Table>
                            </Grid.Column>
                            <Grid.Column width={6}>
                                <ReportActions
                                        actions={[
                                            { label: 'Add visualization', onClick: this.handleOpenEditModal },
                                            { label: `View as ${viewMode === VIEW_MODES.LIST ? 'grid' : 'list'}`, onClick: this.handleViewModeChange },
                                        ]}
                                        autoload={autoload}
                                        onAutoloadChange={() => this.handleAutoloadChange(!autoload)}
                                        onDownload={() => this.setState({ isDownloadOpen: true })}
                                        onFilter={() => this.setState({ isFilterOpen: true })} />
                                <ReportSelector active="dashboard" reportTypes={reportTypes} />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <Grid>
                        {isLoading ? <Loader active/> : isEmpty ? placeholder : content}
                    </Grid>
                    <ComponentDeleteModal isOpen={isDeleteOpen} contextPath={contextPath} component={selectedComponent} form={resource} onDelete={this.handleDelete} onClose={this.handleCloseModal} />
                    <ComponentEditorModal isOpen={isEditOpen} contextPath={contextPath} component={selectedComponent} defaultColumn={defaultColumn} flexData={flexData} form={resource} isLoading={isLoading} onSave={this.handleEdit} onClose={this.handleCloseModal} />
                    <DownloadModal apiBaseUrl={apiBaseUrl} isOpen={isDownloadOpen} onClose={() => this.setState({ isDownloadOpen: false })}/>
                    <FilterFormContainer isOpen={isFilterOpen} onCloseModal={this.handleFilter} fields={fields} users={users} />
                    <Notification show={showNotification}
                        onYesClicked={this.handleReload}
                        onNoClicked={() => this.setState({ showNotification: false })} />
                </Container>
            </React.Fragment>
        );
    }
}

DashboardContainer.propTypes = {
    apiBaseUrl: PropTypes.string.isRequired,
    breadcrumbSections: PropTypes.array.isRequired,
    contextPath: PropTypes.string.isRequired,
    fields: PropTypes.array.isRequired,
    history: PropTypes.object.isRequired,
    reportTypes: PropTypes.arrayOf(PropTypes.shape({ text: PropTypes.string.isRequired, path: PropTypes.string.isRequired })).isRequired,
    resource: PropTypes.object.isRequired,
    resourceType: PropTypes.string.isRequired,
    users: PropTypes.array.isRequired,
};

export default DashboardContainer;

function dashboardComponentJsonProcessor(response, onSuccess) {
    const components = response.map(component => {
        if (component.configuration.conditions) {
            const conditions = component.configuration.conditions.map(condition => Object.assign({}, condition, { isTotal: false }));
            const newComponent = Object.assign({}, component);
            _object.merge(newComponent, { configuration: { conditions } });
            return newComponent;
        }
        return component;
    });
    onSuccess(components);
}
