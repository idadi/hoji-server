import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Button, Dropdown, Checkbox, Form, Modal} from 'semantic-ui-react';
import MaskedInput from 'react-text-mask';
import moment from "moment";
import { getFilters, saveFilters } from "../store.util";

class FilterFormContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            choices: [],
            selectedField: null,
            selectedChoice: null,
            completed: null,
            completedAfter: null,
            completedBefore: null,
            errors: {},
            uploaded: null,
            uploadedAfter: null,
            uploadedBefore: null,
            testMode: false,
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleSelectField = this.handleSelectField.bind(this);
        this.handleSelectPeriod = this.handleSelectPeriod.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    componentDidMount() {
        const filters = getFilters();
        const choices = filters.selectedField && this.props.fields.find(field => field.id === filters.selectedField)
            ? this.props.fields.find(field => field.id === filters.selectedField).choiceGroup.choices
            : [];
        const selectedField = choices.length ? filters.selectedField : "";
        const selectedChoice = choices.length ? filters.selectedChoice : "";
        const newState = Object.assign({}, filters, {
            choices,
            selectedField,
            selectedChoice,
            completedAfter: filters.completedAfter ? moment(filters.completedAfter, "YYYY-MM-DD").format("DD/MM/YYYY") : "",
            completedBefore: filters.completedBefore ? moment(filters.completedBefore, "YYYY-MM-DD").format("DD/MM/YYYY") : "",
            uploadedAfter: filters.uploadedAfter ? moment(filters.uploadedAfter, "YYYY-MM-DD").format("DD/MM/YYYY") : "",
            uploadedBefore: filters.uploadedBefore ? moment(filters.uploadedBefore, "YYYY-MM-DD").format("DD/MM/YYYY") : "",
        });
        this.setState(newState);
    }

    handleChange(event, data) {
        if (data.name === "testMode") {
            this.setState({ "testMode": data.checked });
        } else {
            this.setState({ [data.name]: data.value });
        }
    }

    handleDateChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSelectField(event, data) {
        if (data.value) {
            const { fields } = this.props;
            const fieldId = parseInt(data.value);
            const choiceGroup = fields.find(field => field.id === fieldId).choiceGroup;
            this.setState({ selectedField: fieldId, selectedChoice: "", choices: choiceGroup.choices });
        }
    }

    handleSelectPeriod(event, data) {
        this.setState({ [data.name]: data.value });
    }

    handleSubmit() {
        const errors = {};
        if (this.state.completedAfter && !moment(this.state.completedAfter, "DD/MM/YYYY").isValid()) {
            errors.completedAfter = "This is not a valid date";
        }
        if (this.state.completedBefore && !moment(this.state.completedBefore, "DD/MM/YYYY").isValid()) {
            errors.completedBefore = "This is not a valid date";
        }
        if (this.state.uploadedAfter && !moment(this.state.uploadedAfter, "DD/MM/YYYY").isValid()) {
            errors.uploadedAfter = "This is not a valid date";
        }
        if (this.state.uploadedBefore && !moment(this.state.uploadedBefore, "DD/MM/YYYY").isValid()) {
            errors.uploadedBefore = "This is not a valid date";
        }
        if (Object.keys(errors).length) {
            this.setState({ errors });
            return;
        }
        const selectedUsers = this.state.selectedUsers;
        const selectedField = this.state.selectedField;
        const selectedChoice = this.state.selectedChoice;
        const completed = this.state.completed;
        const completedAfter = this.state.completedAfter ?
            moment(this.state.completedAfter, "DD/MM/YYYY").format("YYYY-MM-DD") : "";
        const completedBefore = this.state.completedBefore ?
            moment(this.state.completedBefore, "DD/MM/YYYY").format("YYYY-MM-DD") : "";
        const uploaded = this.state.uploaded;
        const uploadedAfter = this.state.uploadedAfter ?
            moment(this.state.uploadedAfter, "DD/MM/YYYY").format("YYYY-MM-DD") : "";
        const uploadedBefore = this.state.uploadedBefore ?
            moment(this.state.uploadedBefore, "DD/MM/YYYY").format("YYYY-MM-DD") : "";
        const testMode = this.state.testMode;
        const updatedFilters = {
            selectedUsers, selectedField, selectedChoice, completed, completedAfter,
            completedBefore, uploaded, uploadedAfter, uploadedBefore, testMode
        };
        saveFilters(updatedFilters);
        this.props.onCloseModal(true);
    }

    handleReset() {
        const updatedFilters = {};
        saveFilters(updatedFilters);
        this.props.onCloseModal(true);
    }

    render() {
        const { onCloseModal, fields, isOpen, users, } = this.props;
        const { choices, selectedField, selectedChoice, completed, completedAfter, completedBefore, uploaded, uploadedAfter, uploadedBefore, testMode, selectedUsers, errors } = this.state;
        const userOptions = users.map(user => ({ key: user.id, value: user.id, text: user.fullName }));
        const fieldOptions = fields.map(f => ({ key: f.id, value: f.id, text: f.description }));
        const choiceOptions = choices.map(c => ({ key: c.id, value: c.id, text: c.name }));
        const periodOptions = [
            { key: 1, value: 'Whenever', text: 'Whenever' },
            { key: 2, value: 'Today', text: 'Today' },
            { key: 3, value: 'Yesterday', text: 'Yesterday' },
            { key: 4, value: 'This week', text: 'This week' },
            { key: 5, value: 'Last week', text: 'Last week' },
            { key: 6, value: 'This month', text: 'This month' },
            { key: 7, value: 'Last month', text: 'Last month' },
            { key: 8, value: 'This year', text: 'This year' },
            { key: 9, value: 'Last year', text: 'Last year' },
            { key: 10, value: 'Between', text: 'Between' },
        ];
        let completedDateInputs = null;
        if (completed === 'Between') {
            completedDateInputs = (
                <Form.Group widths="equal">
                    <Form.Input label={<span>Start date<span className="optional label"> - Optional</span></span>} error={errors.completedAfter && { content: errors.completedAfter, pointing }} children={<MaskedInput mask={[/[0-3]/, /\d/, '/', /[0-1]/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]} guide={false} name="completedAfter" value={completedAfter} onChange={this.handleDateChange} placeholder="dd/mm/yyyy" />} />
                    <Form.Input label={<span>End date<span className="optional label"> - Optional</span></span>} error={errors.completedBefore && { content: errors.completedBefore, pointing }} children={<MaskedInput mask={[/[0-3]/, /\d/, '/', /[0-1]/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]} guide={false} name="completedBefore" value={completedBefore} onChange={this.handleDateChange} placeholder="dd/mm/yyyy" />} />
                </Form.Group>
            );
        }
        let uploadedDateInputs = null;
        if (uploaded === 'Between') {
            uploadedDateInputs = (
                <Form.Group widths="equal">
                    <Form.Input label={<span>Start date<span className="optional label"> - Optional</span></span>} error={errors.uploadedAfter && { content: errors.uploadedAfter, pointing }} children={<MaskedInput mask={[/[0-3]/, /\d/, '/', /[0-1]/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]} guide={false} name="uploadedAfter" value={uploadedAfter} onChange={this.handleDateChange} placeholder="dd/mm/yyyy" />} />
                    <Form.Input label={<span>End date<span className="optional label"> - Optional</span></span>} error={errors.uploadedBefore && { content: errors.uploadedBefore, pointing }} children={<MaskedInput mask={[/[0-3]/, /\d/, '/', /[0-1]/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]} guide={false} name="uploadedBefore" value={uploadedBefore} onChange={this.handleDateChange} placeholder="dd/mm/yyyy" />} />
                </Form.Group>
            );
        }
        return (
            <Modal closeOnDimmerClick={false} closeOnEscape={true} onClose={() => onCloseModal(false)} open={isOpen}>
                <Modal.Header className="ui orange">Report filter</Modal.Header>
                <Modal.Content>
                    <div className="ui visible positive message">
                        <strong>TIP: </strong>
                        You can filter your report by any of the attributes below.
                        <a href="https://www.hoji.co.ke/support/#reamaze#0#/kb/reports/how-to-filter-a-report"
                           target="_blank">
                            {' More on report filtering.'}
                        </a>
                    </div>
                    <Form onSubmit={this.handleSubmit}>
                        <div className="field">
                            <label>Filter by</label>
                            <Dropdown name="selectedField" search selection clearable options={fieldOptions} defaultValue={selectedField} onChange={this.handleSelectField} />
                            <span className="optional label">
                                {`Select the field by which to filter your report.`}
                            </span>
                        </div>
                        <div className="field">
                            <label>Select value</label>
                            <Dropdown name="selectedChoice" search selection clearable options={choiceOptions} defaultValue={selectedChoice} onChange={this.handleChange} />
                            <span className="optional label">
                                {`Select the value by which to filter your report.`}
                            </span>
                        </div>
                        <div className="field">
                            <label>Created by</label>
                            <Dropdown name="selectedUsers" search selection multiple clearable options={userOptions} defaultValue={selectedUsers} onChange={this.handleChange} />
                            <span className="optional label">
                                {`Select the user or users by which to filter your report.`}
                            </span>
                        </div>
                        <div className="field">
                            <label>Completed</label>
                            <Dropdown name="completed" selection options={periodOptions} defaultValue={completed} onChange={this.handleSelectPeriod} />
                            {completedDateInputs}
                            <span className="optional label">
                                {`Select the completion period by which to filter your report.`}
                            </span>
                        </div>
                        <div className="field">
                            <label>Uploaded</label>
                            <Dropdown name="uploaded" selection options={periodOptions} defaultValue={uploaded} onChange={this.handleSelectPeriod} />
                            {uploadedDateInputs}
                            <span className="optional label">
                                {`Select the upload period by which to filter your report.`}
                            </span>
                        </div>
                        <div className="field">
                            <Checkbox name="testMode" label="Test data" checked={testMode} onChange={this.handleChange} />
                            <span className="optional label">
                                {` - Tick to load test instead of production data.`}
                                <a href="https://www.hoji.co.ke/support/#reamaze#0#/kb/reports/test-data"
                                   target="_blank">
                                        {' More on test data.'}
                                </a>
                            </span>
                        </div>
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button color="orange" onClick={this.handleSubmit}>Filter</Button>
                    <Button basic onClick={this.handleReset}>Reset</Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

FilterFormContainer.propTypes = {
    onCloseModal: PropTypes.func.isRequired,
    fields: PropTypes.array.isRequired,
    isOpen: PropTypes.bool.isRequired,
    users: PropTypes.array.isRequired,
};

export default FilterFormContainer;
