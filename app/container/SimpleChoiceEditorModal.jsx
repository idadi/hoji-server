import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, Form, Modal} from "semantic-ui-react";
import _lang from "lodash/lang";

class SimpleChoiceEditorModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            choice: {},
            errors: {},
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.choice !== this.props.choice) {
            const choice = Object.assign({scale: 0}, this.props.choice);
            this.setState({ choice })
        }
    }

    handleChange(event, data) {
        const choice = Object.assign({}, this.state.choice);
        choice[data.name] = data.value;
        this.setState({choice});
    }

    handleSubmit() {
        const choice = this.state.choice;
        const errors = {};
        if (!choice.name) {
            errors.name = "name is required";
        }
        if (!_lang.isEmpty(errors)) {
            this.setState({errors});
            return;
        }
        this.props.save(choice);
    }

    render() {
        const { closeModal, isOpen, } = this.props;
        const { choice, errors } = this.state;
        return (
            <Modal size="tiny" closeOnDimmerClick={false} closeOnEscape={true} onClose={closeModal} open={isOpen}>
                <Modal.Header className="ui orange">{choice.id ? "Edit choice" : "Add choice"}</Modal.Header>
                <Modal.Content>
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Input fluid name="name" label="Name" value={choice.name || ""} onChange={this.handleChange} error={errors.name && { content: errors.name, pointing: 'below' }} />
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button className="edit-submit" color="orange" onClick={this.handleSubmit}>{`${choice.id?'Update':'Add'} choice`}</Button>
                    <Button basic onClick={closeModal}>Cancel</Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

SimpleChoiceEditorModal.defaultProps = {
    isDefault: true,
}

SimpleChoiceEditorModal.propTypes = {
    choice: PropTypes.object,
    closeModal: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
    save: PropTypes.func.isRequired,
};

export default SimpleChoiceEditorModal;