import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Modal } from 'semantic-ui-react';
import FlexContainer from '../components/Flexmonster.jsx';
import { List } from 'immutable';
import _object from 'lodash/object';
import { fetch } from "../network.utils";

class ComponentEditorModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            componentName: '',
            showFilter: false,
            errors: {},
        }

        this.customizeToolbar = this.customizeToolbar.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleFlexmonsterLoaded = this.handleFlexmonsterLoaded.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.component !== this.props.component) {
            this.setState({
                componentName: this.props.component ? this.props.component.name : '',
                showFilter: this.props.component && this.props.component.configuration.options.chart && this.props.component.configuration.options.chart.showFilter ? true : false,
            })
        }
    }

    customizeToolbar(toolbar) {
        const tabs = toolbar.getTabs().filter(tab => !['fm-tab-connect', 'fm-tab-open', 'fm-tab-save'].includes(tab.id));
        toolbar.getTabs = () => tabs;
    };

    handleChange(event, data) {
        this.setState({ [data.name]: data.value || data.checked });
    }

    handleFlexmonsterLoaded(flexmonster) {
        this.flexmonster = flexmonster;
    }

    handleSubmit(event) {
        event.preventDefault();
        const { contextPath, component, form } = this.props;
        const componentId = component ? component.id : '';
        const postUrl = `${contextPath}/api/project/${form.survey.id}/form/${form.id}/dashboard/component/${componentId}`;
        const componentName = this.state.componentName;
        if (!componentName) {
            this.setState({ errors: { componentName: 'Required' } });
        } else {
            const reportConfig = _object.omit(this.flexmonster.getReport(), 'dataSource.data');
            _object.merge(reportConfig, {
                options: {
                    chart: { showFilter: this.state.showFilter }
                }
            });
            const data = JSON.stringify(Object.assign(
                {
                    id: component ? component.id : null,
                    name: componentName,
                    ordinal: component ? component.ordinal : null,
                    configuration: reportConfig,
                    form: form.id
                })
            );
            const onSuccess = value => {
                this.props.onSave(value);
            };
            fetch({ method: 'post', url: postUrl, data, onSuccess, headers: { 'Content-Type': 'application/json'} });
        }
    }

    render() {
        const { component, onClose, defaultColumn, flexData, isLoading } = this.props;
        const { errors, componentName, showFilter } = this.state;
        if (component && component.configuration.conditions) {
            const conditions = component.configuration.conditions.map(condition => Object.assign({}, condition, { isTotal: false }));
            const newComponent = Object.assign({}, component);
            _object.merge(newComponent, { configuration: { conditions } });
            component = newComponent;
        }
        return (
            <Modal size="fullscreen" open={this.props.isOpen} closeOnDimmerClick={false} closeOnEscape={true} onClose={onClose}>
                <Modal.Header className="ui orange">{`${component ? 'Update' : 'Add'} visualization`}</Modal.Header>
                <Modal.Content>
                    <Form onSubmit={this.handleSubmit}>
                        <div className="field">
                            <FlexContainer flexData={flexData}
                                defaultColumn={defaultColumn}
                                isLoading={isLoading}
                                report={component && component.configuration}
                                toolbarCustomizer={this.customizeToolbar}
                                onFlexmonsterLoad={this.handleFlexmonsterLoaded}
                            />
                        </div>
                        <Form.Input label='Name' name='componentName' value={componentName} error={errors.componentName && { content: errors.componentName, pointing: 'below', }} onChange={this.handleChange} />
                        <Form.Checkbox label='Show filter' name='showFilter' checked={showFilter} onChange={this.handleChange} />
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button className="edit-submit" color='orange' onClick={this.handleSubmit} disabled={isLoading}>{`${component ? 'Update' : 'Add'} visualization`}</Button>
                    <Button onClick={onClose}>Cancel</Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

ComponentEditorModal.propTypes = {
    contextPath: PropTypes.string.isRequired,
    component: PropTypes.object,
    defaultColumn: PropTypes.string,
    flexData: PropTypes.instanceOf(List).isRequired,
    form: PropTypes.object.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onSave: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
};

export default ComponentEditorModal;