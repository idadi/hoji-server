import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import _function from 'lodash/function';
import { List } from 'immutable';
import striptags from 'striptags';
import { fetch, setupSocket, closeSocket } from "../network.utils";
import { Container, Grid, Header, Loader, Segment, Table } from "semantic-ui-react";
import { Map, GoogleApiWrapper } from 'google-maps-react';
import { getFilters, saveAutoloadSetting, getAutoloadSetting } from '../store.util';
import MarkerClusterer from '@google/markerclusterer';

import DownloadModal from './DownloadModal.jsx';
import FilterFormContainer from './FilterFormContainer.jsx';

import Notification from '../components/NewDataNotification.jsx';
import ReportActions from '../components/ReportActions.jsx';
import ReportSelector from '../components/ReportSelector.jsx';
import SecondaryNav from '../components/SecondaryNav.jsx';

class MapContainer extends Component {
    constructor(props) {
        super(props);
        const autoload = getAutoloadSetting();
        this.state = {
            autoload,
            gpsPoints: List(),
            isLoading: true,
            isDownloadOpen: false,
            isFilterOpen: false,
            mapVisible: true,
            showNotification: false,
        };

        this.showInfoWindow = this.showInfoWindow.bind(this);
        this.closeInfoWindow = this.closeInfoWindow.bind(this);
        this.handleAutoloadChange = this.handleAutoloadChange.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.handleReady = this.handleReady.bind(this);
        this.handleResize = this.handleResize.bind(this);
        this.handleReload = this.handleReload.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.initSocket = this.initSocket.bind(this);
    }

    componentDidMount() {
        ReactDOM.findDOMNode(this).parentNode.setAttribute('style', "display: flex; flex-flow: column; flex: 1 0 auto;")
        this.fetchData();
        this.initSocket();
    }

    componentDidUpdate() {
        if (this.markerClusterer) {
            this.markerClusterer.clearMarkers();
        }
        if (this.state.mapVisible && this.props.google && this.state.gpsPoints.size > 0) {
            const markers = this.state.gpsPoints
                .map(gp => {
                    const marker = new this.props.google.maps.Marker({ position: { lat: gp.latitude, lng: gp.longitude } });
                    marker.addListener('click', () => this.showInfoWindow(gp, marker));
                    return marker;
                })
                .toJS();
            this.markerClusterer = new MarkerClusterer(this.map, markers, { imagePath: '/resources/img/m', maxZoom: 21 });
            this.infoWindow = new this.props.google.maps.InfoWindow();
        }
    }

    componentWillUnmount() {
        if (this.infoWindow) {
            this.infoWindow.close();
        }
        if (this.markerClusterer) {
            this.markerClusterer.clearMarkers();
        }
        closeSocket(this.stompClient);
    }

    handleAutoloadChange(autoload) {
        saveAutoloadSetting(autoload)
        this.setState({ autoload });
    }

    handleFilter(updated) {
        this.setState({ isFilterOpen: false });
        if (updated) {
            this.fetchData();
        }
    }

    handleReload() {
        this.fetchData();
        this.setState({ showNotification: false });
    }

    fetchData() {
        this.setState({ isLoading: true });
        const filters = getFilters();
        filters.id = this.props.resource.id;
        const url = `${this.props.apiBaseUrl}/map?${toQueryString(filters)}`;
        const jsonProcessor = _function.partialRight(
            mapDataJsonProcessor,
            this,
            (gpsPoints) => {
                this.setState({ isLoading: false, gpsPoints, mapVisible: false })
            }
        );
        fetch({ url, onSuccess: jsonProcessor });
    }

    initSocket() {
        this.stompClient = setupSocket(`/topic/${this.props.resourceType}/${this.props.resource.id}/report/map`, () => {
            if (this.state.autoload) {
                this.fetchData();
            } else {
                this.setState({ showNotification: true });
            }
        });
    }

    showInfoWindow(gpsPoint, marker) {
        const recordUrl = `${this.props.baseUrl.replace('report', '')}record?recordUuid=${gpsPoint.uuid}`;
        const content = `
        <table class="ui single line very basic table">
            <tbody>
                <tr>
                    <td>
                        <span class="ui orange header">${gpsPoint.caption}</span>
                    </td>
                    <td class="collapsing">
                        <span><a href="${recordUrl}" target="_blank">view record <i aria-hidden="true" class="external alternate icon"></i></a></span>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="ui single line very basic table">
            <tbody>
                <tr>
                    <td class="collapsing">Location: </td>
                    <td class="right aligned">${gpsPoint.address}</td>
                </tr>
                <tr>
                    <td class="collapsing">Collected By: </td>
                    <td class="right aligned">${gpsPoint.user}</td>
                </tr>
                <tr>
                    <td class="collapsing">Started On: </td>
                    <td class="right aligned">${gpsPoint.dateCreated}</td>
                </tr>
                <tr>
                    <td class="collapsing">Completed On: </td>
                    <td class="right aligned">${gpsPoint.dateCompleted}</td>
                </tr>
                <tr>
                    <td class="collapsing">Updated On: </td>
                    <td class="right aligned">${gpsPoint.dateUpdated}</td>
                </tr>
                <tr>
                    <td class="collapsing">Uploaded On: </td>
                    <td class="right aligned">${gpsPoint.dateUploaded}</td>
                </tr>
                <tr>
                    <td class="collapsing">Record Version: </td>
                    <td class="right aligned">${gpsPoint.version}</td>
                </tr>
                <tr>
                    <td class="collapsing">Accuracy (m): </td>
                    <td class="right aligned">${gpsPoint.accuracy}</td>
                </tr>
                <tr>
                    <td class="collapsing">Co-ordinates: </td>
                    <td class="right aligned">${gpsPoint.latitude}, ${gpsPoint.longitude}</td>
                </tr>
            </tbody>
        </table>`;
        this.infoWindow.close();
        this.infoWindow.setContent(content);
        this.infoWindow.open(this.map, marker);
    }

    closeInfoWindow() {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeGpsPoint: null
            })
        }
    }

    handleReady(_, map) {
        this.map = map;
        this.setState({
            mapVisible: true,
        });
    }

    handleResize(mapProps, map) {
        this.map = map;
        if (!this.state.mapVisible) {
            this.setState({
                mapVisible: true
            });
        }
        const bounds = getBounds(this.state.gpsPoints);
        const googleMap = window["google"];
        const googleBounds = new googleMap.maps.LatLngBounds(bounds.sw, bounds.ne);
        this.map.fitBounds(googleBounds);
    }

    render() {
        const { apiBaseUrl, breadcrumbSections, resource, resourceType, users } = this.props;
        const { autoload, gpsPoints, isLoading, isDownloadOpen, isFilterOpen, mapVisible, showNotification, } = this.state;
        const style = { display: "flex", flexFlow: "column", flex: "1 0 auto", marginTop: "5 rem" };
        const containerStyle = Object.assign({}, style, { position: "relative" });
        const initialCenter = { lat: 6.342596964836323, lng: -341.3671875 };
        const placeholder = (
            <Grid.Row>
                <Grid.Column>
                    <Segment placeholder>
                        <Header icon>
                            {'No data has been submited for this form.'}
                        </Header>
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        );
        const content = (
            <Grid.Row>
                <Grid.Column style={{ display: "flex", minHeight: '70vh' }}>
                    <Segment attached>
                        <Map google={this.props.google} zoom={14} containerStyle={containerStyle}
                            visible={mapVisible} onReady={this.handleReady}
                            onResize={this.handleResize} initialCenter={initialCenter} clickableIcons={false} />
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        );
        const header = resourceType === "form" ? resource.name : striptags(resource.description);
        return (
            <React.Fragment>
                <SecondaryNav sections={[...breadcrumbSections, { key: "map", content: "Geo-map", active: true },]}/>
                <Container style={{ display: "flex", flexFlow: "column", flex: "1 0 auto", minHeight: "70vh", marginTop: "2rem" }}>
                    <Grid style={{ marginTop: '1rem' }}>
                        <Grid.Row>
                            <Grid.Column width={10}>
                                <Table basic="very" fixed singleLine>
                                    <Table.Body>
                                        <Table.Row>
                                            <Table.Cell className="compact">
                                                <Header as="h2">{header}</Header>
                                            </Table.Cell>
                                        </Table.Row>
                                    </Table.Body>
                                </Table>
                            </Grid.Column>
                            <Grid.Column width={6}>
                                <ReportActions autoload={autoload}
                                        onAutoloadChange={() => this.handleAutoloadChange(!autoload)}
                                        onDownload={() => this.setState({ isDownloadOpen: true })}
                                        onFilter={() => this.setState({ isFilterOpen: true })} />
                                <ReportSelector active="map" reportTypes={reportTypes} />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <Grid>
                        <Loader active={isLoading} />
                        {!isLoading && gpsPoints.isEmpty() ? placeholder : content}
                    </Grid>
                    <DownloadModal apiBaseUrl={apiBaseUrl} isOpen={isDownloadOpen} onClose={() => this.setState({ isDownloadOpen: false })}/>
                    <Notification show={showNotification}
                        onYesClicked={this.handleReload}
                        onNoClicked={() => this.setState({ showNotification: false })} />
                    <FilterFormContainer isOpen={isFilterOpen} onCloseModal={this.handleFilter} fields={fields} users={users} />
                </Container>
            </React.Fragment>
        );
    }
}

MapContainer.propTypes = {
    apiBaseUrl: PropTypes.string.isRequired,
    baseUrl: PropTypes.string.isRequired,
    breadcrumbSections: PropTypes.array.isRequired,
    fields: PropTypes.array.isRequired,
    reportTypes: PropTypes.arrayOf(PropTypes.shape({ text: PropTypes.string.isRequired, path: PropTypes.string.isRequired })).isRequired,
    resource: PropTypes.object.isRequired,
    resourceType: PropTypes.string.isRequired,
    users: PropTypes.array.isRequired,
};

export default GoogleApiWrapper({
    apiKey: 'AIzaSyCTAKKrUmEVyF0JN2BxmG9KWSKv_7aOpIw',
    version: '3.37'
})(MapContainer)

function mapDataJsonProcessor(response, instance, onSuccess) {
    onSuccess(List(response.gpsPoints));
}

function getBounds(gpsPoints) {
    let bounds = gpsPoints
        .filter(gp => gp && gp.latitude && gp.longitude)
        .reduce((acc, gp) => {
            if (acc.sw && acc.ne) {
                return {
                    sw: {
                        lat: Math.max(acc.sw.lat, gp.latitude),
                        lng: Math.min(acc.sw.lng, gp.longitude)
                    },
                    ne: {
                        lat: Math.min(acc.ne.lat, gp.latitude),
                        lng: Math.max(acc.ne.lng, gp.longitude)
                    }
                };
            } else {
                return {
                    sw: {
                        lat: gp.latitude,
                        lng: gp.longitude
                    },
                    ne: {
                        lat: gp.latitude,
                        lng: gp.longitude
                    }
                };
            }
        }, {});
    return Object.assign({}, {
        sw: { lat: 43.40903821777057, lng: -33.48185394054356 },
        ne: { lat: -42.890625, lng: 80.15625 }
    }, bounds);
}