import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, Message, Modal} from "semantic-ui-react";
import {fetch} from "../network.utils";
import {API_BASE_URL} from "../constants";

class DeleteChoiceGroupModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            errorMessage: null,
        }
        this.handleClose = this.handleClose.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleClose() {
        this.setState({ errorMessage: null });
        this.props.closeModal();
    }

    handleDelete() {
        const {choiceGroup} = this.props;
        const onSuccess = () => {
            this.setState({ errorMessage: null });
            this.props.onDelete();
        };
        const onError = errorMessage => {
            this.setState({ errorMessage })
        }
        fetch({url: `${API_BASE_URL}/groups/${choiceGroup.id}`, method: "delete", onSuccess, onError});
    }

    render() {
        const { errorMessage } = this.state;
        const { choiceGroup, isOpen, } = this.props;
        return (
            <Modal size="tiny" closeOnDimmerClick={false} closeOnEscape={true} onClose={this.handleClose} open={isOpen}>
                <Modal.Header className="ui orange">Delete choice group</Modal.Header>
                <Modal.Content>
                    {errorMessage && <Message compact negative>{errorMessage}</Message>}
                    <p>
                        {'Are you sure you want to delete the choice group '}
                        <strong>{choiceGroup && choiceGroup.name}</strong>
                    </p>
                </Modal.Content>
                <Modal.Actions>
                    <Button className="delete-submit" disabled={!!errorMessage} color="red" onClick={this.handleDelete}>Delete choice group</Button>
                    <Button basic onClick={this.handleClose}>Cancel</Button>
                </Modal.Actions>
            </Modal>
        );
    }
}

DeleteChoiceGroupModal.propTypes = {
    choiceGroup: PropTypes.object,
    closeModal: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onDelete: PropTypes.func.isRequired,
};

export default DeleteChoiceGroupModal;