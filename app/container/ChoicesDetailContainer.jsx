import React, { Component } from "react";
import PropTypes from "prop-types";
import { Breadcrumb, Container, Dropdown, Form, Grid, Header, Input, Segment, Table } from "semantic-ui-react";
import { fetch } from "../network.utils";
import { API_BASE_URL, CONTEXT_PATH } from "../constants";
import GroupedChoicesList from "../components/GroupedChoicesList.jsx";
import ChoicesEditorModal from "./ChoicesEditorModal.jsx";
import ChoiceGroupDeleteModal from "./ChoiceGroupDeleteModal.jsx";
import ChoiceEditorModal from "./ChoiceEditorModal.jsx";
import ChoiceRemoverModal from "./ChoiceRemoverModal.jsx";
import { getOrderingStrategy } from "./ChoicesListContainer.jsx";


const CUSTOM_ORDER = 0;

const getSortParams = by => {
    switch(by) {
        case 0: return "&sort=ordinal,asc";
        case 2: return "&sort=name,asc"; 
        default: return ""; 
    }
}

class ChoicesDetailContainer extends Component {

    state = {
        activeChoice: {},
        animate: false,
        choiceBeforeActive: {},
        choiceGroup: {},
        choices: [],
        currentPage: 0,
        isDeleteModalOpen: false,
        isEditModalOpen: false,
        isEditChoiceModalOpen: false,
        isChoicesLoading: true,
        isGroupLoading: true,
        isRemoveModalOpen: false,
        lastOrdinal: 1,
        searchQuery: '',
        totalPages: 0,
    }

    componentDidMount() {
        this.fetch();
    }

    componentDidUpdate(_, prevState) {
        const { choiceGroup, isGroupLoading } = this.state;
        if (choiceGroup.id && prevState.isGroupLoading && !isGroupLoading) {
            this.fetchChoices(1);
        }
    }

    fetch = () => {
        const onSuccess = choiceGroup => {
            this.setState({ choiceGroup, isGroupLoading: false });
        }
        fetch({ url: `${API_BASE_URL}/groups/${this.props.match.params.id}`, onSuccess });
    }

    fetchChoices = page => {
        const { choiceGroup } = this.state;
        const url = `${API_BASE_URL}/groups/${choiceGroup.id}/choices?page=${page}&size=10${getSortParams(choiceGroup.orderingStrategy)}`
        const onSuccess = pagedChoices => {
            if (pagedChoices.content.length) {
                const lastChoice = pagedChoices.content[pagedChoices.content.length - 1];
                const lastOrdinal = pagedChoices.totalElements >= lastChoice.ordinal ? pagedChoices.totalElements : lastChoice.ordinal;
                this.setState({ choices: pagedChoices.content, currentPage: page, totalPages: pagedChoices.totalPages, lastOrdinal, isChoicesLoading: false, });
            }
            this.setState({ choices: pagedChoices.content, currentPage: page, totalPages: pagedChoices.totalPages, isChoicesLoading: false, });
        }
        fetch({ url, onSuccess });
    }

    closeModal = () => {
        this.setState({
            addOpen: false,
            isDeleteModalOpen: false,
            isEditModalOpen: false,
            isEditChoiceModalOpen: false,
            isRemoveModalOpen: false,
            activeChoice: null,
        });
    }

    handleOpenAddModal = () => {
        this.setState({ addOpen: true });
    }

    handleOpenDeleteModal = () => {
        this.setState({ isDeleteModalOpen: true });
    }

    handleDelete = () => {
        this.props.history.push(`${CONTEXT_PATH}/choice-group`);
    }

    handleOpenEditModal = () => {
        this.setState({ isEditModalOpen: true });
    }

    handleEdit = () => {
        this.setState({ isEditModalOpen: false , isGroupLoading: true });
        this.fetch();
    }

    handleOpenEditChoiceModal = (choice, pos) => {
        const { choices } = this.state;
        const isFirstOrLast = pos === 0 || pos === (choices.length - 1)
        const choiceBeforeActive = isFirstOrLast ? {} : choices[pos - 1];
        this.setState({ animate: true, isEditChoiceModalOpen: true, activeChoice: choice, choiceBeforeActive  });
    }

    handleEditChoice = () => {
        this.fetchChoices(this.state.currentPage);
        this.setState({ activeChoice: {}, choiceBeforeActive: {}, isEditChoiceModalOpen: false, isChoicesLoading: true, })
    }

    handleOpenRemoveModal = (choice) => {
        this.setState({ animate: true, isRemoveModalOpen: true, activeChoice: choice });
    }

    handlePageChange = (page) => {
        this.setState({ animate: false });
        this.fetchChoices(page);
    }

    handleRemove = () => {
        const { choices, currentPage, totalPages } = this.state;
        if (totalPages > 1 && choices.length === 1) {
            this.fetchChoices(currentPage - 1);
        } else {
            this.fetchChoices(currentPage);
        }
        this.setState({ activeChoice: {}, isRemoveModalOpen: false, isChoicesLoading: true, });
    }

    handleCloseModal = () => {
        this.setState({ isDeleteModalOpen: false, isEditModalOpen: false, isEditChoiceModalOpen: false, isRemoveModalOpen: false })
    }
    
    searchChoices = event => {
        event.preventDefault();
        const { searchQuery } = this.state;
        if (!searchQuery) {
            return;
        }
        this.setState({ isChoicesLoading: true });
        const onSuccess = pagedChoices => this.setState({ choices: pagedChoices.content, totalPages: pagedChoices.totalPages, isChoicesLoading: false });
        fetch({ url: `${API_BASE_URL}/groups/${this.state.choiceGroup.id}/choices?query=${searchQuery}`, onSuccess });
    }

    render() {
        const { activeChoice, animate, choiceBeforeActive, choiceGroup, choices, currentPage, isDeleteModalOpen, isEditModalOpen, isEditChoiceModalOpen, isChoicesLoading, isGroupLoading, isRemoveModalOpen, lastOrdinal, searchQuery, totalPages } = this.state;
        const orderingStrategy = getOrderingStrategy(choiceGroup.orderingStrategy);
        const sections = [
            { key: 'choices', content: 'Choices', href: `${contextPath}/choice-group` },
            { key: 'choices-detail', content: choiceGroup.name, active: true },
        ];
        const actionOptions = [
            { key: 'edit', text: 'Edit choice group', onClick: this.handleOpenEditModal },
            { key: 'delete', text: 'Delete choice group', onClick: this.handleOpenDeleteModal },
        ]
        return (
            <Container style={{ marginTop: '1.5rem' }}>
                <Breadcrumb icon='right angle' sections={sections} />
                <Segment vertical>
                    <Grid>
                        <Grid.Column width="sixteen" style={{ marginTop: '1rem' }}>
                            <Header as="h2" className="title">{choiceGroup.name}</Header>
                            <Dropdown basic button className="orange right floated more-actions" text={"More actions"} options={actionOptions} />
                        </Grid.Column>
                    </Grid>
                    <Grid columns="equal">
                        <Grid.Column>
                            <Table basic="very">
                                <Table.Body>
                                    <Table.Row>
                                        <Table.Cell>Ordering strategy</Table.Cell>
                                        <Table.Cell className="ordering-strategy" textAlign="right">{orderingStrategy}</Table.Cell>
                                    </Table.Row>
                                </Table.Body>
                            </Table>
                        </Grid.Column>
                        <Grid.Column width="one" only="computer" />
                        <Grid.Column>
                            <Table basic="very">
                                <Table.Body>
                                    <Table.Row>
                                        <Table.Cell>Anchor last</Table.Cell>
                                        <Table.Cell textAlign="right">{choiceGroup.excludeLast}</Table.Cell>
                                    </Table.Row>
                                </Table.Body>
                            </Table>
                        </Grid.Column>
                    </Grid>
                </Segment>
                <Table basic="very">
                    <Table.Body>
                        <Table.Row>
                            <Table.Cell collapsing>
                                <a className="reset link" onClick={() => this.fetchChoices(1)}>View all choices</a>
                            </Table.Cell>
                            <Table.Cell>
                                <Form onSubmit={this.searchChoices}>
                                    <Input
                                        fluid
                                        value={searchQuery} 
                                        onChange={(e, { value }) => this.setState({ searchQuery: value })} 
                                        action={{ icon: 'search', color: 'orange' }} placeholder='Search...' 
                                    />
                                </Form>
                            </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
                <GroupedChoicesList
                    animate={animate}
                    choices={choices}
                    currentPage={currentPage}
                    isLoading={isChoicesLoading || isGroupLoading}
                    onOpenEdit={this.handleOpenEditChoiceModal}
                    onOpenRemove={this.handleOpenRemoveModal}
                    onPageChange={this.handlePageChange}
                    searchQuery={searchQuery}
                    totalPages={totalPages}
                />
                <ChoicesEditorModal choiceGroup={choiceGroup} closeModal={this.handleCloseModal} isOpen={isEditModalOpen} onEdit={this.handleEdit} />
                <ChoiceGroupDeleteModal choiceGroup={choiceGroup} closeModal={this.handleCloseModal} isOpen={isDeleteModalOpen} onDelete={this.handleDelete} />
                <ChoiceEditorModal anchor={choiceBeforeActive} choice={activeChoice} closeModal={this.handleCloseModal} groupId={choiceGroup.id || 0} lastOrdinal={lastOrdinal} isOpen={isEditChoiceModalOpen} onEdit={this.handleEditChoice} showPosition={choiceGroup.orderingStrategy === CUSTOM_ORDER} />
                <ChoiceRemoverModal choice={activeChoice} choiceGroup={choiceGroup} closeModal={this.handleCloseModal} isOpen={isRemoveModalOpen} onRemove={this.handleRemove} />
            </Container>
        )
    }
}

ChoicesDetailContainer.propTypes = {
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
}

export default ChoicesDetailContainer;