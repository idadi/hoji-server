import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _function from 'lodash/function';
import { List } from 'immutable';
import striptags from 'striptags';
import { Container, Grid, Header, Loader, Segment, Table } from "semantic-ui-react";
import { fetch, setupSocket, closeSocket } from '../network.utils';
import { getFilters, getAutoloadSetting, saveAutoloadSetting } from '../store.util';

import DownloadModal from './DownloadModal.jsx';
import FilterFormContainer from './FilterFormContainer.jsx';

import GoogleTable from '../components/GoogleTable.jsx';
import Notification from '../components/NewDataNotification.jsx';
import ReportActions from '../components/ReportActions.jsx';
import ReportSelector from '../components/ReportSelector.jsx';
import SecondaryNav from '../components/SecondaryNav.jsx';

const moment = require('moment');

class GoogleTableContainer extends Component {

    constructor(props) {
        super(props);
        const autoload = getAutoloadSetting();
        this.state = {
            autoload,
            tableDataWithoutUuids: List(),
            isDownloadOpen: false,
            isFilterOpen: false,
            isLoading: true,
            showNotification: false,
            visible: true,
        };
        this.queuedData = List();

        this.handleAutoloadChange = this.handleAutoloadChange.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.handleReload = this.handleReload.bind(this);
        this.handleRowClick = this.handleRowClick.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.initSocket = this.initSocket.bind(this);
    }

    componentDidMount() {
        this.fetchData();
        this.initSocket();
    }

    componentWillUnmount() {
        closeSocket(this.stompClient);
    }

    handleAutoloadChange(autoload) {
        saveAutoloadSetting(autoload)
        this.setState({ autoload });
    }

    handleFilter(updated) {
        this.setState({ isFilterOpen: false });
        if (updated) {
            this.fetchData();
        }
    }

    handleReload() {
        this.fetchData();
        this.setState({ showNotification: false });
    }

    handleRowClick(chartComponent) {
        const selection = chartComponent.chartWrapper.getChart().getSelection()[0].row;
        const rowId = chartComponent.chartWrapper.getDataTable().getValue(selection, 0);
        const uuid = this.state.uuidToId.find(entry => entry.id === rowId).uuid;
        if (uuid) {
            window.location = `${this.props.baseUrl.replace('report', '')}record?recordUuid=${uuid}`;
        }
    }

    fetchData() {
        this.setState({ isLoading: true });
        const filters = getFilters();
        filters.id = this.props.resource.id;
        const url = `${this.props.apiBaseUrl}/data?${toQueryString(filters)}`;
        const jsonProcessor = _function.partialRight(
            tableDataJsonProcessor, this.props.resourceType,
            ({ uuidToId, tableDataWithoutUuids }) => {
                this.setState({ tableDataWithoutUuids, uuidToId, isLoading: false });
            });
        fetch({ url, onSuccess: jsonProcessor });
    }

    initSocket() {
        this.stompClient = setupSocket(`/topic/${this.props.resourceType}/${this.props.resource.id}/report/table`, () => {
            if (this.state.autoload) {
                this.fetchData()
            } else {
                this.setState({ showNotification: true });
            }
        });
    }

    render() {
        const { apiBaseUrl, breadcrumbSections, resource, resourceType, users } = this.props;
        const { autoload, isDownloadOpen, isFilterOpen, isLoading, showNotification, tableDataWithoutUuids, } = this.state;
        const choiceValueTypeOptions = ['Labels', 'Codes'].map(valueType =>
            ({ key: valueType.toLowerCase(), value: valueType.toLowerCase(), text: valueType })
        );
        const placeholder = (
            <Grid.Row>
                <Grid.Column>
                    <Segment placeholder>
                        <Header icon>
                            {'No data has been submited for this form.'}
                        </Header>
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        );
        const content = (
            <Grid.Row>
                <Grid.Column>
                    <Segment attached>
                        <GoogleTable data={tableDataWithoutUuids} showRowNumber={false} pageSize={20}
                            height={'100%'} chartEvents={[{ eventName: 'select', callback: this.handleRowClick }]} />
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        );
        const header = resourceType === "form" ? resource.name : striptags(resource.description);
        return (
            <React.Fragment>
                <SecondaryNav sections={[...breadcrumbSections, { key: "table", content: "Data table", active: true },]}/>
                <Container style={{ marginTop: '3rem' }}>
                    <Grid style={{ marginTop: '1rem' }}>
                        <Grid.Row>
                            <Grid.Column width={10}>
                                <Table basic="very" fixed singleLine>
                                    <Table.Body>
                                        <Table.Row>
                                            <Table.Cell className="compact">
                                                <Header as="h2">{header}</Header>
                                            </Table.Cell>
                                        </Table.Row>
                                    </Table.Body>
                                </Table>
                            </Grid.Column>
                            <Grid.Column width={6}>
                                <ReportActions
                                        autoload={autoload}
                                        onAutoloadChange={() => this.handleAutoloadChange(!autoload)}
                                        onDownload={() => this.setState({ isDownloadOpen: true })}
                                        onFilter={() => this.setState({ isFilterOpen: true })} />
                                <ReportSelector active="table" reportTypes={reportTypes} />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <Grid>
                        <Loader active={isLoading} />
                        {!isLoading && tableDataWithoutUuids.isEmpty() ? placeholder : content}
                    </Grid>
                    <DownloadModal apiBaseUrl={apiBaseUrl} isOpen={isDownloadOpen} onClose={() => this.setState({ isDownloadOpen: false })}/>
                    <Notification show={showNotification}
                        onYesClicked={this.handleReload}
                        onNoClicked={() => this.setState({ showNotification: false })} />
                    <FilterFormContainer isOpen={isFilterOpen} onCloseModal={this.handleFilter} fields={fields} users={users} />
                </Container>
            </React.Fragment>
        )
    }
}

GoogleTableContainer.propTypes = {
    apiBaseUrl: PropTypes.string.isRequired,
    baseUrl: PropTypes.string.isRequired,
    breadcrumbSections: PropTypes.array.isRequired,
    fields: PropTypes.array.isRequired,
    history: PropTypes.object.isRequired,
    reportTypes: PropTypes.arrayOf(PropTypes.shape({ text: PropTypes.string.isRequired, path: PropTypes.string.isRequired })).isRequired,
    resource: PropTypes.object.isRequired,
    resourceType: PropTypes.string.isRequired,
    users: PropTypes.array.isRequired,
};

export default GoogleTableContainer;

function tableDataJsonProcessor(response, resourceType, onSuccess) {
    onSuccess(setupState(response.dataRows, resourceType));
}

function translateRows(dataRow) {
    return List().concat(dataRow.dataElements).map(dataElement => {
        if (dataElement.type === 'datetime' || dataElement.type === 'date') {
            return Object.assign(dataElement, {
                value: dataElement.value ? moment(dataElement.value, dataElement.format || 'YYYY-MM-DD HH:mm:ss').toDate() : null,
                format: (dataElement.format ? moment(dataElement.value).format(dataElement.format) : null)
            });
        } else if (dataElement.type === 'time') {
            return Object.assign(dataElement, {
                value: dataElement.value ? dataElement.value.split(":") : null,
                format: null
            });
        } else if (dataElement.type === 'number') {
            return Object.assign(dataElement, {
                value: dataElement.value ? Number(dataElement.value) : null,
                format: (dataElement.format ? dataElement.format : null)
            });
        } else {
            return Object.assign(dataElement, {
                format: (dataElement.format ? dataElement.format : null)
            });
        }
    });
}

function layOutTableData(acc, dataRow) {
    let header = List();
    let body = List();
    dataRow.forEach(dataElement => {
        const id = dataElement.label.trim().replace(/[()'"]/g, '').replace(/ /g, '_');
        const label = dataElement.label.trim();
        const type = dataElement.type === 'time' ? 'timeofday' : dataElement.type;
        if (acc.isEmpty()) {
            header = header.push({ id, label, type });
        }
        body = body.push({ v: dataElement.value, f: dataElement.format });
    });
    !header.isEmpty() && (acc = acc.push(header));
    acc = acc.push(body);
    return acc;
}

function setupState(dataRows, resourceType) {
    const tableData = dataRows.map(translateRows).reduce(layOutTableData, List());
    if (resourceType === 'form') {
        const uuidToId = tableData.skip(1).map(tableRow => {
            return { uuid: tableRow.get(0).v, id: tableRow.get(1).v }
        });
        const tableDataWithoutUuids = tableData.map(tableRow => tableRow.skip(1));
        return { uuidToId, tableDataWithoutUuids };
    } else {
        const uuidToId = tableData.skip(1).map(tableRow => {
            return { uuid: tableRow.get(1).v, id: tableRow.get(2).v }
        });
        const tableDataWithoutUuids = tableData.map(tableRow => tableRow.skip(2));
        return { uuidToId, tableDataWithoutUuids };
    }
}