import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _function from 'lodash/function';
import { fromJS, List } from 'immutable';
import striptags from 'striptags';
import { Container, Grid, Header, Loader, Segment, Table } from "semantic-ui-react";
import { fetch, setupSocket, closeSocket } from "../network.utils";
import { getFilters, saveAutoloadSetting, getAutoloadSetting } from '../store.util';

import DownloadModal from './DownloadModal.jsx';
import FilterFormContainer from './FilterFormContainer.jsx';

import BasicVisualization from '../components/BasicVisualization.jsx'
import Notification from '../components/NewDataNotification.jsx';
import ReportActions from '../components/ReportActions.jsx';
import ReportSelector from '../components/ReportSelector.jsx';
import SecondaryNav from '../components/SecondaryNav.jsx';

class MonitorContainer extends Component {
    constructor(props) {
        super(props);
        const autoload = getAutoloadSetting();
        this.state = {
            autoload,
            charts: List(),
            isLoading: true,
            isFilterOpen: false,
            showNotification: false,
        };

        this.handleAutoloadChange = this.handleAutoloadChange.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.handleReload = this.handleReload.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.initSocket = this.initSocket.bind(this);
    }

    componentDidMount() {
        this.fetchData();
        this.initSocket();
    }

    componentWillUnmount() {
        closeSocket(this.stompClient);
    }

    handleAutoloadChange(autoload) {
        saveAutoloadSetting(autoload)
        this.setState({ autoload });
    }

    handleFilter(updated) {
        this.setState({ isFilterOpen: false });
        if (updated) {
            this.fetchData();
        }
    }

    handleReload() {
        this.fetchData();
        this.setState({ showNotification: false });
    }

    fetchData() {
        this.setState({ isLoading: true });
        const filters = getFilters();
        filters.id = this.props.resource.id;
        const url = `${this.props.apiBaseUrl}/monitor?${toQueryString(filters)}`;
        const jsonProcessor = _function.partialRight(
            chartDataJsonProcessor,
            (newCharts) => {
                this.setState({ charts: newCharts, isLoading: false });
            });
        fetch({ url, onSuccess: jsonProcessor });
    }

    initSocket() {
        this.stompClient = setupSocket(`/topic/${this.props.resourceType}/${this.props.resource.id}/report/monitor`,
            () => {
                if (this.state.autoload) {
                    this.fetchData();
                } else {
                    this.setState({ showNotification: true });
                }
            });
    }

    render() {
        const { apiBaseUrl, breadcrumbSections, resource, resourceType, users } = this.props;
        const { autoload, charts, isLoading, isFilterOpen, isDownloadOpen, showNotification, } = this.state;
        const isEmpty = charts.isEmpty() || charts.get(0).get('sampleSize') === 0;
        const placeholder = (
            <Grid.Row>
                <Grid.Column>
                    <Segment placeholder>
                        <Header icon>
                            {'No data has been submited for this form.'}
                        </Header>
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        );
        const content = (
            <BasicVisualization charts={charts} />
        );
        const header = resourceType === "form" ? resource.name : striptags(resource.description);
        return (
            <React.Fragment>
                <SecondaryNav sections={[...breadcrumbSections, { key: "monitor", content: "Activity monitor", active: true },]}/>
                <Container style={{ marginTop: '3rem' }}>
                    <Grid style={{ marginTop: '1rem' }} >
                        <Grid.Row>
                            <Grid.Column width={10}>
                                <Table basic="very" fixed singleLine>
                                    <Table.Body>
                                        <Table.Row>
                                            <Table.Cell className="compact">
                                                <Header as="h2">{header}</Header>
                                            </Table.Cell>
                                        </Table.Row>
                                    </Table.Body>
                                </Table>
                            </Grid.Column>
                            <Grid.Column width={6}>
                                <ReportActions autoload={autoload}
                                        onAutoloadChange={() => this.handleAutoloadChange(!autoload)}
                                        onDownload={() => this.setState({ isDownloadOpen: true })}
                                        onFilter={() => this.setState({ isFilterOpen: true })} />
                                <ReportSelector active="monitor" reportTypes={reportTypes} />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <Grid>
                        <Loader active={isLoading} />
                        {!isLoading && isEmpty ? placeholder : content}
                    </Grid>
                    <DownloadModal apiBaseUrl={apiBaseUrl} isOpen={isDownloadOpen} onClose={() => this.setState({ isDownloadOpen: false })}/>
                    <Notification show={showNotification}
                        onYesClicked={this.handleReload}
                        onNoClicked={() => this.setState({ showNotification: false })} />
                    <FilterFormContainer isOpen={isFilterOpen} onCloseModal={this.handleFilter} fields={fields} users={users} />
                </Container>
            </React.Fragment>
        );
    }
}

MonitorContainer.propTypes = {
    apiBaseUrl: PropTypes.string.isRequired,
    breadcrumbSections: PropTypes.array.isRequired,
    fields: PropTypes.array.isRequired,
    reportTypes: PropTypes.arrayOf(PropTypes.shape({ text: PropTypes.string.isRequired, path: PropTypes.string.isRequired })).isRequired,
    resource: PropTypes.object.isRequired,
    resourceType: PropTypes.string.isRequired,
    users: PropTypes.array.isRequired,
};

export default MonitorContainer;

export function chartDataJsonProcessor(response, onSuccess) {
    const newCharts = response.sort((chartA, chartB) => chartA.ordinal - chartB.ordinal);
    onSuccess(fromJS(newCharts));
}
