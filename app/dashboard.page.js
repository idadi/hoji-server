import React from 'react';
import ReactDom from 'react-dom';
import DashboardComponentsContainer from './container/DashboardComponentsContainer.jsx';
import {API_BASE_URL, } from "./constants"

import './lib/hoji.css';

const form = window.form;
const apiBaseUrl = `${API_BASE_URL}/project/${form.survey.id}`;

ReactDom.render(
    <DashboardComponentsContainer apiBaseUrl={apiBaseUrl} form={form} />,
    document.getElementById('visualizations-container')
);