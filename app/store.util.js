import _object from 'lodash/object';
import {VIEW_MODES} from "./constants";

const AUTOLOAD_KEY = 'autoload';
const DEFAULT_AUTOLOAD_VALUE = true;
const VIEW_MODE_KEY = 'view_mode';
const FILTERS_KEY = 'filters';

export function getAutoloadSetting() {
    return window.localStorage.getItem(AUTOLOAD_KEY) ? window.localStorage.getItem(AUTOLOAD_KEY) === 'true' : DEFAULT_AUTOLOAD_VALUE;
}

export function saveAutoloadSetting(setting) {
    window.localStorage.setItem(AUTOLOAD_KEY, setting);
}

export function getFilters() {
    const filters = _object.merge(
        { selectedUsers: [], selectedField: '', selectedChoice: '', completed: 'Whenever', uploaded: 'Whenever', testMode: false },
        JSON.parse(window.sessionStorage.getItem(FILTERS_KEY)),
    );
    return filters;
}

export function saveFilters(filters) {
    window.sessionStorage.setItem(FILTERS_KEY, JSON.stringify(filters));
}

export function getViewModeSetting() {
    return window.localStorage.getItem(VIEW_MODE_KEY) ? window.localStorage.getItem(VIEW_MODE_KEY) : VIEW_MODES.LIST;
}

export function saveViewModeSetting(viewMode) {
    window.localStorage.setItem(VIEW_MODE_KEY, viewMode);
}