import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
import ScrollToTop from './components/ScrollToTop.jsx';
import _string from 'lodash/string';
import striptags from 'striptags';
import AnalysisContainer from './container/AnalysisContainer.jsx';
import DashboardContainer from './container/DashboardContainer.jsx';
import EvolutionContainer from "./container/EvolutionContainer.jsx";
import GalleryContainer from "./container/GalleryContainer.jsx";
import MonitorContainer from './container/MonitorContainer.jsx';
import MapContainer from './container/MapContainer.jsx';
import MainNav from './components/MainNav.jsx';
import SecondaryNav from "./components/SecondaryNav.jsx";
import PivotContainer from './container/PivotContainer.jsx';
import TableContainer from './container/GoogleTableContainer.jsx';

import './lib/print.css';
import './lib/hoji.css';

class App extends Component {

    render() {
        return (
            <Router>
                <MainNav anonymousUser={this.props.anonymousUser} canfly={this.props.canfly} contextPath={this.props.contextPath} fullName={this.props.fullName} />
                <ScrollToTop>
                    <Route exact path={`${this.props.baseUrl}/analysis`} render={props =>
                        <AnalysisContainer {...this.props} {...props} />
                    } />
                    <Route exact path={`${this.props.baseUrl}/monitor`} render={props =>
                        <MonitorContainer {...this.props} {...props} />
                    } />
                    <Route exact path={`${this.props.baseUrl}/table`} render={props =>
                        <TableContainer {...this.props} {...props} />
                    } />
                    <Route exact path={`${this.props.baseUrl}/map`} render={props =>
                        <MapContainer {...this.props} {...props} />
                    } />
                    <Route exact path={`${this.props.baseUrl}/pivot`} render={props =>
                        <PivotContainer {...this.props} {...props} />
                    } />
                    <Route exact path={`${this.props.baseUrl}/evolution`} render={props =>
                        <EvolutionContainer {...this.props} {...props} />
                    } />
                    <Route exact path={`${this.props.baseUrl}/gallery`} render={props =>
                        <GalleryContainer {...this.props} {...props} />
                    } />
                    <Route exact path={`${this.props.baseUrl}/dashboard`} render={props =>
                        <DashboardContainer {...this.props} {...props} />
                    } />
                </ScrollToTop>
            </Router>
        )
    }
}

App.propTypes = {
    anonymousUser: PropTypes.bool.isRequired,
    apiBaseUrl: PropTypes.string.isRequired,
    breadcrumbSections: PropTypes.array.isRequired,
    baseUrl: PropTypes.string.isRequired,
    canfly: PropTypes.bool.isRequired,
    contextPath: PropTypes.string.isRequired,
    fields: PropTypes.array.isRequired,
    reportTypes: PropTypes.arrayOf(PropTypes.shape({ text: PropTypes.string.isRequired, path: PropTypes.string.isRequired })).isRequired,
    resource: PropTypes.object.isRequired,
    resourceType: PropTypes.string.isRequired,
    users: PropTypes.array.isRequired,
}

const breadcrumbSections = window.breadcrumbSections.map(bc => {
    bc.content = _string.capitalize((striptags(bc.content)));
    return bc;
})

const props = {
    anonymousUser: window.anonymousUser,
    apiBaseUrl: window.apiBaseUrl,
    breadcrumbSections: breadcrumbSections.map(bc => <a key={bc.key} href={`${window.contextPath}${bc.href}`}>{bc.content}</a>),
    baseUrl: window.baseUrl,
    canfly: window.canfly,
    contextPath: window.contextPath,
    fields: window.fields,
    fullName: window.fullName,
    users: window.users,
    reportTypes: window.reportTypes,
    resource: window.resource,
    resourceType: window.resourceType,
}

ReactDOM.render(
    <App {...props} />,
    document.getElementById('report-container')
);
