export const CONTEXT_PATH = `${window.contextPath}`;
export const API_BASE_URL = `${window.contextPath}/api`;
export const VIEW_MODES = { LIST: 'list', GRID: 'grid' };