import _fetch from 'isomorphic-fetch';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';

export function status(response) {
    if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response)
    } else {
        return Promise.reject(response)
    }
}

export function json(response) {
    return response.json()
}

export function error(response) {
    if (!response.status) {
        return;
    }
    if (response.status === 401) {
        window.location.reload();
    } else {
        window.location.href = `/error?statusCode=${response.status}`
    }
}

const NOOP = () => { };

export function fetch({ url, method = "get", headers = {}, data, onSuccess = NOOP, onError = NOOP }) {
    _fetch(url, {
        method,
        headers: Object.assign({
            'Accept': 'application/json',
            [window.header]: window.token
        }, headers),
        body: data,
        credentials: "include"
    })
        .then(status)
        .then(json)
        .then(result => {
            if (result.success) {
                onSuccess(result.value);
            } else {
                onError(result.value);
            }
        })
        .catch(error);
}

export function setupSocket(topicUrl, messageProcessor) {
    const socket = new SockJS(window.contextPath + '/hoji-websocket');
    const stompClient = Stomp.over(socket);
    stompClient.connect({}, (frame) => {
        stompClient.subscribe(topicUrl, (message) => {
            messageProcessor(message.body)
        });
    }, (message) => {
        console.dir(message);
    });
    return stompClient;
}

export function closeSocket(stompClient) {
    stompClient && stompClient.disconnect(() => {
        //No need to reload page if user is only moving between different report types.
        //Assumption: report urls contains "/report/" and no other url does. 
        if (!window.location.href.includes("/report/")) {
            window.location.reload();
        }
    });
}
