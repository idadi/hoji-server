import React from 'react';
import PropTypes from 'prop-types';
import { Dropdown } from 'semantic-ui-react';

const ReportActions = ({ actions, autoload, onDownload, onAutoloadChange, onFilter }) => {
    let additionalActions;
    if (actions) {
        additionalActions = (
            <React.Fragment>
                {actions.map((action, index) => <Dropdown.Item key={index} onClick={action.onClick} content={action.label} />)}
                <Dropdown.Divider/>
            </React.Fragment>
        );
    }
    return (
        <Dropdown basic button text="Actions" className="right floated">
            <Dropdown.Menu>
                {additionalActions}
                <Dropdown.Item onClick={onFilter} content='Filter'/>
                <Dropdown.Item onClick={onAutoloadChange}
                    content={autoload ? 'Turn off auto-refresh' : 'Turn on auto-refresh'}/>
                <Dropdown.Item onClick={onDownload} content='Download data'/>
            </Dropdown.Menu>
        </Dropdown>
    );
}

ReportActions.propTypes = {
    actions: PropTypes.arrayOf(PropTypes.shape({ label: PropTypes.string.isRequired, onClick: PropTypes.func.isRequired })),
    autoload: PropTypes.bool.isRequired,
    onAutoloadChange: PropTypes.func.isRequired,
    onDownload: PropTypes.func.isRequired,
    onFilter: PropTypes.func.isRequired,
}

export default ReportActions;