import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { Chart } from 'react-google-charts';
import {Loader} from "semantic-ui-react";

class GoogleTable extends Component {
    render() {
        const options = {
            cssClassNames : {
                tableCell: "nowrap"
            },
            showRowNumber: this.props.showRowNumber,
            sort: this.props.sort,
            sortAscending: this.props.sortAscending,
            sortColumn: this.props.sortColumn,
            page: this.props.page,
            pageSize: this.props.pageSize,
            width: '100%',
            height: '100%'
        };
        const dataProps = this.props.data ? {data: this.props.data.toJS()} :
            {columns: this.props.columns, rows: this.props.rows};
        return (
            <Chart
                chartType="Table"
                {...dataProps}
                loader={<Loader />}
                options={options}
                height={this.props.height}
                width="100%"
                chartPackages={["controls"]}
                chartEvents={this.props.chartEvents}
            />
        )
    }
}

GoogleTable.propTypes = {
    showRowNumber: PropTypes.bool.isRequired,
    sort: PropTypes.string,
    sortAscending: PropTypes.bool,
    sortColumn: PropTypes.number,
    page: PropTypes.string,
    pageSize: PropTypes.number,
    height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    columns: PropTypes.array,
    rows: PropTypes.array,
    data: PropTypes.instanceOf(List),
    chartEvents: PropTypes.array
};

export default GoogleTable
