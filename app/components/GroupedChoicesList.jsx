import React, { Component } from "react";
import PropTypes from "prop-types";
import { Grid, Icon, Loader, Menu, Segment, Table, Transition } from "semantic-ui-react";

class GroupedChoicesList extends Component {

    render() {
        const { animate, choices, currentPage, isLoading, onOpenEdit, onOpenRemove, onPageChange,
            searchQuery, totalPages, } = this.props;
        let content = (
            <React.Fragment>
                {choices.map((choice, index) => {
                    const content = (
                        <React.Fragment>
                            <Table.Cell>
                                <Grid columns="equal">
                                    <Grid.Row stretched>
                                        <Grid.Column>
                                            <div><strong>{choice.name}</strong></div>
                                            <div className="resource detail">
                                                <span>
                                                    <strong>ID: </strong>
                                                    {` ${choice.id}`}
                                                </span>
                                                <span>
                                                    {"; "}
                                                    <strong>CODE: </strong>
                                                    {` ${choice.code}`}
                                                </span>
                                                <span>   
                                                    {"; "}
                                                    <strong>SCALE: </strong>
                                                    {` ${choice.scale}`}
                                                </span>
                                                {choice.parent &&
                                                    <span>
                                                        {"; "}
                                                        <strong>PARENT: </strong>
                                                        {` ${choice.parent.name}`}
                                                    </span>
                                                }
                                            </div>
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Table.Cell>
                            <Table.Cell collapsing>
                                <Icon className="edit" name="edit" color="black" link onClick={() => onOpenEdit(choice, index)} />
                                <Icon className="remove" name="remove circle" color="black" link onClick={() => onOpenRemove(choice)} />
                            </Table.Cell>
                        </React.Fragment>
                    );
                    return <Table.Row key={choice.id}>{content}</Table.Row>;
                })}
            </React.Fragment>
        );
        if (searchQuery) {
            if (choices.length === 0) {
                content = <Table.Row key={'empty-table'}><Table.Cell colSpan={4}>{isLoading ?
                    'Loading...' :
                    <span>No choices found matching: <strong>{searchQuery}</strong></span>}
                </Table.Cell></Table.Row>;
            }
        } else {
            if (choices.length === 0) {
                content = <Table.Row key={'empty-table'}><Table.Cell colSpan={4}>{isLoading ? 'Loading...' : 'No choices under this group.'}</Table.Cell></Table.Row>;
            }
        }
        let pageLinks = null;
        if (totalPages > 1 && totalPages <= 10) {
            pageLinks = (
                <Menu pagination>
                    {[...Array(totalPages).keys()].map(page => (
                        <Menu.Item
                            key={`${page + 1}`}
                            name={`${page + 1}`}
                            active={currentPage === page + 1}
                            onClick={() => onPageChange(page + 1)}
                        />
                    ))}
                </Menu>
            );
        }
        if (totalPages > 10) {
            let pages = [];
            if (currentPage > 5 && currentPage < totalPages - 4) {
                pages = [1, '...', currentPage - 2, currentPage - 1, currentPage, currentPage + 1, currentPage + 2 , currentPage + 3, '...', totalPages]
            }
            if (currentPage <= 5) {
                pages = [1, 2, 3, 4, 5, 6, 7, 8, '...', totalPages]
            }
            if (currentPage >= totalPages - 4) {
                pages = [1, '...', totalPages - 7, totalPages - 6, totalPages - 5, totalPages - 4, totalPages - 3, totalPages - 2, totalPages - 1, totalPages]
            }
            pageLinks = (
                <Menu pagination>
                    {pages.map((page, index) => {
                        if (page === '...') {
                            return <Menu.Item key={`disabled-${index}`} disabled>{page}</Menu.Item>
                        }
                        return <Menu.Item
                            key={`${page}`}
                            name={`${page}`}
                            active={currentPage === page}
                            onClick={() => onPageChange(page)}
                        />
                    })}
                </Menu>
            );
        }

        return (
            <Segment basic style={{ padding: 0 }}>
                <Loader active={isLoading} />
                <Table className="choice" compact striped>
                    <Table.Body>
                        {animate
                        ? <Transition.Group animation="glow">{content}</Transition.Group>
                        : content
                        }
                    </Table.Body>
                </Table>
                {pageLinks}
            </Segment>
        );
    }

};

GroupedChoicesList.propTypes = {
    animate: PropTypes.bool.isRequired,
    choices: PropTypes.array.isRequired,
    currentPage: PropTypes.number,
    isLoading: PropTypes.bool.isRequired,
    onOpenEdit: PropTypes.func.isRequired,
    onOpenRemove: PropTypes.func.isRequired,
    onPageChange: PropTypes.func,
    searchQuery: PropTypes.string,
    totalPages: PropTypes.number,
};

export default GroupedChoicesList;