// Chart visualization constants
export const BINARY = 0;
export const CATEGORICAL = 1;
export const TEXTUAL = 3;
export const MAX_BARS_IN_BAR_CHART = 10;

export const ItemTypes = {
    DRAGGABLE_ROW: 'draggable_row'
};