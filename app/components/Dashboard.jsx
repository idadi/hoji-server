import React, { Component } from "react";
import PropTypes from "prop-types";
import { List } from "immutable";
import { DndProvider } from "react-dnd";
import HTML5Backend from 'react-dnd-html5-backend';
import { Grid, Transition } from "semantic-ui-react";
import DashboardControl from "./DashboardControl.jsx";

class Dashboard extends Component {

    shouldComponentUpdate(nextProps) {
        return this.props.viewMode !== nextProps.viewMode || !this.props.components.equals(nextProps.components) || !this.props.flexData.equals(nextProps.flexData);
    }

    render() {
        const {components, defaultColumn, flexData, sortComponent, sortingDone, onDuplicate, onOpenDeleteModal, onOpenEditModal, viewMode } = this.props;
        return (
            <DndProvider backend={HTML5Backend}>
                <Transition.Group as={Grid.Row}>
                    {
                        components.map((component, index) => (
                            <DashboardControl
                                key={component.id + '_container'}
                                defaultColumn={defaultColumn}
                                flexData={flexData}
                                index={index}
                                name={component.name}
                                report={component.configuration}
                                sortComponent={sortComponent}
                                sortingDone={sortingDone}
                                onDuplicate={() => onDuplicate(component.id)}
                                onOpenDeleteModal={() => onOpenDeleteModal(component.id)}
                                onOpenEditModal={() => onOpenEditModal(component.id)}
                                viewMode={viewMode}
                            />
                        ))
                    }
                </Transition.Group>
            </DndProvider>
        );
    }
}

Dashboard.propTypes = {
    components: PropTypes.instanceOf(List).isRequired,
    defaultColumn: PropTypes.string,
    flexData: PropTypes.instanceOf(List).isRequired,
    sortComponent: PropTypes.func.isRequired,
    sortingDone: PropTypes.func.isRequired,
    onDuplicate: PropTypes.func.isRequired,
    onOpenEditModal: PropTypes.func.isRequired,
    onOpenDeleteModal: PropTypes.func.isRequired,
    viewMode: PropTypes.string.isRequired,
}

export default Dashboard;