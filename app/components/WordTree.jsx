import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Chart } from 'react-google-charts';
import { Loader } from 'semantic-ui-react';

class WordTree extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const words = this.props.chartData.get('data')
            .flatMap(dp => dp.get('value').split(/\s/))
            // remove empty/blank
            .filter(word => word);
        const wordFrequency =
            words.reduce((map, word) =>
                Object.assign(map, {
                    [word]: (map[word])
                        ? map[word] + 1
                        : 1,
                }),
                {}
            );
        let mostFrequentWord = "";
        let highestFrequency = 0;
        for (const word in wordFrequency) {
            if (wordFrequency[word] > highestFrequency) {
                mostFrequentWord = word;
                highestFrequency = wordFrequency[word]
            }
        }
        if (highestFrequency < 2) {
            return <div style={{ height: "100px" }}>{'Not enough data to analyze'}</div>;
        }
        const options = {
            wordtree: {
                format: "implicit",
                type: "double",
                word: mostFrequentWord
            }
        };
        const chartData = this.props.chartData.get('data')
            .filter(dp => dp.get('value'))
            .map(dp => [dp.get('value')])
            .unshift(['Phrases']);
        return (
            <Chart
                chartType="WordTree"
                data={chartData.toJS()}
                loader={<Loader active />}
                options={options}
                height="250px"
                width="100%"
                chartPackages={["controls", "wordtree"]}
            />
        )
    }
}

WordTree.propTypes = {
    chartData: PropTypes.object.isRequired
};

export default WordTree
