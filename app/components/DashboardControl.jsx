import React from "react";
import PropTypes from "prop-types";
import { useDrop, useDrag } from "react-dnd";
import _object from 'lodash/object';
import { List } from "immutable";
import { Header, Icon, Segment, Table, } from 'semantic-ui-react';
import { VIEW_MODES } from '../constants';
import Flexmonster from './Flexmonster.jsx';

const DashboardControl = ({ defaultColumn, flexData, index, isLoading, name, report, extraReportConfig, sortComponent, sortingDone, onDuplicate, onOpenEditModal, onOpenDeleteModal, viewMode, }) => {
    let colSize;
    let style = {};
    if (viewMode === VIEW_MODES.LIST) {
        colSize = "sixteen";
    } else {
        style = { marginBottom: '2rem' };
        colSize = "eight";
    }
    let ref;
    const [{ isDragging }, drag, preview] = useDrag({
        item: { type: 'component', index },
        end(item, monitor) {
            if (monitor.didDrop()) {
                sortingDone();
            }
        },
        collect: monitor => ({
            isDragging: monitor.isDragging(),
        }),
    });
    const [, drop] = useDrop({
        accept: 'component',
        hover(item, monitor) {
            const dragIndex = item.index;
            const hoverIndex = index;
            if (dragIndex === hoverIndex) {
                return; // Don't replace items with themselves
            }
            // Time to actually perform the action
            sortComponent(dragIndex, hoverIndex)
            // Note: we're mutating the monitor item here!
            // Generally it's better to avoid mutations,
            // but it's good here for the sake of performance
            // to avoid expensive index searches.
            item.index = hoverIndex
        }
    });
    const opacity = isDragging ? 0.5 : 1;
    return (
        <div className={`${colSize} wide column block`} ref={node => ref = drag(drop(node))} width={colSize} style={{ ...style, opacity }}>
            <Header attached="top" as="h5">
                <table className="ui very basic single line table" ref={preview}>
                    <Table.Body>
                        <Table.Row>
                            <Table.Cell className="compact">
                                <Table basic="very" singleLine>
                                    <Table.Body>
                                        <Table.Row>
                                            <Table.Cell className="compact">
                                                {name}
                                            </Table.Cell>
                                        </Table.Row>
                                    </Table.Body>
                                </Table>
                            </Table.Cell>
                            <Table.Cell className="compact" collapsing>
                                <Icon name="edit" className="edit-component" color="black" link onClick={onOpenEditModal} />
                                <Icon name="copy" className="duplicate-component" color="black" link onClick={onDuplicate} />
                                <Icon name="trash" className="delete-component" color="black" link onClick={onOpenDeleteModal} />
                            </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </table>
            </Header>
            <Segment attached >
                <Flexmonster height={400}
                    toolbar={false}
                    flexData={flexData}
                    defaultColumn={defaultColumn}
                    report={report}
                />
            </Segment>
        </div>
    );
};

DashboardControl.propTypes = {
    defaultColumn: PropTypes.string,
    flexData: PropTypes.instanceOf(List).isRequired,
    index: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    report: PropTypes.object.isRequired,
    sortComponent: PropTypes.func.isRequired,
    sortingDone: PropTypes.func.isRequired,
    onDuplicate: PropTypes.func.isRequired,
    onOpenEditModal: PropTypes.func.isRequired,
    onOpenDeleteModal: PropTypes.func.isRequired,
    viewMode: PropTypes.string.isRequired,
}

export default DashboardControl;