import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { Dimmer, Loader } from 'semantic-ui-react';
import _lang from 'lodash/lang';
import _object from 'lodash/object';
import Pivot from './Pivot.jsx';

const moment = require('moment');

class Flexmonster extends Component {

    constructor(props) {
        super(props);
        this.state = {
            flexmonster: null,
            showLoader: true,
        };

        this.getCurrentReport = this.getCurrentReport.bind(this);
        this.handleReady = this.handleReady.bind(this);
        this.updateFlexmonsterConditionals = this.updateFlexmonsterConditionals.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        const loaderStateChanged = this.state.showLoader !== nextState.showLoader;
        const reportConfigChanged = this.props.report !== nextProps.report;
        const dataChanged = !this.props.flexData.equals(nextProps.flexData);
        const flexmonsterSetup = this.state.flexmonster !== nextState.flexmonster;
        return loaderStateChanged || reportConfigChanged || dataChanged || flexmonsterSetup;
    }

    componentDidUpdate(prevProps, prevState) {
        if (_lang.isNull(this.state.flexmonster)) {
            return;
        }
        if (!prevState.showLoader) {
            this.setState({ showLoader: true });
        }
        let onlyUpdateData = false;
        if (prevProps.report !== this.props.report) {
            onlyUpdateData = true;
        } 
        if (onlyUpdateData) {
            if (prevProps.flexData.equals(this.props.flexData)) {
                return;
            }
            this.state.flexmonster.updateData({ data: this.props.flexData.toJS() });
        } else {
            this.state.flexmonster.setReport(this.getCurrentReport());
        }
    }

    getCurrentReport() {
        let currentReport;
        if (!_lang.isEmpty(this.props.report)) {
            currentReport = _object.merge({}, this.props.report, DEFAULT_DASHBOARD_CONFIG,
                { dataSource: { data: this.props.flexData.toJS(), dataSourceType: 'json' } });
        } else {
            currentReport = _object.merge({}, DEFAULT_CONFIG, {
                slice: {
                    rows: [{ uniqueName: this.props.defaultColumn }],
                    measures: [
                        {
                            uniqueName: "Record UUID",
                            caption: "Records",
                            aggregation: "distinctcount"
                        }
                    ]
                }
            }, { dataSource: { data: this.props.flexData.toJS(), dataSourceType: 'json' } });
        }
        return currentReport;
    }

    handleReady(flexmonster) {
        flexmonster.on('reportchange', () => { this.updateFlexmonsterConditionals(); });
        this.props.onFlexmonsterLoad && this.props.onFlexmonsterLoad(flexmonster);
        if (this.state.flexmonster === null) {
            this.setState({
                flexmonster: flexmonster
            });
        }
    }

    updateFlexmonsterConditionals() {
        const reportConfig = this.state.flexmonster.getReport();
        if (reportConfig.conditions && reportConfig.conditions.find(c => !c.hasOwnProperty('isTotal'))) {
            const conditions = reportConfig.conditions.map(condition => Object.assign({}, condition, { isTotal: false }));
            const newReportConfig = Object.assign({}, reportConfig, { conditions });
            this.state.flexmonster.setReport(newReportConfig);
        }
    }

    render() {
        return (
            <React.Fragment>
                <Dimmer active={this.state.showLoader} inverted>
                    <Loader inverted>Loading...</Loader>
                </Dimmer>
                <Pivot
                    componentFolder={window.contextPath + "/resources/flexmonster/"}
                    toolbar={this.props.toolbar}
                    height={this.props.height}
                    width={this.props.width}
                    beforetoolbarcreated={this.props.toolbarCustomizer}
                    loadingdata={() => this.setState({ showLoader: false })}
                    openingreportfile={this.props.onJsonDataLoading}
                    dataloaded={this.props.onJsonDataLoaded}
                    ready={this.handleReady}
                    report={this.getCurrentReport()}
                />
            </React.Fragment>
        )
    }
}

Flexmonster.propTypes = {
    flexData: PropTypes.instanceOf(List).isRequired,
    defaultColumn: PropTypes.string,
    report: PropTypes.object,
    extraConfiguration: PropTypes.object,
    height: PropTypes.number,
    width: PropTypes.number,
    onFlexmonsterLoad: PropTypes.func,
    onJsonDataLoading: PropTypes.func,
    onJsonDataLoaded: PropTypes.func,
    toolbarCustomizer: PropTypes.func
};

Flexmonster.defaultProps = {
    toolbar: true,
};

const DEFAULT_DASHBOARD_CONFIG = {
    dataSource: {
        data: []
    },
    options: {
        configuratorActive: false,
        configuratorButton: false,
        chartMultipleMeasures: false,
        chart: {
            showMeasures: false,
            showDataLabels: true,
        },
        dateTimePattern: "yyyy-MM-dd HH:mm:ss",
        sorting: "columns"
    }
};

const DEFAULT_CONFIG = {
    dataSource: {
        data: []
    },
    formats: [
        {
            thousandsSeparator: ",",
            decimalPlaces: 2
        }
    ],
    options: {
        configuratorButton: false,
        configuratorActive: false,
        chartMultipleMeasures: false,
        chart: {
            showDataLabels: true,
        },
        dateTimePattern: "yyyy-MM-dd HH:mm:ss",
        sorting: "columns"
    },
};

export default Flexmonster;

export function pivotJsonProcessor(response, onSuccess) {
    const flexData = setupState(response.dataRows);
    onSuccess(flexData, response.defaultColumn, response.transponsed);
}

function translateRows(dataRow) {
    const counter = { type: "number", label: "Counter", value: 1 };
    return List(dataRow.dataElements).map(dataElement => {
        if (dataElement.type === "datetime") {
            return Object.assign({}, dataElement, {
                value: moment(dataElement.value, "YYYY-MM-DD HH:mm:ss").toISOString()
            })
        }
        return dataElement;
    }).push(counter);
}

function layOutTableData(acc, dataRow) {
    let header = {};
    let body = {};
    dataRow.forEach(dataElement => {
        const cleanedLabel = dataElement.label.trim().replace(/[()'"]/g, "");
        if (acc.isEmpty()) {
            header[cleanedLabel] = { type: dataElement.type };
        }
        body[dataElement.label]=dataElement.value;
    });
    !_lang.isEmpty(header) && (acc = acc.push(header));
    acc = acc.push(body);
    return acc;
}

export function setupState(dataRows) {
    return dataRows.map(translateRows).reduce(layOutTableData, List());
}
