import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';
import { useDrag, useDrop } from 'react-dnd';
import { ItemTypes } from "./constants";

const DraggableRow = ({ index, content, style, sortItem, sortingDone }) => {
    let ref;
    const [{ isDragging }, drag, preview] = useDrag({
        item: { type: ItemTypes.DRAGGABLE_ROW, index },
        end(item, monitor) {
            if (monitor.didDrop()) {
                sortingDone();
            }
        },
        collect: monitor => ({
            isDragging: monitor.isDragging(),
        }),
    });
    const [, drop] = useDrop({
        accept: ItemTypes.DRAGGABLE_ROW,
        hover(item, monitor) {
            const dragIndex = item.index;
            const hoverIndex = index;
            if (dragIndex === hoverIndex) {
                return; // Don't replace items with themselves
            }
            // Determine rectangle on screen
            const hoverBoundingRect = ref.getBoundingClientRect()
            // Get vertical middle
            const hoverMiddleY =
                (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
            // Determine mouse position
            const clientOffset = monitor.getClientOffset()
            // Get pixels to the top
            const hoverClientY = clientOffset.y - hoverBoundingRect.top
            // Only perform the move when the mouse has crossed half of the items height
            // When dragging downwards, only move when the cursor is below 50%
            // When dragging upwards, only move when the cursor is above 50%
            // Dragging downwards
            if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
                return
            }
            // Dragging upwards
            if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
                return
            }
            // Time to actually perform the action
            sortItem(dragIndex, hoverIndex)
            // Note: we're mutating the monitor item here!
            // Generally it's better to avoid mutations,
            // but it's good here for the sake of performance
            // to avoid expensive index searches.
            item.index = hoverIndex
        }
    });
    const opacity = isDragging ? 0.5 : 1;
    return (
        <tr ref={node => ref = drop(preview(node))} style={{ ...style, opacity }}>
            <td ref={node => drag(node)} className='sort-handle'><Icon name="bars" /></td>
            {content}
        </tr>
    );
};

DraggableRow.propTypes = {
    content: PropTypes.node.isRequired,
    index: PropTypes.number.isRequired,
    sortItem: PropTypes.func.isRequired,
    sortingDone: PropTypes.func.isRequired,
    style: PropTypes.object,
};

export default DraggableRow