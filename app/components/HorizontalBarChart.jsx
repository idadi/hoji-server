import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { Chart } from 'react-google-charts';
import { Loader } from 'semantic-ui-react';
import Table from './GoogleTable.jsx';
import { MAX_BARS_IN_BAR_CHART } from "./constants";

class HorizontalBarChart extends Component {

    render() {
        const chartAreaHeight = (this.props.chartData.get('data').size - 1) * 50;
        const chartHeight = chartAreaHeight + 80;
        const options = {
            legend: { position: 'none' },
            hAxis: {
                minValue: 0,
            },
            height: chartHeight,
            bars: 'horizontal',
            theme: 'material',
            chartArea: {
                left: "auto",
                top: 0,
                height: (chartHeight - 20),
            },
            colors: ['#F15854']
        };
        if (this.props.chartData.get('data').size < MAX_BARS_IN_BAR_CHART) {
            const rows = this.props.chartData.get('data')
                .map(dp => {
                    const percentage = Number((dp.get('value') / this.props.chartData.get('sampleSize') * 100).toFixed(2));
                    return [dp.get('label'), dp.get('value'), dp.get('value') + '(' + percentage + '%)'];
                });
            const columns = [
                { type: 'string', label: 'Response' },
                { type: 'number', label: 'Frequency' },
                { type: 'string', role: 'annotation' }
            ];
            return (
                <Chart
                    chartType="BarChart"
                    columns={columns}
                    rows={rows.toJS()}
                    loader={<Loader active />}
                    options={options}
                    height={chartHeight + 'px'}
                    width="100%"
                    chartPackages={["controls"]}
                />
            )
        } else {
            const rows = this.props.chartData.get('data')
                .map(dp => [dp.get('label'), dp.get('value'), (dp.get('value') / this.props.chartData.get('sampleSize') * 100).toFixed(2)]);
            const columns = [
                { type: 'string', label: 'RESPONSE' },
                { type: 'number', label: 'FREQUENCY' },
                { type: 'string', label: 'PERCENTAGE (%)' }
            ];
            return (
                <Table
                    columns={columns}
                    rows={rows.toJS()}
                    showRowNumber={true}
                    sortAscending={false}
                    sortColumn={1}
                    page="enabled"
                    pageSize={this.props.maxBars}
                />
            )
        }
    }
}

HorizontalBarChart.propTypes = {
    chartData: PropTypes.instanceOf(Map).isRequired,
    maxBars: PropTypes.number.isRequired
};

export default HorizontalBarChart
