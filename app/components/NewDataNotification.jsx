import React, { Component } from 'react';
import { Message, Transition } from 'semantic-ui-react';
import PropTypes from 'prop-types';

class NewDataNotification extends Component {

    render() {
        return (
            <Transition.Group animation="fly right" duration="500">
                {
                    this.props.show &&
                    <Message compact style={{ position: 'fixed', bottom: '5px', left: '5px', zIndex: 999 }}>
                        <Message.Content>
                            <p>
                                <span>New data received. Reload? </span>
                                <a onClick={this.props.onYesClicked} style={{ textDecoration: 'underline', cursor: 'pointer' }}>Yes</a>
                                {' | '}
                                <a onClick={this.props.onNoClicked} style={{ textDecoration: 'underline', cursor: 'pointer' }}>No</a>
                            </p>
                        </Message.Content>
                    </Message>
                }
            </Transition.Group>
        );
    }
}

NewDataNotification.propTypes = {
    show: PropTypes.bool.isRequired,
    onYesClicked: PropTypes.func.isRequired,
    onNoClicked: PropTypes.func.isRequired
};

export default NewDataNotification;
