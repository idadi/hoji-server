import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { Grid, Header, Segment } from "semantic-ui-react";
import PieChart from '../components/PieChart.jsx';
import HorizontalBarChart from '../components/HorizontalBarChart.jsx';
import Histogram from '../components/Histogram.jsx';
import WordTree from "../components/WordTree.jsx";
import { BINARY, CATEGORICAL, MAX_BARS_IN_BAR_CHART, TEXTUAL } from "./constants";

class BasicVisualization extends Component {
    shouldComponentUpdate(nextProps) {
        return !this.props.charts.equals(nextProps.charts);
    }

    render() {
        const charts = this.props.charts.map(chart => {
            let chartElement;
            if (chart.get('type') === BINARY) {
                chartElement = <PieChart chartData={chart} />;
            } else if (chart.get('type') === CATEGORICAL) {
                chartElement = <HorizontalBarChart chartData={chart} maxBars={MAX_BARS_IN_BAR_CHART}
                />;
            } else if (chart.get('type') === TEXTUAL) {
                chartElement = <WordTree chartData={chart} />;
            } else {
                chartElement = <Histogram chartData={chart} />;
            }
            return (
                <Grid.Row key={chart.get('key')}>
                    <Grid.Column className="block">
                        <Header attached="top" className="normal"><span dangerouslySetInnerHTML={{ __html: `${chart.get("label")} (n=${chart.get("sampleSize")})` }}></span></Header>
                        <Segment attached>
                            {chartElement}
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            )
        });
        return (
            <React.Fragment>
                {charts}
            </React.Fragment>
        );
    }
}

BasicVisualization.propTypes = {
    charts: PropTypes.instanceOf(List).isRequired,
};

export default BasicVisualization;