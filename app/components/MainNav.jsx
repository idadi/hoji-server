import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { Container, Dropdown, Menu } from "semantic-ui-react";

class MainNav extends Component {

    render() {
        const { anonymousUser, canfly, contextPath, fullName } = this.props;
        if (anonymousUser) {
            return (
                <React.Fragment>
                    <Menu inverted color="orange" borderless size="massive" fixed="top">
                        <Container>
                            <div class="right menu">
                                <a className="item" key="sign-up" href={`${contextPath}/signUp`}>Create account</a>
                                <a className="item" key="sign-in" href={`${contextPath}/signIn`}>Sign in</a>
                            </div>
                        </Container>
                    </Menu>
                </React.Fragment>
            );
        }
        return (
            <React.Fragment>
                <Menu inverted color="orange" borderless size="massive" fixed="top">
                    <Container>
                        <a className="item" key="website" href="https://www.hoji.co.ke">Hoji</a>
                        <a className="item" key="projects" href={`${contextPath}/`}>Projects</a>
                        <a className="item" key="choices" href={`${contextPath}/choice-group`}>Choices</a>
                        <Dropdown className="right item" text={fullName}>
                            <Dropdown.Menu>
                                <a className="item" key="account" href={`${contextPath}/user/view`}>My Account</a>
                                {canfly && <a className="item" key="e-templates" href={`${contextPath}/admin/template/list`}>Email Templates</a>}
                                <a className="item" key="sign-out" href={`${contextPath}/logout`}>Sign Out</a>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Container>
                </Menu>
            </React.Fragment>
        );
    }
}

MainNav.defaultProps = {
    reportTypes: [],
}

MainNav.propsTypes = {
    canfly: PropTypes.bool.isRequired,
    contextPath: PropTypes.string.isRequired,
    fullName: PropTypes.string.isRequired,
};

export default MainNav;