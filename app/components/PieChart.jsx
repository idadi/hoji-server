import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Map } from 'immutable';
import { Loader } from 'semantic-ui-react';
import { Chart } from 'react-google-charts';

class PieChart extends Component {

    render() {
        const options = {
            is3D: false,
            pieSliceText: 'value-and-percentage',
            legend: { position: 'right', alignment: 'center' },
            width: '100%',
            height: '300',
            theme: 'material',
            chartArea: {
                left: 0,
                top: 20,
                width: '100%',
                height: '272',
            },
            colors: ['#F15854', '#5DA5DA', '#FAA43A', '#60BD68', '#F17CB0', '#B2912F', '#B276B2', '#DECF3F', '#4D4D4D']
        };
        let chartData = this.props.chartData.get('data').map(dp => [dp.get('label'), dp.get('value')]);
        chartData = chartData.unshift(['Value', 'Count']);
        return (
            <Chart
                chartType="PieChart"
                data={chartData.toJS()}
                options={options}
                loader={<Loader active />}
                height="350px"
                width="100%"
                chartPackages={["controls"]}
            />
        )
    }
}

PieChart.propTypes = {
    chartData: PropTypes.instanceOf(Map).isRequired
};

export default PieChart
