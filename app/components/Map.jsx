import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import MarkerClusterer from '@google/markerclusterer'
import { Map as GoogleMap, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';

class Map extends Component {
    constructor(props) {
        super(props);

        const self = this;
        self.showInfoWindow = self.showInfoWindow.bind(self);
        self.closeInfoWindow = self.closeInfoWindow.bind(self);
        self.handleReady = self.handleReady.bind(self);
        self.handleResize = self.handleResize.bind(self);
        self.getBounds = self.getBounds.bind(self);

        self.state = {
            markers: [],
            mapVisible: false,
            activeMarker: null,
            activeGpsPoint: null,
            showingInfoWindow: false
        };
    }

    componentDidMount() {
        const self = this;
        ReactDOM.findDOMNode(self)
            .parentNode.setAttribute('style', "display: flex; flex-flow: column; flex: 1 0 auto;");
        console.log(self.props);
        const markers = self.props.gpsPoints.map((gp, index) => (
            <Marker key={index} name={gp.caption} position={{lat: gp.latitude, lng: gp.longitude}}
                    onClick={self.showInfoWindow} gpsPoint={gp}/>
        ));
        self.setState({
            markers: markers || [],
            mapVisible: false,
            activeMarker: null,
            activeGpsPoint: null,
            showingInfoWindow: false
        });
    }

    showInfoWindow(props, marker) {
        this.setState({
            activeMarker: marker,
            activeGpsPoint: props.gpsPoint,
            showingInfoWindow: true
        });
    }

    closeInfoWindow(props, marker) {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeGpsPoint: null
            })
        }
    }

    handleReady() {
        this.setState({
            mapVisible: true
        });
    }

    handleResize(mapProps, map) {
        this.setState({
            mapVisible: true
        });
        const bounds = this.getBounds(mapProps.children);
        const googleMap = window["google"];
        const googleBounds = new googleMap.maps.LatLngBounds(bounds.sw, bounds.ne);
        map.fitBounds(googleBounds);
    }

    getBounds(markers) {
        let bounds = markers.reduce((acc, marker) => acc.concat(marker), [])
            .filter(marker => marker && marker.props.position)
            .reduce((acc, marker) => {
                if (acc.sw && acc.ne) {
                    return {
                        sw: {
                            lat: Math.max(acc.sw.lat, marker.props.position.lat),
                            lng: Math.min(acc.sw.lng, marker.props.position.lng)
                        },
                        ne: {
                            lat: Math.min(acc.ne.lat, marker.props.position.lat),
                            lng: Math.max(acc.ne.lng, marker.props.position.lng)
                        }
                    };
                } else {
                    return {
                        sw: {
                            lat: marker.props.position.lat,
                            lng: marker.props.position.lng
                        },
                        ne: {
                            lat: marker.props.position.lat,
                            lng: marker.props.position.lng
                        }
                    };
                }
            }, {});
        return Object.assign({}, {
            sw: { lat: 43.40903821777057, lng: -33.48185394054356 },
            ne: { lat: -42.890625, lng: 80.15625 }
        }, bounds);
    }

    render() {
        const style = {display: "flex", flexFlow: "column", flex: "1 0 auto" };
        const containerStyle = Object.assign({}, style, {position: "relative"});
        const initialCenter = { lat: 6.342596964836323, lng: -341.3671875 };
        let mapToDisplay;
        if (this.state.markers.length > 100) {
            new MarkerClusterer(this.props.map, this.state.markers);
            mapToDisplay = null;
        } else {
            mapToDisplay = (
                <GoogleMap google={this.props.google} zoom={14} containerStyle={containerStyle}
                           visible={this.state.mapVisible} onReady={this.handleReady}
                           onResize={this.handleResize} initialCenter={initialCenter} clickableIcons={false}>
                    {this.state.markers}
                    <InfoWindow
                        marker={this.state.activeMarker}
                        visible={this.state.showingInfoWindow}
                        onClose={this.closeInfoWindow}>
                        <div id="info-window">
                            {this.state.activeGpsPoint &&
                            <Row>
                                <div>
                                    <h5 className="span_h2">{this.state.activeGpsPoint.address}</h5>
                                </div>
                                <div>
                                    <div className="divider">{''}</div>
                                    <p className="light">
                                        <span className="span_h2 green-text">Caption: </span>
                                        {this.state.activeGpsPoint.caption}
                                    </p>
                                    <p className="light">
                                        <span className="span_h2 green-text">Collected By: </span>
                                        {this.state.activeGpsPoint.user}
                                    </p>
                                    <p className="light">
                                        <span className="span_h2 green-text">Started On: </span>
                                        {this.state.activeGpsPoint.dateCreated}
                                    </p>
                                    <p className="light">
                                        <span className="span_h2 green-text">Completed On: </span>
                                        {this.state.activeGpsPoint.dateCompleted}
                                    </p>
                                    <p className="light">
                                        <span className="span_h2 green-text">Updated On: </span>
                                        {this.state.activeGpsPoint.dateUpdated}
                                    </p>
                                    <p className="light">
                                        <span className="span_h2 green-text">Uploaded On: </span>
                                        {this.state.activeGpsPoint.dateUploaded}
                                    </p>
                                    <p className="light">
                                        <span className="span_h2 green-text">Record Version: </span>
                                        {this.state.activeGpsPoint.version}
                                    </p>
                                    <p className="light">
                                        <span className="span_h2 green-text">Accuracy (m): </span>
                                        {this.state.activeGpsPoint.accuracy}
                                    </p>
                                    <p className="light">
                                        <span className="span_h2 green-text">Co-ordinates: </span>
                                        {this.state.activeGpsPoint.latitude + ', ' + this.state.activeGpsPoint.longitude}
                                    </p>
                                </div>
                            </Row>
                            }
                        </div>
                    </InfoWindow>
                </GoogleMap>
            )
        }
        return (
            <div style={style}>
                {mapToDisplay}
            </div>
        );
    }
}

Map.propTypes = {
    gpsPoints: PropTypes.array.isRequired
};

export default GoogleApiWrapper({
    apiKey: 'AIzaSyCTAKKrUmEVyF0JN2BxmG9KWSKv_7aOpIw',
    version: '3.34'
})(Map)