import React from "react";
import PropTypes from "prop-types";
import { Breadcrumb, Container } from "semantic-ui-react";

const SecondaryNav = ({sections}) => {
    return (
        <div className='breadcrumb-container'>
            <Container>
                <Breadcrumb icon='right angle' sections={sections} />
            </Container>
        </div>
    );
}

SecondaryNav.propsTypes = {
    sections: PropTypes.array.isRequired,
};

export default SecondaryNav;