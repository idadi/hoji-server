import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Dropdown } from 'semantic-ui-react';

const ReportSelector = ({ active, reportTypes }) => {
    return (
        <Dropdown button text='More reports' className='right floated green'>
            <Dropdown.Menu>
              {reportTypes
                  .filter(rt => !rt.path.includes(active))
                  .map((rt) => (<Dropdown.Item key={rt.text} as={Link} to={`./${rt.path}`} content={rt.text} />))}
            </Dropdown.Menu>
      </Dropdown>
    );
};

ReportSelector.propTypes = {
    active: PropTypes.string.isRequired,
    reportTypes: PropTypes.arrayOf(PropTypes.shape({ text: PropTypes.string.isRequired, path: PropTypes.string.isRequired })).isRequired,
};

export default ReportSelector;