//from: https://github.com/flexmonster/pivot-react/blob/master/ES6/app/containers/HomePage/flexmonster.react.js
import React from "react";
import PropTypes from "prop-types";
import ReactDOM from "react-dom";

class Pivot extends React.Component {
    constructor(props) {
        super(props);
        this.flexmonster = null;
    }

    render() {
        return <div id={'pivot-container'}>{' '}</div>;
    }

    componentDidMount() {
        let config = {};
        config.container = ReactDOM.findDOMNode(this);
        config.container.id = "fm-" + Date.now();
        this.parseProps(config);
        if (window.Flexmonster) {
            this.flexmonster = new window.Flexmonster(config);
        }
    }

    shouldComponentUpdate() {
        return false;
    }

    componentWillUnmount() {
        try {
            this.flexmonster.dispose();
        } catch (error) {
            // dispose already called
        }
    }

    parseProps(config) {
        if (this.props.toolbar !== undefined && this.props.toolbar) {
            config.toolbar = this.props.toolbar;
        }
        if (this.props.licenseKey !== undefined) {
            config.licenseKey = this.props.licenseKey;
        }
        if (this.props.width !== undefined) {
            config.width = this.props.width;
        }
        if (this.props.height !== undefined) {
            config.height = this.props.height;
        }
        if (this.props.componentFolder !== undefined) {
            config.componentFolder = this.props.componentFolder;
        }
        if (this.props.report !== undefined) {
            config.report = this.props.report;
        }
        if (this.props.global !== undefined) {
            config.global = this.props.global;
        }
        if (this.props.customizeCell !== undefined) {
            config.customizeCell = this.props.customizeCell;
        }
        // events
        if (this.props.cellclick !== undefined) {
            config.cellclick = this.props.cellclick;
        }
        if (this.props.celldoubleclick !== undefined) {
            config.celldoubleclick = this.props.celldoubleclick;
        }
        if (this.props.dataerror !== undefined) {
            config.dataerror = this.props.dataerror;
        }
        if (this.props.datafilecancelled !== undefined) {
            config.datafilecancelled = this.props.datafilecancelled;
        }
        if (this.props.dataloaded !== undefined) {
            config.dataloaded = this.props.dataloaded;
        }
        if (this.props.datachanged !== undefined) {
            config.datachanged = this.props.datachanged;
        }
        if (this.props.fieldslistclose !== undefined) {
            config.fieldslistclose = this.props.fieldslistclose;
        }
        if (this.props.fieldslistopen !== undefined) {
            config.fieldslistopen = this.props.fieldslistopen;
        }
        if (this.props.filteropen !== undefined) {
            config.filteropen = this.props.filteropen;
        }
        if (this.props.fullscreen !== undefined) {
            config.fullscreen = this.props.fullscreen;
        }
        if (this.props.loadingdata !== undefined) {
            config.loadingdata = this.props.loadingdata;
        }
        if (this.props.loadinglocalization !== undefined) {
            config.loadinglocalization = this.props.loadinglocalization;
        }
        if (this.props.loadingolapstructure !== undefined) {
            config.loadingolapstructure = this.props.loadingolapstructure;
        }
        if (this.props.loadingreportfile !== undefined) {
            config.loadingreportfile = this.props.loadingreportfile;
        }
        if (this.props.localizationerror !== undefined) {
            config.localizationerror = this.props.localizationerror;
        }
        if (this.props.localizationloaded !== undefined) {
            config.localizationloaded = this.props.localizationloaded;
        }
        if (this.props.olapstructureerror !== undefined) {
            config.olapstructureerror = this.props.olapstructureerror;
        }
        if (this.props.olapstructureloaded !== undefined) {
            config.olapstructureloaded = this.props.olapstructureloaded;
        }
        if (this.props.openingreportfile !== undefined) {
            config.openingreportfile = this.props.openingreportfile;
        }
        if (this.props.querycomplete !== undefined) {
            config.querycomplete = this.props.querycomplete;
        }
        if (this.props.queryerror !== undefined) {
            config.queryerror = this.props.queryerror;
        }
        if (this.props.ready !== undefined) {
            config.ready = () => {
                const collapseHandler = () => {
                    const report = this.flexmonster.getReport();
                    report.slice = Object.assign(report.slice, {
                        expands: { expandAll: false, row: [] },
                        drills: { drillAll: false }
                    });
                    this.flexmonster.setReport(report);
                    this.flexmonster.refresh();
                };
                const expandHandler = () => {
                    const report = this.flexmonster.getReport();
                    report.slice = Object.assign(report.slice, {
                        expands: { expandAll: true, row: [] },
                        drills: { drillAll: true }
                    });
                    this.flexmonster.setReport(report);
                    this.flexmonster.refresh();
                };
                this.flexmonster.customizeContextMenu((items, data, viewType) => {
                    const report = this.flexmonster.getReport();
                    const isExpandableContext = viewType === "pivot" && !data.isClassicTotalRow &&
                        report.slice.rows && (report.slice.rows.length - 1) > data.level;
                    if (isExpandableContext) {
                        const shouldCollapse = report.slice.expands;
                        if (shouldCollapse) {
                            items.unshift({
                                label: "Collapse all",
                                handler: collapseHandler
                            });
                        }
                        const shouldExpand = !report.slice.expands || (report.slice.expands && report.slice.expands.rows);
                        if (shouldExpand) {
                            items.unshift({
                                label: "Expand all",
                                handler: expandHandler
                            });
                        }
                    }
                    return items;
                });
                this.props.ready(this.flexmonster);
            }
        }
        if (this.props.reportchange !== undefined) {
            config.reportchange = this.props.reportchange;
        }
        if (this.props.reportcomplete !== undefined) {
            config.reportcomplete = this.props.reportcomplete;
        }
        if (this.props.reportfilecancelled !== undefined) {
            config.reportfilecancelled = this.props.reportfilecancelled;
        }
        if (this.props.reportfileerror !== undefined) {
            config.reportfileerror = this.props.reportfileerror;
        }
        if (this.props.reportfileloaded !== undefined) {
            config.reportfileloaded = this.props.reportfileloaded;
        }
        if (this.props.runningquery !== undefined) {
            config.runningquery = this.props.runningquery;
        }
        if (this.props.update !== undefined) {
            config.update = this.props.update;
        }
        if (this.props.beforetoolbarcreated !== undefined) {
            config.beforetoolbarcreated = this.props.beforetoolbarcreated;
        }
    }

}

Pivot.propTypes = {
    componentFolder: PropTypes.string,
    global: PropTypes.object,
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    report: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    licenseKey: PropTypes.string,
    toolbar: PropTypes.bool,
    customizeCell: PropTypes.func,
    cellclick: PropTypes.func,
    celldoubleclick: PropTypes.func,
    dataerror: PropTypes.func,
    datafilecancelled: PropTypes.func,
    dataloaded: PropTypes.func,
    datachanged: PropTypes.func,
    fieldslistclose: PropTypes.func,
    fieldslistopen: PropTypes.func,
    filteropen: PropTypes.func,
    fullscreen: PropTypes.func,
    loadingdata: PropTypes.func,
    loadinglocalization: PropTypes.func,
    loadingolapstructure: PropTypes.func,
    loadingreportfile: PropTypes.func,
    localizationerror: PropTypes.func,
    localizationloaded: PropTypes.func,
    olapstructureerror: PropTypes.func,
    olapstructureloaded: PropTypes.func,
    openingreportfile: PropTypes.func,
    querycomplete: PropTypes.func,
    queryerror: PropTypes.func,
    ready: PropTypes.func,
    reportchange: PropTypes.func,
    reportcomplete: PropTypes.func,
    reportfilecancelled: PropTypes.func,
    reportfileerror: PropTypes.func,
    reportfileloaded: PropTypes.func,
    runningquery: PropTypes.func,
    update: PropTypes.func,
    beforetoolbarcreated: PropTypes.func
};

export default Pivot;
