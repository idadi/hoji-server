import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'immutable';
import { Chart } from 'react-google-charts';
import { Loader } from 'semantic-ui-react';
import Table from './GoogleTable.jsx';

class Histogram extends Component {

    render() {
        const options = {
            legend: { position: 'none' },
            histogram: {
                lastBucketPercentile: 5
            },
            colors: ['#F15854']
        };
        let chartData = this.props.chartData.get('data').map(dp => [dp.get('label'), dp.get('value')]);
        chartData = chartData.unshift(['Label', 'Value']);
        const summaryStats = List().concat(this.props.chartData.get('summaryStats')
            .reduce((acc, ss) => {
                acc[0].push(ss.get('label'));
                acc[1].push(ss.get('value'));
                return acc;
            }, [[], []]));
        return (
            <div>
                <Chart
                    chartType="Histogram"
                    data={chartData.toJS()}
                    loader={<Loader active/>}
                    options={options}
                    height="250px"
                    width="100%"
                    chartPackages={["controls"]}
                />
                <Table
                    data={summaryStats}
                    showRowNumber={false}
                    sort="disabled"
                    height="50px"
                />
            </div>
        )
    }
}

Histogram.propTypes = {
    chartData: PropTypes.object.isRequired
};

export default Histogram
